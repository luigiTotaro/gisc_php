<?php
ob_start(); 
session_name( 'PHPSESSID' );
session_start();

    //se non loggato ritorno a login
    if (!isset($_SESSION['IdUtente'])){
        header("location: index.php");//redirect
    }else{
        $idUtente = $_SESSION['IdUtente'];
        $nominativo = $_SESSION['Nominativo'];
        $tipologiaUtente = $_SESSION['Tipologia'];
    }


    if (!isset($_GET['idRic']))
    {
        $ricoveroId = -1;
    }else{
        $ricoveroId = $_GET['idRic'];
    }

    if (!isset($_GET['id']))
    {
        $userId = -1;
    }else{
        $userId = $_GET['id'];
    }


    include('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
    $xcrudTemp = Xcrud::get_instance();
	$db = Xcrud_db::get_instance();

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Ricovero non cardiologico</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
    <script defer src="js/fontawesome-all.min.js"></script>
</head>
 
<body style="background-color:#cccccc;">

    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;display:none;">

        <div style="width:100%;">
            <img src="img/testataGisc.jpg" style="width:100%;">
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="logoff();">Esci</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="nuovoreclutamento();">Nuovo Reclutamento</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goMain();">Pannello Principale</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goStoria();">Storia Paziente</button>
            <div style="clear:both;"></div>
        </div>
        <div style="width:100%;padding:5%;">
            <?php

                
                //DATI PAZIENTE	
                //se abbiamo uno user id, sta facendo un nuovo ricovero non cardiologico, dunque non abbiamo l'id ricovero
                if ($ricoveroId!=-1)
                {
                    $sel = "SELECT *, DATE_FORMAT(DataRegistrazione, '%d-%m-%Y') as DataRegistrazioneFormat FROM paziente, ricoverononcardiologico WHERE paziente.IdPaziente=ricoverononcardiologico.idpaziente and ricoverononcardiologico.IDRicovero =".$ricoveroId;
                }
                else
                {
                    $sel = "SELECT *, DATE_FORMAT(DataRegistrazione, '%d-%m-%Y') as DataRegistrazioneFormat FROM paziente WHERE IdPaziente=".$userId;
                }

                $db->query($sel);
                //echo ($sel);
                $result = $db->result();
            	$resultPaziente = $result[0];

                $nomeCognome = $resultPaziente["Cognome"].' '.$resultPaziente["Nome"]; 
                $userId = $resultPaziente["IdPaziente"];
            
            ?>    
            <h3 class="titolo">Paziente: <? echo $nomeCognome ?></h3>
            <h4 >Reclutato il <? echo $resultPaziente["DataRegistrazioneFormat"] ?></h4>
            
            <div style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;">
                <?php

                    $xcrud->table('ricoverononcardiologico');
                    $xcrud->label('Data','Data Ricovero');
                    $xcrud->label('Note','Note');
                    $xcrud->fields('Data,Note');
                    $xcrud->hide_button( 'save_new, save_return, return' );
                    //$xcrud->buttons_position('left');
                    $xcrud->table_name('Scheda Ricovero non cardiologico');
                    $xcrud->layout(4);
                    if ($ricoveroId == -1)
                    {
                        $xcrud->set_var("IdUtente", $idUtente ); 
                        $xcrud->set_var("IdPaziente", $userId );
                        $xcrud->before_insert('before_insert_ricoverononcardio');
                        $xcrud->after_insert('after_insert_ricoverononcardio');
                        $xcrud->pass_default('Data',date('Y-m-d'));
                        echo $xcrud->render('create');
                    }
                    else
                    {
                        $xcrud->after_update('after_update_general');
                        echo $xcrud->render('edit', $ricoveroId);
                    }

                ?>    
            </div>
        </div>
    </div>

    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <script>

    function nuovoreclutamento()
    {
        window.location.href = 'schedaPaziente.php?id=-1';
    }

    function reclutamento()
    {
        window.location.href = 'schedaPaziente.php?id=<? echo $userId ?>';
    }

    function logoff()
    {
        window.location.href = 'logout.php';
    }

    function trend()
    {
        window.location.href = 'trend.php?id=<? echo $userId ?>';
    }

    function goMain()
    {
        window.location.href = 'main.php';
    }

    function goStoria()
    {
        window.location.href = 'storia.php?id=<? echo $userId ?>';
    }

    $( window ).resize(function() {
      var larghezza=$(window).width();
      if (larghezza>=1024) $('#contenuto').css("width","1024px");
      else $('#contenuto').css("width","100%");
      var larghFinestra=$('#contenuto').width();
      //console.log(larghFinestra);
      //console.log(larghezza);
      $('#contenuto').css("margin-left",(larghezza-larghFinestra)/2+"px");

    });

    jQuery(document).ready(function() { 

        $(window).trigger('resize');
        $('#contenuto').show();

    });


    </script>

</body>
</html>
<?php

    $query_statistiche_pazienti = "select (select count(IdPaziente) from Paziente) as NumTotale,
            (select count(IdPaziente) from Paziente where Sesso='M') as NumMaschi,
            (select count(IdPaziente) from Paziente where Sesso='F') as NumFemmine,
            (select sum(Eta) from Paziente where data_nascita is NULL) as SumEta,
            (select sum(Eta) from Paziente where Sesso='M' and data_nascita is NULL) as SumEtaM,
            (select sum(Eta) from Paziente where Sesso='F' and data_nascita is NULL) as SumEtaF,
            (select min(Eta) from Paziente where data_nascita is NULL) as EtaMin,
            (select max(Eta) from Paziente where data_nascita is NULL) as EtaMax,
            (select min(Eta) from Paziente where Sesso='M' and data_nascita is NULL) as EtaMinM,
            (select max(Eta) from Paziente where Sesso='M' and data_nascita is NULL) as EtaMaxM,
            (select min(Eta) from Paziente where Sesso='F' and data_nascita is NULL) as EtaMinF,
            (select max(Eta) from Paziente where Sesso='F' and data_nascita is NULL) as EtaMaxF,
            (select count(IdPaziente) from Paziente where gDead is not NULL) as dead,
            (select count(IdPaziente) from Paziente where Sesso='M' and gDead is not NULL) as deadM,
            (select count(IdPaziente) from Paziente where Sesso='F' and gDead is not NULL) as deadF,
            (select count(IdPaziente) from FattoreRischio_eziologia where FumoValore='+' or FumoValore='++' or FumoValore='+++') as Fumo,
            (select count(FattoreRischio_eziologia.IdPaziente) from FattoreRischio_eziologia, Paziente where (FumoValore='+' or FumoValore='++' or FumoValore='+++') and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and Sesso='M') as FumoM,
            (select count(FattoreRischio_eziologia.IdPaziente) from FattoreRischio_eziologia, Paziente where (FumoValore='+' or FumoValore='++' or FumoValore='+++') and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and Sesso='F') as FumoF,
            (select count(IdPaziente) from FattoreRischio_eziologia where FumoValore='+') as FumoP,
            (select count(FattoreRischio_eziologia.IdPaziente) from FattoreRischio_eziologia, Paziente where FumoValore='+' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and Sesso='M') as FumoPM,
            (select count(FattoreRischio_eziologia.IdPaziente) from FattoreRischio_eziologia, Paziente where FumoValore='+' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and Sesso='F') as FumoPF,
            (select count(IdPaziente) from FattoreRischio_eziologia where FumoValore='++') as FumoPP,
            (select count(FattoreRischio_eziologia.IdPaziente) from FattoreRischio_eziologia, Paziente where FumoValore='++' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and Sesso='M') as FumoPPM,
            (select count(FattoreRischio_eziologia.IdPaziente) from FattoreRischio_eziologia, Paziente where FumoValore='++' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and Sesso='F') as FumoPPF,
            (select count(IdPaziente) from FattoreRischio_eziologia where FumoValore='+++') as FumoPPP,
            (select count(FattoreRischio_eziologia.IdPaziente) from FattoreRischio_eziologia, Paziente where FumoValore='+++' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and Sesso='M') as FumoPPPM,
            (select count(FattoreRischio_eziologia.IdPaziente) from FattoreRischio_eziologia, Paziente where FumoValore='+++' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and Sesso='F') as FumoPPPF,
            (select count(IdQuadroClinico)  from QuadroClinico, Paziente where Paziente.idPaziente=QuadroClinico.idPaziente) as NumTotaleOss,
            (select count(IdQuadroClinico)  from QuadroClinico, Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and Sesso='M') as NumOssM,
            (select count(IdQuadroClinico)  from QuadroClinico, Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and Sesso='F') as NumOssF,
            (Select count(IdPaziente) from FattoreRischio_eziologia where ExFumo='1') as ExFumo,
            (Select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente where ExFumo='1' and FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and Sesso='M') as ExFumoM,
            (Select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente where ExFumo='1' and FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and Sesso='F') as ExFumoF";
    

    $query_trend = "Select ECGFrequenza as gFrequenzaCard,Eco2DFE,EcoDopplerPressPolm,ClasseDispneaSforzo,Replace(IngrCreat, ',', '.') as IngrCreat ,Replace(Replace(IngrBNP,'.',''),',','.') as IngrBNP, DATE_FORMAT(DataQuadroClinico, '%d/%m') as Data, DATE_FORMAT(DataQuadroClinico, '%d/%m/%Y') as DataEstesa ,DataQuadroClinico from QuadroClinico where idPaziente=%%idpaziente%% order by DataQuadroClinico ASC";


    $query_pagina_statistiche_pazienti_totali = "SELECT COUNT(0) num FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO'";


    $query_statistiche_FattoriRischio = "select ";
    // ipertensione
    $query_statistiche_FattoriRischio .= "(select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where TipoIperArt!='' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpert,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where TipoIperArt!='' and Sesso='M' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertM,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where TipoIperArt!='' and Sesso='F' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertF,";
    //insufficienza renale cronica
    $query_statistiche_FattoriRischio .= "(select count(Paziente.IdPaziente)  from QuadroClinico,Paziente where  QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and cast(REPLACE(IFNULL(IngrCreat,'0'),',','.') as DECIMAL) > 1.4) as NumCasiInsuffRenale,
                 (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and cast(REPLACE(IFNULL(IngrCreat,'0'),',','.') as DECIMAL) > 1.4 and Sesso='M') as NumCasiInsuffRenaleM,
                 (select count(Paziente.IdPaziente)  from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and  gTipo='RECLUTAMENTO' and cast(REPLACE(IFNULL(IngrCreat,'0'),',','.') as DECIMAL) > 1.4 and Sesso='F')as NumCasiInsuffRenaleF,"; 
    //diabete
    $query_statistiche_FattoriRischio .= "(select count(Paziente.IdPaziente)  from FattoreRischio_eziologia,QuadroClinico,Paziente where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and (Diabete1Data!= '' or Diabete2Data!='')) as NumCasiDiabete,
                 (select count(FattoreRischio_eziologia.IdPaziente)  from FattoreRischio_eziologia, Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and ((Diabete1Data!= '' or Diabete2Data!='') and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and Sesso='M')) as NumCasiDiabeteM,
                 (select count(FattoreRischio_eziologia.IdPaziente)  from FattoreRischio_eziologia, Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and ((Diabete1Data!= '' or Diabete2Data!='') and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and Sesso='F')) as NumCasiDiabeteF,";
    //iperdislipidemia
    $query_statistiche_FattoriRischio .= "(select count(Paziente.IdPaziente)  from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and (LDLDopoMag130='True' or LDLDopo100tra130='True' or HDLMin35='Si' or  TriglMag170='1')) as NumCasiIperdislipidemia,
                 (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and (LDLDopoMag130='True' or LDLDopo100tra130='True' or HDLMin35='Si' or  TriglMag170='1') and Sesso='M') as NumCasiIperdislipidemiaM,
                 (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and (LDLDopoMag130='True' or LDLDopo100tra130='True' or HDLMin35='Si' or  TriglMag170='1') and Sesso='F') as NumCasiIperdislipidemiaF,";
    //anemia
    $query_statistiche_FattoriRischio .= "(select count(Paziente.IdPaziente)  from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and ANEMIAHB='1' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO') as NumCasiAnemia,
                 (select count(Paziente.IdPaziente)  from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and ANEMIAHB='1' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and Sesso='M') as NumCasiAnemiaM,
                 (select count(Paziente.IdPaziente)  from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and ANEMIAHB='1' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and Sesso='F') as NumCasiAnemiaF,";
    //BPCO    
    $query_statistiche_FattoriRischio .= "(select count(Paziente.IdPaziente)  from FattoreRischio_eziologia,Paziente,QuadroClinico where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente  and FattoreRischio_eziologia.BPCO=1 and QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO') as NumCasiBPCO,
                 (select count(Paziente.IdPaziente)  from FattoreRischio_eziologia,Paziente,QuadroClinico where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente  and FattoreRischio_eziologia.BPCO=1 and QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and Sesso='M') as NumCasiBPCOM,
                 (select count(Paziente.IdPaziente)  from FattoreRischio_eziologia,Paziente,QuadroClinico where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente  and FattoreRischio_eziologia.BPCO=1 and QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and Sesso='F') as NumCasiBPCOF";
    $query_statistiche_FattoriRischio .= ";";

    $query_statistiche_FattoriRischio_ipertensione = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.TipoIperArt!='' @Condizioni GROUP BY sesso";
    $query_statistiche_FattoriRischio_insuffRenale = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO' and cast(REPLACE(IFNULL(Q.IngrCreat,'0'),',','.') as DECIMAL) > 1.4 @Condizioni GROUP BY sesso";
    $query_statistiche_FattoriRischio_diabete = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and (FR.Diabete1Data!= '' or FR.Diabete2Data!='') @Condizioni GROUP BY sesso";
    $query_statistiche_FattoriRischio_dislipidemia = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and (FR.LDLDopoMag130='True' or FR.LDLDopo100tra130='True' or FR.HDLMin35='Si' or  FR.TriglMag170='1') @Condizioni GROUP BY sesso";
    $query_statistiche_FattoriRischio_anemia = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.ANEMIAHB='1' @Condizioni GROUP BY sesso";
    $query_statistiche_FattoriRischio_bpco = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.BPCO=1 @Condizioni GROUP BY sesso";

    
    $query_statistiche_Eziologia = "select ";
    // ischemia
    $query_statistiche_Eziologia .= "(select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_coronaropatia='2' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIschemia,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_coronaropatia='2' and Sesso='M' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIschemiaM,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_coronaropatia='2' and Sesso='F' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIschemiaF,";

    //dilatativa
    $query_statistiche_Eziologia .= "(select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_cardiomiopatia='1' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiDilatativa,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_cardiomiopatia='1' and Sesso='M' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiDilatativaM,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_cardiomiopatia='1' and Sesso='F' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiDilatativaF,";

    //valvolopatie
    $query_statistiche_Eziologia .= "(select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_difettiValvolariCardiaci!='0' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiValvolopatie,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_difettiValvolariCardiaci!='0' and Sesso='M' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiValvolopatieM,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_difettiValvolariCardiaci!='0' and Sesso='F' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiValvolopatieF,";

    //ipertensione
    $query_statistiche_Eziologia .= "(select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where IpertensioneArt=1 and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertensiva,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where IpertensioneArt=1 and Sesso='M' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertensivaM,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where IpertensioneArt=1 and Sesso='F' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertensivaF,";

    //ipertensione + valvol
    $query_statistiche_Eziologia .= "(select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where IpertensioneArt=1 and Eziologia_difettiValvolariCardiaci!='0' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertensivaValvolare,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where IpertensioneArt=1 and Eziologia_difettiValvolariCardiaci!='0' and Sesso='M' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertensivaValvolareM,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where IpertensioneArt=1 and Eziologia_difettiValvolariCardiaci!='0' and Sesso='F' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertensivaValvolareF,";

    //ischemia + valvol
    $query_statistiche_Eziologia .= "(select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_coronaropatia='2' and Eziologia_difettiValvolariCardiaci!='0' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIschemiaValvolare,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_coronaropatia='2' and Eziologia_difettiValvolariCardiaci!='0' and Sesso='M' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIschemiaValvolareM,
                  (select count(Paziente.IdPaziente) from FattoreRischio_eziologia,Paziente,QuadroClinico where Eziologia_coronaropatia='2' and Eziologia_difettiValvolariCardiaci!='0' and Sesso='F' and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIschemiaValvolareF";

    $query_statistiche_Eziologia .= ";";

    $query_statistiche_Eziologia_ischemia = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.Eziologia_coronaropatia='2' @Condizioni GROUP BY sesso";
    $query_statistiche_Eziologia_dilatativa = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.Eziologia_cardiomiopatia='1' @Condizioni GROUP BY sesso";
    $query_statistiche_Eziologia_valvolopatie = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.Eziologia_difettiValvolariCardiaci!='0' @Condizioni GROUP BY sesso";
    $query_statistiche_Eziologia_ipertensiva = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.IpertensioneArt=1 @Condizioni GROUP BY sesso";
    $query_statistiche_Eziologia_iperValv = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.IpertensioneArt=1 and FR.Eziologia_difettiValvolariCardiaci!='0' @Condizioni GROUP BY sesso";
    $query_statistiche_Eziologia_ischemValv = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.Eziologia_coronaropatia='2' and FR.Eziologia_difettiValvolariCardiaci!='0' @Condizioni GROUP BY sesso";



    
    $query_statistiche_Clinica = "select ";
    // ischemia
    $query_statistiche_Clinica .= "(select count(Paziente.IdPaziente) from Paziente,QuadroClinico where (ClasseDispneaSforzo='III classe' or ClasseDispneaSforzo='IV classe') and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiNyha,
                  (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where (ClasseDispneaSforzo='III classe' or ClasseDispneaSforzo='IV classe') and Sesso='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiNyhaM,
                  (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where (ClasseDispneaSforzo='III classe' or ClasseDispneaSforzo='IV classe') and Sesso='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiNyhaF,";
    // FE < 35
    $query_statistiche_Clinica .= "(select count(Paziente.IdPaziente) from Paziente,QuadroClinico where CAST(Eco2DFE AS UNSIGNED) <35 and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiFE,
                  (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where CAST(Eco2DFE AS UNSIGNED) <35 and Sesso='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiFEM,
                  (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where CAST(Eco2DFE AS UNSIGNED) <35 and Sesso='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiFEF,";
    
    // FE tra 35 e 50
    $query_statistiche_Clinica .= "(select count(Paziente.IdPaziente) from Paziente,QuadroClinico where (CAST(Eco2DFE AS UNSIGNED) >= 35 AND CAST(Eco2DFE AS UNSIGNED) <=50) and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIntervalloFE,
                  (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where (CAST(Eco2DFE AS UNSIGNED) >= 35 AND CAST(Eco2DFE AS UNSIGNED) <=50) and Sesso='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIntervalloFEM,
                  (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where (CAST(Eco2DFE AS UNSIGNED) >= 35 AND CAST(Eco2DFE AS UNSIGNED) <=50) and Sesso='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIntervalloFEF,";
    
    // Ipertensione Polmonare
    $query_statistiche_Clinica .= "(select count(Paziente.IdPaziente) from Paziente,QuadroClinico where CAST(EcoDopplerPressPolm AS UNSIGNED) > 35 and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertPolmonare,
                  (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where CAST(EcoDopplerPressPolm AS UNSIGNED) > 35 and Sesso='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertPolmonareM,
                  (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where CAST(EcoDopplerPressPolm AS UNSIGNED) > 35 and Sesso='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiIpertPolmonareF,";

    //disf. diastolica
    $query_statistiche_Clinica .= "(select count(Paziente.IdPaziente) from Paziente,QuadroClinico where EcoDopplerDisfDiast = 'Alterato rilasciamento' and CAST(Eco2DFE AS UNSIGNED) >=50 and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiDisfDiastolica,
                  (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where EcoDopplerDisfDiast = 'Alterato rilasciamento' and CAST(Eco2DFE AS UNSIGNED) >=50 and Sesso='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiDisfDiastolicaM,
                  (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where EcoDopplerDisfDiast = 'Alterato rilasciamento' and CAST(Eco2DFE AS UNSIGNED) >=50 and Sesso='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO') as NumCasiDisfDiastolicaF";

    $query_statistiche_Clinica .= ";";

    
    $query_statistiche_Clinica_nyha = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO' and (Q.ClasseDispneaSforzo='III classe' or Q.ClasseDispneaSforzo='IV classe') @Condizioni GROUP BY sesso";
    $query_statistiche_Clinica_fe = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO' and CAST(Q.Eco2DFE AS UNSIGNED) <35 @Condizioni GROUP BY sesso";
    $query_statistiche_Clinica_intervFe = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO' and (CAST(Q.Eco2DFE AS UNSIGNED) >= 35 AND CAST(Q.Eco2DFE AS UNSIGNED) <=50) @Condizioni GROUP BY sesso";
    $query_statistiche_Clinica_iperPolm = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO' and CAST(EcoDopplerPressPolm AS UNSIGNED) > 35 @Condizioni GROUP BY sesso";
    $query_statistiche_Clinica_disfDiast = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO' and Q.EcoDopplerDisfDiast = 'Alterato rilasciamento' and CAST(Q.Eco2DFE AS UNSIGNED) >=50 @Condizioni GROUP BY sesso";


    $query_statistiche_Fumo = "select ";
    $query_statistiche_Fumo .= "(Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Fumo='Si') as numFumatori,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Fumo='Si' and Sesso='M') as numFumatoriM,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Fumo='Si' and Sesso='F') as numFumatoriF,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Fumo='Ex') as exFumatori,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Fumo='Ex' and Sesso='M') as exFumatoriM,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Fumo='Ex' and Sesso='F') as exFumatoriF";

    $query_statistiche_Ipertensione = "select ";
    $query_statistiche_Ipertensione .= "(Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1) as numIpertesi,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and Sesso='M') as numIpertesiM,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and Sesso='F') as numIpertesiF,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and data_nascita is NULL) as SumEta,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and data_nascita is NULL and Sesso='M') as SumEtaM,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and data_nascita is NULL and Sesso='F') as SumEtaF,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and data_nascita is NULL) as MinEta,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and data_nascita is NULL and Sesso='M') as MinEtaM,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and data_nascita is NULL and Sesso='F') as MinEtaF,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and data_nascita is NULL) as MaxEta,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and data_nascita is NULL and Sesso='M') as MaxEtaM,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and data_nascita is NULL and Sesso='F') as MaxEtaF";

    $query_statistiche_Diabete = "select ";
    $query_statistiche_Diabete .= "(Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1)) as numDiabetici,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and Sesso='M') as numDiabeticiM,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and Sesso='F') as numDiabeticiF,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and data_nascita is NULL) as SumEta,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and data_nascita is NULL and Sesso='M') as SumEtaM,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and data_nascita is NULL and Sesso='F') as SumEtaF,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and data_nascita is NULL) as MinEta,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and data_nascita is NULL and Sesso='M') as MinEtaM,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and data_nascita is NULL and Sesso='F') as MinEtaF,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and data_nascita is NULL) as MaxEta,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and data_nascita is NULL and Sesso='M') as MaxEtaM,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and data_nascita is NULL and Sesso='F') as MaxEtaF";

    $query_statistiche_ischemica = "select ";
    $query_statistiche_ischemica .= "(Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2) as numCasi,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and Sesso='M') as numCasiM,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and Sesso='F') as numCasiF,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and data_nascita is NULL) as SumEta,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and data_nascita is NULL and Sesso='M') as SumEtaM,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and data_nascita is NULL and Sesso='F') as SumEtaF,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and data_nascita is NULL) as MinEta,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and data_nascita is NULL and Sesso='M') as MinEtaM,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and data_nascita is NULL and Sesso='F') as MinEtaF,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and data_nascita is NULL) as MaxEta,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and data_nascita is NULL and Sesso='M') as MaxEtaM,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=2 and data_nascita is NULL and Sesso='F') as MaxEtaF";

    $query_statistiche_ima = "select ";
    $query_statistiche_ima .= "(Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1) as numCasi,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and Sesso='M') as numCasiM,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and Sesso='F') as numCasiF,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and data_nascita is NULL) as SumEta,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and data_nascita is NULL and Sesso='M') as SumEtaM,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and data_nascita is NULL and Sesso='F') as SumEtaF,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and data_nascita is NULL) as MinEta,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and data_nascita is NULL and Sesso='M') as MinEtaM,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and data_nascita is NULL and Sesso='F') as MinEtaF,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and data_nascita is NULL) as MaxEta,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and data_nascita is NULL and Sesso='M') as MaxEtaM,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.Eziologia_coronaropatia=1 and data_nascita is NULL and Sesso='F') as MaxEtaF";


    $query_statistiche_bpco = "select ";
    $query_statistiche_bpco .= "(Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1) as numCasi,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and Sesso='M') as numCasiM,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and Sesso='F') as numCasiF,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and data_nascita is NULL) as SumEta,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and data_nascita is NULL and Sesso='M') as SumEtaM,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and data_nascita is NULL and Sesso='F') as SumEtaF,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and data_nascita is NULL) as MinEta,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and data_nascita is NULL and Sesso='M') as MinEtaM,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and data_nascita is NULL and Sesso='F') as MinEtaF,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and data_nascita is NULL) as MaxEta,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and data_nascita is NULL and Sesso='M') as MaxEtaM,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.BPCO=1 and data_nascita is NULL and Sesso='F') as MaxEtaF";

    $query_statistiche_InsufRenCron = "select ";
    $query_statistiche_InsufRenCron .= "(Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1) as numCasi,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and Sesso='M') as numCasiM,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and Sesso='F') as numCasiF,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and data_nascita is NULL) as SumEta,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and data_nascita is NULL and Sesso='M') as SumEtaM,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and data_nascita is NULL and Sesso='F') as SumEtaF,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and data_nascita is NULL) as MinEta,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and data_nascita is NULL and Sesso='M') as MinEtaM,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and data_nascita is NULL and Sesso='F') as MinEtaF,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and data_nascita is NULL) as MaxEta,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and data_nascita is NULL and Sesso='M') as MaxEtaM,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.InsufRenaleCronica=1 and data_nascita is NULL and Sesso='F') as MaxEtaF";

    $query_statistiche_Anemia = "select ";
    $query_statistiche_Anemia .= "(Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1) as numCasi,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and Sesso='M') as numCasiM,
                    (Select count(Paziente.idPaziente) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and Sesso='F') as numCasiF,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and data_nascita is NULL) as SumEta,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and data_nascita is NULL and Sesso='M') as SumEtaM,
                    (select sum(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and data_nascita is NULL and Sesso='F') as SumEtaF,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and data_nascita is NULL) as MinEta,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and data_nascita is NULL and Sesso='M') as MinEtaM,
                    (select min(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and data_nascita is NULL and Sesso='F') as MinEtaF,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and data_nascita is NULL) as MaxEta,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and data_nascita is NULL and Sesso='M') as MaxEtaM,
                    (select max(Eta) from Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and data_nascita is NULL and Sesso='F') as MaxEtaF";

    $query_statistiche_Colesterolo = "select ";
    $query_statistiche_Colesterolo .= "(select count(Paziente.IdPaziente) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100) as numCasi,
                    (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and Sesso='M') as numCasiM,
                    (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and Sesso='F') as numCasiF,
                    (select sum(Eta) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and data_nascita is NULL) as SumEta,
                    (select sum(Eta) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and data_nascita is NULL and Sesso='M') as SumEtaM,
                    (select sum(Eta) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and data_nascita is NULL and Sesso='F') as SumEtaF,
                    (select min(Eta) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and data_nascita is NULL) as MinEta,
                    (select min(Eta) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and data_nascita is NULL and Sesso='M') as MinEtaM,
                    (select min(Eta) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and data_nascita is NULL and Sesso='F') as MinEtaF,
                    (select max(Eta) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and data_nascita is NULL) as MaxEta,
                    (select max(Eta) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and data_nascita is NULL and Sesso='M') as MaxEtaM,
                    (select max(Eta) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and IngrLDL>100 and data_nascita is NULL and Sesso='F') as MaxEtaF";

    $query_statistiche_RigurgitoMitralicoSevero = "select ";
    $query_statistiche_RigurgitoMitralicoSevero .= "(select count(Paziente.IdPaziente) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and EcoDopplerRigurMitralico='+++') as numCasi,
                    (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and EcoDopplerRigurMitralico='+++' and Sesso='M') as numCasiM,
                    (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and EcoDopplerRigurMitralico='+++' and Sesso='F') as numCasiF";

    $query_statistiche_StenosiAortica = "select ";
    $query_statistiche_StenosiAortica .= "(select count(Paziente.IdPaziente) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and gStenosiAortica<>'') as numCasi,
                    (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and gStenosiAortica<>'' and Sesso='M') as numCasiM,
                    (select count(Paziente.IdPaziente) from Paziente,QuadroClinico where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and gStenosiAortica<>'' and Sesso='F') as numCasiF";


    $query_statistiche_Ricoveri = "select ";
    $query_statistiche_Ricoveri .= "(Select count(*) from RicoveroNonCardiologico) as NumRicoveriNonCardio,
                                    (Select count(*) from QuadroClinico where gTipo='RICOVERO' and (RicoveroProgrammato IS NULL or RicoveroProgrammato='False')) as NumRicoveri,
                                    (Select count(*) from QuadroClinico where gTipo='RICOVERO' and (RicoveroProgrammato='True')) as NumRicoveriProgrammati,
                                    (Select count(*) from QuadroClinico,Utente where Utente.idUtente = QuadroClinico.idUtente and idTipoUtente=2 and IFNULL(gTipo,'')!= 'RECLUTAMENTO') as NumControlliMMG,
                                    (Select count(*) from QuadroClinico,Utente where Utente.idUtente = QuadroClinico.idUtente and idTipoUtente=4 and IFNULL(gTipo,'')!='RECLUTAMENTO') as NumControlliSpec";


    $query_pagina_statistiche_Aritmie_totale = "SELECT COUNT(0) num FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO'";

    $query_pagina_statistiche_Aritmie_generale = "SELECT COUNT(0) num, SUM(P.Eta) sum, MIN(P.Eta) min, MAX(P.Eta) max, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_Aritmie_fumo = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.Fumo='Si' @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_Aritmie_ipert = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.IpertensioneArt=1 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_Aritmie_diabete = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and (FR.Diabete1=1 OR FR.Diabete2=1) @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_Aritmie_bpco = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.BPCO=1 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_Aritmie_insufRenal = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.InsufRenaleCronica=1 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_Aritmie_anemia = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.ANEMIAHB=1 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_Aritmie_ischemica = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.Eziologia_coronaropatia=2 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_Aritmie_ima = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.Eziologia_coronaropatia=1 @Condizioni GROUP BY sesso";

    //$query_pagina_statistiche_Aritmie_bmi = "SELECT COUNT(0), p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente  WHERE gtipo='RECLUTAMENTO' and (Q.Obesita <>'No' and Q.Obesita <> '') @Condizioni GROUP BY sesso";

    //$query_pagina_statistiche_Aritmie_ipertiroidismo = "SELECT COUNT(0), p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente  WHERE gtipo='RECLUTAMENTO' and Q.IpertiroidInizio <>'' @Condizioni GROUP BY sesso";

    //$query_pagina_statistiche_Aritmie_sindrome = "SELECT COUNT(0), p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente  WHERE gtipo='RECLUTAMENTO' and (Q.ObesitaUnifAddomData <>'' or Q.IperglicemiaData <>'' or Q.IpertArtData <>'' or Q.IperDislitemiaData <>'') @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_Aritmie_colesterolo = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente  INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and Q.IngrLDL > 100 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_Aritmie_dead = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente  INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and P.gDead != '' @Condizioni GROUP BY sesso";


/*
    $query_pagina_statistiche_Aritmie = "select ";

    $query_pagina_statistiche_Aritmie .= "(select count(Paziente.IdPaziente) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' @Condizioni) as NumTot,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO') as NumTotaleGenerale,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and Sesso='M' @Condizioni) as NumM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO'and Sesso='F' @Condizioni) as NumF,
                                        (select sum(Eta) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' @Condizioni) as SumEta,
                                        (select sum(Eta) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and Sesso='M' @Condizioni) as SumEtaM,
                                        (select sum(Eta) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and Sesso='F' @Condizioni) as SumEtaF,
                                        (select min(Eta) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as EtaMin,
                                        (select max(Eta) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as EtaMax,
                                        (select min(Eta) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and Sesso='M' @Condizioni) as EtaMinM,
                                        (select max(Eta) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and Sesso='M' @Condizioni) as EtaMaxM,
                                        (select min(Eta) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and Sesso='F' @Condizioni) as EtaMinF,
                                        (select max(Eta) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and Sesso='F' @Condizioni) as EtaMaxF,

                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where QuadroClinico.IdPaziente=Paziente.IdPaziente and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO'  and (FattoreRischio_eziologia.Fumo='Si') @Condizioni) as Fumo,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where QuadroClinico.IdPaziente=Paziente.IdPaziente and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO'  and (FattoreRischio_eziologia.Fumo='Si') and Sesso='M' @Condizioni) as FumoM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where QuadroClinico.IdPaziente=Paziente.IdPaziente and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO'  and (FattoreRischio_eziologia.Fumo='Si') and Sesso='F' @Condizioni) as FumoF,
                                        
                                        (select count(Paziente.IdPaziente) from Paziente,QuadroClinico,FattoreRischio_eziologia where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' @Condizioni) as NumCasiIpert,
                                        (select count(Paziente.IdPaziente) from Paziente,QuadroClinico,FattoreRischio_eziologia where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and Sesso='M'  and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' @Condizioni) as NumCasiIpertM,
                                        (select count(Paziente.IdPaziente) from Paziente,QuadroClinico,FattoreRischio_eziologia where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and FattoreRischio_eziologia.IpertensioneArt=1 and Sesso='F'  and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' @Condizioni) as NumCasiIpertF,
                                        
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where QuadroClinico.IdPaziente=Paziente.IdPaziente and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO'  and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) @Condizioni) as NumCasiDiabete,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where QuadroClinico.IdPaziente=Paziente.IdPaziente and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO'  and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and Sesso='M' @Condizioni) as NumCasiDiabeteM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where QuadroClinico.IdPaziente=Paziente.IdPaziente and FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO'  and (FattoreRischio_eziologia.Diabete1=1 OR FattoreRischio_eziologia.Diabete2=1) and Sesso='F' @Condizioni) as NumCasiDiabeteF,
                                        
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,IMA where IMA.Data <>'' and QuadroClinico.IdPaziente=Paziente.IdPaziente and IMA.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiIMA,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,IMA where IMA.Data <>'' and Sesso ='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente and IMA.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiIMAM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,IMA where IMA.Data <>''and Sesso ='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente and IMA.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiIMAF,

                                        (select count(Paziente.IdPaziente) from QuadroClinico,FattoreRischio_eziologia,Paziente where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO'  and FattoreRischio_eziologia.BPCO=1 @Condizioni) as NumCasiBPCO,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,FattoreRischio_eziologia,Paziente where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO'  and  FattoreRischio_eziologia.BPCO=1 and Sesso='M' @Condizioni) as NumCasiBPCOM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,FattoreRischio_eziologia,Paziente where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO'  and  FattoreRischio_eziologia.BPCO=1 and Sesso='F' @Condizioni) as NumCasiBPCOF,

                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and FattoreRischio_eziologia.InsufRenaleCronica=1 @Condizioni) as NumCasiInsuffRenale,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and FattoreRischio_eziologia.InsufRenaleCronica=1 and Sesso='M' @Condizioni) as NumCasiInsuffRenaleM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and FattoreRischio_eziologia.InsufRenaleCronica=1 and Sesso='F' @Condizioni) as NumCasiInsuffRenaleF,

                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiAnemia,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and Sesso='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiAnemiaM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,FattoreRischio_eziologia where FattoreRischio_eziologia.idPaziente=Paziente.idPaziente and FattoreRischio_eziologia.ANEMIAHB=1 and Sesso='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiAnemiaF,

                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where Obesita <>'No' and Obesita <> '' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiBMI,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where Obesita <>'No' and Obesita <> ''and Sesso='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiBMIM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where Obesita <>'No' and Obesita <>''and Sesso='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiBMIF,

                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where IpertiroidInizio <>'' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiIpertiroidismo,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where IpertiroidInizio <>'' and Sesso='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiIpertiroidismoM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where IpertiroidInizio <>'' and Sesso='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiIpertiroidismoF,

                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where (ObesitaUnifAddomData <>'' or IperglicemiaData <>'' or IpertArtData <>'' or IperDislitemiaData <>'') and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiSindrome,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where (ObesitaUnifAddomData <>'' or IperglicemiaData <>'' or IpertArtData <>'' or IperDislitemiaData <>'') and Sesso='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiSindromeM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where Sesso='F'and (ObesitaUnifAddomData <>'' or IperglicemiaData <>'' or IpertArtData <>'' or IperDislitemiaData <>'') and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiSindromeF,

                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where IngrLDL > 100 and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiColesterolo,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where IngrLDL > 100 and Sesso='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiColesteroloM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where IngrLDL > 100 and Sesso='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiColesteroloF,

                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,CardiopIschemNOIMA where CardiopIschemNOIMA.DataPrimoRilievo <>'' and QuadroClinico.IdPaziente=Paziente.IdPaziente and CardiopIschemNOIMA.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiIMANodoc,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,CardiopIschemNOIMA where CardiopIschemNOIMA.DataPrimoRilievo <>'' and Sesso ='M' and QuadroClinico.IdPaziente=Paziente.IdPaziente and CardiopIschemNOIMA.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiIMANodocM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente,CardiopIschemNOIMA where CardiopIschemNOIMA.DataPrimoRilievo <>'' and Sesso ='F' and QuadroClinico.IdPaziente=Paziente.IdPaziente and CardiopIschemNOIMA.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' @Condizioni) as NumCasiIMANodocF,

                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and gDead != '' @Condizioni) as NumDeath,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and gDead != '' and Sesso='M' @Condizioni) as NumDeathM,
                                        (select count(Paziente.IdPaziente) from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and gDead != '' and Sesso='F' @Condizioni) as NumDeathF";

    $query_pagina_statistiche_Aritmie .= ";";
*/

    $query_pagina_statistiche_altre_generale = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_pm = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and Q.PM='1' @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_icd = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and Q.ICD=1 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_rtc = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and (Q.PMBiventricolDataImpianto !='' or Q.PMBiventricol!='') @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_rtcicd = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and (Q.PMBiventricolDataImpianto !='' or Q.PMBiventricol!='') and Q.ICD=1 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_fapaross = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and Q.FAParadossDataInizio!='' @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_faperm = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and Q.FAPermanenteDataInizio!='' @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_irc = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.InsufRenaleCronica=1 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_bpco = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.BPCO=1 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_anemia = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.ANEMIAHB='1' @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_nyha = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and (Q.ClasseDispneaSforzo='III classe' or Q.ClasseDispneaSforzo='IV classe') @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_fe = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and CAST(Q.Eco2DFE AS UNSIGNED) <35 @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_intervFe = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and (CAST(Q.Eco2DFE AS UNSIGNED) >= 35 AND CAST(Q.Eco2DFE AS UNSIGNED) <50) @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_diabete = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and (FR.Diabete1Data!= '' or FR.Diabete2Data!='') @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_congPolm = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO' and Q.CongestionePolmonare<>'' and Q.CongestionePolmonare IS NOT NULL and Q.CongestionePolmonare<>'Assente' @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_congPerif = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO' and (Q.CongestionePerifGiugularimag3=1 or Q.CongestionePerifEpatomegalia=1 or Q.CongestionePerifEdemiPeriferici != '' or Q.CongestionePerifAscite=1) @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_congPolmPerif = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente WHERE gtipo='RECLUTAMENTO' and Q.CongestionePolmonare<>'' and Q.CongestionePolmonare IS NOT NULL and Q.CongestionePolmonare<>'Assente' and (Q.CongestionePerifGiugularimag3=1 or Q.CongestionePerifEpatomegalia=1 or Q.CongestionePerifEdemiPeriferici != '' or Q.CongestionePerifAscite=1) @Condizioni GROUP BY sesso";

    $query_pagina_statistiche_altre_valvolopatie = "SELECT COUNT(0) num, p.sesso FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' and FR.Eziologia_difettiValvolariCardiaci<>0 @Condizioni GROUP BY sesso";


    $query_pagina_statistiche_mortalita = "SELECT count(0) num FROM paziente P WHERE P.gdead IS NOT NULL and DATEDIFF(P.gdead, P.DataRegistrazione)<=@Valore";
/*
somme considerando la data di nascita.... e l'età inserita dove non presente la data di nascita
select sum(Eta) as sum,
    sum(IF(IFNULL(data_nascita,0)=0,Eta, FLOOR
        (DATEDIFF(CURRENT_DATE, Paziente.data_nascita)/365))) as sum1
    from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO'
*/

?>

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="nuovopaziente.aspx.cs" Inherits="nuovopaziente" %>

<%@ Register Src="datipaziente.ascx" TagName="datipaziente" TagPrefix="uc1" %>
<!--#include file="include/controllo.inc"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="stile.css" type=text/css rel="stylesheet" />
<script language="JavaScript" src="./js/ts_picker.js"></script>
<script language="JavaScript" src="./js/controlli.js"></script>
<title>Gestione Paziente</title>

<style type="text/css">
<!--
body {
	background-image: url(./files/bg_blueGrad.gif);
	background-color: #324671;
}
.Stile1 {color: #336DD0}
-->
</style>
<script>
    function data_change(field) {
        var check = true;
        var value = field.value; //get characters
        //check that all characters are digits, ., -, or ""
        for (var i = 0; i < field.value.length; ++i) {
            var new_key = value.charAt(i); //cycle through characters
            if (((new_key < "0") || (new_key > "9")) &&
                    !(new_key == "") &&  !(new_key == ".") && !(new_key==",")) {
                check = false;
                break;
            }
        }
        //apply appropriate colour based on value
        if (!check) {
            field.style.backgroundColor = "red";
        }
        else {
            field.style.backgroundColor = "white";
        }
    }
                                       </script>
</head>

<body>
   <form id="form1" runat="server">
<table width="812" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td width="10" valign="top" background="./files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center">
    
    <div align="left" style="margin-bottom:10px">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
              <!--#include file="include/pazienti.inc"-->

             <div class="modernbricksmenuline">
                <ul>
                    <li style="margin-left: 1px"></li>
                  
                    <asp:Label ID="lb_menu" runat="server" Text=""></asp:Label>
                </ul>
              </div>
          </td>
          </tr>
          <tr>
            <td align="center">
                <br />
                <br />
                <span style="font-size:18px;color:#324671;"><asp:Label ID="lb_titolo" runat="server" Text=""></asp:Label></span>
                <br />
                <br />
                <uc1:datipaziente ID="Datipaziente1" runat="server" Visible="false" />
            </td>
          </tr>
          <tr >
            <td align="center">
                <br />
                <br />
                I campi contrassegnati con * sono obbligatori                 <br /><br />
                <asp:Label ID="lb_steps" Visible="false" runat="server" Text=""></asp:Label><br />
                
                <table><tr style="text-align:center;">
             
                <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
               
                <asp:LinkButton  ForeColor="Blue" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttAnagrafica" Text="Anagrafica Paziente" 
                        onclick="bttAnamnesi_Click"> <img src="img/profile.png" style="margin:3px; border:0px;" /><br />Anagrafica Paziente</asp:LinkButton>
                </td>
                 <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                 
                <asp:LinkButton ToolTip="Precedenti Accessi Pronto Soccorso per Scompensi"  ForeColor="#333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                         runat="server" ID="bttPrecScomp" Text="Prec.Scomp." 
                         onclick="bttQuadro_Click">
                         <img src="img/Broken.png" style="margin:3px;border:0px;" /><br />Prec.Acc.P.S. Per Scomp.
                         </asp:LinkButton>
                </td>
               
                <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                
                <asp:LinkButton ToolTip="Precedenti Ospedalizzazioni per Scompensi"  ForeColor="#333333" style="font-size:12px;  font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttPrecOsp" Text="Prec.Osp." onclick="bttStrumen_Click">
                        <img src="img/Ambunance 1.png" style="margin:3px;border:0px;" /><br />Prec.Osp. Scomp.
                        </asp:LinkButton>
                </td>
                  <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:middle;">
              
                <asp:LinkButton  ForeColor="#333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttFattRischio" Text="Fatt.Rischio e Comorbidit�" 
                        onclick="bttConsulenze_Click">
                          <img src="img/Red Cross.png" style="margin:3px; border:0px;" /><br />Fatt.Rischio e Comorbidit�
                        </asp:LinkButton>
                </td>
                <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:middle;">
               
                <asp:LinkButton  ForeColor="#333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttEziologie" Text="Eziologie" 
                        onclick="bttLaboratorio_Click">
                         <img src="img/Documents.png" style="margin:3px; border:0px;" /><br />Eziologie
                        </asp:LinkButton>
                </td>
                 <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                
                     &nbsp;<asp:LinkButton  ForeColor="333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttAnamnesi" Text="Aritmie e Quadro Clinico" onclick="bttQuadroClinico_Click1" 
                        ><img src="img/Folder History.png" style="margin:3px;border:0px;" /><br />Quadro Clinico</asp:LinkButton>
                </td>
                
                </tr><tr style="text-align:center;">
                 <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                
                     &nbsp;<asp:LinkButton  ForeColor="333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="LinkButton3" Text="Aritmie e Quadro Clinico" onclick="bttAnamnesi_Click1" 
                        ><img src="img/Folder History.png" style="margin:3px;border:0px;" /><br />Aritmie</asp:LinkButton>
                </td>
                
                
                
              
                   <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                
                <asp:LinkButton  ForeColor="#333333" style="font-size:12px;  font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttStrumen" Text="Es. Strum." onclick="bttStrumen_Click1" > <img src="img/Red Stethoscope.png" style="border:0px;margin:3px;" />&nbsp;
                    <br />Es.Strum.</asp:LinkButton>
                </td>
                <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                
                <asp:LinkButton  ForeColor="#333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttLaboratorio" Text="Lab." 
                        onclick="bttLaboratorio_Click1" ><img src="img/Colba.png" style="margin:3px;border:0px;" />&nbsp;
                    <br />Lab.</asp:LinkButton>
                </td>
                <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                 
                <asp:LinkButton  ForeColor="#333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttConsulenze" Text="Consulenze" onclick="bttConsulenze_Click1" 
                        > <img src="img/Head physician.png" style="margin:3px; border:0px;" /><br />Consulenze</asp:LinkButton>
                </td>
                <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
              
                <asp:LinkButton  ForeColor="#333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttTerapie" Text="Terapie" onclick="bttTerapie_Click" >  <img src="img/Pills 5.png" style="border:0px;margin:3px;" />&nbsp;
                    <br />Terapie</asp:LinkButton>
                </td>
                 <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                
                     &nbsp;<asp:LinkButton  ForeColor="333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttDiagnosi" Text="Diagnosi" onclick="LinkButton4_Click"
                        ><img src="img/Edit Text.png" style="margin:3px;border:0px;" /><br />Diagnosi</asp:LinkButton>
                </td>
                </tr>
                </table>
                <br />
                <div style="width:100%; background-color:#eeeeee; height:30px; vertical-align:middle; text-decoration:none; font-size:12px; color:#333333;">
                    <div style="margin-top:8px;"><i><asp:LinkButton ForeColor="#333333" 
                            ID="LinkButton1" runat="server" style="font-weight: 700;" 
                            onclick="LinkButton1_Click">Salva Tutto</asp:LinkButton></i></div>
                    </div>
                <br />
                <br />
            </td>
          </tr>
          <tr >
            <td align="center">
                <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" FinishCompleteButtonText="Fine"
                    FinishPreviousButtonText="Indietro" OnFinishButtonClick="Wizard1_FinishButtonClick"
                    StartNextButtonText="Avanti" StepNextButtonText="Avanti" 
                    StepPreviousButtonText="Indietro" BackColor="#EFF3FB" BorderColor="#B5C7DE" 
                    BorderWidth="1px" DisplaySideBar="False" Font-Names="Verdana" Font-Size="12px" 
                    OnActiveStepChanged="Wizard1_ActiveStepChanged"  
                    DisplayCancelButton="false" NavigationButtonStyle-Height="0" 
                    NavigationButtonStyle-Width="0" NavigationButtonStyle-BorderStyle="None"
                    OnNextButtonClick="Wizard1_NextButtonClick" CancelButtonText="Annulla">

<NavigationButtonStyle Height="0px" Width="0px"></NavigationButtonStyle>

                    <SideBarStyle VerticalAlign="Top" BackColor="#507CD1" Font-Size="12px" />
                     <StepNavigationTemplate>

<asp:Button ID="StepPreviousButton" runat="server" CausesValidation="False" CommandName="MovePrevious"

Text="Previous" Visible="False" />

<asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" Text=" s Next"

Visible="False" />

</StepNavigationTemplate>

<FinishNavigationTemplate>

<asp:Button ID="FinishPreviousButton" runat="server" CausesValidation="False" CommandName="MovePrevious"

Text="Previous" Visible="False" />

<asp:Button ID="FinishButton" runat="server" CommandName="MoveComplete" Text="Finish"

Visible="False" />

</FinishNavigationTemplate>
                    <WizardSteps>
                        <asp:WizardStep ID="WizardStep1" runat="server" Title="Anagrafica">
                            <div class="titolo_step">ANAGRAFICA PAZIENTE</div>
                            <table cellpadding="4" cellspacing="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DATI PAZIENTE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Data_Registrazione.value='';document.form1.Wizard1$Txt_Nome.value='';document.form1.Wizard1$Txt_Cognome.value='';document.form1.Wizard1$Txt_Indirizzo.value='';document.form1.Wizard1$Txt_Telefono.value='';document.form1.Wizard1$Txt_Codice_Fiscale.value='';document.form1.Wizard1$Txt_Eta.value='';document.form1.Wizard1$Txt_Altezza.value='';document.form1.Wizard1$Txt_Peso1.value='';document.form1.Wizard1$Txt_Circonferenza_Addominale.value='';document.getElementById('Wizard1_LbInfoMedico').innerHTML='';document.form1.Wizard1$Sesso.selectedIndex=0;document.form1.Wizard1$Medico.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data Registrazione *<br />(gg/mm/aaaa)</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Data_Registrazione" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Data_Registrazione.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Data_Registrazione" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Data_Registrazione', document.form1.Wizard1$Cld_Data_Registrazione.value, 'calendar1');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar1" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Nome *</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Nome.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire il nome del paziente</div></span>" ControlToValidate="Txt_Nome"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Nome" runat="server" MaxLength="255" Columns="30"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Cognome *</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Cognome.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire il cognome del paziente</div></span>" ControlToValidate="Txt_Cognome"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Cognome" runat="server" MaxLength="255" Columns="30"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Indirizzo
                                    </td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Indirizzo" runat="server" MaxLength="255" Columns="30"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        N� Telefono</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Telefono" runat="server" MaxLength="255" Columns="30"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Codice fiscale *</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Codice_Fiscale.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire il codice fiscale del paziente</div></span>" ControlToValidate="Txt_Codice_Fiscale"></asp:RequiredFieldValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Codice_Fiscale" runat="server" MaxLength="255" Columns="30"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Sesso</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Sesso" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>F</asp:ListItem>
                                            <asp:ListItem>M</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Et�</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\d*" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Eta.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire l'et� del paziente in un formato numerico</div></span>" ControlToValidate="Txt_Eta"></asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Eta" runat="server" MaxLength="3" Columns="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Altezza (cm)</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="\d*" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Altezza.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire l'altezza del paziente in un formato numerico</div></span>" ControlToValidate="Txt_Altezza"></asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Altezza" runat="server" MaxLength="3" Columns="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Peso (kg)</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                        <asp:CompareValidator id="cmpProductPrice" ControlToValidate="Txt_Peso1" Text="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Peso1.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire il peso in un formato corretto</div></span>" Operator="DataTypeCheck" Type="Double" Runat="server" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Peso1" runat="server" MaxLength="6" Columns="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Circ. Addominale (cm)</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                        <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ValidationExpression="\d*" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Circonferenza_Addominale.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire la circonferenza addominale del paziente in un formato numerico</div></span>" ControlToValidate="Txt_Circonferenza_Addominale"></asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Circonferenza_Addominale" runat="server" MaxLength="3" Columns="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Medico curante</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Medico" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Medico_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Data primo Controllo</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox runat="server" Columns="10" MaxLength="10" 
                                     ID="txtDataProxControllo"></asp:TextBox>

                                      
                                     <a href="javascript:show_calendar('document.form1.Wizard1$txtDataProxControllo', document.form1.Wizard1$txtDataProxControllo.value, 'calendar1pc');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar1pc" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align="right" valign="middle">
                                    </td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:Label ID="LbInfoMedico" runat="server" ForeColor="Black"></asp:Label>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep2" runat="server" Title="Prec. Scomp.">
                            <div class="titolo_step">PRECEDENTI ACCESSI P.S. PER SCOMPENSO 
                                <br>
                                 (Anno precedente al reclutamento)<br>
                            </div>
                            <table cellpadding="4" cellspacing="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">1� ACCESSO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Precedenti_Accessi_Scompenso.value='';document.form1.Wizard1$Txt_Diagnosi_Precedenti_Accessi_Scompenso.value='';document.form1.Wizard1$Txt_Terapia_Precedenti_Accessi_Scompenso.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Precedenti_Accessi_Scompenso" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Precedenti_Accessi_Scompenso.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Precedenti_Accessi_Scompenso" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Precedenti_Accessi_Scompenso', document.form1.Wizard1$Cld_Precedenti_Accessi_Scompenso.value, 'calendar2');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar2" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diagnosi</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Diagnosi_Precedenti_Accessi_Scompenso" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top" style="display:none;">
                                        Terapia</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  Visible="false" ID="Txt_Terapia_Precedenti_Accessi_Scompenso" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">2� ACCESSO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld2_Precedenti_Accessi_Scompenso.value='';document.form1.Wizard1$Txt2_Diagnosi_Precedenti_Accessi_Scompenso.value='';document.form1.Wizard1$Txt2_Terapia_Precedenti_Accessi_Scompenso.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator3" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld2_Precedenti_Accessi_Scompenso" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld2_Precedenti_Accessi_Scompenso.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld2_Precedenti_Accessi_Scompenso" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld2_Precedenti_Accessi_Scompenso', document.form1.Wizard1$Cld2_Precedenti_Accessi_Scompenso.value, 'calendar3');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar3" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diagnosi</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt2_Diagnosi_Precedenti_Accessi_Scompenso" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top"  style="display:none;">
                                        Terapia</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Visible="false" ID="Txt2_Terapia_Precedenti_Accessi_Scompenso" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">3� ACCESSO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld3_Precedenti_Accessi_Scompenso.value='';document.form1.Wizard1$Txt3_Diagnosi_Precedenti_Accessi_Scompenso.value='';document.form1.Wizard1$Txt3_Terapia_Precedenti_Accessi_Scompenso.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator4" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld3_Precedenti_Accessi_Scompenso" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld3_Precedenti_Accessi_Scompenso.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld3_Precedenti_Accessi_Scompenso" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld3_Precedenti_Accessi_Scompenso', document.form1.Wizard1$Cld3_Precedenti_Accessi_Scompenso.value, 'calendar4');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar4" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diagnosi</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt3_Diagnosi_Precedenti_Accessi_Scompenso" runat="server" MaxLength="255" Columns="40" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right"  style="display:none;" valign="top">
                                        Terapia</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Visible="false" ID="Txt3_Terapia_Precedenti_Accessi_Scompenso" runat="server" MaxLength="255" Columns="40" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep3" runat="server" Title="Prec. Osped.">
                            <div class="titolo_step">PRECEDENTI OSPEDALIZZAZIONI PER SCOMPENSO<br />
                              (Anno precedente al reclutamento)</div>
                            <table cellpadding="4" cellspacing="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">RICOVERO 1<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld1_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Cld_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Txt_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Txt_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Ddl_Classe_NYHA_Al_Ricovero.selectedIndex=0;document.form1.Wizard1$Ddl_Classe_NYHA_Alla_Dimissione.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data ricovero</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator5" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld1_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld1_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld1_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld1_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso', document.form1.Wizard1$Cld1_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.value, 'calendar5');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar5" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data dimissione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator6" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso', document.form1.Wizard1$Cld_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.value, 'calendar6');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar6" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diagnosi</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="display:none;" align="right" valign="top">
                                        Classe NYHA al ricovero</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList Visible="false" ID="Ddl_Classe_NYHA_Al_Ricovero" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="display:none;" align="right" valign="top">
                                        Terapia dimissione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Visible="false" ID="Txt_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso"
                                            runat="server" Height="76px" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="display:none;" align="right" valign="top">
                                        Classe NYHA alla dimissione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList Visible="false" ID="Ddl_Classe_NYHA_Alla_Dimissione" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">RICOVERO 2<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld2_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Cld2_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Txt2_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Txt2_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Ddl2_Classe_NYHA_Al_Ricovero.selectedIndex=0;document.form1.Wizard1$Ddl2_Classe_NYHA_Alla_Dimissione.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data ricovero</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator7" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld2_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld2_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld2_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld2_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso', document.form1.Wizard1$Cld2_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.value, 'calendar7');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar7" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data dimissione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator8" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld2_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld2_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld2_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld2_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso', document.form1.Wizard1$Cld2_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.value, 'calendar8');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar8" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diagnosi</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt2_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="display:none;" align="right" valign="top">
                                        Classe NYHA al ricovero</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList Visible="false" ID="Ddl2_Classe_NYHA_Al_Ricovero" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top"  style="display:none;">
                                        Terapia dimissione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Visible="false" ID="Txt2_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso"
                                            runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top"  style="display:none;">
                                        Classe NYHA alla dimissione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList Visible="false" ID="Ddl2_Classe_NYHA_Alla_Dimissione" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">RICOVERO 3<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld3_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Cld3_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Txt3_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Txt3_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.value='';document.form1.Wizard1$Ddl3_Classe_NYHA_Al_Ricovero.selectedIndex=0;document.form1.Wizard1$Ddl3_Classe_NYHA_Alla_Dimissione.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data ricovero</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator9" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld3_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld3_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld3_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld3_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso', document.form1.Wizard1$Cld3_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.value, 'calendar9');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar9" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data dimissione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator10" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld3_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld3_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld3_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld3_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso', document.form1.Wizard1$Cld3_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.value, 'calendar10');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar10" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diagnosi</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt3_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso" runat="server" MaxLength="255" Columns="40" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top"  style="display:none;">
                                        Classe NYHA al ricovero</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList Visible="false" ID="Ddl3_Classe_NYHA_Al_Ricovero" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top"  style="display:none;">
                                        Terapia dimissione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Visible="false" ID="Txt3_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso"
                                            runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top"  style="display:none;">
                                        Classe NYHA alla dimissione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList Visible="false" ID="Ddl3_Classe_NYHA_Alla_Dimissione" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                             <asp:WizardStep ID="WizardStep9" runat="server" Title="Anamnesi Aritmica" 
                            StepType="Step">
                            <div class="titolo_step">ARITMIE</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">EX. SOPRAVENTRICOL.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Aritmie_Ex_Sopraventricol.value='';document.form1.Wizard1$Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg[1].checked=true;document.form1.Wizard1$Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg_Holter[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Ex_Sopraventtricol_Terapia.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td style="display:none;" align="right" valign="top">
                                        Data primo riscontro</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator27" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Aritmie_Ex_Sopraventricol" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Ex_Sopraventricol.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Aritmie_Ex_Sopraventricol" runat="server" Columns="10" 
                                            MaxLength="10" Visible="False"></asp:TextBox>
                                        &nbsp;<div id="calendar27" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Document. ECG</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtDocECGSopraventricol" runat="server"></asp:TextBox>*/%>
                                        <asp:CheckBox runat="server" ID="txtDocECGSopraventricol_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Document. ECG Holter</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtECGHolterSopraventricol" runat="server"></asp:TextBox>*/%>
                                        <asp:CheckBox runat="server" ID="txtECGHolterSopraventricol_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td class="titolo_sezione" colspan="3">
                                        EX. VENTRICOL.<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Cld_Aritmie_Ex_Ventricol.value='';document.form1.Wizard1$Rbl_Aritmie_Ex_Ventricol_Document_Ecg[1].checked=true;document.form1.Wizard1$Rbl_Aritmie_Ex_Ventricol_Document_Ecg_Holter[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Ex_Ventricol_Semplici.value='';document.form1.Wizard1$Txt_Aritmie_Ex_Ventricol_Coppie_Triplette.value='';document.form1.Wizard1$Txt_Aritmie_Ex_Ventricol_Tv_Non_Sostenuta.value='';document.form1.Wizard1$Txt_Aritmie_Ex_Ventricol_Terapia.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Document. ECG</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtDocECGVentricol" runat="server"></asp:TextBox>*/%>
                                        <asp:CheckBox runat="server" ID="txtDocECGVentricol_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Document ECG Holter</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtECGVentricol" runat="server"></asp:TextBox>*/%>
                                        <asp:CheckBox runat="server" ID="txtECGVentricol_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Semplici</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox onblur="data_change(this)" ID="Txt_Aritmie_Ex_Ventricol_Semplici" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Coppie e triplette</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox onblur="data_change(this)" ID="Txt_Aritmie_Ex_Ventricol_Coppie_Triplette" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TV non sostenuta</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox onblur="data_change(this)" ID="Txt_Aritmie_Ex_Ventricol_Tv_Non_Sostenuta" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titolo_sezione" colspan="3">
                                        TACHICARDIA PAROSSISTICA SOPRAVENTRICOLARE<div class="div_reset">
                                           </div>
                                    </td>
                                </tr>
                                <tr>
                                 <td align="right" valign="middle">
                                        TP SV</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                    <table><tr><td>
                                         <asp:RadioButtonList ID="radioTPSV" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Si</asp:ListItem>
                                                            <asp:ListItem Selected="True" Value="1">No</asp:ListItem>
                                                        </asp:RadioButtonList></td><td> - Ablazione </td><td>  <asp:RadioButtonList ID="radioTPSVAblazione" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Si</asp:ListItem>
                                                            <asp:ListItem Selected="True" Value="1">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                        </td></tr></table>
                                    </td>
                                </tr>
                                <tr>
                                 <td align="right" valign="middle">
                                        Preeccitazione</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                     <table><tr><td>
                                         <asp:RadioButtonList ID="radioPreeccitazioneTachicardia" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Si</asp:ListItem>
                                                            <asp:ListItem Selected="True" Value="1">No</asp:ListItem>
                                                        </asp:RadioButtonList> </td><td>  - Ablazione</td><td>   <asp:RadioButtonList ID="radioPreeccitazioneTachicardiaAblazione" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Si</asp:ListItem>
                                                            <asp:ListItem Selected="True" Value="1">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                        </td></tr></table>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titolo_sezione" colspan="3">
                                        FA PAROSS.<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Cld_Aritmie_Fa_Paross.value='';document.form1.Wizard1$Txt_Aritmie_FA_Paradoss_Episodi.value='';document.form1.Wizard1$Txt_Aritmie_FA_Paradoss_Durata_Episodi.value='';document.form1.Wizard1$Txt_Aritmie_FA_Paross_terapia.value='';document.form1.Wizard1$Rbl_Aritmie_Fa_Paradoss_Efficacia[0].checked=false;document.form1.Wizard1$Rbl_Aritmie_Fa_Paradoss_Efficacia[1].checked=false;document.form1.Wizard1$Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat[0].checked=false;document.form1.Wizard1$Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat[1].checked=false;return false;">
                                            reset</a></div>
                                        &nbsp;O PERSISTENTE</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data inizio</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        <asp:CustomValidator ID="CustomValidator29" runat="server" 
                                            ClientValidationFunction="ClientValidate" 
                                            ControlToValidate="Cld_Aritmie_Fa_Paross" 
                                            ErrorMessage="&lt;span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Fa_Paross.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;&gt;&lt;img src='files/info.gif' /&gt;&lt;div&gt;Occorre inserire una data valida nel formato gg/mm/aaaa&lt;/div&gt;&lt;/span&gt;"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Aritmie_Fa_Paross" runat="server" Columns="10" 
                                            MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Fa_Paross', document.form1.Wizard1$Cld_Aritmie_Fa_Paross.value, 'calendar29');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar29" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        N. Episodi</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Aritmie_FA_Paradoss_Episodi" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Durata episodi</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_FA_Paradoss_Durata_Episodi" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ablazione transcat.</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <script>
                                        var count = 0;
                                        function disattivalo(obj) {
                                          
                                                    document.getElementById(obj).checked = false;
                                            
                                        }
                                    </script>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat" 
                                            runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="False" >Efficace</asp:ListItem>
                                            <asp:ListItem Selected="False">Non efficace</asp:ListItem>
                                        </asp:RadioButtonList>
                                        <a href='javascript:disattivalo("Wizard1_Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat_0");disattivalo("Wizard1_Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat_1")'>Disattiva Selezione</a>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titolo_sezione" colspan="3">
                                        FA PERMANENTE<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Cld_Aritmie_Fa_Permanente.value='';document.form1.Wizard1$Txt_Aritmie_Fa_Permanente_Terapia.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data inizio</td>
                                    <td align="left" valign="top" width="16px">
                                        <asp:CustomValidator ID="CustomValidator30" runat="server" 
                                            ClientValidationFunction="ClientValidate" 
                                            ControlToValidate="Cld_Aritmie_Fa_Permanente" 
                                            ErrorMessage="&lt;span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Fa_Permanente.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;&gt;&lt;img src='files/info.gif' /&gt;&lt;div&gt;Occorre inserire una data valida nel formato gg/mm/aaaa&lt;/div&gt;&lt;/span&gt;"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Aritmie_Fa_Permanente" runat="server" Columns="10" 
                                            MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Fa_Permanente', document.form1.Wizard1$Cld_Aritmie_Fa_Permanente.value, 'calendar30');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar30" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titolo_sezione" colspan="3">
                                        CVE<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Rbl_Aritmie_Cve[1].checked=true;return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CVE</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Aritmie_Cve" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList> */%>
                                        <asp:CheckBox runat="server" ID="Rbl_Aritmie_Cve_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Data Ultima CVE</td>
                                    <td>
                                        <asp:CustomValidator ID="CustomValidator41" runat="server" 
                                            ClientValidationFunction="ClientValidate" ControlToValidate="txtDataCVE" 
                                            ErrorMessage="&lt;span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$txtDataCVE.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;&gt;&lt;img src='files/info.gif' /&gt;&lt;div&gt;Occorre inserire una data valida nel formato gg/mm/aaaa&lt;/div&gt;&lt;/span&gt;"></asp:CustomValidator>
                                    </td>
                                    <td style="text-align:left;">
                                        <asp:TextBox ID="txtDataCVE" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$txtDataCVE', document.form1.Wizard1$Cld_Aritmie_Fa_Permanente.value, 'calendar30');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a></td>
                                </tr>
                                <tr>
                                    <td class="titolo_sezione" colspan="3">
                                        TV SOSTENUTA<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Rbl_Aritmie_Tv_Sostenuta[0].checked=false;document.form1.Wizard1$Rbl_Aritmie_Tv_Sostenuta[1].checked=false;document.form1.Wizard1$Cld_Aritmie_Tv_Sostenuta.value='';document.form1.Wizard1$Txt_Aritmie_Tv_Sostenuta_Num_Episodi.value='';document.form1.Wizard1$Txt_Aritmie_Tv_Sostenuta_Durata_Episodi.value='';document.form1.Wizard1$Txt_Aritmie_Tv_Sostenuta_Freq_Ventricol.value='';document.form1.Wizard1$Txt_Aritmie_Tv_Sostenuta_Sincope.value='';document.form1.Wizard1$Txt_Aritmie_Tv_Sostenuta_Lipotimia.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TV SOSTENUTA</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmie_Tv_Sostenuta" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Value="Sintomatica">Sintomatica</asp:ListItem>
                                            <asp:ListItem Value="Asintomatica">Asintomatica</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data prima osservazione</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        <asp:CustomValidator ID="CustomValidator31" runat="server" 
                                            ClientValidationFunction="ClientValidate" 
                                            ControlToValidate="Cld_Aritmie_Tv_Sostenuta" 
                                            ErrorMessage="&lt;span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Tv_Sostenuta.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;&gt;&lt;img src='files/info.gif' /&gt;&lt;div&gt;Occorre inserire una data valida nel formato gg/mm/aaaa&lt;/div&gt;&lt;/span&gt;"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Aritmie_Tv_Sostenuta" runat="server" Columns="10" 
                                            MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Tv_Sostenuta', document.form1.Wizard1$Cld_Aritmie_Tv_Sostenuta.value, 'calendar31');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar31" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        N. episodi</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Aritmie_Tv_Sostenuta_Num_Episodi" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Durata Max episodi</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Tv_Sostenuta_Durata_Episodi" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Freq. ventricolare</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Tv_Sostenuta_Freq_Ventricol" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titolo_sezione" colspan="3">
                                        MI RESUSCITATA<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Rbl_Aritmia_Mi_Resuscitata[1].checked=true;return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        MI RESUSCITATA</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Aritmia_Mi_Resuscitata" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Aritmia_Mi_Resuscitata_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titolo_sezione" colspan="3">
                                        PM<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Rbl_Aritmia_Pm[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Pm_Causale_Impianto.value='';document.form1.Wizard1$Cld_Aritmie_Pm.value='';document.form1.Wizard1$Txt_Aritmie_Pm_Tipologia.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PM</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Aritmia_Pm" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                         <asp:CheckBox runat="server" ID="Rbl_Aritmia_Pm_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Pm_Causale_Impianto" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data Impianto</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        <asp:CustomValidator ID="CustomValidator32" runat="server" 
                                            ClientValidationFunction="ClientValidate" ControlToValidate="Cld_Aritmie_Pm" 
                                            ErrorMessage="&lt;span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Pm.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;&gt;&lt;img src='files/info.gif' /&gt;&lt;div&gt;Occorre inserire una data valida nel formato gg/mm/aaaa&lt;/div&gt;&lt;/span&gt;"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Aritmie_Pm" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Pm', document.form1.Wizard1$Cld_Aritmie_Pm.value, 'calendar32');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar32" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Tipologia</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Pm_Tipologia" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titolo_sezione" colspan="3">
                                        ICD<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Rbl_Aritmia_Icd[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Causale_Impianto.value='';document.form1.Wizard1$Cld_Aritmie_Icd.value='';document.form1.Wizard1$Txt_Aritmie_Icd_Scariche_Appropriate.value='';document.form1.Wizard1$Txt_Aritmie_Icd_Scariche_Inappropriate.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        ICD</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Aritmia_Icd" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Aritmia_Icd_ch"  Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Causale_Impianto" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data impianto</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        <asp:CustomValidator ID="CustomValidator33" runat="server" 
                                            ClientValidationFunction="ClientValidate" ControlToValidate="Cld_Aritmie_Icd" 
                                            ErrorMessage="&lt;span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Icd.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;&gt;&lt;img src='files/info.gif' /&gt;&lt;div&gt;Occorre inserire una data valida nel formato gg/mm/aaaa&lt;/div&gt;&lt;/span&gt;"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Aritmie_Icd" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Icd', document.form1.Wizard1$Cld_Aritmie_Icd.value, 'calendar33');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar33" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Scariche appropriate</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox onblur="data_change(this)" ID="Txt_Aritmie_Icd_Scariche_Appropriate" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Scariche inappropriate</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox onblur="data_change(this)" ID="Txt_Aritmie_Icd_Scariche_Inappropriate" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td align="right" valign="middle">
                                        Ablazione</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Visible="false" ID="txtAblazione" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td align="right" valign="middle">
                                        Preeccitazione</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Visible="false" ID="txtPreeccitazione" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="titolo_sezione" colspan="3">
                                        RCT<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Txt_Aritmie_Pm_Biventr.value='';document.form1.Wizard1$Cld_Aritmie_Data_Impianto.value='';document.form1.Wizard1$Ddl_Aritmia_Pm_Biventr.selectedIndex=0;document.form1.Wizard1$Txt_Aritmie_FE_Prima.value='';document.form1.Wizard1$Txt_Aritmie_FE_A_6_Mesi.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Pm_Biventr" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data impianto</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        <asp:CustomValidator ID="CustomValidator34" runat="server" 
                                            ClientValidationFunction="ClientValidate" 
                                            ControlToValidate="Cld_Aritmie_Data_Impianto" 
                                            ErrorMessage="&lt;span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Data_Impianto.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;&gt;&lt;img src='files/info.gif' /&gt;&lt;div&gt;Occorre inserire una data valida nel formato gg/mm/aaaa&lt;/div&gt;&lt;/span&gt;"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Aritmie_Data_Impianto" runat="server" Columns="10" 
                                            MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Data_Impianto', document.form1.Wizard1$Cld_Aritmie_Data_Impianto.value, 'calendar34');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar34" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                               
                                </table>
                                </asp:WizardStep>
                              
                           
                        <asp:WizardStep ID="WizardStep4" runat="server" Title="Eziologie">
                            <div class="titolo_step">EZIOLOGIA</div>
                            <table cellpadding="4" cellspacing="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">IMA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Eziologia_IMA.value='';document.form1.Wizard1$Rbl_Eziologia_IMA_Documentazione_Ecg[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_IMA_Documentazione_Eco[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_IMA_Documentazione_Scintigrafia[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_IMA_Documentazione_Coronogr[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator11" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Eziologia_IMA" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Eziologia_IMA.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Eziologia_IMA" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Eziologia_IMA', document.form1.Wizard1$Cld_Eziologia_IMA.value, 'calendar11');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar11" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Documentaz. ECG</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_IMA_Documentazione_Ecg" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_IMA_Documentazione_Ecg_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Documentaz. ECO</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_IMA_Documentazione_Eco" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_IMA_Documentazione_Eco_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Documentaz. scintigrafia</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_IMA_Documentazione_Scintigrafia" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_IMA_Documentazione_Scintigrafia_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. Coronarogr.</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_IMA_Documentazione_Coronogr" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_IMA_Documentazione_Coronogr_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CARDIOP. ISCHEM. SENZA DOCUMENT. IMA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Eziologia_Cardiop_Ischem.value='';document.form1.Wizard1$Rbl_Eziologia_Cardiop_Document_ECG[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_Cardiop_Document_ECO[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_Cardiop_Document_Scintigrafia[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_Cardiop_Document_Coronarogr[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data primo rilievo</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator12" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Eziologia_Cardiop_Ischem" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Eziologia_Cardiop_Ischem.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Eziologia_Cardiop_Ischem" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Eziologia_Cardiop_Ischem', document.form1.Wizard1$Cld_Eziologia_Cardiop_Ischem.value, 'calendar12');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar12" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. ECG</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_Cardiop_Document_ECG" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_Cardiop_Document_ECG_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. ECO</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_Cardiop_Document_ECO" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_Cardiop_Document_ECO_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. scintigrafia</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_Cardiop_Document_Scintigrafia" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_Cardiop_Document_Scintigrafia_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. Coronarogr.</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_Cardiop_Document_Coronarogr" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_Cardiop_Document_Coronarogr_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CARDIOMIOPATIA IPERTROFICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Ddl_Eziologia_Cardiomiopatia_Ipertrofica_Ostruttiva.selectedIndex=0;document.form1.Wizard1$Cld_Eziologia_Cardiomiopatia_Ipertrofica.value='';document.form1.Wizard1$Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Ecg[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Eco[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Tipologia</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Eziologia_Cardiomiopatia_Ipertrofica_Ostruttiva" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Ostruttiva</asp:ListItem>
                                            <asp:ListItem>Non Ostruttiva</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data primo rilievo</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator13" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Eziologia_Cardiomiopatia_Ipertrofica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Eziologia_Cardiomiopatia_Ipertrofica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Eziologia_Cardiomiopatia_Ipertrofica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Eziologia_Cardiomiopatia_Ipertrofica', document.form1.Wizard1$Cld_Eziologia_Cardiomiopatia_Ipertrofica.value, 'calendar13');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar13" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. ECG</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Ecg" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Ecg_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. ECO</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Eco" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Eco_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CARDIOMIOPATIA DILATATIVA PRIMITIVA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Eziologia_Cardiomiopatia_Dilatativa.value='';document.form1.Wizard1$Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Ecg[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Eco[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data primo rilievo</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator14" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Eziologia_Cardiomiopatia_Dilatativa" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Eziologia_Cardiomiopatia_Dilatativa.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Eziologia_Cardiomiopatia_Dilatativa" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Eziologia_Cardiomiopatia_Dilatativa', document.form1.Wizard1$Cld_Eziologia_Cardiomiopatia_Dilatativa.value, 'calendar14');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar14" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. ECG</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                       <%/* <asp:RadioButtonList ID="Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Ecg" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Ecg_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. ECO</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Eco" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Eco_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">VALVOLOPATIE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Ddl_Eziologia_Valvolopatie_Tipo.selectedIndex=0;document.form1.Wizard1$Cld_Eziologia_Valvolopatie.value='';document.form1.Wizard1$Rbl_Eziologia_Valvolopatie_Document_Ecg[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_Valvolopatie_Document_Eco[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data primo rilievo</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator15" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Eziologia_Valvolopatie" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Eziologia_Valvolopatie.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Eziologia_Valvolopatie" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Eziologia_Valvolopatie', document.form1.Wizard1$Cld_Eziologia_Valvolopatie.value, 'calendar15');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar15" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Tipo valvolopatie</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Eziologia_Valvolopatie_Tipo" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. ECG</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_Valvolopatie_Document_Ecg" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_Valvolopatie_Document_Ecg_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. ECO</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Eziologia_Valvolopatie_Document_Eco" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Eziologia_Valvolopatie_Document_Eco_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                  <tr>
                                    <td colspan="3" class="titolo_sezione">CARDIOMIOPATIA IPERTENSIVA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Altre_Eziologie_Descrizione.value='';document.form1.Wizard1$Txt_Altre_Eziologie_Denominazione.value='';document.form1.Wizard1$Cld_Eziologia_Altre_Eziologie.value='';document.form1.Wizard1$Rbl_Eziologia_Altre_Eziologie_Document_Ecg[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_Altre_Eziologie_Document_Eco[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Cardiomiopatia Ipertensiva</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox runat="server" ID="chb_cardiomiopatia" Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                       Data Inizio</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="DataCardioMiopatia" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$DataCardioMiopatia', document.form1.Wizard1$DataCardioMiopatia.value, 'calendar70');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar70" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ALTRE EZIOLOGIE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Altre_Eziologie_Descrizione.value='';document.form1.Wizard1$Txt_Altre_Eziologie_Denominazione.value='';document.form1.Wizard1$Cld_Eziologia_Altre_Eziologie.value='';document.form1.Wizard1$Rbl_Eziologia_Altre_Eziologie_Document_Ecg[1].checked=true;document.form1.Wizard1$Rbl_Eziologia_Altre_Eziologie_Document_Eco[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Descrizione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Altre_Eziologie_Descrizione" runat="server" MaxLength="255"
                                            TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="display:none;" align="right" valign="top">
                                        Denominazione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Visible="false" ID="Txt_Altre_Eziologie_Denominazione" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data primo rilievo</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator16" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Eziologia_Altre_Eziologie" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Eziologia_Altre_Eziologie.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Eziologia_Altre_Eziologie" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Eziologia_Altre_Eziologie', document.form1.Wizard1$Cld_Eziologia_Altre_Eziologie.value, 'calendar16');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar16" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="display:none;" align="right" valign="top">
                                        Document. ECG</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList Visible="false" ID="Rbl_Eziologia_Altre_Eziologie_Document_Ecg" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Eziologia_Altre_Eziologie_Document_Ecg_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td  style="display:none;" align="right" valign="top">
                                        Document. ECO</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList Visible="false" ID="Rbl_Eziologia_Altre_Eziologie_Document_Eco" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Eziologia_Altre_Eziologie_Document_Eco_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="Step5" runat="server" Title="Fatt. Rischio">
                            <div class="titolo_step">FATTORI DI RISCHIO E COMORBIDIT�</div>
                            <table cellpadding="4" cellspacing="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">FUMO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Fumo.value='';document.form1.Wizard1$Ddl_Fattori_Rischio_Fumo_Grado.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Anni</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                       <%-- <asp:CustomValidator ID="CustomValidator17" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Fattori_Rischio_Comorbidita_Fumo" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Fattori_Rischio_Comorbidita_Fumo.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>--%>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Fattori_Rischio_Comorbidita_Fumo" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a style="display:none;" href="javascript:show_calendar('document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Fumo', document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Fumo.value, 'calendar17');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar17" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Grado</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Fattori_Rischio_Fumo_Grado" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Ex-Fumo</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                       <asp:CheckBox runat="server" ID="chbExFumo" Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DIABETE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Diabete1.value='';document.form1.Wizard1$Txt_diabete1_grado.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr><td><b>Diabete 1</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Anni</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <%--<asp:CustomValidator ID="CustomValidator18" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Fattori_Rischio_Comorbidita_Diabete1" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Fattori_Rischio_Comorbidita_Diabete1.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>--%>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  ID="Cld_Fattori_Rischio_Comorbidita_Diabete1" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a style="display:none;" href="javascript:show_calendar('document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Diabete1', document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Diabete1.value, 'calendar18');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar18" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Glicemia Attuale</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_diabete1_grado" runat="server" MaxLength="255" Columns="40"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr><td><b>Diabete 2</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                       Anni</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                       <%-- <asp:CustomValidator ID="CustomValidator19" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Fattori_Rischio_Comorbidita_Diabete2" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Fattori_Rischio_Comorbidita_Diabete2.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>--%>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Fattori_Rischio_Comorbidita_Diabete2" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a style="display:none;" href="javascript:show_calendar('document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Diabete2', document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Diabete2.value, 'calendar19');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar19" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Glicemia Attuale</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_diabete2_grado" runat="server" MaxLength="255" Columns="40"></asp:TextBox>
                                    </td>
                                </tr>
                              
                                <tr>
                                    <td colspan="2" align="left" valign="top">
                                        Terapie</td>
                                    
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Terapie_Praticate_Diabete" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">IPERT. ARTER.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Ipert_Arter.value='';document.form1.Wizard1$Ddl_Ipert_Art_Tipologia.selectedIndex=0;document.form1.Wizard1$Txt_Terapie_Praticate_Ipert_Art.value='';document.form1.Wizard1$Rbl_Ipert_Art_Normalizzazione[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Anni</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <%--<asp:CustomValidator ID="CustomValidator20" runat="server" ClientValidationFunction="ClientValidate"
                                                ControlToValidate="Cld_Fattori_Rischio_Comorbidita_Ipert_Arter" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Fattori_Rischio_Comorbidita_Ipert_Arter.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>--%>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Cld_Fattori_Rischio_Comorbidita_Ipert_Arter" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a style="display:none;" href="javascript:show_calendar('document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Ipert_Arter', document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Ipert_Arter.value, 'calendar20');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar20" style="display: none"></div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Tipologia</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Ipert_Art_Tipologia" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Sist.</asp:ListItem>
                                            <asp:ListItem>Diast.</asp:ListItem>
                                            <asp:ListItem>Sist.-Diast.</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Terapie praticate</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Terapie_Praticate_Ipert_Art" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Controllata</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Ipert_Art_Normalizzazione" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="Si">Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Ipert_Art_Normalizzazione" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">IPERDISLIPIDEMIA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Iperdislipidemia_LDL_Superiore_130[1].checked=true;document.form1.Wizard1$Rbl_Iperdislipidemia_HDL_Inferiore_35[1].checked=true;document.form1.Wizard1$Rbl_Iperdislipidemia_Trigl_Superiore_170[1].checked=true;document.form1.Wizard1$Rbl_Iperdislipidemia_Statine[1].checked=true;document.form1.Wizard1$Txt_Iperdislipidemia_Tipo_Farmaco.value='';document.form1.Wizard1$Rbl_Iperdislipidemia_LDL_Dosi[0].checked=false;document.form1.Wizard1$Rbl_Iperdislipidemia_LDL_Dosi[1].checked=false;document.form1.Wizard1$Rbl_Iperdislipidemia_LDL_Dosi[2].checked=false;return false;">
                                        reset</a></div></td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="top">
                                        Dosi</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Iperdislipidemia_LDL_Dosi" runat="server">
                                            <asp:ListItem Value="3">LDL &gt;= 130 mg/dl</asp:ListItem>
                                            <asp:ListItem Value="1">100 mg/dl &lt; LDL &lt; 130 mg/dl</asp:ListItem>
                                            <asp:ListItem Value="2">LDL &lt;  100 mg/dl</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="display:none;" align="right" valign="top">
                                        LDL &gt;= 130 mg/dl</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList Visible="false" ID="Rbl_Iperdislipidemia_LDL_Superiore_130" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Iperdislipidemia_LDL_Superiore_130_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        HDL</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Iperdislipidemia_HDL_Inferiore_35" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>&nbsp;&nbsp;(Uomo: &lt; 40 mg/dl &nbsp;&nbsp;&nbsp;&nbsp; Donna: &lt; 50 mg/dl)
                                    </td>
                                  
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        TRIGL &gt; 170 mg/dl</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Iperdislipidemia_Trigl_Superiore_170" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Iperdislipidemia_Trigl_Superiore_170_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Statine</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Iperdislipidemia_Statine" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/%>
                                        <asp:CheckBox runat="server" ID="Rbl_Iperdislipidemia_Statine_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Tipo farmaco</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Iperdislipidemia_Tipo_Farmaco" runat="server" MaxLength="255" Columns="40"></asp:TextBox>
                                    </td>
                                </tr>
                                    <tr>
                                    <td colspan="3" class="titolo_sezione">BPCO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Fumo.value='';document.form1.Wizard1$Ddl_Fattori_Rischio_Fumo_Grado.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                  
                               <td align="right" valign="top">
                                        BPCO</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_rpco" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                       
                                    </td>
                                    </tr>
                                    <tr>
                                    <td colspan="3" class="titolo_sezione">Anemia<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Fattori_Rischio_Comorbidita_Fumo.value='';document.form1.Wizard1$Ddl_Fattori_Rischio_Fumo_Grado.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                    <tr>
                                   <td align="right" valign="top">
                                        Anemia</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList Visible="false" ID="Rbl_Anemia" AutoPostBack="true" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                      
                                       <asp:CheckBox runat="server" ID="chbHbAnemia" Checked="false" Visible="true" Text="Hb < 12 g/dl" />
                                    </td>
                                    </tr>
                                     <tr>
                                    <td colspan="3" class="titolo_sezione">INSUFFICIENZA RENALE CRONICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Quadro_Clinico_Insufficienza_Renale[0].checked=false;document.form1.Wizard1$Rbl_Quadro_Clinico_Insufficienza_Renale[1].checked=false;document.form1.Wizard1$Rbl_Quadro_Clinico_Insufficienza_Renale[2].checked=false;document.form1.Wizard1$Cld_Quadro_Clinico_Data_Insufficienza_Renale.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td valign="top" align="right">
                                        </td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Quadro_Clinico_Insufficienza_Renale" runat="server" RepeatDirection="Vertical">
                                            <asp:ListItem Value="0" Selected="True">Nessuna</asp:ListItem>
                                            <asp:ListItem Value="1">1,4 mg/dl &lt; Creatininemia &lt;= 2,5 mg/dl</asp:ListItem>
                                            <asp:ListItem Value="2">Creatininemia Superiore a  2,5 mg/dl</asp:ListItem>
                                           
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                          <asp:WizardStep ID="WizardStep7" runat="server" Title="Es. Strum.">
                            <div class="titolo_step">ESAMI STRUMENTALI</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ECG<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Esami_Strumentali_Ritmo_Sinusale[1].checked=true;document.form1.Wizard1$Txt_Esami_Strumentali_Ecg_Ingresso_Frequenza_Cardiaca.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Ex_Sopraventricol[1].checked=true;document.form1.Wizard1$Ddl_Esami_Strumentali_Ecg_Ingresso_Ex_Ventricol.selectedIndex=0;document.form1.Wizard1$Rbl_Esami_Strumentali_Tv[0].checked=false;document.form1.Wizard1$Rbl_Esami_Strumentali_Tv[1].checked=false;document.form1.Wizard1$Rbl_Esami_Strumentali_Fa[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Danno_Atriale[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Bbs[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Bbd[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Eas[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Eps[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Sovraccarico_Sistolico[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Sovraccarico_Volume[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ischemia[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ritmo sinusale</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ritmo_Sinusale" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Ritmo_Sinusale_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="middle">
                                        FA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Fa" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Fa_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ritmo elettroindotto</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                     
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Ritmo_ElettroIndotto_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Frequenza cardiaca/min</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Esami_Strumentali_Ecg_Ingresso_Frequenza_Cardiaca" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Preeccitazione</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="radioPreeccitazioneECG" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="middle">
                                        TPSV</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="radioTPSVECG" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td align="right" valign="middle">
                                        BAV</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_BAV" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>I</asp:ListItem>
                                            <asp:ListItem>II</asp:ListItem>
                                            <asp:ListItem>III</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ex. sopraventricol</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ex_Sopraventricol" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                         <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Ex_Sopraventricol_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ex. Ventricol.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Ecg_Ingresso_Ex_Ventricol" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>1a</asp:ListItem>
                                            <asp:ListItem>1b</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4a</asp:ListItem>
                                            <asp:ListItem>4b</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TV</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Tv" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Sostenuta</asp:ListItem>
                                            <asp:ListItem>Non sostenuta</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                               
                                <tr>
                                    <td align="right" valign="middle">
                                        Danno atriale sx</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Danno_Atriale" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                        */ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Danno_Atriale_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Bbs</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Bbs" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList> */ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Bbs_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Bbd</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Bbd" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Bbd_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Eas</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <% /*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Eas" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Eas_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Eps</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Eps" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Eps_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ipertrofia ventricolare sx</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Pregresso IMA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="radioPregressoIMA" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td align="right" valign="middle">
                                        Sovraccarico sistolico</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Sovraccarico_Sistolico" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Esami_Strumentali_Sovraccarico_Sistolico_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Sovraccarico di volume</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Sovraccarico_Volume" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Sovraccarico_Volume_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ischemia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ischemia" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Ischemia_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">RX TORACE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Ddl_Esami_Strumentali_Rx_Torace.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Killip</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Rx_Torace" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ECO 2D<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Esami_Strumentali_Eco2d_Ipertrofia_Settale.value='';document.form1.Wizard1$Txt_Esami_Strumentali_Eco2d_Ipertrofia_Concentrica.value='';document.form1.Wizard1$Ddl_Esami_Strumentali_Eco2d_Ipocinesia.selectedIndex=0;document.form1.Wizard1$Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare.value='';document.form1.Wizard1$Txt_Esami_Strumentali_Eco2d_Volume_Diastolico.value='';document.form1.Wizard1$Txt_Esami_Strumentali_Eco2d_Fe.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Disincronizzazione[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Ipertrofia settale mm.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Esami_Strumentali_Eco2d_Ipertrofia_Settale" runat="server" 
                                            MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Ipertrofia concentrica mm.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Esami_Strumentali_Eco2d_Ipertrofia_Concentrica" 
                                            runat="server" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ipocinesia - Acinesia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Eco2d_Ipocinesia" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Anteriore</asp:ListItem>
                                            <asp:ListItem>Inferiore</asp:ListItem>
                                            <asp:ListItem>Settale</asp:ListItem>
                                            <asp:ListItem>Apicale</asp:ListItem>
                                             <asp:ListItem>Globale</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diametro sistolico ventricolare SX mm.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ValidationExpression="\d*" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire il diametro sistolico del paziente in un formato numerico</div></span>" ControlToValidate="Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare"></asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare" 
                                            runat="server" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diametro diastolico mm.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Esami_Strumentali_Eco2d_Volume_Diastolico" runat="server" 
                                            MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Fe %</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                     <asp:CompareValidator id="CompareValidator1" ControlToValidate="Txt_Esami_Strumentali_Eco2d_Fe" Text="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Esami_Strumentali_Eco2d_Fe.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire il valore di FE in formato numerico</div></span>" Operator="DataTypeCheck" Type="Double" Runat="server" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)"  ID="Txt_Esami_Strumentali_Eco2d_Fe" runat="server" MaxLength="255" 
                                            Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diametro Atriale Sx</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                  
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)"  ID="txtDiametroAtrialeSX" runat="server" MaxLength="255" 
                                            Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Desincronizzazione</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Disincronizzazione" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="middle">
                                        Diametro Atriale Dx</td>
                                           <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left" valign="middle">
                                    <asp:TextBox onblur="data_change(this)"  ID="txtDiametroAtrialeDX" runat="server" MaxLength="255" 
                                            Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                    
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Diametro Ventricolare Dx</td>
                                           <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left" valign="middle">
                                    <asp:TextBox onblur="data_change(this)"  ID="txtDiametroVentricolareDX" runat="server" MaxLength="255" 
                                            Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                   
                                </tr>
                                 <tr>
                                    <td align="right" valign="middle">
                                        Funzione Ventricolo Dx</td>
                                           <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left" valign="middle">
                                    <asp:TextBox ID="txtFunzVentricoloDX" runat="server" MaxLength="255" 
                                            Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                   
                                </tr>
                                 <tr>
                                    <td align="right" valign="middle">
                                       Prossimo Controllo</td>
                                           <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left" valign="middle">
                                    <asp:TextBox ID="Cld_Data_ProssimoControllo" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Data_ProssimoControllo', document.form1.Wizard1$Cld_Data_ProssimoControllo.value, 'calendar1005');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar1005" style="display: none">
                                        </div>
                                    </td>
                                   
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ECO DOPPLER<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Esami_Strumentali_Eco_Doppler_Descrizione.value='';document.form1.Wizard1$Ddl_Esami_Strumentali_Eco_Doppler_Disfunzione_Diastolica.selectedIndex=0;document.form1.Wizard1$Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Mitralico.selectedIndex=0;document.form1.Wizard1$Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Aortico.selectedIndex=0;document.form1.Wizard1$Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Tricuspidale.selectedIndex=0;document.form1.Wizard1$Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Disfunzione diastolica</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Eco_Doppler_Disfunzione_Diastolica" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Normale</asp:ListItem>
                                            <asp:ListItem>Alterato rilasciamento</asp:ListItem>
                                            <asp:ListItem>Pseudonormalizzazione</asp:ListItem>
                                            <asp:ListItem>Restrittivo reversibile</asp:ListItem>
                                            <asp:ListItem>Restrittivo irreversibile</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Rigurgito mitralico</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Mitralico" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Rigurgito aortico</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Aortico" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Rigurgito tricuspidale</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Tricuspidale" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Pressione polmonare</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    <asp:CompareValidator id="CompareValidator2" ControlToValidate="Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare" Text="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire la pressione polmonare in un formato corretto</div></span>" Operator="DataTypeCheck" Type="Double" Runat="server" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Stenosi aortica gradiente mmHg</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStenosiAortica" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="middle">
                                        Stenosi mitralica area cm2</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtStenosiMitralica" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DOPPLER ARTERIOSO ARTI INFERIORI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Esami_Strumentali_Doppler_Arterioso_Arti_Inferiori.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Stenosi significative</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="3" class="titolo_sezione">DOPPLER VASI SOPRAORTICI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Esami_Strumentali_Doppler_Arterioso_Arti_Inferiori.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Stenosi significative</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative_chvasi" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ECG HOLTER<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale[1].checked=true;document.form1.Wizard1$Txt_Esami_Strumentali_Ecg_Holter_Frequenza_Cardiaca.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr[1].checked=true;document.form1.Wizard1$Ddl_Esami_Strumentali_Ecg_Holter_Ex_Ventricol.selectedIndex=0;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Tv[0].checked=false;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Tv[1].checked=false;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Fa[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Bbs[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Bbd[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Eas[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Eps[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Ischemia[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ritmo sinusale</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Frequenza cardiaca/min</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Esami_Strumentali_Ecg_Holter_Frequenza_Cardiaca" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="middle">
                                        Frequenza cardiaca/max</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Esami_Strumentali_Ecg_Holter_Frequenza_CardiacaMAX" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="middle">
                                        BAV</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_BAVHolter" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>I</asp:ListItem>
                                            <asp:ListItem>II</asp:ListItem>
                                            <asp:ListItem>III</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ex. sopraventricol</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ex. Ventricol.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Ecg_Holter_Ex_Ventricol" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>1a</asp:ListItem>
                                            <asp:ListItem>1b</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4a</asp:ListItem>
                                            <asp:ListItem>4b</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TV</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Tv" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Sostenuta</asp:ListItem>
                                            <asp:ListItem>Non sostenuta</asp:ListItem>
                                        </asp:RadioButtonList>
                                       <%-- <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Tv_ch" Text=""
                                         Checked="false" />--%>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        FA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Fa" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Fa_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td align="right" valign="middle">
                                        Danno atriale sx</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr  style="display:none;">
                                    <td align="right" valign="middle">
                                        Bbs</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Bbs" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Bbs_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr  style="display:none;">
                                    <td align="right" valign="middle">
                                        Bbd</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Bbd" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Bbd_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr  style="display:none;">
                                    <td align="right" valign="middle">
                                        Eas</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                       <%/* <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Eas" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Eas_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr  style="display:none;">
                                    <td align="right" valign="middle">
                                        Eps</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Eps" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Eps_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr  style="display:none;">
                                    <td align="right" valign="middle">
                                        Ipertrofia ventricolare sx</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr  style="display:none;">
                                    <td align="right" valign="middle">
                                        Sovraccarico sistolico</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr  style="display:none;">
                                    <td align="right" valign="middle">
                                        Sovraccarico di volume</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox Visible="false" runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ischemia Transitoria</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Ischemia" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Ecg_Holter_Ischemia_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>  <td colspan="3" class="titolo_sezione">6MWT</td></tr>
                                <tr><td align="right" valign="middle">
                                        Ec. m.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                 <td align="left">
                                        <asp:TextBox ID="txtGMWTEc" 
                                            runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                    </tr>
                                      <tr>  <td colspan="3" class="titolo_sezione">EMOGASANALISI</td></tr>
                                <tr><td align="right" valign="middle">
                                        pO <sub>2</sub></td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                 <td align="left">
                                        <asp:TextBox ID="txtPO2" 
                                            runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                    </tr>
                                       <tr><td align="right" valign="middle">
                                           pCO <sub>2</sub></td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                 <td align="left">
                                        <asp:TextBox ID="txtPCD2" 
                                            runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                    </tr>
                            </table>
                        </asp:WizardStep>
                           <asp:WizardStep ID="WizardStep5" runat="server" Title="Lab.">
                            <div class="titolo_step">LABORATORIO</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DATI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Laboratorio_Bnp.value='';document.form1.Wizard1$Txt_Laboratorio_Troponina.value='';document.form1.Wizard1$Txt_Laboratorio_Pcr.value='';document.form1.Wizard1$Txt_Laboratorio_Uricemia.value='';document.form1.Wizard1$Txt_Laboratorio_Azot.value='';document.form1.Wizard1$Txt_Laboratorio_Creat.value='';document.form1.Wizard1$Txt_Laboratorio_Microalbumin.value='';document.form1.Wizard1$Txt_Laboratorio_Na.value='';document.form1.Wizard1$Txt_Laboratorio_K.value='';document.form1.Wizard1$Txt_Laboratorio_Ca.value='';document.form1.Wizard1$Txt_Laboratorio_Ph.value='';document.form1.Wizard1$Txt_Laboratorio_Mg.value='';document.form1.Wizard1$Txt_Laboratorio_Got.value='';document.form1.Wizard1$Txt_Laboratorio_Gpt.value='';document.form1.Wizard1$Txt_Laboratorio_Cpk.value='';document.form1.Wizard1$Txt_Laboratorio_Gr.value='';document.form1.Wizard1$Txt_Laboratorio_Emogl.value='';document.form1.Wizard1$Txt_Laboratorio_Gb.value='';document.form1.Wizard1$Txt_Laboratorio_Sideremia.value='';document.form1.Wizard1$Txt_Laboratorio_Glicemia.value='';document.form1.Wizard1$Txt_Laboratorio_Emogl_Glic.value='';document.form1.Wizard1$Txt_Laboratorio_Col_Tot.value='';document.form1.Wizard1$Txt_Laboratorio_Ldl.value='';document.form1.Wizard1$Txt_Laboratorio_Hdl.value='';document.form1.Wizard1$Txt_Laboratorio_Triglic.value='';document.form1.Wizard1$Txt_Laboratorio_T3.value='';document.form1.Wizard1$Txt_Laboratorio_T4.value='';document.form1.Wizard1$Txt_Laboratorio_TSH.value='';document.form1.Wizard1$Txt_Laboratorio_Digitalemia.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PRO BNP</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Bnp" runat="server" MaxLength="255"></asp:TextBox> 
                                        PG/DL
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TROPONINA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Troponina" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PCR HS</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Pcr" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        URICEMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Uricemia" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        AZOT.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Azot" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CREAT.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Creat" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        ALBUMINURIA</td>
                                    <td align="left" onblur="data_change(this)" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Microalbumin" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        NA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Na" runat="server" MaxLength="255"></asp:TextBox> 
                                        mEq/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        K</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_K" runat="server" MaxLength="255"></asp:TextBox> 
                                        mEq/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Ca" runat="server" MaxLength="255"></asp:TextBox> 
                                        mEq/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PH</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Ph" runat="server" MaxLength="255"></asp:TextBox> 
                                        mEq/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        MG</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Mg" runat="server" MaxLength="255"></asp:TextBox> 
                                        mEq/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GOT</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Got" runat="server" MaxLength="255"></asp:TextBox> 
                                        IU/L
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GPT</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Gpt" runat="server" MaxLength="255"></asp:TextBox> 
                                        IU/L
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CPK MB</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Cpk" runat="server" MaxLength="255"></asp:TextBox> 
                                        IU/L
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GR</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Gr" runat="server" MaxLength="255"></asp:TextBox> 
                                        M/&#956;l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        EMOGL.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Emogl" runat="server" MaxLength="255"></asp:TextBox> 
                                        gr./dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GB</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Gb" runat="server" MaxLength="255"></asp:TextBox> 
                                        K/&#956;L
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        SIDEREMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Sideremia" runat="server" MaxLength="255"></asp:TextBox> 
                                        ng/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GLICEMIA</td>
                                    <td align="left"  width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Glicemia" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/dl
                                    </td>
                                </tr>
                                   <tr>
                                    <td align="right" valign="middle">
                                        PIASTRINE</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_piastrine" runat="server" MaxLength="255"></asp:TextBox> 
                                        10<sup>9</sup>/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        FERRITINA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_ferritina" runat="server" MaxLength="255"></asp:TextBox> 
                                        &#956;g/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        EMOGL. GLIC.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Emogl_Glic" runat="server" MaxLength="255"></asp:TextBox> 
                                        %
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        COL. TOT.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Col_Tot" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        LDL</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ValidationExpression="\d*" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Laboratorio_Ldl.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire un valore numerico per il campo LDL</div></span>" ControlToValidate="Txt_Laboratorio_Ldl"></asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Ldl" runat="server" MaxLength="255"></asp:TextBox> 
                                        
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        HDL</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Hdl" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TRIGLIC.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Triglic" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        F T3</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_T3" runat="server" MaxLength="255"></asp:TextBox> 
                                        pg/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        F T4</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_T4" runat="server" MaxLength="255"></asp:TextBox> 
                                        mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TSH</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_TSH" runat="server" MaxLength="255"></asp:TextBox> 
                                        &#956;u/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        DIGITALEMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Digitalemia" runat="server" MaxLength="255"></asp:TextBox> 
                                        ng/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PTH</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="txtATH" runat="server" MaxLength="255"></asp:TextBox> pg/ml
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="middle">
                                        ADH</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_laboratorio_adh" runat="server" MaxLength="255"></asp:TextBox> 
                                        pg/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Albumina</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_laboratorio_albumina" runat="server" MaxLength="255"></asp:TextBox> 
                                        g/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Aldosterone</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_laboratorio_aldosterone" runat="server" MaxLength="255"></asp:TextBox>
                                        ng/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Vit.D</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_VitD" runat="server" MaxLength="255"></asp:TextBox> 
                                        pg/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Epinefrina</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Laboratorio_Epinefrina" runat="server" MaxLength="255"></asp:TextBox> 
                                        nmol/d
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep6" runat="server" Title="Consulenze">
                            <div class="titolo_step">CONSULENZE<br />
                                (durante il periodo dello studio)</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">NEFROLOGICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Consulenze_Nefrologica[1].checked=true;document.form1.Wizard1$Cld_Consulenze_Nefrologica.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        NEFROLOGICA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtNefrologica" runat="server"></asp:TextBox>*/%>
                                        <asp:CheckBox runat="server" ID="txtNefrologica_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator35" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Consulenze_Nefrologica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Consulenze_Nefrologica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Consulenze_Nefrologica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Consulenze_Nefrologica', document.form1.Wizard1$Cld_Consulenze_Nefrologica.value, 'calendar35');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar35" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">EMATOLOGICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Consulenze_Ematologica[1].checked=true;document.form1.Wizard1$Cld_Consulenze_Ematologica.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        EMATOLOGICA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtEmatologica" runat="server"></asp:TextBox>*/%>
                                        <asp:CheckBox runat="server" ID="txtEmatologica_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator36" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Consulenze_Ematologica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Consulenze_Ematologica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Consulenze_Ematologica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Consulenze_Ematologica', document.form1.Wizard1$Cld_Consulenze_Ematologica.value, 'calendar36');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar36" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">NEUROPSICHIATRICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Consulenze_Neuropsichiatra[1].checked=true;document.form1.Wizard1$Cld_Consulenze_Neuropsichiatrica.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        NEUROPSICHIATRICA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtNeuroPsichi" runat="server"></asp:TextBox>*/%>
                                        <asp:CheckBox runat="server" ID="txtNeuroPsichi_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator37" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Consulenze_Neuropsichiatrica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Consulenze_Neuropsichiatrica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Consulenze_Neuropsichiatrica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Consulenze_Neuropsichiatrica', document.form1.Wizard1$Cld_Consulenze_Neuropsichiatrica.value, 'calendar37');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar37" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ENDOCRINOLOGICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Consulenze_Endocrinologica[1].checked=true;document.form1.Wizard1$Cld_Consulenze_Endocrinologica.value='';;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        ENDOCRINOLOGICA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtEndocrinologica" runat="server"></asp:TextBox>*/%>
                                        <asp:CheckBox runat="server" ID="txtEndocrinologica_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator38" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Consulenze_Endocrinologica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Consulenze_Endocrinologica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Consulenze_Endocrinologica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Consulenze_Endocrinologica', document.form1.Wizard1$Cld_Consulenze_Endocrinologica.value, 'calendar38');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar38" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">PNEUMOLOGICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Consulenze_Pneumologica[1].checked=true;document.form1.Wizard1$Cld_Consulenze_Pneumologica.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PNEUMOLOGICA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPneumologica" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator39" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Consulenze_Pneumologica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Consulenze_Pneumologica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Consulenze_Pneumologica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Consulenze_Pneumologica', document.form1.Wizard1$Cld_Consulenze_Pneumologica.value, 'calendar39');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar39" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep8" runat="server" Title="Terapie" 
                            StepType="Finish">
                            <div class="titolo_step">TERAPIE</div>
                            <table cellspacing="4" cellpadding="4">
                              
                                <tr runat="server" id="trEsaStrum1" visible="false">
                                    <td align="right" valign="middle">
                                        &nbsp;</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA" 
                                            runat="server" Visible="False">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr runat="server" id="trEsaStrum2" visible="false">
                                    <td align="right" valign="middle">
                                        &nbsp;</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                        &nbsp;</td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Peso1" runat="server" MaxLength="6" 
                                            Columns="10" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                
                                
                                <tr>
                                    <td align="center" valign="top" colspan="3">
                                    
                                             <asp:GridView ID="gv_farmaci" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="gv_farmaci_RowDataBound">
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" VerticalAlign="Bottom" />
                                            <AlternatingRowStyle BackColor="White" VerticalAlign="Bottom" />
                                            <EditRowStyle BackColor="#2461BF" VerticalAlign="Bottom" />
                                            <Columns>
                                                <asp:BoundField  ItemStyle-VerticalAlign="Bottom" DataField="IdFarmaco" 
                                                    Visible="False" >
<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField  ItemStyle-VerticalAlign="Bottom" DataField="NomeFarmaco" 
                                                    HeaderText="Farmaco" >
<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField  ItemStyle-VerticalAlign="Bottom" DataField="Valore" 
                                                    Visible="True" >
<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField  ItemStyle-VerticalAlign="Bottom" DataField="Intolleranza" 
                                                    Visible="True" >
<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField  ItemStyle-VerticalAlign="Bottom" DataField="CostoFarmaco" 
                                                    Visible="True" >
<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField ItemStyle-VerticalAlign="Bottom" HeaderText="Valore">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Columns="20" onblur="data_change(this)" ></asp:TextBox>
                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Si</asp:ListItem>
                                                            <asp:ListItem Selected="True" Value="1">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        
                                                    </ItemTemplate>

<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Intolleranza"  ItemStyle-VerticalAlign="Bottom">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                                    </ItemTemplate>

<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Costo Farmaco"  ItemStyle-VerticalAlign="Bottom">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox2" onblur="data_change(this)" runat="server" Columns="15" ></asp:TextBox>
                                                    </ItemTemplate>

<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle BackColor="#EFF3FB" VerticalAlign="Bottom" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                               <tr >
                                    <td align="right" valign="top">
                                        Altro</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNoteTerapie" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trCounc" visible="false">
                                    <td align="right" valign="middle">
                                        Counceling</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                       <%/* <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Counceling" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Esami_Strumentali_Counceling_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trPM" visible="false">
                                    <td class="titolo_sezione" colspan="3">
                                        PM<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Rbl_Aritmia_Pm[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Pm_Causale_Impianto.value='';document.form1.Wizard1$Cld_Aritmie_Pm.value='';document.form1.Wizard1$Txt_Aritmie_Pm_Tipologia.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trPM2" visible="false">
                                    <td align="right" valign="middle">
                                        PM</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Aritmia_Pm" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                         <asp:CheckBox runat="server" ID="Terapia_ch_PM" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trCausale3" visible="false">
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_Causale" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trDataImp" visible="false">
                                    <td align="right" valign="top">
                                        Data Impianto</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_data_PM" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Terapia_data_PM', document.form1.Wizard1$Terapia_data_PM.value, 'calendar320');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar320" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trTipol" visible="false">
                                    <td align="right" valign="middle">
                                        Tipologia</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_Tipologia" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trICD" visible="false">
                                    <td class="titolo_sezione" colspan="3">
                                        ICD<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Rbl_Aritmia_Icd[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Causale_Impianto.value='';document.form1.Wizard1$Cld_Aritmie_Icd.value='';document.form1.Wizard1$Txt_Aritmie_Icd_Scariche_Appropriate.value='';document.form1.Wizard1$Txt_Aritmie_Icd_Scariche_Inappropriate.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trICD2" visible="false">
                                    <td align="right" valign="middle">
                                        ICD</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Aritmia_Icd" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Terapia_ch_ICD"  Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trCausale2" visible="false">
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_CausaleImp" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trDataImp2" visible="false">
                                    <td align="right" valign="top">
                                        Data impianto</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_data_impianto" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Terapia_data_impianto', document.form1.Wizard1$Terapia_data_impianto.value, 'calendar330');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar330" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trScariche" visible="false">
                                    <td align="right" valign="middle">
                                        Scariche appropriate</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox ID="Terapia_txt_scaricheApp" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trScaricheIn" visible="false">
                                    <td align="right" valign="middle">
                                        Scariche inappropriate</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox ID="Terapia_txt_scaricheInapp" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trAblazione" visible="false">
                                    <td align="right" valign="middle">
                                        Ablazione</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_Ablazione" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trPree" visible="false">
                                    <td align="right" valign="middle">
                                        Preeccitazione</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_preeccitazione" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trPMBiv" visible="false">
                                    <td class="titolo_sezione" colspan="3">
                                        PM BIVENTR.<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Txt_Aritmie_Pm_Biventr.value='';document.form1.Wizard1$Cld_Aritmie_Data_Impianto.value='';document.form1.Wizard1$Ddl_Aritmia_Pm_Biventr.selectedIndex=0;document.form1.Wizard1$Txt_Aritmie_FE_Prima.value='';document.form1.Wizard1$Txt_Aritmie_FE_A_6_Mesi.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trCausale" visible="false">
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_CausaleBiventricolare" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trDataImp3" visible="false">
                                    <td align="right" valign="top">
                                        Data impianto</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        &nbsp;</td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_data_PmBiventricolare" runat="server" Columns="10" 
                                            MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Terapia_data_PmBiventricolare', document.form1.Wizard1$Terapia_data_PmBiventricolare.value, 'calendar340');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar340" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                         <asp:WizardStep ID="WizardStep10" runat="server" Title="Diagnosi">
                         <table>
                           <tr>
                                    <td colspan="3" class="titolo_sezione">DIAGNOSI <asp:Label ID="lb_gv" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diagnosi</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Diagnosi" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                         </table>
                         </asp:WizardStep>
                           <asp:WizardStep runat="server" ID="wizard_QUADRO"  StepType="Step" Title="QUADRO CLINICO">
                                  <div class="titolo_step">QUADRO CLINICO</div>
                                <table>
                                  <tr>
                                    <td colspan="3" class="titolo_sezione">DATA INIZIO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Data_Anamnesi_Scompenso_Attuale.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data inizio scompenso attuale</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator21" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Data_Anamnesi_Scompenso_Attuale" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Data_Anamnesi_Scompenso_Attuale.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Data_Anamnesi_Scompenso_Attuale" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Data_Anamnesi_Scompenso_Attuale', document.form1.Wizard1$Cld_Data_Anamnesi_Scompenso_Attuale.value, 'calendar123');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar123" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ED. POL. ACUTO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Ed_Pol_Acuto[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        ED. POL. ACUTO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtEdPolAcuto" runat="server" MaxLength="255"></asp:TextBox>*/%>
                                        <asp:CheckBox runat="server" ID="txtEdPolAcuto_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DISPNEA PAROSS. NOTT.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Dispnea_Nott.value='';document.form1.Wizard1$Cld_Data_Anamnesi_Dispnea_Nott.value='';document.form1.Wizard1$Cld2_Data_Anamnesi_Dispnea_Nott.value='';document.form1.Wizard1$Cld3_Data_Anamnesi_Dispnea_Nott.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        N� casi
                                       
                                        </td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Anamnesi_Dispnea_Nott" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data Inizio</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator22" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Data_Anamnesi_Dispnea_Nott" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Data_Anamnesi_Dispnea_Nott.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Data_Anamnesi_Dispnea_Nott" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Data_Anamnesi_Dispnea_Nott', document.form1.Wizard1$Cld_Data_Anamnesi_Dispnea_Nott.value, 'calendar130');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar130" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">Classe NYHA <div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Ddl_Anamnesi_Dispnea_Sforzo.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        </td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Anamnesi_Dispnea_Sforzo" runat="server">
                                            <asp:ListItem Value=""></asp:ListItem>
                                            <asp:ListItem Value="II classe">II classe</asp:ListItem>
                                            <asp:ListItem Value="III classe">III classe</asp:ListItem>
                                            <asp:ListItem Value="IV classe">IV classe</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ASTENIA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$ddl_Anamnesi_Astenia.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        ASTENIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddl_Anamnesi_Astenia" runat="server">
                                            <asp:ListItem Value=""></asp:ListItem>
                                            <asp:ListItem Value="II classe">II classe</asp:ListItem>
                                            <asp:ListItem Value="III classe">III classe</asp:ListItem>
                                            <asp:ListItem Value="IV classe">IV classe</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ORTOPNEA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_N_Cuscini.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        N� cuscini</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox onblur="data_change(this)" ID="Txt_Anamnesi_N_Cuscini" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">SLEEP APNEA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Sleep.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        SLEEP APNEA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/* <asp:TextBox ID="Txt_Anamnesi_Sleep" runat="server" MaxLength="255" 
                                            Columns="40" Rows="5"></asp:TextBox>*/ %> <asp:CheckBox runat="server" ID="Txt_Anamnesi_Sleep_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CONTRAZ. DIURESI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$RBL_Anamnesi_Contraz_Diuresi.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CONTRAZ. DIURESI</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="RBL_Anamnesi_Contraz_Diuresi" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Lieve</asp:ListItem>
                                            <asp:ListItem>Moderata</asp:ListItem>
                                            <asp:ListItem>Severa</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">PA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Pa_Ingresso.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PA Sistolica</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  ID="txtPASistolica" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PA Diastolica</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtPADiastolica" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">FREQUENZA CARDIACA</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        F.C.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Fc_Ingresso" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td colspan="3" class="titolo_sezione">POLSO RITMICO - ARITMICO <div class="div_reset">
                                        <a href="#" 
                                            onclick="document.form1.Wizard1$Txt_Anamnesi_Polso_Ingresso.value='';document.form1.Wizard1$Rbl_Anamnesi_Polso_Ingresso[0].checked=false;document.form1.Wizard1$Rbl_Anamnesi_Polso_Ingresso[1].checked=false;return false;">
                                        reset</a></div></td>
                                </tr>--%>
                                <tr style="display:none;">
                                    <td align="right" valign="middle">
                                        POLSO INGRESSO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox Visible="false" ID="Txt_Anamnesi_Polso_Ingresso" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr style="display:none;">
                                    <td align="right" valign="middle">
                                        Tipologia ritmica</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList Visible="false" ID="Rbl_Anamnesi_Polso_Ingresso" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Ritmico</asp:ListItem>
                                            <asp:ListItem>Aritmico</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CIANOSI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Cianosi[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CIANOSI</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtCianosi" runat="server" AutoPostBack="True"></asp:TextBox>*/ %>
                                         <asp:CheckBox runat="server" ID="txtCianosi_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">TONI CARDIACI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Toni_Cardiaci[1].checked=true;document.form1.Wizard1$ddl_Anamnesi_Descrizione_Toni_Cardiaci.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TONI CARDIACI</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Toni_Cardiaci" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True">Validi</asp:ListItem>
                                            <asp:ListItem>Ridotti</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Descrizione toni cardiaci</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddl_Anamnesi_Descrizione_Toni_Cardiaci" runat="server">
                                            <asp:ListItem Value=""></asp:ListItem>
                                            <asp:ListItem Value="I tono ridotto">I tono ridotto</asp:ListItem>
                                            <asp:ListItem Value="III tono">III tono</asp:ListItem>
                                            <asp:ListItem Value="IV tono">IV tono</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">SOFFI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Soffi[1].checked=true;document.form1.Wizard1$Txt_Anamnesi_Soffi_Descrizione.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        SOFFI</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Anamnesi_Soffi" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                         <asp:CheckBox runat="server" ID="Rbl_Anamnesi_Soffi_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Descrizione</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Soffi_Descrizione" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CONGESTIONE POLM.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Congestione_Polm.selectedIndex=0;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">CONGESTIONE POLM.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Rbl_Anamnesi_Congestione_Polm" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Lieve</asp:ListItem>
                                            <asp:ListItem>Moderata</asp:ListItem>
                                            <asp:ListItem>Severa</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CONGEST. PERIF.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Giugulari[1].checked=true;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Epatomegalia[1].checked=true;document.form1.Wizard1$Ddl_Anamnesi_Edemi_Periferici.selectedIndex=0;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Versamento_Pleurico[1].checked=true;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Ascite[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Giugulari</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtGiugulari" runat="server"></asp:TextBox>*/ %>
                                         <asp:CheckBox runat="server" ID="txtGiugulari_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Epatomegalia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtEpatomegalia" runat="server"></asp:TextBox>*/ %>
                                         <asp:CheckBox runat="server" ID="txtEpatomegalia_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Edemi periferici</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Anamnesi_Edemi_Periferici" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                       </td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtVersamentoPleurico" Visible="false" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ascite</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <%/*<asp:TextBox ID="txtAscite" runat="server"></asp:TextBox>*/ %>
                                         <asp:CheckBox runat="server" ID="txtAscite_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">POLSI ARTER.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Polsi_Arter_Presenti[1].checked=true;document.form1.Wizard1$Txt_Anamnesi_Polsi_Arter_Non_Percepibili.value='';document.form1.Wizard1$Rbl_Anamnesi_Polsi_Arter_Doppler[1].checked=true;return false;">
                                        reset</a></div>
                                        &nbsp;PERIFERICI</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Presenti</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <% /* <asp:RadioButtonList ID="Rbl_Anamnesi_Polsi_Arter_Presenti" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Anamnesi_Polsi_Arter_Presenti_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Non percepibili (precisare distretto)</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Polsi_Arter_Non_Percepibili" runat="server" MaxLength="255" Columns="40" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Document. Doppler</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                       <%/*  <asp:RadioButtonList ID="Rbl_Anamnesi_Polsi_Arter_Doppler" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Rbl_Anamnesi_Polsi_Arter_Doppler_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                
                            </table>
                        </asp:WizardStep>
                    </WizardSteps>
                    <StepStyle Font-Size="0.8em" ForeColor="#333333" />
                  
                    <SideBarButtonStyle BackColor="#507CD1" Font-Names="Verdana" ForeColor="White" />
                    <HeaderStyle BackColor="#284E98" BorderColor="#EFF3FB" BorderStyle="Solid" BorderWidth="2px"
                        Font-Bold="True" Font-Size="0.9em" ForeColor="White" HorizontalAlign="Center" />
                </asp:Wizard>

            </td>
          </tr>
      </table>
    </div>
        

        <br />
        <div style="width:100%; background-color:#eeeeee; height:30px; vertical-align:middle; text-decoration:none; font-size:12px; color:#333333;">
                    <div style="margin-top:8px;"><i><asp:LinkButton ForeColor="#333333" 
                            ID="LinkButton2" runat="server" style="font-weight: 700;" 
                            onclick="LinkButton1_Click">Salva Tutto</asp:LinkButton></i></div>
                    </div>
        <asp:Label ID="lblQuadro" runat="server" Visible="False"></asp:Label>
        <br />
        <asp:Label ID="lb_mess" runat="server" Text="" ForeColor="Red"></asp:Label>
    
    </td>
    <td width="10" valign="top" background="./files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="./files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="2" style="height: 20px"><!--#include file="include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="./files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" background="./files/bg_content_bottom.gif">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>

<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StatisticheDiametroSistolicoNew.aspx.cs" Inherits="StatisticheDiametroSistolicoNew" %>

<%@ Register Src="statistichegeneralinew.ascx" TagName="statistichegenerali" TagPrefix="uc1" %>
<!--#include file="include/controllo_utenti.inc"-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="stile.css" type=text/css rel="stylesheet" />
<script language="JavaScript" src="./js/ts_picker.js"></script>
<script language="JavaScript" src="./js/controlli.js"></script>
<title>Statistiche</title>

<style type="text/css">
<!--
body {
	background-image: url(./files/bg_blueGrad.gif);
	background-color: #324671;
}
.style1 {color: #FFFFFF}
.Stile1 {color: #336DD0}
-->
</style>
</head>

<body>
   <form id="form1" runat="server">
<table width="812" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td width="10" valign="top" background="./files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center"><div align="left">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
              <!--#include file="include/statistiche.inc"-->
          </td>
        </tr>
        <tr>
            <td align="center">
                <br />
                <br />
                <span style="font-size:18px;color:#324671;font-weight:bold">Statistiche</span>
                <br />
                <br />
            </td>
          </tr>
          <tr>
          <td>
            <uc1:statistichegenerali ID="Statistichegenerali1" runat="server" />
            <div class="blocco_statistiche_big">
                <div class="titolo_blocco">
                    Diametro Sistolico</div>
                <table cellpadding="4" cellspacing="4" width="100%">
                    <tr>
                        <td align="right">
                            Diametro Sistolico</td>
                        <td align="left">
                            <asp:RadioButtonList ID="Rbl_Diametro_Sistolico" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True" >Maggiore di 38 mm</asp:ListItem>
                                                <asp:ListItem>Maggiore di 50 mm</asp:ListItem>
                                            <asp:ListItem>Maggiore di 55 mm</asp:ListItem>
                                        </asp:RadioButtonList>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btn_cerca" runat="server" Text="Cerca" OnClick="btn_cerca_Click" />
                        </td>
                    </tr>
                </table>
            </div>
          <asp:Panel ID="pn_risultati" runat="server" CssClass="blocco_statistiche_big" Visible="false">
                <div class="titolo_blocco" style="margin-bottom:10px">Dati generali</div>
                <div class="sottoblocco_statistiche_150" style="margin-left:8px">
	                <div style="padding:3px;">
                        <asp:Label ID="lb_pazienti" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <div class="sottoblocco_statistiche_150">
	                <div style="padding:3px;">
                        <asp:Label ID="lb_eta" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <div class="sottoblocco_statistiche_150">
	                <div style="padding:3px;">
                        <asp:Label ID="lb_etaMin" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <div class="sottoblocco_statistiche_150" style="margin-right:8px">
	                <div style="padding:3px;">
                        <asp:Label ID="lb_etaMax" runat="server" Text=""></asp:Label>
                    </div>
                </div>
                <div style="clear:both;height:1px;padding:0;margin:0"></div>
                <asp:PlaceHolder ID="ph_fumo" runat="server">
                    <div class="sottoblocco_statistiche_150" style="margin-left:8px;">
		                <div class="titolo_sottoblocco">Fumatori</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_fumatori" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                  <div class="sottoblocco_statistiche_150">
		                <div class="titolo_sottoblocco">CARDIOP. ISCHEM. SENZA DOCUMENT. IMA</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_IMANODoc" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                  <div class="sottoblocco_statistiche_150">
		                <div class="titolo_sottoblocco">Colesterolo (LDL>100)</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_Colesterolo" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                  <div class="sottoblocco_statistiche_150">
		                <div class="titolo_sottoblocco">Anemia</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_Anemia" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
                    <div class="sottoblocco_statistiche_150" style="display:none;">
		                <div class="titolo_sottoblocco">Fumatori +</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_fumatori1" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                <div class="sottoblocco_statistiche_150" style="display:none;">
		                <div class="titolo_sottoblocco">Fumatori ++</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_fumatori2" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                <div class="sottoblocco_statistiche_150" style="margin-right:8px; display:none;">
		                <div class="titolo_sottoblocco">Fumatori +++</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_fumatori3" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                <div style="clear:both;height:1px;padding:0;margin:0"></div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="ph_row2stat" runat="server">
                    <div class="sottoblocco_statistiche_150" style="margin-left:8px">
		                <div class="titolo_sottoblocco">Ipert. Arteriosa</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_ipertensione_art" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
                    <div class="sottoblocco_statistiche_150">
		                <div class="titolo_sottoblocco">Diabete</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_diabete" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                <div class="sottoblocco_statistiche_150">
		                <div class="titolo_sottoblocco">IMA</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_IMA" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                <div class="sottoblocco_statistiche_150" style="margin-right:8px">
		                <div class="titolo_sottoblocco">BPCO</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_BPCO" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                <div style="clear:both;height:1px;padding:0;margin:0"></div>
                </asp:PlaceHolder>
                
                <asp:PlaceHolder ID="ph_row3stat" runat="server">
                    <div class="sottoblocco_statistiche_150" style="margin-left:8px">
		                <div class="titolo_sottoblocco">Insuff. Renale</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_InsuffRenale" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
                  
	                <div class="sottoblocco_statistiche_150" style="display:none;">
		                <div class="titolo_sottoblocco">BMI > 30</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_BMI" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                <div class="sottoblocco_statistiche_150" style="margin-right:8px;display:none;">
		                <div class="titolo_sottoblocco">Ipertiroidismo</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_Ipertiroidismo" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	                <div style="clear:both;height:1px;padding:0;margin:0"></div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="ph_row4stat" runat="server">
                    <div class="sottoblocco_statistiche_150" style="margin-left:8px;display:none;">
		                <div class="titolo_sottoblocco">Sind. Metabolica</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_Sindrome" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
                    <div class="sottoblocco_statistiche_150" style="display:none;">
		                <div class="titolo_sottoblocco">Ipercolesterolemia</div>
		                <div style="padding:3px;">
                            <asp:Label ID="lb_ColesteroloOLD" runat="server" Text=""></asp:Label>
                        </div>
	                </div>
	               
	                <div style="clear:both;height:1px;padding:0;margin:0"></div>
                </asp:PlaceHolder>
                
                
            </asp:Panel>

            <div style="margin-left:40px;float:left;display: inline;">
                <asp:GridView ID="gv_pazienti" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gv_pazienti_PageIndexChanging" PageSize="15">
                    <FooterStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <RowStyle BackColor="#EFF3FB" />
                    <EditRowStyle BackColor="#2461BF" />
                    <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                    <PagerStyle BackColor="#006699" ForeColor="White" HorizontalAlign="Center" />
                    <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
                    <AlternatingRowStyle BackColor="White" />
                    <Columns>
                        <asp:BoundField DataField="IdPaziente" Visible="false" />
                        <asp:BoundField DataField="IdQuadroClinico" Visible="false" />
                        <asp:BoundField DataField="CodiceFiscale" HeaderText="Codice Fiscale" Visible="false" />
                        <asp:BoundField DataField="Cognome" HeaderText="Cognome" />
                        <asp:BoundField DataField="Nome" HeaderText="Nome" />
                        <asp:TemplateField HeaderText="Data Registrazione">
                            <ItemTemplate>
                                <%# Utility.DecodificaData(DataBinder.Eval(Container.DataItem, "DataRegistrazione").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Data Osservazione">
                            <ItemTemplate>
                                <%# Utility.DecodificaData(DataBinder.Eval(Container.DataItem, "DataQuadroClinico").ToString())%>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <a href='<%# "nuovopaziente.aspx?idpaziente="+DataBinder.Eval(Container.DataItem, "IdPaziente")%>'>scheda</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <a href='<%# "dativariabili.aspx?idpaziente="+DataBinder.Eval(Container.DataItem, "IdPaziente") +"&idquadro="+DataBinder.Eval(Container.DataItem, "IdQuadroClinico")%>'>quadro cl.</a>
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
                        <br />
        <br />
        <asp:Label ID="lb_mess" runat="server" Text="" ForeColor="Red"></asp:Label>
            </div>            
          </td>
        </tr>
      </table>
    </div>

    
    </td>
    <td width="10" valign="top" background="./files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="./files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="3" style="height: 20px"><!--#include file="include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="./files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" background="./files/bg_content_bottom.gif">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>



<%@ Page Language="C#" AutoEventWireup="true" CodeFile="statistichenew.aspx.cs" Inherits="statistichenew" %>

<%@ Register Src="statistichegeneralinew.ascx" TagName="statistichegenerali" TagPrefix="uc1" %>
<!--#include file="include/controllo_utenti.inc"-->

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="stile.css" type=text/css rel="stylesheet" />
<script language="JavaScript" src="./js/ts_picker.js"></script>
<script language="JavaScript" src="./js/controlli.js"></script>
<title>Statistiche</title>

<style type="text/css">
<!--
body {
	background-image: url(./files/bg_blueGrad.gif);
	background-color: #324671;
}
.style1 {color: #FFFFFF}
.Stile1 {color: #336DD0}
-->
</style>
<link rel="stylesheet" type="text/css" href="./ext-4.0.2a/resources/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="./ext-4.0.2a/examples/shared/example.css" />
    <script type="text/javascript" src="./ext-4.0.2a/bootstrap.js"></script>
    <script type="text/javascript" src="./ext-4.0.2a/examples/example-data.js"></script>
</head>

<body>
   <form id="form1" runat="server">
   <script>
    <!--#include FILE="./ext-4.0.2a/examples/example-data.js" --> 
    
    Ext.require('Ext.chart.*');
Ext.require('Ext.layout.container.Fit');

Ext.onReady(function () {
   store1.loadData(generateDataGISCSesso());
    var pazientiTotali=<%=numPazienti %>;
    var donut = true,
        panel1 = Ext.create('widget.panel', {
        width: 450,
        height: 330,
        title: 'Dashboard Pazienti',
        renderTo: 'divDashboard',
        hidden:false,
        layout: 'fit',
        items: {
            xtype: 'chart',
            id: 'chartCmp',
            animate: true,
            store: store1,
            shadow: true,
            legend: {
                position: 'bottom'
            },
            insetPadding: 20,
            theme: 'Base:gradients',
            series: [{
                type: 'pie',
                field: 'data1',
                showInLegend: true,
                donut: donut,
                tips: {
                  trackMouse: true,
                  width: 350,
                  height: 28,
                  renderer: function(storeItem, item) {
                    //calculate percentage.
                    var total = 0;
                    store1.each(function(rec) {
                        total += parseInt(rec.get('data1'));
                    });
                  
                    this.setTitle(storeItem.get('name') + ': N.' + storeItem.get('data1')+ ' - '+ (Math.round(parseInt(storeItem.get('data1')) / total * 100)) + '%');
                  }
                },
                highlight: {
                  segment: {
                    margin: 20
                  }
                },
                label: {
                    field: 'name',
                     display: 'outside',
                    //contrast: true,
                    font: '14px Arial'
                }
            }]
        }
    });
    
    //torta diabete
     //store3.loadData(generateDataGISCDiabete());
      var donut2 = true,
        panel2 = Ext.create('widget.panel', {
        width: 400,
        height: 500,
        hidden:true,
        title: 'Dashboard Diabetici',
        renderTo: 'divDiabete',
        layout: 'fit',
        items: {
            xtype: 'chart',
            id: 'chartCmp',
            animate: true,
            store: store3,
            shadow: true,
            legend: {
                position: 'bottom'
            },
            insetPadding: 60,
           // theme: 'Base:gradients',
            series: [{
                type: 'pie',
                field: 'data1',
                showInLegend: true,
                donut: donut,
                tips: {
                  trackMouse: true,
                 width: 350,
                  height: 28,
                  renderer: function(storeItem, item) {
                    //calculate percentage.
                    var total = 0;
                    store1.each(function(rec) {
                        total += parseInt(rec.get('data1'));
                    });
                   // alert(total);
                    this.setTitle(storeItem.get('name') + ': N.' + storeItem.get('data1')+ ' - '+ (Math.round(parseInt(storeItem.get('data1')) / total * 100)) + '%');
                  }
                },
                
                highlight: {
                  segment: {
                    margin: 20
                  }
                },
                label: {
                    field: 'name',
                    display: 'outside',
                    //contrast: true,
                    font: '14px Arial'
                }
            }]
        }
    });
    
    //grafico dei valori BBS,BBD,ecc.
    window.store2 = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: generateData()
    });
     store2.loadData(generateDataGISCValori());
    panel2 = Ext.create('widget.panel', {
        width: 800,
        height: 500,
        title: 'Statistiche Fattori di Rischio e ComorbiditÓ',
        renderTo: 'divDashboardValori',
        layout: 'fit',
        items: 
        {
    id: 'barTopTen',
    xtype: 'chart',
    style: 'background:#fff;margin:8px;',
     animate: {
                easing: 'bounceOut',
                duration: 750
            },
    height: 400,
    width: 892,
    shadow: true,
    store: store2,

    axes: 
    [
        {
            type: 'Numeric',
            position: 'bottom',
            fields: 'data1',
            minimum: 0,
	        maximum: <%=numPazienti %>,
	        decimals:0,
            label: 
            {
            },

            grid: true,
            title:'Numero di pazienti'
        }, 
        {
            type: 'Category',
            position: 'left',
            fields: 'name',
            title:'Valori'
        }
    ],

    series: 
    [
        {
            type: 'bar',
            axis: 'bottom',
            xField: 'name',
            yField: 'data1',
            tips: {
                  trackMouse: true,
                width: 350,
                  height: 80,
                  renderer: function(storeItem, item) {
                 
                   this.setTitle('Numero pazienti con ' + storeItem.get('name') + ':' + storeItem.get('data1')+ '<br/>( '  +  (Math.round(parseInt(storeItem.get('data1')) / pazientiTotali * 100)) + '% sul totale) <br/> Maschi: ' + storeItem.get('data2') + '<br/> Femmine:' + storeItem.get('data3'));
                    
                  }
                }

        }
    ]
}


         });
         
         
          //grafico dei valori BBS,BBD,ecc.
    window.store21 = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: generateData()
    });
     store21.loadData(generateDataGISCEziologia());
    panel2 = Ext.create('widget.panel', {
        width: 800,
        height: 500,
        title: 'Statistiche Eziologia',
        renderTo: 'divEziologia',
        layout: 'fit',
        items: 
        {
    id: 'barTopTen',
    xtype: 'chart',
    style: 'background:#fff;margin:8px;',
     animate: {
                easing: 'bounceOut',
                duration: 750
            },
    height: 400,
    width: 892,
    shadow: true,
    store: store21,

    axes: 
    [
        {
            type: 'Numeric',
            position: 'bottom',
            fields: 'data1',
            minimum: 0,
	        maximum: <%=numPazienti %>,
	        decimals:0,
            label: 
            {
            },

            grid: true,
            title:'Numero di pazienti'
        }, 
        {
            type: 'Category',
            position: 'left',
            fields: 'name',
            title:'Valori'
        }
    ],

    series: 
    [
        {
            type: 'bar',
            axis: 'bottom',
            xField: 'name',
            yField: 'data1',
            style: {
                    fill: '#38B8BF'
                },
            tips: {
                  trackMouse: true,
                  width: 350,
                  height: 80,
                  renderer: function(storeItem, item) {
                 
                   this.setTitle('Numero pazienti con ' + storeItem.get('name') + ':' + storeItem.get('data1')+ '<br/>( '  +  (Math.round(parseInt(storeItem.get('data1')) / pazientiTotali * 100)) + '% sul totale) <br/> Maschi: ' + storeItem.get('data2') + '<br/> Femmine:' + storeItem.get('data3'));
                    
                  }
                }

        }
    ]
}


         });
         
         
          //grafico dei valori BBS,BBD,ecc.
    window.store22 = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: generateData()
    });
     store22.loadData(generateDataGISCClinica());
    panel2 = Ext.create('widget.panel', {
        width: 800,
        height: 500,
        title: 'Statistiche Clinica',
        renderTo: 'divClinica',
        layout: 'fit',
        items: 
        {
    id: 'barTopTen',
    xtype: 'chart',
    style: 'background:#fff;margin:8px;',
     animate: {
                easing: 'bounceOut',
                duration: 750
            },
    height: 400,
    width: 892,
    shadow: true,
    store: store22,

    axes: 
    [
        {
            type: 'Numeric',
            position: 'bottom',
            fields: 'data1',
            minimum: 0,
	        maximum: <%=numPazienti %>,
	        decimals:0,
            label: 
            {
            },

            grid: true,
            title:'Numero di pazienti'
        }, 
        {
            type: 'Category',
            position: 'left',
            fields: 'name',
            title:'Valori'
        }
    ],

    series: 
    [
        {
            type: 'bar',
            axis: 'bottom',
            xField: 'name',
            yField: 'data1',
             style: {
                    fill: '#ff77ff'
                },
            tips: {
                  trackMouse: true,
                 width: 350,
                  height: 80,
                  renderer: function(storeItem, item) {
                 
                   this.setTitle('Numero pazienti con ' + storeItem.get('name') + ':' + storeItem.get('data1')+ '<br/>( '  +  (Math.round(parseInt(storeItem.get('data1')) / pazientiTotali * 100)) + '% sul totale) <br/> Maschi: ' + storeItem.get('data2') + '<br/> Femmine:' + storeItem.get('data3'));
                    
                  }
                }

        }
    ]
}


         });
         
});

    
    </script>
<table width="812" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td width="10" valign="top" background="./files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center"><div align="left">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
              <!--#include file="include/statistiche.inc"-->
          </td>
        </tr>
        <tr>
            <td align="center">
                <br />
                <br />
                <span style="font-size:18px;color:#324671;font-weight:bold">Statistiche</span>
                <br />
                <br />
            </td>
          </tr>
          <tr>
          <td>
            <uc1:statistichegenerali ID="Statistichegenerali1" runat="server" />
            <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichearitmienew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Aritmie</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichebbsnew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche BBS</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichebbdnew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche BBD</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;">
                <a href="statisticheischemianew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Ischemia</a>
            </div>
             <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichebavnew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche BAV</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;">
                <a href="statisticheipertrofianew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Ipertrofia</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;display:none;">
                <a href="StatisticheIpocinesiaNew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Ipocinesia</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichediametrosistoliconew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Diametro Sistolico</a>
            </div>
             <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichediametrodiastolico.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Diametro Diastolico</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichefenew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche FE</a>
            </div>
             <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichefcnew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche FC</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichepressionenew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Pressione Polmonare</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichedisfdiastNEW.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Disfunzione Diastolica</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;">
                <a href="StatisticheArtInfNew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Doppler Arterioso Arti Inferiori con Stenosi Significative</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;display:none;">
                <a href="statistichedopplerteanew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Doppler TEA con Stenosi Significative</a>
            </div>
            <div style="margin: 0px 20px 10px 40px;">
                <a href="statistichenyhanew.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche NYHA</a>
            </div>
             <div style="margin: 0px 20px 10px 40px;">
                <a href="statisticheDeaths.aspx" class="link" style="font-size:14px;font-weight:bold">Statistiche Deads</a>
            </div>
             <div style="margin: 0px 20px 10px 40px;">
                <a href="AltreStatisticheNew.aspx" class="link" style="font-size:14px;font-weight:bold">Altre Statistiche</a>
            </div>
             <div style="margin: 0px 20px 10px 40px;">
                <a href="trendsTotali.aspx" class="link" style="font-size:14px;color:#C72E2E ;font-weight:bold">Trend Totali</a>
            </div>
              <div id="divDashboard" style="margin-left:40px;"></div>
          </td>
        </tr>
      </table>
      <table style="width:100%;">
      <tr><td>
    
       
        </td>
        <td> <div style="width:50%;" id="divDiabete"></div></td>
        </tr>
        </table>
      <div style="width:100%; height:500px;" id="divDashboardValori"></div>
      <div style="width:100%; height:500px;" id="divEziologia"></div>
      <div style="width:100%; height:500px;" id="divClinica"></div>
      
      
    </div>
        <br />
        <br />
        <asp:Label ID="lb_mess" runat="server" Text="" ForeColor="Red"></asp:Label>
    
    </td>
    <td width="10" valign="top" background="./files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="./files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="3" style="height: 20px"><!--#include file="include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="./files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" background="./files/bg_content_bottom.gif">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>
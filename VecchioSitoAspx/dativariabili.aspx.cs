﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
public partial class dativariabili : System.Web.UI.Page
{
    private string conn_str;


    private int IdDispnea1;
    private int IdDispnea2;
    private int IdDispnea3;
    private int IdPaziente;
    private int idquadro;
    private int idreliquato;
    private int IdSincope1;
    private int IdSincope2;
    private int IdSincope3;
    private int IdUltimoQuadro;


    private bool ApportaModificheDispnea(SqlTransaction trans, SqlConnection conn, TextBox cld1, int iddispnea)
    {
        string str;
        bool flag = false;
        if (this.idquadro.Equals(-1))
        {
            str = "inserisci_dispnea";
        }
        else
        {
            str = "modifica_dispnea";
        }
        try
        {
            int idUltimoQuadro;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.idquadro.Equals(-1))
            {
                idUltimoQuadro = this.IdUltimoQuadro;
            }
            else
            {
                idUltimoQuadro = this.idquadro;
                command.Parameters.AddWithValue("@IdDispneaParosNott", iddispnea);
            }
            command.Parameters.AddWithValue("@IdQuadro", idUltimoQuadro);
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(cld1.Text.Trim()));
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModificheFarmaci(SqlTransaction trans, SqlConnection conn, int idQuadro)
    {
        string str;
        bool flag = false;
        if (this.idquadro.Equals(-1))
        {
            str = "inserisci_farmaci";
        }
        else
        {
            str = "modifica_farmaci";
        }
        try
        {
            foreach (GridViewRow row in this.gv_farmaci.Rows)
            {
                int idquadro;
                string selectedValue = "";
                string str3 = "";
                string str4 = "";
                if (row.Cells[5].Controls.Count > 0)
                {
                    if (row.Cells[1].Text.Equals("Anticoagulanti"))
                    {
                        RadioButtonList list = (RadioButtonList)row.Cells[5].Controls[3];
                        selectedValue = list.SelectedValue;
                    }
                    else
                    {
                        TextBox box = (TextBox)row.Cells[5].Controls[1];
                        selectedValue = box.Text.Trim();
                    }
                }
                if (row.Cells[6].Controls.Count > 0)
                {
                    CheckBox box2 = (CheckBox)row.Cells[6].Controls[1];
                    if (box2.Checked)
                    {
                        str3 = "S";
                    }
                    else
                    {
                        str3 = "N";
                    }
                }
                if (row.Cells[7].Controls.Count > 0)
                {
                    TextBox box3 = (TextBox)row.Cells[7].Controls[1];
                    str4 = box3.Text.Trim();
                }
                SqlCommand command = new SqlCommand(str, conn, trans);
                command.CommandType = CommandType.StoredProcedure;
                if (this.idquadro.Equals(-1))
                {
                    idquadro = idQuadro;
                }
                else
                {
                    idquadro = this.idquadro;
                }
                command.Parameters.AddWithValue("@IdQuadroClinico", idquadro);
                command.Parameters.AddWithValue("@NomeFarmaco", row.Cells[1].Text);
                command.Parameters.AddWithValue("@Valore", selectedValue);
                command.Parameters.AddWithValue("@Intolleranza", str3);
                command.Parameters.AddWithValue("@CostoFarmaco", str4);
                command.ExecuteNonQuery();
            }
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModificheReliquati(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.idquadro.Equals(-1))
        {
            str = "inserisci_reliquato";
        }
        else
        {
            str = "modifica_reliquato";
        }
        try
        {
            int idUltimoQuadro;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.idquadro.Equals(-1))
            {
                idUltimoQuadro = this.IdUltimoQuadro;
            }
            else
            {
                idUltimoQuadro = this.idquadro;
            }
            command.Parameters.AddWithValue("@IdQuadroClinico", idUltimoQuadro);
            command.Parameters.AddWithValue("@DataIctus", Utility.CodificaData(this.Cld_Reliquati_Motori_Ictus.Text.Trim()));
            command.Parameters.AddWithValue("@DataTIA", Utility.CodificaData(this.Cld_Reliquati_Motori_TIA.Text.Trim()));
            command.Parameters.AddWithValue("@NumSincope", Convert.ToInt32(this.Txt_Reliquati_Motori_Sincope_Episodi.Text.Trim()));
            command.Parameters.AddWithValue("@NomePatologiaAortica", this.Txt_Quadro_Clinico_Patologia_Aortica.Text.Trim());
            command.Parameters.AddWithValue("@DataPatolAortica", Utility.CodificaData(this.Cld_Reliquati_Motori_Data_Patologia_Aortica.Text.Trim()));
            command.Parameters.AddWithValue("@DopplerPatolAortica", this.Txt_Reliquati_Motori__Document_Doppler.Text.Trim());
            command.Parameters.AddWithValue("@TacPatolAortica", this.Txt_Reliquati_Motori_Document_TAC.Text.Trim());
            command.Parameters.AddWithValue("@VascoPerifData", Utility.CodificaData(this.Cld_Data_Reliquati_Motori_Vascolare_Periferica.Text.Trim()));
            command.Parameters.AddWithValue("@VascoPerifDocDoppler", this.Txt_Reliquati_Motori_Vascolare_Perif_Document_Doppler.Text.Trim());
            command.Parameters.AddWithValue("@ChemioData", Utility.CodificaData(this.Cld__Reliquati_Motori_Data_Chemioterapia.Text.Trim()));
            command.Parameters.AddWithValue("@ChemioFarmaci", this.Txt_Reliquati_Motori_Farmaci.Text.Trim());
            command.Parameters.AddWithValue("@RadioData", Utility.CodificaData(this.Cld_Reliquati_Motori_Data_Radioter_Tor.Text.Trim()));
            command.Parameters.AddWithValue("@HIV", this.Rbl_Reliquati_Motori_Hiv.Text.Trim());
            command.Parameters.AddWithValue("@EpatHCVCorr", this.Rbl_Reliquati_Motori_Epat_Hcv_Corr.Text.Trim());
            command.Parameters.AddWithValue("@Alcoolismo", this.Rbl_Reliquati_Motori_Alcoolismo.Text.Trim());
            command.Parameters.AddWithValue("@Stupefacenti", this.Txt_Reliquati_Motori_Stupefacenti.Text.Trim());
            command.Parameters.AddWithValue("@Altro", this.Txt_Reliquati_Motori_Altro.Text.Trim());
            command.Parameters.AddWithValue("@LivelloAnsieta", this.Ddl_Reliquati_Motori_Grado_Ansietà.SelectedValue);
            command.Parameters.AddWithValue("@DataAnsieta", Utility.CodificaData(this.Cld_Reliquati_Motori_Ansieta.Text.Trim()));
            command.Parameters.AddWithValue("@TerapiaAnsieta", this.Txt_Reliquati_Motori_Ansieta_Terapia.Text.Trim());
            command.Parameters.AddWithValue("@DataDepr", Utility.CodificaData(this.Cld_Reliquati_Motori_Depressione.Text.Trim()));
            command.Parameters.AddWithValue("@LivelloDepr", this.Ddl_Reliquati_Motori_Grado_Depressione.SelectedValue);
            command.Parameters.AddWithValue("@TerapiaDepr", this.Txt_Reliquati_Motori_Terapie_Depressione.Text.Trim());
            command.Parameters.AddWithValue("@TraumiPsichi", this.Txt_Reliquati_Motori_Traumi_Psichici_Specifica.Text.Trim());
            command.Parameters.AddWithValue("@ConfFarm", this.Ddl_Reliquati_Motori_Conflittualita_Famigliare.SelectedValue);
            command.Parameters.AddWithValue("@ConfLav", this.Ddl_Reliquati_Motori_Conflittualita_Lavorativa.SelectedValue);
            command.Parameters.AddWithValue("@ConfSoc", this.Ddl_Reliquati_Motori_Conflittualita_Sociale.SelectedValue);
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModificheSincope(SqlTransaction trans, SqlConnection conn, TextBox cld1, DropDownList drop1, int idsincope)
    {
        string str;
        bool flag = false;
        if (this.idquadro.Equals(-1))
        {
            str = "inserisci_sincope";
        }
        else
        {
            str = "modifica_sincope";
        }
        try
        {
            int idUltimoQuadro;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.idquadro.Equals(-1))
            {
                idUltimoQuadro = this.IdUltimoQuadro;
            }
            else
            {
                idUltimoQuadro = this.idquadro;
                command.Parameters.AddWithValue("@IdSincope", idsincope);
            }
            command.Parameters.AddWithValue("@IdQuadro", idUltimoQuadro);
            command.Parameters.AddWithValue("@data", Utility.CodificaData(cld1.Text.Trim()));
            command.Parameters.AddWithValue("@TipoSincope", drop1.SelectedValue);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    protected void CreaMenuStep(string current_step_id)
    {
        this.lb_steps.Text = "<div class=\"menu_step\" ><ul>";
        foreach (Control control in this.Wizard1.WizardSteps)
        {
            if (control.GetType().ToString().Equals("System.Web.UI.WebControls.WizardStep"))
            {
                WizardStep step = (WizardStep)control;
                if (step.ID.Equals(current_step_id))
                {
                    this.lb_steps.Text = this.lb_steps.Text + "<li class=\"current\"><span>" + step.Title + "</span></li>";
                }
                else
                {
                    this.lb_steps.Text = this.lb_steps.Text + "<li><span>" + step.Title + "</span></li>";
                }
            }
        }
        this.lb_steps.Text = this.lb_steps.Text + "</ul></div>";
    }

    private void ElencaFarmaci()
    {
        DataTable dataTable = new DataTable();
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            string cmdText = "";
            if (this.idquadro.Equals(-1))
            {
                cmdText = "select Farmaco.IdFarmaco as IdFarmaco, NomeFarmaco, IdFarmaco as Valore, IdFarmaco as Intolleranza, CostoFarmaco from Farmaco";
            }
            else
            {
                cmdText = "select Farmaco.IdFarmaco as IdFarmaco, NomeFarmaco, Valore, Intolleranza, PrevistoInTerapia.CostoFarmaco as CostoFarmaco from Farmaco left outer join PrevistoInTerapia on Farmaco.IdFarmaco=PrevistoInTerapia.IdFarmaco";
                cmdText = cmdText + " where IdQuadroClinico=" + this.idquadro;
            }
            SqlCommand command = new SqlCommand(cmdText, connection);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            try
            {
                try
                {
                    adapter.Fill(dataTable);
                }
                finally
                {
                    connection.Dispose();
                }
            }
            catch (Exception exception)
            {
                this.lb_mess.Text = exception.Message;
            }
            finally
            {
                adapter = null;
            }
            this.gv_farmaci.DataSource = dataTable;
            this.lb_gv.Text = "<div class=\"div_reset\"><a href=\"#\" onclick=\"document.form1.Wizard1$Txt_Esami_Strumentali_Diagnosi.value='';document.form1.Wizard1$Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA.selectedIndex=0;document.form1.Wizard1$Txt_Esami_Strumentali_Peso1.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Counceling[1].checked=true;";
            this.gv_farmaci.DataBind();
            this.lb_gv.Text = this.lb_gv.Text + "return false;\">reset</a></div>";
            if (dataTable.Rows.Count == 0)
            {
                this.gv_farmaci.Visible = false;
                this.lb_mess.Text = "Nessun farmaco presente.";
            }
            else
            {
                this.gv_farmaci.Visible = true;
                this.lb_mess.Text = "";
            }
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message;
        }
        finally
        {
            connection.Close();
        }
    }

    private bool estrai_Dispnea_Notturna()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("estrai_dispnea", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdQuadro", this.idquadro);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            this.Cld_Data_Anamnesi_Dispnea_Nott.Text = Utility.DecodificaData(reader["Data"].ToString());
                            break;

                        case 1:
                            this.Cld2_Data_Anamnesi_Dispnea_Nott.Text = Utility.DecodificaData(reader["Data"].ToString());
                            break;

                        case 2:
                            this.Cld3_Data_Anamnesi_Dispnea_Nott.Text = Utility.DecodificaData(reader["Data"].ToString());
                            break;
                    }
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = true;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool estrai_NYHA(DropDownList dropdown)
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        bool flag = false;
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_NYHA", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            for (int i = 0; reader.Read(); i++)
            {
                ListItem item = new ListItem(reader["Denominazione"].ToString(), reader["idClasseNYHA"].ToString());
                dropdown.Items.Add(item);
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool estrai_quadro()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("estrai_quadro", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Idquadro", this.idquadro);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Quadro_Clinico_Generale.Text = Utility.DecodificaData(reader["DataQuadroClinico"].ToString());
                this.lb_titolo.Text = "Quadro Clinico del " + Utility.DecodificaData(reader["DataQuadroClinico"].ToString()) + "<br />";
                this.Quadro_Clinico_Obesita.SelectedValue = reader["Obesita"].ToString();
                this.Quadro_Clinico_Sovrappeso.SelectedValue = reader["Sovrappeso"].ToString();
                this.Rbl_Quadro_Clinico_Obesita_Add.SelectedValue = reader["ObesitaUnifAddom"].ToString();
                this.Cld_Quadro_Clinico_Data_Obesita_Add.Text = Utility.DecodificaData(reader["ObesitaUnifAddomData"].ToString());
                this.Rbl_Quadro_Clinico_Iperglicemia.SelectedValue = reader["Iperglicemia"].ToString();
                this.Cld_Quadro_Clinico_Data_Iperglicemia.Text = Utility.DecodificaData(reader["IperglicemiaData"].ToString());
                this.Rbl_Quadro_Clinico_Ipertensione_Arteriosa.SelectedValue = reader["IpertArt"].ToString();
                this.Cld_Quadro_Clinico_Data_Ipertensione_Arteriosa.Text = Utility.DecodificaData(reader["IpertArtData"].ToString());
                this.Rbl_Quadro_Clinico_Iperslidemia.SelectedValue = reader["IperDislitemia"].ToString();
                this.Cld_Quadro_Clinico_Data_Iperdislipidemia.Text = Utility.DecodificaData(reader["IperDislitemiaData"].ToString());
                this.Rbl_Quadro_Clinico_BPCO.SelectedValue = reader["BPCO"].ToString();
                this.Cld_Quadro_Clinico_Data_BPCO.Text = Utility.DecodificaData(reader["BPCOData"].ToString());
                this.Rbl_Quadro_Clinico_Asma_Bronchiale.SelectedValue = reader["AsmaBronchiale"].ToString();
                this.Cld_Quadro_Clinico_Data_Asma_Bronchiale.Text = Utility.DecodificaData(reader["AsmaBronchialeData"].ToString());
                this.Rbl_Quadro_Clinico_Insufficienza_Renale.SelectedValue = reader["InsuffRenale"].ToString();
                this.Cld_Quadro_Clinico_Data_Insufficienza_Renale.Text = Utility.DecodificaData(reader["InsuffRenaleData"].ToString());
                this.Rbl_Quadro_Clinico_Anemia.SelectedValue = reader["Anemia"].ToString();
                this.Cld_Quadro_Clinico_Data_Anemia.Text = Utility.DecodificaData(reader["AnemiaData"].ToString());
                this.Cld_Quadro_Clinico_Data_Inizio_Ipertiroid.Text = Utility.DecodificaData(reader["IpertiroidInizio"].ToString());
                this.Txt_Quadro_Clinico_Ipertiroid_Terapia.Text = reader["IpertiroTerapia"].ToString();
                this.Rbl_Ipertiroid_Normalizz.SelectedValue = reader["IpertiroNormInd"].ToString();
                this.Cld_Quadro_Clinico_Data_Inizio_Ipotiroid.Text = Utility.DecodificaData(reader["Ipotirodinizio"].ToString());
                this.Txt_Quadro_Clinico_Ipotiroid_Terapia.Text = reader["IpotiroTerapia"].ToString();
                this.Rbl_Quadro_Clinico_Ipotiroid_Normalizzaz_Indici_Lab.SelectedValue = reader["Ipotironormind"].ToString();
                this.Cld_Data_Anamnesi_Scompenso_Attuale.Text = Utility.DecodificaData(reader["DataInizioscmPensoAttuale"].ToString());
                this.Rbl_Anamnesi_Ed_Pol_Acuto.SelectedValue = reader["EdPolAcuto"].ToString();
                this.Txt_Anamnesi_Dispnea_Nott.Text = reader["NEpisodiDispnea"].ToString();
                this.Ddl_Anamnesi_Dispnea_Sforzo.SelectedValue = reader["ClasseDispneaSforzo"].ToString();
                this.ddl_Anamnesi_Astenia.SelectedValue = reader["Astenia"].ToString();
                this.Txt_Anamnesi_N_Cuscini.Text = reader["Ncuscini"].ToString();
                this.Txt_Anamnesi_Sleep.Text = reader["SleepApnea"].ToString();
                this.RBL_Anamnesi_Contraz_Diuresi.SelectedValue = reader["ContrazDiuresi"].ToString();
                this.Txt_Anamnesi_Rapido_Increm_Peso.Text = reader["RecRapidoIncrementoPeso"].ToString();
                this.Txt_Anamnesi_Cardiopalmo.Text = reader["Cardiopalmo"].ToString();
                this.Txt_Anamnesi_Pa_Ingresso.Text = reader["PaIngresso"].ToString();
                this.Txt_Anamnesi_Pa_Dimissione.Text = reader["PaDimiss"].ToString();
                this.Txt_Anamnesi_Fc_Ingresso.Text = reader["FCIngresso"].ToString();
                this.Txt_Anamnesi_Fc_Dimissione.Text = reader["FcDimissione"].ToString();
                this.Txt_Anamnesi_Polso_Ingresso.Text = reader["PolsoIngresso"].ToString();
                this.Rbl_Anamnesi_Polso_Ingresso.SelectedValue = reader["PolsoIngrRitmo"].ToString();
                this.Txt_Anamnesi_Polso_Dimissione.Text = reader["PolsoDimissione"].ToString();
                this.Rbl_Anamnesi_Polso_Dimissione.SelectedValue = reader["PolsoDimRitmo"].ToString();
                this.Rbl_Anamnesi_Cianosi.SelectedValue = reader["Cianosi"].ToString();
                this.Rbl_Anamnesi_Toni_Cardiaci.SelectedValue = reader["ToniValidiRidotti"].ToString();
                this.ddl_Anamnesi_Descrizione_Toni_Cardiaci.SelectedValue = reader["DescrizioneToniCardiaci"].ToString();
                this.Rbl_Anamnesi_Soffi.SelectedValue = reader["Soffi"].ToString();
                this.Txt_Anamnesi_Soffi_Descrizione.Text = reader["DescrizioneSoffi"].ToString();
                this.Rbl_Anamnesi_Congestione_Polm.SelectedValue = reader["CongestionePolmonare"].ToString();
                this.Rbl_Anamnesi_Congestione_Perif_Giugulari.SelectedValue = reader["CongestionePerifGiugularimag3"].ToString();
                this.Rbl_Anamnesi_Congestione_Perif_Epatomegalia.SelectedValue = reader["CongestionePerifEpatomegalia"].ToString();
                this.Ddl_Anamnesi_Edemi_Periferici.SelectedValue = reader["CongestionePerifEdemiPeriferici"].ToString();
                this.Rbl_Anamnesi_Congestione_Perif_Versamento_Pleurico.SelectedValue = reader["CongestionePerifVersamentoPleurico"].ToString();
                this.Rbl_Anamnesi_Congestione_Perif_Ascite.SelectedValue = reader["CongestionePerifAscite"].ToString();
                this.Rbl_Anamnesi_Polsi_Arter_Presenti.SelectedValue = reader["PolsiArterPresenti"].ToString();
                this.Txt_Anamnesi_Polsi_Arter_Non_Percepibili.Text = reader["PolsiArterNoPerc"].ToString();
                this.Rbl_Anamnesi_Polsi_Arter_Doppler.SelectedValue = reader["DocumentDoppler"].ToString();
                this.Cld_Aritmie_Ex_Sopraventricol.Text = Utility.DecodificaData(reader["ExtraSopraVentrDataRisc"].ToString());
                this.Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg.SelectedValue = reader["ExtraSopraVentrECG"].ToString();
                this.Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg_Holter.SelectedValue = reader["ExtraSopraVentrECGHolter"].ToString();
                this.Txt_Aritmie_Ex_Sopraventtricol_Terapia.Text = reader["ExtraSopraVentrTerapia"].ToString();
                this.Cld_Aritmie_Ex_Ventricol.Text = Utility.DecodificaData(reader["ExVentricolDataRisc"].ToString());
                this.Rbl_Aritmie_Ex_Ventricol_Document_Ecg.SelectedValue = reader["ExVentricolEcg"].ToString();
                this.Rbl_Aritmie_Ex_Ventricol_Document_Ecg_Holter.SelectedValue = reader["ExVentricolEcgHolter"].ToString();
                this.Txt_Aritmie_Ex_Ventricol_Semplici.Text = reader["ExVentricolSempliciN"].ToString();
                this.Txt_Aritmie_Ex_Ventricol_Coppie_Triplette.Text = reader["ExVentricolCoppieTripleN"].ToString();
                this.Txt_Aritmie_Ex_Ventricol_Tv_Non_Sostenuta.Text = reader["ExVentricolTVNonSostenutaN"].ToString();
                this.Txt_Aritmie_Ex_Ventricol_Terapia.Text = reader["ExVentricolTerapia"].ToString();
                this.Cld_Aritmie_Fa_Paross.Text = Utility.DecodificaData(reader["FaParadossDataInizio"].ToString());
                this.Txt_Aritmie_FA_Paradoss_Episodi.Text = reader["FAParadossNEpisodi"].ToString();
                this.Txt_Aritmie_FA_Paradoss_Durata_Episodi.Text = reader["FAParadossDurataEpisodi"].ToString();
                this.Rbl_Aritmie_Fa_Paradoss_Efficacia.SelectedValue = reader["FAParadossEffTerapia"].ToString();
                this.Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat.SelectedValue = reader["FAParadossAblazTrans"].ToString();
                this.Txt_Aritmie_FA_Paross_terapia.Text = reader["FAParadossTerapia"].ToString();
                this.Txt_Aritmie_Fa_Permanente_Terapia.Text = reader["FAPermanenteTerapia"].ToString();
                this.Cld_Aritmie_Fa_Permanente.Text = Utility.DecodificaData(reader["FAPermanenteDataInizio"].ToString());
                this.Rbl_Aritmie_Cve.SelectedValue = reader["CVE"].ToString();
                this.Rbl_Aritmie_Tv_Sostenuta.SelectedValue = reader["TVSostenutaSintomatica"].ToString();
                this.Cld_Aritmie_Tv_Sostenuta.Text = Utility.DecodificaData(reader["TVSostenutaSintomaticaData"].ToString());
                this.Txt_Aritmie_Tv_Sostenuta_Num_Episodi.Text = reader["TVSostenutaSintomaticaEpisodiN"].ToString();
                this.Txt_Aritmie_Tv_Sostenuta_Durata_Episodi.Text = reader["TVSostenutaSintomaticaDurata"].ToString();
                this.Txt_Aritmie_Tv_Sostenuta_Freq_Ventricol.Text = reader["TVSostenutaSintomaticaFreqVentr"].ToString();
                this.Txt_Aritmie_Tv_Sostenuta_Sincope.Text = reader["TVSostenutaSincope"].ToString();
                this.Txt_Aritmie_Tv_Sostenuta_Lipotimia.Text = reader["TVSostenutaSintomaticaLipotimia"].ToString();
                this.Rbl_Aritmia_Mi_Resuscitata.SelectedValue = reader["MIResuscitata"].ToString();
                this.Rbl_Aritmia_Pm.SelectedValue = reader["PM"].ToString();
                this.Txt_Aritmie_Pm_Causale_Impianto.Text = reader["PMCausaleImpianto"].ToString();
                this.Cld_Aritmie_Pm.Text = Utility.DecodificaData(reader["PMDataImpianto"].ToString());
                this.Txt_Aritmie_Pm_Tipologia.Text = reader["PMTipologia"].ToString();
                this.Txt_Aritmie_Causale_Impianto.Text = reader["ICDCausaleImpianto"].ToString();
                this.Cld_Aritmie_Icd.Text = Utility.DecodificaData(reader["ICDDataImpianto"].ToString());
                this.Txt_Aritmie_Icd_Scariche_Appropriate.Text = reader["ICDScaricheAppropriateN"].ToString();
                this.Txt_Aritmie_Icd_Scariche_Inappropriate.Text = reader["ICDScaricheInappropriate"].ToString();
                this.Rbl_Aritmia_Icd.SelectedValue = reader["ICD"].ToString();
                this.Txt_Aritmie_Pm_Biventr.Text = reader["PMBiventricol"].ToString();
                this.Cld_Aritmie_Data_Impianto.Text = Utility.DecodificaData(reader["PMBiventricolDataImpianto"].ToString());
                this.Ddl_Aritmia_Pm_Biventr.SelectedValue = reader["PMBiventricolBeneficiSint"].ToString();
                this.Txt_Aritmie_FE_Prima.Text = reader["PercFEPrima"].ToString();
                this.Txt_Aritmie_FE_A_6_Mesi.Text = reader["PerFESeiMesi"].ToString();
                this.Txt_Laboratorio_Bnp.Text = reader["IngrBNP"].ToString();
                this.Txt_Laboratorio_Troponina.Text = reader["IngrTroponina"].ToString();
                this.Txt_Laboratorio_Pcr.Text = reader["IngrPCR"].ToString();
                this.Txt_Laboratorio_Uricemia.Text = reader["IngrUricemia"].ToString();
                this.Txt_Laboratorio_Azot.Text = reader["IngrAzot"].ToString();
                this.Txt_Laboratorio_Creat.Text = reader["IngrCreat"].ToString();
                this.Txt_Laboratorio_Microalbumin.Text = reader["IngrMicroAlbumin"].ToString();
                this.Txt_Laboratorio_Na.Text = reader["IngrNA"].ToString();
                this.Txt_Laboratorio_K.Text = reader["IngrK"].ToString();
                this.Txt_Laboratorio_Ca.Text = reader["IngrCA"].ToString();
                this.Txt_Laboratorio_Ph.Text = reader["IngrPH"].ToString();
                this.Txt_Laboratorio_Mg.Text = reader["IngrMG"].ToString();
                this.Txt_Laboratorio_Got.Text = reader["IngrGOT"].ToString();
                this.Txt_Laboratorio_Gpt.Text = reader["IngrGPT"].ToString();
                this.Txt_Laboratorio_Cpk.Text = reader["IngrCPK"].ToString();
                this.Txt_Laboratorio_Gr.Text = reader["IngrGR"].ToString();
                this.Txt_Laboratorio_Emogl.Text = reader["IngrEmoglob"].ToString();
                this.Txt_Laboratorio_Gb.Text = reader["IngrGb"].ToString();
                this.Txt_Laboratorio_Sideremia.Text = reader["IngrSideremia"].ToString();
                this.Txt_Laboratorio_Glicemia.Text = reader["IngrGlicemia"].ToString();
                this.Txt_Laboratorio_Emogl_Glic.Text = reader["IngrEmoglGlic"].ToString();
                this.Txt_Laboratorio_Col_Tot.Text = reader["IngrColTot"].ToString();
                this.Txt_Laboratorio_Ldl.Text = reader["IngrLdl"].ToString();
                this.Txt_Laboratorio_Hdl.Text = reader["IngrHdl"].ToString();
                this.Txt_Laboratorio_Triglic.Text = reader["IngrTrigl"].ToString();
                this.Txt_Laboratorio_T3.Text = reader["IngrT3"].ToString();
                this.Txt_Laboratorio_T4.Text = reader["IngrT4"].ToString();
                this.Txt_Laboratorio_TSH.Text = reader["IngrTSH"].ToString();
                this.Txt_Laboratorio_Digitalemia.Text = reader["IngrDigitalemia"].ToString();
                this.Rbl_Consulenze_Nefrologica.SelectedValue = reader["ConsNefro"].ToString();
                this.Cld_Consulenze_Nefrologica.Text = Utility.DecodificaData(reader["ConsNefroData"].ToString());
                this.Rbl_Consulenze_Ematologica.SelectedValue = reader["ConsEmato"].ToString();
                this.Cld_Consulenze_Ematologica.Text = Utility.DecodificaData(reader["ConsEmatoData"].ToString());
                this.Rbl_Consulenze_Neuropsichiatra.SelectedValue = reader["ConsNeuro"].ToString();
                this.Cld_Consulenze_Neuropsichiatrica.Text = Utility.DecodificaData(reader["ConsNeuroData"].ToString());
                this.Rbl_Consulenze_Endocrinologica.SelectedValue = reader["ConsEndoCri"].ToString();
                this.Cld_Consulenze_Endocrinologica.Text = Utility.DecodificaData(reader["ConsEndoCriData"].ToString());
                this.Rbl_Consulenze_Pneumologica.SelectedValue = reader["ConsPneumo"].ToString();
                this.Cld_Consulenze_Pneumologica.Text = Utility.DecodificaData(reader["ConsPneumoData"].ToString());
                this.Rbl_Esami_Strumentali_Ritmo_Sinusale.SelectedValue = reader["ECGRitmoSin"].ToString();
                this.Txt_Esami_Strumentali_Ecg_Ingresso_Frequenza_Cardiaca.Text = reader["ECGFrequenza"].ToString();
                this.Rbl_Esami_Strumentali_Ex_Sopraventricol.SelectedValue = reader["ECGExSopraventr"].ToString();
                this.Ddl_Esami_Strumentali_Ecg_Ingresso_Ex_Ventricol.SelectedValue = reader["ECGExVentricolClasseLown"].ToString();
                this.Rbl_Esami_Strumentali_Tv.SelectedValue = reader["ECGTV"].ToString();
                this.Rbl_Esami_Strumentali_Fa.SelectedValue = reader["ECGFA"].ToString();
                this.Rbl_Esami_Strumentali_Danno_Atriale.SelectedValue = reader["ECGDannoAtrialeSx"].ToString();
                this.Rbl_Esami_Strumentali_Bbs.SelectedValue = reader["ECGBBS"].ToString();
                this.Rbl_Esami_Strumentali_Bbd.SelectedValue = reader["ECGBBD"].ToString();
                this.Rbl_Esami_Strumentali_Eas.SelectedValue = reader["ECGEAS"].ToString();
                this.Rbl_Esami_Strumentali_Eps.SelectedValue = reader["ECGEPS"].ToString();
                this.Rbl_Esami_Strumentali_Sovraccarico_Sistolico.SelectedValue = reader["ECGSovraSistolico"].ToString();
                this.Rbl_Esami_Strumentali_Ischemia.SelectedValue = reader["ECGIschemia"].ToString();
                this.Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx.SelectedValue = reader["ECGIpertVentrSx"].ToString();
                this.Rbl_Esami_Strumentali_Sovraccarico_Volume.SelectedValue = reader["ECGSovraVolume"].ToString();
                this.Ddl_Esami_Strumentali_Rx_Torace.SelectedValue = reader["RXKillip"].ToString();
                this.Rbl_Esami_Strumentali_Versamento_Pleurico.SelectedValue = reader["VersamentoPleurico"].ToString();
                this.Txt_Esami_Strumentali_Eco2d_Ipertrofia_Settale.Text = reader["Eco2DIpertrSet"].ToString();
                this.Txt_Esami_Strumentali_Eco2d_Ipertrofia_Concentrica.Text = reader["Eco2DIpertrConc"].ToString();
                this.Ddl_Esami_Strumentali_Eco2d_Ipocinesia.SelectedValue = reader["Eco2DIpocinesia"].ToString();
                this.Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare.Text = reader["Eco2DVolSistVentrSx"].ToString();
                this.Txt_Esami_Strumentali_Eco2d_Volume_Diastolico.Text = reader["Eco2DVolDiastolico"].ToString();
                this.Txt_Esami_Strumentali_Eco2d_Fe.Text = reader["Eco2DFE"].ToString();
                this.Rbl_Esami_Strumentali_Disincronizzazione.SelectedValue = reader["Eco2DDisincr"].ToString();
                this.Txt_Esami_Strumentali_Eco_Doppler_Descrizione.Text = reader["EcoDopplerDescrizione"].ToString();
                this.Ddl_Esami_Strumentali_Eco_Doppler_Disfunzione_Diastolica.SelectedValue = reader["EcoDopplerDisfDiast"].ToString();
                this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Mitralico.SelectedValue = reader["EcoDopplerRigurMitralico"].ToString();
                this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Aortico.SelectedValue = reader["EcoDopplerRigurAortico"].ToString();
                this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Tricuspidale.SelectedValue = reader["EcoDopplerRigurTricuspi"].ToString();
                this.Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare.Text = reader["EcoDopplerPressPolm"].ToString();
                this.Txt_Esami_Strumentali_Doppler_Arterioso_Arti_Inferiori.Text = reader["DopplerArtArtiInfDesc"].ToString();
                this.Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative.SelectedValue = reader["DopplerArtArtiInfSvenosi"].ToString();
                this.Txt_Esami_Strumentali_Doppler_Tea.Text = reader["DopplerTEADesc"].ToString();
                this.Rbl_Esami_Strumentali_Doppler_Tea_Stenosi_Significative.SelectedValue = reader["DopplerTEASvenosi"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale.SelectedValue = reader["ECGHRitmoSin"].ToString();
                this.Txt_Esami_Strumentali_Ecg_Holter_Frequenza_Cardiaca.Text = reader["ECGHFrequenza"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr.SelectedValue = reader["ECGHExSopraventr"].ToString();
                this.Ddl_Esami_Strumentali_Ecg_Holter_Ex_Ventricol.SelectedValue = reader["ECGHExVentricolClasseLow"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Tv.SelectedValue = reader["ECGHTV"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Fa.SelectedValue = reader["ECGHFA"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx.SelectedValue = reader["ECGHDannoAtrialeSx"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Bbs.SelectedValue = reader["ECGHBBS"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Bbd.SelectedValue = reader["ECGHBBD"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Eas.SelectedValue = reader["ECGHEAS"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Eps.SelectedValue = reader["ECGHEPS"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico.SelectedValue = reader["ECGHSovraSistolico"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Ischemia.SelectedValue = reader["ECGHIschemia"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare.SelectedValue = reader["ECGHIpertVentrSx"].ToString();
                this.Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume.SelectedValue = reader["ECGHSovraVolume"].ToString();
                this.Txt_Esami_Strumentali_Peso1.Text = reader["Peso1"].ToString();
                this.Rbl_Esami_Strumentali_Counceling.SelectedValue = reader["Counceling"].ToString();
                this.Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA.SelectedValue = Convert.ToInt32(reader["ClasseNYHA"].ToString()).ToString();
                this.Txt_Esami_Strumentali_Diagnosi.Text = reader["Diagnosi"].ToString();
                this.Ddl_Esami_Strumentali_BAV.SelectedValue = reader["BAV"].ToString();
                this.Txt_Numero_Ospedalizzazioni.Text = reader["OspedalizzazioniNumero"].ToString();
                this.Txt_Quest_Minnesota_Life.Text = reader["QuestMinnesotaLife"].ToString();
                this.Txt_Costo_Totale.Text = reader["CostoTotaleTerapia"].ToString();
                this.Cld_Data_Morte.Text = Utility.DecodificaData(reader["MorteData"].ToString());
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = true;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool estrai_reliquati()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("estrai_reliquati", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdQuadro", this.idquadro);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Reliquati_Motori_Ictus.Text = Utility.DecodificaData(reader["DataIctus"].ToString());
                this.Cld_Reliquati_Motori_TIA.Text = Utility.DecodificaData(reader["DataTIA"].ToString());
                this.Txt_Reliquati_Motori_Sincope_Episodi.Text = reader["NumSincope"].ToString();
                this.idreliquato = Convert.ToInt32(reader["IdReliquatoMotorio"].ToString());
                this.Txt_Quadro_Clinico_Patologia_Aortica.Text = reader["NomePatologiaAortica"].ToString();
                this.Cld_Reliquati_Motori_Data_Patologia_Aortica.Text = Utility.DecodificaData(reader["DataPatolAortica"].ToString());
                this.Txt_Reliquati_Motori__Document_Doppler.Text = reader["DopplerPatolAortica"].ToString();
                this.Txt_Reliquati_Motori_Document_TAC.Text = reader["TacPatolAortica"].ToString();
                this.Cld_Data_Reliquati_Motori_Vascolare_Periferica.Text = Utility.DecodificaData(reader["VascoPerifData"].ToString());
                this.Txt_Reliquati_Motori_Vascolare_Perif_Document_Doppler.Text = reader["VascoPerifDocDoppler"].ToString();
                this.Cld__Reliquati_Motori_Data_Chemioterapia.Text = Utility.DecodificaData(reader["ChemioData"].ToString());
                this.Txt_Reliquati_Motori_Farmaci.Text = reader["ChemioFarmaci"].ToString();
                this.Cld_Reliquati_Motori_Data_Radioter_Tor.Text = Utility.DecodificaData(reader["RadioData"].ToString());
                this.Rbl_Reliquati_Motori_Hiv.SelectedValue = reader["HIV"].ToString();
                this.Txt_Reliquati_Motori_Stupefacenti.Text = reader["Stupefacenti"].ToString();
                this.Txt_Reliquati_Motori_Altro.Text = reader["Altro"].ToString();
                this.Rbl_Reliquati_Motori_Epat_Hcv_Corr.SelectedValue = reader["EpatHCVCorr"].ToString();
                this.Rbl_Reliquati_Motori_Alcoolismo.SelectedValue = reader["Alcoolismo"].ToString();
                this.Cld_Reliquati_Motori_Ansieta.Text = Utility.DecodificaData(reader["DataAnsieta"].ToString());
                this.Ddl_Reliquati_Motori_Grado_Ansietà.SelectedValue = reader["LivelloAnsieta"].ToString();
                this.Txt_Reliquati_Motori_Ansieta_Terapia.Text = reader["TerapiaAnsieta"].ToString();
                this.Cld_Reliquati_Motori_Depressione.Text = Utility.DecodificaData(reader["Datadepr"].ToString());
                this.Ddl_Reliquati_Motori_Grado_Depressione.SelectedValue = reader["LivelloDepr"].ToString();
                this.Txt_Reliquati_Motori_Terapie_Depressione.Text = reader["TerapiaDepr"].ToString();
                this.Txt_Reliquati_Motori_Traumi_Psichici_Specifica.Text = reader["TraumiPsichi"].ToString();
                this.Ddl_Reliquati_Motori_Conflittualita_Famigliare.SelectedValue = reader["ConfFarm"].ToString();
                this.Ddl_Reliquati_Motori_Conflittualita_Lavorativa.SelectedValue = reader["ConfLav"].ToString();
                this.Ddl_Reliquati_Motori_Conflittualita_Sociale.SelectedValue = reader["ConfSoc"].ToString();
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = true;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool estrai_sincope()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("estrai_sincope", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdQuadroClinico", this.idquadro);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                for (int i = 1; reader.Read(); i++)
                {
                    switch (i)
                    {
                        case 1:
                            this.Cld1_Reliquati_Motori_Sincope.Text = Utility.DecodificaData(reader["data"].ToString());
                            this.Ddl1_Reliquati_Motori_Tipologia_Sincope.SelectedValue = reader["TipoSincope"].ToString();
                            break;

                        case 2:
                            this.Cld2_Reliquati_Motori_Sincope.Text = Utility.DecodificaData(reader["data"].ToString());
                            this.Ddl2_Reliquati_Motori_Tipologia_Sincope.SelectedValue = reader["TipoSincope"].ToString();
                            break;

                        case 3:
                            this.Cld3_Reliquati_Motori_Sincope.Text = Utility.DecodificaData(reader["data"].ToString());
                            this.Ddl3_Reliquati_Motori_Tipologia_Sincope.SelectedValue = reader["TipoSincope"].ToString();
                            break;
                    }
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = true;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool EstraiDati()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("cerca_paziente_id", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    this.lb_titolo.Text = this.lb_titolo.Text + reader["Cognome"].ToString() + " " + reader["Nome"].ToString();
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    protected void gv_farmaci_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.Cells[5].Controls.Count > 0)
        {
            if (e.Row.Cells[1].Text.Equals("Anticoagulanti"))
            {
                TextBox box = (TextBox)e.Row.Cells[5].Controls[1];
                box.Visible = false;
                RadioButtonList list = (RadioButtonList)e.Row.Cells[5].Controls[3];
                if (!this.idquadro.Equals(-1))
                {
                    list.SelectedValue = e.Row.Cells[2].Text;
                }
                this.lb_gv.Text = this.lb_gv.Text + "document.form1." + list.UniqueID + "[1].checked=true;";
            }
            else
            {
                RadioButtonList list2 = (RadioButtonList)e.Row.Cells[5].Controls[3];
                list2.Visible = false;
                TextBox box2 = (TextBox)e.Row.Cells[5].Controls[1];
                if (!this.idquadro.Equals(-1) && !e.Row.Cells[2].Text.Equals("&nbsp;"))
                {
                    box2.Text = e.Row.Cells[2].Text;
                }
                this.lb_gv.Text = this.lb_gv.Text + "document.form1." + box2.UniqueID + ".value='';";
            }
        }
        if (e.Row.Cells[6].Controls.Count > 0)
        {
            CheckBox box3 = (CheckBox)e.Row.Cells[6].Controls[1];
            if (e.Row.Cells[3].Text.Equals("S") && !this.idquadro.Equals(-1))
            {
                box3.Checked = true;
            }
            this.lb_gv.Text = this.lb_gv.Text + "document.form1." + box3.UniqueID + ".checked=false;";
        }
        if (e.Row.Cells[7].Controls.Count > 0)
        {
            TextBox box4 = (TextBox)e.Row.Cells[7].Controls[1];
            if (!this.idquadro.Equals(-1) && !e.Row.Cells[4].Text.Equals("&nbsp;"))
            {
                box4.Text = e.Row.Cells[4].Text;
            }
            this.lb_gv.Text = this.lb_gv.Text + "document.form1." + box4.UniqueID + ".value='';";
        }
        e.Row.Cells[2].Visible = false;
        e.Row.Cells[3].Visible = false;
        e.Row.Cells[4].Visible = false;
    }

    private bool inserisci_quadro()
    {
        string str;
        if (this.idquadro.Equals(-1))
        {
            str = "inserisci_quadro";
        }
        else
        {
            str = "modifica_quadro";
        }
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlTransaction transaction = connection.BeginTransaction();
            try
            {
                SqlCommand command = new SqlCommand(str, connection, transaction);
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.AddWithValue("@DataInizioScmpensoAttuale", Utility.CodificaData(this.Cld_Data_Anamnesi_Scompenso_Attuale.Text.Trim()));
                command.Parameters.AddWithValue("@EdPolAcuto", this.Rbl_Anamnesi_Ed_Pol_Acuto.SelectedValue);
                command.Parameters.AddWithValue("@ClasseDispneaSforzo", this.Ddl_Anamnesi_Dispnea_Sforzo.SelectedValue);
                command.Parameters.AddWithValue("@Astenia", this.ddl_Anamnesi_Astenia.SelectedValue);
                command.Parameters.AddWithValue("@NCuscini", this.Txt_Anamnesi_N_Cuscini.Text.Trim());
                command.Parameters.AddWithValue("@SleepApnea", this.Txt_Anamnesi_Sleep.Text.Trim());
                command.Parameters.AddWithValue("@ContrazDiuresi", this.RBL_Anamnesi_Contraz_Diuresi.SelectedValue.ToString());
                command.Parameters.AddWithValue("@RecRapidoIncrementoPeso", this.Txt_Anamnesi_Rapido_Increm_Peso.Text.Trim());
                command.Parameters.AddWithValue("@Cardiopalmo", this.Txt_Anamnesi_Cardiopalmo.Text.Trim());
                command.Parameters.AddWithValue("@PAingresso", this.Txt_Anamnesi_Pa_Ingresso.Text.Trim());
                command.Parameters.AddWithValue("@PADimiss", this.Txt_Anamnesi_Pa_Dimissione.Text.Trim());
                command.Parameters.AddWithValue("@FCIngresso", this.Txt_Anamnesi_Fc_Ingresso.Text.Trim());
                command.Parameters.AddWithValue("@FCDimissione", this.Txt_Anamnesi_Fc_Dimissione.Text.Trim());
                command.Parameters.AddWithValue("@PolsoDimissione", this.Txt_Anamnesi_Polso_Dimissione.Text.Trim());
                command.Parameters.AddWithValue("@PolsoIngresso", this.Txt_Anamnesi_Polso_Ingresso.Text.Trim());
                command.Parameters.AddWithValue("@PolsoIngrRitmo", this.Rbl_Anamnesi_Polso_Ingresso.SelectedValue.ToString());
                command.Parameters.AddWithValue("@PolsoDimRitmo", this.Rbl_Anamnesi_Polso_Dimissione.SelectedValue.ToString());
                command.Parameters.AddWithValue("@Cianosi", this.Rbl_Anamnesi_Cianosi.SelectedValue.ToString());
                command.Parameters.AddWithValue("@DescrizioneToniCardiaci", this.ddl_Anamnesi_Descrizione_Toni_Cardiaci.SelectedValue.ToString());
                command.Parameters.AddWithValue("@ToniValidiRidotti", this.Rbl_Anamnesi_Toni_Cardiaci.SelectedValue.ToString());
                command.Parameters.AddWithValue("@Soffi", this.Rbl_Anamnesi_Soffi.SelectedValue.ToString());
                command.Parameters.AddWithValue("@DescrizioneSoffi", this.Txt_Anamnesi_Soffi_Descrizione.Text.Trim());
                command.Parameters.AddWithValue("@CongestionePolmonare", this.Rbl_Anamnesi_Congestione_Polm.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifGiugularimag3", this.Rbl_Anamnesi_Congestione_Perif_Giugulari.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifEpatomegalia", this.Rbl_Anamnesi_Congestione_Perif_Epatomegalia.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifEdemiPeriferici", this.Ddl_Anamnesi_Edemi_Periferici.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifVersamentoPleurico", this.Rbl_Anamnesi_Congestione_Perif_Versamento_Pleurico.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifAscite", this.Rbl_Anamnesi_Congestione_Perif_Ascite.SelectedValue.ToString());
                command.Parameters.AddWithValue("@PolsiArterPresenti", this.Rbl_Anamnesi_Polsi_Arter_Presenti.SelectedValue.ToString());
                command.Parameters.AddWithValue("@PolsiArterNoPerc", this.Txt_Anamnesi_Polsi_Arter_Non_Percepibili.Text.Trim());
                command.Parameters.AddWithValue("@DocumentDoppler", this.Rbl_Anamnesi_Polsi_Arter_Doppler.SelectedValue.ToString());
                command.Parameters.AddWithValue("@ExtraSopraVentrDataRisc", Utility.CodificaData(this.Cld_Aritmie_Ex_Sopraventricol.Text.Trim()));
                command.Parameters.AddWithValue("@ExtraSopraVentrECG", this.Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg.SelectedValue.ToString());
                command.Parameters.AddWithValue("@ExtraSopraVentrECGHolter", this.Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg_Holter.SelectedValue.ToString());
                command.Parameters.AddWithValue("@ExtraSopraVentrTerapia", this.Txt_Aritmie_Ex_Sopraventtricol_Terapia.Text.Trim());
                command.Parameters.AddWithValue("@ExVentricolDataRisc", Utility.CodificaData(this.Cld_Aritmie_Ex_Ventricol.Text.Trim()));
                command.Parameters.AddWithValue("@ExVentricolECG", this.Rbl_Aritmie_Ex_Ventricol_Document_Ecg.SelectedValue);
                command.Parameters.AddWithValue("@ExVentricolECGHolter", this.Rbl_Aritmie_Ex_Ventricol_Document_Ecg_Holter.SelectedValue);
                command.Parameters.AddWithValue("@ExVentricolSempliciN", this.Txt_Aritmie_Ex_Ventricol_Semplici.Text.Trim());
                command.Parameters.AddWithValue("@ExVentricolCoppieTripleN", this.Txt_Aritmie_Ex_Ventricol_Coppie_Triplette.Text.Trim());
                command.Parameters.AddWithValue("@ExVentricolTVNonSostenutaN", this.Txt_Aritmie_Ex_Ventricol_Tv_Non_Sostenuta.Text.Trim());
                command.Parameters.AddWithValue("@ExVentricolTerapia", this.Txt_Aritmie_Ex_Ventricol_Terapia.Text.Trim());
                command.Parameters.AddWithValue("@FAParadossDataInizio", Utility.CodificaData(this.Cld_Aritmie_Fa_Paross.Text.Trim()));
                command.Parameters.AddWithValue("@FAParadossNEpisodi", this.Txt_Aritmie_FA_Paradoss_Episodi.Text.Trim());
                command.Parameters.AddWithValue("@FAParadossDurataEpisodi", this.Txt_Aritmie_FA_Paradoss_Durata_Episodi.Text.Trim());
                command.Parameters.AddWithValue("@FAParadossTerapia", this.Txt_Aritmie_FA_Paross_terapia.Text.Trim());
                command.Parameters.AddWithValue("@FAParadossEffTerapia", this.Rbl_Aritmie_Fa_Paradoss_Efficacia.SelectedValue);
                command.Parameters.AddWithValue("@FAParadossAblazTrans", this.Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat.SelectedValue);
                command.Parameters.AddWithValue("@FAPermanenteDataInizio", Utility.CodificaData(this.Cld_Aritmie_Fa_Permanente.Text.Trim()));
                command.Parameters.AddWithValue("@FAPermanenteTerapia", this.Txt_Aritmie_Fa_Permanente_Terapia.Text.Trim());
                command.Parameters.AddWithValue("@CVE", this.Rbl_Aritmie_Cve.SelectedValue);
                command.Parameters.AddWithValue("@TVSostenutaSintomatica", this.Rbl_Aritmie_Tv_Sostenuta.SelectedValue);
                command.Parameters.AddWithValue("@TVSostenutaSintomaticaData", Utility.CodificaData(this.Cld_Aritmie_Tv_Sostenuta.Text.Trim()));
                command.Parameters.AddWithValue("@TVSostenutaSintomaticaEpisodiN", this.Txt_Aritmie_Tv_Sostenuta_Num_Episodi.Text.Trim());
                command.Parameters.AddWithValue("@TVSostenutaSintomaticaDurata", this.Txt_Aritmie_Tv_Sostenuta_Durata_Episodi.Text.Trim());
                command.Parameters.AddWithValue("@TVSostenutaSintomaticaFreqVentr", this.Txt_Aritmie_Tv_Sostenuta_Freq_Ventricol.Text.Trim());
                command.Parameters.AddWithValue("@TVSostenutaSincope", this.Txt_Aritmie_Tv_Sostenuta_Sincope.Text.Trim());
                command.Parameters.AddWithValue("@TVSostenutaSintomaticaLipotimia", this.Txt_Aritmie_Tv_Sostenuta_Lipotimia.Text.Trim());
                command.Parameters.AddWithValue("@MIResuscitata", this.Rbl_Aritmia_Mi_Resuscitata.SelectedValue);
                command.Parameters.AddWithValue("@PM", this.Rbl_Aritmia_Pm.SelectedValue);
                command.Parameters.AddWithValue("@PMCausaleImpianto", this.Txt_Aritmie_Pm_Causale_Impianto.Text.Trim());
                command.Parameters.AddWithValue("@PMDataImpianto", Utility.CodificaData(this.Cld_Aritmie_Pm.Text.Trim()));
                command.Parameters.AddWithValue("@PMTipologia", this.Txt_Aritmie_Pm_Tipologia.Text.Trim());
                command.Parameters.AddWithValue("@ICD", this.Rbl_Aritmia_Icd.SelectedValue);
                command.Parameters.AddWithValue("@ICDCausaleImpianto", this.Txt_Aritmie_Causale_Impianto.Text.Trim());
                command.Parameters.AddWithValue("@ICDDataImpianto", Utility.CodificaData(this.Cld_Aritmie_Icd.Text.Trim()));
                command.Parameters.AddWithValue("@ICDScaricheAppropriateN", this.Txt_Aritmie_Icd_Scariche_Appropriate.Text.Trim());
                command.Parameters.AddWithValue("@ICDScaricheInappropriate", this.Txt_Aritmie_Icd_Scariche_Inappropriate.Text.Trim());
                command.Parameters.AddWithValue("@PMBiventricol", this.Txt_Aritmie_Pm_Biventr.Text.Trim());
                command.Parameters.AddWithValue("@PMBiventricolCausaleImp", this.Txt_Aritmie_Pm_Causale_Impianto.Text.Trim());
                command.Parameters.AddWithValue("@PMBiventricolDataImpianto", Utility.CodificaData(this.Cld_Aritmie_Data_Impianto.Text.Trim()));
                command.Parameters.AddWithValue("@PMBiventricolBeneficiSint", this.Ddl_Aritmia_Pm_Biventr.SelectedValue);
                command.Parameters.AddWithValue("@PercFEPrima", this.Txt_Aritmie_FE_Prima.Text.Trim());
                command.Parameters.AddWithValue("@PerFESeiMesi", this.Txt_Aritmie_FE_A_6_Mesi.Text.Trim());
                command.Parameters.AddWithValue("@IngrBNP", this.Txt_Laboratorio_Bnp.Text.Trim());
                command.Parameters.AddWithValue("@IngrTroponina", this.Txt_Laboratorio_Troponina.Text.Trim());
                command.Parameters.AddWithValue("@IngrPCR", this.Txt_Laboratorio_Pcr.Text.Trim());
                command.Parameters.AddWithValue("@IngrUricemia", this.Txt_Laboratorio_Uricemia.Text.Trim());
                command.Parameters.AddWithValue("@IngrAzot", this.Txt_Laboratorio_Azot.Text.Trim());
                command.Parameters.AddWithValue("@IngrCreat", this.Txt_Laboratorio_Creat.Text.Trim());
                command.Parameters.AddWithValue("@IngrMicroAlbumin", this.Txt_Laboratorio_Microalbumin.Text.Trim());
                command.Parameters.AddWithValue("@IngrNA", this.Txt_Laboratorio_Na.Text.Trim());
                command.Parameters.AddWithValue("@IngrK", this.Txt_Laboratorio_K.Text.Trim());
                command.Parameters.AddWithValue("@IngrCA", this.Txt_Laboratorio_Ca.Text.Trim());
                command.Parameters.AddWithValue("@IngrPH", this.Txt_Laboratorio_Ph.Text.Trim());
                command.Parameters.AddWithValue("@IngrMG", this.Txt_Laboratorio_Mg.Text.Trim());
                command.Parameters.AddWithValue("@IngrGOT", this.Txt_Laboratorio_Got.Text.Trim());
                command.Parameters.AddWithValue("@IngrGPT", this.Txt_Laboratorio_Gpt.Text.Trim());
                command.Parameters.AddWithValue("@IngrCPK", this.Txt_Laboratorio_Cpk.Text.Trim());
                command.Parameters.AddWithValue("@IngrGR", this.Txt_Laboratorio_Gr.Text.Trim());
                command.Parameters.AddWithValue("@IngrEmoglob", this.Txt_Laboratorio_Emogl.Text.Trim());
                command.Parameters.AddWithValue("@IngrGB", this.Txt_Laboratorio_Gb.Text.Trim());
                command.Parameters.AddWithValue("@IngrSideremia", this.Txt_Laboratorio_Sideremia.Text.Trim());
                command.Parameters.AddWithValue("@IngrGlicemia", this.Txt_Laboratorio_Glicemia.Text.Trim());
                command.Parameters.AddWithValue("@IngrEmoglGlic", this.Txt_Laboratorio_Emogl_Glic.Text.Trim());
                command.Parameters.AddWithValue("@IngrColTot", this.Txt_Laboratorio_Col_Tot.Text.Trim());
                command.Parameters.AddWithValue("@IngrHDL", this.Txt_Laboratorio_Hdl.Text.Trim());
                command.Parameters.AddWithValue("@IngrLDL", this.Txt_Laboratorio_Ldl.Text.Trim());
                command.Parameters.AddWithValue("@IngrTrigl", this.Txt_Laboratorio_Triglic.Text.Trim());
                command.Parameters.AddWithValue("@IngrT3", this.Txt_Laboratorio_T3.Text.Trim());
                command.Parameters.AddWithValue("@IngrT4", this.Txt_Laboratorio_T4.Text.Trim());
                command.Parameters.AddWithValue("@IngrTSH", this.Txt_Laboratorio_TSH.Text.Trim());
                command.Parameters.AddWithValue("@IngrDigitalemia", this.Txt_Laboratorio_Digitalemia.Text.Trim());
                command.Parameters.AddWithValue("@ECGRitmoSin", this.Rbl_Esami_Strumentali_Ritmo_Sinusale.SelectedValue);
                command.Parameters.AddWithValue("@ECGFrequenza", this.Txt_Esami_Strumentali_Ecg_Ingresso_Frequenza_Cardiaca.Text.Trim());
                command.Parameters.AddWithValue("@ECGExSopraventr", this.Rbl_Esami_Strumentali_Ex_Sopraventricol.SelectedValue);
                command.Parameters.AddWithValue("@ECGExVentricolClasseLown", this.Ddl_Esami_Strumentali_Ecg_Ingresso_Ex_Ventricol.SelectedValue);
                command.Parameters.AddWithValue("@ECGTV", this.Rbl_Esami_Strumentali_Tv.SelectedValue);
                command.Parameters.AddWithValue("@ECGFA", this.Rbl_Esami_Strumentali_Fa.SelectedValue);
                command.Parameters.AddWithValue("@ECGDannoAtrialeSx", this.Rbl_Esami_Strumentali_Danno_Atriale.SelectedValue);
                command.Parameters.AddWithValue("@ECGBBS", this.Rbl_Esami_Strumentali_Bbs.SelectedValue);
                command.Parameters.AddWithValue("@ECGBBD", this.Rbl_Esami_Strumentali_Bbd.SelectedValue);
                command.Parameters.AddWithValue("@ECGEAS", this.Rbl_Esami_Strumentali_Eas.SelectedValue);
                command.Parameters.AddWithValue("@ECGEPS", this.Rbl_Esami_Strumentali_Eps.SelectedValue);
                command.Parameters.AddWithValue("@ECGSovraSistolico", this.Rbl_Esami_Strumentali_Sovraccarico_Sistolico.SelectedValue);
                command.Parameters.AddWithValue("@ECGIschemia", this.Rbl_Esami_Strumentali_Ischemia.SelectedValue);
                command.Parameters.AddWithValue("@ECGIpertVentrSx", this.Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx.SelectedValue);
                command.Parameters.AddWithValue("@ECGSovraVolume", this.Rbl_Esami_Strumentali_Sovraccarico_Volume.SelectedValue);
                command.Parameters.AddWithValue("@RXKillip", this.Ddl_Esami_Strumentali_Rx_Torace.SelectedValue);
                command.Parameters.AddWithValue("@VersamentoPleurico", this.Rbl_Esami_Strumentali_Versamento_Pleurico.SelectedValue);
                command.Parameters.AddWithValue("@Eco2DIpertrSet", this.Txt_Esami_Strumentali_Eco2d_Ipertrofia_Settale.Text.Trim());
                command.Parameters.AddWithValue("@Eco2DIpertrConc", this.Txt_Esami_Strumentali_Eco2d_Ipertrofia_Concentrica.Text.Trim());
                command.Parameters.AddWithValue("@Eco2DIpocinesia", this.Ddl_Esami_Strumentali_Eco2d_Ipocinesia.SelectedValue);
                command.Parameters.AddWithValue("@Eco2DVolSistVentrSx", this.Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare.Text.Trim());
                command.Parameters.AddWithValue("@Eco2DVolDiastolico", this.Txt_Esami_Strumentali_Eco2d_Volume_Diastolico.Text.Trim());
                command.Parameters.AddWithValue("@Eco2DFE", this.Txt_Esami_Strumentali_Eco2d_Fe.Text.Trim());
                command.Parameters.AddWithValue("@Eco2DDisincr", this.Rbl_Esami_Strumentali_Disincronizzazione.SelectedValue);
                command.Parameters.AddWithValue("@EcoDopplerDescrizione", this.Txt_Esami_Strumentali_Eco_Doppler_Descrizione.Text.Trim());
                command.Parameters.AddWithValue("@EcoDopplerDisfDiast", this.Ddl_Esami_Strumentali_Eco_Doppler_Disfunzione_Diastolica.SelectedValue);
                command.Parameters.AddWithValue("@EcoDopplerRigurMitralico", this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Mitralico.SelectedValue);
                command.Parameters.AddWithValue("@EcoDopplerRigurAortico", this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Aortico.SelectedValue);
                command.Parameters.AddWithValue("@EcoDopplerRigurTricuspi", this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Tricuspidale.SelectedValue);
                command.Parameters.AddWithValue("@EcoDopplerPressPolm", this.Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare.Text.Trim());
                command.Parameters.AddWithValue("@DopplerArtArtiInfDesc", this.Txt_Esami_Strumentali_Doppler_Arterioso_Arti_Inferiori.Text.Trim());
                command.Parameters.AddWithValue("@DopplerArtArtiInfSvenosi", this.Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative.SelectedValue);
                command.Parameters.AddWithValue("@DopplerTEADesc", this.Txt_Esami_Strumentali_Doppler_Tea.Text.Trim());
                command.Parameters.AddWithValue("@DopplerTEASvenosi", this.Rbl_Esami_Strumentali_Doppler_Tea_Stenosi_Significative.SelectedValue);
                command.Parameters.AddWithValue("@Diagnosi", this.Txt_Esami_Strumentali_Diagnosi.Text.Trim());
                command.Parameters.AddWithValue("@ClasseNYHA", this.Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA.SelectedValue);
                command.Parameters.AddWithValue("@Peso1", this.Txt_Esami_Strumentali_Peso1.Text.Trim());
                command.Parameters.AddWithValue("@Counceling", this.Rbl_Esami_Strumentali_Counceling.SelectedValue);
                command.Parameters.AddWithValue("@ECGHRitmoSin", this.Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale.SelectedValue);
                command.Parameters.AddWithValue("@ECGHFrequenza", this.Txt_Esami_Strumentali_Ecg_Holter_Frequenza_Cardiaca.Text.Trim());
                command.Parameters.AddWithValue("@ECGHExSopraventr", this.Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr.SelectedValue);
                command.Parameters.AddWithValue("@ECGHExVentricolClasseLow", this.Ddl_Esami_Strumentali_Ecg_Holter_Ex_Ventricol.SelectedValue);
                command.Parameters.AddWithValue("@ECGHTV", this.Rbl_Esami_Strumentali_Ecg_Holter_Tv.SelectedValue);
                command.Parameters.AddWithValue("@ECGHFA", this.Rbl_Esami_Strumentali_Ecg_Holter_Fa.SelectedValue);
                command.Parameters.AddWithValue("@ECGHDannoAtrialeSx", this.Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx.SelectedValue);
                command.Parameters.AddWithValue("@ECGHBBS", this.Rbl_Esami_Strumentali_Ecg_Holter_Bbs.SelectedValue);
                command.Parameters.AddWithValue("@ECGHBBD", this.Rbl_Esami_Strumentali_Ecg_Holter_Bbd.SelectedValue);
                command.Parameters.AddWithValue("@ECGHEAS", this.Rbl_Esami_Strumentali_Ecg_Holter_Eas.SelectedValue);
                command.Parameters.AddWithValue("@ECGHEPS", this.Rbl_Esami_Strumentali_Ecg_Holter_Eps.SelectedValue);
                command.Parameters.AddWithValue("@ECGHSovraSistolico", this.Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico.SelectedValue);
                command.Parameters.AddWithValue("@ECGHIschemia", this.Rbl_Esami_Strumentali_Ecg_Holter_Ischemia.SelectedValue);
                command.Parameters.AddWithValue("ECGHIpertVentrSx", this.Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare.SelectedValue);
                command.Parameters.AddWithValue("@ECGHSovraVolume", this.Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume.SelectedValue);
                command.Parameters.AddWithValue("@ConsNefro", this.Rbl_Consulenze_Nefrologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsNefroData", Utility.CodificaData(this.Cld_Consulenze_Nefrologica.Text.Trim()));
                command.Parameters.AddWithValue("@ConsEmato", this.Rbl_Consulenze_Ematologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsEmatoData", Utility.CodificaData(this.Cld_Consulenze_Ematologica.Text.Trim()));
                command.Parameters.AddWithValue("@ConsNeuro", this.Rbl_Consulenze_Neuropsichiatra.SelectedValue);
                command.Parameters.AddWithValue("@ConsNeuroData", Utility.CodificaData(this.Cld_Consulenze_Neuropsichiatrica.Text.Trim()));
                command.Parameters.AddWithValue("@ConsEndoCri", this.Rbl_Consulenze_Endocrinologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsEndoCriData", Utility.CodificaData(this.Cld_Consulenze_Endocrinologica.Text.Trim()));
                command.Parameters.AddWithValue("@ConsPneumo", this.Rbl_Consulenze_Pneumologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsPneumoData", Utility.CodificaData(this.Cld_Consulenze_Pneumologica.Text.Trim()));
                command.Parameters.AddWithValue("@Obesita", this.Quadro_Clinico_Obesita.SelectedValue);
                command.Parameters.AddWithValue("@Sovrappeso", this.Quadro_Clinico_Sovrappeso.SelectedValue);
                command.Parameters.AddWithValue("@ObesitaUnifAddom", this.Rbl_Quadro_Clinico_Obesita_Add.SelectedValue);
                command.Parameters.AddWithValue("@ObesitaUnifAddomData", Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Obesita_Add.Text.Trim()));
                command.Parameters.AddWithValue("@Iperglicemia", this.Rbl_Quadro_Clinico_Iperglicemia.SelectedValue);
                command.Parameters.AddWithValue("@IperglicemiaData", Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Iperglicemia.Text.Trim()));
                command.Parameters.AddWithValue("@IpertArt", this.Rbl_Quadro_Clinico_Ipertensione_Arteriosa.SelectedValue);
                command.Parameters.AddWithValue("@IpertArtData", Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Ipertensione_Arteriosa.Text.Trim()));
                command.Parameters.AddWithValue("@IperDislitemia", this.Rbl_Quadro_Clinico_Iperslidemia.SelectedValue);
                command.Parameters.AddWithValue("@IperDislitemiaData", Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Iperdislipidemia.Text.Trim()));
                command.Parameters.AddWithValue("@BPCO", this.Rbl_Quadro_Clinico_BPCO.SelectedValue);
                command.Parameters.AddWithValue("@BPCOData", Utility.CodificaData(this.Cld_Quadro_Clinico_Data_BPCO.Text.Trim()));
                command.Parameters.AddWithValue("@AsmaBronchiale", this.Rbl_Quadro_Clinico_Asma_Bronchiale.SelectedValue);
                command.Parameters.AddWithValue("@AsmaBronchialeData", Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Asma_Bronchiale.Text.Trim()));
                command.Parameters.AddWithValue("@InsuffRenale", this.Rbl_Quadro_Clinico_Insufficienza_Renale.SelectedValue);
                command.Parameters.AddWithValue("@InsuffRenaleData", Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Insufficienza_Renale.Text.Trim()));
                command.Parameters.AddWithValue("@Anemia", this.Rbl_Quadro_Clinico_Anemia.SelectedValue);
                command.Parameters.AddWithValue("@AnemiaData", Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Anemia.Text.Trim()));
                command.Parameters.AddWithValue("@IpertiroidInizio", Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Inizio_Ipertiroid.Text.Trim()));
                command.Parameters.AddWithValue("@IpertiroTerapia", this.Txt_Quadro_Clinico_Ipertiroid_Terapia.Text.Trim());
                command.Parameters.AddWithValue("@IpertiroNormInd", this.Rbl_Ipertiroid_Normalizz.SelectedValue);
                command.Parameters.AddWithValue("@IpotirodInizio", Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Inizio_Ipotiroid.Text.Trim()));
                command.Parameters.AddWithValue("@IpotiroTerapia", this.Txt_Quadro_Clinico_Ipotiroid_Terapia.Text.Trim());
                command.Parameters.AddWithValue("@IpotiroNormInd", this.Rbl_Quadro_Clinico_Ipotiroid_Normalizzaz_Indici_Lab.SelectedValue);
                command.Parameters.AddWithValue("@NEpisodiDispnea", this.Txt_Anamnesi_Dispnea_Nott.Text.Trim());
                command.Parameters.AddWithValue("@DataQuadroClinico", Utility.CodificaData(this.Cld_Quadro_Clinico_Generale.Text.Trim()));
                command.Parameters.AddWithValue("@MorteData", Utility.CodificaData(this.Cld_Data_Morte.Text.Trim()));
                command.Parameters.AddWithValue("@OspedalizzazioniNumero", this.Txt_Numero_Ospedalizzazioni.Text.Trim());
                command.Parameters.AddWithValue("@QuestMinnesotaLife", this.Txt_Quest_Minnesota_Life.Text.Trim());
                command.Parameters.AddWithValue("@CostoTotaleTerapia", this.Txt_Costo_Totale.Text.Trim());
                command.Parameters.AddWithValue("@BAV", this.Ddl_Esami_Strumentali_BAV.SelectedValue);
                if (this.Session["IdUtente"] != null)
                {
                    command.Parameters.AddWithValue("@IdUtente", this.Session["IdUtente"].ToString());
                }
                else
                {
                    command.Parameters.AddWithValue("@IdUtente", 0);
                }
                if (this.idquadro.Equals(-1))
                {
                    command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
                }
                else
                {
                    command.Parameters.AddWithValue("@IdQuadro", this.idquadro);
                }
                command.ExecuteNonQuery();
                if (this.idquadro.Equals(-1))
                {
                    SqlCommand command2 = new SqlCommand("ultimo_quadro", connection, transaction);
                    command2.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = command2.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        this.IdUltimoQuadro = Convert.ToInt32(reader["ultimoquadro"]);
                        reader.Close();
                        this.ApportaModificheFarmaci(transaction, connection, this.IdUltimoQuadro);
                    }
                }
                else
                {
                    this.ApportaModificheFarmaci(transaction, connection, this.idquadro);
                }
                this.trova_id_dispnea(this.idquadro);
                this.ApportaModificheDispnea(transaction, connection, this.Cld_Data_Anamnesi_Dispnea_Nott, this.IdDispnea1);
                this.ApportaModificheDispnea(transaction, connection, this.Cld2_Data_Anamnesi_Dispnea_Nott, this.IdDispnea2);
                this.ApportaModificheDispnea(transaction, connection, this.Cld3_Data_Anamnesi_Dispnea_Nott, this.IdDispnea3);
                this.ApportaModificheReliquati(transaction, connection);
                this.trova_id_sincope(this.idquadro);
                this.ApportaModificheSincope(transaction, connection, this.Cld1_Reliquati_Motori_Sincope, this.Ddl1_Reliquati_Motori_Tipologia_Sincope, this.IdSincope1);
                this.ApportaModificheSincope(transaction, connection, this.Cld2_Reliquati_Motori_Sincope, this.Ddl2_Reliquati_Motori_Tipologia_Sincope, this.IdSincope2);
                this.ApportaModificheSincope(transaction, connection, this.Cld3_Reliquati_Motori_Sincope, this.Ddl3_Reliquati_Motori_Tipologia_Sincope, this.IdSincope3);
                transaction.Commit();
                return true;
            }
            catch (Exception exception)
            {
                transaction.Rollback();
                this.lb_mess.Text = exception.Message;
                return false;
            }
            finally
            {
                connection.Close();
            }
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message;
        }
        return false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Response.Redirect("nuovopaziente.aspx?idPaziente=" + Request.Params["idpaziente"]);
        if (Session["Utente"] == null) { Response.Redirect("login.aspx"); }
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        if (((base.Request.Params["idquadro"] != null) && Utility.IsSignInteger(base.Request.Params["idquadro"].ToString())) && ((base.Request.Params["idpaziente"] != null) && Utility.IsSignInteger(base.Request.Params["idpaziente"].ToString())))
        {
            this.idquadro = Convert.ToInt32(base.Request.Params["idquadro"]);
            this.IdPaziente = Convert.ToInt32(base.Request.Params["idpaziente"]);
        }
        else
        {
            base.Response.Redirect("homepage.aspx");
        }
        this.lb_mess.Text = "";
        if (!base.IsPostBack)
        {
            if (this.idquadro.Equals("-1"))
            {
                this.lb_titolo.Text = "Inserimento nuovo quadro clinico";
                this.lb_menu.Text = "";
            }
            else
            {
                this.lb_menu.Text = string.Concat(new object[] { "<li><a onclick=\"return confirm('Sei sicuro di voler eliminare il quadro clinico corrente?');\" href=\"quadriclinici.aspx?idpaziente=", this.IdPaziente, "&del=", this.idquadro, "\">Elimina Quadro<br />Clinico</a></li>" });
            }
            if ((base.Request.Params["mod"] != null) && base.Request.Params["mod"].ToString().Equals("0"))
            {
                this.lb_mess.Text = "Il nuovo paziente \x00e8 stato inserito correttamente.";
            }
            this.estrai_NYHA(this.Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA);
            this.estrai_quadro();
            this.EstraiDati();
            this.estrai_Dispnea_Notturna();
            this.estrai_reliquati();
            this.estrai_sincope();
            this.ElencaFarmaci();
        }
        this.CreaMenuStep(this.Wizard1.ActiveStep.ID);
    }

    private bool trova_id_dispnea(int idquadro)
    {
        bool flag = false;
        int num = 1;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Dispnea", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Idquadro", idquadro);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                switch (num)
                {
                    case 1:
                        this.IdDispnea1 = Convert.ToInt32(reader["IdDispneaParosNott"].ToString());
                        break;

                    case 2:
                        this.IdDispnea2 = Convert.ToInt32(reader["IdDispneaParosNott"].ToString());
                        break;

                    case 3:
                        this.IdDispnea3 = Convert.ToInt32(reader["IdDispneaParosNott"].ToString());
                        break;
                }
                num++;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
            flag = false;
        }
        finally
        {
            connection.Close();
            flag = true;
        }
        return flag;
    }

    private bool trova_id_sincope(int idquadro)
    {
        bool flag = false;
        int num = 1;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("estrai_sincope", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdQuadroClinico", idquadro);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                switch (num)
                {
                    case 1:
                        this.IdSincope1 = Convert.ToInt32(reader["IdSincope"].ToString());
                        break;

                    case 2:
                        this.IdSincope2 = Convert.ToInt32(reader["IdSincope"].ToString());
                        break;

                    case 3:
                        this.IdSincope3 = Convert.ToInt32(reader["IdSincope"].ToString());
                        break;
                }
                num++;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
            flag = false;
        }
        finally
        {
            connection.Close();
            flag = true;
        }
        return flag;
    }

    protected void Wizard1_ActiveStepChanged(object sender, EventArgs e)
    {
        this.CreaMenuStep(this.Wizard1.ActiveStep.ID);
        double num = 0.0;
        if (this.Wizard1.ActiveStep.ID == "WizardStep9")
        {
            foreach (GridViewRow row in this.gv_farmaci.Rows)
            {
                if (row.Cells[5].Controls.Count <= 0)
                {
                    continue;
                }
                TextBox box = (TextBox)row.Cells[5].Controls[1];
                if (row.Cells[1].Text.Equals("Anticoagulanti"))
                {
                    RadioButtonList list = (RadioButtonList)row.Cells[5].Controls[3];
                    if (list.SelectedIndex.Equals(0))
                    {
                        box.Text = "1";
                    }
                    else
                    {
                        box.Text = "";
                    }
                }
                if (!box.Text.Trim().Equals(""))
                {
                    if (Utility.IsDouble(box.Text.Trim()))
                    {
                        if (row.Cells[7].Controls.Count <= 0)
                        {
                            continue;
                        }
                        TextBox box2 = (TextBox)row.Cells[7].Controls[1];
                        if (!box2.Text.Trim().Equals(""))
                        {
                            if (Utility.IsDouble(box2.Text.Trim()))
                            {
                                num += Convert.ToDouble(box2.Text.Trim());
                                continue;
                            }
                            this.Wizard1.ActiveStepIndex = 7;
                            this.lb_mess.Text = "Occorre inserire i costi in formato numerico.";
                        }
                        else
                        {
                            this.Wizard1.ActiveStepIndex = 7;
                            this.lb_mess.Text = "Occorre inserire i costi corrispondenti alle quantit\x00e0 inserite.";
                        }
                    }
                    else
                    {
                        this.Wizard1.ActiveStepIndex = 7;
                        this.lb_mess.Text = "Occorre inserire le quantit\x00e0 in formato numerico.";
                    }
                    break;
                }
                if (row.Cells[7].Controls.Count > 0)
                {
                    TextBox box3 = (TextBox)row.Cells[7].Controls[1];
                    if (box3.Text.Trim().Equals(""))
                    {
                        continue;
                    }
                    this.Wizard1.ActiveStepIndex = 7;
                    this.lb_mess.Text = "Occorre inserire le quantit\x00e0 corrispondenti ai costi inseriti.";
                    break;
                }
            }
        }
        this.Txt_Costo_Totale.Text = num.ToString();
    }

    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        if (this.inserisci_quadro())
        {
            if (this.idquadro.Equals(-1))
            {
                base.Response.Redirect(string.Concat(new object[] { "quadriclinici.aspx?idpaziente=", this.IdPaziente, "&idquadro", this.IdUltimoQuadro, "&mod=3" }));
            }
            else
            {
                base.Response.Redirect(string.Concat(new object[] { "quadriclinici.aspx?idpaziente=", this.IdPaziente, "&idquadro", this.idquadro, "&mod=1" }));
            }
        }
    }

    
}

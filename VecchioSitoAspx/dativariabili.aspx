<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dativariabili.aspx.cs" Inherits="dativariabili" %>

<%@ Register Src="datipaziente.ascx" TagName="datipaziente" TagPrefix="uc1" %>
<!--#include file="include/controllo.inc"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="stile.css" type="text/css" rel="stylesheet" />
<script language="JavaScript" src="./js/ts_picker.js"></script>
<script language="JavaScript" src="./js/controlli.js"></script>
<title>Gestione Quadri Clinici</title>

<style type="text/css">
<!--
body {
	background-image: url(./files/bg_blueGrad.gif);
	background-color: #324671;
}
.Stile1 {color: #336DD0}
-->
</style>
</head>

<body>
   <form id="form1" runat="server">
<table width="812" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td width="10" valign="top" background="./files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center"><div align="left">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
              <!--#include file="include/pazienti.inc"-->

              <div class="modernbricksmenuline">
                <ul>
                    <li style="margin-left: 1px"><a href="nuovopaziente.aspx?IdPaziente=-1">Nuovo<br />Reclutamento</a></li>
                    <li><a href="cercapaziente.aspx">Cerca<br />Paziente</a></li>
                    <li><a href="nuovopaziente.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>">Scheda<br />Paziente</a></li>
                    <li><a onclick="return confirm('Sei sicuro di voler eliminare il paziente corrente?');" href="cercapaziente.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>">Elimina<br />Paziente</a></li>
                    <li><a href="quadriclinici.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>">Controlli<br />Effettuati</a></li>
                    <li><a href="dativariabili.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>&idquadro=-1">
                        Nuovo <br />Controllo</a></li>
                    <asp:Label ID="lb_menu" runat="server" Text=""></asp:Label>
                </ul>
              </div>
          </td>
        </tr>
        <tr>
            <td align="center">
                <br />
                <br />
                <span style="font-size:18px;color:#324671;font-weight:bold"><asp:Label ID="lb_titolo" runat="server" Text=""></asp:Label></span>
                <br />
                <br />
                <uc1:datipaziente ID="Datipaziente1" runat="server" />
            </td>
          </tr>
          <tr>
            <td align="center">
                <br />
                <br />
                I campi contrassegnati con * sono obbligatori
                <br /><br />
                <asp:Label ID="lb_steps" runat="server" Text=""></asp:Label>
            </td>
          </tr>
          <tr>
            <td align="center">
                <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="5" 
                    OnFinishButtonClick="Wizard1_FinishButtonClick" BackColor="#EFF3FB" 
                    BorderColor="#B5C7DE" BorderWidth="1px" Font-Names="Verdana" Font-Size="12px" 
                    OnActiveStepChanged="Wizard1_ActiveStepChanged" CancelButtonText="Annulla" 
                    FinishCompleteButtonText="Fine" FinishPreviousButtonText="Indietro" 
                    StartNextButtonText="Avanti" StepNextButtonText="Avanti" 
                    StepPreviousButtonText="Indietro" DisplaySideBar="False">
                    <WizardSteps>
                        <asp:WizardStep ID="WizardStep1" runat="server" Title="Step 1">
                            <div class="titolo_step">Step 1</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">Step 1<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Quadro_Clinico_Generale.value='';document.form1.Wizard1$Quadro_Clinico_Obesita[0].checked=true;document.form1.Wizard1$Quadro_Clinico_Obesita[1].checked=false;document.form1.Wizard1$Quadro_Clinico_Obesita[2].checked=false;document.form1.Wizard1$Quadro_Clinico_Sovrappeso[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="top">
                                        Data osservazione *</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Generale" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Generale.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Generale" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Generale', document.form1.Wizard1$Cld_Quadro_Clinico_Generale.value, 'calendar1');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar1" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Obesit� (BMI &gt; 30)</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Quadro_Clinico_Obesita" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                            <asp:ListItem>Uniforme</asp:ListItem>
                                            <asp:ListItem>Addominale</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Sovrappeso
                                        <br />
                                        25 &lt; BMI &lt; 30</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Quadro_Clinico_Sovrappeso" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">SINDROME METABOLICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Quadro_Clinico_Obesita_Add[1].checked=true;document.form1.Wizard1$Cld_Quadro_Clinico_Data_Obesita_Add.value='';document.form1.Wizard1$Rbl_Quadro_Clinico_Iperglicemia[1].checked=true;document.form1.Wizard1$Cld_Quadro_Clinico_Data_Iperglicemia.value='';document.form1.Wizard1$Rbl_Quadro_Clinico_Ipertensione_Arteriosa[1].checked=true;document.form1.Wizard1$Cld_Quadro_Clinico_Data_Ipertensione_Arteriosa.value='';document.form1.Wizard1$Rbl_Quadro_Clinico_Iperslidemia[1].checked=true;document.form1.Wizard1$Cld_Quadro_Clinico_Data_Iperdislipidemia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Obesit� addominale<br />
                                        (&gt;94 cm. M - &gt; 80 cm. F)</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Quadro_Clinico_Obesita_Add" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        <asp:CustomValidator ID="CustomValidator2" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Data_Obesita_Add" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Data_Obesita_Add.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Data_Obesita_Add" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Data_Obesita_Add', document.form1.Wizard1$Cld_Quadro_Clinico_Data_Obesita_Add.value, 'calendar2');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar2" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Iperglicemia</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Quadro_Clinico_Iperglicemia" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        <asp:CustomValidator ID="CustomValidator3" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Data_Iperglicemia" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Data_Iperglicemia.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Data_Iperglicemia" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Data_Iperglicemia', document.form1.Wizard1$Cld_Quadro_Clinico_Data_Iperglicemia.value, 'calendar3');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar3" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ipertensione arteriosa</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Quadro_Clinico_Ipertensione_Arteriosa" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        <asp:CustomValidator ID="CustomValidator4" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Data_Ipertensione_Arteriosa" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Data_Ipertensione_Arteriosa.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Data_Ipertensione_Arteriosa" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Data_Ipertensione_Arteriosa', document.form1.Wizard1$Cld_Quadro_Clinico_Data_Ipertensione_Arteriosa.value, 'calendar4');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar4" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Iperdislipidemia</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Quadro_Clinico_Iperslidemia" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        <asp:CustomValidator ID="CustomValidator5" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Data_Iperdislipidemia" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Data_Iperdislipidemia.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Data_Iperdislipidemia" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Data_Iperdislipidemia', document.form1.Wizard1$Cld_Quadro_Clinico_Data_Iperdislipidemia.value, 'calendar5');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar5" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">BPCO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Quadro_Clinico_BPCO[1].checked=true;document.form1.Wizard1$Cld_Quadro_Clinico_Data_BPCO.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        BPCO
                                    </td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Quadro_Clinico_BPCO" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        <asp:CustomValidator ID="CustomValidator6" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Data_BPCO" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Data_BPCO.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Data_BPCO" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Data_BPCO', document.form1.Wizard1$Cld_Quadro_Clinico_Data_BPCO.value, 'calendar6');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar6" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ASMA BRONCHIALE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Quadro_Clinico_Asma_Bronchiale[1].checked=true;document.form1.Wizard1$Cld_Quadro_Clinico_Data_Asma_Bronchiale.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Asma bronchiale</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Quadro_Clinico_Asma_Bronchiale" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        <asp:CustomValidator ID="CustomValidator7" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Data_Asma_Bronchiale" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Data_Asma_Bronchiale.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Data_Asma_Bronchiale" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Data_Asma_Bronchiale', document.form1.Wizard1$Cld_Quadro_Clinico_Data_Asma_Bronchiale.value, 'calendar7');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar7" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">INSUFFICIENZA RENALE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Quadro_Clinico_Insufficienza_Renale[0].checked=false;document.form1.Wizard1$Rbl_Quadro_Clinico_Insufficienza_Renale[1].checked=false;document.form1.Wizard1$Rbl_Quadro_Clinico_Insufficienza_Renale[2].checked=false;document.form1.Wizard1$Cld_Quadro_Clinico_Data_Insufficienza_Renale.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td valign="top" align="right">
                                        Creatininemia</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Quadro_Clinico_Insufficienza_Renale" runat="server" RepeatDirection="Vertical">
                                            <asp:ListItem Value="1">1,1 mg/dl &lt; Creatininemia &lt;= 2,5 mg/dl</asp:ListItem>
                                            <asp:ListItem Value="2">2,5 mg/dl &lt; Creatininemia &lt;= 3,4 mg/dl</asp:ListItem>
                                            <asp:ListItem Value="3">Creatininemia &gt; 3,4 mg/dl</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        <asp:CustomValidator ID="CustomValidator8" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Data_Insufficienza_Renale" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Data_Insufficienza_Renale.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Data_Insufficienza_Renale" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Data_Insufficienza_Renale', document.form1.Wizard1$Cld_Quadro_Clinico_Data_Insufficienza_Renale.value, 'calendar8');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar8" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ANEMIA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Quadro_Clinico_Anemia[1].checked=true;document.form1.Wizard1$Cld_Quadro_Clinico_Data_Anemia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        (emogl. &lt; 11 g/dl M - &lt; 10 g/dl F)</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Quadro_Clinico_Anemia" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        <asp:CustomValidator ID="CustomValidator9" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Data_Anemia" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Data_Anemia.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Data_Anemia" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Data_Anemia', document.form1.Wizard1$Cld_Quadro_Clinico_Data_Anemia.value, 'calendar9');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar9" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">IPERTIROID.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Ipertiroid_Normalizz[1].checked=true;document.form1.Wizard1$Cld_Quadro_Clinico_Data_Inizio_Ipertiroid.value='';document.form1.Wizard1$Txt_Quadro_Clinico_Ipertiroid_Terapia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data Inizio</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        <asp:CustomValidator ID="CustomValidator10" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Data_Inizio_Ipertiroid" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Data_Inizio_Ipertiroid.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Data_Inizio_Ipertiroid" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Data_Inizio_Ipertiroid', document.form1.Wizard1$Cld_Quadro_Clinico_Data_Inizio_Ipertiroid.value, 'calendar10');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar10" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Terapia</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Quadro_Clinico_Ipertiroid_Terapia" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Normalizzazione indici lab.</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Ipertiroid_Normalizz" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">IPOTIROID.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Quadro_Clinico_Ipotiroid_Normalizzaz_Indici_Lab[1].checked=true;document.form1.Wizard1$Cld_Quadro_Clinico_Data_Inizio_Ipotiroid.value='';document.form1.Wizard1$Txt_Quadro_Clinico_Ipotiroid_Terapia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data inizio</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        <asp:CustomValidator ID="CustomValidator11" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Quadro_Clinico_Data_Inizio_Ipotiroid" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Quadro_Clinico_Data_Inizio_Ipotiroid.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Quadro_Clinico_Data_Inizio_Ipotiroid" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Quadro_Clinico_Data_Inizio_Ipotiroid', document.form1.Wizard1$Cld_Quadro_Clinico_Data_Inizio_Ipotiroid.value, 'calendar11');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar11" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Terapia</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Quadro_Clinico_Ipotiroid_Terapia" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Normalizzazione indici lab.</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Quadro_Clinico_Ipotiroid_Normalizzaz_Indici_Lab" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep2" runat="server" Title="Anamnesi">
                            <div class="titolo_step">ANAMNESI E QUADRO CLINICO</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DATA INIZIO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Data_Anamnesi_Scompenso_Attuale.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data inizio scompenso attuale</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator12" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Data_Anamnesi_Scompenso_Attuale" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Data_Anamnesi_Scompenso_Attuale.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Data_Anamnesi_Scompenso_Attuale" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Data_Anamnesi_Scompenso_Attuale', document.form1.Wizard1$Cld_Data_Anamnesi_Scompenso_Attuale.value, 'calendar12');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar12" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ED. POL. ACUTO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Ed_Pol_Acuto[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        ED. POL. ACUTO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Ed_Pol_Acuto" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DISPNEA PAROSS. NOTT.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Dispnea_Nott.value='';document.form1.Wizard1$Cld_Data_Anamnesi_Dispnea_Nott.value='';document.form1.Wizard1$Cld2_Data_Anamnesi_Dispnea_Nott.value='';document.form1.Wizard1$Cld3_Data_Anamnesi_Dispnea_Nott.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        N� casi</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Dispnea_Nott" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data 1</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator13" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Data_Anamnesi_Dispnea_Nott" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Data_Anamnesi_Dispnea_Nott.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Data_Anamnesi_Dispnea_Nott" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Data_Anamnesi_Dispnea_Nott', document.form1.Wizard1$Cld_Data_Anamnesi_Dispnea_Nott.value, 'calendar13');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar13" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data 2</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator14" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld2_Data_Anamnesi_Dispnea_Nott" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld2_Data_Anamnesi_Dispnea_Nott.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld2_Data_Anamnesi_Dispnea_Nott" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld2_Data_Anamnesi_Dispnea_Nott', document.form1.Wizard1$Cld2_Data_Anamnesi_Dispnea_Nott.value, 'calendar14');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar14" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data 3</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator15" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld3_Data_Anamnesi_Dispnea_Nott" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld3_Data_Anamnesi_Dispnea_Nott.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld3_Data_Anamnesi_Dispnea_Nott" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld3_Data_Anamnesi_Dispnea_Nott', document.form1.Wizard1$Cld3_Data_Anamnesi_Dispnea_Nott.value, 'calendar15');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar15" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DISPNEA DA SFORZO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Ddl_Anamnesi_Dispnea_Sforzo.selectedIndex=0;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        DISPNEA DA SFORZO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Anamnesi_Dispnea_Sforzo" runat="server">
                                            <asp:ListItem Value=""></asp:ListItem>
                                            <asp:ListItem Value="II classe">II classe</asp:ListItem>
                                            <asp:ListItem Value="III classe">III classe</asp:ListItem>
                                            <asp:ListItem Value="IV classe">IV classe</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ASTENIA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$ddl_Anamnesi_Astenia.selectedIndex=0;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        ASTENIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddl_Anamnesi_Astenia" runat="server">
                                            <asp:ListItem Value=""></asp:ListItem>
                                            <asp:ListItem Value="II classe">II classe</asp:ListItem>
                                            <asp:ListItem Value="III classe">III classe</asp:ListItem>
                                            <asp:ListItem Value="IV classe">IV classe</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ORTOPNEA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_N_Cuscini.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        N� cuscini</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_N_Cuscini" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">SLEEP APNEA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Sleep.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        SLEEP APNEA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Sleep" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CONTRAZ. DIURESI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$RBL_Anamnesi_Contraz_Diuresi.selectedIndex=0;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CONTRAZ. DIURESI</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="RBL_Anamnesi_Contraz_Diuresi" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Lieve</asp:ListItem>
                                            <asp:ListItem>Moderata</asp:ListItem>
                                            <asp:ListItem>Severa</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">RECENTE RAPIDO INCREM. PESO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Rapido_Increm_Peso.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        RECENTE RAPIDO INCREM. PESO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Rapido_Increm_Peso" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CARDIOPALMO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Cardiopalmo.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        CARDIOPALMO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Cardiopalmo" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">PA INGRESSO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Pa_Ingresso.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PA INGRESSO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Pa_Ingresso" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">PA DIMISSIONE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Pa_Dimissione.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PA DIMISSIONE</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Pa_Dimissione" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">FC INGRESSO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Fc_Ingresso.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        FC INGRESSO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Fc_Ingresso" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">FC DIMISSIONE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Fc_Dimissione.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        FC DIMISSIONE</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Fc_Dimissione" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">POLSO INGRESSO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Polso_Ingresso.value='';document.form1.Wizard1$Rbl_Anamnesi_Polso_Ingresso[0].checked=false;document.form1.Wizard1$Rbl_Anamnesi_Polso_Ingresso[1].checked=false;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        POLSO INGRESSO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Polso_Ingresso" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Tipologia ritmica</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Polso_Ingresso" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Ritmico</asp:ListItem>
                                            <asp:ListItem>Aritmico</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">POLSO DIMISSIONE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Anamnesi_Polso_Dimissione.value='';document.form1.Wizard1$Rbl_Anamnesi_Polso_Dimissione[0].checked=false;document.form1.Wizard1$Rbl_Anamnesi_Polso_Dimissione[1].checked=false;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        POLSO DIMISSIONE</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Polso_Dimissione" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Tipologia ritmica</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Polso_Dimissione" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Ritmico</asp:ListItem>
                                            <asp:ListItem>Aritmico</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CIANOSI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Cianosi[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CIANOSI</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Cianosi" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">TONI CARDIACI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Toni_Cardiaci[1].checked=true;document.form1.Wizard1$ddl_Anamnesi_Descrizione_Toni_Cardiaci.selectedIndex=0;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TONI CARDIACI</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Toni_Cardiaci" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True">Validi</asp:ListItem>
                                            <asp:ListItem>Ridotti</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Descrizione toni cardiaci</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddl_Anamnesi_Descrizione_Toni_Cardiaci" runat="server">
                                            <asp:ListItem Value=""></asp:ListItem>
                                            <asp:ListItem Value="I tono ridotto">I tono ridotto</asp:ListItem>
                                            <asp:ListItem Value="III tono">III tono</asp:ListItem>
                                            <asp:ListItem Value="IV tono">IV tono</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">SOFFI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Soffi[1].checked=true;document.form1.Wizard1$Txt_Anamnesi_Soffi_Descrizione.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        SOFFI</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Soffi" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Descrizione</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Soffi_Descrizione" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                        </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CONGESTIONE POLM.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Congestione_Polm.selectedIndex=0;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">CONGESTIONE POLM.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Rbl_Anamnesi_Congestione_Polm" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Lieve</asp:ListItem>
                                            <asp:ListItem>Moderata</asp:ListItem>
                                            <asp:ListItem>Severa</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CONGEST. PERIF.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Giugulari[1].checked=true;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Epatomegalia[1].checked=true;document.form1.Wizard1$Ddl_Anamnesi_Edemi_Periferici.selectedIndex=0;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Versamento_Pleurico[1].checked=true;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Ascite[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Giugulari</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Congestione_Perif_Giugulari" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Epatomegalia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Congestione_Perif_Epatomegalia" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Edemi periferici</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Anamnesi_Edemi_Periferici" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Versamento pleurico</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Congestione_Perif_Versamento_Pleurico" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ascite</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Congestione_Perif_Ascite" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">POLSI ARTER.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Polsi_Arter_Presenti[1].checked=true;document.form1.Wizard1$Txt_Anamnesi_Polsi_Arter_Non_Percepibili.value='';document.form1.Wizard1$Rbl_Anamnesi_Polsi_Arter_Doppler[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Presenti</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Polsi_Arter_Presenti" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Non percepibili (precisare)</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Anamnesi_Polsi_Arter_Non_Percepibili" runat="server" MaxLength="255" Columns="40" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Document. Doppler</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Anamnesi_Polsi_Arter_Doppler" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep3" runat="server" Title="Rel. Mot.">
                            <div class="titolo_step">RELIQUATI MOTORI</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ICTUS<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Reliquati_Motori_Ictus.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator16" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Reliquati_Motori_Ictus" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Reliquati_Motori_Ictus.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Reliquati_Motori_Ictus" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Reliquati_Motori_Ictus', document.form1.Wizard1$Cld_Reliquati_Motori_Ictus.value, 'calendar16');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar16" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">TIA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Reliquati_Motori_TIA.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator17" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Reliquati_Motori_TIA" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Reliquati_Motori_TIA.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Reliquati_Motori_TIA" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Reliquati_Motori_TIA', document.form1.Wizard1$Cld_Reliquati_Motori_TIA.value, 'calendar17');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar17" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">SINCOPE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Reliquati_Motori_Sincope_Episodi.value='';document.form1.Wizard1$Cld1_Reliquati_Motori_Sincope.value='';document.form1.Wizard1$Ddl1_Reliquati_Motori_Tipologia_Sincope.selectedIndex=0;document.form1.Wizard1$Cld2_Reliquati_Motori_Sincope.value='';document.form1.Wizard1$Ddl2_Reliquati_Motori_Tipologia_Sincope.selectedIndex=0;document.form1.Wizard1$Cld3_Reliquati_Motori_Sincope.value='';document.form1.Wizard1$Ddl3_Reliquati_Motori_Tipologia_Sincope.selectedIndex=0;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        N. Episodi</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Reliquati_Motori_Sincope_Episodi" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data 1</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator18" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld1_Reliquati_Motori_Sincope" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld1_Reliquati_Motori_Sincope.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld1_Reliquati_Motori_Sincope" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld1_Reliquati_Motori_Sincope', document.form1.Wizard1$Cld1_Reliquati_Motori_Sincope.value, 'calendar18');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar18" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Tipologia 1</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl1_Reliquati_Motori_Tipologia_Sincope" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Cardiaca</asp:ListItem>
                                            <asp:ListItem>Neurologica</asp:ListItem>
                                            <asp:ListItem>Psichiatrica</asp:ListItem>
                                            <asp:ListItem>Vasovagale</asp:ListItem>
                                            <asp:ListItem>Ez. indeterminata</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data 2</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator19" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld2_Reliquati_Motori_Sincope" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld2_Reliquati_Motori_Sincope.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld2_Reliquati_Motori_Sincope" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld2_Reliquati_Motori_Sincope', document.form1.Wizard1$Cld2_Reliquati_Motori_Sincope.value, 'calendar19');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar19" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Tipologia 2</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl2_Reliquati_Motori_Tipologia_Sincope" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Cardiaca</asp:ListItem>
                                            <asp:ListItem>Neurologica</asp:ListItem>
                                            <asp:ListItem>Psichiatrica</asp:ListItem>
                                            <asp:ListItem>Vasovagale</asp:ListItem>
                                            <asp:ListItem>Ez. indeterminata</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data 3</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator20" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld3_Reliquati_Motori_Sincope" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld3_Reliquati_Motori_Sincope.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld3_Reliquati_Motori_Sincope" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld3_Reliquati_Motori_Sincope', document.form1.Wizard1$Cld3_Reliquati_Motori_Sincope.value, 'calendar20');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar20" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Tipologia 3</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl3_Reliquati_Motori_Tipologia_Sincope" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Cardiaca</asp:ListItem>
                                            <asp:ListItem>Neurologica</asp:ListItem>
                                            <asp:ListItem>Psichiatrica</asp:ListItem>
                                            <asp:ListItem>Vasovagale</asp:ListItem>
                                            <asp:ListItem>Ez. indeterminata</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">PATOL. AORTICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Quadro_Clinico_Patologia_Aortica.value='';document.form1.Wizard1$Cld_Reliquati_Motori_Data_Patologia_Aortica.value='';document.form1.Wizard1$Txt_Reliquati_Motori__Document_Doppler.value='';document.form1.Wizard1$Txt_Reliquati_Motori_Document_TAC.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Specificare</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Quadro_Clinico_Patologia_Aortica" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data primo riscontro</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator21" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Reliquati_Motori_Data_Patologia_Aortica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Reliquati_Motori_Data_Patologia_Aortica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Reliquati_Motori_Data_Patologia_Aortica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Reliquati_Motori_Data_Patologia_Aortica', document.form1.Wizard1$Cld_Reliquati_Motori_Data_Patologia_Aortica.value, 'calendar21');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar21" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. Doppler</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Reliquati_Motori__Document_Doppler" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. TAC</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Reliquati_Motori_Document_TAC" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">VASCOL. PERIF.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Data_Reliquati_Motori_Vascolare_Periferica.value='';document.form1.Wizard1$Txt_Reliquati_Motori_Vascolare_Perif_Document_Doppler.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data primo riscontro</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator22" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Data_Reliquati_Motori_Vascolare_Periferica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Data_Reliquati_Motori_Vascolare_Periferica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Data_Reliquati_Motori_Vascolare_Periferica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Data_Reliquati_Motori_Vascolare_Periferica', document.form1.Wizard1$Cld_Data_Reliquati_Motori_Vascolare_Periferica.value, 'calendar22');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar22" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Document. Doppler</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Reliquati_Motori_Vascolare_Perif_Document_Doppler" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CHEMIOTERAPIA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld__Reliquati_Motori_Data_Chemioterapia.value='';document.form1.Wizard1$Txt_Reliquati_Motori_Farmaci.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data trattamento</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator23" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld__Reliquati_Motori_Data_Chemioterapia" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld__Reliquati_Motori_Data_Chemioterapia.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld__Reliquati_Motori_Data_Chemioterapia" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld__Reliquati_Motori_Data_Chemioterapia', document.form1.Wizard1$Cld__Reliquati_Motori_Data_Chemioterapia.value, 'calendar23');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar23" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Farmaci</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Reliquati_Motori_Farmaci" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">RADIOTERAPIA TOR. (CR)<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Reliquati_Motori_Data_Radioter_Tor.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data trattamento</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator24" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Reliquati_Motori_Data_Radioter_Tor" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Reliquati_Motori_Data_Radioter_Tor.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Reliquati_Motori_Data_Radioter_Tor" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Reliquati_Motori_Data_Radioter_Tor', document.form1.Wizard1$Cld_Reliquati_Motori_Data_Radioter_Tor.value, 'calendar24');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar24" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">HIV<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Reliquati_Motori_Hiv[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        HIV</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Reliquati_Motori_Hiv" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td colspan="3" class="titolo_sezione">EPAT. HCV CORR.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Reliquati_Motori_Epat_Hcv_Corr[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        EPAT. HCV CORR.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Reliquati_Motori_Epat_Hcv_Corr" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ALCOOLISMO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Reliquati_Motori_Alcoolismo[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        ALCOOLISMO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Reliquati_Motori_Alcoolismo" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">STUPEFACENTI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Reliquati_Motori_Stupefacenti.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        STUPEFACENTI</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Reliquati_Motori_Stupefacenti" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ALTRO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Reliquati_Motori_Altro.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        ALTRO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Reliquati_Motori_Altro" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ANSIET&Agrave;<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Reliquati_Motori_Ansieta.value='';document.form1.Wizard1$Ddl_Reliquati_Motori_Grado_Ansiet�.selectedIndex=0;document.form1.Wizard1$Txt_Reliquati_Motori_Ansieta_Terapia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data inizio</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator25" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Reliquati_Motori_Ansieta" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Reliquati_Motori_Ansieta.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Reliquati_Motori_Ansieta" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Reliquati_Motori_Ansieta', document.form1.Wizard1$Cld_Reliquati_Motori_Ansieta.value, 'calendar25');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar25" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Grado</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Reliquati_Motori_Grado_Ansiet&#224;" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Terapia in atto</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Reliquati_Motori_Ansieta_Terapia" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DEPRESSIONE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Reliquati_Motori_Depressione.value='';document.form1.Wizard1$Ddl_Reliquati_Motori_Grado_Depressione.selectedIndex=0;document.form1.Wizard1$Txt_Reliquati_Motori_Terapie_Depressione.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data inizio</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator26" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Reliquati_Motori_Depressione" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Reliquati_Motori_Depressione.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Reliquati_Motori_Depressione" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Reliquati_Motori_Depressione', document.form1.Wizard1$Cld_Reliquati_Motori_Depressione.value, 'calendar26');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar26" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Grado</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Reliquati_Motori_Grado_Depressione" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Terapia in atto</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Reliquati_Motori_Terapie_Depressione" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">TRAUMI PSICHICI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Reliquati_Motori_Traumi_Psichici_Specifica.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Precedenti inizio scompenso</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Reliquati_Motori_Traumi_Psichici_Specifica" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CONFLITTUALIT&Agrave;<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Ddl_Reliquati_Motori_Conflittualita_Famigliare.selectedIndex=0;document.form1.Wizard1$Ddl_Reliquati_Motori_Conflittualita_Lavorativa.selectedIndex=0;document.form1.Wizard1$Ddl_Reliquati_Motori_Conflittualita_Sociale.selectedIndex=0;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Famigliare</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Reliquati_Motori_Conflittualita_Famigliare" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Lavorativa</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Reliquati_Motori_Conflittualita_Lavorativa" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Sociale</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Reliquati_Motori_Conflittualita_Sociale" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep4" runat="server" Title="Aritmie">
                            <div class="titolo_step">ARITMIE</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">EX. SOPRAVENTRICOL.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Aritmie_Ex_Sopraventricol.value='';document.form1.Wizard1$Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg[1].checked=true;document.form1.Wizard1$Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg_Holter[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Ex_Sopraventtricol_Terapia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data primo riscontro</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator27" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Aritmie_Ex_Sopraventricol" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Ex_Sopraventricol.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Aritmie_Ex_Sopraventricol" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Ex_Sopraventricol', document.form1.Wizard1$Cld_Aritmie_Ex_Sopraventricol.value, 'calendar27');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar27" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Document. ECG</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Document. ECG Holter</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg_Holter" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Terapia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Ex_Sopraventtricol_Terapia" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">EX. VENTRICOL.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Aritmie_Ex_Ventricol.value='';document.form1.Wizard1$Rbl_Aritmie_Ex_Ventricol_Document_Ecg[1].checked=true;document.form1.Wizard1$Rbl_Aritmie_Ex_Ventricol_Document_Ecg_Holter[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Ex_Ventricol_Semplici.value='';document.form1.Wizard1$Txt_Aritmie_Ex_Ventricol_Coppie_Triplette.value='';document.form1.Wizard1$Txt_Aritmie_Ex_Ventricol_Tv_Non_Sostenuta.value='';document.form1.Wizard1$Txt_Aritmie_Ex_Ventricol_Terapia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data primo riscontro</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator28" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Aritmie_Ex_Ventricol" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Ex_Ventricol.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Aritmie_Ex_Ventricol" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Ex_Ventricol', document.form1.Wizard1$Cld_Aritmie_Ex_Ventricol.value, 'calendar28');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar28" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Document. ECG</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmie_Ex_Ventricol_Document_Ecg" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Document ECG Holter</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmie_Ex_Ventricol_Document_Ecg_Holter" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Semplici</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox ID="Txt_Aritmie_Ex_Ventricol_Semplici" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Coppie e triplette</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox ID="Txt_Aritmie_Ex_Ventricol_Coppie_Triplette" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TV non sostenuta</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox ID="Txt_Aritmie_Ex_Ventricol_Tv_Non_Sostenuta" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Terapia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Ex_Ventricol_Terapia" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">FA PAROSS.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Aritmie_Fa_Paross.value='';document.form1.Wizard1$Txt_Aritmie_FA_Paradoss_Episodi.value='';document.form1.Wizard1$Txt_Aritmie_FA_Paradoss_Durata_Episodi.value='';document.form1.Wizard1$Txt_Aritmie_FA_Paross_terapia.value='';document.form1.Wizard1$Rbl_Aritmie_Fa_Paradoss_Efficacia[0].checked=false;document.form1.Wizard1$Rbl_Aritmie_Fa_Paradoss_Efficacia[1].checked=false;document.form1.Wizard1$Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat[0].checked=false;document.form1.Wizard1$Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat[1].checked=false;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data inizio</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator29" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Aritmie_Fa_Paross" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Fa_Paross.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Aritmie_Fa_Paross" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Fa_Paross', document.form1.Wizard1$Cld_Aritmie_Fa_Paross.value, 'calendar29');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar29" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        N. Episodi</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_FA_Paradoss_Episodi" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Durata episodi</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_FA_Paradoss_Durata_Episodi" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Terapia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_FA_Paross_terapia" runat="server" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Efficacia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmie_Fa_Paradoss_Efficacia" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Efficace</asp:ListItem>
                                            <asp:ListItem>Non efficace</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ablazione transcat.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem Selected="True">Efficace</asp:ListItem>
                                            <asp:ListItem>Non efficace</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">FA PERMANENTE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Aritmie_Fa_Permanente.value='';document.form1.Wizard1$Txt_Aritmie_Fa_Permanente_Terapia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data inizio</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator30" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Aritmie_Fa_Permanente" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Fa_Permanente.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Aritmie_Fa_Permanente" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Fa_Permanente', document.form1.Wizard1$Cld_Aritmie_Fa_Permanente.value, 'calendar30');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar30" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Terapia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Fa_Permanente_Terapia" runat="server" TextMode="MultiLine" MaxLength="255" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">CVE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Aritmie_Cve[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CVE</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmie_Cve" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">TV SOSTENUTA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Aritmie_Tv_Sostenuta[0].checked=false;document.form1.Wizard1$Rbl_Aritmie_Tv_Sostenuta[1].checked=false;document.form1.Wizard1$Cld_Aritmie_Tv_Sostenuta.value='';document.form1.Wizard1$Txt_Aritmie_Tv_Sostenuta_Num_Episodi.value='';document.form1.Wizard1$Txt_Aritmie_Tv_Sostenuta_Durata_Episodi.value='';document.form1.Wizard1$Txt_Aritmie_Tv_Sostenuta_Freq_Ventricol.value='';document.form1.Wizard1$Txt_Aritmie_Tv_Sostenuta_Sincope.value='';document.form1.Wizard1$Txt_Aritmie_Tv_Sostenuta_Lipotimia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TV SOSTENUTA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmie_Tv_Sostenuta" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="Sintomatica">Sintomatica</asp:ListItem>
                                            <asp:ListItem Value="Asintomatica">Asintomatica</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data prima osservazione</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator31" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Aritmie_Tv_Sostenuta" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Tv_Sostenuta.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Aritmie_Tv_Sostenuta" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Tv_Sostenuta', document.form1.Wizard1$Cld_Aritmie_Tv_Sostenuta.value, 'calendar31');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar31" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        N. episodi</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Tv_Sostenuta_Num_Episodi" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Durata episodi</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Tv_Sostenuta_Durata_Episodi" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Freq. ventricolare</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Tv_Sostenuta_Freq_Ventricol" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Sincope</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Tv_Sostenuta_Sincope" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Lipotimia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Tv_Sostenuta_Lipotimia" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">MI RESUSCITATA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Aritmia_Mi_Resuscitata[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        MI RESUSCITATA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmia_Mi_Resuscitata" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">PM<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Aritmia_Pm[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Pm_Causale_Impianto.value='';document.form1.Wizard1$Cld_Aritmie_Pm.value='';document.form1.Wizard1$Txt_Aritmie_Pm_Tipologia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PM</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmia_Pm" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Pm_Causale_Impianto" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data Impianto</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator32" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Aritmie_Pm" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Pm.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Aritmie_Pm" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Pm', document.form1.Wizard1$Cld_Aritmie_Pm.value, 'calendar32');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar32" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Tipologia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Pm_Tipologia" runat="server" MaxLength="255" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ICD<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Aritmia_Icd[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Causale_Impianto.value='';document.form1.Wizard1$Cld_Aritmie_Icd.value='';document.form1.Wizard1$Txt_Aritmie_Icd_Scariche_Appropriate.value='';document.form1.Wizard1$Txt_Aritmie_Icd_Scariche_Inappropriate.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        ICD</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Aritmia_Icd" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Causale_Impianto" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data impianto</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator33" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Aritmie_Icd" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Icd.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Aritmie_Icd" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Icd', document.form1.Wizard1$Cld_Aritmie_Icd.value, 'calendar33');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar33" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Scariche appropriate</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox ID="Txt_Aritmie_Icd_Scariche_Appropriate" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Scariche inappropriate</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox ID="Txt_Aritmie_Icd_Scariche_Inappropriate" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                
                                    <td colspan="3" class="titolo_sezione">PM BIVENTR.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Aritmie_Pm_Biventr.value='';document.form1.Wizard1$Cld_Aritmie_Data_Impianto.value='';document.form1.Wizard1$Ddl_Aritmia_Pm_Biventr.selectedIndex=0;document.form1.Wizard1$Txt_Aritmie_FE_Prima.value='';document.form1.Wizard1$Txt_Aritmie_FE_A_6_Mesi.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_Pm_Biventr" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data impianto</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator34" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Aritmie_Data_Impianto" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Aritmie_Data_Impianto.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Aritmie_Data_Impianto" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Aritmie_Data_Impianto', document.form1.Wizard1$Cld_Aritmie_Data_Impianto.value, 'calendar34');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar34" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Benefici sintomatol.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Aritmia_Pm_Biventr" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>-</asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        FE prima</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                   
                                    </td>
                                    <td align="left">
                                    
                                        <asp:TextBox ID="Txt_Aritmie_FE_Prima" runat="server" MaxLength="3" Columns="3"></asp:TextBox>
                                        %</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        FE a 6 mesi</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Aritmie_FE_A_6_Mesi" runat="server" MaxLength="3" Columns="3"></asp:TextBox>
                                        %</td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep5" runat="server" Title="Lab.">
                            <div class="titolo_step">LABORATORIO</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DATI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Laboratorio_Bnp.value='';document.form1.Wizard1$Txt_Laboratorio_Troponina.value='';document.form1.Wizard1$Txt_Laboratorio_Pcr.value='';document.form1.Wizard1$Txt_Laboratorio_Uricemia.value='';document.form1.Wizard1$Txt_Laboratorio_Azot.value='';document.form1.Wizard1$Txt_Laboratorio_Creat.value='';document.form1.Wizard1$Txt_Laboratorio_Microalbumin.value='';document.form1.Wizard1$Txt_Laboratorio_Na.value='';document.form1.Wizard1$Txt_Laboratorio_K.value='';document.form1.Wizard1$Txt_Laboratorio_Ca.value='';document.form1.Wizard1$Txt_Laboratorio_Ph.value='';document.form1.Wizard1$Txt_Laboratorio_Mg.value='';document.form1.Wizard1$Txt_Laboratorio_Got.value='';document.form1.Wizard1$Txt_Laboratorio_Gpt.value='';document.form1.Wizard1$Txt_Laboratorio_Cpk.value='';document.form1.Wizard1$Txt_Laboratorio_Gr.value='';document.form1.Wizard1$Txt_Laboratorio_Emogl.value='';document.form1.Wizard1$Txt_Laboratorio_Gb.value='';document.form1.Wizard1$Txt_Laboratorio_Sideremia.value='';document.form1.Wizard1$Txt_Laboratorio_Glicemia.value='';document.form1.Wizard1$Txt_Laboratorio_Emogl_Glic.value='';document.form1.Wizard1$Txt_Laboratorio_Col_Tot.value='';document.form1.Wizard1$Txt_Laboratorio_Ldl.value='';document.form1.Wizard1$Txt_Laboratorio_Hdl.value='';document.form1.Wizard1$Txt_Laboratorio_Triglic.value='';document.form1.Wizard1$Txt_Laboratorio_T3.value='';document.form1.Wizard1$Txt_Laboratorio_T4.value='';document.form1.Wizard1$Txt_Laboratorio_TSH.value='';document.form1.Wizard1$Txt_Laboratorio_Digitalemia.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        BNP</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Bnp" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TROPONINA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Troponina" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PCR</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Pcr" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        URICEMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Uricemia" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        AZOT.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Azot" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CREAT.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Creat" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        MICROALBUMIN.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Microalbumin" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        NA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Na" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        K</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_K" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Ca" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PH</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Ph" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        MG</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Mg" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GOT</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Got" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GPT</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Gpt" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CPK</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Cpk" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GR</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Gr" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        EMOGL.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Emogl" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GB</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Gb" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        SIDEREMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Sideremia" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GLICEMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Glicemia" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        EMOGL. GLIC.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Emogl_Glic" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        COL. TOT.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Col_Tot" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        LDL</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\d*" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Laboratorio_Ldl.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire un valore numerico per il campo LDL</div></span>" ControlToValidate="Txt_Laboratorio_Ldl"></asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Ldl" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        HDL</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Hdl" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TRIGLIC.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Triglic" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        T3</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_T3" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        T4</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_T4" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TSH</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_TSH" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        DIGITALEMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Laboratorio_Digitalemia" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep6" runat="server" Title="Consulenze">
                            <div class="titolo_step">CONSULENZE</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">NEFROLOGICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Consulenze_Nefrologica[1].checked=true;document.form1.Wizard1$Cld_Consulenze_Nefrologica.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        NEFROLOGICA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Consulenze_Nefrologica" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator35" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Consulenze_Nefrologica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Consulenze_Nefrologica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Consulenze_Nefrologica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Consulenze_Nefrologica', document.form1.Wizard1$Cld_Consulenze_Nefrologica.value, 'calendar35');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar35" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">EMATOLOGICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Consulenze_Ematologica[1].checked=true;document.form1.Wizard1$Cld_Consulenze_Ematologica.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        EMATOLOGICA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Consulenze_Ematologica" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator36" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Consulenze_Ematologica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Consulenze_Ematologica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Consulenze_Ematologica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Consulenze_Ematologica', document.form1.Wizard1$Cld_Consulenze_Ematologica.value, 'calendar36');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar36" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">NEUROPSICHIATRICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Consulenze_Neuropsichiatra[1].checked=true;document.form1.Wizard1$Cld_Consulenze_Neuropsichiatrica.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        NEUROPSICHIATRICA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Consulenze_Neuropsichiatra" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator37" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Consulenze_Neuropsichiatrica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Consulenze_Neuropsichiatrica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Consulenze_Neuropsichiatrica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Consulenze_Neuropsichiatrica', document.form1.Wizard1$Cld_Consulenze_Neuropsichiatrica.value, 'calendar37');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar37" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ENDOCRINOLOGICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Consulenze_Endocrinologica[1].checked=true;document.form1.Wizard1$Cld_Consulenze_Endocrinologica.value='';;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        ENDOCRINOLOGICA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Consulenze_Endocrinologica" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator38" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Consulenze_Endocrinologica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Consulenze_Endocrinologica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Consulenze_Endocrinologica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Consulenze_Endocrinologica', document.form1.Wizard1$Cld_Consulenze_Endocrinologica.value, 'calendar38');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar38" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">PNEUMOLOGICA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Consulenze_Pneumologica[1].checked=true;document.form1.Wizard1$Cld_Consulenze_Pneumologica.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PNEUMOLOGICA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Consulenze_Pneumologica" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator39" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Consulenze_Pneumologica" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Consulenze_Pneumologica.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Consulenze_Pneumologica" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Consulenze_Pneumologica', document.form1.Wizard1$Cld_Consulenze_Pneumologica.value, 'calendar39');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar39" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep7" runat="server" Title="Es. Strum.">
                            <div class="titolo_step">ESAMI STRUMENTALI</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ECG<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Esami_Strumentali_Ritmo_Sinusale[1].checked=true;document.form1.Wizard1$Txt_Esami_Strumentali_Ecg_Ingresso_Frequenza_Cardiaca.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Ex_Sopraventricol[1].checked=true;document.form1.Wizard1$Ddl_Esami_Strumentali_Ecg_Ingresso_Ex_Ventricol.selectedIndex=0;document.form1.Wizard1$Rbl_Esami_Strumentali_Tv[0].checked=false;document.form1.Wizard1$Rbl_Esami_Strumentali_Tv[1].checked=false;document.form1.Wizard1$Rbl_Esami_Strumentali_Fa[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Danno_Atriale[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Bbs[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Bbd[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Eas[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Eps[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Sovraccarico_Sistolico[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Sovraccarico_Volume[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ischemia[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ritmo sinusale</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ritmo_Sinusale" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Frequenza cardiaca/min</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Ecg_Ingresso_Frequenza_Cardiaca" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        BAV</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_BAV" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>I</asp:ListItem>
                                            <asp:ListItem>II</asp:ListItem>
                                            <asp:ListItem>III</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ex. sopraventricol</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ex_Sopraventricol" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ex. Ventricol.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Ecg_Ingresso_Ex_Ventricol" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>1a</asp:ListItem>
                                            <asp:ListItem>1b</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4a</asp:ListItem>
                                            <asp:ListItem>4b</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TV</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Tv" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Sostenuta</asp:ListItem>
                                            <asp:ListItem>Non sostenuta</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        FA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Fa" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Danno atriale sx</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Danno_Atriale" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Bbs</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Bbs" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Bbd</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Bbd" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Eas</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Eas" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Eps</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Eps" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ipertrofia ventricolare sx</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Sovraccarico sistolico</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Sovraccarico_Sistolico" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Sovraccarico di volume</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Sovraccarico_Volume" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ischemia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ischemia" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">RX TORACE<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Ddl_Esami_Strumentali_Rx_Torace.selectedIndex=0;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Killip</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Rx_Torace" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">VERSAMENTO PLEURICO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Esami_Strumentali_Versamento_Pleurico[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        VERSAMENTO PLEURICO</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Versamento_Pleurico" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ECO 2D<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Esami_Strumentali_Eco2d_Ipertrofia_Settale.value='';document.form1.Wizard1$Txt_Esami_Strumentali_Eco2d_Ipertrofia_Concentrica.value='';document.form1.Wizard1$Ddl_Esami_Strumentali_Eco2d_Ipocinesia.selectedIndex=0;document.form1.Wizard1$Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare.value='';document.form1.Wizard1$Txt_Esami_Strumentali_Eco2d_Volume_Diastolico.value='';document.form1.Wizard1$Txt_Esami_Strumentali_Eco2d_Fe.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Disincronizzazione[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Ipertrofia settale</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Eco2d_Ipertrofia_Settale" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Ipertrofia concentrica</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Eco2d_Ipertrofia_Concentrica" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ipocinesia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Eco2d_Ipocinesia" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Anteriore</asp:ListItem>
                                            <asp:ListItem>Settale</asp:ListItem>
                                            <asp:ListItem>Apicale</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diametro sistolico ventricolare SX</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ValidationExpression="\d*" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire il diametro sistolico del paziente in un formato numerico</div></span>" ControlToValidate="Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare"></asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diametro diastolico</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Eco2d_Volume_Diastolico" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Fe</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                     <asp:CompareValidator id="CompareValidator1" ControlToValidate="Txt_Esami_Strumentali_Eco2d_Fe" Text="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Esami_Strumentali_Eco2d_Fe.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire il valore di FE in formato numerico</div></span>" Operator="DataTypeCheck" Type="Double" Runat="server" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Eco2d_Fe" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Disincronizzazione</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Disincronizzazione" runat="server"
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ECO DOPPLER<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Esami_Strumentali_Eco_Doppler_Descrizione.value='';document.form1.Wizard1$Ddl_Esami_Strumentali_Eco_Doppler_Disfunzione_Diastolica.selectedIndex=0;document.form1.Wizard1$Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Mitralico.selectedIndex=0;document.form1.Wizard1$Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Aortico.selectedIndex=0;document.form1.Wizard1$Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Tricuspidale.selectedIndex=0;document.form1.Wizard1$Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare.value='';return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        ECO DOPPLER</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Eco_Doppler_Descrizione" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Disfunzione diastolica</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Eco_Doppler_Disfunzione_Diastolica" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>Normale</asp:ListItem>
                                            <asp:ListItem>Alterato rilasciamento</asp:ListItem>
                                            <asp:ListItem>Pseudonormalizzazione</asp:ListItem>
                                            <asp:ListItem>Restrittivo reversibile</asp:ListItem>
                                            <asp:ListItem>Restrittivo irreversibile</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Rigurgito mitralico</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Mitralico" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Rigurgito aortico</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Aortico" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Rigurgito tricuspidale</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Tricuspidale" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Pressione polmonare</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    <asp:CompareValidator id="CompareValidator2" ControlToValidate="Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare" Text="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire la pressione polmonare in un formato corretto</div></span>" Operator="DataTypeCheck" Type="Double" Runat="server" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DOPPLER ARTERIOSO ARTI INFERIORI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Esami_Strumentali_Doppler_Arterioso_Arti_Inferiori.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        DOPPLER ARTERIOSO ARTI INFERIORI</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Doppler_Arterioso_Arti_Inferiori" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Stenosi significative</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DOPPLER TEA<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Esami_Strumentali_Doppler_Tea.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Doppler_Tea_Stenosi_Significative[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        DOPPLER TEA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Doppler_Tea" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Stenosi significative</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Doppler_Tea_Stenosi_Significative" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3" class="titolo_sezione">ECG HOLTER<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale[1].checked=true;document.form1.Wizard1$Txt_Esami_Strumentali_Ecg_Holter_Frequenza_Cardiaca.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr[1].checked=true;document.form1.Wizard1$Ddl_Esami_Strumentali_Ecg_Holter_Ex_Ventricol.selectedIndex=0;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Tv[0].checked=false;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Tv[1].checked=false;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Fa[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Bbs[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Bbd[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Eas[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Eps[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume[1].checked=true;document.form1.Wizard1$Rbl_Esami_Strumentali_Ecg_Holter_Ischemia[1].checked=true;return false;">reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ritmo sinusale</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Frequenza cardiaca/min</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Ecg_Holter_Frequenza_Cardiaca" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ex. sopraventricol</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ex. Ventricol.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Ecg_Holter_Ex_Ventricol" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>1a</asp:ListItem>
                                            <asp:ListItem>1b</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4a</asp:ListItem>
                                            <asp:ListItem>4b</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TV</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Tv" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Sostenuta</asp:ListItem>
                                            <asp:ListItem>Non sostenuta</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        FA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Fa" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Danno atriale sx</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Bbs</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Bbs" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Bbd</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Bbd" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Eas</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Eas" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Eps</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Eps" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ipertrofia ventricolare sx</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Sovraccarico sistolico</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Sovraccarico di volume</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ischemia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Ecg_Holter_Ischemia" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep8" runat="server" Title="Diagn. terap.">
                            <div class="titolo_step">DIAGNOSI E TERAPIA</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DIAGNOSI E TERAPIA<asp:Label ID="lb_gv" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diagnosi</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Diagnosi" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Classe NYHA</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Peso (kg)</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                        <asp:CompareValidator id="cmpProductPrice" ControlToValidate="Txt_Esami_Strumentali_Peso1" Text="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Esami_Strumentali_Peso1.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire il peso in un formato corretto</div></span>" Operator="DataTypeCheck" Type="Double" Runat="server" />
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Peso1" runat="server" MaxLength="6" Columns="10"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" colspan="3">
                                        <asp:GridView ID="gv_farmaci" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="gv_farmaci_RowDataBound">
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <AlternatingRowStyle BackColor="White" />
                                            <EditRowStyle BackColor="#2461BF" />
                                            <Columns>
                                                <asp:BoundField DataField="IdFarmaco" Visible="False" />
                                                <asp:BoundField DataField="NomeFarmaco" HeaderText="Farmaco" />
                                                <asp:BoundField DataField="Valore" Visible="True" />
                                                <asp:BoundField DataField="Intolleranza" Visible="True" />
                                                <asp:BoundField DataField="CostoFarmaco" Visible="True" />
                                                <asp:TemplateField HeaderText="Valore">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox1" runat="server" Columns="20"></asp:TextBox>
                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Si</asp:ListItem>
                                                            <asp:ListItem Selected="True" Value="1">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Intolleranza">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Costo Farmaco">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox2" runat="server" Columns="15" ></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle BackColor="#EFF3FB" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Counceling</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Counceling" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        
                        
                        <asp:WizardStep ID="WizardStep9" runat="server" Title="EndPointFinale">
                            <div class="titolo_step">ENDPOINT FINALE</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DATI<asp:Label ID="Label1" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Numero Ospedalizzazioni</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Numero_Ospedalizzazioni" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Questionario Minnesota Life</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Quest_Minnesota_Life" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data Morte</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator40" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="Cld_Data_Morte" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Cld_Data_Morte.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="Cld_Data_Morte" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Cld_Data_Morte', document.form1.Wizard1$Cld_Data_Morte.value, 'calendar40');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar40" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Costo Totale Terapia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Costo_Totale" runat="server" MaxLength="255" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                                               
                            </table>
                        </asp:WizardStep>
                        
                        
                        
                        
                        
                    </WizardSteps>
                    <NavigationStyle VerticalAlign="Top" />
                    <StepStyle Font-Size="0.8em" ForeColor="#333333" />
                    <SideBarStyle BackColor="#507CD1" Font-Size="0.9em" VerticalAlign="Top" />
                    <NavigationButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" />
                    <SideBarButtonStyle BackColor="#507CD1" Font-Names="Verdana" ForeColor="White" />
                    <HeaderStyle BackColor="#284E98" BorderColor="#EFF3FB" BorderStyle="Solid" BorderWidth="2px"
                        Font-Bold="True" Font-Size="0.9em" ForeColor="White" HorizontalAlign="Center" />
                </asp:Wizard>
                
            </td>
          </tr>
      </table>
    </div>
        <br />
        <br />
        <asp:Label ID="lb_mess" runat="server" Text="" ForeColor="Red"></asp:Label>
    
    </td>
    <td width="10" valign="top" background="./files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="./files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="3" style="height: 20px"><!--#include file="include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="./files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" background="./files/bg_content_bottom.gif">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>

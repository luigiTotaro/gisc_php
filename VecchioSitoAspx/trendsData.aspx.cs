﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using Newtonsoft.Json;

public partial class trendsData : System.Web.UI.Page
{
    private string conn_str;
    private string aPartireDa = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        string action = Request.Params["action"].ToString();
        if (Request.Params["DA"] != null)
        {
            aPartireDa = Request.Params["DA"].ToString();
        }
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        switch (action)
        {
            case "fcardiaca":
                getfcardiaca();
                break;
            case "totali":
                totali();
                break;
            case "totaliFC":
                totaliFC();
                break;
            case "totaliFE":
                totaliFE();
                break;
            case "totaliPP":
                totaliPP();
                break;
            case "totaliIC":
                totaliIC();
                break;
            case "totaliBP":
                totaliBP();
                break;

        }
    }
    private void totaliBP()
    {

        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command;
            if (aPartireDa=="")
                command  = new SqlCommand("Select Replace(IngrBNP,',','.') as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico where ISNUMERIC ( Replace(IngrBNP,',','.') ) = 1  order by DataQuadroClinico ASC", connection);
            else
                command = new SqlCommand("Select Replace(IngrBNP,',','.') as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico,Paziente where Paziente.idPaziente=QuadroClinico.idPaziente and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) and ISNUMERIC ( Replace(IngrBNP,',','.') ) = 1  order by DataQuadroClinico ASC", connection);
           
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                string res = "{Data: ";
                res += JsonConvert.SerializeObject(dataTable);

                res += " }";
                res = res.Replace("\"IV classe\"", "4");
                res = res.Replace("\"III classe\"", "3");
                res = res.Replace("\"II classe\"", "2");
                res = res.Replace("\"I classe\"", "1");
                res = res.Replace("\"undefined\"", "8");

                int l = dataTable.Rows.Count;

                for (int i = 0; i < (l / 50) + 1; i++)
                //for (int i = 0; i < (l / 50); i++)
                {
                    String strCmd = "Select avg(data1) from (Select cast(Replace(IngrBNP,',','.') as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico where Replace(IngrBNP,',','.')!= '' and ISNUMERIC ( Replace(IngrBNP,',','.') ) = 1 ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50);
                    if (aPartireDa !="")
                        strCmd = "Select avg(data1) from (Select cast(Replace(IngrBNP,',','.') as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico,Paziente where Paziente.idPaziente=QuadroClinico.idPAziente and Cast(DataRegistrazione as datetime) <= Cast('" + aPartireDa + "' as datetime) and Replace(IngrBNP,',','.')!= '' and ISNUMERIC ( Replace(IngrBNP,',','.') ) = 1 ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50);
                    //Response.Write("<BR/><BR/>" + strCmd + "<BR/><BR/>");

                    command = new SqlCommand(strCmd, connection);
                    adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    res = res.Replace("\"RANGE" + i + "\"", dataTable.Rows[0][0].ToString().Replace(",","."));

                }





                res = res.Replace("\"RANGEundefined\"", "undefined");

                Response.Write(res);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message + "-" + ex.StackTrace);
        }
        finally
        {

            connection.Close();
        }
    }

    private void totaliIC()
    {

        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command;
            if (aPartireDa == "")
                command = new SqlCommand("Select Replace(IngrCreat, ',', '.') as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico where ISNUMERIC ( Replace(IngrCreat, ',', '.') ) = 1 order by DataQuadroClinico ASC", connection);
            else
                command = new SqlCommand("Select Replace(IngrCreat, ',', '.') as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico,paziente where Paziente.idPaziente=QuadroClinico.idPaziente and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa+ "' as datetime) and ISNUMERIC ( Replace(IngrCreat, ',', '.') ) = 1 order by DataQuadroClinico ASC", connection);
           
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                string res = "{Data: ";
                res += JsonConvert.SerializeObject(dataTable);

                res += " }";
                res = res.Replace("\"IV classe\"", "4");
                res = res.Replace("\"III classe\"", "3");
                res = res.Replace("\"II classe\"", "2");
                res = res.Replace("\"I classe\"", "1");
                res = res.Replace("\"undefined\"", "8");

                int l = dataTable.Rows.Count;

                for (int i = 0; i < (l / 50) + 1; i++)
                //for (int i = 0; i < (l / 50); i++)
                {
                    String strCmd = "Select avg(data1) from (Select cast(Replace(IngrCreat, ',', '.') as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico where Replace(IngrCreat, ',', '.')!= '' and ISNUMERIC ( Replace(IngrCreat, ',', '.') ) = 1 ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50) +" and data1 <= 10 ";
                    if (aPartireDa != "")
                        strCmd = "Select avg(data1) from (Select cast(Replace(IngrCreat, ',', '.') as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico,Paziente where Paziente.idPaziente =QuadroClinico.idPaziente and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) and Replace(IngrCreat, ',', '.')!= '' and ISNUMERIC ( Replace(IngrCreat, ',', '.') ) = 1 ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50) +" and data1 <= 10 ";
                  
                    command = new SqlCommand(strCmd, connection);
                    adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    res = res.Replace("\"RANGE" + i + "\"", dataTable.Rows[0][0].ToString().Replace(",", "."));

                }





                res = res.Replace("\"RANGEundefined\"", "undefined");

                Response.Write(res);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }

    private void totaliPP()
    {

        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command;
            if (aPartireDa == "")
                command = new SqlCommand("Select EcoDopplerPressPolm as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico where ISNUMERIC ( EcoDopplerPressPolm ) = 1 order by DataQuadroClinico ASC", connection);
            else
                command = new SqlCommand("Select EcoDopplerPressPolm as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico,Paziente  where Paziente.idPaziente=QuadroClinico.idPaziente and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) and ISNUMERIC ( EcoDopplerPressPolm ) = 1 order by DataQuadroClinico ASC", connection);
           
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                string res = "{Data: ";
                res += JsonConvert.SerializeObject(dataTable);

                res += " }";
                res = res.Replace("\"IV classe\"", "4");
                res = res.Replace("\"III classe\"", "3");
                res = res.Replace("\"II classe\"", "2");
                res = res.Replace("\"I classe\"", "1");
                res = res.Replace("\"undefined\"", "8");

                int l = dataTable.Rows.Count;

                for (int i = 0; i < (l / 50) + 1; i++)
                //for (int i = 0; i < (l / 50); i++)
                {
                    String strCmd = "Select avg(data1) from (Select cast(EcoDopplerPressPolm as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico where EcoDopplerPressPolm!= '' and ISNUMERIC ( EcoDopplerPressPolm ) = 1 ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50); ;
                    if (aPartireDa != "")
                        strCmd = "Select avg(data1) from (Select cast(EcoDopplerPressPolm as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico,Paziente where Paziente.idPAziente=QuadroClinico.idPaziente and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) and EcoDopplerPressPolm!= '' and ISNUMERIC ( EcoDopplerPressPolm ) = 1 ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50); ;
                    
                    command = new SqlCommand(strCmd, connection);
                    adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    res = res.Replace("\"RANGE" + i + "\"", dataTable.Rows[0][0].ToString().Replace(",", "."));

                }





                res = res.Replace("\"RANGEundefined\"", "undefined");

                Response.Write(res);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }
    private void totaliFE()
    {

        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command;
            if (aPartireDa =="")
                command = new SqlCommand("Select Eco2DFE as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico where ISNUMERIC ( Eco2DFE ) = 1 order by DataQuadroClinico ASC", connection);
            else
                command = new SqlCommand("Select Eco2DFE as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico,Paziente where Paziente.idPaziente=QuadroClinico.idPaziente and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) and ISNUMERIC ( Eco2DFE ) = 1 order by DataQuadroClinico ASC", connection);
            
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                string res = "{Data: ";
                res += JsonConvert.SerializeObject(dataTable);

                res += " }";
                res = res.Replace("\"IV classe\"", "4");
                res = res.Replace("\"III classe\"", "3");
                res = res.Replace("\"II classe\"", "2");
                res = res.Replace("\"I classe\"", "1");
                res = res.Replace("\"undefined\"", "8");

                int l = dataTable.Rows.Count;

                for (int i = 0; i < (l / 50) + 1; i++)
                //for (int i = 0; i < (l / 50); i++)
                {
                    String strCmd = "Select avg(data1) from (Select cast(Eco2DFE as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico where Eco2DFE!= '' and ISNUMERIC ( Eco2DFE ) = 1 ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50); ;
                    if (aPartireDa!= "")
                        strCmd = "Select avg(data1) from (Select cast(Eco2DFE as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico,Paziente  where Paziente.idPaziente=QuadroClinico.IdPaziente and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) and  Eco2DFE!= '' and ISNUMERIC ( Eco2DFE ) = 1 ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50); ;
                 
                    command = new SqlCommand(strCmd, connection);
                    adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    res = res.Replace("\"RANGE" + i + "\"", dataTable.Rows[0][0].ToString().Replace(",", "."));

                }





                res = res.Replace("\"RANGEundefined\"", "undefined");

                Response.Write(res);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }

    private void totaliFC()
    {

        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command;
            if (aPartireDa=="")
                command = new SqlCommand("Select ECGFrequenza as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico where ISNUMERIC ( ECGFrequenza ) = 1 order by DataQuadroClinico ASC", connection);
            else
                command = new SqlCommand("Select ECGFrequenza as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico,Paziente where Paziente.idPaziente = QuadroClinico.idPaziente and  ISNUMERIC ( ECGFrequenza ) = 1 and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) order by DataQuadroClinico ASC", connection);
           
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                string res = "{Data: ";
                res += JsonConvert.SerializeObject(dataTable);

                res += " }";
                res = res.Replace("\"IV classe\"", "4");
                res = res.Replace("\"III classe\"", "3");
                res = res.Replace("\"II classe\"", "2");
                res = res.Replace("\"I classe\"", "1");
                res = res.Replace("\"undefined\"", "8");

                int l = dataTable.Rows.Count;

                for (int i = 0; i < (l / 50) + 1; i++)
                //for (int i = 0; i < (l / 50); i++)
                {
                    String strCmd = "Select avg(data1) from (Select cast(ECGFrequenza as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico where ECGFrequenza!= '' and ISNUMERIC ( ECGFrequenza ) = 1 ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50);
                    if (aPartireDa != "")
                        strCmd = "Select avg(data1) from (Select cast(ECGFrequenza as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico,Paziente  where Paziente.idPaziente=QuadroClinico.idPaziente and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) and  ECGFrequenza!= '' and ISNUMERIC ( ECGFrequenza ) = 1 ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50);
                    command = new SqlCommand(strCmd, connection);
                    adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    res = res.Replace("\"RANGE" + i + "\"", dataTable.Rows[0][0].ToString().Replace(",", "."));

                }





                res = res.Replace("\"RANGEundefined\"", "undefined");

                Response.Write(res);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }

    private void totali()
    {
   
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command;
           if (aPartireDa == "")
                command = new SqlCommand("Select ClasseDispneaSforzo as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico where ClasseDispneaSforzo!= '' order by DataQuadroClinico ASC", connection);
              else
               command = new SqlCommand("Select ClasseDispneaSforzo as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, dbo.getRange(ROW_NUMBER() OVER (ORDER BY DataQuadroClinico)) as data3 from QuadroClinico,Paziente where Paziente.idPaziente=QuadroClinico.idPaziente and ClasseDispneaSforzo!= '' and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) order by DataQuadroClinico ASC", connection);
           
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                string res = "{Data: ";
                res += JsonConvert.SerializeObject(dataTable);

                res += " }";
                res = res.Replace("\"IV classe\"", "4");
                res = res.Replace("\"III classe\"", "3");
                res = res.Replace("\"II classe\"", "2");
                res = res.Replace("\"I classe\"", "1");
                res = res.Replace("\"undefined\"", "8");

                int l = dataTable.Rows.Count;

                for (int i = 0; i < (l / 50)+1; i++)
                //for (int i = 0; i < (l / 50); i++)
                {
                    String strCmd = "Select avg(data1) from (Select cast(Replace(Replace(Replace(Replace(ClasseDispneaSforzo, 'IV classe', '4'),'III classe','3'),'II classe','2'),'I classe','1') as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico where ClasseDispneaSforzo!= '') a where data2 >= " + (i*50) + " and data2 < " + ((i*50)+50);
                    if (aPartireDa != "")
                        strCmd = "Select avg(data1) from (Select cast(Replace(Replace(Replace(Replace(ClasseDispneaSforzo, 'IV classe', '4'),'III classe','3'),'II classe','2'),'I classe','1') as float) as data1, ROW_NUMBER() OVER (ORDER BY DataQuadroClinico) AS data2, 'undefined' as data3 from QuadroClinico,Paziente where Paziente.idPaziente=QuadroClinico.idPaziente and ClasseDispneaSforzo!= '' and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) ) a where data2 >= " + (i * 50) + " and data2 < " + ((i * 50) + 50);
                    command = new SqlCommand(strCmd, connection);
                    adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    res = res.Replace("\"RANGE" + i + "\"", dataTable.Rows[0][0].ToString().Replace(",", "."));

                }

                

                

                res = res.Replace("\"RANGEundefined\"","undefined");
               
                Response.Write(res);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }

    private void getfcardiaca()
    {
        string paz = Request.Params["idPaziente"].ToString();
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command;
            if (aPartireDa == "")
                command = new SqlCommand("Select ECGFrequenza as gFrequenzaCard,Eco2DFE,EcoDopplerPressPolm,ClasseDispneaSforzo,Replace(IngrCreat, ',', '.') as IngrCreat ,Replace(IngrBNP,',','.') as IngrBNP, DataQuadroClinico from QuadroClinico where idPaziente=" + paz + " order by DataQuadroClinico ASC", connection);
            else
                command = new SqlCommand("Select ECGFrequenza as gFrequenzaCard,Eco2DFE,EcoDopplerPressPolm,ClasseDispneaSforzo,Replace(IngrCreat, ',', '.') as IngrCreat ,Replace(IngrBNP,',','.') as IngrBNP, DataQuadroClinico from QuadroClinico,Paziente where Paziente.idPaziente=" + paz + " and Paziente.idPaziente=QuadroClinico.idPaziente and Cast(DataRegistrazione as datetime) <= cast('" + aPartireDa + "' as datetime) order by DataQuadroClinico ASC", connection);
           
            //command.Parameters.AddWithValue("@IdPaziente", Request.Params["idpaziente"].ToString());
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            if (dataTable.Rows.Count > 0)
            {
                string res = "{Data: ";
                res += JsonConvert.SerializeObject(dataTable);

                res += " }";
                Response.Write(res);
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }
}

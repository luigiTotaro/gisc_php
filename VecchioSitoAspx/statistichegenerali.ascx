<%@ control language="C#" autoeventwireup="true" inherits="statistichegenerali, App_Web_statistichegenerali.ascx.cdcab7d2" %>
<div class="div_dx">
    <div class="blocco_statistiche">
		<div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('1', false); return false;">Pazienti</a></div>
		<div id="descb_1">
		    <div style="padding:3px;">
                <asp:Label ID="lb_pazienti" runat="server" Text=""></asp:Label>
            </div>
            <div class="sottoblocco_statistiche">
		        <div class="titolo_sottoblocco">Osservazioni</div>
		        <div style="padding:3px;">
                    <asp:Label ID="lb_osservazioni" runat="server" Text=""></asp:Label>
                </div>
	        </div>
	    </div>
	</div>
	<div class="blocco_statistiche">
		<div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('2', false); return false;">Fumo</a></div>
		<div id="descb_2" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_fumatori" runat="server" Text=""></asp:Label>
            </div>
            <div class="sottoblocco_statistiche">
		        <div class="titolo_sottoblocco">Fumatori +</div>
		        <div style="padding:3px;">
                    <asp:Label ID="lb_fumatori1" runat="server" Text=""></asp:Label>
                </div>
	        </div>
	        <div class="sottoblocco_statistiche">
		        <div class="titolo_sottoblocco">Fumatori ++</div>
		        <div style="padding:3px;">
                    <asp:Label ID="lb_fumatori2" runat="server" Text=""></asp:Label>
                </div>
	        </div>
	        <div class="sottoblocco_statistiche">
		        <div class="titolo_sottoblocco">Fumatori +++</div>
		        <div style="padding:3px;">
                    <asp:Label ID="lb_fumatori3" runat="server" Text=""></asp:Label>
                </div>
	        </div>
	    </div>
	</div>
	
	<div class="blocco_statistiche">
		<div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('3', false); return false;">Ipertensione Arteriosa</a></div>
		<div id="descb_3" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_ipertensione_art" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
    
    <div class="blocco_statistiche">
        <div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('4', false); return false;">Diabete</a></div>
		<div id="descb_4" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_diabete" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
	
	<div class="blocco_statistiche">
        <div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('5', false); return false;">IMA</a></div>
		<div id="descb_5" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_IMA" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
    
    <div class="blocco_statistiche">
        <div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('6', false); return false;">BPCO</a></div>
		<div id="descb_6" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_BPCO" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
    
    <div class="blocco_statistiche">
        <div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('7', false); return false;">Insuff. Renale</a></div>
		<div id="descb_7" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_InsuffRenale" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
    <div class="blocco_statistiche">
        <div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('8', false); return false;">Anemia</a></div>
		<div id="descb_8" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_Anemia" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
    <div class="blocco_statistiche">
        <div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('9', false); return false;">BMI > 30</a></div>
		<div id="descb_9" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_Obesita" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
    <div class="blocco_statistiche">
        <div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('10', false); return false;">Ipertiroidismo</a></div>
		<div id="descb_10" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_Ipertiroidismo" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
    <div class="blocco_statistiche">
        <div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('11', false); return false;">Sindrome Metabolica</a></div>
		<div id="descb_11" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_Sindrome" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
    <div class="blocco_statistiche">
        <div class="titolo_blocco"><a class="titolo_blocco_link" href="javascript:void(0);" onclick="ShowHide('12', false); return false;">Colesterolo (LDL>100)</a></div>
		<div id="descb_12" style="display:none">
		    <div style="padding:3px;">
                <asp:Label ID="lb_Colesterolo" runat="server" Text=""></asp:Label>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
<!--
function getEl(id)
{
    element = document.getElementById(id);
    return element;
}

function hideEl(id)
{
    var element = getEl(id);
    element.style.display = 'none';
}

function showEl(id)
{
    var element = getEl(id);
    element.style.display = '';
}

function ShowHide(sid, last)
{
    var el = 'descb_' + sid;    

    if(getEl(el).style.display == 'none') {
        showEl(el);

    } else {
        hideEl(el);
    }
}
//-->
</script>
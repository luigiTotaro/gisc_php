﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Net.Mail;

public partial class nuovopaziente : System.Web.UI.Page
{
    private string conn_str;


    private int IdOspedalizzazione1;
    private int IdOspedalizzazione2;
    private int IdOspedalizzazione3;
    private int IdPaziente;
    private int IdPrecedenteScomp1;
    private int IdPrecedenteScomp2;
    private int IdPrecedenteScomp3;
    private int IdUltimoPaziente;
    private int idQuadro = -1;
    private int IdUltimoQuadro;
    private bool ApportaModifiche()
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_paziente";
        }
        else
        {
            str = "modifica_paziente";
        }
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlTransaction transaction = connection.BeginTransaction();
            try
            {
                try
                {
                    SqlCommand command = new SqlCommand(str, connection, transaction);
                    command.CommandType = CommandType.StoredProcedure;
                    if (!this.IdPaziente.Equals(-1))
                    {
                        command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
                    }
                    command.Parameters.AddWithValue("@DataRegistrazione", Utility.CodificaData(this.Cld_Data_Registrazione.Text.Trim()));
                    if (!IsPostBack)
                        this.lb_titolo.Text += "<br>Reclutato il <b>" + Utility.CodificaData(this.Cld_Data_Registrazione.Text.Trim()) + "</b>";
                    command.Parameters.AddWithValue("@Nome", this.Txt_Nome.Text.Trim());
                    command.Parameters.AddWithValue("@Cognome", this.Txt_Cognome.Text.Trim());
                    command.Parameters.AddWithValue("@Indirizzo", this.Txt_Indirizzo.Text.Trim());
                    command.Parameters.AddWithValue("@Telefono", this.Txt_Telefono.Text.Trim());
                    command.Parameters.AddWithValue("@Sesso", this.Sesso.SelectedValue);
                    command.Parameters.AddWithValue("@CodiceFiscale", this.Txt_Codice_Fiscale.Text.Trim());
                    command.Parameters.AddWithValue("@Eta", this.Txt_Eta.Text.Trim());
                    command.Parameters.AddWithValue("@Altezza", this.Txt_Altezza.Text.Trim());
                    command.Parameters.AddWithValue("@Peso", this.Txt_Peso1.Text.Trim());
                    command.Parameters.AddWithValue("@CircAddominale", this.Txt_Circonferenza_Addominale.Text.Trim());
                    command.Parameters.AddWithValue("@IdMedicoCurante", this.Medico.SelectedValue);
                    if (this.Session["IdUtente"] != null)
                    {
                        command.Parameters.AddWithValue("@IdUtente", this.Session["IdUtente"].ToString());
                    }
                    else
                    {
                        command.Parameters.AddWithValue("@IdUtente", 0);
                    }
                    SqlDataReader reader = command.ExecuteReader();
                    if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            this.IdUltimoPaziente = Convert.ToInt32(reader["idultimopaziente"]);
                        }
                    }
                    else
                    {
                        this.IdUltimoPaziente = 0;
                    }
                    reader.Close();
                    this.ApportaModificheIMA(transaction, connection);
                    this.ApportaModificheCardiopIschemNoIMA(transaction, connection);
                    this.ApportaModificheCardiMioIpertrofica(transaction, connection);
                    this.ApportaModificheCardioMiopatiaDilatativa(transaction, connection);
                    this.ApportaModificheValvolopatia(transaction, connection);
                    this.ApportaModificheAltreEziologie(transaction, connection);
                    this.trova_id_ospedalizzazioni(this.IdPaziente);
                    this.ApportaModificheOspedalizzazioni(transaction, connection, this.Cld1_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Cld_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Txt_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Ddl_Classe_NYHA_Al_Ricovero, this.Txt_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Ddl_Classe_NYHA_Alla_Dimissione, this.IdOspedalizzazione1);
                    this.ApportaModificheOspedalizzazioni(transaction, connection, this.Cld2_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Cld2_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Txt2_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Ddl2_Classe_NYHA_Al_Ricovero, this.Txt2_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Ddl2_Classe_NYHA_Alla_Dimissione, this.IdOspedalizzazione2);
                    this.ApportaModificheOspedalizzazioni(transaction, connection, this.Cld3_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Cld3_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Txt3_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Ddl3_Classe_NYHA_Al_Ricovero, this.Txt3_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso, this.Ddl3_Classe_NYHA_Alla_Dimissione, this.IdOspedalizzazione3);
                    this.trova_id_scompensi(this.IdPaziente);
                    this.ApportaModifichePrecedentiScomp(transaction, connection, this.Cld_Precedenti_Accessi_Scompenso, this.Txt_Diagnosi_Precedenti_Accessi_Scompenso, this.Txt_Terapia_Precedenti_Accessi_Scompenso, this.IdPrecedenteScomp1);
                    this.ApportaModifichePrecedentiScomp(transaction, connection, this.Cld2_Precedenti_Accessi_Scompenso, this.Txt2_Diagnosi_Precedenti_Accessi_Scompenso, this.Txt2_Terapia_Precedenti_Accessi_Scompenso, this.IdPrecedenteScomp2);
                    this.ApportaModifichePrecedentiScomp(transaction, connection, this.Cld3_Precedenti_Accessi_Scompenso, this.Txt3_Diagnosi_Precedenti_Accessi_Scompenso, this.Txt3_Terapia_Precedenti_Accessi_Scompenso, this.IdPrecedenteScomp3);
                    this.ApportaModificheRischi(transaction, connection);
                    transaction.Commit();
                    flag = true;
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
                }
                return flag;
            }
            finally
            {
                connection.Close();
            }
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message + " " + exception2.StackTrace; ;
        }
        return flag;
    }

    private bool ApportaModificheAltreEziologie(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_altre_eziologie";
        }
        else
        {
            str = "modifica_altre_eziologie";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", this.Txt_Altre_Eziologie_Descrizione.Text.Trim());
            command.Parameters.AddWithValue("@Denominazione", this.Txt_Altre_Eziologie_Denominazione.Text.Trim());
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_Altre_Eziologie.Text.Trim()));
            //command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Altre_Eziologie_Document_Ecg.Text.Trim());
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Altre_Eziologie_Document_Ecg_ch.Checked);
            //command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Altre_Eziologie_Document_Eco.Text.Trim());
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Altre_Eziologie_Document_Eco_ch.Checked);
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace;
        }
        return flag;
    }

    private bool ApportaModificheCardiMioIpertrofica(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_CardiMioIpertrofica";
        }
        else
        {
            str = "modifica_CardiMioIpertrofica";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_Cardiomiopatia_Ipertrofica.Text.Trim()));
            //command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Ecg.Text.Trim());
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Ecg_ch.Checked);
            //command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Eco.Text.Trim());
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Eco_ch.Checked);
            command.Parameters.AddWithValue("@Ostruttiva", this.Ddl_Eziologia_Cardiomiopatia_Ipertrofica_Ostruttiva.Text.Trim());
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        return flag;
    }

    private bool ApportaModificheCardioMiopatiaDilatativa(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_CardioMiopatiaDilatativa";
        }
        else
        {
            str = "modifica_CardioMiopatiaDilatativa";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_Cardiomiopatia_Dilatativa.Text.Trim()));
            //command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Ecg.Text.Trim());
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Ecg_ch.Checked);
            //command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Eco.Text.Trim());
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Eco_ch.Checked);
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        return flag;
    }

    private bool ApportaModificheCardiopIschemNoIMA(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_CardiopIschemNoIMA";
        }
        else
        {
            str = "modifica_CardiopIschemNoIMA";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_Cardiop_Ischem.Text.Trim()));
            //command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Cardiop_Document_ECG.Text.Trim());
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Cardiop_Document_ECG_ch.Checked);
            //command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Cardiop_Document_ECO.Text.Trim());
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Cardiop_Document_ECO_ch.Checked);
            //command.Parameters.AddWithValue("@Coronografia", this.Rbl_Eziologia_Cardiop_Document_Coronarogr.Text.Trim());
            command.Parameters.AddWithValue("@Coronografia", this.Rbl_Eziologia_Cardiop_Document_Coronarogr_ch.Checked);
            //command.Parameters.AddWithValue("@Scintigrafia", this.Rbl_Eziologia_Cardiop_Document_Scintigrafia.Text.Trim());
            command.Parameters.AddWithValue("@Scintigrafia", this.Rbl_Eziologia_Cardiop_Document_Scintigrafia_ch.Checked);
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        return flag;
    }
    protected string alfabeto = "";
    protected void gv_farmaci_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.Cells[5].Controls.Count > 0)
        {
            //Response.Write("NOME FARMACO:" + e.Row.Cells[1].Text + " </br>");
            if ((e.Row.Cells[1].Text == "Furosemide") || (e.Row.Cells[1].Text == "Torasemide 10") || (e.Row.Cells[1].Text == "Carvedilolo"))
            {
                TextBox box = (TextBox)e.Row.Cells[5].Controls[1];
                box.Text = e.Row.Cells[2].Text.Replace("&nbsp;", "");
                box.Visible = true;
                RadioButtonList rbl = (RadioButtonList)e.Row.Cells[5].Controls[3];
                rbl.Visible = false;
            }
            else
            {
                TextBox box = (TextBox)e.Row.Cells[5].Controls[1];
                box.Visible = false;
                RadioButtonList list = new RadioButtonList();
                list.Items.Add(new ListItem("SI", "SI"));
                list.Items.Add(new ListItem("NO", "NO"));
                box.Text = e.Row.Cells[2].Text;
                //Response.Write("VALOREEEE:" + box.Text + " ESITO:" + (box.Text == "SI") + "<br/>");
                if (box.Text == "SI")
                {

                    list.SelectedIndex = 0;
                }
                else
                {

                    list.SelectedIndex = 1;
                }

                e.Row.Cells[5].Controls.AddAt(3, list);
               
            }
           
        }
        if (e.Row.Cells[6].Controls.Count > 0)
        {
            CheckBox box3 = (CheckBox)e.Row.Cells[6].Controls[1];
            if (e.Row.Cells[3].Text.Equals("S") && !this.idQuadro.Equals(-1))
            {
                box3.Checked = true;
            }
            this.lb_gv.Text = this.lb_gv.Text + "document.form1." + box3.UniqueID + ".checked=false;";
        }
        if (e.Row.Cells[7].Controls.Count > 0)
        {
            TextBox box4 = (TextBox)e.Row.Cells[7].Controls[1];
            if (!this.idQuadro.Equals(-1) && !e.Row.Cells[4].Text.Equals("&nbsp;"))
            {
                box4.Text = e.Row.Cells[4].Text;
            }
            this.lb_gv.Text = this.lb_gv.Text + "document.form1." + box4.UniqueID + ".value='';";
        }
        e.Row.Cells[2].Visible = false;
        e.Row.Cells[3].Visible = false;
        e.Row.Cells[4].Visible = false;
        if (e.Row.RowIndex >= 0)
        {
            if (alfabeto.IndexOf(e.Row.Cells[1].Text.Substring(0, 1)) == -1)
            {
                alfabeto += e.Row.Cells[1].Text.Substring(0, 1);
                e.Row.Cells[1].Text = "<b style='font-size:16px;'>" + e.Row.Cells[1].Text.Substring(0, 1) + "</b><br/><br/><br/><span></span>" + e.Row.Cells[1].Text;
            }
        }
    }
    private bool ApportaModificheIMA(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_IMA";
        }
        else
        {
            str = "modifica_IMA";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_IMA.Text.Trim()));
            //command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_IMA_Documentazione_Ecg.Text.Trim());
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_IMA_Documentazione_Ecg_ch.Checked);
            //command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_IMA_Documentazione_Eco.Text.Trim());
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_IMA_Documentazione_Eco_ch.Checked);
            //command.Parameters.AddWithValue("@Coronografia", this.Rbl_Eziologia_IMA_Documentazione_Coronogr.Text.Trim());
            command.Parameters.AddWithValue("@Coronografia", this.Rbl_Eziologia_IMA_Documentazione_Coronogr_ch.Checked);
            //command.Parameters.AddWithValue("@Scintigrafia", this.Rbl_Eziologia_IMA_Documentazione_Scintigrafia.Text.Trim());
            command.Parameters.AddWithValue("@Scintigrafia", this.Rbl_Eziologia_IMA_Documentazione_Scintigrafia_ch.Checked);
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        return flag;
    }

    private bool ApportaModificheOspedalizzazioni(SqlTransaction trans, SqlConnection conn, TextBox cld1, TextBox cld2, TextBox text1, DropDownList drop1, TextBox text2, DropDownList drop2, int idospedalizzazione)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_ospedalizzazione";
        }
        else
        {
            str = "modifica_ospedalizzazione";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
                command.Parameters.AddWithValue("@IdOspedalizzazioneScomp", idospedalizzazione);
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@DataAccesso", Utility.CodificaData(cld1.Text.Trim()));
            command.Parameters.AddWithValue("@DataUscita", Utility.CodificaData(cld2.Text.Trim()));
            command.Parameters.AddWithValue("@Diagnosi", text1.Text.Trim());
            command.Parameters.AddWithValue("@Terapia", text2.Text.Trim());
            command.Parameters.AddWithValue("@IdClasseNYHADim", Convert.ToInt32(drop2.SelectedValue));
            command.Parameters.AddWithValue("@IdClasseNYHA", Convert.ToInt32(drop1.SelectedValue));
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        return flag;
    }

    private bool ApportaModifichePrecedentiScomp(SqlTransaction trans, SqlConnection conn, TextBox cld1, TextBox text1, TextBox text2, int idprecedentescompenso)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_precedente_scompenso";
        }
        else
        {
            str = "modifica_precedente_scompenso";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
                command.Parameters.AddWithValue("@IdPrecedentiScompenso", idprecedentescompenso);
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Diagnosi", text1.Text);
            command.Parameters.AddWithValue("@DataAccesso", Utility.CodificaData(cld1.Text.Trim()));
            command.Parameters.AddWithValue("@Terapia", text2.Text);
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        return flag;
    }

    private bool ApportaModificheRischi(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_rischio";
        }
        else
        {
            str = "modifica_rischio";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@FumoValore", this.Ddl_Fattori_Rischio_Fumo_Grado.Text.Trim());
            //Response.Write("EXFUMO:"+chbExFumo.Checked);
            command.Parameters.AddWithValue("@ExFumo", this.chbExFumo.Checked);

            //command.Parameters.AddWithValue("@FumoData", Utility.CodificaData(this.Cld_Fattori_Rischio_Comorbidita_Fumo.Text.Trim()));
            command.Parameters.AddWithValue("@FumoData", this.Cld_Fattori_Rischio_Comorbidita_Fumo.Text.Trim());
            
            //command.Parameters.AddWithValue("@Diabete1Data", Utility.CodificaData(this.Cld_Fattori_Rischio_Comorbidita_Diabete1.Text.Trim()));
            command.Parameters.AddWithValue("@Diabete1Data", this.Cld_Fattori_Rischio_Comorbidita_Diabete1.Text.Trim());
            
            command.Parameters.AddWithValue("@Diabete1Valore", this.Txt_diabete1_grado.Text.Trim());
            //command.Parameters.AddWithValue("@Diabete2Data", Utility.CodificaData(this.Cld_Fattori_Rischio_Comorbidita_Diabete2.Text.Trim()));
            command.Parameters.AddWithValue("@Diabete2Data", this.Cld_Fattori_Rischio_Comorbidita_Diabete2.Text.Trim());
            command.Parameters.AddWithValue("@Diabete2Valore", this.Txt_diabete2_grado.Text.Trim());
            command.Parameters.AddWithValue("@TerapieDiabete", this.Txt_Terapie_Praticate_Diabete.Text.Trim());
           // command.Parameters.AddWithValue("@IperArtData", Utility.CodificaData(this.Cld_Fattori_Rischio_Comorbidita_Ipert_Arter.Text.Trim()));
            command.Parameters.AddWithValue("@IperArtData", this.Cld_Fattori_Rischio_Comorbidita_Ipert_Arter.Text.Trim());
            
            command.Parameters.AddWithValue("@TipoIperArt", this.Ddl_Ipert_Art_Tipologia.Text.Trim());
            command.Parameters.AddWithValue("@TerapiaIperArt", this.Txt_Terapie_Praticate_Ipert_Art.Text.Trim());
            command.Parameters.AddWithValue("@NormPA", this.Rbl_Ipert_Art_Normalizzazione.Text.Trim());
            //command.Parameters.AddWithValue("@LDLMag130", this.Rbl_Iperdislipidemia_LDL_Superiore_130.Text.Trim());
            command.Parameters.AddWithValue("@LDLMag130", this.Rbl_Iperdislipidemia_LDL_Superiore_130_ch.Checked);
            command.Parameters.AddWithValue("@HDLMin35", this.Rbl_Iperdislipidemia_HDL_Inferiore_35.Text.Trim());

            command.Parameters.AddWithValue("@RPCO", this.Rbl_rpco.Text.Trim());
            command.Parameters.AddWithValue("@ANEMIA", this.Rbl_Anemia.Text.Trim());
            command.Parameters.AddWithValue("@ANEMIAHB", this.chbHbAnemia.Checked);

            //command.Parameters.AddWithValue("@TriglMag170", this.Rbl_Iperdislipidemia_Trigl_Superiore_170.Text.Trim());
            //Response.Write("TRIGL:" + Rbl_Iperdislipidemia_Trigl_Superiore_170_ch.Checked);
            command.Parameters.AddWithValue("@TriglMag170", Rbl_Iperdislipidemia_Trigl_Superiore_170_ch.Checked);
            //command.Parameters.AddWithValue("@Statine", this.Rbl_Iperdislipidemia_Statine.Text.Trim());
            command.Parameters.AddWithValue("@Statine", this.Rbl_Iperdislipidemia_Statine_ch.Checked);
            command.Parameters.AddWithValue("@TipoFarmaco", this.Txt_Iperdislipidemia_Tipo_Farmaco.Text.Trim());
            if (this.Rbl_Iperdislipidemia_LDL_Dosi.SelectedValue == "3")
            {
                command.Parameters.AddWithValue("@LDLDopoMag130", "True");
                command.Parameters.AddWithValue("@LDLDopo100tra130", "False");
                command.Parameters.AddWithValue("@LDLDopoMin100", "False");
            }
            if (this.Rbl_Iperdislipidemia_LDL_Dosi.SelectedValue == "1")
            {
                command.Parameters.AddWithValue("@LDLDopoMag130", "False");
                command.Parameters.AddWithValue("@LDLDopo100tra130", "True");
                command.Parameters.AddWithValue("@LDLDopoMin100", "False");
            }
            if (this.Rbl_Iperdislipidemia_LDL_Dosi.SelectedValue == "2")
            {
                command.Parameters.AddWithValue("@LDLDopoMag130", "False");
                command.Parameters.AddWithValue("@LDLDopo100tra130", "False");
                command.Parameters.AddWithValue("@LDLDopoMin100", "True");
            }
            if (this.Rbl_Iperdislipidemia_LDL_Dosi.SelectedValue == "")
            {
                command.Parameters.AddWithValue("@LDLDopoMag130", "False");
                command.Parameters.AddWithValue("@LDLDopo100tra130", "False");
                command.Parameters.AddWithValue("@LDLDopoMin100", "False");
            }
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        return flag;
    }

    private bool ApportaModificheValvolopatia(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_Valvolopatia";
        }
        else
        {
            str = "modifica_Valvolopatia";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_Valvolopatie.Text.Trim()));
            //command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Valvolopatie_Document_Ecg.Text);
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Valvolopatie_Document_Ecg_ch.Checked);
            //command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Valvolopatie_Document_Eco.Text);
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Valvolopatie_Document_Eco_ch.Checked);
            //command.Parameters.AddWithValue("@Cardiomiopatia", this.chb_cardiomiopatia.Checked);
            command.Parameters.AddWithValue("@TipoValvolopatia", this.Ddl_Eziologia_Valvolopatie_Tipo.SelectedValue);
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        return flag;
    }

    private bool CercaDuplicati()
    {
        string str;
        bool flag = false;
        if (!this.IdPaziente.Equals(-1))
        {
            str = "cerca_paziente_id_dati";
        }
        else
        {
            str = "cerca_paziente_dati";
        }
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand(str, connection);
            command.CommandType = CommandType.StoredProcedure;
            if (!this.IdPaziente.Equals(-1))
            {
                command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            }
            command.Parameters.AddWithValue("@Nome", this.Txt_Nome.Text.Trim());
            command.Parameters.AddWithValue("@Cognome", this.Txt_Cognome.Text.Trim());
            command.Parameters.AddWithValue("@CodiceFiscale", this.Txt_Codice_Fiscale.Text.Trim());
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = true;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool controllaformati()
    {
        bool flag = false;
        if (!Utility.IsNotNegInteger(this.Txt_Altezza.Text))
        {
            this.lb_mess.Text = this.lb_mess.Text + "<br>L'altezza deve essere espressa in cm senza virgole o punti";
            flag = true;
        }
        if (!Utility.IsNotNegInteger(this.Txt_Eta.Text))
        {
            this.lb_mess.Text = this.lb_mess.Text + "<br>L'eta deve essere espressa in formato numerico";
            flag = true;
        }
        if (!Utility.IsNotNegInteger(this.Txt_Peso1.Text))
        {
            this.lb_mess.Text = this.lb_mess.Text + "<br>Il peso deve essere espresso in kg prima della virgola";
            flag = true;
        }
        if (!Utility.IsNotNegInteger(this.Txt_Circonferenza_Addominale.Text))
        {
            this.lb_mess.Text = this.lb_mess.Text + "<br>La circonferenza addominale deve essere espresso in cm senza virgole o punti";
            flag = true;
        }
        return flag;
    }

    protected void CreaMenuStep(string current_step_id)
    {
        this.lb_steps.Text = "<div class=\"menu_step\"><ul>";
        foreach (Control control in this.Wizard1.WizardSteps)
        {
            if (control.GetType().ToString().Equals("System.Web.UI.WebControls.WizardStep"))
            {
                WizardStep step = (WizardStep)control;
                if (step.ID.Equals(current_step_id))
                {
                    this.lb_steps.Text = this.lb_steps.Text + "<li class=\"current\"><span>" + step.Title + "</span></li>";
                }
                else
                {
                    this.lb_steps.Text = this.lb_steps.Text + "<li><span>" + step.Title + "</span></li>";
                }
            }
        }
        this.lb_steps.Text = this.lb_steps.Text + "</ul></div>";
    }

    private void ElencaMedici()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
           // SqlCommand command = new SqlCommand("elenca_medici", connection);
            SqlCommand command = new SqlCommand("elenca_medici_mmg", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
             ListItem item = new ListItem("[Nessuno]","-1");
             this.Medico.Items.Add(item);
            while (reader.Read())
            {
                 item = new ListItem(reader["Nominativo"].ToString(), reader["idUtente"].ToString());
                this.Medico.Items.Add(item);
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
    }

    private void ElencaTipoValvolopatie()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("elenca_valvolopatietipo", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                ListItem item = new ListItem(reader["TipoValvolopatia"].ToString(), reader["IdTipoValvolopatia"].ToString());
                this.Ddl_Eziologia_Valvolopatie_Tipo.Items.Add(item);
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
    }
    public bool getValoreCheck(string valore)
    {
        if ((valore == "1")||(valore=="True"))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
    private bool Estrai_Altre_Eziologie()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Eziologie", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Eziologia_Altre_Eziologie.Text = Utility.DecodificaData(reader["DataPrimoRilievo"].ToString());
                //this.Rbl_Eziologia_Altre_Eziologie_Document_Ecg.SelectedValue = reader["DocumentECG"].ToString();
                this.Rbl_Eziologia_Altre_Eziologie_Document_Ecg_ch.Checked = getValoreCheck( reader["DocumentECG"].ToString());
                //this.Rbl_Eziologia_Altre_Eziologie_Document_Eco.SelectedValue = reader["DocumentECO"].ToString();
                this.Rbl_Eziologia_Altre_Eziologie_Document_Eco_ch.Checked = getValoreCheck(reader["DocumentECO"].ToString());
                this.Txt_Altre_Eziologie_Denominazione.Text = reader["Denominazione"].ToString();
                this.Txt_Altre_Eziologie_Descrizione.Text = reader["Descrizione"].ToString();
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Cardiomiopatia_Dilatativa()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Cardiomiopatia_Dilatativa", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Eziologia_Cardiomiopatia_Dilatativa.Text = Utility.DecodificaData(reader["DataPrimoRilievo"].ToString());
                //this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Ecg.SelectedValue = reader["ECG"].ToString();
                this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Ecg_ch.Checked = getValoreCheck(reader["ECG"].ToString());
                //this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Eco.SelectedValue = reader["ECO"].ToString();
                this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Eco_ch.Checked = getValoreCheck(reader["ECO"].ToString());
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Cardiomiopatia_Ipertrofica()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Cardiomiopatia_Ipertrofica", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Eziologia_Cardiomiopatia_Ipertrofica.Text = Utility.DecodificaData(reader["DataPrimoRilievo"].ToString());
                //this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Ecg.SelectedValue = reader["DocumentazioneECG"].ToString();
                this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Ecg_ch.Checked = getValoreCheck( reader["DocumentazioneECG"].ToString());
                //this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Eco.SelectedValue = reader["DocumentazioneECO"].ToString();
                this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Eco_ch.Checked = getValoreCheck(reader["DocumentazioneECO"].ToString());
                this.Ddl_Eziologia_Cardiomiopatia_Ipertrofica_Ostruttiva.SelectedValue = reader["Ostruttiva"].ToString();
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_CARDIOP_ISCHEM_SENZA_DOCUMENT_IMA()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_CARDIOP_ISCHEM_SENZA_DOCUMENT_IMA", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Eziologia_Cardiop_Ischem.Text = Utility.DecodificaData(reader["DataPrimoRilievo"].ToString());
                //this.Rbl_Eziologia_Cardiop_Document_ECG.SelectedValue = reader["ECG"].ToString();
                this.Rbl_Eziologia_Cardiop_Document_ECG_ch.Checked = getValoreCheck( reader["ECG"].ToString());
                //this.Rbl_Eziologia_Cardiop_Document_ECO.SelectedValue = reader["ECO"].ToString();
                this.Rbl_Eziologia_Cardiop_Document_ECO_ch.Checked = getValoreCheck( reader["ECO"].ToString());
                //this.Rbl_Eziologia_Cardiop_Document_Scintigrafia.SelectedValue = reader["Scintigrafia"].ToString();
                this.Rbl_Eziologia_Cardiop_Document_Scintigrafia_ch.Checked = getValoreCheck( reader["Scintigrafia"].ToString());
                //this.Rbl_Eziologia_Cardiop_Document_Coronarogr.SelectedValue = reader["Coronografia"].ToString();
                this.Rbl_Eziologia_Cardiop_Document_Coronarogr_ch.Checked = getValoreCheck( reader["Coronografia"].ToString());
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Dati_Precedenti_Ospedalizzazioni_Per_Scompenso()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Dati_Precedenti_Ospedalizzazioni_Per_Scompenso", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            {
                                this.Cld1_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataAccesso"].ToString());
                                this.Cld_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataUscita"].ToString());
                                this.Txt_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Diagnosi"].ToString();
                                this.Txt_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Terapia"].ToString();
                                int num2 = Convert.ToInt32(reader["IdClasseNYHA"].ToString());
                                int num3 = Convert.ToInt32(reader["IdClasseNYHADim"].ToString());
                                this.Ddl_Classe_NYHA_Al_Ricovero.SelectedValue = num2.ToString();
                                this.Ddl_Classe_NYHA_Alla_Dimissione.SelectedValue = num3.ToString();
                                break;
                            }
                        case 1:
                            {
                                this.Cld2_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataAccesso"].ToString());
                                this.Cld2_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataUscita"].ToString());
                                this.Txt2_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Diagnosi"].ToString();
                                this.Txt2_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Terapia"].ToString();
                                int num4 = Convert.ToInt32(reader["IdClasseNYHA"].ToString());
                                int num5 = Convert.ToInt32(reader["IdClasseNYHADim"].ToString());
                                this.Ddl2_Classe_NYHA_Al_Ricovero.SelectedValue = num4.ToString();
                                this.Ddl2_Classe_NYHA_Alla_Dimissione.SelectedValue = num5.ToString();
                                break;
                            }
                        case 2:
                            {
                                this.Cld3_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataAccesso"].ToString());
                                this.Cld3_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataUscita"].ToString());
                                this.Txt3_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Diagnosi"].ToString();
                                this.Txt3_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Terapia"].ToString();
                                int num6 = Convert.ToInt32(reader["IdClasseNYHA"].ToString());
                                int num7 = Convert.ToInt32(reader["IdClasseNYHADim"].ToString());
                                this.Ddl3_Classe_NYHA_Al_Ricovero.SelectedValue = num6.ToString();
                                this.Ddl3_Classe_NYHA_Alla_Dimissione.SelectedValue = num7.ToString();
                                break;
                            }
                    }
                }
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_IMA()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_IMA", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Eziologia_IMA.Text = Utility.DecodificaData(reader["Data"].ToString());
                //this.Rbl_Eziologia_IMA_Documentazione_Ecg.SelectedValue = reader["ECG"].ToString();
                this.Rbl_Eziologia_IMA_Documentazione_Ecg_ch.Checked = getValoreCheck( reader["ECG"].ToString());
                //this.Rbl_Eziologia_IMA_Documentazione_Eco.SelectedValue = reader["ECO"].ToString();
                this.Rbl_Eziologia_IMA_Documentazione_Eco_ch.Checked = getValoreCheck(reader["ECO"].ToString());
                //this.Rbl_Eziologia_IMA_Documentazione_Scintigrafia.SelectedValue = reader["Scintigrafia"].ToString();
                this.Rbl_Eziologia_IMA_Documentazione_Scintigrafia_ch.Checked = getValoreCheck( reader["Scintigrafia"].ToString());
                //this.Rbl_Eziologia_IMA_Documentazione_Coronogr.SelectedValue = reader["Coronografia"].ToString();
                this.Rbl_Eziologia_IMA_Documentazione_Coronogr_ch.Checked = getValoreCheck( reader["Coronografia"].ToString());
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool estrai_NYHA(DropDownList dropdown)
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        bool flag = false;
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_NYHA", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            for (int i = 0; reader.Read(); i++)
            {
                ListItem item = new ListItem(reader["Denominazione"].ToString(), reader["idClasseNYHA"].ToString());
                dropdown.Items.Add(item);
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Precedenti_Accessi_Per_Scompenso()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Dati_Precedenti_Accessi_Per_Scompenso", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            this.Cld_Precedenti_Accessi_Scompenso.Text = Utility.DecodificaData(reader["DataRicovero"].ToString());
                            this.Txt_Diagnosi_Precedenti_Accessi_Scompenso.Text = reader["Diagnosi"].ToString();
                            this.Txt_Terapia_Precedenti_Accessi_Scompenso.Text = reader["TerapiaDimissione"].ToString();
                            break;

                        case 1:
                            this.Cld2_Precedenti_Accessi_Scompenso.Text = Utility.DecodificaData(reader["DataRicovero"].ToString());
                            this.Txt2_Diagnosi_Precedenti_Accessi_Scompenso.Text = reader["Diagnosi"].ToString();
                            this.Txt2_Terapia_Precedenti_Accessi_Scompenso.Text = reader["TerapiaDimissione"].ToString();
                            break;

                        case 2:
                            this.Cld3_Precedenti_Accessi_Scompenso.Text = Utility.DecodificaData(reader["DataRicovero"].ToString());
                            this.Txt3_Diagnosi_Precedenti_Accessi_Scompenso.Text = reader["Diagnosi"].ToString();
                            this.Txt3_Terapia_Precedenti_Accessi_Scompenso.Text = reader["TerapiaDimissione"].ToString();
                            break;
                    }
                }
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Rischi()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Rischi", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                //this.Cld_Fattori_Rischio_Comorbidita_Fumo.Text = Utility.DecodificaData(reader["FumoData"].ToString());
                this.Cld_Fattori_Rischio_Comorbidita_Fumo.Text = reader["FumoData"].ToString();
                
                this.Ddl_Fattori_Rischio_Fumo_Grado.SelectedValue = reader["FumoValore"].ToString();
                //this.Cld_Fattori_Rischio_Comorbidita_Diabete1.Text = Utility.DecodificaData(reader["Diabete1Data"].ToString());
                this.Cld_Fattori_Rischio_Comorbidita_Diabete1.Text = reader["Diabete1Data"].ToString();
                
                //this.Cld_Fattori_Rischio_Comorbidita_Diabete2.Text = Utility.DecodificaData(reader["Diabete2Data"].ToString());
                this.Cld_Fattori_Rischio_Comorbidita_Diabete2.Text = reader["Diabete2Data"].ToString();
               
                this.Txt_diabete1_grado.Text = reader["Diabete1Valore"].ToString();
                this.Txt_diabete2_grado.Text = reader["Diabete2Valore"].ToString();
                this.Txt_Terapie_Praticate_Diabete.Text = reader["TerapieDiabete"].ToString();
                //this.Cld_Fattori_Rischio_Comorbidita_Ipert_Arter.Text = Utility.DecodificaData(reader["IperArtData"].ToString());
                this.Cld_Fattori_Rischio_Comorbidita_Ipert_Arter.Text = reader["IperArtData"].ToString();
                
                this.Ddl_Ipert_Art_Tipologia.SelectedValue = reader["TipoIperArt"].ToString();
                this.Txt_Terapie_Praticate_Ipert_Art.Text = reader["TerapiaIperArt"].ToString();
                //this.Rbl_Ipert_Art_Normalizzazione.SelectedValue = reader["NormPA"].ToString();
                this.Rbl_Ipert_Art_Normalizzazione.Checked = getValoreCheck( reader["NormPA"].ToString());
                this.chbExFumo.Checked = getValoreCheck(reader["ExFumo"].ToString());
                //this.Rbl_Iperdislipidemia_LDL_Superiore_130.SelectedValue = reader["LDLMag130"].ToString();
                this.Rbl_Iperdislipidemia_LDL_Superiore_130_ch.Checked = getValoreCheck( reader["LDLMag130"].ToString());
                this.Rbl_Iperdislipidemia_HDL_Inferiore_35.SelectedValue = reader["HDLMin35"].ToString();

                this.Rbl_rpco.SelectedValue = reader["RPCO"].ToString();
                this.Rbl_Anemia.SelectedValue = reader["ANEMIA"].ToString();
              
                this.chbHbAnemia.Checked = getValoreCheck(reader["ANEMIAHB"].ToString());
                Rbl_Anemia_SelectedIndexChanged(null, null);
                //this.Rbl_Iperdislipidemia_Trigl_Superiore_170.SelectedValue = reader["TriglMag170"].ToString();
                this.Rbl_Iperdislipidemia_Trigl_Superiore_170_ch.Checked = getValoreCheck( reader["TriglMag170"].ToString());
                //this.Rbl_Iperdislipidemia_Statine.SelectedValue = reader["Statine"].ToString();
                this.Rbl_Iperdislipidemia_Statine_ch.Checked = getValoreCheck( reader["Statine"].ToString());
                this.Txt_Iperdislipidemia_Tipo_Farmaco.Text = reader["TipoFarmaco"].ToString();
                string str = "0";
                if (reader["LDLDopo100tra130"].ToString() == "True")
                {
                    str = "1";
                }
                if (reader["LDLDopoMin100"].ToString() == "True")
                {
                    str = "2";
                }
                if (reader["LDLDopoMag130"].ToString() == "True")
                {
                    str = "3";
                }
                this.Rbl_Iperdislipidemia_LDL_Dosi.SelectedValue = str;
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Valvolopatie()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Valvolopatie", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                return false;
            }
            reader.Read();
            this.Cld_Eziologia_Valvolopatie.Text = Utility.DecodificaData(reader["DataPrimoRilievo"].ToString());
           // this.Rbl_Eziologia_Valvolopatie_Document_Ecg.SelectedValue = reader["DocumentECG"].ToString();
            this.Rbl_Eziologia_Valvolopatie_Document_Ecg_ch.Checked = getValoreCheck( reader["DocumentECG"].ToString());
            //this.Rbl_Eziologia_Valvolopatie_Document_Eco.SelectedValue = reader["DocumentECO"].ToString();
            //this.Rbl_Eziologia_Valvolopatie_Document_Eco_ch.Checked = getValoreCheck( reader["DocumentECO"].ToString());
            //this.chb_cardiomiopatia.Checked = getValoreCheck(reader["Cardiomiopatia"].ToString());
            int num = Convert.ToInt32(reader["TipoValvolopatia"]);
            reader.Close();
            SqlCommand command2 = new SqlCommand("Estrai_Valvolopatie_Tipo", connection);
            command2.CommandType = CommandType.StoredProcedure;
            command2.Parameters.AddWithValue("@IdTipo", num);
            SqlDataReader reader2 = command2.ExecuteReader();
            if (reader2.HasRows)
            {
                reader2.Read();
                this.Ddl_Eziologia_Valvolopatie_Tipo.SelectedValue = reader2["IdTipoValvolopatia"].ToString();
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader2.Close();
            return flag;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool EstraiDati()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("cerca_paziente_id", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    this.Cld_Data_Registrazione.Text = Utility.DecodificaData(reader["DataRegistrazione"].ToString());
                    this.Txt_Nome.Text = reader["Nome"].ToString();
                    this.Txt_Cognome.Text = reader["Cognome"].ToString();
                    this.Txt_Telefono.Text = reader["Telefoni"].ToString();
                    this.Txt_Indirizzo.Text = reader["Indirizzo"].ToString();
                    this.Txt_Codice_Fiscale.Text = reader["CodiceFiscale"].ToString();
                    this.Txt_Eta.Text = reader["Eta"].ToString();
                    this.Txt_Altezza.Text = reader["Altezza"].ToString();
                    this.Txt_Peso1.Text = reader["Peso"].ToString();
                    this.Txt_Circonferenza_Addominale.Text = reader["CircAddominale"].ToString();
                    this.Sesso.SelectedValue = reader["Sesso"].ToString();
                    this.Medico.SelectedValue = reader["IdMedicoCurante"].ToString();
                    if (!IsPostBack)
                        this.lb_titolo.Text = "Paziente: <b><a href='HomePageTipo.aspx?idPaziente=" + reader["idPaziente"] + "'>" + reader["Cognome"].ToString() + " " + reader["Nome"].ToString() + "</a></b><br>Reclutato il <b>" + Utility.DecodificaData(reader["DataRegistrazione"].ToString()) + "</b>";

                    if (Session["IdUtente"].ToString() != reader["IdUtente"].ToString())
                    {
                        if ((this.Session["Utente"].ToString() != "cardio") && (this.Session["Utente"].ToString() != "Amministratore"))
                        {
                            LinkButton1.Enabled = false;
                            LinkButton1.Text = "Non puoi effettuare modifiche a questo controllo";
                            LinkButton2.Enabled = false;
                            LinkButton2.Text = "Non puoi effettuare modifiche a questo controllo";
                            Wizard1.Enabled = false;
                            //txtDataControllo.Enabled = false;
                            //imgCal.Style.Add(HtmlTextWriterStyle.Display, "none");

                        }
                    }
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }
    private bool estrai_quadro()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
           
            SqlCommand command = new SqlCommand("estrai_reclutamento", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            //Page.RegisterStartupScript("ciao", "<script>alert('Estraggo');</script>");
            //Response.Write("Estraggo QUADRO:" + reader.HasRows + " PAZIENTE:" + this.IdPaziente);
            if (reader.HasRows)
            {
                reader.Read();
              
                //this.Cld_Quadro_Clinico_Generale.Text = "";//Utility.DecodificaData(reader["DataQuadroClinico"].ToString());
                /*string tipo = "";
                switch (reader["tipo"].ToString())
                {
                    case "admin":
                        tipo = " ADMIN";

                        break;
                    case "Medico di medicina generale":
                        tipo = " MMG";

                        break;
                    case "Ospedaliero":
                        tipo = " OSPEDALIERO";

                        break;
                    case "Cardiologo ambulatoriale":
                        tipo = " SPECIALIST";

                        break;
                }*/
                this.idQuadro = int.Parse(reader["IdQuadroClinico"].ToString());
                lblQuadro.Text = reader["IdQuadroClinico"].ToString();
                //Response.Write("CODICE QUADRO:" + lblQuadro.Text + "   " + this.idQuadro);
                //this.lb_titolo.Text += "<br>Controllo <b>" + tipo + "</b> del <b>" + Utility.DecodificaData(reader["DataQuadroClinico"].ToString()) + "</b><br />";
                //txtDataControllo.Text = Utility.DecodificaData(reader["DataQuadroClinico"].ToString());
                //this.Quadro_Clinico_Obesita.SelectedValue = reader["Obesita"].ToString();
                //this.Quadro_Clinico_Sovrappeso.SelectedValue = reader["Sovrappeso"].ToString();
               // this.Rbl_Quadro_Clinico_Obesita_Add.SelectedValue = reader["ObesitaUnifAddom"].ToString();
                //this.Cld_Quadro_Clinico_Data_Obesita_Add.Text = Utility.DecodificaData(reader["ObesitaUnifAddomData"].ToString());
                //this.Rbl_Quadro_Clinico_Iperglicemia.SelectedValue = reader["Iperglicemia"].ToString();
                //this.Cld_Quadro_Clinico_Data_Iperglicemia.Text = Utility.DecodificaData(reader["IperglicemiaData"].ToString());
                //this.Rbl_Quadro_Clinico_Ipertensione_Arteriosa.SelectedValue = reader["IpertArt"].ToString();
                //this.Cld_Quadro_Clinico_Data_Ipertensione_Arteriosa.Text = Utility.DecodificaData(reader["IpertArtData"].ToString());
                //this.Rbl_Quadro_Clinico_Iperslidemia.SelectedValue = reader["IperDislitemia"].ToString();
                //this.Cld_Quadro_Clinico_Data_Iperdislipidemia.Text = Utility.DecodificaData(reader["IperDislitemiaData"].ToString());
                //this.Rbl_Quadro_Clinico_BPCO.SelectedValue = reader["BPCO"].ToString();
                //this.Cld_Quadro_Clinico_Data_BPCO.Text = Utility.DecodificaData(reader["BPCOData"].ToString());
                //this.Rbl_Quadro_Clinico_Asma_Bronchiale.SelectedValue = reader["AsmaBronchiale"].ToString();
                //this.Cld_Quadro_Clinico_Data_Asma_Bronchiale.Text = Utility.DecodificaData(reader["AsmaBronchialeData"].ToString());
                this.Rbl_Quadro_Clinico_Insufficienza_Renale.SelectedValue = reader["InsuffRenale"].ToString();
                //this.Cld_Quadro_Clinico_Data_Insufficienza_Renale.Text = Utility.DecodificaData(reader["InsuffRenaleData"].ToString());
                //this.Rbl_Quadro_Clinico_Anemia.SelectedValue = reader["Anemia"].ToString();
                //txtAnemia.Text = reader["Anemia"].ToString();
                //this.Cld_Quadro_Clinico_Data_Anemia.Text = Utility.DecodificaData(reader["AnemiaData"].ToString());
                //this.Cld_Quadro_Clinico_Data_Inizio_Ipertiroid.Text = Utility.DecodificaData(reader["IpertiroidInizio"].ToString());
                //this.Txt_Quadro_Clinico_Ipertiroid_Terapia.Text = reader["IpertiroTerapia"].ToString();
                //this.Rbl_Ipertiroid_Normalizz.SelectedValue = reader["IpertiroNormInd"].ToString();
                //this.Cld_Quadro_Clinico_Data_Inizio_Ipotiroid.Text = Utility.DecodificaData(reader["Ipotirodinizio"].ToString());
                //this.Txt_Quadro_Clinico_Ipotiroid_Terapia.Text = reader["IpotiroTerapia"].ToString();
                //this.Rbl_Quadro_Clinico_Ipotiroid_Normalizzaz_Indici_Lab.SelectedValue = reader["Ipotironormind"].ToString();
                this.Cld_Data_Anamnesi_Scompenso_Attuale.Text = Utility.DecodificaData(reader["DataInizioscmPensoAttuale"].ToString());
                try
                {
                    this.txtDataProxControllo.Text = Utility.DecodificaData(reader["DataProssimoQuadroClinico"].ToString());
                }
                catch (Exception ex) { 
                
                }
                try
                {
                    txtEdPolAcuto_ch.Checked = getValoreCheck(reader["EdPolAcuto"].ToString());
                }
                catch (Exception ex) { }
                //txtEdPolAcuto.Text = reader["EdPolAcuto"].ToString();
                txtPADiastolica.Text = reader["gPADiastolica"].ToString();
                txtPASistolica.Text = reader["gPASistolica"].ToString();
                // this.Rbl_Anamnesi_Ed_Pol_Acuto.SelectedValue = reader["EdPolAcuto"].ToString();
                this.Txt_Anamnesi_Dispnea_Nott.Text = reader["NEpisodiDispnea"].ToString();
                this.Ddl_Anamnesi_Dispnea_Sforzo.SelectedValue = reader["ClasseDispneaSforzo"].ToString();
                this.ddl_Anamnesi_Astenia.SelectedValue = reader["Astenia"].ToString();
                this.Txt_Anamnesi_N_Cuscini.Text = reader["Ncuscini"].ToString();
                try
                {
                    Txt_Anamnesi_Sleep_ch.Checked = getValoreCheck(reader["SleepApnea"].ToString());
                }
                catch (Exception ex) { }
                // this.Txt_Anamnesi_Sleep.Text = reader["SleepApnea"].ToString();
                this.RBL_Anamnesi_Contraz_Diuresi.SelectedValue = reader["ContrazDiuresi"].ToString();
                //this.Txt_Anamnesi_Rapido_Increm_Peso.Text = reader["RecRapidoIncrementoPeso"].ToString();
                //this.Txt_Anamnesi_Cardiopalmo.Text = reader["Cardiopalmo"].ToString();
                //this.Txt_Anamnesi_Pa_Ingresso.Text = reader["PaIngresso"].ToString();
                //this.Txt_Anamnesi_Pa_Dimissione.Text = reader["PaDimiss"].ToString();
                this.Txt_Anamnesi_Fc_Ingresso.Text = reader["FCIngresso"].ToString();
                //this.Txt_Anamnesi_Fc_Dimissione.Text = reader["FcDimissione"].ToString();
                this.Txt_Anamnesi_Polso_Ingresso.Text = reader["PolsoIngresso"].ToString();
                this.Rbl_Anamnesi_Polso_Ingresso.SelectedValue = reader["PolsoIngrRitmo"].ToString();
                //this.Txt_Anamnesi_Polso_Dimissione.Text = reader["PolsoDimissione"].ToString();
                //this.Rbl_Anamnesi_Polso_Dimissione.SelectedValue = reader["PolsoDimRitmo"].ToString();
                //this.Rbl_Anamnesi_Cianosi.SelectedValue = reader["Cianosi"].ToString();
                this.Rbl_Anamnesi_Toni_Cardiaci.SelectedValue = reader["ToniValidiRidotti"].ToString();
                this.ddl_Anamnesi_Descrizione_Toni_Cardiaci.SelectedValue = reader["DescrizioneToniCardiaci"].ToString();
                try
                {
                    Rbl_Anamnesi_Soffi_ch.Checked = getValoreCheck(reader["Soffi"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Anamnesi_Soffi.SelectedValue = reader["Soffi"].ToString();
                this.Txt_Anamnesi_Soffi_Descrizione.Text = reader["DescrizioneSoffi"].ToString();
                this.Rbl_Anamnesi_Congestione_Polm.SelectedValue = reader["CongestionePolmonare"].ToString();
                try
                {
                    txtGiugulari_ch.Checked = getValoreCheck(reader["CongestionePerifGiugularimag3"].ToString());
                }
                catch (Exception ex) { }
                //txtGiugulari.Text = reader["CongestionePerifGiugularimag3"].ToString();
                //this.Rbl_Anamnesi_Congestione_Perif_Giugulari.SelectedValue = reader["CongestionePerifGiugularimag3"].ToString();
                try
                {
                    txtEpatomegalia_ch.Checked = getValoreCheck(reader["CongestionePerifEpatomegalia"].ToString());
                }
                catch (Exception ex) { }
                //txtEpatomegalia.Text = reader["CongestionePerifEpatomegalia"].ToString();
                //this.Rbl_Anamnesi_Congestione_Perif_Epatomegalia.SelectedValue = reader["CongestionePerifEpatomegalia"].ToString();
                this.Ddl_Anamnesi_Edemi_Periferici.SelectedValue = reader["CongestionePerifEdemiPeriferici"].ToString();
                txtVersamentoPleurico.Text = reader["CongestionePerifVersamentoPleurico"].ToString();
                //this.Rbl_Anamnesi_Congestione_Perif_Versamento_Pleurico.SelectedValue = reader["CongestionePerifVersamentoPleurico"].ToString();
                try
                {
                    txtAscite_ch.Checked = getValoreCheck(reader["CongestionePerifAscite"].ToString());
                }
                catch (Exception ex) { }
                //txtAscite.Text = reader["CongestionePerifAscite"].ToString(); 
                //this.Rbl_Anamnesi_Congestione_Perif_Ascite.SelectedValue = reader["CongestionePerifAscite"].ToString();
                txtStenosiAortica.Text = reader["gStenosiAortica"].ToString();
                txtStenosiMitralica.Text = reader["gStenosiMitralica"].ToString();
                try
                {
                    Rbl_Anamnesi_Polsi_Arter_Presenti_ch.Checked = getValoreCheck(reader["PolsiArterPresenti"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Anamnesi_Polsi_Arter_Presenti.SelectedValue = reader["PolsiArterPresenti"].ToString();
                this.Txt_Anamnesi_Polsi_Arter_Non_Percepibili.Text = reader["PolsiArterNoPerc"].ToString();
                try
                {
                    Rbl_Anamnesi_Polsi_Arter_Doppler_ch.Checked = getValoreCheck(reader["DocumentDoppler"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Anamnesi_Polsi_Arter_Doppler.SelectedValue = reader["DocumentDoppler"].ToString();
                this.Cld_Aritmie_Ex_Sopraventricol.Text = Utility.DecodificaData(reader["ExtraSopraVentrDataRisc"].ToString());
                try
                {
                    txtDocECGSopraventricol_ch.Checked = getValoreCheck(reader["ExtraSopraVentrECG"].ToString());
                }
                catch (Exception ex) { }
                //txtDocECGSopraventricol.Text= reader["ExtraSopraVentrECG"].ToString();
                //this.Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg.SelectedValue = reader["ExtraSopraVentrECG"].ToString();
                try
                {
                    txtECGHolterSopraventricol_ch.Checked = getValoreCheck(reader["ExtraSopraVentrECGHolter"].ToString());
                }
                catch (Exception ex) { }
                //txtECGHolterSopraventricol.Text = reader["ExtraSopraVentrECGHolter"].ToString();
                //this.Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg_Holter.SelectedValue = reader["ExtraSopraVentrECGHolter"].ToString();
                //this.Txt_Aritmie_Ex_Sopraventtricol_Terapia.Text = reader["ExtraSopraVentrTerapia"].ToString();
                //this.Cld_Aritmie_Ex_Ventricol.Text = Utility.DecodificaData(reader["ExVentricolDataRisc"].ToString());
                //this.Rbl_Aritmie_Ex_Ventricol_Document_Ecg.SelectedValue = reader["ExVentricolEcg"].ToString();
                //this.Rbl_Aritmie_Ex_Ventricol_Document_Ecg_Holter.SelectedValue = reader["ExVentricolEcgHolter"].ToString();
                this.Txt_Aritmie_Ex_Ventricol_Semplici.Text = reader["ExVentricolSempliciN"].ToString();
                this.Txt_Aritmie_Ex_Ventricol_Coppie_Triplette.Text = reader["ExVentricolCoppieTripleN"].ToString();
                this.Txt_Aritmie_Ex_Ventricol_Tv_Non_Sostenuta.Text = reader["ExVentricolTVNonSostenutaN"].ToString();
                //this.Txt_Aritmie_Ex_Ventricol_Terapia.Text = reader["ExVentricolTerapia"].ToString();
                txtDataCVE.Text = Utility.DecodificaData(reader["gDataCVE"].ToString());
                this.Cld_Aritmie_Fa_Paross.Text = Utility.DecodificaData(reader["FaParadossDataInizio"].ToString());
                this.Txt_Aritmie_FA_Paradoss_Episodi.Text = reader["FAParadossNEpisodi"].ToString();
                this.Txt_Aritmie_FA_Paradoss_Durata_Episodi.Text = reader["FAParadossDurataEpisodi"].ToString();
                //this.Rbl_Aritmie_Fa_Paradoss_Efficacia.SelectedValue = reader["FAParadossEffTerapia"].ToString();
                this.Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat.SelectedValue = reader["FAParadossAblazTrans"].ToString();

                this.radioTPSV.SelectedValue = reader["TPSV"].ToString();
                this.radioTPSVAblazione.SelectedValue = reader["TPSVAblazione"].ToString();
                this.radioPreeccitazioneTachicardia.SelectedValue = reader["PreeccitazioneTac"].ToString();
                this.radioPreeccitazioneTachicardiaAblazione.SelectedValue = reader["PreeccitazioneTacAblazione"].ToString();

                //this.Txt_Aritmie_FA_Paross_terapia.Text = reader["FAParadossTerapia"].ToString();
                //this.Txt_Aritmie_Fa_Permanente_Terapia.Text = reader["FAPermanenteTerapia"].ToString();
                this.Cld_Aritmie_Fa_Permanente.Text = Utility.DecodificaData(reader["FAPermanenteDataInizio"].ToString());
                this.DataCardioMiopatia.Text = Utility.DecodificaData(reader["DataCardiomiopatia"].ToString());
                try
                {
                    chb_cardiomiopatia.Checked = getValoreCheck(reader["Cardiomiopatia"].ToString());
                 //   Response.Write(chb_cardiomiopatia.Checked + " " + getValoreCheck(reader["Cardiomiopatia"].ToString()));
                }
                catch (Exception ex) {
                  //  Response.Write(ex.Message + " errore imp");
                }
                try
                {
                    Rbl_Aritmie_Cve_ch.Checked = getValoreCheck(reader["CVE"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Aritmie_Cve.SelectedValue = reader["CVE"].ToString();
                this.Rbl_Aritmie_Tv_Sostenuta.SelectedValue = reader["TVSostenutaSintomatica"].ToString();
                this.Cld_Aritmie_Tv_Sostenuta.Text = Utility.DecodificaData(reader["TVSostenutaSintomaticaData"].ToString());
                this.Txt_Aritmie_Tv_Sostenuta_Num_Episodi.Text = reader["TVSostenutaSintomaticaEpisodiN"].ToString();
                this.Txt_Aritmie_Tv_Sostenuta_Durata_Episodi.Text = reader["TVSostenutaSintomaticaDurata"].ToString();
                this.Txt_Aritmie_Tv_Sostenuta_Freq_Ventricol.Text = reader["TVSostenutaSintomaticaFreqVentr"].ToString();
                //this.Txt_Aritmie_Tv_Sostenuta_Sincope.Text = reader["TVSostenutaSincope"].ToString();
                //this.Txt_Aritmie_Tv_Sostenuta_Lipotimia.Text = reader["TVSostenutaSintomaticaLipotimia"].ToString();
                try
                {
                    Rbl_Aritmia_Mi_Resuscitata_ch.Checked = getValoreCheck(reader["MIResuscitata"].ToString());
                }
                catch (Exception ex) { }
                // this.Rbl_Aritmia_Mi_Resuscitata.SelectedValue = reader["MIResuscitata"].ToString();
                try
                {
                    Rbl_Aritmia_Pm_ch.Checked = getValoreCheck(reader["PM"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Aritmia_Pm.SelectedValue = reader["PM"].ToString();
                this.Txt_Aritmie_Pm_Causale_Impianto.Text = reader["PMCausaleImpianto"].ToString();
                this.Cld_Aritmie_Pm.Text = Utility.DecodificaData(reader["PMDataImpianto"].ToString());
                this.Txt_Aritmie_Pm_Tipologia.Text = reader["PMTipologia"].ToString();
                this.Txt_Aritmie_Causale_Impianto.Text = reader["ICDCausaleImpianto"].ToString();
                this.Cld_Aritmie_Icd.Text = Utility.DecodificaData(reader["ICDDataImpianto"].ToString());
                this.Txt_Aritmie_Icd_Scariche_Appropriate.Text = reader["ICDScaricheAppropriateN"].ToString();
                this.Txt_Aritmie_Icd_Scariche_Inappropriate.Text = reader["ICDScaricheInappropriate"].ToString();
                try
                {
                    Rbl_Aritmia_Icd_ch.Checked = getValoreCheck(reader["ICD"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Aritmia_Icd.SelectedValue = reader["ICD"].ToString();
                this.Txt_Aritmie_Pm_Biventr.Text = reader["PMBiventricol"].ToString();
                this.Cld_Aritmie_Data_Impianto.Text = Utility.DecodificaData(reader["PMBiventricolDataImpianto"].ToString());
                //this.Ddl_Aritmia_Pm_Biventr.SelectedValue = reader["PMBiventricolBeneficiSint"].ToString();
                //this.Txt_Aritmie_FE_Prima.Text = reader["PercFEPrima"].ToString();
                //this.Txt_Aritmie_FE_A_6_Mesi.Text = reader["PerFESeiMesi"].ToString();
                this.Txt_Laboratorio_Bnp.Text = reader["IngrBNP"].ToString();
                this.Txt_Laboratorio_Troponina.Text = reader["IngrTroponina"].ToString();
                this.Txt_Laboratorio_Pcr.Text = reader["IngrPCR"].ToString();
                this.Txt_Laboratorio_Uricemia.Text = reader["IngrUricemia"].ToString();
                this.Txt_Laboratorio_Azot.Text = reader["IngrAzot"].ToString();
                this.Txt_Laboratorio_Creat.Text = reader["IngrCreat"].ToString();
                this.Txt_Laboratorio_Microalbumin.Text = reader["IngrMicroAlbumin"].ToString();
                this.Txt_Laboratorio_Na.Text = reader["IngrNA"].ToString();
                this.Txt_Laboratorio_K.Text = reader["IngrK"].ToString();
                this.Txt_Laboratorio_Ca.Text = reader["IngrCA"].ToString();
                this.Txt_Laboratorio_Ph.Text = reader["IngrPH"].ToString();
                this.Txt_Laboratorio_Mg.Text = reader["IngrMG"].ToString();
                this.Txt_Laboratorio_Got.Text = reader["IngrGOT"].ToString();
                this.Txt_Laboratorio_Gpt.Text = reader["IngrGPT"].ToString();
                this.Txt_Laboratorio_Cpk.Text = reader["IngrCPK"].ToString();
                this.Txt_Laboratorio_Gr.Text = reader["IngrGR"].ToString();
                this.Txt_Laboratorio_Emogl.Text = reader["IngrEmoglob"].ToString();
                this.Txt_Laboratorio_Gb.Text = reader["IngrGb"].ToString();
                this.Txt_Laboratorio_Sideremia.Text = reader["IngrSideremia"].ToString();
                this.Txt_Laboratorio_Glicemia.Text = reader["IngrGlicemia"].ToString();
                this.Txt_Laboratorio_Emogl_Glic.Text = reader["IngrEmoglGlic"].ToString();
                this.Txt_Laboratorio_Col_Tot.Text = reader["IngrColTot"].ToString();
                this.Txt_Laboratorio_Ldl.Text = reader["IngrLdl"].ToString();
                this.Txt_Laboratorio_Hdl.Text = reader["IngrHdl"].ToString();
                this.Txt_Laboratorio_Triglic.Text = reader["IngrTrigl"].ToString();
                this.Txt_Laboratorio_T3.Text = reader["IngrT3"].ToString();
                this.Txt_Laboratorio_T4.Text = reader["IngrT4"].ToString();
                this.Txt_Laboratorio_TSH.Text = reader["IngrTSH"].ToString();
                //UPDATE 2011
                this.Txt_Laboratorio_piastrine.Text = reader["lPiastrine"].ToString();
                this.Txt_Laboratorio_ferritina.Text = reader["lFerritina"].ToString();
                this.Txt_laboratorio_adh.Text = reader["lADH"].ToString();
                this.Txt_laboratorio_albumina.Text = reader["lAlbumina"].ToString();
                this.Txt_laboratorio_aldosterone.Text = reader["lAldosterone"].ToString();
                this.Txt_Laboratorio_VitD.Text = reader["lVitD"].ToString();
                this.Txt_Laboratorio_Epinefrina.Text = reader["lEpinefrina"].ToString();
                this.Cld_Data_Anamnesi_Dispnea_Nott.Text = reader["Data_Anamnesi_Dispnea_Nott"].ToString();
                
                //
                this.Txt_Laboratorio_Digitalemia.Text = reader["IngrDigitalemia"].ToString();
                try
                {
                    txtNefrologica_ch.Checked = getValoreCheck(reader["ConsNefro"].ToString());
                }
                catch (Exception ex) { }
                // txtNefrologica.Text = reader["ConsNefro"].ToString();
                //this.Rbl_Consulenze_Nefrologica.SelectedValue = reader["ConsNefro"].ToString();
                this.Cld_Consulenze_Nefrologica.Text = Utility.DecodificaData(reader["ConsNefroData"].ToString());
                try
                {
                    txtEmatologica_ch.Checked = getValoreCheck(reader["ConsEmato"].ToString());
                }
                catch (Exception ex) { }
                //txtEmatologica.Text = reader["ConsEmato"].ToString();
                //this.Rbl_Consulenze_Ematologica.SelectedValue = reader["ConsEmato"].ToString();
                this.Cld_Consulenze_Ematologica.Text = Utility.DecodificaData(reader["ConsEmatoData"].ToString());
                try
                {
                    txtNeuroPsichi_ch.Checked = getValoreCheck(reader["ConsNeuro"].ToString());
                }
                catch (Exception ex) { }
                //txtNeuroPsichi.Text = reader["ConsNeuro"].ToString();
                //this.Rbl_Consulenze_Neuropsichiatra.SelectedValue = reader["ConsNeuro"].ToString();
                this.Cld_Consulenze_Neuropsichiatrica.Text = Utility.DecodificaData(reader["ConsNeuroData"].ToString());
                try
                {
                    txtEndocrinologica_ch.Checked = getValoreCheck(reader["ConsEndoCri"].ToString());
                }
                catch (Exception ex) { }
                //txtEndocrinologica.Text = reader["ConsEndoCri"].ToString();
                //this.Rbl_Consulenze_Endocrinologica.SelectedValue = reader["ConsEndoCri"].ToString();
                this.Cld_Consulenze_Endocrinologica.Text = Utility.DecodificaData(reader["ConsEndoCriData"].ToString());
                txtPneumologica.Text = reader["ConsPneumo"].ToString();
                //this.Rbl_Consulenze_Pneumologica.SelectedValue = reader["ConsPneumo"].ToString();
                this.Cld_Consulenze_Pneumologica.Text = Utility.DecodificaData(reader["ConsPneumoData"].ToString());
                try
                {
                    Rbl_Esami_Strumentali_Ritmo_Sinusale_ch.Checked = getValoreCheck(reader["ECGRitmoSin"].ToString());
                }
                catch (Exception ex) { }
                try
                {
                    Rbl_Esami_Strumentali_Ritmo_ElettroIndotto_ch.Checked = getValoreCheck(reader["ECGRitmoElettro"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ritmo_Sinusale.SelectedValue = reader["ECGRitmoSin"].ToString();
                this.Txt_Esami_Strumentali_Ecg_Ingresso_Frequenza_Cardiaca.Text = reader["ECGFrequenza"].ToString();

                this.radioPreeccitazioneECG.SelectedValue = reader["preeccitazioneECG"].ToString();
                this.radioTPSVECG.SelectedValue = reader["TPSVECG"].ToString();

                
                try
                {
                    Rbl_Esami_Strumentali_Ex_Sopraventricol_ch.Checked = getValoreCheck(reader["ECGExSopraventr"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ex_Sopraventricol.SelectedValue = reader["ECGExSopraventr"].ToString();
                this.Ddl_Esami_Strumentali_Ecg_Ingresso_Ex_Ventricol.SelectedValue = reader["ECGExVentricolClasseLown"].ToString();
                this.Rbl_Esami_Strumentali_Tv.SelectedValue = reader["ECGTV"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Fa_ch.Checked = getValoreCheck(reader["ECGFA"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Fa.SelectedValue = reader["ECGFA"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Danno_Atriale_ch.Checked = getValoreCheck(reader["ECGDannoAtrialeSx"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Danno_Atriale.SelectedValue = reader["ECGDannoAtrialeSx"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Bbs_ch.Checked = getValoreCheck(reader["ECGBBS"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Bbs.SelectedValue = reader["ECGBBS"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Bbd_ch.Checked = getValoreCheck(reader["ECGBBD"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Bbd.SelectedValue = reader["ECGBBD"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Eas_ch.Checked = getValoreCheck(reader["ECGEAS"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Eas.SelectedValue = reader["ECGEAS"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Eps_ch.Checked = getValoreCheck(reader["ECGEPS"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Eps.SelectedValue = reader["ECGEPS"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Sovraccarico_Sistolico_ch.Checked = getValoreCheck(reader["ECGSovraSistolico"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Sovraccarico_Sistolico.SelectedValue = reader["ECGSovraSistolico"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ischemia_ch.Checked = getValoreCheck(reader["ECGIschemia"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ischemia.SelectedValue = reader["ECGIschemia"].ToString();
                radioPregressoIMA.SelectedValue = reader["pregressoIMA"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx_ch.Checked = getValoreCheck(reader["ECGIpertVentrSx"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx.SelectedValue = reader["ECGIpertVentrSx"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Sovraccarico_Volume_ch.Checked = getValoreCheck(reader["ECGSovraVolume"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Sovraccarico_Volume.SelectedValue = reader["ECGSovraVolume"].ToString();
                this.Ddl_Esami_Strumentali_Rx_Torace.SelectedValue = reader["RXKillip"].ToString();
                //this.Rbl_Esami_Strumentali_Versamento_Pleurico.SelectedValue = reader["VersamentoPleurico"].ToString();
                this.Txt_Esami_Strumentali_Eco2d_Ipertrofia_Settale.Text = reader["Eco2DIpertrSet"].ToString();
                this.Txt_Esami_Strumentali_Eco2d_Ipertrofia_Concentrica.Text = reader["Eco2DIpertrConc"].ToString();
                this.Ddl_Esami_Strumentali_Eco2d_Ipocinesia.SelectedValue = reader["Eco2DIpocinesia"].ToString();
                this.Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare.Text = reader["Eco2DVolSistVentrSx"].ToString();
                this.Txt_Esami_Strumentali_Eco2d_Volume_Diastolico.Text = reader["Eco2DVolDiastolico"].ToString();
                this.Txt_Esami_Strumentali_Eco2d_Fe.Text = reader["Eco2DFE"].ToString();
                this.txtDiametroAtrialeSX.Text = reader["diametroAtrialeSX"].ToString();
                this.txtDiametroAtrialeDX.Text = reader["diametroAtrialeDX"].ToString();
                this.txtDiametroVentricolareDX.Text = reader["diametroVentricolareDX"].ToString();
                this.txtFunzVentricoloDX.Text = reader["FunzVentricoloDX"].ToString();

                try
                {
                    this.Cld_Data_ProssimoControllo.Text = Utility.DecodificaData(reader["ProssimoControlloECO2D"].ToString());
                }
                catch (Exception ex)
                { 
                }

                this.Rbl_Esami_Strumentali_Disincronizzazione.SelectedValue = reader["Eco2DDisincr"].ToString();
                //this.Txt_Esami_Strumentali_Eco_Doppler_Descrizione.Text = reader["EcoDopplerDescrizione"].ToString();
                this.Ddl_Esami_Strumentali_Eco_Doppler_Disfunzione_Diastolica.SelectedValue = reader["EcoDopplerDisfDiast"].ToString();
                this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Mitralico.SelectedValue = reader["EcoDopplerRigurMitralico"].ToString();
                this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Aortico.SelectedValue = reader["EcoDopplerRigurAortico"].ToString();
                this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Tricuspidale.SelectedValue = reader["EcoDopplerRigurTricuspi"].ToString();
                this.Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare.Text = reader["EcoDopplerPressPolm"].ToString();
                txtNoteTerapie.Text = reader["AltroTerapie"].ToString();
                //this.Txt_Esami_Strumentali_Doppler_Arterioso_Arti_Inferiori.Text = reader["DopplerArtArtiInfDesc"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative_ch.Checked = getValoreCheck(reader["DopplerArtArtiInfSvenosi"].ToString());
                }
                catch (Exception ex) { }
                try
                {
                    Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative_chvasi.Checked = getValoreCheck(reader["DopplerVasi"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative.SelectedValue = reader["DopplerArtArtiInfSvenosi"].ToString();
                //this.Txt_Esami_Strumentali_Doppler_Tea.Text = reader["DopplerTEADesc"].ToString();
                //this.Rbl_Esami_Strumentali_Doppler_Tea_Stenosi_Significative.SelectedValue = reader["DopplerTEASvenosi"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale_ch.Checked = getValoreCheck(reader["ECGHRitmoSin"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale.SelectedValue = reader["ECGHRitmoSin"].ToString();
                this.Txt_Esami_Strumentali_Ecg_Holter_Frequenza_Cardiaca.Text = reader["ECGHFrequenza"].ToString();
                this.Txt_Esami_Strumentali_Ecg_Holter_Frequenza_CardiacaMAX.Text = reader["ECGHFrequenzaMax"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr_ch.Checked = getValoreCheck(reader["ECGHExSopraventr"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr.SelectedValue = reader["ECGHExSopraventr"].ToString();
                this.Ddl_Esami_Strumentali_Ecg_Holter_Ex_Ventricol.SelectedValue = reader["ECGHExVentricolClasseLow"].ToString();
                //try
                //{
                //    Rbl_Esami_Strumentali_Ecg_Holter_Tv_ch.Checked = getValoreCheck(reader["ECGHTV"].ToString());
                //}
                //catch (Exception ex) { }
                 this.Rbl_Esami_Strumentali_Ecg_Holter_Tv.SelectedValue = reader["ECGHTV"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Fa_ch.Checked = getValoreCheck(reader["ECGHFA"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ecg_Holter_Fa.SelectedValue = reader["ECGHFA"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx_ch.Checked = getValoreCheck(reader["ECGHDannoAtrialeSx"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx.SelectedValue = reader["ECGHDannoAtrialeSx"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Bbs_ch.Checked = getValoreCheck(reader["ECGHBBS"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ecg_Holter_Bbs.SelectedValue = reader["ECGHBBS"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Bbd_ch.Checked = getValoreCheck(reader["ECGHBBD"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ecg_Holter_Bbd.SelectedValue = reader["ECGHBBD"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Eas_ch.Checked = getValoreCheck(reader["ECGHEAS"].ToString());
                }
                catch (Exception ex) { }
                // this.Rbl_Esami_Strumentali_Ecg_Holter_Eas.SelectedValue = reader["ECGHEAS"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Eps_ch.Checked = getValoreCheck(reader["ECGHEPS"].ToString());
                }
                catch (Exception ex) { }
                // this.Rbl_Esami_Strumentali_Ecg_Holter_Eps.SelectedValue = reader["ECGHEPS"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico_ch.Checked = getValoreCheck(reader["ECGHSovraSistolico"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico.SelectedValue = reader["ECGHSovraSistolico"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Ischemia_ch.Checked = getValoreCheck(reader["ECGHIschemia"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ecg_Holter_Ischemia.SelectedValue = reader["ECGHIschemia"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare_ch.Checked = getValoreCheck(reader["ECGHIpertVentrSx"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare.SelectedValue = reader["ECGHIpertVentrSx"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume_ch.Checked = getValoreCheck(reader["ECGHSovraVolume"].ToString());
                }
                catch (Exception ex) { }
                //this.Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume.SelectedValue = reader["ECGHSovraVolume"].ToString();
                this.Txt_Esami_Strumentali_Peso1.Text = reader["Peso1"].ToString();
                try
                {
                    Rbl_Esami_Strumentali_Counceling_ch.Checked = getValoreCheck(reader["Counceling"].ToString());
                }
                catch (Exception ex) { }
                // this.Rbl_Esami_Strumentali_Counceling.SelectedValue = reader["Counceling"].ToString();
                try
                {
                    this.Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA.SelectedValue = Convert.ToInt32(reader["ClasseNYHA"].ToString()).ToString();
                }
                catch (Exception ex) { }
                this.Txt_Esami_Strumentali_Diagnosi.Text = reader["Diagnosi"].ToString();
                this.Ddl_Esami_Strumentali_BAV.SelectedValue = reader["BAV"].ToString();
                this.Ddl_Esami_Strumentali_BAVHolter.SelectedValue = reader["BAVHolter"].ToString();
                //this.Txt_Numero_Ospedalizzazioni.Text = reader["OspedalizzazioniNumero"].ToString();
                //this.Txt_Quest_Minnesota_Life.Text = reader["QuestMinnesotaLife"].ToString();
                //this.Txt_Costo_Totale.Text = reader["CostoTotaleTerapia"].ToString();
                //this.Cld_Data_Morte.Text = Utility.DecodificaData(reader["MorteData"].ToString());
                txtPreeccitazione.Text = reader["gPreeccitazione"].ToString();
                txtAblazione.Text = reader["gAblazione"].ToString();
                txtATH.Text = reader["gATH"].ToString();
                //Response.Write("GIANNI:" + reader["gIpertiroid"].ToString());
                //if (reader["gIpertiroid"].ToString() == "0") { radioIpertiroid.SelectedIndex = 1; } else { radioIpertiroid.SelectedIndex = 0; }
                //if (reader["gIpotiroid"].ToString() == "0") { radioIpotiroid.SelectedIndex = 1; } else { radioIpotiroid.SelectedIndex = 0; }
                try
                {
                    txtDocECGVentricol_ch.Checked = getValoreCheck(reader["ExVentricolECG"].ToString());
                }
                catch (Exception ex) { }
                //txtDocECGVentricol.Text = reader["ExVentricolECG"].ToString();
                try
                {
                    txtECGVentricol_ch.Checked = getValoreCheck(reader["ExVentricolECGHolter"].ToString());
                }
                catch (Exception ex) { }
                // txtECGVentricol.Text = reader["ExVentricolECGHolter"].ToString();
                txtGMWTEc.Text = reader["gGMWT"].ToString();
                txtPO2.Text = reader["gPO2"].ToString();
                txtPCD2.Text = reader["gPCD2"].ToString();

                try
                {
                    Terapia_ch_PM.Checked = getValoreCheck(reader["gPM"].ToString());
                }
                catch (Exception ex) { }
                Terapia_txt_Causale.Text = reader["gCausalePM"].ToString();
               Terapia_data_PM.Text = Utility.DecodificaData(reader["gDataPM"].ToString());
                Terapia_txt_Tipologia.Text = reader["gTipologiaPM"].ToString();
                try
                {
                    Terapia_ch_ICD.Checked = getValoreCheck(reader["gICD"].ToString());
                }
                catch (Exception ex) { }
                Terapia_txt_CausaleImp.Text = reader["gCausaleICD"].ToString();
                Terapia_data_impianto.Text = Utility.DecodificaData(reader["gDataICD"].ToString());
                Terapia_txt_scaricheApp.Text = reader["gScaricheAppICD"].ToString();
                Terapia_txt_scaricheInapp.Text = reader["gScaricheInappICD"].ToString();
                Terapia_txt_Ablazione.Text = reader["gAblazioneICD"].ToString();
                Terapia_txt_preeccitazione.Text = reader["gPreeccitazioneICD"].ToString();
                Terapia_txt_CausaleBiventricolare.Text = reader["gCausalePMBiv"].ToString();
               Terapia_data_PmBiventricolare.Text = Utility.DecodificaData(reader["gDataPMBiv"].ToString());

             
                //command.Parameters.AddWithValue("@", Utility.CodificaData(.Text.Trim()));


                if (Session["IdUtente"].ToString() != reader["IdUtente"].ToString())
                {
                    if ((this.Session["Utente"].ToString() != "cardio") && (this.Session["Utente"].ToString() != "Amministratore"))
                    {
                        LinkButton1.Enabled = false;
                        LinkButton1.Text = "Non puoi effettuare modifiche a questo controllo";
                        LinkButton2.Enabled = false;
                        LinkButton2.Text = "Non puoi effettuare modifiche a questo controllo";
                        Wizard1.Enabled = false;
                        //txtDataControllo.Enabled = false;
                        //imgCal.Style.Add(HtmlTextWriterStyle.Display, "none");
                    }
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            Response.Write(exception.Message + " " + exception.StackTrace);
            flag = true;
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    protected void Medico_SelectedIndexChanged(object sender, EventArgs e)
    {
       /* string connectionString = "";
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            connectionString = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        try
        {
            int num = Convert.ToInt32(this.Medico.SelectedItem.Value.ToString());
            SqlCommand command = new SqlCommand("Dati_Medico", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id_medico", num);
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                this.lb_mess.Text = "";
            }
            else
            {
                reader.Read();
                this.LbInfoMedico.Text = "<b>Indirizzo:</b> " + reader["Indirizzo"].ToString() + "<br><b>Telefono: </b>" + reader["telefoni"].ToString();
                reader.Close();
            }
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }*/
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        Rbl_Anemia.SelectedIndexChanged += new EventHandler(Rbl_Anemia_SelectedIndexChanged);
        if (!IsPostBack)
        {
            Cld_Data_Registrazione.Text = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
            
        }
        if (Session["Utente"] == null) { Response.Redirect("login.aspx"); }
        if (lblQuadro.Text != "")
        {
            this.idQuadro = int.Parse(lblQuadro.Text);
        }

        this.LbInfoMedico.Text = "";
        this.lb_mess.Text = "";
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        if ((base.Request.Params["idpaziente"] != null) && Utility.IsSignInteger(base.Request.Params["idpaziente"].ToString()))
        {
            this.IdPaziente = Convert.ToInt32(base.Request.Params["idpaziente"]);
        }
        else
        {
            base.Response.Redirect("homepage.aspx");
        }
        this.lb_mess.Text = "";
        if (!base.IsPostBack)
        {
            if (this.IdPaziente.Equals(-1))
            {
                this.lb_titolo.Text = "Reclutamento nuovo paziente";
                this.lb_menu.Text = "";
                if ((Session["tipo"].ToString() == "Medico di medicina generale"))
                {
                 Response.Redirect("HomePageTipo.aspx");
                }
            }
            else
            {
                string urlControllo = "";
                switch (Session["tipo"].ToString())
                {
                    case "admin":
                        urlControllo = "dativariabilispecialist.aspx";
                       
                        break;
                    case "Medico di medicina generale":
                        urlControllo = "dativariabilimmg.aspx";
                        
                        break;
                    case "Ospedaliero":
                        urlControllo = "dativariabilispecialist.aspx";
                       
                        break;
                    case "Cardiologo ambulatoriale":
                        urlControllo = "dativariabilispecialist.aspx";
                    
                        break;
                }
                this.lb_menu.Text = string.Concat(new object[] { "<li><a href=\"nuovopaziente.aspx?idpaziente=", this.IdPaziente, "\">Scheda<br />Paziente</a></li><li><a href=\"quadriclinici.aspx?idpaziente=", this.IdPaziente, "\">Controllo<br />Effettuati</a></li><li><a href=\"" + urlControllo + "?idpaziente=", this.IdPaziente, "&idquadro=-1\">Nuovo <br />Controllo</a></li>" });
                
            }
            this.ElencaMedici();
            this.ElencaTipoValvolopatie();
            this.estrai_NYHA(this.Ddl_Classe_NYHA_Al_Ricovero);
            this.estrai_NYHA(this.Ddl_Classe_NYHA_Alla_Dimissione);
            this.estrai_NYHA(this.Ddl2_Classe_NYHA_Al_Ricovero);
            this.estrai_NYHA(this.Ddl2_Classe_NYHA_Alla_Dimissione);
            this.estrai_NYHA(this.Ddl3_Classe_NYHA_Al_Ricovero);
            this.estrai_NYHA(this.Ddl3_Classe_NYHA_Alla_Dimissione);
            
            if (this.IdPaziente != -1)
            {
                if (this.EstraiDati())
                {
                   
                    this.Estrai_Precedenti_Accessi_Per_Scompenso();
                    this.Estrai_Dati_Precedenti_Ospedalizzazioni_Per_Scompenso();
                    this.Estrai_IMA();
                    this.Estrai_CARDIOP_ISCHEM_SENZA_DOCUMENT_IMA();
                    this.Estrai_Cardiomiopatia_Ipertrofica();
                    this.Estrai_Cardiomiopatia_Dilatativa();
                    this.Estrai_Valvolopatie();
                    this.Estrai_Altre_Eziologie();
                    this.Estrai_Rischi();
                   
                    this.estrai_quadro();
                    
                    //this.ElencaFarmaci();

                    this.estrai_NYHA(this.Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA);
                    //this.estrai_quadro();
                    //this.EstraiDati();
                    //this.estrai_Dispnea_Notturna();
                    //this.estrai_reliquati();
                    //this.estrai_sincope();
                    
                    //this.ElencaFarmaci();
                }
                else
                {
                    base.Response.Redirect("homepage.aspx");
                }
            }
            this.ElencaFarmaci();
            this.CreaMenuStep(this.Wizard1.ActiveStep.ID);
        }
    }

    void Rbl_Anemia_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        if (Rbl_Anemia.Text == "Si")
        {
          //  chbHbAnemia.Visible = true;
        }
        else
        {
           // chbHbAnemia.Visible = false;
        }
    }
    private void ElencaFarmaci()
    {
        DataTable dataTable = new DataTable();
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            string cmdText = "";
            if (this.idQuadro.Equals(-1))
            {
                cmdText = "select Farmaco.IdFarmaco as IdFarmaco, NomeFarmaco, '' as Valore, IdFarmaco as Intolleranza, CostoFarmaco from Farmaco order by (SUBSTRING(NomeFarmaco,1,LEN(NomeFarmaco) - CHARINDEX(' ', REVERSE(NomeFarmaco)) + 1)),DisplayOrder";
            }
            else
            {
                cmdText = "select Farmaco.IdFarmaco as IdFarmaco, NomeFarmaco, Valore, Intolleranza, PrevistoInTerapia.CostoFarmaco as CostoFarmaco from Farmaco left outer join PrevistoInTerapia on Farmaco.IdFarmaco=PrevistoInTerapia.IdFarmaco";
                cmdText = cmdText + " where IdQuadroClinico=" + this.idQuadro + " order by (SUBSTRING(NomeFarmaco,1,LEN(NomeFarmaco) - CHARINDEX(' ', REVERSE(NomeFarmaco)) + 1)),DisplayOrder";
            }
            SqlCommand command = new SqlCommand(cmdText, connection);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            try
            {
                try
                {
                    adapter.Fill(dataTable);
                }
                finally
                {
                    connection.Dispose();
                }
            }
            catch (Exception exception)
            {
                this.lb_mess.Text = exception.Message + " " + exception.StackTrace;
            }
            finally
            {
                adapter = null;
            }
            this.gv_farmaci.DataSource = dataTable;
            this.lb_gv.Text = "<div class=\"div_reset\"><a href=\"#\" onclick=\"document.form1.Wizard1$Txt_Esami_Strumentali_Diagnosi.value='';document.form1.Wizard1$Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA.selectedIndex=0;document.form1.Wizard1$Txt_Esami_Strumentali_Peso1.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Counceling[1].checked=true;";
            this.gv_farmaci.DataBind();
            this.lb_gv.Text = this.lb_gv.Text + "return false;\">reset</a></div>";
            if (dataTable.Rows.Count == 0)
            {
                this.gv_farmaci.Visible = false;
                this.lb_mess.Text = "Nessun farmaco presente.";
            }
            else
            {
                this.gv_farmaci.Visible = true;
                this.lb_mess.Text = "";
            }
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message + " " + exception2.StackTrace; ;
        }
        finally
        {
            connection.Close();
        }
    }
    private bool trova_id_ospedalizzazioni(int idpaziente)
    {
        bool flag = false;
        int num = 1;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Dati_Precedenti_Ospedalizzazioni_Per_Scompenso", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", idpaziente);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                switch (num)
                {
                    case 1:
                        this.IdOspedalizzazione1 = Convert.ToInt32(reader["IdOspedalizzazioneScomp"].ToString());
                        break;

                    case 2:
                        this.IdOspedalizzazione2 = Convert.ToInt32(reader["IdOspedalizzazioneScomp"].ToString());
                        break;

                    case 3:
                        this.IdOspedalizzazione3 = Convert.ToInt32(reader["IdOspedalizzazioneScomp"].ToString());
                        break;
                }
                num++;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
            flag = false;
        }
        finally
        {
            connection.Close();
            flag = true;
        }
        return flag;
    }

    private bool trova_id_scompensi(int idpaziente)
    {
        bool flag = false;
        int num = 1;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Dati_Precedenti_Accessi_Per_Scompenso", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", idpaziente);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                switch (num)
                {
                    case 1:
                        this.IdPrecedenteScomp1 = Convert.ToInt32(reader["IdPrecedentiScompenso"].ToString());
                        break;

                    case 2:
                        this.IdPrecedenteScomp2 = Convert.ToInt32(reader["IdPrecedentiScompenso"].ToString());
                        break;

                    case 3:
                        this.IdPrecedenteScomp3 = Convert.ToInt32(reader["IdPrecedentiScompenso"].ToString());
                        break;
                }
                num++;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message + " " + exception.StackTrace; ;
            flag = false;
        }
        finally
        {
            connection.Close();
            flag = true;
        }
        return flag;
    }

    protected void Wizard1_ActiveStepChanged(object sender, EventArgs e)
    {
        this.CreaMenuStep(this.Wizard1.ActiveStep.ID);
    }
    private bool ApportaModificheFarmaci(SqlTransaction trans, SqlConnection conn, int idQuadro)
    {
        string str;
        bool flag = false;
        if (this.idQuadro.Equals(-1))
        {
            str = "inserisci_farmaci";
        }
        else
        {
            str = "modifica_farmaci";
        }
        try
        {
            foreach (GridViewRow row in this.gv_farmaci.Rows)
            {
                int idquadro;
                string selectedValue = "";
                string str3 = "";
                string str4 = "";
                string nomeFarmaco = "";
                if (row.Cells[1].Text.IndexOf("<span></span>") > -1)
                { nomeFarmaco = row.Cells[1].Text.Substring(row.Cells[1].Text.IndexOf("<span></span>")).Replace("<span></span>", ""); }
                else
                { nomeFarmaco = row.Cells[1].Text; };
                if (row.Cells[5].Controls.Count > 0)
                {
                    TextBox box = (TextBox)row.Cells[5].Controls[1];
                    selectedValue = box.Text.Trim();
                    if ((nomeFarmaco != "Furosemide") && (nomeFarmaco != "Torasemide 10") && (nomeFarmaco != "Carvedilolo"))
                    {
                        RadioButtonList list = (RadioButtonList)row.Cells[5].Controls[3];
                        selectedValue = list.SelectedItem.Text.ToUpper();
                    }
                       // Response.Write("FARMACO:" + nomeFarmaco + " VALORE:" + selectedValue + "<BR/>");
                }

                if (row.Cells[6].Controls.Count > 0)
                {
                    CheckBox box2 = (CheckBox)row.Cells[6].Controls[1];
                    if (box2.Checked)
                    {
                        str3 = "S";
                    }
                    else
                    {
                        str3 = "N";
                    }
                }
                if (row.Cells[7].Controls.Count > 0)
                {
                    TextBox box3 = (TextBox)row.Cells[7].Controls[1];
                    str4 = box3.Text.Trim();
                }
                SqlCommand command = new SqlCommand(str, conn, trans);
                command.CommandType = CommandType.StoredProcedure;
                if (this.idQuadro.Equals(-1))
                {
                    idquadro = idQuadro;
                }
                else
                {
                    idquadro = this.idQuadro;
                }
                command.Parameters.AddWithValue("@IdQuadroClinico", idquadro);

                //Response.Write("ACQUISISCO:" + nomeFarmaco  + "<br/>");

                command.Parameters.AddWithValue("@NomeFarmaco", nomeFarmaco);
                command.Parameters.AddWithValue("@Valore", selectedValue);
                command.Parameters.AddWithValue("@Intolleranza", str3);
                command.Parameters.AddWithValue("@CostoFarmaco", str4);
                command.ExecuteNonQuery();
               // Response.Write("CARICO: " + str + " QUADRO:" + idquadro + " NOME:" + nomeFarmaco + " Valore:" + selectedValue + " Intolleranza:" + str3 + " Costo:" + str4 + " <br/>");
            }
            flag = true;
        }
        catch (Exception exception)
        {
            Response.Write(exception.Message + " " + exception.StackTrace);
        }
        return flag;
    }
    
    private bool controllaCampiObbligatori()
    {

        if (Ddl_Anamnesi_Dispnea_Sforzo.SelectedValue == "")
        {
            this.lb_mess.Text = "<b>NON HAI INSERITO LA CLASSE NYHA NEL QUADRO CLINICO. Aggiungila prima di salvare il reclutamento</b>";
            return false;
        }
        if (txtPASistolica.Text == "")
        {
            this.lb_mess.Text = "<b>NON HAI INSERITO LA PA SISTOLICA NEL QUADRO CLINICO. Aggiungila prima di salvare il reclutamento</b>";
            return false;
        }
        if (txtPADiastolica.Text == "")
        {
            this.lb_mess.Text = "<b>NON HAI INSERITO LA PA DIASTOLICA NEL QUADRO CLINICO. Aggiungila prima di salvare il reclutamento</b>";
            return false;
        }
        if (Txt_Anamnesi_Fc_Ingresso.Text == "")
        {
            this.lb_mess.Text = "<b>NON HAI INSERITO LA FREQUENZA CARDICA (F.C.) NEL QUADRO CLINICO. Aggiungila prima di salvare il reclutamento</b>";
            return false;
        }
        if (Txt_Esami_Strumentali_Eco2d_Fe.Text == "")
        {
            this.lb_mess.Text = "<b>NON HAI INSERITO LA F.E. % NEGLI ESAMI STRUMENTALI. Aggiungila prima di salvare il reclutamento</b>";
            return false;
        }
        //if (Txt_Esami_Strumentali_Eco2d_Volume_Diastolico.Text == "")
        //{
        //    this.lb_mess.Text = "<b>NON HAI INSERITO IL DIAMETRO DIASTOLICO NEGLI ESAMI STRUMENTALI. Aggiungilo prima di salvare il reclutamento</b>";
        //    return false;
        //}
        //if (Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare.Text == "")
        //{
        //    this.lb_mess.Text = "<b>NON HAI INSERITO IL DIAMETRO SISTOLICO VENTRICOLARE SX NEGLI ESAMI STRUMENTALI. Aggiungilo prima di salvare il reclutamento</b>";
        //    return false;
        //}
        //if (txtDiametroAtrialeSX.Text == "")
        //{
        //    this.lb_mess.Text = "<b>NON HAI INSERITO IL DIAMETRO ATRIALE SX NEGLI ESAMI STRUMENTALI. Aggiungilo prima di salvare il reclutamento</b>";
        //    return false;
        //}
        if (Txt_Laboratorio_Creat.Text == "")
        {
            this.lb_mess.Text = "<b>NON HAI INSERITO IL VALORE DI CREATINEMIA NEGLI ESAMI DI LABORATORIO. Aggiungilo prima di salvare il reclutamento</b>";
            return false;
        }
        if (Txt_Laboratorio_Bnp.Text == "")
        {
            this.lb_mess.Text = "<b>NON HAI INSERITO IL VALORE DI PRO BNP NEGLI ESAMI DI LABORATORIO. Aggiungilo prima di salvare il reclutamento</b>";
            return false;
        }
        if (Txt_Laboratorio_Emogl.Text == "")
        {
            this.lb_mess.Text = "<b>NON HAI INSERITO IL VALORE DI EMOGLOBINA NEGLI ESAMI DI LABORATORIO. Aggiungilo prima di salvare il reclutamento</b>";
            return false;
        }
        return true;
    
    }
    private bool inserisci_quadro()
    {
        

        string str;
        if (this.IdPaziente.Equals(-1))
        {
            //str = "inserisci_quadro_specialist";
            this.IdPaziente = this.IdUltimoPaziente;
            str = "inserisci_reclutamento_quadro";

            try
            {
                //INVIA MAIL DI ALERT
                MailMessage mm = new MailMessage();
                mm.From = new MailAddress("info@mediasoftonline.com");
                mm.To.Add(new MailAddress("fpisano@libero.it"));
                mm.To.Add(new MailAddress("info@mediasoftonline.com"));
                mm.Subject = "GISC - NUOVO RECLUTAMENTO EFFETTUATO";
                mm.IsBodyHtml = true;
                mm.Body = "Salve,<br/> il dott." + this.Session["Utente"] + " ha reclutato un nuovo paziente in data " + DateTime.Now + "<br/>Saluti,<br/><a href='http://www.studiogisc.net'>StudioGisc.Net</a><br/><br/><i style='font-size:10px;'>Attenzione. E-mail generata automaticamente dalla piattaforma GISC, non rispondere.</i>";
                SmtpClient sc = new SmtpClient("smtp.aruba.it");
                sc.Send(mm);

                
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message + " " + ex.StackTrace);
                
            }
            SqlConnection conn = new SqlConnection(this.conn_str);
            try
            {

                //INVIA MAIL AL MMG DI RIFERIMENTO.
                //recuperiamo l'indirizzo email (se c'è)
                conn.Open();
                SqlCommand cmd = new SqlCommand(str, conn);
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = "Select email from Utente,Paziente where Utente.idUtente=Paziente.idMedicoCurante and idPaziente=" + this.IdUltimoPaziente;
                SqlDataReader reader = cmd.ExecuteReader();
                if (reader.HasRows)
                {
                    reader.Read();

                    MailMessage mm = new MailMessage();
                    mm.From = new MailAddress("info@mediasoftonline.com");
                    mm.To.Add(new MailAddress(reader["Email"].ToString()));
                    mm.Subject = "GISC - NUOVO RECLUTAMENTO";
                    mm.IsBodyHtml = true;
                    mm.Body = "Salve,<br/> il dott." + this.Session["Utente"] + " ha reclutato un nuovo paziente in data " + DateTime.Now + "<br/>. Ti ha selezionato come Medico di Medicina Generale.<br/>Saluti,<br/><a href='http://www.studiogisc.net'>StudioGisc.Net</a><br/><br/><i style='font-size:10px;'>Attenzione. E-mail generata automaticamente dalla piattaforma GISC, non rispondere.</i>";
                    SmtpClient sc = new SmtpClient("smtp.aruba.it");
                    sc.Send(mm);
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message + " " + ex.StackTrace);

            }
            finally
            {
                conn.Close();
            }


           
        }
        else
        {
            //str = "modifica_quadro_specialist";
            str = "modifica_reclutamento_quadro";
        }
       
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlTransaction transaction = connection.BeginTransaction();
            try
            {
                SqlCommand command = new SqlCommand(str, connection, transaction);
                command.CommandType = CommandType.StoredProcedure;
                //command.Parameters.AddWithValue("@DataQuadroClinico", Utility.CodificaData(txtDataControllo.Text.Trim()));

                command.Parameters.AddWithValue("@DataProssimoQuadroClinico", Utility.CodificaData(this.txtDataProxControllo.Text.Trim()));
                command.Parameters.AddWithValue("@DataInizioScmpensoAttuale", Utility.CodificaData(this.Cld_Data_Anamnesi_Scompenso_Attuale.Text.Trim()));
                //command.Parameters.AddWithValue("@EdPolAcuto", txtEdPolAcuto.Text);//this.Rbl_Anamnesi_Ed_Pol_Acuto.SelectedValue);
                command.Parameters.AddWithValue("@EdPolAcuto", txtEdPolAcuto_ch.Checked);//this.Rbl_Anamnesi_Ed_Pol_Acuto.SelectedValue);
                command.Parameters.AddWithValue("@ClasseDispneaSforzo", this.Ddl_Anamnesi_Dispnea_Sforzo.SelectedValue);
                command.Parameters.AddWithValue("@Astenia", this.ddl_Anamnesi_Astenia.SelectedValue);
                command.Parameters.AddWithValue("@NCuscini", this.Txt_Anamnesi_N_Cuscini.Text.Trim());
                //command.Parameters.AddWithValue("@SleepApnea", this.Txt_Anamnesi_Sleep.Text.Trim());
                command.Parameters.AddWithValue("@SleepApnea", this.Txt_Anamnesi_Sleep_ch.Checked);
                command.Parameters.AddWithValue("@ContrazDiuresi", this.RBL_Anamnesi_Contraz_Diuresi.SelectedValue.ToString());
                command.Parameters.AddWithValue("@RecRapidoIncrementoPeso", "");//this.Txt_Anamnesi_Rapido_Increm_Peso.Text.Trim());
                command.Parameters.AddWithValue("@Cardiopalmo", "");//this.Txt_Anamnesi_Cardiopalmo.Text.Trim());
                command.Parameters.AddWithValue("@PAingresso", "");//this.Txt_Anamnesi_Pa_Ingresso.Text.Trim());
                command.Parameters.AddWithValue("@PADimiss", "");//this.Txt_Anamnesi_Pa_Dimissione.Text.Trim());
                command.Parameters.AddWithValue("@FCIngresso", Txt_Anamnesi_Fc_Ingresso.Text);//this.Txt_Anamnesi_Fc_Ingresso.Text.Trim());
                command.Parameters.AddWithValue("@FCDimissione", "");//this.Txt_Anamnesi_Fc_Dimissione.Text.Trim());
                command.Parameters.AddWithValue("@PolsoDimissione", "");//this.Txt_Anamnesi_Polso_Dimissione.Text.Trim());
                command.Parameters.AddWithValue("@PolsoIngresso", this.Txt_Anamnesi_Polso_Ingresso.Text.Trim());
                command.Parameters.AddWithValue("@PolsoIngrRitmo", this.Rbl_Anamnesi_Polso_Ingresso.SelectedValue.ToString());
                command.Parameters.AddWithValue("@PolsoDimRitmo", "");//this.Rbl_Anamnesi_Polso_Dimissione.SelectedValue.ToString());
                //command.Parameters.AddWithValue("@Cianosi",txtCianosi.Text);// this.Rbl_Anamnesi_Cianosi.SelectedValue.ToString());
                command.Parameters.AddWithValue("@Cianosi", txtCianosi_ch.Checked);// this.Rbl_Anamnesi_Cianosi.SelectedValue.ToString());
                command.Parameters.AddWithValue("@DescrizioneToniCardiaci", this.ddl_Anamnesi_Descrizione_Toni_Cardiaci.SelectedValue.ToString());
                command.Parameters.AddWithValue("@ToniValidiRidotti", this.Rbl_Anamnesi_Toni_Cardiaci.SelectedValue.ToString());
                //command.Parameters.AddWithValue("@Soffi", this.Rbl_Anamnesi_Soffi.SelectedValue.ToString());
                command.Parameters.AddWithValue("@Soffi", this.Rbl_Anamnesi_Soffi_ch.Checked);
                command.Parameters.AddWithValue("@DescrizioneSoffi", this.Txt_Anamnesi_Soffi_Descrizione.Text.Trim());
                command.Parameters.AddWithValue("@CongestionePolmonare", this.Rbl_Anamnesi_Congestione_Polm.SelectedValue.ToString());
                //command.Parameters.AddWithValue("@CongestionePerifGiugularimag3",txtGiugulari.Text);// this.Rbl_Anamnesi_Congestione_Perif_Giugulari.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifGiugularimag3", txtGiugulari_ch.Checked);// this.Rbl_Anamnesi_Congestione_Perif_Giugulari.SelectedValue.ToString());
                //command.Parameters.AddWithValue("@CongestionePerifEpatomegalia",txtEpatomegalia.Text);// this.Rbl_Anamnesi_Congestione_Perif_Epatomegalia.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifEpatomegalia", txtEpatomegalia_ch.Checked);// this.Rbl_Anamnesi_Congestione_Perif_Epatomegalia.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifEdemiPeriferici", this.Ddl_Anamnesi_Edemi_Periferici.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifVersamentoPleurico", txtVersamentoPleurico.Text);// this.Rbl_Anamnesi_Congestione_Perif_Versamento_Pleurico.SelectedValue.ToString());
                // command.Parameters.AddWithValue("@CongestionePerifAscite", txtAscite.Text);// this.Rbl_Anamnesi_Congestione_Perif_Ascite.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifAscite", txtAscite_ch.Checked);// this.Rbl_Anamnesi_Congestione_Perif_Ascite.SelectedValue.ToString());
                //command.Parameters.AddWithValue("@PolsiArterPresenti", this.Rbl_Anamnesi_Polsi_Arter_Presenti.SelectedValue.ToString());
                command.Parameters.AddWithValue("@PolsiArterPresenti", this.Rbl_Anamnesi_Polsi_Arter_Presenti_ch.Checked);
                command.Parameters.AddWithValue("@PolsiArterNoPerc", this.Txt_Anamnesi_Polsi_Arter_Non_Percepibili.Text.Trim());
                //command.Parameters.AddWithValue("@DocumentDoppler", this.Rbl_Anamnesi_Polsi_Arter_Doppler.SelectedValue.ToString());
                command.Parameters.AddWithValue("@DocumentDoppler", this.Rbl_Anamnesi_Polsi_Arter_Doppler_ch.Checked);
                command.Parameters.AddWithValue("@ExtraSopraVentrDataRisc", Utility.CodificaData(this.Cld_Aritmie_Ex_Sopraventricol.Text.Trim()));
                //command.Parameters.AddWithValue("@ExtraSopraVentrECG", txtDocECGSopraventricol.Text);// this.Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg.SelectedValue.ToString());
                command.Parameters.AddWithValue("@ExtraSopraVentrECG", txtDocECGSopraventricol_ch.Checked);// this.Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg.SelectedValue.ToString());
                //command.Parameters.AddWithValue("@ExtraSopraVentrECGHolter", txtECGHolterSopraventricol.Text);// this.Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg_Holter.SelectedValue.ToString());
                command.Parameters.AddWithValue("@ExtraSopraVentrECGHolter", txtECGHolterSopraventricol_ch.Checked);// this.Rbl_Aritmie_Ex_Sopraventricol_Document_Ecg_Holter.SelectedValue.ToString());
                command.Parameters.AddWithValue("@ExtraSopraVentrTerapia", "");// this.Txt_Aritmie_Ex_Sopraventtricol_Terapia.Text.Trim());
                command.Parameters.AddWithValue("@ExVentricolDataRisc", "");//Utility.CodificaData(this.Cld_Aritmie_Ex_Ventricol.Text.Trim()));
                //command.Parameters.AddWithValue("@ExVentricolECG", txtDocECGVentricol.Text);// this.Rbl_Aritmie_Ex_Ventricol_Document_Ecg.SelectedValue);
                command.Parameters.AddWithValue("@ExVentricolECG", txtDocECGVentricol_ch.Checked);// this.Rbl_Aritmie_Ex_Ventricol_Document_Ecg.SelectedValue);
                //command.Parameters.AddWithValue("@ExVentricolECGHolter", txtDocECGVentricol.Text);// this.Rbl_Aritmie_Ex_Ventricol_Document_Ecg_Holter.SelectedValue);
                command.Parameters.AddWithValue("@ExVentricolECGHolter", txtDocECGVentricol_ch.Checked);// this.Rbl_Aritmie_Ex_Ventricol_Document_Ecg_Holter.SelectedValue);
                command.Parameters.AddWithValue("@ExVentricolSempliciN", this.Txt_Aritmie_Ex_Ventricol_Semplici.Text.Trim());
                command.Parameters.AddWithValue("@ExVentricolCoppieTripleN", this.Txt_Aritmie_Ex_Ventricol_Coppie_Triplette.Text.Trim());
                command.Parameters.AddWithValue("@ExVentricolTVNonSostenutaN", this.Txt_Aritmie_Ex_Ventricol_Tv_Non_Sostenuta.Text.Trim());
                command.Parameters.AddWithValue("@ExVentricolTerapia", "");// this.Txt_Aritmie_Ex_Ventricol_Terapia.Text.Trim());
                command.Parameters.AddWithValue("@FAParadossDataInizio", Utility.CodificaData(this.Cld_Aritmie_Fa_Paross.Text.Trim()));
                command.Parameters.AddWithValue("@FAParadossNEpisodi", this.Txt_Aritmie_FA_Paradoss_Episodi.Text.Trim());
                command.Parameters.AddWithValue("@FAParadossDurataEpisodi", this.Txt_Aritmie_FA_Paradoss_Durata_Episodi.Text.Trim());
                command.Parameters.AddWithValue("@FAParadossTerapia", "");// this.Txt_Aritmie_FA_Paross_terapia.Text.Trim());
                command.Parameters.AddWithValue("@FAParadossEffTerapia", "");// this.Rbl_Aritmie_Fa_Paradoss_Efficacia.SelectedValue);
                command.Parameters.AddWithValue("@FAParadossAblazTrans", this.Rbl_Aritmie_Fa_Paradoss_Ablazione_Transcat.SelectedValue);
                command.Parameters.AddWithValue("@FAPermanenteDataInizio", Utility.CodificaData(this.Cld_Aritmie_Fa_Permanente.Text.Trim()));
                command.Parameters.AddWithValue("@DataCardiomiopatia", Utility.CodificaData(this.DataCardioMiopatia.Text.Trim()));
                command.Parameters.AddWithValue("@FAPermanenteTerapia", "");//this.Txt_Aritmie_Fa_Permanente_Terapia.Text.Trim());
                //command.Parameters.AddWithValue("@CVE", this.Rbl_Aritmie_Cve.SelectedValue);
                command.Parameters.AddWithValue("@CVE", this.Rbl_Aritmie_Cve_ch.Checked);
                command.Parameters.AddWithValue("@TVSostenutaSintomatica", this.Rbl_Aritmie_Tv_Sostenuta.SelectedValue);
                command.Parameters.AddWithValue("@TVSostenutaSintomaticaData", Utility.CodificaData(this.Cld_Aritmie_Tv_Sostenuta.Text.Trim()));
                command.Parameters.AddWithValue("@TVSostenutaSintomaticaEpisodiN", this.Txt_Aritmie_Tv_Sostenuta_Num_Episodi.Text.Trim());
                command.Parameters.AddWithValue("@TVSostenutaSintomaticaDurata", this.Txt_Aritmie_Tv_Sostenuta_Durata_Episodi.Text.Trim());
                command.Parameters.AddWithValue("@TVSostenutaSintomaticaFreqVentr", this.Txt_Aritmie_Tv_Sostenuta_Freq_Ventricol.Text.Trim());
                command.Parameters.AddWithValue("@TVSostenutaSincope", "");//this.Txt_Aritmie_Tv_Sostenuta_Sincope.Text.Trim());
                command.Parameters.AddWithValue("@TVSostenutaSintomaticaLipotimia", "");// this.Txt_Aritmie_Tv_Sostenuta_Lipotimia.Text.Trim());
                //command.Parameters.AddWithValue("@MIResuscitata", this.Rbl_Aritmia_Mi_Resuscitata.SelectedValue);
                command.Parameters.AddWithValue("@MIResuscitata", this.Rbl_Aritmia_Mi_Resuscitata_ch.Checked);
                //command.Parameters.AddWithValue("@PM", this.Rbl_Aritmia_Pm.SelectedValue);
                command.Parameters.AddWithValue("@PM", this.Rbl_Aritmia_Pm_ch.Checked);
                command.Parameters.AddWithValue("@PMCausaleImpianto", this.Txt_Aritmie_Pm_Causale_Impianto.Text.Trim());
                command.Parameters.AddWithValue("@PMDataImpianto", Utility.CodificaData(this.Cld_Aritmie_Pm.Text.Trim()));
                command.Parameters.AddWithValue("@PMTipologia", this.Txt_Aritmie_Pm_Tipologia.Text.Trim());
                //command.Parameters.AddWithValue("@ICD", this.Rbl_Aritmia_Icd.SelectedValue);
                command.Parameters.AddWithValue("@ICD", this.Rbl_Aritmia_Icd_ch.Checked);
                command.Parameters.AddWithValue("@ICDCausaleImpianto", this.Txt_Aritmie_Causale_Impianto.Text.Trim());
                command.Parameters.AddWithValue("@ICDDataImpianto", Utility.CodificaData(this.Cld_Aritmie_Icd.Text.Trim()));
                command.Parameters.AddWithValue("@ICDScaricheAppropriateN", this.Txt_Aritmie_Icd_Scariche_Appropriate.Text.Trim());
                command.Parameters.AddWithValue("@ICDScaricheInappropriate", this.Txt_Aritmie_Icd_Scariche_Inappropriate.Text.Trim());
                command.Parameters.AddWithValue("@PMBiventricol", this.Txt_Aritmie_Pm_Biventr.Text.Trim());
                command.Parameters.AddWithValue("@PMBiventricolCausaleImp", this.Txt_Aritmie_Pm_Causale_Impianto.Text.Trim());
                command.Parameters.AddWithValue("@PMBiventricolDataImpianto", Utility.CodificaData(this.Cld_Aritmie_Data_Impianto.Text.Trim()));
                command.Parameters.AddWithValue("@PMBiventricolBeneficiSint", "");//this.Ddl_Aritmia_Pm_Biventr.SelectedValue);
                command.Parameters.AddWithValue("@PercFEPrima", "");//this.Txt_Aritmie_FE_Prima.Text.Trim());
                command.Parameters.AddWithValue("@PerFESeiMesi", "");//this.Txt_Aritmie_FE_A_6_Mesi.Text.Trim());
                command.Parameters.AddWithValue("@IngrBNP", this.Txt_Laboratorio_Bnp.Text.Trim());
                command.Parameters.AddWithValue("@IngrTroponina", this.Txt_Laboratorio_Troponina.Text.Trim());
                command.Parameters.AddWithValue("@IngrPCR", this.Txt_Laboratorio_Pcr.Text.Trim());
                command.Parameters.AddWithValue("@IngrUricemia", this.Txt_Laboratorio_Uricemia.Text.Trim());
                command.Parameters.AddWithValue("@IngrAzot", this.Txt_Laboratorio_Azot.Text.Trim());
                command.Parameters.AddWithValue("@IngrCreat", this.Txt_Laboratorio_Creat.Text.Trim());
                command.Parameters.AddWithValue("@IngrMicroAlbumin", this.Txt_Laboratorio_Microalbumin.Text.Trim());
                command.Parameters.AddWithValue("@IngrNA", this.Txt_Laboratorio_Na.Text.Trim());
                command.Parameters.AddWithValue("@IngrK", this.Txt_Laboratorio_K.Text.Trim());
                command.Parameters.AddWithValue("@IngrCA", this.Txt_Laboratorio_Ca.Text.Trim());
                command.Parameters.AddWithValue("@IngrPH", this.Txt_Laboratorio_Ph.Text.Trim());
                command.Parameters.AddWithValue("@IngrMG", this.Txt_Laboratorio_Mg.Text.Trim());
                command.Parameters.AddWithValue("@IngrGOT", this.Txt_Laboratorio_Got.Text.Trim());
                command.Parameters.AddWithValue("@IngrGPT", this.Txt_Laboratorio_Gpt.Text.Trim());
                command.Parameters.AddWithValue("@IngrCPK", this.Txt_Laboratorio_Cpk.Text.Trim());
                command.Parameters.AddWithValue("@IngrGR", this.Txt_Laboratorio_Gr.Text.Trim());
                command.Parameters.AddWithValue("@IngrEmoglob", this.Txt_Laboratorio_Emogl.Text.Trim());
                command.Parameters.AddWithValue("@IngrGB", this.Txt_Laboratorio_Gb.Text.Trim());
                command.Parameters.AddWithValue("@IngrSideremia", this.Txt_Laboratorio_Sideremia.Text.Trim());
                command.Parameters.AddWithValue("@IngrGlicemia", this.Txt_Laboratorio_Glicemia.Text.Trim());
                command.Parameters.AddWithValue("@IngrEmoglGlic", this.Txt_Laboratorio_Emogl_Glic.Text.Trim());
                command.Parameters.AddWithValue("@IngrColTot", this.Txt_Laboratorio_Col_Tot.Text.Trim());
                command.Parameters.AddWithValue("@IngrHDL", this.Txt_Laboratorio_Hdl.Text.Trim());
                command.Parameters.AddWithValue("@IngrLDL", this.Txt_Laboratorio_Ldl.Text.Trim());
                command.Parameters.AddWithValue("@IngrTrigl", this.Txt_Laboratorio_Triglic.Text.Trim());
                command.Parameters.AddWithValue("@IngrT3", this.Txt_Laboratorio_T3.Text.Trim());
                command.Parameters.AddWithValue("@IngrT4", this.Txt_Laboratorio_T4.Text.Trim());
                command.Parameters.AddWithValue("@IngrTSH", this.Txt_Laboratorio_TSH.Text.Trim());
                command.Parameters.AddWithValue("@IngrDigitalemia", this.Txt_Laboratorio_Digitalemia.Text.Trim());
                //command.Parameters.AddWithValue("@ECGRitmoSin", this.Rbl_Esami_Strumentali_Ritmo_Sinusale.SelectedValue);
                command.Parameters.AddWithValue("@ECGRitmoSin", this.Rbl_Esami_Strumentali_Ritmo_Sinusale_ch.Checked);
                command.Parameters.AddWithValue("@ECGRitmoElettro", this.Rbl_Esami_Strumentali_Ritmo_ElettroIndotto_ch.Checked);
                
                command.Parameters.AddWithValue("@ECGFrequenza", this.Txt_Esami_Strumentali_Ecg_Ingresso_Frequenza_Cardiaca.Text.Trim());
                command.Parameters.AddWithValue("@preeccitazioneECG", this.radioPreeccitazioneECG.SelectedValue);
                command.Parameters.AddWithValue("@TPSVECG", this.radioTPSVECG.SelectedValue);
                //command.Parameters.AddWithValue("@ECGExSopraventr", this.Rbl_Esami_Strumentali_Ex_Sopraventricol.SelectedValue);
                command.Parameters.AddWithValue("@ECGExSopraventr", this.Rbl_Esami_Strumentali_Ex_Sopraventricol_ch.Checked);
                command.Parameters.AddWithValue("@ECGExVentricolClasseLown", this.Ddl_Esami_Strumentali_Ecg_Ingresso_Ex_Ventricol.SelectedValue);
                command.Parameters.AddWithValue("@ECGTV", this.Rbl_Esami_Strumentali_Tv.SelectedValue);
                //command.Parameters.AddWithValue("@ECGFA", this.Rbl_Esami_Strumentali_Fa.SelectedValue);
                command.Parameters.AddWithValue("@ECGFA", this.Rbl_Esami_Strumentali_Fa_ch.Checked);
                //command.Parameters.AddWithValue("@ECGDannoAtrialeSx", this.Rbl_Esami_Strumentali_Danno_Atriale.SelectedValue);
                command.Parameters.AddWithValue("@ECGDannoAtrialeSx", this.Rbl_Esami_Strumentali_Danno_Atriale_ch.Checked);
                //command.Parameters.AddWithValue("@ECGBBS", this.Rbl_Esami_Strumentali_Bbs.SelectedValue);
                command.Parameters.AddWithValue("@ECGBBS", this.Rbl_Esami_Strumentali_Bbs_ch.Checked);
                //command.Parameters.AddWithValue("@ECGBBD", this.Rbl_Esami_Strumentali_Bbd.SelectedValue);
                command.Parameters.AddWithValue("@ECGBBD", this.Rbl_Esami_Strumentali_Bbd_ch.Checked);
                //command.Parameters.AddWithValue("@ECGEAS", this.Rbl_Esami_Strumentali_Eas.SelectedValue);
                command.Parameters.AddWithValue("@ECGEAS", this.Rbl_Esami_Strumentali_Eas_ch.Checked);
                //command.Parameters.AddWithValue("@ECGEPS", this.Rbl_Esami_Strumentali_Eps.SelectedValue);
                command.Parameters.AddWithValue("@ECGEPS", this.Rbl_Esami_Strumentali_Eps_ch.Checked);
                //command.Parameters.AddWithValue("@ECGSovraSistolico", this.Rbl_Esami_Strumentali_Sovraccarico_Sistolico.SelectedValue);
                command.Parameters.AddWithValue("@ECGSovraSistolico", this.Rbl_Esami_Strumentali_Sovraccarico_Sistolico_ch.Checked);
                //command.Parameters.AddWithValue("@ECGIschemia", this.Rbl_Esami_Strumentali_Ischemia.SelectedValue);
                command.Parameters.AddWithValue("@ECGIschemia", this.Rbl_Esami_Strumentali_Ischemia_ch.Checked);
                //command.Parameters.AddWithValue("@ECGIpertVentrSx", this.Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx.SelectedValue);
                command.Parameters.AddWithValue("@ECGIpertVentrSx", this.Rbl_Esami_Strumentali_Ipertrofia_Ventricolare_Sx_ch.Checked);
                command.Parameters.AddWithValue("@pregressoIMA", this.radioPregressoIMA.SelectedValue);
                //command.Parameters.AddWithValue("@ECGSovraVolume", this.Rbl_Esami_Strumentali_Sovraccarico_Volume.SelectedValue);
                command.Parameters.AddWithValue("@ECGSovraVolume", this.Rbl_Esami_Strumentali_Sovraccarico_Volume_ch.Checked);
                command.Parameters.AddWithValue("@RXKillip", this.Ddl_Esami_Strumentali_Rx_Torace.SelectedValue);
                
                command.Parameters.AddWithValue("@VersamentoPleurico", "");//this.Rbl_Esami_Strumentali_Versamento_Pleurico.SelectedValue);
                command.Parameters.AddWithValue("@Eco2DIpertrSet", this.Txt_Esami_Strumentali_Eco2d_Ipertrofia_Settale.Text.Trim());
                command.Parameters.AddWithValue("@Eco2DIpertrConc", this.Txt_Esami_Strumentali_Eco2d_Ipertrofia_Concentrica.Text.Trim());
                command.Parameters.AddWithValue("@Eco2DIpocinesia", this.Ddl_Esami_Strumentali_Eco2d_Ipocinesia.SelectedValue);
                command.Parameters.AddWithValue("@Eco2DVolSistVentrSx", this.Txt_Esami_Strumentali_Eco2d_Volume_Sistolico_Ventricolare.Text.Trim());
                command.Parameters.AddWithValue("@Eco2DVolDiastolico", this.Txt_Esami_Strumentali_Eco2d_Volume_Diastolico.Text.Trim());
                command.Parameters.AddWithValue("@Eco2DFE", this.Txt_Esami_Strumentali_Eco2d_Fe.Text.Trim());
                command.Parameters.AddWithValue("@diametroAtrialeSX", this.txtDiametroAtrialeSX.Text.Trim());
                command.Parameters.AddWithValue("@diametroAtrialeDX", this.txtDiametroAtrialeDX.Text.Trim());
                command.Parameters.AddWithValue("@diametroVentricolareDX", this.txtDiametroVentricolareDX.Text.Trim());
                command.Parameters.AddWithValue("@FunzVentricoloDX", this.txtFunzVentricoloDX.Text.Trim());
                command.Parameters.AddWithValue("@prossimoControlloECO2D", Utility.CodificaData(this.Cld_Data_ProssimoControllo.Text));

                command.Parameters.AddWithValue("@Eco2DDisincr", this.Rbl_Esami_Strumentali_Disincronizzazione.SelectedValue);
                command.Parameters.AddWithValue("@EcoDopplerDescrizione", "");// this.Txt_Esami_Strumentali_Eco_Doppler_Descrizione.Text.Trim());
                command.Parameters.AddWithValue("@EcoDopplerDisfDiast", this.Ddl_Esami_Strumentali_Eco_Doppler_Disfunzione_Diastolica.SelectedValue);
                command.Parameters.AddWithValue("@EcoDopplerRigurMitralico", this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Mitralico.SelectedValue);
                command.Parameters.AddWithValue("@EcoDopplerRigurAortico", this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Aortico.SelectedValue);
                command.Parameters.AddWithValue("@EcoDopplerRigurTricuspi", this.Ddl_Esami_Strumentali_Eco_Doppler_Rigurgito_Tricuspidale.SelectedValue);
                command.Parameters.AddWithValue("@EcoDopplerPressPolm", this.Txt_Esami_Strumentali_Eco_Doppler_Pressione_Polmonare.Text.Trim());
                command.Parameters.AddWithValue("@DopplerArtArtiInfDesc", "");// this.Txt_Esami_Strumentali_Doppler_Arterioso_Arti_Inferiori.Text.Trim());
                //command.Parameters.AddWithValue("@DopplerArtArtiInfSvenosi", this.Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative.SelectedValue);
                command.Parameters.AddWithValue("@DopplerArtArtiInfSvenosi", this.Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative_ch.Checked);
                command.Parameters.AddWithValue("@DopplerVasi", this.Rbl_Esami_Strumentali_Doppler_Arterioso_Stenosi_Significative_chvasi.Checked);
                command.Parameters.AddWithValue("@DopplerTEADesc", "");//this.Txt_Esami_Strumentali_Doppler_Tea.Text.Trim());
                command.Parameters.AddWithValue("@DopplerTEASvenosi", "");//this.Rbl_Esami_Strumentali_Doppler_Tea_Stenosi_Significative.SelectedValue);
                command.Parameters.AddWithValue("@Diagnosi", this.Txt_Esami_Strumentali_Diagnosi.Text.Trim());
                command.Parameters.AddWithValue("@ClasseNYHA", this.Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA.SelectedValue);
                command.Parameters.AddWithValue("@Peso1", this.Txt_Esami_Strumentali_Peso1.Text.Trim());
                //command.Parameters.AddWithValue("@Counceling", this.Rbl_Esami_Strumentali_Counceling.SelectedValue);
                command.Parameters.AddWithValue("@Counceling", this.Rbl_Esami_Strumentali_Counceling_ch.Checked);
                //command.Parameters.AddWithValue("@ECGHRitmoSin", this.Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale.SelectedValue);
                command.Parameters.AddWithValue("@ECGHRitmoSin", this.Rbl_Esami_Strumentali_Ecg_Holter_Ritmo_Sinusale_ch.Checked);
                command.Parameters.AddWithValue("@ECGHFrequenza", this.Txt_Esami_Strumentali_Ecg_Holter_Frequenza_Cardiaca.Text.Trim());
                command.Parameters.AddWithValue("@ECGHFrequenzaMax", this.Txt_Esami_Strumentali_Ecg_Holter_Frequenza_CardiacaMAX.Text.Trim());
                //command.Parameters.AddWithValue("@ECGHExSopraventr", this.Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr.SelectedValue);
                command.Parameters.AddWithValue("@ECGHExSopraventr", this.Rbl_Esami_Strumentali_Ecg_Holter_Ex_Sopraventr_ch.Checked);
                //Response.Write("EX VENTRICOL:" + this.Ddl_Esami_Strumentali_Ecg_Holter_Ex_Ventricol.SelectedValue);
               // command.Parameters.AddWithValue("@ECGExVentricolClasseLown", this.Ddl_Esami_Strumentali_Ecg_Holter_Ex_Ventricol.SelectedValue);
                command.Parameters.AddWithValue("@ECGHExVentricolClasseLow", this.Ddl_Esami_Strumentali_Ecg_Holter_Ex_Ventricol.SelectedValue);
                command.Parameters.AddWithValue("@ECGHTV", this.Rbl_Esami_Strumentali_Ecg_Holter_Tv.SelectedValue);
                //command.Parameters.AddWithValue("@ECGHTV", this.Rbl_Esami_Strumentali_Ecg_Holter_Tv_ch.Checked);
                //command.Parameters.AddWithValue("@ECGHFA", this.Rbl_Esami_Strumentali_Ecg_Holter_Fa.SelectedValue);
                command.Parameters.AddWithValue("@ECGHFA", this.Rbl_Esami_Strumentali_Ecg_Holter_Fa_ch.Checked);
                //command.Parameters.AddWithValue("@ECGHDannoAtrialeSx", this.Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx.SelectedValue);
                command.Parameters.AddWithValue("@ECGHDannoAtrialeSx", this.Rbl_Esami_Strumentali_Ecg_Holter_Danno_Atriale_Sx_ch.Checked);
                //command.Parameters.AddWithValue("@ECGHBBS", this.Rbl_Esami_Strumentali_Ecg_Holter_Bbs.SelectedValue);
                command.Parameters.AddWithValue("@ECGHBBS", this.Rbl_Esami_Strumentali_Ecg_Holter_Bbs_ch.Checked);
                //command.Parameters.AddWithValue("@ECGHBBD", this.Rbl_Esami_Strumentali_Ecg_Holter_Bbd.SelectedValue);
                command.Parameters.AddWithValue("@ECGHBBD", this.Rbl_Esami_Strumentali_Ecg_Holter_Bbd_ch.Checked);
                //command.Parameters.AddWithValue("@ECGHEAS", this.Rbl_Esami_Strumentali_Ecg_Holter_Eas.SelectedValue);
                command.Parameters.AddWithValue("@ECGHEAS", this.Rbl_Esami_Strumentali_Ecg_Holter_Eas_ch.Checked);
                //command.Parameters.AddWithValue("@ECGHEPS", this.Rbl_Esami_Strumentali_Ecg_Holter_Eps.SelectedValue);
                command.Parameters.AddWithValue("@ECGHEPS", this.Rbl_Esami_Strumentali_Ecg_Holter_Eps_ch.Checked);
                //command.Parameters.AddWithValue("@ECGHSovraSistolico", this.Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico.SelectedValue);
                command.Parameters.AddWithValue("@ECGHSovraSistolico", this.Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Sistolico_ch.Checked);
                //command.Parameters.AddWithValue("@ECGHIschemia", this.Rbl_Esami_Strumentali_Ecg_Holter_Ischemia.SelectedValue);
                command.Parameters.AddWithValue("@ECGHIschemia", this.Rbl_Esami_Strumentali_Ecg_Holter_Ischemia_ch.Checked);
                //command.Parameters.AddWithValue("ECGHIpertVentrSx", this.Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare.SelectedValue);
                command.Parameters.AddWithValue("ECGHIpertVentrSx", this.Rbl_Esami_Strumentali_Ecg_Holter_Ipertrofia_Ventricolare_ch.Checked);
                //command.Parameters.AddWithValue("@ECGHSovraVolume", this.Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume.SelectedValue);
                command.Parameters.AddWithValue("@ECGHSovraVolume", this.Rbl_Esami_Strumentali_Ecg_Holter_Sovraccarico_Volume_ch.Checked);
                //command.Parameters.AddWithValue("@ConsNefro", txtNefrologica.Text);//this.Rbl_Consulenze_Nefrologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsNefro", txtNefrologica_ch.Checked);//this.Rbl_Consulenze_Nefrologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsNefroData", Utility.CodificaData(this.Cld_Consulenze_Nefrologica.Text.Trim()));
                //command.Parameters.AddWithValue("@ConsEmato", txtEmatologica.Text);//this.Rbl_Consulenze_Ematologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsEmato", txtEmatologica_ch.Checked);//this.Rbl_Consulenze_Ematologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsEmatoData", Utility.CodificaData(this.Cld_Consulenze_Ematologica.Text.Trim()));
                //command.Parameters.AddWithValue("@ConsNeuro", txtNeuroPsichi.Text);//this.Rbl_Consulenze_Neuropsichiatra.SelectedValue);
                command.Parameters.AddWithValue("@ConsNeuro", txtNeuroPsichi_ch.Checked);//this.Rbl_Consulenze_Neuropsichiatra.SelectedValue);
                command.Parameters.AddWithValue("@ConsNeuroData", Utility.CodificaData(this.Cld_Consulenze_Neuropsichiatrica.Text.Trim()));
                //command.Parameters.AddWithValue("@ConsEndoCri", txtEndocrinologica.Text);//this.Rbl_Consulenze_Endocrinologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsEndoCri", txtEndocrinologica_ch.Checked);//this.Rbl_Consulenze_Endocrinologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsEndoCriData", Utility.CodificaData(this.Cld_Consulenze_Endocrinologica.Text.Trim()));
                command.Parameters.AddWithValue("@ConsPneumo", txtPneumologica.Text);//this.Rbl_Consulenze_Pneumologica.SelectedValue);
                command.Parameters.AddWithValue("@ConsPneumoData", Utility.CodificaData(this.Cld_Consulenze_Pneumologica.Text.Trim()));
                command.Parameters.AddWithValue("@Obesita", "");//this.Quadro_Clinico_Obesita.SelectedValue);
                command.Parameters.AddWithValue("@Sovrappeso", "");//this.Quadro_Clinico_Sovrappeso.SelectedValue);
                command.Parameters.AddWithValue("@ObesitaUnifAddom", "");//this.Rbl_Quadro_Clinico_Obesita_Add.SelectedValue);
                command.Parameters.AddWithValue("@ObesitaUnifAddomData", "");//Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Obesita_Add.Text.Trim()));
                command.Parameters.AddWithValue("@Iperglicemia", "");//this.Rbl_Quadro_Clinico_Iperglicemia.SelectedValue);
                command.Parameters.AddWithValue("@IperglicemiaData", "");//Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Iperglicemia.Text.Trim()));
                command.Parameters.AddWithValue("@IpertArt", "");//this.Rbl_Quadro_Clinico_Ipertensione_Arteriosa.SelectedValue);
                command.Parameters.AddWithValue("@IpertArtData", "");//Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Ipertensione_Arteriosa.Text.Trim()));
                command.Parameters.AddWithValue("@IperDislitemia", "");//this.Rbl_Quadro_Clinico_Iperslidemia.SelectedValue);
                command.Parameters.AddWithValue("@IperDislitemiaData", "");// Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Iperdislipidemia.Text.Trim()));
                command.Parameters.AddWithValue("@BPCO", "");//this.Rbl_Quadro_Clinico_BPCO.SelectedValue);
                command.Parameters.AddWithValue("@BPCOData", "");//Utility.CodificaData(this.Cld_Quadro_Clinico_Data_BPCO.Text.Trim()));
                command.Parameters.AddWithValue("@AsmaBronchiale", "");//this.Rbl_Quadro_Clinico_Asma_Bronchiale.SelectedValue);
                command.Parameters.AddWithValue("@AsmaBronchialeData", "");//Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Asma_Bronchiale.Text.Trim()));
                command.Parameters.AddWithValue("@InsuffRenale", this.Rbl_Quadro_Clinico_Insufficienza_Renale.SelectedValue);
                command.Parameters.AddWithValue("@InsuffRenaleData", "");//Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Insufficienza_Renale.Text.Trim()));
                command.Parameters.AddWithValue("@Anemia", "");//txtAnemia.Text);//this.Rbl_Quadro_Clinico_Anemia.SelectedValue);
                command.Parameters.AddWithValue("@AnemiaData", "");//Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Anemia.Text.Trim()));
                command.Parameters.AddWithValue("@IpertiroidInizio", "");//Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Inizio_Ipertiroid.Text.Trim()));
                command.Parameters.AddWithValue("@IpertiroTerapia", "");//this.Txt_Quadro_Clinico_Ipertiroid_Terapia.Text.Trim());
                command.Parameters.AddWithValue("@IpertiroNormInd", "");//this.Rbl_Ipertiroid_Normalizz.SelectedValue);
                command.Parameters.AddWithValue("@IpotirodInizio", "");//Utility.CodificaData(this.Cld_Quadro_Clinico_Data_Inizio_Ipotiroid.Text.Trim()));
                command.Parameters.AddWithValue("@IpotiroTerapia", "");//this.Txt_Quadro_Clinico_Ipotiroid_Terapia.Text.Trim());
                command.Parameters.AddWithValue("@IpotiroNormInd", "");//this.Rbl_Quadro_Clinico_Ipotiroid_Normalizzaz_Indici_Lab.SelectedValue);
                command.Parameters.AddWithValue("@NEpisodiDispnea", this.Txt_Anamnesi_Dispnea_Nott.Text.Trim());
                command.Parameters.AddWithValue("@DataQuadroClinico", "");//Utility.CodificaData(txtDataControllo.Text.Trim()));//Utility.CodificaData(this.Cld_Quadro_Clinico_Generale.Text.Trim()));
                command.Parameters.AddWithValue("@MorteData", "");//Utility.CodificaData(this.Cld_Data_Morte.Text.Trim()));
                command.Parameters.AddWithValue("@OspedalizzazioniNumero", "");//this.Txt_Numero_Ospedalizzazioni.Text.Trim());
                command.Parameters.AddWithValue("@QuestMinnesotaLife", "");//this.Txt_Quest_Minnesota_Life.Text.Trim());
                command.Parameters.AddWithValue("@CostoTotaleTerapia", "");//this.Txt_Costo_Totale.Text.Trim());
                command.Parameters.AddWithValue("@BAV", this.Ddl_Esami_Strumentali_BAV.SelectedValue);
                command.Parameters.AddWithValue("@BAVHolter", this.Ddl_Esami_Strumentali_BAVHolter.SelectedValue);
                command.Parameters.AddWithValue("@gDataCVE", Utility.CodificaData(txtDataCVE.Text.Trim()));
                command.Parameters.AddWithValue("@gAblazione", txtAblazione.Text);
                command.Parameters.AddWithValue("@gPreeccitazione", txtPreeccitazione.Text);
                command.Parameters.AddWithValue("@gPASistolica", txtPASistolica.Text);
                command.Parameters.AddWithValue("@gPADiastolica", txtPADiastolica.Text);
                command.Parameters.AddWithValue("@gStenosiAortica", txtStenosiAortica.Text);
                command.Parameters.AddWithValue("@gStenosiMitralica", txtStenosiMitralica.Text);
                command.Parameters.AddWithValue("@gATH", txtATH.Text);
                command.Parameters.AddWithValue("@gGMWT", txtGMWTEc.Text);
                command.Parameters.AddWithValue("@gPO2", txtPO2.Text);
                command.Parameters.AddWithValue("@gPCD2", txtPCD2.Text);

                 command.Parameters.AddWithValue("@gPM",Terapia_ch_PM.Checked);
            command.Parameters.AddWithValue("@gCausalePM",Terapia_txt_Causale.Text);
            command.Parameters.AddWithValue("@gDataPM",Utility.CodificaData(Terapia_data_PM.Text.Trim()));
            command.Parameters.AddWithValue("@gTipologiaPM",Terapia_txt_Tipologia.Text); 
            command.Parameters.AddWithValue("@gICD",Terapia_ch_ICD.Checked); 
           command.Parameters.AddWithValue("@gCausaleICD",Terapia_txt_CausaleImp.Text); 
           command.Parameters.AddWithValue("@gDataICD",Utility.CodificaData(Terapia_data_impianto.Text.Trim())); 
            command.Parameters.AddWithValue("@gScaricheAppICD",Terapia_txt_scaricheApp.Text); 
           command.Parameters.AddWithValue("@gScaricheInappICD",Terapia_txt_scaricheInapp.Text);
           command.Parameters.AddWithValue("@gAblazioneICD",Terapia_txt_Ablazione.Text);
             command.Parameters.AddWithValue("@gPreeccitazioneICD", Terapia_txt_preeccitazione.Text);
            command.Parameters.AddWithValue("@gCausalePMBiv",Terapia_txt_CausaleBiventricolare.Text);
            command.Parameters.AddWithValue("@gDataPMBiv", Utility.CodificaData(Terapia_data_PmBiventricolare.Text.Trim()));

                //UPDATE 2011
            command.Parameters.AddWithValue("@lPiastrine", Txt_Laboratorio_piastrine.Text);
            command.Parameters.AddWithValue("@lFerritina", Txt_Laboratorio_ferritina.Text);
            command.Parameters.AddWithValue("@lADH", Txt_laboratorio_adh.Text);
            command.Parameters.AddWithValue("@lAlbumina", Txt_laboratorio_albumina.Text);
            command.Parameters.AddWithValue("@lAldosterone", Txt_laboratorio_aldosterone.Text);
            command.Parameters.AddWithValue("@lVitD", Txt_Laboratorio_VitD.Text);
            command.Parameters.AddWithValue("@lEpinefrina", Txt_Laboratorio_Epinefrina.Text);
            command.Parameters.AddWithValue("@AltroTerapie", txtNoteTerapie.Text);
            command.Parameters.AddWithValue("@Data_Anamnesi_Dispnea_Nott", this.Cld_Data_Anamnesi_Dispnea_Nott.Text);

                command.Parameters.AddWithValue("@TPSV", radioTPSV.SelectedValue);
                command.Parameters.AddWithValue("@TPSVAblazione", radioTPSVAblazione.SelectedValue);
                 command.Parameters.AddWithValue("@PreeccitazioneTac", radioPreeccitazioneTachicardia.SelectedValue);
                 command.Parameters.AddWithValue("@PreeccitazioneTacAblazione", radioPreeccitazioneTachicardiaAblazione.SelectedValue);
                // command.Parameters.AddWithValue("@DataCardioMiopatia", this.DataCardioMiopatia.Text);
                 Response.Write(this.chb_cardiomiopatia.Checked);
                 command.Parameters.AddWithValue("@Cardiomiopatia", this.chb_cardiomiopatia.Checked+"");
                
            //Response.Write("DATA:" + Cld_Data_Anamnesi_Dispnea_Nott.Text);
                //


                int value = 0;

               // if (radioIpertiroid.SelectedIndex == 0) { value = 1; } else { value = 0; }
                //command.Parameters.AddWithValue("@gIpertiroid", value);
                command.Parameters.AddWithValue("@gIpertiroid", "");

                //if (radioIpotiroid.SelectedIndex == 0) { value = 1; } else { value = 0; }
                //command.Parameters.AddWithValue("@gIpotiroid", value);
                command.Parameters.AddWithValue("@gIpotiroid", "");

                if (this.Session["IdUtente"] != null)
                {
                    command.Parameters.AddWithValue("@IdUtente", this.Session["IdUtente"].ToString());
                }
                else
                {
                    command.Parameters.AddWithValue("@IdUtente", 0);
                }
               // Response.Write("IDQUADRO: " + this.idQuadro);
                if (this.idQuadro.Equals(-1))
                {
                    command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
                }
                else
                {
                    command.Parameters.AddWithValue("@IdQuadro", this.idQuadro);
                }
                command.ExecuteNonQuery();
                //Response.Write("IDQUADRO: " + this.idQuadro);
                if (this.idQuadro.Equals(-1))
                {
                    SqlCommand command2 = new SqlCommand("ultimo_quadro", connection, transaction);
                    command2.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = command2.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        this.IdUltimoQuadro = Convert.ToInt32(reader["ultimoquadro"]);
                        //Response.Write("ULTIMO IDQUADRO: " + this.IdUltimoQuadro);
                        reader.Close();
                        this.ApportaModificheFarmaci(transaction, connection, this.IdUltimoQuadro);
                    }
                }
                else
                {
                    this.ApportaModificheFarmaci(transaction, connection, this.idQuadro);
                }
                //this.trova_id_dispnea(this.idQuadro);
                //this.ApportaModificheDispnea(transaction, connection, this.Cld_Data_Anamnesi_Dispnea_Nott, this.IdDispnea1);
                //this.ApportaModificheDispnea(transaction, connection, this.Cld2_Data_Anamnesi_Dispnea_Nott, this.IdDispnea2);
                //this.ApportaModificheDispnea(transaction, connection, this.Cld3_Data_Anamnesi_Dispnea_Nott, this.IdDispnea3);
                //this.ApportaModificheReliquati(transaction, connection);
                //this.trova_id_sincope(this.idQuadro);
                //this.ApportaModificheSincope(transaction, connection, this.Cld1_Reliquati_Motori_Sincope, this.Ddl1_Reliquati_Motori_Tipologia_Sincope, this.IdSincope1);
                //this.ApportaModificheSincope(transaction, connection, this.Cld2_Reliquati_Motori_Sincope, this.Ddl2_Reliquati_Motori_Tipologia_Sincope, this.IdSincope2);
                //this.ApportaModificheSincope(transaction, connection, this.Cld3_Reliquati_Motori_Sincope, this.Ddl3_Reliquati_Motori_Tipologia_Sincope, this.IdSincope3);
                transaction.Commit();
                return true;
            }
            catch (Exception exception)
            {
                Response.Write(exception.Message + " " + exception.StackTrace);
                transaction.Rollback();
                this.lb_mess.Text = exception.Message;
                inviaMailErrore(exception.Message, exception.StackTrace);
                return false;
            }
            finally
            {
                connection.Close();
            }
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message + " " + exception2.StackTrace; ;
        }
        return false;
    }
    private void inviaMailErrore(string msg, string exception)
    {
        try
        {
            //INVIA MAIL DI ALERT
            MailMessage mm = new MailMessage();
            mm.From = new MailAddress("info@mediasoftonline.com");
            //mm.To.Add(new MailAddress(ConfigurationManager.AppSettings["mailAlertReclutamento"].ToString()));
            mm.To.Add(new MailAddress("gianni.inguscio@mediasoftonline.com"));
            // mm.To.Add(new MailAddress("gianni.inguscio@mediasoftonline.com"));
            mm.Subject = "GISC - ERRORE ";
            mm.IsBodyHtml = true;
            mm.Body = "Salve,<br/> errore generato da utente " + this.Session["Utente"] + ": <br/> Message: " + msg + " <br/><br/><br/>Stack trace:" + exception;
            //SmtpClient sc = new SmtpClient("authsmtp.mediasoftonline.com");
            //System.Net.NetworkCredential SMTPUserInfo = new System.Net.NetworkCredential("smtp@mediasoftonline.com", "mediasoft");
            //sc.Credentials = SMTPUserInfo;
            //sc.UseDefaultCredentials = false;
            SmtpClient sc = new SmtpClient("smtp.aruba.it");
            sc.Send(mm);


        }
        catch (Exception ex)
        {
            Response.Write(ex.Message + " " + ex.StackTrace);

        }
    }
    private bool ApportaModificheDispnea(SqlTransaction trans, SqlConnection conn, TextBox cld1, int iddispnea)
    {
        string str;
        bool flag = false;
        if (this.idQuadro.Equals(-1))
        {
            str = "inserisci_dispnea";
        }
        else
        {
            str = "modifica_dispnea";
        }
        try
        {
            int idUltimoQuadro;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.idQuadro.Equals(-1))
            {
                idUltimoQuadro = this.IdUltimoQuadro;
            }
            else
            {
                idUltimoQuadro = this.idQuadro;
                command.Parameters.AddWithValue("@IdDispneaParosNott", iddispnea);
            }
            command.Parameters.AddWithValue("@IdQuadro", idUltimoQuadro);
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(cld1.Text.Trim()));
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }
   
    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        if (!controllaCampiObbligatori())
        {
            return;
        }
        else
        {
            this.lb_mess.Text = "";
        }
        if ((((!this.Txt_Nome.Text.Contains("<") && !this.Txt_Cognome.Text.Contains("<")) && (!this.Txt_Indirizzo.Text.Contains("<") && !this.Txt_Telefono.Text.Contains("<"))) && ((!this.Txt_Codice_Fiscale.Text.Contains("<") && !this.Txt_Eta.Text.Contains("<")) && (!this.Txt_Altezza.Text.Contains("<") && !this.Txt_Peso1.Text.Contains("<")))) && (!this.Txt_Eta.Text.Contains("<") && !this.Txt_Circonferenza_Addominale.Text.Contains("<")))
        {
            if (((this.Txt_Nome.Text.Trim() == "") || (this.Txt_Cognome.Text.Trim() == "")) || (this.Txt_Codice_Fiscale.Text.Trim() == "") ||(Cld_Data_Registrazione.Text==""))
            {
                this.lb_mess.Text = "Occorre inserire un valore per i dati obbligatori.";
            }
            else if (this.CercaDuplicati())
            {
                this.lb_mess.Text = "Paziente gi\x00e0 esistente. Verificare i dati inseriti.";
            }
            else if (this.ApportaModifiche())
            {
               
                this.inserisci_quadro();
                
                //
                if (this.IdPaziente.Equals(-1))
                {
                   
                   base.Response.Redirect("HomePageTipo.aspx?idpaziente=" + this.IdUltimoPaziente);
                }
                else
                {
                    base.Response.Redirect("HomePageTipo.aspx?idpaziente=" + this.IdPaziente);
                }
            }
        }
        else
        {
            this.lb_mess.Text = "Per motivi di sicurezza occorre eliminare i caratteri speciali dalle informazioni inserite.";
        }
    }

    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
    }
    public void impostaFontBtt()
    {
        //bttStep1.ForeColor = System.Drawing.Color.FromName("#333333");
        //bttRelMot.ForeColor = System.Drawing.Color.FromName("#333333");

        bttAnagrafica.ForeColor = System.Drawing.Color.FromName("#333333");

        bttEziologie.ForeColor = System.Drawing.Color.FromName("#333333");

        bttPrecOsp.ForeColor = System.Drawing.Color.FromName("#333333");

        bttPrecScomp.ForeColor = System.Drawing.Color.FromName("#333333");

        bttFattRischio.ForeColor = System.Drawing.Color.FromName("#333333");
      
        bttConsulenze.ForeColor = System.Drawing.Color.FromName("#333333");

            bttLaboratorio.ForeColor = System.Drawing.Color.FromName("#333333");
            bttStrumen.ForeColor = System.Drawing.Color.FromName("#333333");
            bttTerapie.ForeColor = System.Drawing.Color.FromName("#333333");
            bttAnamnesi.ForeColor = System.Drawing.Color.FromName("#333333");
            bttDiagnosi.ForeColor = System.Drawing.Color.FromName("#333333");
            LinkButton3.ForeColor = System.Drawing.Color.FromName("#333333");
    }

    protected void bttStrumen_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 2;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttAnamnesi_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 0;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttQuadro_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 1;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttLaboratorio_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 4;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttConsulenze_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 5;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        Wizard1_FinishButtonClick(null, null);
    }
    protected void bttStrumen_Click1(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 6;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttLaboratorio_Click1(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 7;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttConsulenze_Click1(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 8;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttTerapie_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 9;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttQuadroClinico_Click1(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 11;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttAnamnesi_Click1(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 3;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void LinkButton4_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 10;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
}

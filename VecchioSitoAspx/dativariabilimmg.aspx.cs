﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
public partial class dativariabilimmg : System.Web.UI.Page
{
    private string conn_str;


    private int IdDispnea1;
    private int IdDispnea2;
    private int IdDispnea3;
    private int IdPaziente;
    private int idquadro;
    private int idreliquato;
    private int IdSincope1;
    private int IdSincope2;
    private int IdSincope3;
    private int IdUltimoQuadro;


  /*  private bool ApportaModificheDispnea(SqlTransaction trans, SqlConnection conn, TextBox cld1, int iddispnea)
    {

        string str;
        bool flag = false;
        if (this.idquadro.Equals(-1))
        {
            str = "inserisci_dispnea";
        }
        else
        {
            str = "modifica_dispnea";
        }
        try
        {
            int idUltimoQuadro;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.idquadro.Equals(-1))
            {
                idUltimoQuadro = this.IdUltimoQuadro;
            }
            else
            {
                idUltimoQuadro = this.idquadro;
                command.Parameters.AddWithValue("@IdDispneaParosNott", iddispnea);
            }
            command.Parameters.AddWithValue("@IdQuadro", idUltimoQuadro);
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(cld1.Text.Trim()));
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }
    */
    private bool ApportaModificheFarmaci(SqlTransaction trans, SqlConnection conn, int idQuadro)
    {
        string str;
        bool flag = false;
        if (this.idquadro.Equals(-1))
        {
            str = "inserisci_farmaci";
        }
        else
        {
            str = "modifica_farmaci";
        }
        try
        {
            foreach (GridViewRow row in this.gv_farmaci.Rows)
            {
                int idquadro;
                string selectedValue = "";
                string str3 = "";
                string str4 = "";
                string nomeFarmaco = "";
                if (row.Cells[1].Text.IndexOf("<span></span>") > -1)
                { nomeFarmaco = row.Cells[1].Text.Substring(row.Cells[1].Text.IndexOf("<span></span>")).Replace("<span></span>", ""); }
                else
                { nomeFarmaco = row.Cells[1].Text; };
                if (row.Cells[5].Controls.Count > 0)
                {
                    TextBox box = (TextBox)row.Cells[5].Controls[1];
                    selectedValue = box.Text.Trim();
                    if ((nomeFarmaco != "Furosemide") && (nomeFarmaco != "Torasemide 10") && (nomeFarmaco != "Carvedilolo"))
                    {
                        RadioButtonList list = (RadioButtonList)row.Cells[5].Controls[3];
                        selectedValue = list.SelectedItem.Text.ToUpper();
                    }
                    // Response.Write("FARMACO:" + nomeFarmaco + " VALORE:" + selectedValue + "<BR/>");
                }

                if (row.Cells[6].Controls.Count > 0)
                {
                    CheckBox box2 = (CheckBox)row.Cells[6].Controls[1];
                    if (box2.Checked)
                    {
                        str3 = "S";
                    }
                    else
                    {
                        str3 = "N";
                    }
                }
                if (row.Cells[7].Controls.Count > 0)
                {
                    TextBox box3 = (TextBox)row.Cells[7].Controls[1];
                    str4 = box3.Text.Trim();
                }
                SqlCommand command = new SqlCommand(str, conn, trans);
                command.CommandType = CommandType.StoredProcedure;
                if (this.idquadro.Equals(-1))
                {
                    idquadro = idQuadro;
                }
                else
                {
                    idquadro = this.idquadro;
                }
                command.Parameters.AddWithValue("@IdQuadroClinico", idquadro);

                //Response.Write("ACQUISISCO:" + nomeFarmaco  + "<br/>");

                command.Parameters.AddWithValue("@NomeFarmaco", nomeFarmaco);
                command.Parameters.AddWithValue("@Valore", selectedValue);
                command.Parameters.AddWithValue("@Intolleranza", str3);
                command.Parameters.AddWithValue("@CostoFarmaco", str4);
                command.ExecuteNonQuery();
                // Response.Write("CARICO: " + str + " QUADRO:" + idquadro + " NOME:" + nomeFarmaco + " Valore:" + selectedValue + " Intolleranza:" + str3 + " Costo:" + str4 + " <br/>");
            }
            flag = true;
        }
        catch (Exception exception)
        {
            Response.Write(exception.Message + " " + exception.StackTrace);
        }
        return flag;
    }

    /*
     private bool ApportaModificheReliquati(SqlTransaction trans, SqlConnection conn)
     {
         string str;
         bool flag = false;
         if (this.idquadro.Equals(-1))
         {
             str = "inserisci_reliquato";
         }
         else
         {
             str = "modifica_reliquato";
         }
         try
         {
             int idUltimoQuadro;
             SqlCommand command = new SqlCommand(str, conn, trans);
             command.CommandType = CommandType.StoredProcedure;
             if (this.idquadro.Equals(-1))
             {
                 idUltimoQuadro = this.IdUltimoQuadro;
             }
             else
             {
                 idUltimoQuadro = this.idquadro;
             }
             command.Parameters.AddWithValue("@IdQuadroClinico", idUltimoQuadro);
             command.Parameters.AddWithValue("@DataIctus", Utility.CodificaData(this.Cld_Reliquati_Motori_Ictus.Text.Trim()));
             command.Parameters.AddWithValue("@DataTIA", Utility.CodificaData(this.Cld_Reliquati_Motori_TIA.Text.Trim()));
             command.Parameters.AddWithValue("@NumSincope", Convert.ToInt32(this.Txt_Reliquati_Motori_Sincope_Episodi.Text.Trim()));
             command.Parameters.AddWithValue("@NomePatologiaAortica", this.Txt_Quadro_Clinico_Patologia_Aortica.Text.Trim());
             command.Parameters.AddWithValue("@DataPatolAortica", Utility.CodificaData(this.Cld_Reliquati_Motori_Data_Patologia_Aortica.Text.Trim()));
             command.Parameters.AddWithValue("@DopplerPatolAortica", this.Txt_Reliquati_Motori__Document_Doppler.Text.Trim());
             command.Parameters.AddWithValue("@TacPatolAortica", this.Txt_Reliquati_Motori_Document_TAC.Text.Trim());
             command.Parameters.AddWithValue("@VascoPerifData", Utility.CodificaData(this.Cld_Data_Reliquati_Motori_Vascolare_Periferica.Text.Trim()));
             command.Parameters.AddWithValue("@VascoPerifDocDoppler", this.Txt_Reliquati_Motori_Vascolare_Perif_Document_Doppler.Text.Trim());
             command.Parameters.AddWithValue("@ChemioData", Utility.CodificaData(this.Cld__Reliquati_Motori_Data_Chemioterapia.Text.Trim()));
             command.Parameters.AddWithValue("@ChemioFarmaci", this.Txt_Reliquati_Motori_Farmaci.Text.Trim());
             command.Parameters.AddWithValue("@RadioData", Utility.CodificaData(this.Cld_Reliquati_Motori_Data_Radioter_Tor.Text.Trim()));
             command.Parameters.AddWithValue("@HIV", this.Rbl_Reliquati_Motori_Hiv.Text.Trim());
             command.Parameters.AddWithValue("@EpatHCVCorr", this.Rbl_Reliquati_Motori_Epat_Hcv_Corr.Text.Trim());
             command.Parameters.AddWithValue("@Alcoolismo", this.Rbl_Reliquati_Motori_Alcoolismo.Text.Trim());
             command.Parameters.AddWithValue("@Stupefacenti", this.Txt_Reliquati_Motori_Stupefacenti.Text.Trim());
             command.Parameters.AddWithValue("@Altro", this.Txt_Reliquati_Motori_Altro.Text.Trim());
             command.Parameters.AddWithValue("@LivelloAnsieta", this.Ddl_Reliquati_Motori_Grado_Ansietà.SelectedValue);
             command.Parameters.AddWithValue("@DataAnsieta", Utility.CodificaData(this.Cld_Reliquati_Motori_Ansieta.Text.Trim()));
             command.Parameters.AddWithValue("@TerapiaAnsieta", this.Txt_Reliquati_Motori_Ansieta_Terapia.Text.Trim());
             command.Parameters.AddWithValue("@DataDepr", Utility.CodificaData(this.Cld_Reliquati_Motori_Depressione.Text.Trim()));
             command.Parameters.AddWithValue("@LivelloDepr", this.Ddl_Reliquati_Motori_Grado_Depressione.SelectedValue);
             command.Parameters.AddWithValue("@TerapiaDepr", this.Txt_Reliquati_Motori_Terapie_Depressione.Text.Trim());
             command.Parameters.AddWithValue("@TraumiPsichi", this.Txt_Reliquati_Motori_Traumi_Psichici_Specifica.Text.Trim());
             command.Parameters.AddWithValue("@ConfFarm", this.Ddl_Reliquati_Motori_Conflittualita_Famigliare.SelectedValue);
             command.Parameters.AddWithValue("@ConfLav", this.Ddl_Reliquati_Motori_Conflittualita_Lavorativa.SelectedValue);
             command.Parameters.AddWithValue("@ConfSoc", this.Ddl_Reliquati_Motori_Conflittualita_Sociale.SelectedValue);
             command.ExecuteNonQuery();
             flag = true;
         }
         catch (Exception exception)
         {
             this.lb_mess.Text = exception.Message;
         }
         return flag;
     }

     private bool ApportaModificheSincope(SqlTransaction trans, SqlConnection conn, TextBox cld1, DropDownList drop1, int idsincope)
     {
         string str;
         bool flag = false;
         if (this.idquadro.Equals(-1))
         {
             str = "inserisci_sincope";
         }
         else
         {
             str = "modifica_sincope";
         }
         try
         {
             int idUltimoQuadro;
             SqlCommand command = new SqlCommand(str, conn, trans);
             command.CommandType = CommandType.StoredProcedure;
             if (this.idquadro.Equals(-1))
             {
                 idUltimoQuadro = this.IdUltimoQuadro;
             }
             else
             {
                 idUltimoQuadro = this.idquadro;
                 command.Parameters.AddWithValue("@IdSincope", idsincope);
             }
             command.Parameters.AddWithValue("@IdQuadro", idUltimoQuadro);
             command.Parameters.AddWithValue("@data", Utility.CodificaData(cld1.Text.Trim()));
             command.Parameters.AddWithValue("@TipoSincope", drop1.SelectedValue);
             command.Parameters.AddWithValue("@Descrizione", "");
             command.ExecuteNonQuery();
             flag = true;
         }
         catch (Exception exception)
         {
             this.lb_mess.Text = exception.Message;
         }
         return flag;
     }*/

     protected void CreaMenuStep(string current_step_id)
     {
         /*this.lb_steps.Text = "<div class=\"menu_step\"><ul>";
         foreach (Control control in this.Wizard1.WizardSteps)
         {
             if (control.GetType().ToString().Equals("System.Web.UI.WebControls.WizardStep"))
             {
                 WizardStep step = (WizardStep)control;
                 if (step.ID.Equals(current_step_id))
                 {
                     this.lb_steps.Text = this.lb_steps.Text + "<li class=\"current\"><span style=\"height:35px;\">" + step.Title + "</span></li>";
                 }
                 else
                 {
                     this.lb_steps.Text = this.lb_steps.Text + "<li><span style=\"height:35px;\">" + step.Title + "</span></li>";
                 }
             }
         }
         this.lb_steps.Text = this.lb_steps.Text + "</ul></div>";*/

     }
  
     private void ElencaFarmaci()
     {
         DataTable dataTable = new DataTable();
         SqlConnection connection = new SqlConnection(this.conn_str);
         try
         {
             connection.Open();
             string cmdText = "";
             if (this.idquadro.Equals(-1))
             {
                 cmdText = "select Farmaco.IdFarmaco as IdFarmaco, NomeFarmaco, '' as Valore, IdFarmaco as Intolleranza, CostoFarmaco from Farmaco order by NomeFarmaco";
             }
             else
             {
                 cmdText = "select Farmaco.IdFarmaco as IdFarmaco, NomeFarmaco, Valore, Intolleranza, PrevistoInTerapia.CostoFarmaco as CostoFarmaco from Farmaco left outer join PrevistoInTerapia on Farmaco.IdFarmaco=PrevistoInTerapia.IdFarmaco";
                 cmdText = cmdText + " where IdQuadroClinico=" + this.idquadro + " order by Farmaco.NomeFarmaco";
             }
             SqlCommand command = new SqlCommand(cmdText, connection);
             command.CommandType = CommandType.Text;
             SqlDataAdapter adapter = new SqlDataAdapter();
             adapter.SelectCommand = command;
             try
             {
                 try
                 {
                     adapter.Fill(dataTable);
                 }
                 finally
                 {
                     connection.Dispose();
                 }
             }
             catch (Exception exception)
             {
                 this.lb_mess.Text = exception.Message;
             }
             finally
             {
                 adapter = null;
             }
             this.gv_farmaci.DataSource = dataTable;
             this.lb_gv.Text = "<div class=\"div_reset\"><a href=\"#\" onclick=\"document.form1.Wizard1$Txt_Esami_Strumentali_Diagnosi.value='';document.form1.Wizard1$Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA.selectedIndex=0;document.form1.Wizard1$Txt_Esami_Strumentali_Peso1.value='';document.form1.Wizard1$Rbl_Esami_Strumentali_Counceling[1].checked=true;";
             this.gv_farmaci.DataBind();
             this.lb_gv.Text = this.lb_gv.Text + "return false;\">reset</a></div>";
             if (dataTable.Rows.Count == 0)
             {
                 this.gv_farmaci.Visible = false;
                 this.lb_mess.Text = "Nessun farmaco presente.";
             }
             else
             {
                 this.gv_farmaci.Visible = true;
                 this.lb_mess.Text = "";
             }
         }
         catch (Exception exception2)
         {
             this.lb_mess.Text = exception2.Message;
         }
         finally
         {
             connection.Close();
         }
     }
     /*
    private bool estrai_Dispnea_Notturna()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("estrai_dispnea", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdQuadro", this.idquadro);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            this.Cld_Data_Anamnesi_Dispnea_Nott.Text = Utility.DecodificaData(reader["Data"].ToString());
                            break;

                        case 1:
                            this.Cld2_Data_Anamnesi_Dispnea_Nott.Text = Utility.DecodificaData(reader["Data"].ToString());
                            break;

                        case 2:
                            this.Cld3_Data_Anamnesi_Dispnea_Nott.Text = Utility.DecodificaData(reader["Data"].ToString());
                            break;
                    }
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = true;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool estrai_NYHA(DropDownList dropdown)
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        bool flag = false;
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_NYHA", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            for (int i = 0; reader.Read(); i++)
            {
                ListItem item = new ListItem(reader["Denominazione"].ToString(), reader["idClasseNYHA"].ToString());
                dropdown.Items.Add(item);
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }*/
     public bool getValoreCheck(string valore)
     {
         if (valore == "1")
         {
             return true;
         }
         else
         {
             return false;
         }
     }
     private bool estrai_quadro()
     {
         bool flag = false;
         //Reponse.Write("estraggo");
         SqlConnection connection = new SqlConnection(this.conn_str);
         try
         {
             connection.Open();
             SqlCommand command = new SqlCommand("estrai_quadro", connection);
             command.CommandType = CommandType.StoredProcedure;
             command.Parameters.AddWithValue("@Idquadro", this.idquadro);
             SqlDataReader reader = command.ExecuteReader();
             if (reader.HasRows)
             {
                 reader.Read();
                 //this.Cld_Quadro_Clinico_Generale.Text = Utility.DecodificaData(reader["DataQuadroClinico"].ToString());
                 txtDataControllo.Text = Utility.DecodificaData(reader["DataQuadroClinico"].ToString());
                 txtDataProxControllo.Text = Utility.DecodificaData(reader["DataProssimoQuadroClinico"].ToString());
                 string tipo = "";
                 switch (reader["tipo"].ToString())
                 {
                     case "admin":
                         tipo = " ADMIN";
                     
                         break;
                     case "Medico di medicina generale":
                         tipo = " MMG";
                   
                         break;
                     case "Ospedaliero":
                         tipo = " OSPEDALIERO";
                       
                         break;
                     case "Cardiologo ambulatoriale":
                         tipo = " SPECIALIST";
                   
                         break;
                 }
                 //Response.Write("CIAOO");

                 //UPDATE 2011
                 this.Txt_Laboratorio_Piastrine.Text = reader["lPiastrine"].ToString();
                 this.Txt_Laboratorio_Ferritina.Text = reader["lFerritina"].ToString();
                 this.Txt_laboratorio_adh.Text = reader["lADH"].ToString();
                 this.Txt_laboratorio_albumina.Text = reader["lAlbumina"].ToString();
                 this.Txt_laboratorio_aldosterone.Text = reader["lAldosterone"].ToString();
                 this.Txt_Laboratorio_VitD.Text = reader["lVitD"].ToString();
                 this.Txt_Laboratorio_Epinefrina.Text = reader["lEpinefrina"].ToString();
                 //

                 this.lb_titolo.Text += "<br>Controllo <b>" + tipo + "</b> del <b>" + Utility.DecodificaData(reader["DataQuadroClinico"].ToString()) + "</b><br />";
                 txtSist.Text = reader["gSist"].ToString();
                 txtDiast.Text = reader["gDiast"].ToString();
                 txtDiast.Text = reader["gDiast"].ToString();
                 txtFrequenza.Text = reader["gFrequenzaCard"].ToString();
                 txtPeso.Text = reader["gPesoCorporeo"].ToString();
                 txtAttiResp.Text = reader["gAttiRespiratori"].ToString();
                 //txtCongestione.Text = reader["gCongestionePolmonare"].ToString();
                 cmbCongestione.SelectedValue = reader["gCongestionePolmonare"].ToString();
                 txtDiuresi.Text = reader["gDiuresiGiornaliera"].ToString();
                 txtDiagnosi.Text = reader["gDiagnosi"].ToString();
                 //minnesota1
                 if (reader["gMinnesota1"].ToString() == "0") { cmbMinnesota1_0.Checked = true; }
                 if (reader["gMinnesota1"].ToString() == "1") { cmbMinnesota1_1.Checked = true; }
                 if (reader["gMinnesota1"].ToString() == "2") { cmbMinnesota1_2.Checked = true; }
                 if (reader["gMinnesota1"].ToString() == "3") { cmbMinnesota1_3.Checked = true; }
                 if (reader["gMinnesota1"].ToString() == "4") { cmbMinnesota1_4.Checked = true; }
                 if (reader["gMinnesota1"].ToString() == "5") { cmbMinnesota1_5.Checked = true; }

                 //minnesota2
                
                 if (reader["gMinnesota2"].ToString() == "0") { cmbMinnesota2_0.Checked = true; }
                 if (reader["gMinnesota2"].ToString() == "1") { cmbMinnesota2_1.Checked = true; }
                 if (reader["gMinnesota2"].ToString() == "2") { cmbMinnesota2_2.Checked = true; }
                 if (reader["gMinnesota2"].ToString() == "3") { cmbMinnesota2_3.Checked = true; }
                 if (reader["gMinnesota2"].ToString() == "4") { cmbMinnesota2_4.Checked = true; }
                 if (reader["gMinnesota2"].ToString() == "5") { cmbMinnesota2_5.Checked = true; }

                 //minnesota3
                 if (reader["gMinnesota3"].ToString() == "0") { cmbMinnesota3_0.Checked = true; }
                 if (reader["gMinnesota3"].ToString() == "1") { cmbMinnesota3_1.Checked = true; }
                 if (reader["gMinnesota3"].ToString() == "2") { cmbMinnesota3_2.Checked = true; }
                 if (reader["gMinnesota3"].ToString() == "3") { cmbMinnesota3_3.Checked = true; }
                 if (reader["gMinnesota3"].ToString() == "4") { cmbMinnesota3_4.Checked = true; }
                 if (reader["gMinnesota3"].ToString() == "5") { cmbMinnesota3_5.Checked = true; }

                 //minnesota4
                 if (reader["gMinnesota4"].ToString() == "0") { cmbMinnesota4_0.Checked = true; }
                 if (reader["gMinnesota4"].ToString() == "1") { cmbMinnesota4_1.Checked = true; }
                 if (reader["gMinnesota4"].ToString() == "2") { cmbMinnesota4_2.Checked = true; }
                 if (reader["gMinnesota4"].ToString() == "3") { cmbMinnesota4_3.Checked = true; }
                 if (reader["gMinnesota4"].ToString() == "4") { cmbMinnesota4_4.Checked = true; }
                 if (reader["gMinnesota4"].ToString() == "5") { cmbMinnesota4_5.Checked = true; }

                 //minnesota5
                 if (reader["gMinnesota5"].ToString() == "0") { cmbMinnesota5_0.Checked = true; }
                 if (reader["gMinnesota5"].ToString() == "1") { cmbMinnesota5_1.Checked = true; }
                 if (reader["gMinnesota5"].ToString() == "2") { cmbMinnesota5_2.Checked = true; }
                 if (reader["gMinnesota5"].ToString() == "3") { cmbMinnesota5_3.Checked = true; }
                 if (reader["gMinnesota5"].ToString() == "4") { cmbMinnesota5_4.Checked = true; }
                 if (reader["gMinnesota5"].ToString() == "5") { cmbMinnesota5_5.Checked = true; }

                 //minnesota6
                 if (reader["gMinnesota6"].ToString() == "0") { cmbMinnesota6_0.Checked = true; }
                 if (reader["gMinnesota6"].ToString() == "1") { cmbMinnesota6_1.Checked = true; }
                 if (reader["gMinnesota6"].ToString() == "2") { cmbMinnesota6_2.Checked = true; }
                 if (reader["gMinnesota6"].ToString() == "3") { cmbMinnesota6_3.Checked = true; }
                 if (reader["gMinnesota6"].ToString() == "4") { cmbMinnesota6_4.Checked = true; }
                 if (reader["gMinnesota6"].ToString() == "5") { cmbMinnesota6_5.Checked = true; }

                 //minnesota8
                 if (reader["gMinnesota7"].ToString() == "0") { cmbMinnesota7_0.Checked = true; }
                 if (reader["gMinnesota7"].ToString() == "1") { cmbMinnesota7_1.Checked = true; }
                 if (reader["gMinnesota7"].ToString() == "2") { cmbMinnesota7_2.Checked = true; }
                 if (reader["gMinnesota7"].ToString() == "3") { cmbMinnesota7_3.Checked = true; }
                 if (reader["gMinnesota7"].ToString() == "4") { cmbMinnesota7_4.Checked = true; }
                 if (reader["gMinnesota7"].ToString() == "5") { cmbMinnesota7_5.Checked = true; }

                 //minnesota8
                 if (reader["gMinnesota8"].ToString() == "0") { cmbMinnesota8_0.Checked = true; }
                 if (reader["gMinnesota8"].ToString() == "1") { cmbMinnesota8_1.Checked = true; }
                 if (reader["gMinnesota8"].ToString() == "2") { cmbMinnesota8_2.Checked = true; }
                 if (reader["gMinnesota8"].ToString() == "3") { cmbMinnesota8_3.Checked = true; }
                 if (reader["gMinnesota8"].ToString() == "4") { cmbMinnesota8_4.Checked = true; }
                 if (reader["gMinnesota8"].ToString() == "5") { cmbMinnesota8_5.Checked = true; }

                 //minnesota9
                 if (reader["gMinnesota9"].ToString() == "0") { cmbMinnesota9_0.Checked = true; }
                 if (reader["gMinnesota9"].ToString() == "1") { cmbMinnesota9_1.Checked = true; }
                 if (reader["gMinnesota9"].ToString() == "2") { cmbMinnesota9_2.Checked = true; }
                 if (reader["gMinnesota9"].ToString() == "3") { cmbMinnesota9_3.Checked = true; }
                 if (reader["gMinnesota9"].ToString() == "4") { cmbMinnesota9_4.Checked = true; }
                 if (reader["gMinnesota9"].ToString() == "5") { cmbMinnesota9_5.Checked = true; }

                 //minnesota10
                 if (reader["gMinnesota10"].ToString() == "0") { cmbMinnesota10_0.Checked = true; }
                 if (reader["gMinnesota10"].ToString() == "1") { cmbMinnesota10_1.Checked = true; }
                 if (reader["gMinnesota10"].ToString() == "2") { cmbMinnesota10_2.Checked = true; }
                 if (reader["gMinnesota10"].ToString() == "3") { cmbMinnesota10_3.Checked = true; }
                 if (reader["gMinnesota10"].ToString() == "4") { cmbMinnesota10_4.Checked = true; }
                 if (reader["gMinnesota10"].ToString() == "5") { cmbMinnesota10_5.Checked = true; }
                 //minnesota11
                 if (reader["gMinnesota11"].ToString() == "0") { cmbMinnesota11_0.Checked = true; }
                 if (reader["gMinnesota11"].ToString() == "1") { cmbMinnesota11_1.Checked = true; }
                 if (reader["gMinnesota11"].ToString() == "2") { cmbMinnesota11_2.Checked = true; }
                 if (reader["gMinnesota11"].ToString() == "3") { cmbMinnesota11_3.Checked = true; }
                 if (reader["gMinnesota11"].ToString() == "4") { cmbMinnesota11_4.Checked = true; }
                 if (reader["gMinnesota11"].ToString() == "5") { cmbMinnesota11_5.Checked = true; }

                 lblUteQuadroSelected.Text = reader["idUtente"].ToString();
                 this.Txt_Esami_Strumentali_Diagnosi.Text = reader["Diagnosi"].ToString();
                 this.Rbl_Esami_Strumentali_Counceling.SelectedValue = reader["Counceling"].ToString();

                 this.Txt_Laboratorio_Bnp.Text = reader["IngrBNP"].ToString();
                 this.Txt_Laboratorio_Troponina.Text = reader["IngrTroponina"].ToString();
                 this.Txt_Laboratorio_Pcr.Text = reader["IngrPCR"].ToString();
                 this.Txt_Laboratorio_Uricemia.Text = reader["IngrUricemia"].ToString();
                 this.Txt_Laboratorio_Azot.Text = reader["IngrAzot"].ToString();
                 this.Txt_Laboratorio_Creat.Text = reader["IngrCreat"].ToString();
                 this.Txt_Laboratorio_Microalbumin.Text = reader["IngrMicroAlbumin"].ToString();
                 this.Txt_Laboratorio_Na.Text = reader["IngrNA"].ToString();
                 this.Txt_Laboratorio_K.Text = reader["IngrK"].ToString();
                 this.Txt_Laboratorio_Ca.Text = reader["IngrCA"].ToString();
                 this.Txt_Laboratorio_Ph.Text = reader["IngrPH"].ToString();
                 this.Txt_Laboratorio_Mg.Text = reader["IngrMG"].ToString();
                 this.Txt_Laboratorio_Got.Text = reader["IngrGOT"].ToString();
                 this.Txt_Laboratorio_Gpt.Text = reader["IngrGPT"].ToString();
                 this.Txt_Laboratorio_Cpk.Text = reader["IngrCPK"].ToString();
                 this.Txt_Laboratorio_Gr.Text = reader["IngrGR"].ToString();
                 this.Txt_Laboratorio_Emogl.Text = reader["IngrEmoglob"].ToString();
                 this.Txt_Laboratorio_Gb.Text = reader["IngrGb"].ToString();
                 this.Txt_Laboratorio_Sideremia.Text = reader["IngrSideremia"].ToString();
                 this.Txt_Laboratorio_Glicemia.Text = reader["IngrGlicemia"].ToString();
                 this.Txt_Laboratorio_Emogl_Glic.Text = reader["IngrEmoglGlic"].ToString();
                 this.Txt_Laboratorio_Col_Tot.Text = reader["IngrColTot"].ToString();
                 this.Txt_Laboratorio_Ldl.Text = reader["IngrLdl"].ToString();
                 this.Txt_Laboratorio_Hdl.Text = reader["IngrHdl"].ToString();
                 this.Txt_Laboratorio_Triglic.Text = reader["IngrTrigl"].ToString();
                 this.Txt_Laboratorio_T3.Text = reader["IngrT3"].ToString();
                 this.Txt_Laboratorio_T4.Text = reader["IngrT4"].ToString();
                 this.Txt_Laboratorio_TSH.Text = reader["IngrTSH"].ToString();
                 this.Txt_Laboratorio_Digitalemia.Text = reader["IngrDigitalemia"].ToString();
                 txtATH.Text = reader["gATH"].ToString();
                 txtNoteTerapie.Text = reader["AltroTerapie"].ToString();
                 try
                 {
                     Terapia_ch_PM.Checked = getValoreCheck(reader["gPM"].ToString());
                 }
                 catch (Exception ex) { }
                 Terapia_txt_Causale.Text = reader["gCausalePM"].ToString();
                 Terapia_data_PM.Text = Utility.DecodificaData(reader["gDataPM"].ToString());
                 Terapia_txt_Tipologia.Text = reader["gTipologiaPM"].ToString();
                 try
                 {
                     Terapia_ch_ICD.Checked = getValoreCheck(reader["gICD"].ToString());
                 }
                 catch (Exception ex) { }
                 Terapia_txt_CausaleImp.Text = reader["gCausaleICD"].ToString();
                 Terapia_data_impianto.Text = Utility.DecodificaData(reader["gDataICD"].ToString());
                 Terapia_txt_scaricheApp.Text = reader["gScaricheAppICD"].ToString();
                 Terapia_txt_scaricheInapp.Text = reader["gScaricheInappICD"].ToString();
                 Terapia_txt_Ablazione.Text = reader["gAblazioneICD"].ToString();
                 Terapia_txt_preeccitazione.Text = reader["gPreeccitazioneICD"].ToString();
                 Terapia_txt_CausaleBiventricolare.Text = reader["gCausalePMBiv"].ToString();
                 Terapia_data_PmBiventricolare.Text = Utility.DecodificaData(reader["gDataPMBiv"].ToString());


                 try
                 {
                     txtGiugulari_ch.Checked = getValoreCheck(reader["CongestionePerifGiugularimag3"].ToString());
                 }
                 catch (Exception ex) { }
                 try
                 {
                     txtEpatomegalia_ch.Checked = getValoreCheck(reader["CongestionePerifEpatomegalia"].ToString());
                 }
                 catch (Exception ex) { }
                 this.Ddl_Anamnesi_Edemi_Periferici.SelectedValue = reader["CongestionePerifEdemiPeriferici"].ToString();
                 txtVersamentoPleurico.Text = reader["CongestionePerifVersamentoPleurico"].ToString();
                 try
                 {
                     txtAscite_ch.Checked = getValoreCheck(reader["CongestionePerifAscite"].ToString());
                 }
                 catch (Exception ex) { }



                 if (Session["IdUtente"].ToString() != reader["IdUtente"].ToString())
                 {
                     if ((this.Session["Utente"].ToString() != "cardio") && (this.Session["Utente"].ToString() != "Amministratore"))
                     {
                         LinkButton1.Enabled = false;
                         LinkButton1.Text = "Non puoi effettuare modifiche a questo controllo";
                         LinkButton2.Enabled = false;
                         LinkButton2.Text = "Non puoi effettuare modifiche a questo controllo";
                         Wizard1.Enabled = false;
                         txtDataControllo.Enabled = false;
                         txtDataProxControllo.Enabled = false;
                         imgCal.Style.Add(HtmlTextWriterStyle.Display, "none");
                     }

                 }
                 flag = true;
             }
             else
             {
                 flag = false;
             }
             reader.Close();
         }
         catch (Exception exception)
         {
             flag = true;
             this.lb_mess.Text = exception.Message;
         }
         finally
         {
             connection.Close();
         }
         return flag;
     }
    /*
     private bool estrai_reliquati()
     {
         bool flag = false;
         SqlConnection connection = new SqlConnection(this.conn_str);
         try
         {
             connection.Open();
             SqlCommand command = new SqlCommand("estrai_reliquati", connection);
             command.CommandType = CommandType.StoredProcedure;
             command.Parameters.AddWithValue("@IdQuadro", this.idquadro);
             SqlDataReader reader = command.ExecuteReader();
             if (reader.HasRows)
             {
                 reader.Read();
                 this.Cld_Reliquati_Motori_Ictus.Text = Utility.DecodificaData(reader["DataIctus"].ToString());
                 this.Cld_Reliquati_Motori_TIA.Text = Utility.DecodificaData(reader["DataTIA"].ToString());
                 this.Txt_Reliquati_Motori_Sincope_Episodi.Text = reader["NumSincope"].ToString();
                 this.idreliquato = Convert.ToInt32(reader["IdReliquatoMotorio"].ToString());
                 this.Txt_Quadro_Clinico_Patologia_Aortica.Text = reader["NomePatologiaAortica"].ToString();
                 this.Cld_Reliquati_Motori_Data_Patologia_Aortica.Text = Utility.DecodificaData(reader["DataPatolAortica"].ToString());
                 this.Txt_Reliquati_Motori__Document_Doppler.Text = reader["DopplerPatolAortica"].ToString();
                 this.Txt_Reliquati_Motori_Document_TAC.Text = reader["TacPatolAortica"].ToString();
                 this.Cld_Data_Reliquati_Motori_Vascolare_Periferica.Text = Utility.DecodificaData(reader["VascoPerifData"].ToString());
                 this.Txt_Reliquati_Motori_Vascolare_Perif_Document_Doppler.Text = reader["VascoPerifDocDoppler"].ToString();
                 this.Cld__Reliquati_Motori_Data_Chemioterapia.Text = Utility.DecodificaData(reader["ChemioData"].ToString());
                 this.Txt_Reliquati_Motori_Farmaci.Text = reader["ChemioFarmaci"].ToString();
                 this.Cld_Reliquati_Motori_Data_Radioter_Tor.Text = Utility.DecodificaData(reader["RadioData"].ToString());
                 this.Rbl_Reliquati_Motori_Hiv.SelectedValue = reader["HIV"].ToString();
                 this.Txt_Reliquati_Motori_Stupefacenti.Text = reader["Stupefacenti"].ToString();
                 this.Txt_Reliquati_Motori_Altro.Text = reader["Altro"].ToString();
                 this.Rbl_Reliquati_Motori_Epat_Hcv_Corr.SelectedValue = reader["EpatHCVCorr"].ToString();
                 this.Rbl_Reliquati_Motori_Alcoolismo.SelectedValue = reader["Alcoolismo"].ToString();
                 this.Cld_Reliquati_Motori_Ansieta.Text = Utility.DecodificaData(reader["DataAnsieta"].ToString());
                 this.Ddl_Reliquati_Motori_Grado_Ansietà.SelectedValue = reader["LivelloAnsieta"].ToString();
                 this.Txt_Reliquati_Motori_Ansieta_Terapia.Text = reader["TerapiaAnsieta"].ToString();
                 this.Cld_Reliquati_Motori_Depressione.Text = Utility.DecodificaData(reader["Datadepr"].ToString());
                 this.Ddl_Reliquati_Motori_Grado_Depressione.SelectedValue = reader["LivelloDepr"].ToString();
                 this.Txt_Reliquati_Motori_Terapie_Depressione.Text = reader["TerapiaDepr"].ToString();
                 this.Txt_Reliquati_Motori_Traumi_Psichici_Specifica.Text = reader["TraumiPsichi"].ToString();
                 this.Ddl_Reliquati_Motori_Conflittualita_Famigliare.SelectedValue = reader["ConfFarm"].ToString();
                 this.Ddl_Reliquati_Motori_Conflittualita_Lavorativa.SelectedValue = reader["ConfLav"].ToString();
                 this.Ddl_Reliquati_Motori_Conflittualita_Sociale.SelectedValue = reader["ConfSoc"].ToString();
                 flag = true;
             }
             else
             {
                 flag = false;
             }
             reader.Close();
         }
         catch (Exception exception)
         {
             flag = true;
             this.lb_mess.Text = exception.Message;
         }
         finally
         {
             connection.Close();
         }
         return flag;
     }

     private bool estrai_sincope()
     {
         bool flag = false;
         SqlConnection connection = new SqlConnection(this.conn_str);
         try
         {
             connection.Open();
             SqlCommand command = new SqlCommand("estrai_sincope", connection);
             command.CommandType = CommandType.StoredProcedure;
             command.Parameters.AddWithValue("@IdQuadroClinico", this.idquadro);
             SqlDataReader reader = command.ExecuteReader();
             if (reader.HasRows)
             {
                 for (int i = 1; reader.Read(); i++)
                 {
                     switch (i)
                     {
                         case 1:
                             this.Cld1_Reliquati_Motori_Sincope.Text = Utility.DecodificaData(reader["data"].ToString());
                             this.Ddl1_Reliquati_Motori_Tipologia_Sincope.SelectedValue = reader["TipoSincope"].ToString();
                             break;

                         case 2:
                             this.Cld2_Reliquati_Motori_Sincope.Text = Utility.DecodificaData(reader["data"].ToString());
                             this.Ddl2_Reliquati_Motori_Tipologia_Sincope.SelectedValue = reader["TipoSincope"].ToString();
                             break;

                         case 3:
                             this.Cld3_Reliquati_Motori_Sincope.Text = Utility.DecodificaData(reader["data"].ToString());
                             this.Ddl3_Reliquati_Motori_Tipologia_Sincope.SelectedValue = reader["TipoSincope"].ToString();
                             break;
                     }
                 }
                 flag = true;
             }
             else
             {
                 flag = false;
             }
             reader.Close();
         }
         catch (Exception exception)
         {
             flag = true;
             this.lb_mess.Text = exception.Message;
         }
         finally
         {
             connection.Close();
         }
         return flag;
     }
     */
    private bool EstraiDati()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("cerca_paziente_id", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    //this.lb_titolo.Text = "Paziente Selezionato:<b>" + reader["Cognome"].ToString() + " " + reader["Nome"].ToString() + "</b><span style='font-size:12px'> (Reclutato il " + Utility.DecodificaData(reader["DataRegistrazione"].ToString()) + ")</span>" + this.lb_titolo.Text;
                    this.lb_titolo.Text = "Paziente Selezionato:<b><a href='HomePageTipo.aspx?idPaziente=" + reader["idPaziente"].ToString() + "'>" + reader["Cognome"].ToString() + " " + reader["Nome"].ToString() + "</a></b> <span style='font-size:12px'>(Reclutato il " + Utility.DecodificaData(reader["DataRegistrazione"].ToString()) + ")</span>" + this.lb_titolo.Text;
                    //Response.Write(Session["IdUtente"].ToString() + "!="+ reader["IdUtente"].ToString());
                   
                }
               
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }
    protected string alfabeto = "";
    protected void gv_farmaci_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.Cells[5].Controls.Count > 0)
        {
            //Response.Write("NOME FARMACO:" + e.Row.Cells[1].Text + " </br>");
            if ((e.Row.Cells[1].Text == "Furosemide") || (e.Row.Cells[1].Text == "Torasemide 10") || (e.Row.Cells[1].Text == "Carvedilolo"))
            {
                TextBox box = (TextBox)e.Row.Cells[5].Controls[1];
                box.Text = e.Row.Cells[2].Text.Replace("&nbsp;","");
                box.Visible = true;
                RadioButtonList rbl = (RadioButtonList)e.Row.Cells[5].Controls[3];
                rbl.Visible = false;
            }
            else
            {
                TextBox box = (TextBox)e.Row.Cells[5].Controls[1];
                box.Visible = false;
                RadioButtonList list = new RadioButtonList();
                list.Items.Add(new ListItem("SI", "SI"));
                list.Items.Add(new ListItem("NO", "NO"));
                box.Text = e.Row.Cells[2].Text;
                //Response.Write("VALOREEEE:" + box.Text + " ESITO:" + (box.Text == "SI") + "<br/>");
                if (box.Text == "SI")
                {

                    list.SelectedIndex = 0;
                }
                else
                {

                    list.SelectedIndex = 1;
                }

                e.Row.Cells[5].Controls.AddAt(3, list);

            }

        }
        if (e.Row.Cells[6].Controls.Count > 0)
        {
            CheckBox box3 = (CheckBox)e.Row.Cells[6].Controls[1];
            if (e.Row.Cells[3].Text.Equals("S") && !this.idquadro.Equals(-1))
            {
                box3.Checked = true;
            }
            this.lb_gv.Text = this.lb_gv.Text + "document.form1." + box3.UniqueID + ".checked=false;";
        }
        if (e.Row.Cells[7].Controls.Count > 0)
        {
            TextBox box4 = (TextBox)e.Row.Cells[7].Controls[1];
            if (!this.idquadro.Equals(-1) && !e.Row.Cells[4].Text.Equals("&nbsp;"))
            {
                box4.Text = e.Row.Cells[4].Text;
            }
            this.lb_gv.Text = this.lb_gv.Text + "document.form1." + box4.UniqueID + ".value='';";
        }
        e.Row.Cells[2].Visible = false;
        e.Row.Cells[3].Visible = false;
        e.Row.Cells[4].Visible = false;
        if (e.Row.RowIndex >= 0)
        {
            if (alfabeto.IndexOf(e.Row.Cells[1].Text.Substring(0, 1)) == -1)
            {
                alfabeto += e.Row.Cells[1].Text.Substring(0, 1);
                e.Row.Cells[1].Text = "<b style='font-size:16px;'>" + e.Row.Cells[1].Text.Substring(0, 1) + "</b><br/><br/><br/><span></span>" + e.Row.Cells[1].Text;
            }
        }
    }
    
    private bool inserisci_quadro()
    {
        //if (!this.idquadro.Equals(-1))
        //{
        //    if (Session["IdUtente"].ToString() != lblUteQuadroSelected.Text)
        //    {
        //        Page.RegisterStartupScript("noMod", "<script>alert('Non puoi modificare questo controllo poichè è stato creato da un altro utente');</script>");
        //        return false;
        //    }
        //}
        string str;
        if (this.idquadro.Equals(-1))
        {
            str = "inserisci_quadro_mmg";
        }
        else
        {
            str = "modifica_quadro_mmg";
        }
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlTransaction transaction = connection.BeginTransaction();
            try
            {
                SqlCommand command = new SqlCommand(str, connection, transaction);
                command.CommandType = CommandType.StoredProcedure;
                if (str != "modifica_quadro_mmg")
                {
                    //command.Parameters.AddWithValue("@DataQuadroClinico", Utility.CodificaData(this.Cld_Quadro_Clinico_Generale.Text.Trim()));
                    command.Parameters.AddWithValue("@DataQuadroClinico", Utility.CodificaData(txtDataControllo.Text.Trim()));
                }
               
                command.Parameters.AddWithValue("@DataProssimoQuadroClinico",Utility.CodificaData(txtDataProxControllo.Text.Trim()));
                //UPDATE 2011
                command.Parameters.AddWithValue("@lPiastrine", Txt_Laboratorio_Piastrine.Text);
                command.Parameters.AddWithValue("@lFerritina", Txt_Laboratorio_Ferritina.Text);
                command.Parameters.AddWithValue("@lADH", Txt_laboratorio_adh.Text);
                command.Parameters.AddWithValue("@lAlbumina", Txt_laboratorio_albumina.Text);
                command.Parameters.AddWithValue("@lAldosterone", Txt_laboratorio_aldosterone.Text);
                command.Parameters.AddWithValue("@lVitD", Txt_Laboratorio_VitD.Text);
                command.Parameters.AddWithValue("@lEpinefrina", Txt_Laboratorio_Epinefrina.Text);
                command.Parameters.AddWithValue("@AltroTerapie", txtNoteTerapie.Text);
                //

                command.Parameters.AddWithValue("@gSist", txtSist.Text);
                command.Parameters.AddWithValue("@gDiast", txtDiast.Text);
             
                command.Parameters.AddWithValue("@gPesoCorporeo", txtPeso.Text);
                command.Parameters.AddWithValue("@gAttiRespiratori", txtAttiResp.Text);
                //command.Parameters.AddWithValue("@gCongestionePolmonare", txtCongestione.Text);
                command.Parameters.AddWithValue("@gCongestionePolmonare", cmbCongestione.SelectedValue);

                
                command.Parameters.AddWithValue("@CongestionePerifGiugularimag3", txtGiugulari_ch.Checked);
                command.Parameters.AddWithValue("@CongestionePerifEpatomegalia", txtEpatomegalia_ch.Checked);
                command.Parameters.AddWithValue("@CongestionePerifEdemiPeriferici", this.Ddl_Anamnesi_Edemi_Periferici.SelectedValue.ToString());
                command.Parameters.AddWithValue("@CongestionePerifVersamentoPleurico", txtVersamentoPleurico.Text);
                command.Parameters.AddWithValue("@CongestionePerifAscite", txtAscite_ch.Checked);

                command.Parameters.AddWithValue("@gDiagnosi", txtDiagnosi.Text);
                command.Parameters.AddWithValue("@gFrequenzaCard", txtFrequenza.Text);
                command.Parameters.AddWithValue("@gDiuresiGiornaliera", txtDiuresi.Text);
                
                int m1 = 0, m2 = 0, m3 = 0, m4 = 0, m5 = 0, m6 = 0, m7 = 0, m8 = 0, m9 = 0, m10 = 0, m11 = 0;
                //minnesota 1
                if (cmbMinnesota1_0.Checked) { m1 = 0; } if (cmbMinnesota1_1.Checked) { m1 = 1; } if (cmbMinnesota1_2.Checked) { m1 = 2; }
                if (cmbMinnesota1_3.Checked) { m1 = 3; } if (cmbMinnesota1_4.Checked) { m1 = 4; } if (cmbMinnesota1_5.Checked) { m1 = 5; }

                //minnesota 2
                if (cmbMinnesota2_0.Checked) { m2 = 0; } if (cmbMinnesota2_1.Checked) { m2 = 1; } if (cmbMinnesota2_2.Checked) { m2 = 2; }
                if (cmbMinnesota2_3.Checked) { m2 = 3; } if (cmbMinnesota2_4.Checked) { m2 = 4; } if (cmbMinnesota2_5.Checked) { m2 = 5; }

                //minnesota 3
                if (cmbMinnesota3_0.Checked) { m3 = 0; } if (cmbMinnesota3_1.Checked) { m3 = 1; } if (cmbMinnesota3_2.Checked) { m3 = 2; }
                if (cmbMinnesota3_3.Checked) { m3 = 3; } if (cmbMinnesota3_4.Checked) { m3 = 4; } if (cmbMinnesota3_5.Checked) { m3 = 5; }

                //minnesota 4
                if (cmbMinnesota4_0.Checked) { m4 = 0; } if (cmbMinnesota4_1.Checked) { m4 = 1; } if (cmbMinnesota4_2.Checked) { m4 = 2; }
                if (cmbMinnesota4_3.Checked) { m4 = 3; } if (cmbMinnesota4_4.Checked) { m4 = 4; } if (cmbMinnesota4_5.Checked) { m4 = 5; }

                //minnesota 5
                if (cmbMinnesota5_0.Checked) { m5 = 0; } if (cmbMinnesota5_1.Checked) { m5 = 1; } if (cmbMinnesota5_2.Checked) { m5 = 2; }
                if (cmbMinnesota5_3.Checked) { m5 = 3; } if (cmbMinnesota5_4.Checked) { m5 = 4; } if (cmbMinnesota5_5.Checked) { m5 = 5; }

                //minnesota 6
                if (cmbMinnesota6_0.Checked) { m6 = 0; } if (cmbMinnesota6_1.Checked) { m6 = 1; } if (cmbMinnesota6_2.Checked) { m6 = 2; }
                if (cmbMinnesota6_3.Checked) { m6 = 3; } if (cmbMinnesota6_4.Checked) { m6 = 4; } if (cmbMinnesota6_5.Checked) { m6 = 5; }

                //minnesota 7
                if (cmbMinnesota7_0.Checked) { m7 = 0; } if (cmbMinnesota7_1.Checked) { m7 = 1; } if (cmbMinnesota7_2.Checked) { m7 = 2; }
                if (cmbMinnesota7_3.Checked) { m7 = 3; } if (cmbMinnesota7_4.Checked) { m7 = 4; } if (cmbMinnesota7_5.Checked) { m7 = 5; }

                //minnesota 8
                if (cmbMinnesota8_0.Checked) { m8 = 0; } if (cmbMinnesota8_1.Checked) { m8 = 1; } if (cmbMinnesota8_2.Checked) { m8 = 2; }
                if (cmbMinnesota8_3.Checked) { m8 = 3; } if (cmbMinnesota8_4.Checked) { m8 = 4; } if (cmbMinnesota8_5.Checked) { m8 = 5; }

                //minnesota 9
                if (cmbMinnesota9_0.Checked) { m9 = 0; } if (cmbMinnesota9_1.Checked) { m9 = 1; } if (cmbMinnesota9_2.Checked) { m9 = 2; }
                if (cmbMinnesota9_3.Checked) { m9 = 3; } if (cmbMinnesota9_4.Checked) { m9 = 4; } if (cmbMinnesota9_5.Checked) { m9 = 5; }
                
                //minnesota 10
                if (cmbMinnesota10_0.Checked) { m10 = 0; } if (cmbMinnesota10_1.Checked) { m10 = 1; } if (cmbMinnesota10_2.Checked) { m10 = 2; }
                if (cmbMinnesota10_3.Checked) { m10 = 3; } if (cmbMinnesota10_4.Checked) { m10 = 4; } if (cmbMinnesota10_5.Checked) { m10 = 5; }

                //minnesota 11
                if (cmbMinnesota11_0.Checked) { m11 = 0; } if (cmbMinnesota11_1.Checked) { m11 = 1; } if (cmbMinnesota11_2.Checked) { m11 = 2; }
                if (cmbMinnesota11_3.Checked) { m11 = 3; } if (cmbMinnesota11_4.Checked) { m11 = 4; } if (cmbMinnesota11_5.Checked) { m11 = 5; }

                command.Parameters.AddWithValue("@gMinnesota1", m1);
                command.Parameters.AddWithValue("@gMinnesota2", m2);
                command.Parameters.AddWithValue("@gMinnesota3", m3);
                command.Parameters.AddWithValue("@gMinnesota4", m4);
                command.Parameters.AddWithValue("@gMinnesota5", m5);
                command.Parameters.AddWithValue("@gMinnesota6", m6);
                command.Parameters.AddWithValue("@gMinnesota7", m7);
                command.Parameters.AddWithValue("@gMinnesota8", m8);
                command.Parameters.AddWithValue("@gMinnesota9", m9);
                command.Parameters.AddWithValue("@gMinnesota10", m10);
                command.Parameters.AddWithValue("@gMinnesota11", m11);

                if (this.Session["IdUtente"] != null)
                {
                    command.Parameters.AddWithValue("@IdUtente", this.Session["IdUtente"].ToString());
                }
                else
                {
                    command.Parameters.AddWithValue("@IdUtente", 0);
                }
                if (this.idquadro.Equals(-1))
                {
                    command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
                }
                else
                {
                    command.Parameters.AddWithValue("@IdQuadro", this.idquadro);
                }


                command.Parameters.AddWithValue("@Diagnosi", this.Txt_Esami_Strumentali_Diagnosi.Text.Trim());
                command.Parameters.AddWithValue("@Counceling", this.Rbl_Esami_Strumentali_Counceling.SelectedValue);

                command.Parameters.AddWithValue("@IngrBNP", this.Txt_Laboratorio_Bnp.Text.Trim());
                command.Parameters.AddWithValue("@IngrTroponina", this.Txt_Laboratorio_Troponina.Text.Trim());
                command.Parameters.AddWithValue("@IngrPCR", this.Txt_Laboratorio_Pcr.Text.Trim());
                command.Parameters.AddWithValue("@IngrUricemia", this.Txt_Laboratorio_Uricemia.Text.Trim());
                command.Parameters.AddWithValue("@IngrAzot", this.Txt_Laboratorio_Azot.Text.Trim());
                command.Parameters.AddWithValue("@IngrCreat", this.Txt_Laboratorio_Creat.Text.Trim());
                command.Parameters.AddWithValue("@IngrMicroAlbumin", this.Txt_Laboratorio_Microalbumin.Text.Trim());
                command.Parameters.AddWithValue("@IngrNA", this.Txt_Laboratorio_Na.Text.Trim());
                command.Parameters.AddWithValue("@IngrK", this.Txt_Laboratorio_K.Text.Trim());
                command.Parameters.AddWithValue("@IngrCA", this.Txt_Laboratorio_Ca.Text.Trim());
                command.Parameters.AddWithValue("@IngrPH", this.Txt_Laboratorio_Ph.Text.Trim());
                command.Parameters.AddWithValue("@IngrMG", this.Txt_Laboratorio_Mg.Text.Trim());
                command.Parameters.AddWithValue("@IngrGOT", this.Txt_Laboratorio_Got.Text.Trim());
                command.Parameters.AddWithValue("@IngrGPT", this.Txt_Laboratorio_Gpt.Text.Trim());
                command.Parameters.AddWithValue("@IngrCPK", this.Txt_Laboratorio_Cpk.Text.Trim());
                command.Parameters.AddWithValue("@IngrGR", this.Txt_Laboratorio_Gr.Text.Trim());
                command.Parameters.AddWithValue("@IngrEmoglob", this.Txt_Laboratorio_Emogl.Text.Trim());
                command.Parameters.AddWithValue("@IngrGB", this.Txt_Laboratorio_Gb.Text.Trim());
                command.Parameters.AddWithValue("@IngrSideremia", this.Txt_Laboratorio_Sideremia.Text.Trim());
                command.Parameters.AddWithValue("@IngrGlicemia", this.Txt_Laboratorio_Glicemia.Text.Trim());
                command.Parameters.AddWithValue("@IngrEmoglGlic", this.Txt_Laboratorio_Emogl_Glic.Text.Trim());
                command.Parameters.AddWithValue("@IngrColTot", this.Txt_Laboratorio_Col_Tot.Text.Trim());
                command.Parameters.AddWithValue("@IngrHDL", this.Txt_Laboratorio_Hdl.Text.Trim());
                command.Parameters.AddWithValue("@IngrLDL", this.Txt_Laboratorio_Ldl.Text.Trim());
                command.Parameters.AddWithValue("@IngrTrigl", this.Txt_Laboratorio_Triglic.Text.Trim());
                command.Parameters.AddWithValue("@IngrT3", this.Txt_Laboratorio_T3.Text.Trim());
                command.Parameters.AddWithValue("@IngrT4", this.Txt_Laboratorio_T4.Text.Trim());
                command.Parameters.AddWithValue("@IngrTSH", this.Txt_Laboratorio_TSH.Text.Trim());
                command.Parameters.AddWithValue("@IngrDigitalemia", this.Txt_Laboratorio_Digitalemia.Text.Trim());
                command.Parameters.AddWithValue("@gATH", txtATH.Text);


                command.Parameters.AddWithValue("@gPM", Terapia_ch_PM.Checked);
                command.Parameters.AddWithValue("@gCausalePM", Terapia_txt_Causale.Text);
                command.Parameters.AddWithValue("@gDataPM", Utility.CodificaData(Terapia_data_PM.Text.Trim()));
                command.Parameters.AddWithValue("@gTipologiaPM", Terapia_txt_Tipologia.Text);
                command.Parameters.AddWithValue("@gICD", Terapia_ch_ICD.Checked);
                command.Parameters.AddWithValue("@gCausaleICD", Terapia_txt_CausaleImp.Text);
                command.Parameters.AddWithValue("@gDataICD", Utility.CodificaData(Terapia_data_impianto.Text.Trim()));
                command.Parameters.AddWithValue("@gScaricheAppICD", Terapia_txt_scaricheApp.Text);
                command.Parameters.AddWithValue("@gScaricheInappICD", Terapia_txt_scaricheInapp.Text);
                command.Parameters.AddWithValue("@gAblazioneICD", Terapia_txt_Ablazione.Text);
                command.Parameters.AddWithValue("@gPreeccitazioneICD", Terapia_txt_preeccitazione.Text);
                command.Parameters.AddWithValue("@gCausalePMBiv", Terapia_txt_CausaleBiventricolare.Text);
                command.Parameters.AddWithValue("@gDataPMBiv", Utility.CodificaData(Terapia_data_PmBiventricolare.Text.Trim()));

                command.ExecuteNonQuery();
                if (this.idquadro.Equals(-1))
                {
                    SqlCommand command2 = new SqlCommand("ultimo_quadro", connection, transaction);
                    command2.CommandType = CommandType.StoredProcedure;
                    SqlDataReader reader = command2.ExecuteReader();
                    if (reader.HasRows)
                    {
                        reader.Read();
                        this.IdUltimoQuadro = Convert.ToInt32(reader["ultimoquadro"]);
                        reader.Close();
                        this.ApportaModificheFarmaci(transaction, connection, this.IdUltimoQuadro);
                    }
                }
                else
                {
                    this.ApportaModificheFarmaci(transaction, connection, this.idquadro);
                }
                /*this.trova_id_dispnea(this.idquadro);
                this.ApportaModificheDispnea(transaction, connection, this.Cld_Data_Anamnesi_Dispnea_Nott, this.IdDispnea1);
                this.ApportaModificheDispnea(transaction, connection, this.Cld2_Data_Anamnesi_Dispnea_Nott, this.IdDispnea2);
                this.ApportaModificheDispnea(transaction, connection, this.Cld3_Data_Anamnesi_Dispnea_Nott, this.IdDispnea3);
                this.ApportaModificheReliquati(transaction, connection);
                this.trova_id_sincope(this.idquadro);
                this.ApportaModificheSincope(transaction, connection, this.Cld1_Reliquati_Motori_Sincope, this.Ddl1_Reliquati_Motori_Tipologia_Sincope, this.IdSincope1);
                this.ApportaModificheSincope(transaction, connection, this.Cld2_Reliquati_Motori_Sincope, this.Ddl2_Reliquati_Motori_Tipologia_Sincope, this.IdSincope2);
                this.ApportaModificheSincope(transaction, connection, this.Cld3_Reliquati_Motori_Sincope, this.Ddl3_Reliquati_Motori_Tipologia_Sincope, this.IdSincope3);*/
                transaction.Commit();
                return true;
            }
            catch (Exception exception)
            {
                transaction.Rollback();
                this.lb_mess.Text = exception.Message;
                return false;
            }
            finally
            {
                connection.Close();
            }
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message;
        }
             
        
        return false;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (Session["Tipo"] == null)
        {
            Response.Redirect("login.aspx");
        }
       
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        if (((base.Request.Params["idquadro"] != null) && Utility.IsSignInteger(base.Request.Params["idquadro"].ToString())) && ((base.Request.Params["idpaziente"] != null) && Utility.IsSignInteger(base.Request.Params["idpaziente"].ToString())))
        {
            this.idquadro = Convert.ToInt32(base.Request.Params["idquadro"]);
            this.IdPaziente = Convert.ToInt32(base.Request.Params["idpaziente"]);
        }
        else
        {
            base.Response.Redirect("HomepageTipo.aspx");
        }
        this.lb_mess.Text = "";
        if (!base.IsPostBack)
        {
            txtDataControllo.Text = DateTime.Now.Day + "/" + DateTime.Now.Month + "/" + DateTime.Now.Year;
            if (this.idquadro.Equals("-1"))
            {
                this.lb_titolo.Text = "Inserimento nuovo quadro clinico";
               
                this.lb_menu.Text = "";
            }
            else
            {
                if (Session["Tipo"].ToString() == "admin")
                         this.lb_menu.Text = string.Concat(new object[] { "<li><a onclick=\"return confirm('Sei sicuro di voler eliminare il controllo corrente?');\" href=\"quadriclinici.aspx?idpaziente=", this.IdPaziente, "&del=", this.idquadro, "\">Elimina Controllo<br /> Selezionato</a></li>" });
            }
            if ((base.Request.Params["mod"] != null) && base.Request.Params["mod"].ToString().Equals("0"))
            {
                this.lb_mess.Text = "Il nuovo paziente \x00e8 stato inserito correttamente.";
            }
           // this.estrai_NYHA(this.Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA);
            this.estrai_quadro();
            this.EstraiDati();
          // this.estrai_Dispnea_Notturna();
            //this.estrai_reliquati();
            //this.estrai_sincope();
            this.ElencaFarmaci();
        }
       this.CreaMenuStep(this.Wizard1.ActiveStep.ID);
    }

    /*private bool trova_id_dispnea(int idquadro)
    {
        bool flag = false;
        int num = 1;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Dispnea", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Idquadro", idquadro);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                switch (num)
                {
                    case 1:
                        this.IdDispnea1 = Convert.ToInt32(reader["IdDispneaParosNott"].ToString());
                        break;

                    case 2:
                        this.IdDispnea2 = Convert.ToInt32(reader["IdDispneaParosNott"].ToString());
                        break;

                    case 3:
                        this.IdDispnea3 = Convert.ToInt32(reader["IdDispneaParosNott"].ToString());
                        break;
                }
                num++;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
            flag = false;
        }
        finally
        {
            connection.Close();
            flag = true;
        }
        return flag;
    }

    private bool trova_id_sincope(int idquadro)
    {
        bool flag = false;
        int num = 1;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("estrai_sincope", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdQuadroClinico", idquadro);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                switch (num)
                {
                    case 1:
                        this.IdSincope1 = Convert.ToInt32(reader["IdSincope"].ToString());
                        break;

                    case 2:
                        this.IdSincope2 = Convert.ToInt32(reader["IdSincope"].ToString());
                        break;

                    case 3:
                        this.IdSincope3 = Convert.ToInt32(reader["IdSincope"].ToString());
                        break;
                }
                num++;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
            flag = false;
        }
        finally
        {
            connection.Close();
            flag = true;
        }
        return flag;
    }
    */
    protected void Wizard1_ActiveStepChanged(object sender, EventArgs e)
    {
        this.CreaMenuStep(this.Wizard1.ActiveStep.ID);
        double num = 0.0;
        if (this.Wizard1.ActiveStep.ID == "WizardStep9")
        {
            foreach (GridViewRow row in this.gv_farmaci.Rows)
            {
                if (row.Cells[5].Controls.Count <= 0)
                {
                    continue;
                }
                TextBox box = (TextBox)row.Cells[5].Controls[1];
                if (row.Cells[1].Text.Equals("Anticoagulanti"))
                {
                    RadioButtonList list = (RadioButtonList)row.Cells[5].Controls[3];
                    if (list.SelectedIndex.Equals(0))
                    {
                        box.Text = "1";
                    }
                    else
                    {
                        box.Text = "";
                    }
                }
                if (!box.Text.Trim().Equals(""))
                {
                    if (Utility.IsDouble(box.Text.Trim()))
                    {
                        if (row.Cells[7].Controls.Count <= 0)
                        {
                            continue;
                        }
                        TextBox box2 = (TextBox)row.Cells[7].Controls[1];
                        if (!box2.Text.Trim().Equals(""))
                        {
                            if (Utility.IsDouble(box2.Text.Trim()))
                            {
                                num += Convert.ToDouble(box2.Text.Trim());
                                continue;
                            }
                            this.Wizard1.ActiveStepIndex = 7;
                            this.lb_mess.Text = "Occorre inserire i costi in formato numerico.";
                        }
                        else
                        {
                            this.Wizard1.ActiveStepIndex = 7;
                            this.lb_mess.Text = "Occorre inserire i costi corrispondenti alle quantit\x00e0 inserite.";
                        }
                    }
                    else
                    {
                        this.Wizard1.ActiveStepIndex = 7;
                        this.lb_mess.Text = "Occorre inserire le quantit\x00e0 in formato numerico.";
                    }
                    break;
                }
                if (row.Cells[7].Controls.Count > 0)
                {
                    TextBox box3 = (TextBox)row.Cells[7].Controls[1];
                    if (box3.Text.Trim().Equals(""))
                    {
                        continue;
                    }
                    this.Wizard1.ActiveStepIndex = 7;
                    this.lb_mess.Text = "Occorre inserire le quantit\x00e0 corrispondenti ai costi inseriti.";
                    break;
                }
            }
        }
        //this.Txt_Costo_Totale.Text = num.ToString();
    }
    
    
    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        if (this.inserisci_quadro())
        {
            if (this.idquadro.Equals(-1))
            {
                //base.Response.Redirect(string.Concat(new object[] { "quadriclinici.aspx?idpaziente=", this.IdPaziente, "&idquadro", this.IdUltimoQuadro, "&mod=3" }));
                base.Response.Redirect("HomePageTipo.aspx?idpaziente=" + this.IdPaziente);
            }
            else
            {
                //base.Response.Redirect(string.Concat(new object[] { "quadriclinici.aspx?idpaziente=", this.IdPaziente, "&idquadro", this.idquadro, "&mod=1" }));
                base.Response.Redirect("HomePageTipo.aspx?idpaziente=" + this.IdPaziente);

            }
        }
    }

    public void impostaFontBtt()
    {
       bttDettagliControllo.ForeColor = System.Drawing.Color.FromName("#333333");
        bttLaboratorio.ForeColor = System.Drawing.Color.FromName("#333333");
        bttQdv.ForeColor = System.Drawing.Color.FromName("#333333");
        bttTerapie.ForeColor = System.Drawing.Color.FromName("#333333");
        bttDiagnosi.ForeColor = System.Drawing.Color.FromName("#333333");
    }

    protected void bttTerapie_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 1;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttDettagliControllo_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 0;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttLaboratorio_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 2;
        impostaFontBtt();
        LinkButton lb = (LinkButton)(sender);
        lb.ForeColor = System.Drawing.Color.Blue;
    }
    protected void bttQdv_Click(object sender, EventArgs e)
    {
        bool flag = false;
        int countControlliMMG = 0;
        SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["conn_str"].ToString());
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("elenca_quadri_clinici_utenti", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {

                   
                    switch (reader["tipo"].ToString())
                    {
                       
                        case "Medico di medicina generale":
                            countControlliMMG++;
                            break;
                       
                    }
                    

                }
              
            }
            
            reader.Close();
        }
        catch (Exception exception)
        {
            //flag = false;
          
        }
        finally
        {
            connection.Close();
        }
        if (countControlliMMG == 0)
        {
            this.Wizard1.ActiveStepIndex = 3;
            impostaFontBtt();
            LinkButton lb = (LinkButton)(sender);
            lb.ForeColor = System.Drawing.Color.Blue;
        }
        else
        {
            panelQdv.Visible = true;
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        if (txtDataControllo.Text == "")
        {
            lb_mess.Text = "Seleziona la data del controllo prima di salvare";
            return;
        }
        try
        {
            string[] strData= txtDataControllo.Text.Split('/');
            DateTime dtControllo = new DateTime(int.Parse(strData[2]), int.Parse(strData[1]), int.Parse(strData[0]));
        }
            catch(Exception ex)
        {
                 lb_mess.Text = "La data del controllo non è valida";
                return;
         }
            if (this.inserisci_quadro())
        {
            if (this.idquadro.Equals(-1))
            {
                //base.Response.Redirect(string.Concat(new object[] { "quadriclinici.aspx?idpaziente=", this.IdPaziente, "&idquadro", this.IdUltimoQuadro, "&mod=3" }));
                base.Response.Redirect("HomePageTipo.aspx?idpaziente=" + this.IdPaziente);
            }
            else
            {
                //base.Response.Redirect(string.Concat(new object[] { "quadriclinici.aspx?idpaziente=", this.IdPaziente, "&idquadro", this.idquadro, "&mod=1" }));
                base.Response.Redirect("HomePageTipo.aspx?idpaziente=" + this.IdPaziente);

            }
        }
    }
    protected void bttNoWdv_Click(object sender, EventArgs e)
    {
        panelQdv.Visible = false;
    }
    protected void bttYesQdv_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 3;
        impostaFontBtt();
        LinkButton lb = bttQdv;
        lb.ForeColor = System.Drawing.Color.Blue;
        panelQdv.Visible = false;
    }
    protected void bttDiagnosi_Click(object sender, EventArgs e)
    {
        this.Wizard1.ActiveStepIndex = 4;
        impostaFontBtt();
        LinkButton lb = bttDiagnosi;
        lb.ForeColor = System.Drawing.Color.Blue;
        panelQdv.Visible = false;
    }
}

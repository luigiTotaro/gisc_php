﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class login2 : System.Web.UI.Page
{
    protected void Button2_Click1(object sender, EventArgs e)
    {
        if (this.ControlloUtenza())
        {
            switch (this.Session["Tipo"].ToString())
            {
                case "admin":
                    base.Response.Redirect("HomePage.aspx");
                    break;
              
            }
            base.Response.Redirect("HomePageTipo.aspx");

        }
    }

    private bool ControlloUtenza()
    {
        bool flag = false;
        string str = Utility.Encode(this.tb_password.Text);
        string connectionString = "";
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            connectionString = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        SqlConnection connection = new SqlConnection(connectionString);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("cerca_UN_Pwd_Admin", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@Username", this.tb_username.Text);
            command.Parameters.AddWithValue("@Password", str);
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                this.lb_err.Text = "Username o Password non corretti.";
                return flag;
            }
            flag = true;
            reader.Read();
            this.Session["IdUtente"] = reader["IdUtente"].ToString();
            this.Session["Utente"] = reader["Nominativo"].ToString();
            this.Session["IdSessione"] = this.Session.SessionID;
            this.Session["Tipo"] = reader["Tipo"].ToString();
            reader.Close();
            return flag;
        }
        catch (Exception exception)
        {
            this.lb_err.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
    }

    /* protected HttpApplication ApplicationInstance
     {
         get
         {
             return this.Context.ApplicationInstance;
         }
     }

     protected DefaultProfile Profile
     {
         get
         {
             return (DefaultProfile)this.Context.Profile;
         }
     }*/
}

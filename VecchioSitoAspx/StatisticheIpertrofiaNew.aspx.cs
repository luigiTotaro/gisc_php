﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


public partial class StatisticheIpertrofiaNew : System.Web.UI.Page
{
    private string conn_str;


    protected void btn_cerca_Click(object sender, EventArgs e)
    {
        this.CercaPazienti();
    }

    private void CercaPazienti()
    {
        DataTable dataTable = new DataTable();
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            string text;
            connection.Open();
            string str = " ";
            string str2 = "select Paziente.IdPaziente, CodiceFiscale, Cognome, Nome, DataRegistrazione, IdQuadroClinico, DataQuadroClinico";
            str2 = str2 + " from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' ";
            if (this.cb_Ipertrofia.Checked)
            {
                str = str + " and (ECGIpertVentrSx = '1' or Eco2DIpertrSet <> '' or Eco2DIpertrConc <> '' )";
            }
            else
            {
                str = str + " and ((ECGIpertVentrSx <> '1' and Eco2DIpertrSet = '' and Eco2DIpertrConc = '') or (ECGIpertVentrSx IS NULL)) ";
            }
            SqlCommand command = new SqlCommand(str2 + str, connection);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            try
            {
                adapter.Fill(dataTable);
                this.pn_risultati.Visible = true;
            }
            catch (Exception exception)
            {
                this.lb_mess.Text = exception.Message;
            }
            finally
            {
                adapter = null;
            }
            this.gv_pazienti.DataSource = dataTable;
            this.gv_pazienti.DataBind();
            if (dataTable.Rows.Count == 0)
            {
                this.gv_pazienti.Visible = false;
                this.lb_mess.Text = "Nessun paziente corrispondente ai criteri indicati.";
            }
            else
            {
                this.gv_pazienti.Visible = true;
                this.lb_mess.Text = "";
            }

            SqlCommand command2 = new SqlCommand("statistiche_aritmie", connection);
            command2.CommandType = CommandType.StoredProcedure;
            command2.Parameters.AddWithValue("@Condizioni", str);
            SqlDataReader reader = command2.ExecuteReader();
            reader.Read();
            if (Session["numPazienti"] == null)
                Response.Redirect("login.aspx");
            int num = Convert.ToInt32(reader["NumTot"]);
            int numTot = Convert.ToInt32(Session["numPazienti"].ToString());
            this.lb_pazienti.Text = "Pazienti: <b>" + num.ToString() + "</b><br/> (" + num * 100 / numTot + "% sul tot.)<br />";
            reader.NextResult();
            reader.Read();
            int num2 = Convert.ToInt32(reader["NumM"]);
            if (num != 0)
            {
                double num46 = (num2 * 100) / num;
                text = this.lb_pazienti.Text;
                this.lb_pazienti.Text = text + "M: <b>" + num2.ToString() + "</b> (" + num46.ToString() + "%)<br />";
            }
            else
            {
                this.lb_pazienti.Text = this.lb_pazienti.Text + "M: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            int num3 = Convert.ToInt32(reader["NumF"]);
            if (num != 0)
            {
                double num47 = (num3 * 100) / num;
                text = this.lb_pazienti.Text;
                this.lb_pazienti.Text = text + "F: <b>" + num3.ToString() + "</b> (" + num47.ToString() + "%)<br />";
            }
            else
            {
                this.lb_pazienti.Text = this.lb_pazienti.Text + "F: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (num != 0)
            {
                this.lb_eta.Text = "Et\x00e0 media: <b>" + ((double)(Convert.ToInt32(reader["SumEta"]) / num)).ToString() + "</b><br />";
            }
            else
            {
                this.lb_eta.Text = "Et\x00e0 media: <b>-</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (num2 != 0)
            {
                this.lb_eta.Text = this.lb_eta.Text + "Et\x00e0 media M: <b>" + ((double)(Convert.ToInt32(reader["SumEtaM"]) / num2)).ToString() + "</b><br />";
            }
            else
            {
                this.lb_eta.Text = this.lb_eta.Text + "Et\x00e0 media M: <b>-</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (num3 != 0)
            {
                this.lb_eta.Text = this.lb_eta.Text + "Et\x00e0 media F: <b>" + ((double)(Convert.ToInt32(reader["SumEtaF"]) / num3)).ToString() + "</b><br />";
            }
            else
            {
                this.lb_eta.Text = this.lb_eta.Text + "Et\x00e0 media F: <b>-</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_etaMin.Text = "Et\x00e0 min: <b>" + reader["EtaMin"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            this.lb_etaMax.Text = "Et\x00e0 max: <b>" + reader["EtaMax"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            this.lb_etaMin.Text = this.lb_etaMin.Text + "Et\x00e0 min M: <b>" + reader["EtaMinM"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            this.lb_etaMax.Text = this.lb_etaMax.Text + "Et\x00e0 max M: <b>" + reader["EtaMaxM"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            this.lb_etaMin.Text = this.lb_etaMin.Text + "Et\x00e0 min F: <b>" + reader["EtaMinF"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            this.lb_etaMax.Text = this.lb_etaMax.Text + "Et\x00e0 max F: <b>" + reader["EtaMaxF"].ToString() + "</b><br />";
            if (num != 0)
            {
                object obj2;
                this.ph_fumo.Visible = true;
                reader.NextResult();
                reader.Read();
                int num4 = Convert.ToInt32(reader["Fumo"]);
                double num51 = (Convert.ToDouble(num4) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                this.lb_fumatori.Text = string.Concat(new object[] { "Fumatori: <b>", num4.ToString(), "</b><br />(", Math.Round(num51, 2), "% del totale)<br />" });
                reader.NextResult();
                reader.Read();
                int num5 = Convert.ToInt32(reader["FumoM"]);
                if (num4 != 0)
                {
                    double num52 = (Convert.ToDouble(num5) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    obj2 = this.lb_fumatori.Text;
                    this.lb_fumatori.Text = string.Concat(new object[] { obj2, "M: <b>", num5.ToString(), "</b> (", Math.Round(num52, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori.Text = this.lb_fumatori.Text + "M: <b>" + num5.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num6 = Convert.ToInt32(reader["FumoF"]);
                if (num4 != 0)
                {
                    double num53 = (Convert.ToDouble(num6) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    obj2 = this.lb_fumatori.Text;
                    this.lb_fumatori.Text = string.Concat(new object[] { obj2, "F: <b>", num6.ToString(), "</b> (", Math.Round(num53, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori.Text = this.lb_fumatori.Text + "F: <b>" + num6.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num7 = Convert.ToInt32(reader["Fumo_1"]);
                this.lb_fumatori1.Text = "Fumatori +: <b>" + num7.ToString() + "</b><br />";
                if (num4 != 0)
                {
                    double num54 = (Convert.ToDouble(num7) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    obj2 = this.lb_fumatori1.Text;
                    this.lb_fumatori1.Text = string.Concat(new object[] { obj2, "(", Math.Round(num54, 2), "% dei fum.)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num8 = Convert.ToInt32(reader["FumoM_1"]);
                if (num7 != 0)
                {
                    double num55 = (Convert.ToDouble(num8) * Convert.ToDouble(100)) / Convert.ToDouble(num7);
                    obj2 = this.lb_fumatori1.Text;
                    this.lb_fumatori1.Text = string.Concat(new object[] { obj2, "M: <b>", num8.ToString(), "</b> (", Math.Round(num55, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori1.Text = this.lb_fumatori1.Text + "M: <b>" + num8.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num9 = Convert.ToInt32(reader["FumoF_1"]);
                if (num7 != 0)
                {
                    double num56 = (Convert.ToDouble(num9) * Convert.ToDouble(100)) / Convert.ToDouble(num7);
                    obj2 = this.lb_fumatori1.Text;
                    this.lb_fumatori1.Text = string.Concat(new object[] { obj2, "F: <b>", num9.ToString(), "</b> (", Math.Round(num56, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori1.Text = this.lb_fumatori1.Text + "F: <b>" + num9.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num10 = Convert.ToInt32(reader["Fumo_2"]);
                this.lb_fumatori2.Text = "Fumatori ++: <b>" + num10.ToString() + "</b><br />";
                if (num4 != 0)
                {
                    double num57 = (Convert.ToDouble(num10) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    obj2 = this.lb_fumatori2.Text;
                    this.lb_fumatori2.Text = string.Concat(new object[] { obj2, "(", Math.Round(num57, 2), "% dei fum.)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num11 = Convert.ToInt32(reader["FumoM_2"]);
                if (num10 != 0)
                {
                    double num58 = (Convert.ToDouble(num11) * Convert.ToDouble(100)) / Convert.ToDouble(num10);
                    obj2 = this.lb_fumatori2.Text;
                    this.lb_fumatori2.Text = string.Concat(new object[] { obj2, "M: <b>", num11.ToString(), "</b> (", Math.Round(num58, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori2.Text = this.lb_fumatori2.Text + "M: <b>" + num11.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num12 = Convert.ToInt32(reader["FumoF_2"]);
                if (num10 != 0)
                {
                    double num59 = (Convert.ToDouble(num12) * Convert.ToDouble(100)) / Convert.ToDouble(num10);
                    obj2 = this.lb_fumatori2.Text;
                    this.lb_fumatori2.Text = string.Concat(new object[] { obj2, "F: <b>", num12.ToString(), "</b> (", Math.Round(num59, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori2.Text = this.lb_fumatori2.Text + "F: <b>" + num12.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num13 = Convert.ToInt32(reader["Fumo_3"]);
                this.lb_fumatori3.Text = "Fumatori +++: <b>" + num13.ToString() + "</b><br />";
                if (num4 != 0)
                {
                    double num60 = (Convert.ToDouble(num13) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    obj2 = this.lb_fumatori3.Text;
                    this.lb_fumatori3.Text = string.Concat(new object[] { obj2, "(", Math.Round(num60, 2), "% dei fum.)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num14 = Convert.ToInt32(reader["FumoM_3"]);
                if (num13 != 0)
                {
                    double num61 = (Convert.ToDouble(num14) * Convert.ToDouble(100)) / Convert.ToDouble(num13);
                    obj2 = this.lb_fumatori3.Text;
                    this.lb_fumatori3.Text = string.Concat(new object[] { obj2, "M: <b>", num14.ToString(), "</b> (", Math.Round(num61, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori3.Text = this.lb_fumatori3.Text + "M: <b>" + num14.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num15 = Convert.ToInt32(reader["FumoF_3"]);
                if (num13 != 0)
                {
                    double num62 = (Convert.ToDouble(num15) * Convert.ToDouble(100)) / Convert.ToDouble(num13);
                    obj2 = this.lb_fumatori3.Text;
                    this.lb_fumatori3.Text = string.Concat(new object[] { obj2, "F: <b>", num15.ToString(), "</b> (", Math.Round(num62, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori3.Text = this.lb_fumatori3.Text + "F: <b>" + num15.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num16 = Convert.ToInt32(reader["NumCasiIpert"]);
                this.lb_ipertensione_art.Text = "Pazienti: <b>" + num16.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num63 = (Convert.ToDouble(num16) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_ipertensione_art.Text;
                    this.lb_ipertensione_art.Text = string.Concat(new object[] { obj2, "(", Math.Round(num63, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num17 = Convert.ToInt32(reader["NumCasiIpertM"]);
                if (num16 != 0)
                {
                    double num64 = (Convert.ToDouble(num17) * Convert.ToDouble(100)) / Convert.ToDouble(num16);
                    obj2 = this.lb_ipertensione_art.Text;
                    this.lb_ipertensione_art.Text = string.Concat(new object[] { obj2, "M: <b>", num17.ToString(), "</b> (", Math.Round(num64, 2), "%)<br />" });
                }
                else
                {
                    this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "M: <b>" + num17.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num18 = Convert.ToInt32(reader["NumCasiIpertF"]);
                if (num16 != 0)
                {
                    double num65 = (Convert.ToDouble(num18) * Convert.ToDouble(100)) / Convert.ToDouble(num16);
                    obj2 = this.lb_ipertensione_art.Text;
                    this.lb_ipertensione_art.Text = string.Concat(new object[] { obj2, "F: <b>", num18.ToString(), "</b> (", Math.Round(num65, 2), "%)<br />" });
                }
                else
                {
                    this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "M: <b>" + num18.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num19 = Convert.ToInt32(reader["NumCasiDiabete"]);
                this.lb_diabete.Text = "Pazienti: <b>" + num19.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num66 = (Convert.ToDouble(num19) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_diabete.Text;
                    this.lb_diabete.Text = string.Concat(new object[] { obj2, "(", Math.Round(num66, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num20 = Convert.ToInt32(reader["NumCasiDiabeteM"]);
                if (num19 != 0)
                {
                    double num67 = (Convert.ToDouble(num20) * Convert.ToDouble(100)) / Convert.ToDouble(num19);
                    obj2 = this.lb_diabete.Text;
                    this.lb_diabete.Text = string.Concat(new object[] { obj2, "M: <b>", num20.ToString(), "</b> (", Math.Round(num67, 2), "%)<br />" });
                }
                else
                {
                    this.lb_diabete.Text = this.lb_diabete.Text + "M: <b>" + num20.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num21 = Convert.ToInt32(reader["NumCasiDiabeteF"]);
                if (num19 != 0)
                {
                    double num68 = (Convert.ToDouble(num21) * Convert.ToDouble(100)) / Convert.ToDouble(num19);
                    obj2 = this.lb_diabete.Text;
                    this.lb_diabete.Text = string.Concat(new object[] { obj2, "F: <b>", num21.ToString(), "</b> (", Math.Round(num68, 2), "%)<br />" });
                }
                else
                {
                    this.lb_diabete.Text = this.lb_diabete.Text + "F: <b>" + num21.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num22 = Convert.ToInt32(reader["NumCasiIMA"]);
                this.lb_IMA.Text = "Pazienti: <b>" + num22.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num69 = (Convert.ToDouble(num22) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_IMA.Text;
                    this.lb_IMA.Text = string.Concat(new object[] { obj2, "(", Math.Round(num69, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num23 = Convert.ToInt32(reader["NumCasiIMAM"]);
                if (num22 != 0)
                {
                    double num70 = (Convert.ToDouble(num23) * Convert.ToDouble(100)) / Convert.ToDouble(num22);
                    obj2 = this.lb_IMA.Text;
                    this.lb_IMA.Text = string.Concat(new object[] { obj2, "M: <b>", num23.ToString(), "</b> (", Math.Round(num70, 2), "%)<br />" });
                }
                else
                {
                    this.lb_IMA.Text = this.lb_IMA.Text + "M: <b>" + num23.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num24 = Convert.ToInt32(reader["NumCasiIMAF"]);
                if (num22 != 0)
                {
                    double num71 = (Convert.ToDouble(num24) * Convert.ToDouble(100)) / Convert.ToDouble(num22);
                    obj2 = this.lb_IMA.Text;
                    this.lb_IMA.Text = string.Concat(new object[] { obj2, "F: <b>", num24.ToString(), "</b> (", Math.Round(num71, 2), "%)<br />" });
                }
                else
                {
                    this.lb_IMA.Text = this.lb_IMA.Text + "F: <b>" + num24.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num25 = Convert.ToInt32(reader["NumCasiBPCO"]);
                this.lb_BPCO.Text = "Pazienti: <b>" + num25.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num72 = (Convert.ToDouble(num25) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_BPCO.Text;
                    this.lb_BPCO.Text = string.Concat(new object[] { obj2, "(", Math.Round(num72, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num26 = Convert.ToInt32(reader["NumCasiBPCOM"]);
                if (num25 != 0)
                {
                    double num73 = (Convert.ToDouble(num26) * Convert.ToDouble(100)) / Convert.ToDouble(num25);
                    obj2 = this.lb_BPCO.Text;
                    this.lb_BPCO.Text = string.Concat(new object[] { obj2, "M: <b>", num26.ToString(), "</b> (", Math.Round(num73, 2), "%)<br />" });
                }
                else
                {
                    this.lb_BPCO.Text = this.lb_BPCO.Text + "M: <b>" + num26.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num27 = Convert.ToInt32(reader["NumCasiBPCOF"]);
                if (num25 != 0)
                {
                    double num74 = (Convert.ToDouble(num27) * Convert.ToDouble(100)) / Convert.ToDouble(num25);
                    obj2 = this.lb_BPCO.Text;
                    this.lb_BPCO.Text = string.Concat(new object[] { obj2, "F: <b>", num27.ToString(), "</b> (", Math.Round(num74, 2), "%)<br />" });
                }
                else
                {
                    this.lb_BPCO.Text = this.lb_BPCO.Text + "F: <b>" + num27.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num28 = Convert.ToInt32(reader["NumCasiInsuffRenale"]);
                this.lb_InsuffRenale.Text = "Pazienti: <b>" + num28.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num75 = (Convert.ToDouble(num28) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_InsuffRenale.Text;
                    this.lb_InsuffRenale.Text = string.Concat(new object[] { obj2, "(", Math.Round(num75, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num29 = Convert.ToInt32(reader["NumCasiInsuffRenaleM"]);
                if (num28 != 0)
                {
                    double num76 = (Convert.ToDouble(num29) * Convert.ToDouble(100)) / Convert.ToDouble(num28);
                    obj2 = this.lb_InsuffRenale.Text;
                    this.lb_InsuffRenale.Text = string.Concat(new object[] { obj2, "M: <b>", num29.ToString(), "</b> (", Math.Round(num76, 2), "%)<br />" });
                }
                else
                {
                    this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "M: <b>" + num29.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num30 = Convert.ToInt32(reader["NumCasiInsuffRenaleF"]);
                if (num28 != 0)
                {
                    double num77 = (Convert.ToDouble(num30) * Convert.ToDouble(100)) / Convert.ToDouble(num28);
                    obj2 = this.lb_InsuffRenale.Text;
                    this.lb_InsuffRenale.Text = string.Concat(new object[] { obj2, "F: <b>", num30.ToString(), "</b> (", Math.Round(num77, 2), "%)<br />" });
                }
                else
                {
                    this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "F: <b>" + num30.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num31 = Convert.ToInt32(reader["NumCasiAnemia"]);
                this.lb_Anemia.Text = "Pazienti: <b>" + num31.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num78 = (Convert.ToDouble(num31) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_Anemia.Text;
                    this.lb_Anemia.Text = string.Concat(new object[] { obj2, "(", Math.Round(num78, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num32 = Convert.ToInt32(reader["NumCasiAnemiaM"]);
                if (num31 != 0)
                {
                    double num79 = (Convert.ToDouble(num32) * Convert.ToDouble(100)) / Convert.ToDouble(num31);
                    obj2 = this.lb_Anemia.Text;
                    this.lb_Anemia.Text = string.Concat(new object[] { obj2, "M: <b>", num32.ToString(), "</b> (", Math.Round(num79, 2), "%)<br />" });
                }
                else
                {
                    this.lb_Anemia.Text = this.lb_Anemia.Text + "M: <b>" + num32.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num33 = Convert.ToInt32(reader["NumCasiAnemiaF"]);
                if (num31 != 0)
                {
                    double num80 = (Convert.ToDouble(num33) * Convert.ToDouble(100)) / Convert.ToDouble(num31);
                    obj2 = this.lb_Anemia.Text;
                    this.lb_Anemia.Text = string.Concat(new object[] { obj2, "F: <b>", num33.ToString(), "</b> (", Math.Round(num80, 2), "%)<br />" });
                }
                else
                {
                    this.lb_Anemia.Text = this.lb_Anemia.Text + "F: <b>" + num33.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num34 = Convert.ToInt32(reader["NumCasiBMI"]);
                this.lb_BMI.Text = "Pazienti: <b>" + num34.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num81 = (Convert.ToDouble(num34) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_BMI.Text;
                    this.lb_BMI.Text = string.Concat(new object[] { obj2, "(", Math.Round(num81, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num35 = Convert.ToInt32(reader["NumCasiBMIM"]);
                if (num34 != 0)
                {
                    double num82 = (Convert.ToDouble(num35) * Convert.ToDouble(100)) / Convert.ToDouble(num34);
                    obj2 = this.lb_BMI.Text;
                    this.lb_BMI.Text = string.Concat(new object[] { obj2, "M: <b>", num35.ToString(), "</b> (", Math.Round(num82, 2), "%)<br />" });
                }
                else
                {
                    this.lb_BMI.Text = this.lb_BMI.Text + "M: <b>" + num35.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num36 = Convert.ToInt32(reader["NumCasiBMIF"]);
                if (num34 != 0)
                {
                    double num83 = (Convert.ToDouble(num36) * Convert.ToDouble(100)) / Convert.ToDouble(num34);
                    obj2 = this.lb_BMI.Text;
                    this.lb_BMI.Text = string.Concat(new object[] { obj2, "F: <b>", num36.ToString(), "</b> (", Math.Round(num83, 2), "%)<br />" });
                }
                else
                {
                    this.lb_BMI.Text = this.lb_BMI.Text + "F: <b>" + num36.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num37 = Convert.ToInt32(reader["NumCasiIpertiroidismo"]);
                this.lb_Ipertiroidismo.Text = "Pazienti: <b>" + num37.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num84 = (Convert.ToDouble(num37) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_Ipertiroidismo.Text;
                    this.lb_Ipertiroidismo.Text = string.Concat(new object[] { obj2, "(", Math.Round(num84, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num38 = Convert.ToInt32(reader["NumCasiIpertiroidismoM"]);
                if (num37 != 0)
                {
                    double num85 = (Convert.ToDouble(num38) * Convert.ToDouble(100)) / Convert.ToDouble(num37);
                    obj2 = this.lb_Ipertiroidismo.Text;
                    this.lb_Ipertiroidismo.Text = string.Concat(new object[] { obj2, "M: <b>", num38.ToString(), "</b> (", Math.Round(num85, 2), "%)<br />" });
                }
                else
                {
                    this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "M: <b>" + num38.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num39 = Convert.ToInt32(reader["NumCasiIpertiroidismoF"]);
                if (num37 != 0)
                {
                    double num86 = (Convert.ToDouble(num39) * Convert.ToDouble(100)) / Convert.ToDouble(num37);
                    obj2 = this.lb_Ipertiroidismo.Text;
                    this.lb_Ipertiroidismo.Text = string.Concat(new object[] { obj2, "F: <b>", num39.ToString(), "</b> (", Math.Round(num86, 2), "%)<br />" });
                }
                else
                {
                    this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "F: <b>" + num39.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num40 = Convert.ToInt32(reader["NumCasiSindrome"]);
                this.lb_Sindrome.Text = "Pazienti: <b>" + num40.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num87 = (Convert.ToDouble(num40) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_Sindrome.Text;
                    this.lb_Sindrome.Text = string.Concat(new object[] { obj2, "(", Math.Round(num87, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num41 = Convert.ToInt32(reader["NumCasiSindromeM"]);
                if (num40 != 0)
                {
                    double num88 = (Convert.ToDouble(num41) * Convert.ToDouble(100)) / Convert.ToDouble(num40);
                    obj2 = this.lb_Sindrome.Text;
                    this.lb_Sindrome.Text = string.Concat(new object[] { obj2, "M: <b>", num41.ToString(), "</b> (", Math.Round(num88, 2), "%)<br />" });
                }
                else
                {
                    this.lb_Sindrome.Text = this.lb_Sindrome.Text + "M: <b>" + num41.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num42 = Convert.ToInt32(reader["NumCasiSindromeF"]);
                if (num40 != 0)
                {
                    double num89 = (Convert.ToDouble(num42) * Convert.ToDouble(100)) / Convert.ToDouble(num40);
                    obj2 = this.lb_Sindrome.Text;
                    this.lb_Sindrome.Text = string.Concat(new object[] { obj2, "F: <b>", num42.ToString(), "</b> (", Math.Round(num89, 2), "%)<br />" });
                }
                else
                {
                    this.lb_Sindrome.Text = this.lb_Sindrome.Text + "F: <b>" + num42.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num43 = Convert.ToInt32(reader["NumCasiColesterolo"]);
                this.lb_Colesterolo.Text = "Pazienti: <b>" + num43.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num90 = (Convert.ToDouble(num43) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_Colesterolo.Text;
                    this.lb_Colesterolo.Text = string.Concat(new object[] { obj2, "(", Math.Round(num90, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num44 = Convert.ToInt32(reader["NumCasiColesteroloM"]);
                if (num43 != 0)
                {
                    double num91 = (Convert.ToDouble(num44) * Convert.ToDouble(100)) / Convert.ToDouble(num43);
                    obj2 = this.lb_Colesterolo.Text;
                    this.lb_Colesterolo.Text = string.Concat(new object[] { obj2, "M: <b>", num44.ToString(), "</b> (", Math.Round(num91, 2), "%)<br />" });
                }
                else
                {
                    this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "M: <b>" + num44.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num45 = Convert.ToInt32(reader["NumCasiColesteroloF"]);
                if (num43 != 0)
                {
                    double num92 = (Convert.ToDouble(num45) * Convert.ToDouble(100)) / Convert.ToDouble(num43);
                    obj2 = this.lb_Colesterolo.Text;
                    this.lb_Colesterolo.Text = string.Concat(new object[] { obj2, "F: <b>", num45.ToString(), "</b> (", Math.Round(num92, 2), "%)<br />" });
                }
                else
                {
                    this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "F: <b>" + num45.ToString() + "</b><br />";
                }


                reader.NextResult();
                reader.Read();
                int num46 = Convert.ToInt32(reader["NumCasiIMANodoc"]);
                this.lb_IMANODoc.Text = "Pazienti: <b>" + num46.ToString() + "</b><br />";
                if (num != 0)
                {
                    double num69 = (Convert.ToDouble(num46) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                    obj2 = this.lb_IMANODoc.Text;
                    this.lb_IMANODoc.Text = string.Concat(new object[] { obj2, "(", Math.Round(num69, 2), "%)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num47 = Convert.ToInt32(reader["NumCasiIMAMNodoc"]);
                if (num46 != 0)
                {
                    double num70 = (Convert.ToDouble(num47) * Convert.ToDouble(100)) / Convert.ToDouble(num46);
                    obj2 = this.lb_IMANODoc.Text;
                    this.lb_IMANODoc.Text = string.Concat(new object[] { obj2, "M: <b>", num47.ToString(), "</b> (", Math.Round(num70, 2), "%)<br />" });
                }
                else
                {
                    this.lb_IMA.Text = this.lb_IMA.Text + "M: <b>" + num47.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num48 = Convert.ToInt32(reader["NumCasiIMAFNodoc"]);
                if (num46 != 0)
                {
                    double num71 = (Convert.ToDouble(num48) * Convert.ToDouble(100)) / Convert.ToDouble(num46);
                    obj2 = this.lb_IMANODoc.Text;
                    this.lb_IMANODoc.Text = string.Concat(new object[] { obj2, "F: <b>", num48.ToString(), "</b> (", Math.Round(num71, 2), "%)<br />" });
                }
                else
                {
                    this.lb_IMANODoc.Text = this.lb_IMANODoc.Text + "F: <b>" + num48.ToString() + "</b><br />";
                }

                this.ph_row2stat.Visible = true;
                this.ph_row3stat.Visible = true;
                this.ph_row4stat.Visible = true;

                //morti
                reader.NextResult();
                reader.Read();
                int totalMorti = int.Parse(reader["NumDeath"].ToString());
                float percMorti = float.Parse(totalMorti + "") * 100 / float.Parse(num + "");
                this.lb_pazienti.Text += "Deaths: <b>" + reader["NumDeath"] + "</b> (" + percMorti.ToString("N2") + "%) ";
                reader.NextResult();
                reader.Read();

                this.lb_pazienti.Text += "<br/>M: <b>" + reader["NumDeathM"] + "</b>";
                reader.NextResult();
                reader.Read();
                this.lb_pazienti.Text += "<br/>F: <b>" + reader["NumDeathF"] + "</b>";
            }
            else
            {
                this.ph_fumo.Visible = false;
                this.ph_row2stat.Visible = false;
                this.ph_row3stat.Visible = false;
                this.ph_row4stat.Visible = false;
            }
            reader.Close();
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message;
        }
        finally
        {
            connection.Close();
        }
    }

    protected void gv_pazienti_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gv_pazienti.PageIndex = e.NewPageIndex;
        this.CercaPazienti();
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
    }
}

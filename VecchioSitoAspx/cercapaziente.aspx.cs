﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class cercapaziente : System.Web.UI.Page
{

    private string conn_str;


    protected void btn_cerca_Click(object sender, EventArgs e)
    {
        if ((!this.tb_data_min.Text.Trim().Equals("") && !Utility.IsDate(this.tb_data_min.Text.Trim())) || (!this.tb_data_max.Text.Trim().Equals("") && !Utility.IsDate(this.tb_data_max.Text.Trim())))
        {
            this.lb_mess.Text = "Occorre inserire le date nel formato corretto.";
        }
        else
        {
            this.CercaPazienti();
        }
    }

    private void CercaPazienti()
    {
        DataTable dataTable = new DataTable();
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            string cmdText = "select Paziente.*,Nominativo,dbo.getMedicoCurante(idMedicoCurante) as MedicoCurante from Paziente,Utente where Paziente.idUtente=Utente.idUtente and CodiceFiscale!=''";
            if (!this.tb_cognome.Text.Trim().Equals(""))
            {
                cmdText = cmdText + " and Cognome like @Cognome";
            }
            if (!this.tb_codicefis.Text.Trim().Equals(""))
            {
                cmdText = cmdText + " and CodiceFiscale=@CodiceFiscale";
            }
            if (!this.tb_data_min.Text.Trim().Equals(""))
            {
                cmdText = cmdText + " and DataRegistrazione>=@DataRicoveroMin";
            }
            if (!this.tb_data_max.Text.Trim().Equals(""))
            {
                cmdText = cmdText + " and DataRegistrazione<=@DataRicoveroMax";
            }
            if (!this.ddl_medico.SelectedIndex.Equals(0))
            {
                cmdText = cmdText + " and IdMedicoCurante=@IdMedicoCurante";
            }
            if (Session["Tipo"].ToString() == "Medico di medicina generale")
            {
                cmdText = cmdText + " and IdMedicoCurante=" + this.Session["IdUtente"];
                ddl_medico.Visible = false;
                lb_MedicoCurante.Visible = false;
            }
            else
            {
                /*if (Session["Tipo"].ToString() != "admin")
                {
                    cmdText = cmdText + " and IdUtente=" + this.Session["IdUtente"];
                }*/
            }
            cmdText += " order by convert(datetime,  DataRegistrazione, 112) asc ";
            SqlCommand command = new SqlCommand(cmdText, connection);
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@Cognome", "%" + this.tb_cognome.Text + "%");
            command.Parameters.AddWithValue("@CodiceFiscale", this.tb_codicefis.Text);
            command.Parameters.AddWithValue("@DataRicoveroMin", Utility.CodificaData(this.tb_data_min.Text));
            command.Parameters.AddWithValue("@DataRicoveroMax", Utility.CodificaData(this.tb_data_max.Text));
            command.Parameters.AddWithValue("@IdMedicoCurante", this.ddl_medico.SelectedValue);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            try
            {
                try
                {
                    adapter.Fill(dataTable);
                }
                finally
                {
                    connection.Dispose();
                }
            }
            catch (Exception exception)
            {
                this.lb_mess.Text = exception.Message;
            }
            finally
            {
                adapter = null;
            }
            this.gv_pazienti.DataSource = dataTable;
            this.gv_pazienti.DataBind();
            if (dataTable.Rows.Count == 0)
            {
                this.gv_pazienti.Visible = false;
                this.lb_mess.Text = "Nessun paziente corrispondente ai criteri indicati.";
            }
            else
            {
                this.gv_pazienti.Visible = true;
                this.lb_mess.Text = "";
            }
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message;
        }
        finally
        {
            connection.Close();
        }
    }

    private void ElencaMedici()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
           // SqlCommand command = new SqlCommand("elenca_medici", connection);
            SqlCommand command = new SqlCommand("elenca_medici_mmg", connection);
            
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            ListItem item = new ListItem("[Nessun Filtro]", "-1");
            this.ddl_medico.Items.Add(item);
            while (reader.Read())
            {
                item = new ListItem(reader["Nominativo"].ToString(), reader["idUtente"].ToString());
                this.ddl_medico.Items.Add(item);
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
    }

    protected void gv_pazienti_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gv_pazienti.CurrentPageIndex = e.NewPageIndex;
        this.CercaPazienti();
    }
    void gv_pazienti_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.Item.ItemIndex == -1) { return; }
        string code = gv_pazienti.Items[e.Item.ItemIndex].Cells[0].Text;
        try
        {
            switch (e.CommandName)
            {
                case "seleziona":
                    /*codPazSel.Text = code;
                    lblPazienteSel.Text = "<br>Paziente Selezionato: <b>" + gv_pazienti.Items[e.Item.ItemIndex].Cells[1].Text + " " + gv_pazienti.Items[e.Item.ItemIndex].Cells[2].Text + " - " + gv_pazienti.Items[e.Item.ItemIndex].Cells[3].Text + "</b><br>";
                    bttSearchPaziente_Click(null, null);
                    bttNewControl.Enabled = true;
                    if ((Session["tipo"].ToString() != "Medico di medicina generale") || (Session["tipo"].ToString() != "Ospedaliero"))
                    {
                        bttNewRicovero.Enabled = true;
                    }


                    bttDead.Enabled = true;
                    loadControlli();*/
                    Response.Redirect("HomePageTipo.aspx?idpaziente=" + code);
                    break;
                case "modifica":
                    Response.Redirect("nuovopaziente.aspx?idPaziente=" + code);
                    break;
            }


        }
        catch (Exception ex)
        { }
    }
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["Utente"] == null) { Response.Redirect("login.aspx"); }
        if (Session["Tipo"].ToString() == "admin")
        {
            gv_pazienti.Columns[12].Visible = true;
        }
        else
        {
            gv_pazienti.Columns[12].Visible = false;
        }
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        gv_pazienti.ItemCommand += new DataGridCommandEventHandler(gv_pazienti_ItemCommand);
        if (!base.IsPostBack)
        {
            if (base.Request.Params["del"] != null)
            {
                SqlConnection connection = new SqlConnection(this.conn_str);
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("elimina_paziente", connection);
                    command.Parameters.AddWithValue("@IdPaziente", base.Request.Params["del"].ToString());
                    command.CommandType = CommandType.StoredProcedure;
                    command.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    this.lb_mess.Text = exception.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            this.ElencaMedici();
            this.CercaPazienti();
        }
    }

}


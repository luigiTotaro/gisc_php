﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
public partial class StudioGISC_AltreStatisticheNew : System.Web.UI.Page
{
    private string conn_str;
    protected void Page_Load(object sender, EventArgs e)
    {
        //calcolaStatistiche();
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
    }
    protected void btn_cerca_Click(object sender, EventArgs e)
    {
        lblRisultati.Text = "";
       // this.CercaPazienti();
        if (radioCardioIper.Checked)
        {
            calcolaStatistiche(" and CardioMiopatia='True' and FR.IdPaziente=Paziente.IdPaziente ", " ,FattoreRischio as FR ");
            calcolaValvolopatie(" and CardioMiopatia='True' ","");
        }
        if (radioCardioIsch.Checked)
        {
            calcolaStatistiche("  and (Paziente.idPaziente IN (Select idPaziente from CardiopIschemNOIMA where DataPrimoRilievo <>'' ) or Paziente.idPaziente IN (Select idPaziente from IMA where IMA.Data <>'') ) and FR.IdPaziente=Paziente.IdPaziente   and FR.IdPaziente=Paziente.IdPaziente ", " ,FattoreRischio as FR ");
            calcolaValvolopatie("  and (Paziente.idPaziente IN (Select idPaziente from CardiopIschemNOIMA where DataPrimoRilievo <>'' ) or Paziente.idPaziente IN (Select idPaziente from IMA where IMA.Data <>'') ) and FR.IdPaziente=Paziente.IdPaziente   and FR.IdPaziente=Paziente.IdPaziente ", " ,FattoreRischio as FR  ");
        }
        if (radioBPCO.Checked)
        {
            calcolaStatistiche(" and FR.IdPaziente=Paziente.IdPaziente  and FR.rpco='Si' ", " ,FattoreRischio as FR ");
        }
        if (radioAnemia.Checked)
        {
            calcolaStatistiche(" and FR.IdPaziente=Paziente.IdPaziente  and FR.ANEMIAHB='1' ", " ,FattoreRischio as FR ");
        }
        if (radioIRC.Checked)
        {
            calcolaStatistiche(" and InsuffRenale > 0 and FR.IdPaziente=Paziente.IdPaziente ", " ,FattoreRischio as FR ");
        }
         if (radioDiabete.Checked)
        {
            calcolaStatistiche(" and FR.IdPaziente=Paziente.IdPaziente  and (FR.Diabete1Data!= '' or FR.Diabete2Data!='') ", " ,FattoreRischio as FR ");
        }
         if (radioCongPolm.Checked)
         {
             calcolaCongestionePolm();
         }
         if (radioCongPer.Checked)
         {
             calcolaCongestionePer();
         }
         if (radioCong.Checked)
         {
             calcolaCong();
         }
        pn_risultati.Visible = true;
    }
    private void calcolaCongestionePer()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();


            String strCmd = "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and (CongestionePerifGiugularimag3=1 or CongestionePerifEpatomegalia=1 or ISNULL(CongestionePerifEdemiPeriferici,'') != '' or CongestionePerifAscite=1) " +
              "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO'  and   (CongestionePerifGiugularimag3=1 or CongestionePerifEpatomegalia=1 or ISNULL(CongestionePerifEdemiPeriferici,'') != '' or CongestionePerifAscite=1) and Sesso='M' " +
              "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and   (CongestionePerifGiugularimag3=1 or CongestionePerifEpatomegalia=1 or ISNULL(CongestionePerifEdemiPeriferici,'') != '' or CongestionePerifAscite=1) and Sesso='F' ";



            strCmd += " select count(Paziente.IdPaziente) as NumTot from Paziente";
            //Response.Write(strCmd);
            SqlCommand command = new SqlCommand(strCmd, connection);
            //Response.Write(command.CommandText);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            int tuttiPazienti = int.Parse(ds.Tables[3].Rows[0][0].ToString());
            //int pazientiSel = int.Parse(ds.Tables[0].Rows[0][0].ToString()); ;
            /*lblRisultati.Text += "<fieldset><legend>CONGESTIONE PERIFERICA (N.Pazienti)</legend><table style='text-align:center;'>";
            //if (dataTable.Rows.Count > 0)
            //{
            //Response.Write(int.Parse(ds.Tables[0].Rows[0][0].ToString()) + " / " + tuttiPazienti + "");
            //Response.Write(float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100);
            lblRisultati.Text += "<tr><td style='text-align:left;width:190px;'>Giugulari </td><td><b>" + ds.Tables[0].Rows[0][0] + " ( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) </b></td><td> - M: " + ds.Tables[1].Rows[0][0] + "</td><td> - F: " + ds.Tables[2].Rows[0][0] + "</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;width:190px;'>Moderata </td><td><b>" + ds.Tables[3].Rows[0][0] + " ( " + (float.Parse(ds.Tables[3].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) </b></td><td> - M: " + ds.Tables[4].Rows[0][0] + "</td><td> - F: " + ds.Tables[5].Rows[0][0] + "</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;width:190px;'>Severa </td><td><b>" + ds.Tables[6].Rows[0][0] + " ( " + (float.Parse(ds.Tables[6].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) </b></td><td> - M: " + ds.Tables[7].Rows[0][0] + "</td><td> - F: " + ds.Tables[8].Rows[0][0] + "</td></tr>";
            //}
            lblRisultati.Text += "</table></fieldset>";*/
            lblRisultati.Text += "CONGESTIONE PERIFERICA (N.Pazienti) <b>" + ds.Tables[0].Rows[0][0] + " ( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) - M: " + ds.Tables[1].Rows[0][0] + " - F: " + ds.Tables[2].Rows[0][0];
         
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }
    private void calcolaCong()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();


            String strCmd = "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and ISNULL(CongestionePolmonare,'')!='' and (CongestionePerifGiugularimag3=1 or CongestionePerifEpatomegalia=1 or ISNULL(CongestionePerifEdemiPeriferici,'') != '' or CongestionePerifAscite=1) " +
              "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and ISNULL(CongestionePolmonare,'')!='' and (CongestionePerifGiugularimag3=1 or CongestionePerifEpatomegalia=1 or ISNULL(CongestionePerifEdemiPeriferici,'') != '' or CongestionePerifAscite=1) and Sesso='M' " +
              "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and ISNULL(CongestionePolmonare,'')!='' and (CongestionePerifGiugularimag3=1 or CongestionePerifEpatomegalia=1 or ISNULL(CongestionePerifEdemiPeriferici,'') != '' or CongestionePerifAscite=1) and Sesso='F' ";


            strCmd += " select count(Paziente.IdPaziente) as NumTot from Paziente";
            //Response.Write(strCmd);
            SqlCommand command = new SqlCommand(strCmd, connection);
            //Response.Write(command.CommandText);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            int tuttiPazienti = int.Parse(ds.Tables[3].Rows[0][0].ToString());

            lblRisultati.Text += "CONGESTIONE POLMONARE e PERIFERICA (N.Pazienti) <b>" + ds.Tables[0].Rows[0][0] + " ( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) <br/> M: " + ds.Tables[1].Rows[0][0] + " - F: " + ds.Tables[2].Rows[0][0];

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }
    private void calcolaCongestionePolm()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();


            String strCmd = "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and ISNULL(CongestionePolmonare,'')!='' " +
              "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and ISNULL(CongestionePolmonare,'')!='' and Sesso='M' " +
              "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and ISNULL(CongestionePolmonare,'')!='' and Sesso='F' ";

   
            strCmd += " select count(Paziente.IdPaziente) as NumTot from Paziente";
            //Response.Write(strCmd);
            SqlCommand command = new SqlCommand(strCmd, connection);
            //Response.Write(command.CommandText);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            int tuttiPazienti = int.Parse(ds.Tables[3].Rows[0][0].ToString());
          
            lblRisultati.Text += "CONGESTIONE POLMONARE (N.Pazienti) <b>" + ds.Tables[0].Rows[0][0] + " ( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) - M: " + ds.Tables[1].Rows[0][0] + " - F: " + ds.Tables[2].Rows[0][0];
          
             }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }

    private void calcolaValvolopatie(string val,string from)
    {

        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();


            String strCmd = "select count(Paziente.IdPaziente) as NumTot from Valvolopatia,Paziente,QuadroClinico " + from + " where Valvolopatia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and TipoValvolopatia > 1 " + val +
              "select count(Paziente.IdPaziente) as NumTotM from Valvolopatia,Paziente,QuadroClinico " + from + " where Valvolopatia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO'  and TipoValvolopatia > 1 and Sesso='M' " + val +
              "select count(Paziente.IdPaziente) as NumTotF from Valvolopatia,Paziente,QuadroClinico " + from + "  where Valvolopatia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO'  and TipoValvolopatia> 1 and Sesso='F' " + val;

          
            strCmd += " select count(Paziente.IdPaziente) as NumTot from Paziente";
            //Response.Write(strCmd);
            SqlCommand command = new SqlCommand(strCmd, connection);
            //Response.Write(command.CommandText);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            int tuttiPazienti = int.Parse(ds.Tables[3].Rows[0][0].ToString());
            //int pazientiSel = int.Parse(ds.Tables[0].Rows[0][0].ToString()); ;
           /* lblRisultati.Text += "<fieldset><legend>VALVOLOPATIE (N.Pazienti)</legend><table style='text-align:center;'>";
           
            lblRisultati.Text += "<tr><td style='text-align:left;width:190px;'>Aortica </td><td><b>" + ds.Tables[0].Rows[0][0] + " ( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) </b></td><td> - M: " + ds.Tables[1].Rows[0][0] + "</td><td> - F: " + ds.Tables[2].Rows[0][0] + "</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;width:190px;'>Mitralica </td><td><b>" + ds.Tables[3].Rows[0][0] + " ( " + (float.Parse(ds.Tables[3].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) </b></td><td> - M: " + ds.Tables[4].Rows[0][0] + "</td><td> - F: " + ds.Tables[5].Rows[0][0] + "</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;width:190px;'>Tricuspidale </td><td><b>" + ds.Tables[6].Rows[0][0] + " ( " + (float.Parse(ds.Tables[6].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) </b></td><td> - M: " + ds.Tables[7].Rows[0][0] + "</td><td> - F: " + ds.Tables[8].Rows[0][0] + "</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;width:190px;'>Polmonare </td><td><b>" + ds.Tables[9].Rows[0][0] + " ( " + (float.Parse(ds.Tables[9].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) </b></td><td> - M: " + ds.Tables[10].Rows[0][0] + "</td><td> - F: " + ds.Tables[11].Rows[0][0] + "</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;width:190px;'>Mitro-Aortica </td><td><b>" + ds.Tables[12].Rows[0][0] + " ( " + (float.Parse(ds.Tables[12].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) </b></td><td> - M: " + ds.Tables[13].Rows[0][0] + "</td><td> - F: " + ds.Tables[14].Rows[0][0] + "</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;width:190px;'>Protesi Aortica </td><td><b>" + ds.Tables[15].Rows[0][0] + " ( " + (float.Parse(ds.Tables[15].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) </b></td><td> - M: " + ds.Tables[16].Rows[0][0] + "</td><td> - F: " + ds.Tables[17].Rows[0][0] + "</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;width:190px;'>Protesi Mitralica </td><td><b>" + ds.Tables[18].Rows[0][0] + " ( " + (float.Parse(ds.Tables[18].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) </b></td><td> - M: " + ds.Tables[19].Rows[0][0] + "</td><td> - F: " + ds.Tables[20].Rows[0][0] + "</td></tr>";
            */
            lblRisultati.Text += "<br/><hr/><br/>VALVOLOPATIE (N.Pazienti) <b>" + ds.Tables[0].Rows[0][0] + " ( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) - M: " + ds.Tables[1].Rows[0][0] + " - F: " + ds.Tables[2].Rows[0][0];
            
            //lblRisultati.Text += "</table></fieldset>";
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }

    private void calcolaStatistiche(string val,string from)
    {

        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();


            String strCmd = "select count(Paziente.IdPaziente) as NumTot from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' " + val +
              "select count(Paziente.IdPaziente) as NumTot from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as NumTot from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and Sesso='F' " + val +

               "select count(Paziente.IdPaziente) as PM from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and PM='1' " + val +
                "select count(Paziente.IdPaziente) as PMM from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and PM='1' and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as PMF from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and PM='1' and Sesso='F' " + val +

               "select count(Paziente.IdPaziente) as ICD from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and ICD=1 " + val +
                "select count(Paziente.IdPaziente) as ICD_M from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and ICD=1 and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as ICD_F from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and ICD=1 and Sesso='F' " + val +

               "select count(Paziente.IdPaziente) as RTC from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (PMBiventricolDataImpianto !='' or PMBiventricol!='') " + val +
                "select count(Paziente.IdPaziente) as RTC_M from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (PMBiventricolDataImpianto !='' or PMBiventricol!='') and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as RTC_F from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (PMBiventricolDataImpianto !='' or PMBiventricol!='') and Sesso='F' " + val +

               "select count(Paziente.IdPaziente) as RTCICD from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (PMBiventricolDataImpianto !='' or PMBiventricol!='') and ICD=1  " + val +
                "select count(Paziente.IdPaziente) as RTCICD_M from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (PMBiventricolDataImpianto !='' or PMBiventricol!='') and ICD=1 and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as RTCICD_F from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (PMBiventricolDataImpianto !='' or PMBiventricol!='') and ICD=1 and Sesso='F' " + val +

         "select count(Paziente.IdPaziente) as FAPAR from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and FAParadossDataInizio!='' " + val +
                "select count(Paziente.IdPaziente) as FAPAR_M from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and FAParadossDataInizio!='' and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as FAPAR_F from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and FAParadossDataInizio!='' and Sesso='F' " + val +

                "select count(Paziente.IdPaziente) as FAPER from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and FAPermanenteDataInizio!='' " + val +
                "select count(Paziente.IdPaziente) as FAPER_M from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and FAPermanenteDataInizio!='' and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as FAPER_F from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and FAPermanenteDataInizio!='' and Sesso='F' " + val +

                 "select count(Paziente.IdPaziente) as IRC from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO'  and InsuffRenale > 0 " + val +
                "select count(Paziente.IdPaziente) as IRC_M from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and InsuffRenale > 0 and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as IRC_F from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and  InsuffRenale > 0 and Sesso='F' " + val;
            if (!radioBPCO.Checked)
            {

                strCmd += "select count(Paziente.IdPaziente) as NumCasiBPCO from FattoreRischio,Paziente,QuadroClinico " + from + " where FattoreRischio.IdPaziente=Paziente.IdPaziente  and FattoreRischio.rpco='Si' and QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' " + val +
                "select count(Paziente.IdPaziente) as NumCasiBPCO from FattoreRischio,Paziente,QuadroClinico " + from + " where FattoreRischio.IdPaziente=Paziente.IdPaziente  and FattoreRischio.rpco='Si' and QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as NumCasiBPCO from FattoreRischio,Paziente,QuadroClinico " + from + " where FattoreRischio.IdPaziente=Paziente.IdPaziente  and FattoreRischio.rpco='Si' and QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and Sesso='F' " + val;
            }
            else
            {
                strCmd += "select count(Paziente.IdPaziente) as NumTot from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' " + val +
              "select count(Paziente.IdPaziente) as NumTot from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as NumTot from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and Sesso='F' " + val;
            }
            if (!radioAnemia.Checked)
            {

                strCmd += "select count(Paziente.IdPaziente) as NumCasiAnemia from QuadroClinico,Paziente,FattoreRischio " + from + " where FattoreRischio.idPaziente=Paziente.idPaziente and FattoreRischio.ANEMIAHB='1' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' " + val +
               "select count(Paziente.IdPaziente) as NumCasiAnemia from QuadroClinico,Paziente,FattoreRischio " + from + " where FattoreRischio.idPaziente=Paziente.idPaziente and FattoreRischio.ANEMIAHB='1' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and Sesso='M' " + val +
              "select count(Paziente.IdPaziente) as NumCasiAnemia from QuadroClinico,Paziente,FattoreRischio " + from + " where FattoreRischio.idPaziente=Paziente.idPaziente and FattoreRischio.ANEMIAHB='1' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and Sesso='F' " + val;
            }
            else
            {
                strCmd += "select count(Paziente.IdPaziente) as NumTot from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' " + val +
              "select count(Paziente.IdPaziente) as NumTot from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and Sesso='M' " + val +
               "select count(Paziente.IdPaziente) as NumTot from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and Sesso='F' " + val;
            }

            strCmd += "select count(Paziente.IdPaziente) as NYHA from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO'  and (ClasseDispneaSforzo = 'III classe' or ClasseDispneaSforzo = 'IV classe') " + val +
               "select count(Paziente.IdPaziente) as NYHA_M from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (ClasseDispneaSforzo = 'III classe' or ClasseDispneaSforzo = 'IV classe') and Sesso='M' " + val +
              "select count(Paziente.IdPaziente) as NYHA_F from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (ClasseDispneaSforzo = 'III classe' or ClasseDispneaSforzo = 'IV classe') and Sesso='F' " + val +

               "select count(Paziente.IdPaziente) as FE from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO'   and (CONVERT(int,Eco2DFE) < 35)  " + val +
               "select count(Paziente.IdPaziente) as FE_M from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO'  and (CONVERT(int,Eco2DFE) < 35)  and Sesso='M' " + val +
              "select count(Paziente.IdPaziente) as FE_F from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (CONVERT(int,Eco2DFE) < 35)  and Sesso='F' " + val +

               "select count(Paziente.IdPaziente) as FEInt from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO'  and (CONVERT(int,Eco2DFE) >= 35 and CONVERT(int,Eco2DFE) < 50) " + val +
               "select count(Paziente.IdPaziente) as FEInt_M from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (CONVERT(int,Eco2DFE) >= 35 and CONVERT(int,Eco2DFE) < 50) and Sesso='M' " + val +
              "select count(Paziente.IdPaziente) as FEInt_F from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO' and (CONVERT(int,Eco2DFE) >= 35 and CONVERT(int,Eco2DFE) < 50) and Sesso='F' " + val +

             "select count(Paziente.IdPaziente) as NumTot from Paziente;";

            strCmd += "select count(Paziente.IdPaziente) as DIAB from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO'   and (FR.Diabete1Data!= '' or FR.Diabete2Data!='')  " + val +
                "select count(Paziente.IdPaziente) as DIAB_M from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO'   and (FR.Diabete1Data!= '' or FR.Diabete2Data!='')  and Sesso='M' " + val +
            "select count(Paziente.IdPaziente) as DIAB_F from QuadroClinico,Paziente " + from + " where QuadroClinico.IdPaziente=Paziente.IdPaziente and gtipo='RECLUTAMENTO'   and (FR.Diabete1Data!= '' or FR.Diabete2Data!='')  and Sesso='F' " + val;
               
            SqlCommand command = new SqlCommand(strCmd   , connection);
            //Response.Write(command.CommandText);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            int tuttiPazienti = int.Parse(ds.Tables[39].Rows[0][0].ToString());
            int pazientiSel = int.Parse(ds.Tables[0].Rows[0][0].ToString()); ;
            lblRisultati.Text = "<table style='text-align:center;' cellpadding=3 cellspacing=3><tr><td></td><td>Num.Pazienti</td><td>Maschi</td><td>Femmine</td></tr>";
            //if (dataTable.Rows.Count > 0)
            //{
            //Response.Write(int.Parse(ds.Tables[0].Rows[0][0].ToString()) + " / " + tuttiPazienti + "");
            //Response.Write(float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100);
            lblRisultati.Text += "<tr><td style='text-align:left;'>Totali </td><td><b>" + ds.Tables[0].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % sul totale )</td><td><b>" + ds.Tables[1].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[2].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>PM</td><td><b>" + ds.Tables[3].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[3].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[4].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[5].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>ICD</td><td><b>" + ds.Tables[6].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[6].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[7].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[8].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>RTC</td><td><b>" + ds.Tables[9].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[9].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[10].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[11].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>RTC+ICD</td><td><b>" + ds.Tables[12].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[12].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[13].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[14].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>FA Paross.</td><td><b>" + ds.Tables[15].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[15].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[16].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[17].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>FA Perm.</td><td><b>" + ds.Tables[18].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[18].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[19].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[20].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>IRC</td><td><b>" + ds.Tables[21].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[21].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[22].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[23].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>BPCO</td><td><b>" + ds.Tables[24].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[24].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[25].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[26].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>ANEMIA</td><td><b>" + ds.Tables[27].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[27].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[28].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[29].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>Classe NYHA (da III a IV)</td><td><b>" + ds.Tables[30].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[30].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[31].Rows[0][0] + "</b></td><td><b>" + ds.Tables[32].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'>F.E. < 35%</td><td><b>" + ds.Tables[33].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[33].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[34].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[35].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'> 35% <= F.E. < 50%</td><td><b>" + ds.Tables[36].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[36].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[37].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[38].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            lblRisultati.Text += "<tr><td style='text-align:left;'> Diabete </td><td><b>" + ds.Tables[40].Rows[0][0] + "</b><br/>( " + (float.Parse(ds.Tables[40].Rows[0][0].ToString()) / pazientiSel * 100).ToString("N2") + " % )</td><td><b>" + ds.Tables[41].Rows[0][0] + "</b><br/>&nbsp;</td><td><b>" + ds.Tables[42].Rows[0][0] + "</b><br/>&nbsp;</td></tr>";
            
           
            //}
            lblRisultati.Text += "</table>";
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;

public partial class trends : System.Web.UI.Page
{
    private string conn_str;



    protected int IdPaziente;

    private int IdUltimoPaziente;


    protected void Page_Load(object sender, EventArgs e)
    {
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }

        if (Request.Params["idpaziente"] != null)
        {
            IdPaziente = int.Parse(Request.Params["idpaziente"].ToString());
            SqlConnection connection = new SqlConnection(this.conn_str);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("cerca_paziente_id", connection);
                command.Parameters.AddWithValue("@IdPaziente", Request.Params["idpaziente"].ToString());
                command.CommandType = CommandType.StoredProcedure;
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    lblPaziente.Text = dataTable.Rows[0]["Cognome"] + " " + dataTable.Rows[0]["Nome"] + " - " + dataTable.Rows[0]["CodiceFiscale"];
                    //loadDataPolmonare();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {

                connection.Close();
            }
        }
    }

    private void loadDataPolmonare()
    {
        if (Request.Params["idpaziente"] != null)
        {
            SqlConnection connection = new SqlConnection(this.conn_str);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select gFrequenzaCard,DataQuadroClinico from QuadroClinico where idPaziente=" + Request.Params["idPaziente"] + " and DataQuadroClinico != ''" , connection);
                //command.Parameters.AddWithValue("@IdPaziente", Request.Params["idpaziente"].ToString());
                command.CommandType = CommandType.Text;
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                DataTable dataTable = new DataTable();
                adapter.Fill(dataTable);
                if (dataTable.Rows.Count > 0)
                {
                    lblPaziente.Text = dataTable.Rows[0]["Cognome"] + " " + dataTable.Rows[0]["Nome"] + " - " + dataTable.Rows[0]["CodiceFiscale"];
                    loadDataPolmonare();
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {

                connection.Close();
            }
        }
    }
}

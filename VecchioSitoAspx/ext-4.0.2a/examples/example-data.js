/*

This file is part of Ext JS 4

Copyright (c) 2011 Sencha Inc

Contact:  http://www.sencha.com/contact

GNU General Public License Usage
This file may be used under the terms of the GNU General Public License version 3.0 as published by the Free Software Foundation and appearing in the file LICENSE included in the packaging of this file.  Please review the following information to ensure the GNU General Public License version 3.0 requirements will be met: http://www.gnu.org/copyleft/gpl.html.

If you are unsure which license is appropriate for your use, please contact the sales department at http://www.sencha.com/contact.

*/
Ext.require(['Ext.data.*']);

Ext.onReady(function() {
    //alert("FACCIO LO STORE");
    window.generateDataGISCSesso = function() {
           var data = [],
            p = (Math.random() * 11) + 1,
            i;

   // floor = (!floor && floor !== 0) ? 20 : floor;
   
    //for (i = 0; i < (n || 12); i++) {
        data.push({
            name: "Deads",
            data1: '<%=numPazientiD%>'
        });
        data.push({
            name: "Pazienti in studio",
            data1: '<%=numPazientiV%>',
            
        });
        /*data.push({
            name: "Non Specificato",
            //data1: '<%=(numPazienti - (numPazientiM + numPazientiF))%>',
            data1:0
        });*/
    //}
    return data;
    }
    
    window.generateDataGISCDiabete = function() {
           var data = [],
            p = (Math.random() * 11) + 1,
            i;

   // floor = (!floor && floor !== 0) ? 20 : floor;
   
    //for (i = 0; i < (n || 12); i++) {
        data.push({
            name: "Maschi",
            data1: '<%=numPazientiMDiabete%>'
        });
        data.push({
            name: "Femmine",
            data1: '<%=numPazientiFDiabete%>',
            
        });
       /* data.push({
            name: "Non Specificato",
            data1: '<%=(numPazientiDiabete - (numPazientiMDiabete + numPazientiFDiabete))%>',
            data1:0
        });*/
    //}
    return data;
    }
    
    window.generateDataGISCClinica = function()
    {
       var data = [],
            p = (Math.random() * 11) + 1,
            i;

        
         data.push({
            name: "Classe NYHA (da III a IV)",
          data1: <%= lic.FindByText("NYHA").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("NYHA").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("NYHA").Value.Split("-".ToCharArray())[2]%>
        });
      
       /* data.push({
            name: "Ipertrofia",
            data1: <%= lic.FindByText("Ipertrofia").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("Ipertrofia").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("Ipertrofia").Value.Split("-".ToCharArray())[2]%>
        });
        data.push({
            name: "Ipertensione Arteriosa",
            data1: <%= lic.FindByText("IpertensioneArteriosa").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("IpertensioneArteriosa").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("IpertensioneArteriosa").Value.Split("-".ToCharArray())[2]%>
        });*/
        data.push({
            name: "FE < 35%",
           data1: <%= lic.FindByText("FE").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("FE").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("FE").Value.Split("-".ToCharArray())[2]%>
        });
          data.push({
            name: "35% <= FE < 50%",
           data1: <%= lic.FindByText("FEIntervallo").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("FEIntervallo").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("FEIntervallo").Value.Split("-".ToCharArray())[2]%>
        });
        data.push({
            name: "Ipert.Polm.",
          data1: <%= lic.FindByText("PressionePolmonare").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("PressionePolmonare").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("PressionePolmonare").Value.Split("-".ToCharArray())[2]%>
        });
         data.push({
            name: "Disf.Diastolica",
            data1: <%= lic.FindByText("DisfunzioneDiastolica").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("DisfunzioneDiastolica").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("DisfunzioneDiastolica").Value.Split("-".ToCharArray())[2]%>
        });
         /*data.push({
            name: "Diabetici",
            data1: <%= lic.FindByText("Diabete").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("Diabete").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("Diabete").Value.Split("-".ToCharArray())[2]%>
        });
        data.push({
            name: "Insufficienza Renale Cronica ",
            data1: <%= lic.FindByText("InsufficienzaRenale").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("InsufficienzaRenale").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("InsufficienzaRenale").Value.Split("-".ToCharArray())[2]%>
        });*/
        
        
        
        
    //}
    return data;
    }
    
     window.generateDataGISCEziologia = function()
    {
       var data = [],
            p = (Math.random() * 11) + 1,
            i;

        
        data.push({
            name: "Ischemia",
           data1: <%= lic.FindByText("Ischemia").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("Ischemia").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("Ischemia").Value.Split("-".ToCharArray())[2]%>
        });
        
        data.push({
            name: "Dilatativa",
            data1: <%= lic.FindByText("CardiomiopatiaDilatativa").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("CardiomiopatiaDilatativa").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("CardiomiopatiaDilatativa").Value.Split("-".ToCharArray())[2]%>
        });
        data.push({
            name: "Valvolopatie",
            data1: <%= lic.FindByText("Valvolopatie").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("Valvolopatie").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("Valvolopatie").Value.Split("-".ToCharArray())[2]%>
        });
        data.push({
            name: "Ipertensiva",
           data1: <%= lic.FindByText("Cardiomiopatia").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("Cardiomiopatia").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("Cardiomiopatia").Value.Split("-".ToCharArray())[2]%>
        });
        data.push({
            name: "Ipertensiva + Valvolare",
           data1: <%= lic.FindByText("IpertensivaValvolare").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("IpertensivaValvolare").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("IpertensivaValvolare").Value.Split("-".ToCharArray())[2]%>
        });
         data.push({
            name: "Ischemia + Valvolare",
           data1: <%= lic.FindByText("IschemiaValvolare").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("IschemiaValvolare").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("IschemiaValvolare").Value.Split("-".ToCharArray())[2]%>
        });
        
        /*
      
         data.push({
            name: "Disf.Diastolica",
            data1: <%= lic.FindByText("DisfunzioneDiastolica").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("DisfunzioneDiastolica").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("DisfunzioneDiastolica").Value.Split("-".ToCharArray())[2]%>
        });
         data.push({
            name: "Diabetici",
            data1: <%= lic.FindByText("Diabete").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("Diabete").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("Diabete").Value.Split("-".ToCharArray())[2]%>
        });
        data.push({
            name: "Insufficienza Renale Cronica ",
            data1: <%= lic.FindByText("InsufficienzaRenale").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("InsufficienzaRenale").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("InsufficienzaRenale").Value.Split("-".ToCharArray())[2]%>
        });*/
        
        
        
        
    //}
    return data;
    }
    
    window.generateDataGISCValori = function()
    {
       var data = [],
            p = (Math.random() * 11) + 1,
            i;

        /*
        data.push({
            name: "Ischemia",
           data1: <%= lic.FindByText("Ischemia").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("Ischemia").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("Ischemia").Value.Split("-".ToCharArray())[2]%>
        });
        
        data.push({
            name: "Ipertrofia",
            data1: <%= lic.FindByText("Ipertrofia").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("Ipertrofia").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("Ipertrofia").Value.Split("-".ToCharArray())[2]%>
        });*/
        data.push({
            name: "Ipertensione Arteriosa",
            data1: <%= lic.FindByText("IpertensioneArteriosa").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("IpertensioneArteriosa").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("IpertensioneArteriosa").Value.Split("-".ToCharArray())[2]%>
        });
        data.push({
            name: "BPCO",
           data1: <%= lic.FindByText("BPCO").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("BPCO").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("BPCO").Value.Split("-".ToCharArray())[2]%>
        });
        data.push({
            name: "Anemia",
          data1: <%= lic.FindByText("Anemia").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("Anemia").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("Anemia").Value.Split("-".ToCharArray())[2]%>
        });
         data.push({
            name: "IPERDISLIPIDEMIA",
            data1: <%= lic.FindByText("IPERDISLIPIDEMIA").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("IPERDISLIPIDEMIA").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("IPERDISLIPIDEMIA").Value.Split("-".ToCharArray())[2]%>
        });
         data.push({
            name: "Diabete",
            data1: <%= lic.FindByText("Diabete").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("Diabete").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("Diabete").Value.Split("-".ToCharArray())[2]%>
        });
        data.push({
            name: "Insufficienza Renale Cronica ",
            data1: <%= lic.FindByText("InsufficienzaRenale").Value.Split("-".ToCharArray())[0]%>,
            data2: <%= lic.FindByText("InsufficienzaRenale").Value.Split("-".ToCharArray())[1]%>,
            data3: <%= lic.FindByText("InsufficienzaRenale").Value.Split("-".ToCharArray())[2]%>
        });
        
        
        
        
    //}
    return data;
    }
    
    window.generateData = function(n, floor) {
        var data = [],
            p = (Math.random() * 11) + 1,
            i;

        floor = (!floor && floor !== 0) ? 20 : floor;

        for (i = 0; i < (n || 12); i++) {
            data.push({
                name: Ext.Date.monthNames[i % 12],
                data1: Math.floor(Math.max((Math.random() * 100), floor)),
                data2: Math.floor(Math.max((Math.random() * 100), floor)),
                data3: Math.floor(Math.max((Math.random() * 100), floor)),
                data4: Math.floor(Math.max((Math.random() * 100), floor)),
                data5: Math.floor(Math.max((Math.random() * 100), floor)),
                data6: Math.floor(Math.max((Math.random() * 100), floor)),
                data7: Math.floor(Math.max((Math.random() * 100), floor)),
                data8: Math.floor(Math.max((Math.random() * 100), floor)),
                data9: Math.floor(Math.max((Math.random() * 100), floor))
            });
        }
        return data;
    };

    window.generateDataNegative = function(n, floor) {
        var data = [],
            p = (Math.random() * 11) + 1,
            i;

        floor = (!floor && floor !== 0) ? 20 : floor;

        for (i = 0; i < (n || 12); i++) {
            data.push({
                name: Ext.Date.monthNames[i % 12],
                data1: Math.floor(((Math.random() - 0.5) * 100), floor),
                data2: Math.floor(((Math.random() - 0.5) * 100), floor),
                data3: Math.floor(((Math.random() - 0.5) * 100), floor),
                data4: Math.floor(((Math.random() - 0.5) * 100), floor),
                data5: Math.floor(((Math.random() - 0.5) * 100), floor),
                data6: Math.floor(((Math.random() - 0.5) * 100), floor),
                data7: Math.floor(((Math.random() - 0.5) * 100), floor),
                data8: Math.floor(((Math.random() - 0.5) * 100), floor),
                data9: Math.floor(((Math.random() - 0.5) * 100), floor)
            });
        }
        return data;
    };

    window.store1 = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: generateData()
    });
    window.storeNegatives = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: generateDataNegative()
    });
    window.store3 = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: generateData()
    });
    window.store4 = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: generateData()
    });
    window.store5 = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: generateData()
    });


});


﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class statistichegeneralinew : System.Web.UI.UserControl
{
    private string conn_str;
    protected int numPazienti;
    protected int numPazientiF;
    protected int numPazientiM;

    protected void GeneraStatisticheAnemia()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_Anemia", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumCasiAnemia"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiAnemia"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;
                //double num4 = (Convert.ToDouble(num) * Convert.ToDouble(100)) / Convert.ToDouble(this.numPazienti);
                string text = this.lb_Anemia.Text;
                this.lb_Anemia.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiAnemiaM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiAnemiaM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
               
                double num5 = (Convert.ToDouble(num2) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str2 = this.lb_Anemia.Text;
                this.lb_Anemia.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiAnemiaF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiAnemiaF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
                //double num6 = (num3 * 100) / num;
                double num6 = (Convert.ToDouble(num3) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str3 = this.lb_Anemia.Text;
                this.lb_Anemia.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_Anemia.Text = this.lb_Anemia.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiAnemiaEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiAnemiaEtaMediaM"].ToString() != "")
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Maschi: <b> " + reader["NumCasiAnemiaEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiAnemiaEtaMediaF"].ToString() != "")
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Femmine: <b>" + reader["NumCasiAnemiaEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiAnemiaEtaMin"].ToString() != "")
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiAnemiaEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiAnemiaEtaMinM"].ToString() != "")
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Maschi: <b>" + reader["NumCasiAnemiaEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiAnemiaEtaMinF"].ToString() != "")
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Femmine: <b>" + reader["NumCasiAnemiaEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiAnemiaEtaMax"].ToString() != "")
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiAnemiaEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiAnemiaEtaMaxM"].ToString() != "")
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Maschi: <b>" + reader["NumCasiAnemiaEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiAnemiaEtaMaxF"].ToString() != "")
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Femmine: <b>" + reader["NumCasiAnemiaEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Anemia.Text = this.lb_Anemia.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void GeneraStatisticheBPCO()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_BPCO", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumCasiBPCO"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiBPCO"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;
                string text = this.lb_BPCO.Text;
                this.lb_BPCO.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString() + "%)<br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiBPCOM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiBPCOM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
                //double num5 = (num2 * 100) / num;
                double num5 = (Convert.ToDouble(num2) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str2 = this.lb_BPCO.Text;
                this.lb_BPCO.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiBPCOF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiBPCOF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
                //double num6 = (num3 * 100) / num;
                double num6 = (Convert.ToDouble(num3) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str3 = this.lb_BPCO.Text;
                this.lb_BPCO.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_BPCO.Text = this.lb_BPCO.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiBPCOEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiBPCOEtaMediaM"].ToString() != "")
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Maschi: <b> " + reader["NumCasiBPCOEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiBPCOEtaMediaF"].ToString() != "")
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Femmine: <b>" + reader["NumCasiBPCOEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiBPCOEtaMin"].ToString() != "")
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiBPCOEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiBPCOEtaMinM"].ToString() != "")
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Maschi: <b>" + reader["NumCasiBPCOEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiBPCOEtaMinF"].ToString() != "")
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Femmine: <b>" + reader["NumCasiBPCOEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiBPCOEtaMax"].ToString() != "")
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiBPCOEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiBPCOEtaMaxM"].ToString() != "")
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Maschi: <b>" + reader["NumCasiBPCOEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiBPCOEtaMaxF"].ToString() != "")
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Femmine: <b>" + reader["NumCasiBPCOEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_BPCO.Text = this.lb_BPCO.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void GeneraStatisticheColesterolo()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_Colesterolo", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumCasiColesterolo"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiColesterolo"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;
                string text = this.lb_Colesterolo.Text;
                this.lb_Colesterolo.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiColesteroloM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiColesteroloM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
             
                double num5 = (Convert.ToDouble(num2) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str2 = this.lb_Colesterolo.Text;
                this.lb_Colesterolo.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiColesteroloF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiColesteroloF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
                //double num6 = (num3 * 100) / num;
                double num6 = (Convert.ToDouble(num3) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str3 = this.lb_Colesterolo.Text;
                this.lb_Colesterolo.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiColesteroloEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiColesteroloEtaMediaM"].ToString() != "")
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Maschi: <b> " + reader["NumCasiColesteroloEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiColesteroloEtaMediaF"].ToString() != "")
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Femmine: <b>" + reader["NumCasiColesteroloEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiColesteroloEtaMin"].ToString() != "")
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiColesteroloEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiColesteroloEtaMinM"].ToString() != "")
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Maschi: <b>" + reader["NumCasiColesteroloEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiColesteroloEtaMinF"].ToString() != "")
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Femmine: <b>" + reader["NumCasiColesteroloEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiColesteroloEtaMax"].ToString() != "")
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiColesteroloEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiColesteroloEtaMaxM"].ToString() != "")
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Maschi: <b>" + reader["NumCasiColesteroloEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiColesteroloEtaMaxF"].ToString() != "")
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Femmine: <b>" + reader["NumCasiColesteroloEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Colesterolo.Text = this.lb_Colesterolo.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void GeneraStatisticheDiabete()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_diabete", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumCasiDiabete"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiDiabete"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;
                string text = this.lb_diabete.Text;
                this.lb_diabete.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString() + "%)<br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiDiabeteM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
               
                double num5 = (Convert.ToDouble(num2) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str2 = this.lb_diabete.Text;
                this.lb_diabete.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiDiabeteF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
             
                double num6 = (Convert.ToDouble(num3) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str3 = this.lb_diabete.Text;
                this.lb_diabete.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_diabete.Text = this.lb_diabete.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiDiabeteEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteEtaMediaM"].ToString() != "")
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Maschi: <b> " + reader["NumCasiDiabeteEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteEtaMediaF"].ToString() != "")
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Femmine: <b>" + reader["NumCasiDiabeteEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteEtaMin"].ToString() != "")
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiDiabeteEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteEtaMinM"].ToString() != "")
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Maschi: <b>" + reader["NumCasiDiabeteEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteEtaMinF"].ToString() != "")
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Femmine: <b>" + reader["NumCasiDiabeteEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteEtaMax"].ToString() != "")
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiDiabeteEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteEtaMaxM"].ToString() != "")
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Maschi: <b>" + reader["NumCasiDiabeteEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteEtaMaxF"].ToString() != "")
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Femmine: <b>" + reader["NumCasiDiabeteEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_diabete.Text = this.lb_diabete.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void GeneraStatisticheGenerali()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_generali", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            int num = Convert.ToInt32(reader["NumTotale"]);
            this.lb_pazienti.Text = "Pazienti: <b>" + num.ToString() + "</b><br />";
            Session["numPazienti"] = num.ToString();
            reader.NextResult();
            reader.Read();
            int num2 = Convert.ToInt32(reader["NumMaschi"]);
            if (num != 0)
            {
                //double num19 = (num2 * 100) / num;
                double num19 = (Convert.ToDouble(num2) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string text = this.lb_pazienti.Text;
                this.lb_pazienti.Text = text + "Maschi: <b>" + num2.ToString() + "</b> (" + num19.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_pazienti.Text = this.lb_pazienti.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            int num3 = Convert.ToInt32(reader["NumFemmine"]);
            if (num != 0)
            {
                //double num20 = (num3 * 100) / num;
                double num20 = (Convert.ToDouble(num3) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str2 = this.lb_pazienti.Text;
                this.lb_pazienti.Text = str2 + "Femmine: <b>" + num3.ToString() + "</b> (" + num20.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_pazienti.Text = this.lb_pazienti.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (num != 0)
            {
                this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 media pazienti: <b>" + ((double)(Convert.ToInt32(reader["SumEta"]) / num)).ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (num2 != 0)
            {
                this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 media M: <b>" + ((double)(Convert.ToInt32(reader["SumEtaM"]) / num2)).ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (num3 != 0)
            {
                this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 media F: <b>" + ((double)(Convert.ToInt32(reader["SumEtaF"]) / num3)).ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
           // this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 minima: <b>" + reader["EtaMin"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 massima: <b>" + reader["EtaMax"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
           // this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 minima M: <b>" + reader["EtaMinM"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 massima M: <b>" + reader["EtaMaxM"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            //this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 minima F: <b>" + reader["EtaMinF"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 massima F: <b>" + reader["EtaMaxF"].ToString() + "</b><br />";
            this.lb_pazienti.Text = this.lb_pazienti.Text + "<br />";
            if (num != 0)
            {
                object obj2;
                reader.NextResult();
                reader.Read();
                int num4 = Convert.ToInt32(reader["Fumo"]);
                double num24 = (Convert.ToDouble(num4) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                this.lb_fumatori.Text = string.Concat(new object[] { "Fumatori: <b>", num4.ToString(), "</b><br />(", Math.Round(num24, 2), "% dei pazienti)<br />" });
                reader.NextResult();
                reader.Read();
                int num5 = Convert.ToInt32(reader["FumoM"]);
                if (num4 != 0)
                {
                    double num25 = (Convert.ToDouble(num5) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    obj2 = this.lb_fumatori.Text;
                    this.lb_fumatori.Text = string.Concat(new object[] { obj2, "Maschi: <b>", num5.ToString(), "</b> (", Math.Round(num25, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori.Text = this.lb_fumatori.Text + "Maschi: <b>" + num5.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num6 = Convert.ToInt32(reader["FumoF"]);
                if (num4 != 0)
                {
                    double num26 = (Convert.ToDouble(num6) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    object obj3 = this.lb_fumatori.Text;
                    this.lb_fumatori.Text = string.Concat(new object[] { obj3, "Femmine: <b>", num6.ToString(), "</b> (", Math.Round(num26, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori.Text = this.lb_fumatori.Text + "Femmine: <b>" + num6.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num7 = Convert.ToInt32(reader["Fumo+"]);
                this.lb_fumatori1.Text = "Fumatori +: <b>" + num7.ToString() + "</b><br />";
                if (num4 != 0)
                {
                    double num27 = (Convert.ToDouble(num7) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    object obj4 = this.lb_fumatori1.Text;
                    this.lb_fumatori1.Text = string.Concat(new object[] { obj4, "(", Math.Round(num27, 2), "% dei fumatori)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num8 = Convert.ToInt32(reader["Fumo+M"]);
                if (num7 != 0)
                {
                    double num28 = (Convert.ToDouble(num8) * Convert.ToDouble(100)) / Convert.ToDouble(num7);
                    object obj5 = this.lb_fumatori1.Text;
                    this.lb_fumatori1.Text = string.Concat(new object[] { obj5, "Maschi: <b>", num8.ToString(), "</b> (", Math.Round(num28, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori1.Text = this.lb_fumatori1.Text + "Maschi: <b>" + num8.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num9 = Convert.ToInt32(reader["Fumo+F"]);
                if (num7 != 0)
                {
                    double num29 = (Convert.ToDouble(num9) * Convert.ToDouble(100)) / Convert.ToDouble(num7);
                    object obj6 = this.lb_fumatori1.Text;
                    this.lb_fumatori1.Text = string.Concat(new object[] { obj6, "Femmine: <b>", num9.ToString(), "</b> (", Math.Round(num29, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori1.Text = this.lb_fumatori1.Text + "Femmine: <b>" + num9.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num10 = Convert.ToInt32(reader["Fumo++"]);
                this.lb_fumatori2.Text = "Fumatori ++: <b>" + num10.ToString() + "</b><br />";
                if (num4 != 0)
                {
                    double num30 = (Convert.ToDouble(num10) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    object obj7 = this.lb_fumatori2.Text;
                    this.lb_fumatori2.Text = string.Concat(new object[] { obj7, "(", Math.Round(num30, 2), "% dei fumatori)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num11 = Convert.ToInt32(reader["Fumo++M"]);
                if (num10 != 0)
                {
                    double num31 = (Convert.ToDouble(num11) * Convert.ToDouble(100)) / Convert.ToDouble(num10);
                    object obj8 = this.lb_fumatori2.Text;
                    this.lb_fumatori2.Text = string.Concat(new object[] { obj8, "Maschi: <b>", num11.ToString(), "</b> (", Math.Round(num31, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori2.Text = this.lb_fumatori2.Text + "Maschi: <b>" + num11.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num12 = Convert.ToInt32(reader["Fumo++F"]);
                if (num10 != 0)
                {
                    double num32 = (Convert.ToDouble(num12) * Convert.ToDouble(100)) / Convert.ToDouble(num10);
                    obj2 = this.lb_fumatori2.Text;
                    this.lb_fumatori2.Text = string.Concat(new object[] { obj2, "Femmine: <b>", num12.ToString(), "</b> (", Math.Round(num32, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori2.Text = this.lb_fumatori2.Text + "Femmine: <b>" + num12.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num13 = Convert.ToInt32(reader["Fumo+++"]);
                this.lb_fumatori3.Text = "Fumatori +++: <b>" + num13.ToString() + "</b><br />";
                if (num4 != 0)
                {
                    double num33 = (Convert.ToDouble(num13) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    obj2 = this.lb_fumatori3.Text;
                    this.lb_fumatori3.Text = string.Concat(new object[] { obj2, "(", Math.Round(num33, 2), "% dei fumatori)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num14 = Convert.ToInt32(reader["Fumo+++M"]);
                if (num13 != 0)
                {
                    double num34 = (Convert.ToDouble(num14) * Convert.ToDouble(100)) / Convert.ToDouble(num13);
                    obj2 = this.lb_fumatori3.Text;
                    this.lb_fumatori3.Text = string.Concat(new object[] { obj2, "Maschi: <b>", num14.ToString(), "</b> (", Math.Round(num34, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori3.Text = this.lb_fumatori3.Text + "Maschi: <b>" + num14.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num15 = Convert.ToInt32(reader["Fumo+++F"]);
                if (num13 != 0)
                {
                    double num35 = (Convert.ToDouble(num15) * Convert.ToDouble(100)) / Convert.ToDouble(num13);
                    obj2 = this.lb_fumatori3.Text;
                    this.lb_fumatori3.Text = string.Concat(new object[] { obj2, "Femmine: <b>", num15.ToString(), "</b> (", Math.Round(num35, 2), "%)<br />" });
                }
                else
                {
                    this.lb_fumatori3.Text = this.lb_fumatori3.Text + "Femmine: <b>" + num15.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num16 = Convert.ToInt32(reader["NumTotaleOss"]);
                this.lb_osservazioni.Text = "Osservazioni: <b>" + num16.ToString() + "</b><br />";
                double num36 = Convert.ToDouble(num16) / Convert.ToDouble(num);
                obj2 = this.lb_osservazioni.Text;
                this.lb_osservazioni.Text = string.Concat(new object[] { obj2, "N. medio oss/paziente: <b>", Math.Round(num36, 2), "</b><br />" });
                reader.NextResult();
                reader.Read();
                int num17 = Convert.ToInt32(reader["NumOssM"]);
                if (num16 != 0)
                {
                    double num37 = (Convert.ToDouble(num17) * Convert.ToDouble(100)) / Convert.ToDouble(num16);
                    obj2 = this.lb_osservazioni.Text;
                    this.lb_osservazioni.Text = string.Concat(new object[] { obj2, "Maschi: <b>", num17.ToString(), "</b> (", Math.Round(num37, 2), "%)<br />" });
                }
                else
                {
                    this.lb_osservazioni.Text = this.lb_osservazioni.Text + "Maschi: <b>" + num17.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num18 = Convert.ToInt32(reader["NumOssF"]);
                if (num16 != 0)
                {
                    double num38 = (Convert.ToDouble(num18) * Convert.ToDouble(100)) / Convert.ToDouble(num16);
                    obj2 = this.lb_osservazioni.Text;
                    this.lb_osservazioni.Text = string.Concat(new object[] { obj2, "Femmine: <b>", num18.ToString(), "</b> (", Math.Round(num38, 2), "%)<br />" });
                }
                else
                {
                    this.lb_osservazioni.Text = this.lb_osservazioni.Text + "Femmine: <b>" + num18.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
               
                reader.NextResult();
                reader.Read();

                lb_exfumatori.Text += "Ex-Fumatori:<b>" + reader["ExFumo"] + "</b></br> ( " + 100 * int.Parse(reader["ExFumo"].ToString()) / num + "% dei pazienti totali)<br/>";
                reader.NextResult();
                reader.Read();
                lb_exfumatori.Text += "Maschi:<b>" + reader["ExFumoM"] + "</b><br/>";
                reader.NextResult();
                reader.Read();
                lb_exfumatori.Text += "Femmine:<b>" + reader["ExFumoF"] + "</b>";
                reader.Read();
               /* lb_exfumatori.Text += "Deaths: <b>" + reader["Morti"] + "</b>";
                reader.Read();
                lb_exfumatori.Text += "Deaths M: <b>" + reader["MortiM"] + "</b>";
                reader.Read();
                lb_exfumatori.Text += "Deaths F: <b>" + reader["MortiF"] + "</b>";
                */
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            base.Response.Write("ERRORE " + exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }
    protected void GeneraStatisticheNOIMA()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_NOIMA", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
           // Response.Write("PAZIENTI:" + reader["NumCasiIMA"]);
            if (reader["NumCasiIMA"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiIMA"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;
                string text = this.lb_NOIMA.Text;
                this.lb_NOIMA.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString() + "%)<br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiIMAM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
               
                double num5 = (Convert.ToDouble(num2) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str2 = this.lb_NOIMA.Text;
                this.lb_NOIMA.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_IMA.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiIMAF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
              
                double num6 = (Convert.ToDouble(num3) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str3 = this.lb_NOIMA.Text;
                this.lb_NOIMA.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_IMA.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_NOIMA.Text = this.lb_NOIMA.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiIMAEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMediaM"].ToString() != "")
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Maschi: <b> " + reader["NumCasiIMAEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMediaF"].ToString() != "")
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Femmine: <b>" + reader["NumCasiIMAEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMin"].ToString() != "")
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiIMAEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMinM"].ToString() != "")
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Maschi: <b>" + reader["NumCasiIMAEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMinF"].ToString() != "")
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Femmine: <b>" + reader["NumCasiIMAEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMax"].ToString() != "")
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiIMAEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMaxM"].ToString() != "")
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Maschi: <b>" + reader["NumCasiIMAEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMaxF"].ToString() != "")
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Femmine: <b>" + reader["NumCasiIMAEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_NOIMA.Text = this.lb_NOIMA.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
        GeneraStatisticheMorti();
         GeneraStatisticheRicoveri();
         GeneraStatistichePrecedenti();
    }

    private void GeneraStatistichePrecedenti()
    {

        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Select COUNT(*) from PrecedentiScompenso where DataRicovero!='' or Diagnosi != ''"+
                "Select COUNT(*) from OspedalizzazioneScomp where DataAccesso !='' or Diagnosi!=''" +
                "Select  Count(Distinct idPaziente) from PrecedentiScompenso where DataRicovero!='' or Diagnosi != ''"+
                "Select Count(Distinct idPaziente) from OspedalizzazioneScomp where DataAccesso !='' or Diagnosi!=''"
                , connection);

           
            //command.Parameters.AddWithValue("@IdPaziente", Request.Params["idpaziente"].ToString());
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataSet dsM = new DataSet();
            adapter.Fill(dsM);
            if (dsM.Tables.Count > 0)
            {
                lb_osservazioni.Text += "<br/><br/>Prec.Acc.P.S. per Scomp.: <b>" + dsM.Tables[0].Rows[0][0] + "</b>";
                lb_osservazioni.Text += "<br/>Pazienti con Prec.Acc. P.S. per Scomp.: <b>" + dsM.Tables[2].Rows[0][0] + "</b><br/>";
                lb_osservazioni.Text += "<br/>Prec.Osp. per Scomp.: <b>" + dsM.Tables[1].Rows[0][0] + "</b>";
                lb_osservazioni.Text += "<br/>Pazienti con Prec.Osp. per Scomp.: <b>" + dsM.Tables[3].Rows[0][0] + "</b>";
            

            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }

    }
    private void GeneraStatisticheRicoveri()
    {
        
            SqlConnection connection = new SqlConnection(this.conn_str);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Select count(*) as NumRicoveri from RicoveroNonCardiologico;" +
                "Select count(*) as NumRicoveri from QuadroClinico where gTipo='RICOVERO' and (RicoveroProgrammato IS NULL or RicoveroProgrammato='False'); " +
                "Select count(*) as NumOss from QuadroClinico,Utente where Utente.idUtente = QuadroClinico.idUtente and Tipo='Medico di medicina generale' and ISNULL(gTipo,'')!= 'RECLUTAMENTO';" +
                //"Select count(*) as NumOss from QuadroClinico,Utente where Utente.idUtente = QuadroClinico.idUtente and Tipo='Cardiologo ambulatoriale' and ISNULL(gTipo,'')!='RECLUTAMENTO'", connection);
                "Select count(*) as NumOss from QuadroClinico,Utente where Utente.idUtente = QuadroClinico.idUtente and Tipo='Cardiologo ambulatoriale'" +
                "Select count(*) as NumRicoveriProg from QuadroClinico where gTipo='RICOVERO' and (RicoveroProgrammato='True'); "
                , connection);
                
                //command.Parameters.AddWithValue("@IdPaziente", Request.Params["idpaziente"].ToString());
                command.CommandType = CommandType.Text;
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                DataSet dsM = new DataSet();
                adapter.Fill(dsM);
                if (dsM.Tables.Count > 0)
                {
                      lb_osservazioni.Text += "Controlli MMG: <b>" + dsM.Tables[2].Rows[0][0] + "</b>";
                    lb_osservazioni.Text += "<br/>Controlli Specialist: <b>" + dsM.Tables[3].Rows[0][0] + "</b> <br/>";
                    lb_osservazioni.Text += "Ricoveri Cardiologici: <b>" + dsM.Tables[1].Rows[0][0] + "</b><br/>";
                    lb_osservazioni.Text += "Ricoveri Cardiologici Programmati: <b>" + dsM.Tables[4].Rows[0][0] + "</b>";
                    
                    lb_osservazioni.Text += "<br/>Ricoveri NON Cardiologici: <b>" + dsM.Tables[0].Rows[0][0] + "</b>";
                    
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {

                connection.Close();
            }
        
    }

    
        private void GeneraStatisticheMorti()
    {
        
            SqlConnection connection = new SqlConnection(this.conn_str);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("select count(Paziente.IdPaziente) as NumDeath from Paziente where gDead != '';" +
                "select count(Paziente.IdPaziente) as NumDeath from Paziente where  gDead != '' and Sesso='M';" +
                "select count(Paziente.IdPaziente) as NumDeath from Paziente where  gDead != '' and Sesso='F'", connection);
                //command.Parameters.AddWithValue("@IdPaziente", Request.Params["idpaziente"].ToString());
                command.CommandType = CommandType.Text;
                SqlDataAdapter adapter = new SqlDataAdapter();
                adapter.SelectCommand = command;
                DataSet dsM = new DataSet();
                adapter.Fill(dsM);
                if (dsM.Tables.Count > 0)
                {
                    lb_pazienti.Text += "Deads: <b>" + dsM.Tables[0].Rows[0][0] + "</b>";
                    lb_pazienti.Text += "<br/>Deads M: <b>" + dsM.Tables[1].Rows[0][0] + "</b>";
                    lb_pazienti.Text += "<br/>Deads F: <b>" + dsM.Tables[2].Rows[0][0] + "</b><br/><br/>";
                }
            }
            catch (Exception ex)
            {
                Response.Write(ex.Message);
            }
            finally
            {

                connection.Close();
            }
        
    }

    protected void GeneraStatisticheIMA()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_IMA", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumCasiIMA"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiIMA"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;
                string text = this.lb_IMA.Text;
                this.lb_IMA.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString() + "%)<br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiIMAM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
            
                double num5 = (Convert.ToDouble(num2) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str2 = this.lb_IMA.Text;
                this.lb_IMA.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiIMAF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
                //double num6 = (num3 * 100) / num;
                double num6 = (Convert.ToDouble(num3) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str3 = this.lb_IMA.Text;
                this.lb_IMA.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_IMA.Text = this.lb_IMA.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiIMAEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMediaM"].ToString() != "")
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Maschi: <b> " + reader["NumCasiIMAEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMediaF"].ToString() != "")
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Femmine: <b>" + reader["NumCasiIMAEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMin"].ToString() != "")
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiIMAEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMinM"].ToString() != "")
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Maschi: <b>" + reader["NumCasiIMAEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMinF"].ToString() != "")
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Femmine: <b>" + reader["NumCasiIMAEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMax"].ToString() != "")
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiIMAEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMaxM"].ToString() != "")
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Maschi: <b>" + reader["NumCasiIMAEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIMAEtaMaxF"].ToString() != "")
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Femmine: <b>" + reader["NumCasiIMAEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_IMA.Text = this.lb_IMA.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void GeneraStatisticheInsuffRenale()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_InsuffRenale", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumCasiInsuffRenale"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiInsuffRenale"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;
                string text = this.lb_InsuffRenale.Text;
                this.lb_InsuffRenale.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString() + "%)<br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiInsuffRenaleM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiInsuffRenaleM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
             
                double num5 = (Convert.ToDouble(num2) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str2 = this.lb_InsuffRenale.Text;
                this.lb_InsuffRenale.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiInsuffRenaleF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiInsuffRenaleF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
              
                double num6 = (Convert.ToDouble(num3) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str3 = this.lb_InsuffRenale.Text;
                this.lb_InsuffRenale.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiInsuffRenaleEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiInsuffRenaleEtaMediaM"].ToString() != "")
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Maschi: <b> " + reader["NumCasiInsuffRenaleEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiInsuffRenaleEtaMediaF"].ToString() != "")
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Femmine: <b>" + reader["NumCasiInsuffRenaleEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiInsuffRenaleEtaMin"].ToString() != "")
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiInsuffRenaleEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiInsuffRenaleEtaMinM"].ToString() != "")
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Maschi: <b>" + reader["NumCasiInsuffRenaleEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiInsuffRenaleEtaMinF"].ToString() != "")
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Femmine: <b>" + reader["NumCasiInsuffRenaleEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiInsuffRenaleEtaMax"].ToString() != "")
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiInsuffRenaleEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiInsuffRenaleEtaMaxM"].ToString() != "")
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Maschi: <b>" + reader["NumCasiInsuffRenaleEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiInsuffRenaleEtaMaxF"].ToString() != "")
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Femmine: <b>" + reader["NumCasiInsuffRenaleEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_InsuffRenale.Text = this.lb_InsuffRenale.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void GeneraStatisticheIpertensione()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_ipertensione_art", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumTotale"].ToString() != "")
            {
                this.numPazienti = Convert.ToInt32(reader["NumTotale"]);
            }
            else
            {
                this.numPazienti = 0;
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumMaschi"].ToString() != "")
            {
                this.numPazientiM = Convert.ToInt32(reader["NumMaschi"]);
            }
            else
            {
                this.numPazientiM = 0;
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumFemmine"].ToString() != "")
            {
                this.numPazientiF = Convert.ToInt32(reader["NumFemmine"]);
            }
            else
            {
                this.numPazientiF = 0;
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpert"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiIpert"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                //double num4 = (num * 100) / this.numPazienti;
                double num4 = (Convert.ToDouble(num) * Convert.ToDouble(100)) / Convert.ToDouble(this.numPazienti);
                string text = this.lb_ipertensione_art.Text;
                this.lb_ipertensione_art.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiIpertM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
                
                double num5 = (Convert.ToDouble(num2) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str2 = this.lb_ipertensione_art.Text;
                this.lb_ipertensione_art.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiIpertF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
               
                double num6 = (Convert.ToDouble(num3) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                string str3 = this.lb_ipertensione_art.Text;
                this.lb_ipertensione_art.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString("N2") + "%)<br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiIpertEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertEtaMediaM"].ToString() != "")
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Maschi: <b> " + reader["NumCasiIpertEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertEtaMediaF"].ToString() != "")
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Femmine: <b>" + reader["NumCasiIpertEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertEtaMin"].ToString() != "")
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiIpertEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertEtaMinM"].ToString() != "")
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Maschi: <b>" + reader["NumCasiIpertEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertEtaMinF"].ToString() != "")
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Femmine: <b>" + reader["NumCasiIpertEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertEtaMax"].ToString() != "")
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiIpertEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertEtaMaxM"].ToString() != "")
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Maschi: <b>" + reader["NumCasiIpertEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertEtaMaxF"].ToString() != "")
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Femmine: <b>" + reader["NumCasiIpertEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_ipertensione_art.Text = this.lb_ipertensione_art.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void GeneraStatisticheIpertiroidismo()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_Ipertiroidismo", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumCasiIpertiroidismo"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiIpertiroidismo"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;
                string text = this.lb_Ipertiroidismo.Text;
                this.lb_Ipertiroidismo.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertiroidismoM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiIpertiroidismoM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
                double num5 = (num2 * 100) / num;
                string str2 = this.lb_Ipertiroidismo.Text;
                this.lb_Ipertiroidismo.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertiroidismoF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiIpertiroidismoF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
                double num6 = (num3 * 100) / num;
                string str3 = this.lb_Ipertiroidismo.Text;
                this.lb_Ipertiroidismo.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiIpertiroidismoEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertiroidismoEtaMediaM"].ToString() != "")
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Maschi: <b> " + reader["NumCasiIpertiroidismoEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertiroidismoEtaMediaF"].ToString() != "")
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Femmine: <b>" + reader["NumCasiIpertiroidismoEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertiroidismoEtaMin"].ToString() != "")
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiIpertiroidismoEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertiroidismoEtaMinM"].ToString() != "")
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Maschi: <b>" + reader["NumCasiIpertiroidismoEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertiroidismoEtaMinF"].ToString() != "")
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Femmine: <b>" + reader["NumCasiIpertiroidismoEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertiroidismoEtaMax"].ToString() != "")
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiIpertiroidismoEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertiroidismoEtaMaxM"].ToString() != "")
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Maschi: <b>" + reader["NumCasiIpertiroidismoEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiIpertiroidismoEtaMaxF"].ToString() != "")
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Femmine: <b>" + reader["NumCasiIpertiroidismoEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Ipertiroidismo.Text = this.lb_Ipertiroidismo.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void GeneraStatisticheObesita()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_Obesita", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumCasiObesita"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiObesita"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;
                string text = this.lb_Obesita.Text;
                this.lb_Obesita.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiObesitaM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiObesitaM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
                double num5 = (num2 * 100) / num;
                string str2 = this.lb_Obesita.Text;
                this.lb_Obesita.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiObesitaF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiObesitaF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
                double num6 = (num3 * 100) / num;
                string str3 = this.lb_Obesita.Text;
                this.lb_Obesita.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_Obesita.Text = this.lb_Obesita.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiObesitaEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiObesitaEtaMediaM"].ToString() != "")
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Maschi: <b> " + reader["NumCasiObesitaEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiObesitaEtaMediaF"].ToString() != "")
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Femmine: <b>" + reader["NumCasiObesitaEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiObesitaEtaMin"].ToString() != "")
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiObesitaEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiObesitaEtaMinM"].ToString() != "")
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Maschi: <b>" + reader["NumCasiObesitaEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiObesitaEtaMinF"].ToString() != "")
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Femmine: <b>" + reader["NumCasiObesitaEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiObesitaEtaMax"].ToString() != "")
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiObesitaEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiObesitaEtaMaxM"].ToString() != "")
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Maschi: <b>" + reader["NumCasiObesitaEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiObesitaEtaMaxF"].ToString() != "")
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Femmine: <b>" + reader["NumCasiObesitaEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Obesita.Text = this.lb_Obesita.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void GeneraStatisticheSindrome()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_Sindrome", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumCasiSindrome"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiSindrome"]);
            }
            else
            {
                num = 0;
            }
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;
                string text = this.lb_Sindrome.Text;
                this.lb_Sindrome.Text = text + "<b>Pazienti affetti: " + num.ToString() + "</b> (" + num4.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "<b>Pazienti affetti: " + num.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiSindromeM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiSindromeM"]);
            }
            else
            {
                num2 = 0;
            }
            if (num != 0)
            {
                double num5 = (num2 * 100) / num;
                string str2 = this.lb_Sindrome.Text;
                this.lb_Sindrome.Text = str2 + "Maschi: <b>" + num2.ToString() + "</b> (" + num5.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiSindromeF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiSindromeF"]);
            }
            else
            {
                num3 = 0;
            }
            if (num != 0)
            {
                double num6 = (num3 * 100) / num;
                string str3 = this.lb_Sindrome.Text;
                this.lb_Sindrome.Text = str3 + "Femmine: <b>" + num3.ToString() + "</b> (" + num6.ToString() + "%)<br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            this.lb_Sindrome.Text = this.lb_Sindrome.Text + "<b>Et\x00e0 media pazienti: " + reader["NumCasiSindromeEtaMedia"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiSindromeEtaMediaM"].ToString() != "")
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Maschi: <b> " + reader["NumCasiSindromeEtaMediaM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiSindromeEtaMediaF"].ToString() != "")
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Femmine: <b>" + reader["NumCasiSindromeEtaMediaF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiSindromeEtaMin"].ToString() != "")
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "<b>Et\x00e0 minima pazienti: " + reader["NumCasiSindromeEtaMin"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "<b>Et\x00e0 minima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiSindromeEtaMinM"].ToString() != "")
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Maschi: <b>" + reader["NumCasiSindromeEtaMinM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiSindromeEtaMinF"].ToString() != "")
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Femmine: <b>" + reader["NumCasiSindromeEtaMinF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Femmine: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiSindromeEtaMax"].ToString() != "")
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "<b>Et\x00e0 massima pazienti: " + reader["NumCasiSindromeEtaMax"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "<b>Et\x00e0 massima pazienti:  - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiSindromeEtaMaxM"].ToString() != "")
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Maschi: <b>" + reader["NumCasiSindromeEtaMaxM"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Maschi: <b> - </b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (reader["NumCasiSindromeEtaMaxF"].ToString() != "")
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Femmine: <b>" + reader["NumCasiSindromeEtaMaxF"].ToString() + "</b><br />";
            }
            else
            {
                this.lb_Sindrome.Text = this.lb_Sindrome.Text + "Femmine: <b> - </b><br />";
            }
        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        if (!base.IsPostBack)
        {
            this.GeneraStatisticheGenerali();
            this.GeneraStatisticheIpertensione();
            this.GeneraStatisticheDiabete();
            this.GeneraStatisticheIMA();
            this.GeneraStatisticheNOIMA();
            this.GeneraStatisticheBPCO();
            this.GeneraStatisticheInsuffRenale();
            this.GeneraStatisticheAnemia();
            this.GeneraStatisticheObesita();
            this.GeneraStatisticheIpertiroidismo();
            this.GeneraStatisticheSindrome();
            this.GeneraStatisticheColesterolo();
            this.GeneraStatisticheRigurgito();
            this.GeneraStatisticheStenosi();
        }
    }

    private void GeneraStatisticheRigurgito()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();


            String strCmd = "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and ISNULL(EcoDopplerRigurMitralico,'')='+++' " +
              "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and ISNULL(EcoDopplerRigurMitralico,'')='+++' and Sesso='M' " +
              "select count(Paziente.IdPaziente) as NumTot from Paziente,QuadroClinico where  QuadroClinico.IdPaziente=Paziente.idPaziente  and gTipo='RECLUTAMENTO' and ISNULL(EcoDopplerRigurMitralico,'')='+++' and Sesso='F' ";


            strCmd += " select count(Paziente.IdPaziente) as NumTot from Paziente";
            //Response.Write(strCmd);
            SqlCommand command = new SqlCommand(strCmd, connection);
            //Response.Write(command.CommandText);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            int tuttiPazienti = int.Parse(ds.Tables[3].Rows[0][0].ToString());
            lb_rigurgito.Text = "<b>Pazienti Affetti: " + ds.Tables[0].Rows[0][0] + "</b> ( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % )";
            lb_rigurgito.Text += "<br/>Maschi: <b>" + ds.Tables[1].Rows[0][0] + "</b>";
            lb_rigurgito.Text += "<br/>Femmine: <b>" + ds.Tables[2].Rows[0][0] + "</b>";
            //lblRisultati.Text += "CONGESTIONE POLMONARE (N.Pazienti) <b>" + ds.Tables[0].Rows[0][0] + " ( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) - M: " + ds.Tables[1].Rows[0][0] + " - F: " + ds.Tables[2].Rows[0][0];

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }

    private void GeneraStatisticheStenosi()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();


            String strCmd = "select count(Paziente.IdPaziente) as NumTot from Valvolopatia,Paziente,QuadroClinico where Valvolopatia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and (TipoValvolopatia = 2 or TipoValvolopatia=7)" +
              "select count(Paziente.IdPaziente) as NumTot from Valvolopatia,Paziente,QuadroClinico where Valvolopatia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and (TipoValvolopatia = 2 or TipoValvolopatia=7) and Sesso='M' " +
              "select count(Paziente.IdPaziente) as NumTot from Valvolopatia,Paziente,QuadroClinico where Valvolopatia.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and (TipoValvolopatia = 2 or TipoValvolopatia=7) and Sesso='F' ";


            strCmd += " select count(Paziente.IdPaziente) as NumTot from Paziente";
            //Response.Write(strCmd);
            SqlCommand command = new SqlCommand(strCmd, connection);
            //Response.Write(command.CommandText);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataSet ds = new DataSet();
            adapter.Fill(ds);
            int tuttiPazienti = int.Parse(ds.Tables[3].Rows[0][0].ToString());

            lb_stenosi.Text = "<b>Pazienti Affetti: " + ds.Tables[0].Rows[0][0] + "</b> ( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % )";
            lb_stenosi.Text += "<br/>Maschi: <b>" + ds.Tables[1].Rows[0][0] + "</b>";
            lb_stenosi.Text += "<br/>Femmine: <b>" + ds.Tables[2].Rows[0][0] + "</b>";
            //lblRisultati.Text += "CONGESTIONE POLMONARE (N.Pazienti) <b>" + ds.Tables[0].Rows[0][0] + " ( " + (float.Parse(ds.Tables[0].Rows[0][0].ToString()) / tuttiPazienti * 100).ToString("N2") + " % ) - M: " + ds.Tables[1].Rows[0][0] + " - F: " + ds.Tables[2].Rows[0][0];

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {

            connection.Close();
        }
    }

}

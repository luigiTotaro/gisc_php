<%@ page language="C#" autoeventwireup="true" inherits="GestioneMedici_cercamedico, App_Web_cercamedico.aspx.acf7dae1" %>
<!--#include file="../include/controllo.inc"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="../stile.css" type=text/css rel="stylesheet" />
<title>Gestione Medici</title>

<style type="text/css">
<!--
body {
	background-image: url(../files/bg_blueGrad.gif);
	background-color: #324671;
}
.style1 {color: #FFFFFF}
.Stile1 {color: #336DD0}
-->
</style>
</head>

<body>
   <form id="form1" runat="server">
<table width="812" height="100%" border="0" align="center" cellpadding="0" cellspacing="0" vspace="top">
  
  <tr>
    <td width="10" valign="top" background="../files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="../files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center"><div align="left">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
                <!--#include file="../include/medici.inc"-->
              <div class="modernbricksmenuline">
                <ul>
                    <li style="margin-left: 1px"><a href="medico.aspx?IdMedicoCurante=-1">Nuovo<br />Medico</a></li>
                    <li><a href="cercamedico.aspx">Cerca<br />Medico</a></li>
                </ul>
              </div>
          </td>
          </tr>

      </table>
    </div>
        <br />
        <br />
        <span style="font-size:18px;color:#324671;font-weight:bold">Cerca Medico</span>
        <br />
        <br />
        <table border="0" cellpadding="2" cellspacing="2">
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lb_mess" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td align="right">Nome medico:
                </td>
                <td align="left">
                    <asp:TextBox ID="tb_cognome" runat="server" Columns="40"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btn_cerca" runat="server" Text="Cerca" OnClick="btn_cerca_Click" /></td>
            </tr>
        </table>
        <br />
        <br />
        <asp:GridView ID="gv_medici" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gv_medici_PageIndexChanging" PageSize="15">
            <FooterStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <EditRowStyle BackColor="#2461BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#006699" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="IdMedicoCurante" Visible="False" />
                <asp:TemplateField HeaderText="Nome">
                    <ItemTemplate>
                        <a href='<%# "medico.aspx?IdMedicoCurante="+DataBinder.Eval(Container.DataItem, "IdMedicoCurante")%>'><%# DataBinder.Eval(Container.DataItem, "Medico") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Indirizzo">
                    <ItemTemplate>
                        <a href='<%# "medico.aspx?IdMedicoCurante="+DataBinder.Eval(Container.DataItem, "IdMedicoCurante")%>'><%#DataBinder.Eval(Container.DataItem, "Indirizzo") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Recapiti">
                    <ItemTemplate>
                        <a href='<%# "medico.aspx?IdMedicoCurante="+DataBinder.Eval(Container.DataItem, "IdMedicoCurante")%>'><%#DataBinder.Eval(Container.DataItem, "Telefoni") %></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a onclick='return confirm("Sei sicuro di voler eliminare il medico?");' href='<%# "cercamedico.aspx?del="+DataBinder.Eval(Container.DataItem, "IdMedicoCurante")%>'>elimina</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

    </td>
    <td width="10" valign="top" background="../files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="../files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="../files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="2" style="height: 20px"><!--#include file="../include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="../files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" background="../files/bg_content_bottom.gif">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>

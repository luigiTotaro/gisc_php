<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HomePageTipo.aspx.cs" Inherits="HomePageTipo" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="stile.css" type=text/css rel="stylesheet" />
<script language="JavaScript" src="./js/ts_picker.js"></script>
<title>Titolo</title>

<style type="text/css">
<!--
body {
	background-image: url(./files/bg_blueGrad.gif);
	background-color: #324671;
}
.Stile1 {color: #336DD0}
-->
</style>
</head>

<body>
   <form id="form1" runat="server">
<table width="812" height="auto" border="0" align="center" cellpadding="0" cellspacing="0" vspace="top">
  
  <tr>
    <td width="10" valign="top" background="./files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center"><div align="left">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
		 <div class="modernbricksmenu">
  <ul>
    <li class="current" style="margin-left: 1px">
      <a href="#">Pannello</a>
    </li>
    <li  style="margin-left: 1px">
      <a href="cercapaziente.aspx">Pazienti</a>
    </li>
     <% if (Session["tipo"].ToString() == "admin")
        {%>
     <li class="current">
      <a href="GestioneMedici/cercamedico.aspx">Medici</a>
    </li>
    <%
          }
    %>
    <asp:Label ID="lb_gest_utenti" runat="server" Text=""></asp:Label>
    <li>
      <a href="logout.aspx">Esci</a>
    </li>
  </ul>
</div>
          </td>
          </tr>

      </table>
    </div>
        <br />
        <br />
        <span style="font-size:18px;color:#324671;font-weight:bold"><asp:Label Visible="false" ID="Label1" runat="server" Text=""></asp:Label>
        <br />
        <asp:Label Font-Size="14px" runat="server" ID="lblAlert"></asp:Label>
        <br />
        <div style="width:750px; height:auto; border:1px #99CCFF solid;">
        <div style="width:100%; text-align:left; background-color:#99CCFF; height:30px;color:#003366; font-size:12px;">Benvenuto <b><%=Session["Utente"] %></b> <br /><span style="font-size:10px;"> Pazienti Reclutati: <b><%=numTot %></b> - Pazienti da te reclutati:<b> <%=numTuo %> </b></span></div>
        
        
                <asp:Label runat="server" ID="lblPazienteSel" Font-Bold="false" Font-Size="12"></asp:Label><asp:Label Font-Size="10" runat="server" ID="lblReclutatoIl"></asp:Label>
                <br />
                <asp:Label Font-Size="12px" runat="server" ID="lblDead" Visible="false" Font-Bold="true" ForeColor="Red"></asp:Label><br />
                 <asp:Label runat="server" ID="codPazSel" Visible="false"></asp:Label> 
                <asp:LinkButton runat="server" Font-Size="10" ID="bttSearchPaziente" Text="SELEZIONA PAZIENTE" 
                onclick="bttSearchPaziente_Click"></asp:LinkButton>
          <div runat="server" id="divPaziente" visible="false" style="font-size:10px;">
         <table border="0" cellpadding="2" cellspacing="2">
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lb_mess" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td align="right">Cognome paziente:
                </td>
                <td align="left">
                    <asp:TextBox ID="tb_cognome" runat="server" Columns="30"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right">Codice fiscale paziente:
                </td>
                <td align="left">
                    <asp:TextBox ID="tb_codicefis" runat="server" MaxLength="16" Columns="30"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">Primo accesso dopo il (gg/mm/aaaa):
                </td>
                <td align="left">
                    <asp:TextBox ID="tb_data_min" runat="server" MaxLength="10" Columns="10"></asp:TextBox>
                    <a href="javascript:show_calendar('document.form1.tb_data_min', document.form1.tb_data_min.value, 'calendar1');"><img src="./files/cal.gif" width="16" height="16" border="0" alt="Seleziona la data"></a>
                    <div id="calendar1" style="display: none;"></div>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">Primo accesso entro il (gg/mm/aaaa):
                </td>
                <td align="left">
                    <asp:TextBox ID="tb_data_max" runat="server" MaxLength="10" Columns="10"></asp:TextBox>
                    <a href="javascript:show_calendar('document.form1.tb_data_max', document.form1.tb_data_max.value, 'calendar2');"><img src="./files/cal.gif" width="16" height="16" border="0" alt="Seleziona la data"></a>
                    <div id="calendar2" style="display: none;"></div>
                </td>
            </tr>
            <tr>
                <td align="right"><asp:Label runat="server" ID="lb_MedicoCurante" Text="Medico curante:"></asp:Label> 
                </td>
                <td align="left">
                    <asp:DropDownList ID="ddl_medico" runat="server">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btn_cerca" runat="server" Text="Cerca" OnClick="btn_cerca_Click" /></td>
            </tr>
        </table>
        <br />
        <br />
            <asp:DataGrid Width="97%" BorderColor="#cccccc" BorderStyle="Solid" BorderWidth="1px" ID="gv_pazienti" runat="server" AlternatingItemStyle-BackColor="#eeeeee" Font-Size="12px" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="gv_pazienti_PageIndexChanging" PageSize="15">
            <FooterStyle BackColor="#006699" ForeColor="White" />
               
            <Columns>
                <asp:BoundColumn DataField="IdPaziente" Visible="False" />
                 <asp:BoundColumn DataField="Cognome" Visible="False" />
                  <asp:BoundColumn DataField="Nome" Visible="False" />
                 <asp:BoundColumn DataField="CodiceFiscale" Visible="False" />
                
                <asp:TemplateColumn HeaderText="Cognome">
                    <ItemTemplate>
                       <%#DataBinder.Eval(Container.DataItem, "Cognome") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Nome">
                    <ItemTemplate>
                       <%#DataBinder.Eval(Container.DataItem, "Nome") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Data Reclutamento">
                    <ItemTemplate>
                        <%# Utility.DecodificaData(DataBinder.Eval(Container.DataItem, "DataRegistrazione").ToString())%>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Codice Fiscale">
                    <ItemTemplate>
                        <%# DataBinder.Eval(Container.DataItem, "CodiceFiscale") %>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderStyle-BorderColor="Black">
                    <ItemTemplate>
                   
                    &nbsp;&nbsp;&nbsp;<asp:ImageButton runat="server" ImageAlign="Middle" ImageUrl="img/sel.png" style="" CommandName="seleziona" /> <br />
                        <asp:LinkButton  ForeColor="#333333" Font-Underline="false" runat="server" ID="bttSel" Text="seleziona" CommandName="seleziona"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn >
                    <asp:TemplateColumn HeaderStyle-BorderColor="Black">
                    <ItemTemplate>
                    &nbsp;&nbsp;&nbsp;<asp:ImageButton ID="ImageButton1" ImageAlign="Middle" runat="server" ImageUrl="img/profile edit.png"  CommandName="modifica" /> <br />
                     
                        <asp:LinkButton  ForeColor="#333333" Font-Underline="false" runat="server" ID="bttMod" Text="modifica" CommandName="modifica"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn >
                     <asp:TemplateColumn HeaderStyle-BorderColor="Black">
                    <ItemTemplate>
                    
                     
                        <a style="text-decoration:none;color:#333333;" onclick='return confirm("Sei sicuro di voler eliminare il paziente?");' href='<%# "cercapaziente.aspx?del="+DataBinder.Eval(Container.DataItem, "IdPaziente")%>'>
                        &nbsp;&nbsp;<img src="img/profile remove.png" style="border:0px;" /><br />
                        elimina</a>
                    </ItemTemplate>
                </asp:TemplateColumn>
            </Columns>
            <HeaderStyle Font-Bold="true" />
          <ItemStyle Font-Bold="false" />
          <AlternatingItemStyle Font-Bold="false" />
        </asp:DataGrid>
        
              <br />
        
        </div>
         
         <br /><br /><table runat="server" id="tablePanel" style="width:100%; text-align:center; font-size:14px;" cellpadding="10" cellspacing="10">
        <tr>
        <td style="vertical-align:top;">
        <div style="border:1px #99CCFF solid; background-color:#eeeeee; width:130px; height:170px;">
        <br /><br /><br /><br />
        <asp:LinkButton runat="server" ID="bttReclutamento" ForeColor="#003366" 
                Text="RECLUTAMENTO" onclick="bttReclutamento_Click"></asp:LinkButton>
                </div><br />
                <div style="border:1px #99CCFF solid; background-color:#eeeeee; width:130px; height:85px;">
        <br /><br />
          <asp:LinkButton runat="server" ID="bttNewRicovero" ForeColor="#003366" 
                Text="NUOVO RICOVERO"  Enabled="false" onclick="bttNewRicovero_Click"></asp:LinkButton>
                </div>
                   <div style="border:1px #99CCFF solid; background-color:#eeeeee; width:130px; height:85px;">
        <br />
          <asp:LinkButton runat="server" ID="bttNewRicoveroNonCardio" ForeColor="#003366" 
                Text="NUOVO RICOVERO NON CARDIOLOGICO"  Enabled="false" onclick="bttNewRicovero_Click"></asp:LinkButton>
                </div>
        </td>
        <td style="vertical-align:top;">
          <div style="border:1px #99CCFF solid; background-color:#eeeeee; width:130px; height:170px;">
        <br /><br /><br /><br />
        <asp:LinkButton runat="server" ID="bttNewControl" ForeColor="#003366" 
                Text="NUOVO CONTROLLO" Enabled="false" onclick="bttNewControl_Click"></asp:LinkButton>
                </div><br />
                 <div style="border:1px #99CCFF solid; background-color:#eeeeee; width:130px; height:170px;">
        <br /><br /><br /><br />
          <asp:LinkButton runat="server" ID="bttDead" ForeColor="#003366" Text="DEAD"  
                Enabled="false" onclick="bttDead_Click"></asp:LinkButton>
                </div>
        </td>
        <td rowspan="2" style="vertical-align:top;">
          <div style="width:2px; height:320px; background-color:#99CCFF; vertical-align:middle; "></div>
         </td>
          <td rowspan="2" style="width:500px; vertical-align:middle;">
          <asp:Panel Visible=false runat="server" id="pnlQuadri" Width="390px" Height="260px" BorderStyle="Solid" BorderWidth="1px" BorderColor="#cccccc" ScrollBars="Auto">
     
     </asp:Panel>
      <asp:Label runat="server" ID="lblQuadri"  Font-Bold="false" Text="<i>Seleziona un paziente per visualizzarne i controlli</i>"></asp:Label>
          <br />
      
         </td>
        </tr>
        <tr>
        <td style="vertical-align:top;" >
        </td>
        <td style="vertical-align:top;" >
       
        </td>
         </tr>
       
        </table>
        </div>
        <br />
       
        </span>
        <asp:Panel Visible="true" ID="pnlLegenda" runat="server">
       <fieldset style="width:300px; border:1px #cccccc solid;">
    <legend><b>Legenda</b></legend>
    <table cellpadding="5" cellspacing="5">
     <tr><td><div style="width:20px; height:20px; background-color:#eeeeee; border:1px #cccccc solid;"></div></td>
    <td> Reclutamento
    </td></tr>
    <tr><td><div style="width:20px; height:20px; background-color:#9999FF; border:1px #cccccc solid;"></div></td>
    <td> Controllo Specialist/Ospedaliero
    </td></tr>
    <tr><td><div style="width:20px; height:20px; background-color:#6699CC; border:1px #cccccc solid;"></div></td>
    <td> Controllo MMG
    </td></tr>
    </table>
    
  </fieldset></asp:Panel>
        <br />
        
        <br />
       
        <br />
 
    </td>
    <td width="10" valign="top" background="./files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="./files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="2" style="height: 20px"><!--#include file="include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="./files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" background="./files/bg_content_bottom.gif">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>

﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class HomePageTipo : System.Web.UI.Page
{
    private string conn_str;
    private string urlControllo = "";
    protected int numTot;
    protected int numTuo;
    protected string linkReclutamento;
    protected void btn_cerca_Click(object sender, EventArgs e)
    {
        
        if ((!this.tb_data_min.Text.Trim().Equals("") && !Utility.IsDate(this.tb_data_min.Text.Trim())) || (!this.tb_data_max.Text.Trim().Equals("") && !Utility.IsDate(this.tb_data_max.Text.Trim())))
        {
            this.lb_mess.Text = "Occorre inserire le date nel formato corretto.";
        }
        else
        {
            this.CercaPazienti();
        }
    }

    private void CercaPazienti()
    {
       
        DataTable dataTable = new DataTable();
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            string cmdText = "select * from Paziente where CodiceFiscale!=''";
            if (!this.tb_cognome.Text.Trim().Equals(""))
            {
                cmdText = cmdText + " and Cognome like @Cognome";
            }
            if (!this.tb_codicefis.Text.Trim().Equals(""))
            {
                cmdText = cmdText + " and CodiceFiscale=@CodiceFiscale";
            }
            if (!this.tb_data_min.Text.Trim().Equals(""))
            {
                cmdText = cmdText + " and DataRegistrazione>=@DataRicoveroMin";
            }
            if (!this.tb_data_max.Text.Trim().Equals(""))
            {
                cmdText = cmdText + " and DataRegistrazione<=@DataRicoveroMax";
            }

           /* if ((Session["tipo"].ToString() == "Medico di medicina generale"))
            {
                cmdText = cmdText + " and IdMedicoCurante=@IdMedicoCurante";
            }
            else
            {*/
                if (!this.ddl_medico.SelectedIndex.Equals(0))
                {
                    cmdText = cmdText + " and IdMedicoCurante=@IdMedicoCurante";
                }

                if (Session["Tipo"].ToString() == "Medico di medicina generale")
                {
                    cmdText = cmdText + " and IdMedicoCurante=" + this.Session["IdUtente"];
                    ddl_medico.Visible = false;
                    lb_MedicoCurante.Visible = false;
                }
                else
                {
                    /*if (Session["Tipo"].ToString() != "admin")
                    {
                        cmdText = cmdText + " and IdUtente=" + this.Session["IdUtente"];
                    }*/
                }
            //}
            //cmdText = cmdText + " and IdUtente=@idutente";
                
            SqlCommand command = new SqlCommand(cmdText, connection);
            command.CommandType = CommandType.Text;
            command.Parameters.AddWithValue("@Cognome", "%" + this.tb_cognome.Text + "%");
            command.Parameters.AddWithValue("@CodiceFiscale", this.tb_codicefis.Text);
            command.Parameters.AddWithValue("@DataRicoveroMin", Utility.CodificaData(this.tb_data_min.Text));
            command.Parameters.AddWithValue("@DataRicoveroMax", Utility.CodificaData(this.tb_data_max.Text));
            /*if ((Session["tipo"].ToString() == "Medico di medicina generale"))
            {
                command.Parameters.AddWithValue("@IdMedicoCurante", this.Session["IdUtente"].ToString());
            }
            else
            {*/
                command.Parameters.AddWithValue("@IdMedicoCurante", this.ddl_medico.SelectedValue);
            //}
           // command.Parameters.AddWithValue("@idutente", this.Session["IdUtente"].ToString());
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            try
            {
                try
                {
                    adapter.Fill(dataTable);
                }
                finally
                {
                    connection.Dispose();
                }
            }
            catch (Exception exception)
            {
                this.lb_mess.Text = exception.Message;
            }
            finally
            {
                adapter = null;
            }
            this.gv_pazienti.DataSource = dataTable;
            this.gv_pazienti.DataBind();
            if (dataTable.Rows.Count == 0)
            {
                this.gv_pazienti.Visible = false;
                this.lb_mess.Text = "Nessun paziente corrispondente ai criteri indicati.";
            }
            else
            {
                this.gv_pazienti.Visible = true;
                this.lb_mess.Text = "";
            }
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message;
        }
        finally
        {
            connection.Close();
        }
    }

    private void ElencaMedici()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            //SqlCommand command = new SqlCommand("elenca_medici", connection);
            SqlCommand command = new SqlCommand("elenca_medici_mmg", connection);
            
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            ListItem item = new ListItem("[Nessun Filtro]", "-1");
            this.ddl_medico.Items.Add(item);
            while (reader.Read())
            {
                item = new ListItem(reader["Nominativo"].ToString(), reader["idUtente"].ToString());
                this.ddl_medico.Items.Add(item);
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
    }

    protected void gv_pazienti_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        this.gv_pazienti.CurrentPageIndex = e.NewPageIndex;
        this.CercaPazienti();
    }
    string strRitardi = "";
    string strProssimi = "";
    private string[] calcoloSingoloAlert(string idPaziente,SqlConnection connection)
    {
        DateTime dataCorrente = DateTime.Now;
        string strTmpRitardi = "";
        string strTmpProssimi = "";
        //Simuliamo il fatto che la data non sia quella odierna. utilizzato per motivi di test.
        //DateTime dataCorrente = new DateTime(2011, 6, 12);
        //
        

            //SqlCommand command = new SqlCommand("Select CONVERT(DATETIME,DataRegistrazione,112) as Data,idPaziente as id from Paziente,Utente where idPaziente=@IdPaziente and Paziente.IdUtente=Utente.IdUtente UNION Select CONVERT(DATETIME,DataQuadroClinico,112) as Data,idQuadroClinico as id from QuadroClinico,Utente where QuadroClinico.idPaziente=@IdPaziente and  YEAR(CONVERT(DATETIME,DataQuadroClinico,112))>1900 and QuadroClinico.idUtente=Utente.idUtente and Utente.tipo='Medico di medicina generale'", connection);
            SqlCommand command;
            string tipoControllo = "";

            if ((Session["tipo"].ToString() == "Medico di medicina generale"))
            {
                command = new SqlCommand("Select DataProssimoQuadroClinico,DataQuadroClinico,idQuadroClinico,* from QuadroClinico where idPaziente=@idPaziente and gTipo IS NULL and gMinnesota1 IS NOT NULL or (gTipo ='RECLUTAMENTO' and idPaziente=@idPaziente) order by gTipo,QuadroClinico.DataQuadroClinico", connection);
                //Response.Write("Select DataProssimoQuadroClinico,DataQuadroClinico,idQuadroClinico,* from QuadroClinico where idPaziente=@idPaziente and gTipo IS NULL and gMinnesota1 IS NOT NULL or (gTipo ='RECLUTAMENTO' and idPaziente=@idPaziente) order by gTipo,QuadroClinico.DataQuadroClinico <br/>");
                tipoControllo = " di base ";
            }
            else
            {
                command = new SqlCommand("Select  DataProssimoQuadroClinico,DataQuadroClinico,idQuadroClinico,* from QuadroClinico where idPaziente=@idPaziente and gMinnesota1 IS  NULL and gTipo is NULL or (gTipo ='RECLUTAMENTO' and idPaziente=@idPaziente) order by gTipo,QuadroClinico.DataQuadroClinico", connection);
                //Response.Write("Select  DataProssimoQuadroClinico,DataQuadroClinico,idQuadroClinico,* from QuadroClinico where idPaziente=@idPaziente and gMinnesota1 IS  NULL and gTipo is NULL or (gTipo ='RECLUTAMENTO' and idPaziente=" + idPaziente + ") order by QuadroClinico.DataQuadroClinico <br/>");
                tipoControllo = " specialistico ";
            }
        
            command.Parameters.AddWithValue("@IdPaziente", idPaziente);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);

           // DateTime dataReclutamento = DateTime.Parse(dataTable.Rows[0]["Data"].ToString());
            
          //  if (dataTable.Rows.Count < 27)
           // {
                if (dataTable.Rows.Count == 2)
                {
                    lblAlert.Text += "<br/><b style='color:#0000FF;'>Ricorda di compilare il Questionario della Vita nel primo controllo</b><br>";
                }
                if (dataTable.Rows.Count > 0)
                {
                    //DateTime ultimoControlloEffettuato = DateTime.Parse(dataTable.Rows[dataTable.Rows.Count - 1]["Data"].ToString());
                    if (dataTable.Rows[0]["DataProssimoQuadroClinico"].ToString() != "")
                    {
                        String ultimoControlloEffettuato = Utility.DecodificaData(dataTable.Rows[0]["DataProssimoQuadroClinico"].ToString());

                        //primo mese, un controllo a settimana

                        //if (dataTable.Rows.Count < 5) //se è gia 5 (4 controlli + reclutamento, i controlli settimanali del primo mese sono già stati tutti eseguiti)
                        //{

                        //ultimoControlloEffettuato = ultimoControlloEffettuato.AddDays(7);
                        //lblAlert.Text += "Il prossimo controllo per il paziente selezionato è previsto intorno al " + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/><i>Ricorda che per il primo mese dal reclutamento è previsto un controllo a settimana</i>";
                        lblAlert.Text += "Il prossimo controllo " + tipoControllo + " per il paziente selezionato è il " + ultimoControlloEffettuato + "<br/><i>Ricorda che per il primo mese dal reclutamento è previsto un controllo a settimana</i>";

                        strTmpProssimi = "[PAZIENTE] : prossimo controllo " + tipoControllo + " previsto il :" + ultimoControlloEffettuato + "<br/>";
                       
                        /*}
                        else
                        {
                            //nell'altro caso, un controllo ogni 14 gg.
                            ultimoControlloEffettuato = ultimoControlloEffettuato.AddDays(14);
                            lblAlert.Text += "Il prossimo controllo per il paziente selezionato è previsto intorno al " + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/><i>Ricorda che per i mesi successivi al primo, è previsto un controllo ogni 2 settimane</i>";
                            strTmpProssimi = "[PAZIENTE] : prossimo controllo previsto intorno al :" + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/>"; ;
                            if (dataCorrente.Subtract(ultimoControlloEffettuato).TotalDays > 15)
                            {
                                lblAlert.Text += "<br/><b style='color:#ff0000;'>Attenzione. Sono passati più di 15 gg dall'ultimo controllo</b><br>";
                                strTmpRitardi = "[PAZIENTE] : sono passati più di 15 gg dall'ultimo controllo<br/>";
                            }
                        }
                        if ((dataCorrente.Subtract(dataReclutamento).TotalDays > 30) && (dataTable.Rows.Count < 5))
                        {
                            lblAlert.Text += "<br/><b style='color:#ff0000;'>Attenzione. Il primo mese è passato e non hai ancora eseguito tutti e 4 i controlli previsti.</b><br>";
                            strTmpRitardi = "[PAZIENTE] : Il primo mese è passato e non hai ancora eseguito tutti e 4 i controlli previsti<br/>";
                        }*/
                    }
                }

          /*  }
            else
            {

                lblAlert.Text += "<br/><b style='color:#0000FF;'>Hai effettuato tutti i controlli previsti per lo studio del paziente. Ricordati di compilare il Questionario della vita nell'ultimo controllo.</b><br>";

            }*/
            //controllo compilazione questionario minnesota
            /* command = new SqlCommand(" Select * from QuadroClinico where idPaziente=@IdPaziente and DataQuadroClinico=@dataQuadroClinico", connection);
             command.Parameters.AddWithValue("@dataQuadroClinico",);
             command.CommandType = CommandType.Text;
             adapter = new SqlDataAdapter();
             adapter.SelectCommand = command;
             DataTable dataTableMinnesota = new DataTable();
             adapter.Fill(dataTableMinnesota);*/
        //}
        /*if (Session["tipo"].ToString() == "Cardiologo ambulatoriale")
        {
            SqlCommand command = new SqlCommand("Select CONVERT(DATETIME,DataRegistrazione,112) as Data,idPaziente as id from Paziente,Utente where idPaziente=@IdPaziente and Paziente.IdUtente=Utente.IdUtente UNION Select CONVERT(DATETIME,DataQuadroClinico,112) as Data,idQuadroClinico as id from QuadroClinico,Utente where QuadroClinico.idPaziente=@IdPaziente and  YEAR(CONVERT(DATETIME,DataQuadroClinico,112))>1900 and QuadroClinico.idUtente=Utente.idUtente and Utente.tipo='Cardiologo ambulatoriale'", connection);
            command.Parameters.AddWithValue("@IdPaziente", idPaziente);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            DataTable dataTable = new DataTable();
            adapter.Fill(dataTable);
            DateTime dataReclutamento = DateTime.Parse(dataTable.Rows[0]["Data"].ToString());
            DateTime ultimoControlloEffettuato = DateTime.Parse(dataTable.Rows[dataTable.Rows.Count - 1]["Data"].ToString());
            switch (dataTable.Rows.Count)
            {
                case 1:
                    ultimoControlloEffettuato = ultimoControlloEffettuato.AddDays(30);
                    lblAlert.Text += "Il prossimo controllo specialistico per il paziente selezionato è previsto intorno al " + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/><i>Ricorda che il primo controllo specialistico va eseguito dopo un mese dal reclutamento</i>";
                    strTmpProssimi = "[PAZIENTE] : prossimo controllo specialistico previsto intorno al :" + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/>";
                    if (dataCorrente.Subtract(DateTime.Parse(dataTable.Rows[dataTable.Rows.Count - 1]["Data"].ToString())).TotalDays > 30)
                    { lblAlert.Text += "<br/><b style='color:#ff0000;'>Attenzione. Il primo mese è passato e non hai ancora eseguito il controllo specialistico previsto.</b><br>";
                    strTmpRitardi = "[PAZIENTE] : Il primo mese è passato e non hai ancora eseguito il controllo specialistico previsto<br/>";
                    }
                    break;
                case 2:
                    ultimoControlloEffettuato = ultimoControlloEffettuato.AddDays(60);
                    lblAlert.Text += "Il prossimo controllo specialistico per il paziente selezionato è previsto intorno al " + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/><i>Ricorda che il secondo controllo specialistico va eseguito dopo 3 mesi dal reclutamento</i>";
                    strTmpProssimi = "[PAZIENTE] : prossimo controllo specialistico previsto intorno al :" + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/>";
                    if (dataCorrente.Subtract(DateTime.Parse(dataTable.Rows[dataTable.Rows.Count - 1]["Data"].ToString())).TotalDays > 60)
                    { lblAlert.Text += "<br/><b style='color:#ff0000;'>Attenzione. Il terzo mese è passato e non hai ancora eseguito il controllo specialistico previsto.</b><br>";
                    strTmpRitardi = "[PAZIENTE] :  Il terzo mese è passato e non hai ancora eseguito il controllo specialistico previsto.<br/>";
                    }
                    break;
                case 3:
                    ultimoControlloEffettuato = ultimoControlloEffettuato.AddDays(90);
                    lblAlert.Text += "Il prossimo controllo specialistico per il paziente selezionato è previsto intorno al " + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/><i>Ricorda che il secondo controllo specialistico va eseguito dopo 6 mesi dal reclutamento</i>";
                    strTmpProssimi = "[PAZIENTE] : prossimo controllo specialistico previsto intorno al :" + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/>";
                    if (dataCorrente.Subtract(DateTime.Parse(dataTable.Rows[dataTable.Rows.Count - 1]["Data"].ToString())).TotalDays > 90)
                    { lblAlert.Text += "<br/><b style='color:#ff0000;'>Attenzione. Il sesto mese è passato e non hai ancora eseguito il controllo specialistico previsto.</b><br>";
                    strTmpRitardi = "[PAZIENTE] :  Il sesto mese è passato e non hai ancora eseguito il controllo specialistico previsto.<br/>";
                    }
                    break;
                case 4:
                    ultimoControlloEffettuato = ultimoControlloEffettuato.AddDays(180);
                    lblAlert.Text += "Il prossimo controllo specialistico per il paziente selezionato è previsto intorno al " + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/><i>Ricorda che il secondo controllo specialistico va eseguito dopo 12 mesi dal reclutamento</i>";
                    strTmpProssimi = "[PAZIENTE] : prossimo controllo specialistico previsto intorno al :" + ultimoControlloEffettuato.Day + "/" + ultimoControlloEffettuato.Month + "/" + ultimoControlloEffettuato.Year + "<br/>";
                    if (dataCorrente.Subtract(DateTime.Parse(dataTable.Rows[dataTable.Rows.Count - 1]["Data"].ToString())).TotalDays > 180)
                    { lblAlert.Text += "<br/><b style='color:#ff0000;'>Attenzione. Il dodicesimo mese è passato e non hai ancora eseguito il controllo specialistico previsto.</b><br>";
                    strTmpRitardi = "[PAZIENTE] :  Il dodicesimo mese è passato e non hai ancora eseguito il controllo specialistico previsto.<br/>";
                    }
                    break;
                case 5:

                    lblAlert.Text += "<br/><b style='color:#0000FF;'>Hai effettuato tutti i controlli specialistici previsti per lo studio del paziente</b><br>";
                    break;
            }


        }*/
        string[] str = new string[2];
        str[0] = strTmpRitardi;
        str[1] = strTmpProssimi;
       
        return str;
    }
    private void calcolaAlert()
    {
        lblAlert.Text = "";
         SqlConnection connection = new SqlConnection(this.conn_str);
            try
            {
               
                connection.Open();
                //caso paziente singolo selezionato. alert specifico
                if ((Request.Params["idpaziente"] != null) && (!dead))
                {
                    calcoloSingoloAlert(Request.Params["idpaziente"].ToString(), connection);
                }
                else
                {
                    //caso di nessun paziente ancora selezionato. vanno fatti gli alert per tutti i propri pazienti.
                    SqlCommand command = new SqlCommand("Select * from Paziente where idMedicoCurante=@utente", connection);
                    if ((Session["tipo"].ToString() == "Medico di medicina generale"))
                    {
                    }
                    else
                    {
                        if (Session["tipo"].ToString() == "Cardiologo ambulatoriale")
                        {
                            //command = new SqlCommand("Select * from Paziente where idUtente=@utente", connection);
                            command = new SqlCommand("Select * from Paziente", connection);
                        
                        }
                        else
                        {
                            connection.Close();
                            return;
                        }
                    }

                    command.Parameters.AddWithValue("@utente", Session["IdUtente"].ToString());
                    command.CommandType = CommandType.Text;
                    SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    DataTable dataTable = new DataTable();
                    adapter.Fill(dataTable);
                    strRitardi = "<div style='width:90%;margin:auto; border:1px #ff0000 solid; text-align:left; background-color:#FF0000; height:22px;color:#FFFFFF; font-size:12px;text-align:left;'>&nbsp;&nbsp;<b>Controlli in ritardo</b> <br></div><div style='width:90%; margin:auto; text-align:left;border:1px #ff0000 solid; text-align:left;'><span style='font-size:12px;text-align:left;'>";
                    strProssimi = "<div style='margin-top:10px;width:90%;margin:auto; border:1px #0000FF solid; text-align:left; background-color:#0000FF; height:22px;color:#FFFFFF; font-size:12px;text-align:left;'>&nbsp;&nbsp;<b>Prossimi controlli previsti</b> <br></div><div style='width:90%; margin:auto; text-align:left;border:1px #0000FF solid; text-align:left;'><span style='font-size:12px;text-align:left;'>";
                   
                    bool hasRitardi = false;
                    bool hasNotifiche = false;
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        if ((dataTable.Rows[i]["gDead"] != ""))
                        {
                            
                            if ((Session["tipo"].ToString() == "Medico di medicina generale"))
                            {
                                string[] str = calcoloSingoloAlert(dataTable.Rows[i]["idPaziente"].ToString(), connection);
                                string singoloAlert = str[0];
                                string singoloProssimo = str[1];
                                if (singoloAlert.Replace("[PAZIENTE]", "<a href='HomePageTipo.aspx?idPaziente=" + dataTable.Rows[i]["idPaziente"].ToString() + "'>" + dataTable.Rows[i]["Cognome"].ToString() + " " + dataTable.Rows[i]["Nome"].ToString() + " </a>") != "")
                                    hasRitardi = true;
                                strRitardi += singoloAlert.Replace("[PAZIENTE]", "<a href='HomePageTipo.aspx?idPaziente=" + dataTable.Rows[i]["idPaziente"].ToString() + "'>" + dataTable.Rows[i]["Cognome"].ToString() + " " + dataTable.Rows[i]["Nome"].ToString() +" </a>");
                                strProssimi += singoloProssimo.Replace("[PAZIENTE]", "<a href='HomePageTipo.aspx?idPaziente=" + dataTable.Rows[i]["idPaziente"].ToString() + "'>" + dataTable.Rows[i]["Cognome"].ToString() + " " + dataTable.Rows[i]["Nome"].ToString() + " </a>");
                                hasNotifiche = true;
                               
                                //lblAlert.Text = strRitardi.Replace("[PAZIENTE]", dataTable.Rows[i]["Cognome"].ToString() + " " + dataTable.Rows[i]["Nome"].ToString() + " (" + dataTable.Rows[i]["CodiceFiscale"].ToString() + ") "); 
                                
                            }
                            if ((Session["tipo"].ToString() == "Cardiologo ambulatoriale"))
                            {
                                string[] str = calcoloSingoloAlert(dataTable.Rows[i]["idPaziente"].ToString(), connection);
                                string singoloAlert = str[0];
                                string singoloProssimo = str[1];
                                if (singoloAlert.Replace("[PAZIENTE]", "<a href='HomePageTipo.aspx?idPaziente=" + dataTable.Rows[i]["idPaziente"].ToString() + "'>" + dataTable.Rows[i]["Cognome"].ToString() + " " + dataTable.Rows[i]["Nome"].ToString() + " </a>") != "")
                                    hasRitardi = true;
                                strRitardi += singoloAlert.Replace("[PAZIENTE]", "<a href='HomePageTipo.aspx?idPaziente=" + dataTable.Rows[i]["idPaziente"].ToString() + "'>" + dataTable.Rows[i]["Cognome"].ToString() + " " + dataTable.Rows[i]["Nome"].ToString() + " </a>");
                                strProssimi += singoloProssimo.Replace("[PAZIENTE]", "<a href='HomePageTipo.aspx?idPaziente=" + dataTable.Rows[i]["idPaziente"].ToString() + "'>" + dataTable.Rows[i]["Cognome"].ToString() + " " + dataTable.Rows[i]["Nome"].ToString() + " </a>");
                                hasNotifiche = true;
                            }
                        }
                    }
                    strRitardi += "</span></div>";
                    strProssimi += "</span></div>";
                    string strgruppoControllo = "";
                    if (Session["tipo"].ToString() != "Medico di medicina generale")
                    {
                        strgruppoControllo = "<div style='margin-top:10px;width:90%;margin:auto; border:1px #0000FF solid; text-align:left; background-color:#0000FF; height:22px;color:#FFFFFF; font-size:12px;text-align:left;'>&nbsp;&nbsp;<b>Gruppo di Controllo</b> <br></div><div style='width:90%; margin:auto; text-align:left;border:1px #0000FF solid; text-align:left;'><span style='font-size:12px;text-align:left;'>";
                        command = new SqlCommand("Select * from Paziente where idMedicoCurante=-1 or idMedicoCurante=2", connection);
                        command.CommandType = CommandType.Text;
                        SqlDataAdapter adapter2 = new SqlDataAdapter();
                        adapter2.SelectCommand = command;
                        DataTable dataTable2 = new DataTable();
                        adapter2.Fill(dataTable2);
                        //sResponse.Write(dataTable2.Rows.Count);

                        for (int i = 0; i < dataTable2.Rows.Count; i++)
                        {
                            strgruppoControllo += "<a href='HomePageTipo.aspx?idPaziente=" + dataTable2.Rows[i]["idPaziente"].ToString() + "'>" + dataTable2.Rows[i]["Cognome"].ToString() + " " + dataTable2.Rows[i]["Nome"].ToString() + " </a><br/>";
                        }
                        strgruppoControllo += "</span></div>";
                    }


                    lblAlert.Text = "";
                    if (hasRitardi)
                        lblAlert.Text += strRitardi;
                    if (hasNotifiche)
                        lblAlert.Text += "<br><br>" + strProssimi;
                    lblAlert.Text +=  "<br><br>" +strgruppoControllo;
                }
                
            }
            catch (Exception exception)
            {
                Response.Write(exception.Message + " " + exception.StackTrace);
            }
            finally
            {
                connection.Close();
            }
    }
    protected bool dead = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Utente"] == null) { Response.Redirect("login.aspx"); }
        if ((Session["tipo"].ToString() == "Medico di medicina generale"))
        {
            bttReclutamento.Enabled = false;
        }
        this.Label1.Text = "Benvenuto " + this.Session["Utente"].ToString();
        
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
       
        if (!base.IsPostBack)
        {
            if (base.Request.Params["del"] != null)
            {
                SqlConnection connection = new SqlConnection(this.conn_str);
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("elimina_paziente", connection);
                    command.Parameters.AddWithValue("@IdPaziente", base.Request.Params["del"].ToString());
                    command.CommandType = CommandType.StoredProcedure;
                    command.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    this.lb_mess.Text = exception.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            this.ElencaMedici();
            this.CercaPazienti();

            if (Request.Params["idpaziente"] != null)
            {
                SqlConnection connection = new SqlConnection(this.conn_str);
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("cerca_paziente_id", connection);
                    command.Parameters.AddWithValue("@IdPaziente", Request.Params["idpaziente"].ToString());
                    command.CommandType = CommandType.StoredProcedure;
                       SqlDataAdapter adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    DataTable dataTable = new DataTable() ;
                    adapter.Fill(dataTable);
                    if (dataTable.Rows.Count > 0)
                    {
                        codPazSel.Text = Request.Params["idpaziente"].ToString();
                        //lblPazienteSel.Text = "<br>Paziente Selezionato: <b>" + dataTable.Rows[0]["Cognome"] + " " + dataTable.Rows[0]["Nome"] + " - " + dataTable.Rows[0]["CodiceFiscale"] + "</b><br>";
                        
                        bttSearchPaziente_Click(null, null);
                        bttNewControl.Enabled = true;
                       // Response.Write(Session["tipo"].ToString());
                        
                        if ((Session["tipo"].ToString() != "Medico di medicina generale"))
                        {
                            bttNewRicovero.Enabled = true;
                            bttNewRicoveroNonCardio.Enabled = true;
                        }
                        else
                        {
                            bttReclutamento.Enabled = false;
                            bttNewRicovero.Enabled = false;
                        }


                        bttDead.Enabled = true;
                        loadControlli();
                        lblPazienteSel.Text = "<br>Paziente Selezionato: <b>" + linkReclutamento + " - " + dataTable.Rows[0]["CodiceFiscale"] + "</b><br>";
                        //controllo dead
                        if (dataTable.Rows[0]["gdead"].ToString() != "")
                        {
                            dead = true;
                            bttNewRicovero.Enabled = false;
                            bttNewControl.Enabled = false;
                            lblDead.Visible = true;
                            lblDead.Text = "Attenzione, il paziente risulta 'dead' pertanto non è più possibile registrare controlli o ricoveri";
                        }
                        else
                        {
                            lblDead.Text = "";
                            lblDead.Visible = false;
                        }
                    }
                    bttSearchPaziente_Click(null, null);
                }
                catch (Exception exception)
                {
                    this.lb_mess.Text = exception.Message;
                }
                finally
                {
                    connection.Close();
                }


            

            }
        }
        calcolaAlert();
        gv_pazienti.ItemCommand += new DataGridCommandEventHandler(gv_pazienti_ItemCommand);

        switch (this.Session["Tipo"].ToString())
        {
            case "admin":
                urlControllo = "dativariabili.aspx";
                break;
            case "Medico di medicina generale":
                urlControllo = "dativariabilimmg.aspx";
                break;
            case "Ospedaliero":
                urlControllo = "dativariabilispecialist.aspx";
                break;
            case "Cardiologo ambulatoriale":
                urlControllo = "dativariabilispecialist.aspx";
                break;
        }
        loadToTPazienti();
    }
    private void loadToTPazienti()
    {
        DataTable dataTable = new DataTable();
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            string cmdText = "select * from Paziente;";
            DataTable dtTemp = new DataTable();
            SqlCommand command = new SqlCommand(cmdText, connection);
            command.CommandType = CommandType.Text;
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            try
            {
                try
                {
                    adapter.Fill(dtTemp);
                    numTot = dtTemp.Rows.Count;
                    cmdText = "select * from Paziente where idUtente=" + Session["IdUtente"] + ";";
                    command = new SqlCommand(cmdText, connection);
                    command.CommandType = CommandType.Text;
                    dtTemp.Clear();
                    adapter = new SqlDataAdapter();
                    adapter.SelectCommand = command;
                    adapter.Fill(dtTemp);
                    numTuo = dtTemp.Rows.Count;
                }
                finally
                {
                    connection.Dispose();
                }
            }
            catch (Exception exception)
            {
                this.lb_mess.Text = exception.Message;
            }
            finally
            {
                adapter = null;
            }
           
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message;
        }
        finally
        {
            connection.Close();
        }
    }
    void gv_pazienti_ItemCommand(object source, DataGridCommandEventArgs e)
    {
        if (e.Item.ItemIndex == -1) { return; }
        string code = gv_pazienti.Items[e.Item.ItemIndex].Cells[0].Text;
        try
        {
            switch (e.CommandName)
            {
                case "seleziona":
                    codPazSel.Text = code;
                    Response.Redirect("HomePageTipo.aspx?idPaziente=" + code);
                    /*lblPazienteSel.Text = "<br>Paziente Selezionato: <b>" + gv_pazienti.Items[e.Item.ItemIndex].Cells[1].Text + " " + gv_pazienti.Items[e.Item.ItemIndex].Cells[2].Text + " - " + gv_pazienti.Items[e.Item.ItemIndex].Cells[3].Text + "</b><br>";
                    bttSearchPaziente_Click(null, null);
                    bttNewControl.Enabled = true;
                    if ((Session["tipo"].ToString() != "Medico di medicina generale") || (Session["tipo"].ToString() != "Ospedaliero"))
                    {
                        bttNewRicovero.Enabled = true;
                    }

                   
                    bttDead.Enabled = true;
                    loadControlli();*/
                   break;
                case "modifica":
                   Response.Redirect("nuovopaziente.aspx?idPaziente=" + code);
                   break;
            }


        }
        catch (Exception ex)
        { }
    }
    protected void bttSearchPaziente_Click(object sender, EventArgs e)
    {
        switch(bttSearchPaziente.Text)
        {
            case "SELEZIONA PAZIENTE":
                bttSearchPaziente.Text = "CHIUDI SELEZIONE PAZIENTE";
                divPaziente.Visible = true;
                tablePanel.Visible = false;
                pnlLegenda.Visible = false;
                break;
            case "CHIUDI SELEZIONE PAZIENTE":
                bttSearchPaziente.Text = "SELEZIONA PAZIENTE";
                divPaziente.Visible = false;
                tablePanel.Visible = true;
                pnlLegenda.Visible = true;
                break;
        }
       
    }
    private bool EstraiDati()
    {
        bool flag = false;
        
        SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["conn_str"].ToString());
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("cerca_paziente_medico_id", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", codPazSel.Text);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    linkReclutamento = "<a href=nuovopaziente.aspx?idpaziente=" + reader["IdPaziente"].ToString() + ">" + reader["Cognome"] +"&nbsp;&nbsp;" + reader["Nome"]+"</a>";
                    this.lblQuadri.Text += "</br></br><div style='width:340px; margin-left:10px; height:35px; font-size:14px; margin-top:10px; background-color:#eeeeee; border:1px #003366 solid;'><a style='color:#003366; text-decoration:none;' href=\"nuovopaziente.aspx?idpaziente=" + reader["IdPaziente"].ToString() + "\"><b>RECLUTAMENTO (" + Utility.DecodificaData(reader["DataRegistrazione"].ToString()) + ")</b> <br />Eseguito da <b>" + reader["Nominativo"].ToString() + "</b></a></div>";
                   lblReclutatoIl.Text ="Reclutato il " + Utility.DecodificaData(reader["DataRegistrazione"].ToString()) + "";
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lblQuadri.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }
    private bool EstraiQuadriClinici()
    {
        bool flag = false;
       
        SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["conn_str"].ToString());
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("elenca_quadri_clinici_utenti", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", codPazSel.Text);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                  
                        object obj3 = this.lblQuadri.Text;
                        string color="";
                        string tipo = "";
                        string urldati = "";
                        switch (reader["tipo"].ToString())
                        {
                            case "admin":
                                tipo = " ADMIN";
                                color = "#CCCCCC";
                                urldati = "dativariabili.aspx";
                                break;
                            case "Medico di medicina generale":
                                tipo = " MMG";
                                color = "#6699CC";
                                urldati = "dativariabilimmg.aspx";
                                break;
                            case "Ospedaliero":
                                tipo = " OSPEDALIERO";
                                color = "#9999FF";
                                urldati = "dativariabilispecialist.aspx";
                                break;
                            case "Cardiologo ambulatoriale":
                                tipo = " SPECIALIST";
                                color = "#9999FF";
                                urldati = "dativariabilispecialist.aspx";
                                break;
                        }
                        this.lblQuadri.Text = string.Concat(new object[] { obj3, "<div style='width:340px;margin-left:10px; height:35px; margin-top:10px; font-size:14px; background-color:" + color + "; border:1px #003366 solid;'><a style='color:#003366; text-decoration:none;' href=\"" + urldati + "?idpaziente=", reader["IdPaziente"].ToString(), "&idquadro=", reader["IdQuadroClinico"], "\"><b>CONTROLLO" + tipo + " (", Utility.DecodificaData(reader["DataQuadroClinico"].ToString()), ")</b><br /> Eseguito da <b>", reader["Nominativo"].ToString(), "</b></a></div>" });
                  
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            this.lblQuadri.Text = this.lblQuadri.Text + "</ul></div>";
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lblQuadri.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }
    private void loadControlli()
    {
        this.lblQuadri.Text = "";
        EstraiQuadriClinici();
        EstraiDati();
       
    }
    protected void bttReclutamento_Click(object sender, EventArgs e)
    {
        Response.Redirect("nuovopaziente.aspx?idPaziente=-1");
    }
    protected void bttNewControl_Click(object sender, EventArgs e)
    {
        Response.Redirect(urlControllo + "?idpaziente=" + codPazSel.Text+ "&idquadro=-1");
    }
    protected void bttNewRicovero_Click(object sender, EventArgs e)
    {
        Response.Redirect(urlControllo + "?idpaziente=" + codPazSel.Text + "&idquadro=-1");
    }
    protected void bttDead_Click(object sender, EventArgs e)
    {
        Response.Redirect("dead.aspx?idpaziente=" + codPazSel.Text);
    }
}

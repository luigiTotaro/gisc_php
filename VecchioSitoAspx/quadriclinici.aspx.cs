﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class quadriclinici : System.Web.UI.Page
{
    private string conn_str;

    private string IdPaziente;


    private void ElencaQuadriClinici()
    {
        DataTable dataTable = new DataTable();
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("elenca_quadri_clinici", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataAdapter adapter = new SqlDataAdapter();
            adapter.SelectCommand = command;
            try
            {
                try
                {
                    adapter.Fill(dataTable);
                }
                finally
                {
                    connection.Dispose();
                }
            }
            catch (Exception exception)
            {
                this.lb_mess.Text = exception.Message;
            }
            finally
            {
                adapter = null;
            }
            this.gv_pazienti.DataSource = dataTable;
            this.gv_pazienti.DataBind();
            if (dataTable.Rows.Count == 0)
            {
                this.gv_pazienti.Visible = false;
                this.lb_mess.Text = "Nessun quadro clinico corrispondente al paziente.";
            }
            else
            {
                this.gv_pazienti.Visible = true;
                this.lb_mess.Text = "";
            }
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message;
        }
        finally
        {
            connection.Close();
        }
    }

    private void EstraiDati()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("cerca_paziente_id", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.lb_nome.Text = reader["Nome"].ToString() + " " + reader["Cognome"].ToString();
                this.lb_cognome.Text = reader["Nome"].ToString() + " " + reader["Cognome"].ToString();
                this.lb_codfis.Text = reader["CodiceFiscale"].ToString();
                this.lb_data.Text = Utility.DecodificaData(reader["DataRegistrazione"].ToString());
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        if (!base.IsPostBack)
        {
            if (base.Request.Params["IdPaziente"] != null)
            {
                this.IdPaziente = base.Request.Params["IdPaziente"].ToString();
            }
            if (base.Request.Params["mod"] != null)
            {
                if (base.Request.Params["mod"].ToString().Equals("1"))
                {
                    this.lb_mess.Text = "La modifica \x00e8 stata effettuata correttamente.";
                }
                if (base.Request.Params["mod"].ToString().Equals("2"))
                {
                    this.lb_mess.Text = "Il nuovo paziente \x00e8 stato inserito correttamente.";
                }
                if (base.Request.Params["mod"].ToString().Equals("3"))
                {
                    this.lb_mess.Text = "Il nuovo quadro clinico \x00e8 stato inserito correttamente.";
                }
            }
            else
            {
                this.lb_mess.Text = "";
            }
            if (base.Request.Params["del"] != null)
            {
                SqlConnection connection = new SqlConnection(this.conn_str);
                try
                {
                    connection.Open();
                    SqlCommand command = new SqlCommand("elimina_quadro_clinico", connection);
                    command.Parameters.AddWithValue("@IdQuadroClinico", base.Request.Params["del"].ToString());
                    command.CommandType = CommandType.StoredProcedure;
                    command.ExecuteNonQuery();
                }
                catch (Exception exception)
                {
                    this.lb_mess.Text = exception.Message;
                }
                finally
                {
                    connection.Close();
                }
            }
            //this.EstraiDati();
            //this.ElencaQuadriClinici();

            if (base.Request.Params["IdPaziente"] != null)
            {
                //this.IdPaziente = base.Request.Params["IdPaziente"].ToString();
                Response.Redirect("HomePageTipo.aspx?idPaziente=" + base.Request.Params["IdPaziente"].ToString()); ;
            }
        }
    }

    
}


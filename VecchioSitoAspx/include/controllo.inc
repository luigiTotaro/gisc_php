<%
  if(Session["IdSessione"]==null || Session["IdSessione"].ToString()!=Session.SessionID)
  {
      Session.Clear();
      Response.Redirect("login.aspx");
  }
  else if(Session["Tipo"]!=null && Session["Tipo"].ToString().Equals("admin"))
  {
    lb_gest_utenti.Text="<li><a href=\"statistiche.aspx\">Statistiche</a></li><li><a href=\"gestioneutenti/utenti.aspx\">Utenti</a></li>";
    lb_gest_utenti_footer.Text="<a href=\"statistiche.aspx\">Statistiche</a>&nbsp;|&nbsp;<a href=\"gestioneutenti/utenti.aspx\">Utenti</a>&nbsp;|&nbsp;";
  }
  else
  {
    lb_gest_utenti.Text="";
    lb_gest_utenti_footer.Text="";
  }
%>
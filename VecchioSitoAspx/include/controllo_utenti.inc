<%
  if(Session["IdSessione"]==null || Session["IdSessione"].ToString()!=Session.SessionID)
  {
      Session.Clear();
      Response.Redirect("/Cardio/login.aspx");
  }
  else if(Session["Tipo"]!=null && Session["Tipo"].ToString().Equals("admin"))
  {
    lb_gest_utenti_footer.Text="<a href=\"statistiche.aspx\">Statistiche</a>&nbsp;|&nbsp;<a href=\"gestioneutenti/utenti.aspx\">Utenti</a>&nbsp;|&nbsp;";
  }
  else
  {
    lb_gest_utenti_footer.Text="";
  }
%>
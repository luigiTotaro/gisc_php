<%@ Page Language="C#" AutoEventWireup="true" CodeFile="trends.aspx.cs" Inherits="trends" %>
<%@ Register Src="datipaziente.ascx" TagName="datipaziente" TagPrefix="uc1" %>
<!--#include file="include/controllo.inc"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="stile.css" type=text/css rel="stylesheet" />
<script language="JavaScript" src="./js/ts_picker.js"></script>
<script language="JavaScript" src="./js/controlli.js"></script>
<title>Gestione Paziente</title>

<style type="text/css">
<!--
body {
	background-image: url(./files/bg_blueGrad.gif);
	background-color: #324671;
}
.Stile1 {color: #336DD0}
-->
</style>

<link rel="stylesheet" type="text/css" href="./ext-4.0.2a/resources/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="./ext-4.0.2a/examples/shared/example.css" />
    <script type="text/javascript" src="./ext-4.0.2a/bootstrap.js"></script>
    <%--<script type="text/javascript" src="./ext-4.0.2a/examples/example-data.js"></script>--%>
</head>

<body>
   <form id="form1" runat="server">
    <script>
  
    
    Ext.require('Ext.chart.*');
Ext.require('Ext.layout.container.Fit');
var dataTotale;
window.generateData = function(n, floor) {
    var data = [],
            p = (Math.random() * 11) + 1,
            i;

    floor = (!floor && floor !== 0) ? 20 : floor;

    for (i = 0; i < (n || 12); i++) {
        data.push({
            name: Ext.Date.monthNames[i % 12],
            data1: Math.floor(Math.max((Math.random() * 100), floor)),
            data2: Math.floor(Math.max((Math.random() * 100), floor)),
            data3: Math.floor(Math.max((Math.random() * 100), floor)),
            data4: Math.floor(Math.max((Math.random() * 100), floor)),
            data5: Math.floor(Math.max((Math.random() * 100), floor)),
            data6: Math.floor(Math.max((Math.random() * 100), floor)),
            data7: Math.floor(Math.max((Math.random() * 100), floor)),
            data8: Math.floor(Math.max((Math.random() * 100), floor)),
            data9: Math.floor(Math.max((Math.random() * 100), floor))
        });
    }
    return data;
};
function convertiNYHA(val) {

    if (val == "")
        return "";
    if (val == 'I classe')
        return 1;
    if (val == 'II classe')
        return 2;
    if (val == 'III classe')
        return 3;
    if (val == 'IV classe')
        return 4;
    return ""; 
}

function svisceraData(val) {
    
    if (val == "")
        return "Reclutamento";
    var anno = val.substring(0, 4);
    var mese = val.substring(4, 6);
    var giorno = val.substring(6, 8);
    return giorno + "/" + mese;  //+ "/" + anno;
}

function svisceraDataCompleta(val) {

    if (val == "")
        return "Reclutamento";
    var anno = val.substring(0, 4);
    var mese = val.substring(4, 6);
    var giorno = val.substring(6, 8);
    return giorno + "/" + mese  + "/" + anno;
}
Ext.onReady(function() {
    //store1.loadData(generateDataGISCSesso());

    window.generateDataGISCValori = function() {
        var data = [],
            p = 0,
            i;

        var dataFE = [],
            p = 0,
            i;

        var dataPP = [],
            p = 0,
            i;

        var dataIC = [],
            p = 0,
            i;
        var dataBP = [],
                    p = 0,
                    i;
        var dataNY = [],
                    p = 0,
                    i;


        var varLast = 0;
        var varLastFE = 0;
        var varLastPP = 0;
        var varLastIC = 0;
        var varLastBP = 0;
        var varLastNY = 0;
        Ext.Ajax.request({
            url: 'trendsData.aspx',
            params: {
                idPaziente: '<%=Request.Params["idPaziente"] %>',
                action: 'fcardiaca'
            },
            success: function(response) {

                var text = eval(response.responseText);
                dataTotale = text;
                //alert(text.length);
                for (var i = 0; i < text.length; i++) {
                    var fval = 0;
                    if (text[i].gFrequenzaCard == "")
                        fval = undefined;
                    else
                        fval = parseFloat(text[i].gFrequenzaCard);
                    if (isNaN(fval))
                        fval = undefined;
                    var dataSviscerata = svisceraData(text[i].DataQuadroClinico);

                    data.push({
                        name: dataSviscerata,
                        data1: fval,
                        data2: i,
                        data3: fval

                    });
                    varLast = fval;

                    //FE
                    fval = 0;
                    if (text[i].Eco2DFE == "")
                        fval = undefined;
                    else
                        fval = parseFloat(text[i].Eco2DFE);
                    if (isNaN(fval))
                        fval = undefined;
                    var dataSviscerata = svisceraData(text[i].DataQuadroClinico);

                    dataFE.push({
                        name: dataSviscerata,
                        data1: fval,
                        data2: i,
                        data3: fval

                    });
                    varLastFE = fval;


                    //Pressione Polmonare
                    fval = 0;
                    if (text[i].EcoDopplerPressPolm == "")
                        fval = undefined;
                    else
                        fval = parseFloat(text[i].EcoDopplerPressPolm);
                    if (isNaN(fval))
                        fval = undefined;
                    var dataSviscerata = svisceraData(text[i].DataQuadroClinico);
                    dataPP.push({
                        name: dataSviscerata,
                        data1: fval,
                        data2: i,
                        data3: fval

                    });
                    varLastPP = fval;

                    //Creatininemia
                    fval = 0;
                    if (text[i].IngrCreat == "")
                        fval = undefined;
                    else
                        fval = parseFloat(text[i].IngrCreat);
                    if (isNaN(fval))
                        fval = undefined;
                    var dataSviscerata = svisceraData(text[i].DataQuadroClinico);
                    dataIC.push({
                        name: dataSviscerata,
                        data1: fval,
                        data2: i,
                        data3: fval

                    });
                    varLastIC = fval;


                    //PRO BNP
                    fval = 0;
                    if (text[i].IngrBNP == "")
                        fval = undefined;
                    else
                        fval = parseFloat(text[i].IngrBNP);
                    if (isNaN(fval))
                        fval = undefined;
                    var dataSviscerata = svisceraData(text[i].DataQuadroClinico);

                    dataBP.push({
                        name: dataSviscerata,
                        data1: fval,
                        data2: i,
                        data3: fval

                    });
                    varLastBP = fval;


                    //Classe NYHA

                    fval = 0;
                    //console.log(convertiNYHA(text[i].ClasseDispneaSforzo));
                    if (convertiNYHA(text[i].ClasseDispneaSforzo) == "")
                        fval = undefined;
                    else
                        fval = parseFloat(convertiNYHA(text[i].ClasseDispneaSforzo));
                    if (isNaN(fval))
                        fval = undefined;
                   
//                    if (fval == varLastNY)
//                        fval = undefined;
                    var dataSviscerata = svisceraData(text[i].DataQuadroClinico);
                    dataNY.push({
                        name: dataSviscerata,
                        data1: fval,
                        data2: i,
                        data3: fval

                    });
                    if (fval != undefined)
                        varLastNY = fval;


                }
                store2.loadData(data);
                storeFE.loadData(dataFE);
                storePP.loadData(dataPP);
                storeIC.loadData(dataIC);
                storeBP.loadData(dataBP);
                storeNY.loadData(dataNY);
                // process server response here
            }
        });





        //}
        return data;
    }

    //grafico dei valori BBS,BBD,ecc.
    window.store2 = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: []
    });
    window.storeFE = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: []
    });

    window.storePP = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: []
    });
    window.storeIC = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: []
    });

    window.storeBP = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: []
    });


    window.storeNY = Ext.create('Ext.data.JsonStore', {
        fields: ['name', 'data1', 'data2', 'data3', 'data4', 'data5', 'data6', 'data7', 'data9', 'data9'],
        data: []
    });


    store2.loadData(generateDataGISCValori());
    panel2 = Ext.create('widget.panel', {
        width: 1050,
        height: 200,
        title: '',
        border: false,
        style: 'font-size:9px',
        renderTo: 'divDashboardValori',
        layout: 'fit',
        items: [
        {
            id: 'barTopTen',
            xtype: 'chart',
            style: 'background:#fff;margin:8px;',
            animate: {
                easing: 'bounceOut',
                duration: 750
            },
            height: 1000,
            width: 250,
            shadow: true,
            store: store2,

            axes:
            [
             {
                 type: 'Numeric',
                 position: 'left',
                 fields: 'data1',
                 minimum: 35,
                 maximum: 120,
                 decimals: 0,
                 label:
            {
        },
                 grid: true,
                 title: 'Freq. Cardiaca/min'
             },
            {
                type: 'Category',
                position: 'bottom',
                fields: 'name',
                grid: true
            }
            ],
            series:
            [
             {
                 type: 'line',
                 axis: 'left',
                 xField: 'name',
                 yField: 'data1',
                 tips: {
                     trackMouse: true,
                     width: 250,
                     height: 120,
                     renderer: function(storeItem, item) {
                         var idx = parseInt(storeItem.get('data2'));
                         mostraCallout(idx, this);
                     }
                 }

             }
           ]
}]
        });


        panel3 = Ext.create('widget.panel', {
            width: 1050,
            height: 200,
            border: false,

            title: '',
            style: 'font-size:9px',
            renderTo: 'divDashboardValoriFE',
            layout: 'fit',
            items: [
        {
            id: 'barTopTenFE',
            xtype: 'chart',
            theme: 'Green',
            style: 'background:#fff;margin:8px;',
            animate: {
                easing: 'bounceOut',
                duration: 750
            },
            height: 200,
            width: 1000,
            shadow: true,
            store: storeFE,

            axes:
            [
             {
                 type: 'Numeric',
                 position: 'left',
                 fields: 'data1',
                 minimum: 20,
                 maximum: 70,
                 decimals: 0,
                 label:
            {
        },
                 grid: true,
                 title: 'F.E. %'
             },
            {
                type: 'Category',
                position: 'bottom',
                fields: 'name',
                grid: true
            }
            ],
            series:
            [
             {
                 type: 'line',
                 axis: 'left',
                 xField: 'name',
                 yField: 'data1',
                 tips: {
                     trackMouse: true,
                     width: 250,
                     height: 120,
                     renderer: function(storeItem, item) {
                         var idx = parseInt(storeItem.get('data2'));
                         mostraCallout(idx, this);
                     }
                 }

             }
           ]
}]
        });


        panel4 = Ext.create('widget.panel', {
            width: 1050,
            height: 200,

            border: false,
            title: '',
            style: 'font-size:9px',
            renderTo: 'divDashboardValoriPP',
            layout: 'fit',
            items: [
        {
            id: 'barTopTenFE',
            xtype: 'chart',
            theme: 'Sky',
            style: 'background:#fff;margin:8px;',
            animate: {
                easing: 'bounceOut',
                duration: 750
            },
            height: 200,
            width: 1000,
            shadow: true,
            store: storePP,

            axes:
            [
             {
                 type: 'Numeric',
                 position: 'left',
                 fields: 'data1',
                 minimum: 20,
                 maximum: 80,
                 decimals: 0,
                 label:
            {
        },
                 grid: true,
                 title: 'Pres.Polm. mmhg'
             },
            {
                type: 'Category',
                position: 'bottom',
                fields: 'name',
                grid: true
            }
            ],
            series:
            [
             {
                 type: 'line',
                 axis: 'left',
                 xField: 'name',
                 yField: 'data1',
                 tips: {
                     trackMouse: true,
                     width: 250,
                     height: 120,
                     renderer: function(storeItem, item) {
                         var idx = parseInt(storeItem.get('data2'));
                         mostraCallout(idx, this);
                     }
                 }

             }
           ]
}]
        });



        panel5 = Ext.create('widget.panel', {
            width: 1050,
            height: 200,
            border: false,
            title: '',
            style: 'font-size:9px',
            renderTo: 'divDashboardValoriIC',
            layout: 'fit',
            items: [
        {
            id: 'barTopTenIC',
            xtype: 'chart',
            theme: 'Red',
            style: 'background:#fff;margin:8px;',
            animate: {
                easing: 'bounceOut',
                duration: 750
            },
            height: 200,
            width: 1000,
            shadow: true,
            store: storeIC,

            axes:
            [
             {
                 type: 'Numeric',
                 position: 'left',
                 fields: 'data1',
                 minimum: 0,
                 maximum: 4,
                 decimals: 2,
                 label:
            {
        },
                 grid: true,
                 title: 'Creatininemia mg/dl'
             },
            {
                type: 'Category',
                position: 'bottom',
                fields: 'name',
                grid: true
            }
            ],
            series:
            [
             {
                 type: 'line',
                 axis: 'left',
                 xField: 'name',
                 yField: 'data1',
                 tips: {
                     trackMouse: true,
                     width: 250,
                     height: 120,
                     renderer: function(storeItem, item) {
                         var idx = parseInt(storeItem.get('data2'));
                         mostraCallout(idx, this);
                     }
                 }

             }
           ]
}]
        });


        panel6 = Ext.create('widget.panel', {
            width: 1050,
            height: 200,
            border: false,
            title: '',
            style: 'font-size:9px',
            renderTo: 'divDashboardValoriBP',
            layout: 'fit',
            items: [
        {
            id: 'barTopTenIC',
            xtype: 'chart',
            theme: 'Purple',
            style: 'background:#fff;margin-left:-5px;',
            animate: {
                easing: 'bounceOut',
                duration: 750
            },
            height: 200,
            width: 1000,
            shadow: true,
            store: storeBP,

            axes:
        [
        {
            type: 'Numeric',
            position: 'left',
            fields: 'data1',
            //minimum: 0,
            //maximum: 14000,
            decimals: 2,
            label:
        {
    },
            grid: true,
            title: 'BNP pg/ml'
        },
        {
            type: 'Category',
            position: 'bottom',
            fields: 'name',
            grid: true
        }
        ],
            series:
        [
        {
            type: 'line',
            axis: 'left',
            xField: 'name',
            yField: 'data1',
            tips: {
                trackMouse: true,
                width: 250,
                height: 120,
                renderer: function(storeItem, item) {
                    var idx = parseInt(storeItem.get('data2'));
                    mostraCallout(idx, this);
                }
            }

        }
        ]
}]
        });


        panel7 = Ext.create('widget.panel', {
            width: 1050,
            height: 200,
            border: false,
            title: '',
            style: 'font-size:9px',
            renderTo: 'divDashboardValoriNY',
            layout: 'fit',
            items: [
        {
            id: 'barTopTenNY',
            xtype: 'chart',
            theme: 'Blue',
            style: 'background:#fff;margin:8px;',
            animate: {
                easing: 'bounceOut',
                duration: 750
            },
            height: 200,
            width: 1000,
            shadow: true,
            store: storeNY,

            axes:
        [
        {
            type: 'Numeric',
            position: 'left',
            fields: 'data1',
            minimum: 0,
            maximum: 4,
            minorTickSteps: 1,
            majorTickSteps: 1,
            decimals: 0,
            label:
        {
    },
            grid: true,
            title: 'Classe NYHA'
        },
        {
            type: 'Category',
            position: 'bottom',
            fields: 'name',
            grid: true
        }
        ],
            series:
        [
        {
            type: 'line',
            axis: 'left',
            xField: 'name',
            yField: 'data1',
            tips: {
                trackMouse: true,
                width: 250,
                height: 120,
                renderer: function(storeItem, item, idx) {
                    var idx = parseInt(storeItem.get('data2'));
                    mostraCallout(idx, this);
                }
            }

        }
        ]
}]
        });

    });

    function mostraCallout(idx,obj) {
        var strCallout = '<div><b style="font-size:14px;color:#0000FF">' + svisceraDataCompleta(dataTotale[idx].DataQuadroClinico) + '</b><br/><br/> Frequenza Cardiaca:<b>' + pulisciDato(dataTotale[idx].gFrequenzaCard, '/min') + '</b>';
                    strCallout += '<br/>F.E.: ' + pulisciDato(dataTotale[idx].Eco2DFE, ' %');
                    strCallout += '<br/>Pressione Polmonare: ' + pulisciDato(dataTotale[idx].EcoDopplerPressPolm, ' mmhg');
                    strCallout += '<br/>Creatininemia: ' + pulisciDato(dataTotale[idx].IngrCreat, ' mg/dl');
                    strCallout += '<br/>BNP: ' + pulisciDato(dataTotale[idx].IngrBNP, ' pg/ml');
                    strCallout += '<br/>Classe NYHA: ' + pulisciDato(dataTotale[idx].ClasseDispneaSforzo, ' ');
                    strCallout += "</div>";
                    obj.setTitle(strCallout);
    }
    
    function pulisciDato(val,unita) {
        if ((val == null) || (val == "") || (val == undefined)) { 
         return "NON RILEVATO";
     }
     return val + unita;
    }
    
    </script>
<table width="1100" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td width="10" valign="top" background="./files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center">
    
    <div align="left" style="margin-bottom:10px">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img style="width:100%" src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
                   <div class="modernbricksmenu">
                <ul>
                  <li style="margin-left: 1px">
                  
      <a href="HomePageTipo.aspx?idPaziente=<%=IdPaziente%>">Pannello
                    </a>
                   
                  </li>
                  <li class="current" style="margin-left: 1px">
                    <a href="cercapaziente.aspx">Pazienti</a>
                  </li>

                  <asp:Label ID="lb_gest_utenti" runat="server" Text=""></asp:Label>
                    <li><a href="logout.aspx">Esci</a></li>	
                </ul>
              </div>

             
          </td>
          </tr>
          <tr>
            <td align="center">
                <br />
                <br />
                <span style="font-size:18px;color:#324671;font-weight:bold"><asp:Label ID="lb_titolo" runat="server" Text=""></asp:Label></span>
               
                <uc1:datipaziente ID="Datipaziente1" Visible="false" runat="server" />
            </td>
          </tr>
          <tr>
            <td align="center">
                <br />
                <b>Trends del Paziente <asp:Label runat="server" ID="lblPaziente"></asp:Label></b>
                <br />
                 <div style="width:100%; height:210px;" id="divDashboardValori"></div>
                  <div style="width:100%; height:210px;" id="divDashboardValoriFE"></div>
                    <div style="width:100%; height:210px;" id="divDashboardValoriPP"></div>
                     <div style="width:100%; height:210px;" id="divDashboardValoriIC"></div>
                      <div style="width:100%; height:210px;" id="divDashboardValoriBP"></div>
                       <div style="width:100%; height:210px;" id="divDashboardValoriNY"></div>
                <br /><br />
                <asp:Label ID="lb_steps" runat="server" Text=""></asp:Label>
            </td>
          </tr>
          <tr>
            <td align="center">
                

            </td>
          </tr>
      </table>
    </div>
        

        <br />
        <br />
        <asp:Label ID="lb_mess" runat="server" Text="" ForeColor="Red"></asp:Label>
    
    </td>
    <td width="10" valign="top" background="./files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="./files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="2" style="height: 20px"><!--#include file="include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="./files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
 <%-- <tr>
    <td colspan="3" background="./files/bg_content_bottom.gif">&nbsp;</td>
  </tr>--%>
</table>
</form>
</body>
</html>
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="dativariabilimmg.aspx.cs" Inherits="dativariabilimmg" %>

<%@ Register Src="datipaziente.ascx" TagName="datipaziente" TagPrefix="uc1" %>
<!--#include file="include/controllo.inc"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="stile.css" type="text/css" rel="stylesheet" />
<script language="JavaScript" src="./js/ts_picker.js"></script>
<script language="JavaScript" src="./js/controlli.js"></script>
<title>Gestione Quadri Clinici</title>

<style type="text/css">
<!--
body {
	background-image: url(./files/bg_blueGrad.gif);
	background-color: #324671;
}
.Stile1 {color: #336DD0}
-->
</style>

<script>
    function data_change(field) {
        var check = true;
        var value = field.value; //get characters
        //check that all characters are digits, ., -, or ""
        for (var i = 0; i < field.value.length; ++i) {
            var new_key = value.charAt(i); //cycle through characters
            if (((new_key < "0") || (new_key > "9")) &&
                    !(new_key == "") && !(new_key == ".") && !(new_key == ",")) {
                check = false;
                break;
            }
        }
        //apply appropriate colour based on value
        if (!check) {
            field.style.backgroundColor = "red";
        }
        else {
            field.style.backgroundColor = "white";
        }
    }
                                    
function printDiv(nomeDIV)
{
 var a = window.open('','','width=700,height=600');
 a.document.open("text/html");
 a.document.write(document.getElementById(nomeDIV).innerHTML);
 a.document.close();
 a.print();
}
</script>
</head>

<body>
   <form id="form1" runat="server">
<table width="812" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td width="10" valign="top" background="./files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center"><div align="left">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
              <!--#include file="include/pazienti.inc"-->

                 <div class="modernbricksmenuline">
                <ul>
                    <li style="margin-left: 1px"><a href="nuovopaziente.aspx?IdPaziente=-1">Nuovo<br />
                        Reclutamento</a></li>
                    <li><a href="cercapaziente.aspx">Cerca<br />Paziente</a></li>
                    <li><a href="nuovopaziente.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>">
                        Scheda<br />Paziente</a></li>
                   
                    <li><a href="quadriclinici.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>">
                        Controlli<br />Effettuati</a></li>
                    <li style="display:none;"><a href="dativariabili.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>&idquadro=-1">
                        Nuovo Quadro<br />Clinico</a></li>
                    <asp:Label ID="lb_menu" runat="server" Text=""></asp:Label>
                    
                    
                </ul>
              </div>
          </td>
        </tr>
        <tr>
            <td align="center">
                <br />
                <br />
                <span style="font-size:18px;color:#324671;"><asp:Label ID="lb_titolo" runat="server" Text=""></asp:Label></span>
                <br />
                <br />
                <uc1:datipaziente ID="Datipaziente1" Visible="false" runat="server" />
            </td>
          </tr>
          <tr>
            <td align="center">
                <br />
                <br />
                I campi contrassegnati con * sono obbligatori
                <br /><br />
                <asp:Label ID="lb_steps" runat="server" Text=""></asp:Label>
                 <table><tr style="text-align:center;">
                <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                
                <asp:LinkButton  style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttDettagliControllo" Text="Dettagli Controllo" 
                        onclick="bttDettagliControllo_Click" >  <img src="img/Medical invoice info.png" style="margin:3px;border:0px;" /><br />Dettagli Controllo</asp:LinkButton>
                </td>
                 <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                
                <asp:LinkButton ForeColor="#333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                         runat="server" ID="bttTerapie" Text="Terapie" onclick="bttTerapie_Click">  <img src="img/Pills 5.png" style="margin:3px;border:0px" />&nbsp;
                     <br />Terapia</asp:LinkButton>
                </td>
                <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
               
                <asp:LinkButton ForeColor="#333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttLaboratorio" Text="Esami di Laboratorio" onclick="bttLaboratorio_Click" 
                        > <img src="img/Colba.png" style="margin:3px;border:0px;" />&nbsp;
                    <br />Esami di Laboratorio</asp:LinkButton>
                </td>
             
                 <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
                  
                <asp:LinkButton ForeColor="#333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                         runat="server" ID="bttQdv" Text="QdV" onclick="bttQdv_Click"> <img src="img/My Documents.png" style="margin:3px;border:0px" />&nbsp;
                     <br />QdV</asp:LinkButton>
                </td>
                   <td style="border:1px #cccccc solid; width:100px; height:55px; background-color:#eeeeee; vertical-align:top;">
              
                <asp:LinkButton  ForeColor="#333333" style="font-size:12px; font-weight:bold; text-decoration:none;" 
                        runat="server" ID="bttDiagnosi" Text="Diagnosi" onclick="bttDiagnosi_Click">  <img src="img/Edit Text.png" style="border:0px;margin:3px;" />&nbsp;
                    <br />Diagnosi</asp:LinkButton>
                </td>
                </tr>
                
                </table>
                <br />
                <div visible="false" runat="server" id="panelQdv" style="width:400px; text-align:center; background-color:#eeeeee; border:1px solid #cccccc;">
                    <br />
                    <b style="color:#FF0000;">Attenzione! Il QdV deve essere compilato solo in 
                    occasione del primo e dell'ultimo controllo al paziente. Proseguire con la 
                    visualizzazione del Qdv?<br />
                    </b>
                    <br />
                    <asp:Button runat="server" id="bttYesQdv" Text="Si" onclick="bttYesQdv_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp; 
                    <asp:Button runat="server" id="bttNoWdv" Text="No" onclick="bttNoWdv_Click" />
                    <br />
                    <br />
                    </div>
                <br />
                <asp:Label ID="lblUteQuadroSelected" runat="server" Visible="False"></asp:Label>
                <br /><br />
                  <div style="width:100%; background-color:#eeeeee; height:30px; vertical-align:middle; text-decoration:none; font-size:12px; color:#333333;">
                    <div style="margin-top:8px;"><i><asp:LinkButton ForeColor="#333333" 
                            ID="LinkButton1" runat="server" style="font-weight: 700;" 
                            onclick="LinkButton1_Click">Salva Tutto</asp:LinkButton></i></div>
                    </div>
                <br />
                <br />
                <div style="width:350px; text-align:center; vertical-align:middle;">
                <b>Data del controllo (gg/mm/aaaa) *</b>   
                                        <asp:TextBox runat="server" Columns="10" MaxLength="10" 
                    ID="txtDataControllo"></asp:TextBox>

                                        <a href="javascript:show_calendar('document.form1.txtDataControllo', document.form1.txtDataControllo.value, 'calendar130');">
                                        <img runat="server" id="imgCal" alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a><br />
                                             <div ID="calendar130" style="display: none">
                                        </div>
                <br />
                </div>
                
                <div style="width:350px; text-align:center; vertical-align:middle;">
                <b>Data del PROSSIMO controllo &nbsp; *</b>   
                                        <asp:TextBox runat="server" Columns="10" MaxLength="10" 
                    ID="txtDataProxControllo"></asp:TextBox>

                                        <a href="javascript:show_calendar('document.form1.txtDataProxControllo', document.form1.txtDataProxControllo.value, 'calendar130p');">
                                        <img runat="server" id="img1" alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a><br />
                                             <div ID="calendar130p" style="display: none">
                                        </div>
                <br />
                </div>
                <br />
                <br />
            </td>
          </tr>
          
          
          <tr>
            <td align="center">
                <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0" 
                    OnFinishButtonClick="Wizard1_FinishButtonClick" BackColor="#EFF3FB" 
                    BorderColor="#B5C7DE" BorderWidth="1px" Font-Names="Verdana" Font-Size="12px" 
                    OnActiveStepChanged="Wizard1_ActiveStepChanged" 
                    FinishPreviousButtonText="Indietro" StepPreviousButtonText="Indietro" 
                    DisplaySideBar="False" DisplayCancelButton="false" 
                    NavigationButtonStyle-Height="0" NavigationButtonStyle-Width="0" 
                    NavigationButtonStyle-BorderStyle="None">
                    <StepNavigationTemplate>

<asp:Button ID="StepPreviousButton" runat="server" CausesValidation="False" CommandName="MovePrevious"

Text="Previous" Visible="False" />

<asp:Button ID="StepNextButton" runat="server" CommandName="MoveNext" Text=" s Next"

Visible="False" />

</StepNavigationTemplate>

<FinishNavigationTemplate>

<asp:Button ID="FinishPreviousButton" runat="server" CausesValidation="False" CommandName="MovePrevious"

Text="Previous" Visible="False" />

<asp:Button ID="FinishButton" runat="server" CommandName="MoveComplete" Text="Finish"

Visible="False" />

</FinishNavigationTemplate>
                    <WizardSteps>
                        <asp:WizardStep ID="WizardStep1" runat="server" Title="Dettagli Controllo" 
                            StepType="Start">
                            <div class="titolo_step">Dettagli Controllo</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">Dettagli Controllo<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Cld_Quadro_Clinico_Generale.value='';document.form1.Wizard1$Quadro_Clinico_Obesita[0].checked=true;document.form1.Wizard1$Quadro_Clinico_Obesita[1].checked=false;document.form1.Wizard1$Quadro_Clinico_Obesita[2].checked=false;document.form1.Wizard1$Quadro_Clinico_Sovrappeso[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="top">
                                        &nbsp;</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        
                                        &nbsp;</td>
                                    <td align="left">   
                                        &nbsp; <div id="calendar1" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        <b>Pressione Arteriosa</b></td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Sist.</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)" ID="txtSist" runat="server" Width="70px"></asp:TextBox>
                                    </td>
                                </tr>
             
                                <tr>
                                    <td align="right" valign="middle">
                                        Diast.</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="txtDiast" runat="server" Width="70px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <b>Frequenza Cardiaca<br />
                                        Centrum Cardis</b></td>
                                    <td align="left" width="16" height="16" valign="top">
                                        &nbsp;</td>
                                    <td align="left">   
                                        <asp:TextBox   onblur="data_change(this)"  ID="txtFrequenza" runat="server" Width="70px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Peso Corporeo Kg.</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)"  ID="txtPeso" runat="server" Width="70px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Atti respiratori/min</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        &nbsp;</td>
                                    <td align="left">   
                                        <asp:TextBox   onblur="data_change(this)"  ID="txtAttiResp" runat="server" Width="70px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Congestione Polmonare</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCongestione" Visible="false" runat="server" Width="70px"></asp:TextBox>
                                       <asp:DropDownList runat="server" ID="cmbCongestione">
                                        <asp:ListItem Text=" " Value=" "></asp:ListItem>
                                        <asp:ListItem Text="Lieve" Value="Lieve"></asp:ListItem>
                                         <asp:ListItem Text="Moderata" Value="Moderata"></asp:ListItem>
                                          <asp:ListItem Text="Severa" Value="Severa"></asp:ListItem>
                                       </asp:DropDownList>
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="3" class="titolo_sezione">CONGEST. PERIF.<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Giugulari[1].checked=true;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Epatomegalia[1].checked=true;document.form1.Wizard1$Ddl_Anamnesi_Edemi_Periferici.selectedIndex=0;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Versamento_Pleurico[1].checked=true;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Ascite[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Giugulari</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        &nbsp;<asp:CheckBox runat="server" ID="txtGiugulari_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Epatomegalia</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        &nbsp;<asp:CheckBox runat="server" ID="txtEpatomegalia_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Edemi periferici</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Anamnesi_Edemi_Periferici" runat="server">
                                            <asp:ListItem></asp:ListItem>
                                            <asp:ListItem>+</asp:ListItem>
                                            <asp:ListItem>++</asp:ListItem>
                                            <asp:ListItem>+++</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                       </td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtVersamentoPleurico" Visible="false" runat="server"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ascite</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        &nbsp;<asp:CheckBox runat="server" ID="txtAscite_ch" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                 <tr>
                                    <td colspan="3" class="titolo_sezione">ALTRO<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Giugulari[1].checked=true;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Epatomegalia[1].checked=true;document.form1.Wizard1$Ddl_Anamnesi_Edemi_Periferici.selectedIndex=0;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Versamento_Pleurico[1].checked=true;document.form1.Wizard1$Rbl_Anamnesi_Congestione_Perif_Ascite[1].checked=true;return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diuresi Giornaliera Cc.</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        &nbsp;</td>
                                    <td align="left">   
                                        <div id="calendar4" style="display: none">
                                        </div>
                                        <asp:TextBox   onblur="data_change(this)"  ID="txtDiuresi" runat="server" Width="70px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        <b>Malattie intercorrenti</b></td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diagnosi</td>
                                    <td align="left" width="16" height="16" valign="top">
                                        &nbsp;</td>
                                    <td align="left">   
                                        &nbsp;<div id="calendar5" style="display: none">
                                        </div>
                                        <asp:TextBox ID="txtDiagnosi" runat="server" Height="83px" TextMode="MultiLine" 
                                            Width="221px"></asp:TextBox>
                                    </td>
                                </tr>
                               
                              
                            </table>
                        </asp:WizardStep>
                          <asp:WizardStep ID="WizardStep8" runat="server" Title="Terapie">
                            <div class="titolo_step">TERAPIE</div>
                            <table cellspacing="4" cellpadding="4">
                               
                                <tr id="trEsami1" runat="server" visible="false">
                                    <td align="right" valign="middle">
                                        &nbsp;</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="Ddl_Esami_Strumentali_Ecg_Holter_Classe_NYHA" 
                                            runat="server" Visible="False">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="trEsami2" runat="server" visible="false">
                                    <td align="right" valign="middle">
                                        &nbsp;</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                        &nbsp;</td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Peso1" runat="server" MaxLength="6" 
                                            Columns="10" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" valign="top" colspan="3">
                                            <asp:GridView ID="gv_farmaci" runat="server" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="gv_farmaci_RowDataBound">
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White"  />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" VerticalAlign="Bottom" />
                                            <AlternatingRowStyle BackColor="White" VerticalAlign="Bottom" />
                                            <EditRowStyle BackColor="#2461BF" VerticalAlign="Bottom" />
                                            <Columns>
                                                <asp:BoundField  ItemStyle-VerticalAlign="Bottom" DataField="IdFarmaco" 
                                                    Visible="False" >
<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField  ItemStyle-VerticalAlign="Bottom" DataField="NomeFarmaco" 
                                                    HeaderText="Farmaco" >
<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField  ItemStyle-VerticalAlign="Bottom" DataField="Valore" 
                                                    Visible="True" >
<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField  ItemStyle-VerticalAlign="Bottom" DataField="Intolleranza" 
                                                    Visible="True" >
<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:BoundField  ItemStyle-VerticalAlign="Bottom" DataField="CostoFarmaco" 
                                                    Visible="True" >
<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:BoundField>
                                                <asp:TemplateField ItemStyle-VerticalAlign="Bottom" HeaderText="Valore">
                                                    <ItemTemplate>
                                                        <asp:TextBox   onblur="data_change(this)"  ID="TextBox1" runat="server" Columns="20"></asp:TextBox>
                                                        <asp:RadioButtonList ID="RadioButtonList1" runat="server" RepeatDirection="Horizontal">
                                                            <asp:ListItem Value="0">Si</asp:ListItem>
                                                            <asp:ListItem Selected="True" Value="1">No</asp:ListItem>
                                                        </asp:RadioButtonList>
                                                        
                                                    </ItemTemplate>

<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Intolleranza"  ItemStyle-VerticalAlign="Bottom">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox1" runat="server" />
                                                    </ItemTemplate>

<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Costo Farmaco"  ItemStyle-VerticalAlign="Bottom">
                                                    <ItemTemplate>
                                                        <asp:TextBox   onblur="data_change(this)"  ID="TextBox2" runat="server" Columns="15" ></asp:TextBox>
                                                    </ItemTemplate>

<ItemStyle VerticalAlign="Bottom"></ItemStyle>
                                                </asp:TemplateField>
                                            </Columns>
                                            <RowStyle BackColor="#EFF3FB" VerticalAlign="Bottom" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                        </asp:GridView>
                                        </td>
                                </tr>
                             
                                <tr runat="server" id="trCounc" visible="false">
                                    <td align="right" valign="middle">
                                        Counceling</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:RadioButtonList ID="Rbl_Esami_Strumentali_Counceling" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                </tr>
                                  <tr >
                                    <td align="right" valign="top">
                                        Altro</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNoteTerapie" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trPM" visible="false">
                                    <td class="titolo_sezione" colspan="3">
                                        PM<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Rbl_Aritmia_Pm[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Pm_Causale_Impianto.value='';document.form1.Wizard1$Cld_Aritmie_Pm.value='';document.form1.Wizard1$Txt_Aritmie_Pm_Tipologia.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trPM2" visible="false">
                                    <td align="right" valign="middle">
                                        PM</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Aritmia_Pm" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                         <asp:CheckBox runat="server" ID="Terapia_ch_PM" Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trCausale" visible="false">
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_Causale" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trDataImp1" visible="false">
                                    <td align="right" valign="top">
                                        Data Impianto</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_data_PM" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Terapia_data_PM', document.form1.Wizard1$Terapia_data_PM.value, 'calendar320');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar320" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trTipologia" visible="false">
                                    <td align="right" valign="middle">
                                        Tipologia</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_Tipologia" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trICD" visible="false">
                                    <td class="titolo_sezione" colspan="3">
                                        ICD<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Rbl_Aritmia_Icd[1].checked=true;document.form1.Wizard1$Txt_Aritmie_Causale_Impianto.value='';document.form1.Wizard1$Cld_Aritmie_Icd.value='';document.form1.Wizard1$Txt_Aritmie_Icd_Scariche_Appropriate.value='';document.form1.Wizard1$Txt_Aritmie_Icd_Scariche_Inappropriate.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trICD2" visible="false">
                                    <td align="right" valign="middle">
                                        ICD</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <%/* <asp:RadioButtonList ID="Rbl_Aritmia_Icd" runat="server" 
                                            RepeatDirection="Horizontal">
                                            <asp:ListItem>Si</asp:ListItem>
                                            <asp:ListItem Selected="True">No</asp:ListItem>
                                        </asp:RadioButtonList>*/ %>
                                        <asp:CheckBox runat="server" ID="Terapia_ch_ICD"  Text=""
                                         Checked="false" />
                                    </td>
                                </tr>
                                <tr runat="server" id="trCausale2" visible="false">
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_CausaleImp" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trDataImp2" visible="false">
                                    <td align="right" valign="top">
                                        Data impianto</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_data_impianto" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Terapia_data_impianto', document.form1.Wizard1$Terapia_data_impianto.value, 'calendar330');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar330" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trScariche" visible="false">
                                    <td align="right" valign="middle">
                                        Scariche appropriate</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox ID="Terapia_txt_scaricheApp" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trScariche2" visible="false">
                                    <td align="right" valign="middle">
                                        Scariche inappropriate</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        N.<asp:TextBox ID="Terapia_txt_scaricheInapp" runat="server" 
                                            MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trAblazione" visible="false">
                                    <td align="right" valign="middle">
                                        Ablazione</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_Ablazione" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trPree" visible="false">
                                    <td align="right" valign="middle">
                                        Preeccitazione</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_preeccitazione" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trPMBiv" visible="false">
                                    <td class="titolo_sezione" colspan="3">
                                        PM BIVENTR.<div class="div_reset">
                                            <a href="#" 
                                                onclick="document.form1.Wizard1$Txt_Aritmie_Pm_Biventr.value='';document.form1.Wizard1$Cld_Aritmie_Data_Impianto.value='';document.form1.Wizard1$Ddl_Aritmia_Pm_Biventr.selectedIndex=0;document.form1.Wizard1$Txt_Aritmie_FE_Prima.value='';document.form1.Wizard1$Txt_Aritmie_FE_A_6_Mesi.value='';return false;">
                                            reset</a></div>
                                    </td>
                                </tr>
                                <tr runat="server" id="trCausaleImp" visible="false">
                                    <td align="right" valign="top">
                                        Causale impianto</td>
                                    <td align="left" height="16px" valign="middle" width="16px">
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_txt_CausaleBiventricolare" runat="server" Columns="40" 
                                            MaxLength="255" Rows="5" TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr runat="server" id="trDataImp3" visible="false">
                                    <td align="right" valign="top">
                                        Data impianto</td>
                                    <td align="left" height="16px" valign="top" width="16px">
                                        &nbsp;</td>
                                    <td align="left">
                                        <asp:TextBox ID="Terapia_data_PmBiventricolare" runat="server" Columns="10" 
                                            MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$Terapia_data_PmBiventricolare', document.form1.Wizard1$Terapia_data_PmBiventricolare.value, 'calendar340');">
                                        <img alt="Seleziona la data" border="0" height="16" src="files/cal.gif" 
                                            width="16" /></a>
                                        <div ID="calendar340" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        
                        
                        
                            <asp:WizardStep ID="WizardStep5" runat="server" 
                            Title="Esami di Laboratorio">
                            <div class="titolo_step">LABORATORIO</div>
                            <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">DATI<div class="div_reset"><a href="#" onclick="document.form1.Wizard1$Txt_Laboratorio_Bnp.value='';document.form1.Wizard1$Txt_Laboratorio_Troponina.value='';document.form1.Wizard1$Txt_Laboratorio_Pcr.value='';document.form1.Wizard1$Txt_Laboratorio_Uricemia.value='';document.form1.Wizard1$Txt_Laboratorio_Azot.value='';document.form1.Wizard1$Txt_Laboratorio_Creat.value='';document.form1.Wizard1$Txt_Laboratorio_Microalbumin.value='';document.form1.Wizard1$Txt_Laboratorio_Na.value='';document.form1.Wizard1$Txt_Laboratorio_K.value='';document.form1.Wizard1$Txt_Laboratorio_Ca.value='';document.form1.Wizard1$Txt_Laboratorio_Ph.value='';document.form1.Wizard1$Txt_Laboratorio_Mg.value='';document.form1.Wizard1$Txt_Laboratorio_Got.value='';document.form1.Wizard1$Txt_Laboratorio_Gpt.value='';document.form1.Wizard1$Txt_Laboratorio_Cpk.value='';document.form1.Wizard1$Txt_Laboratorio_Gr.value='';document.form1.Wizard1$Txt_Laboratorio_Emogl.value='';document.form1.Wizard1$Txt_Laboratorio_Gb.value='';document.form1.Wizard1$Txt_Laboratorio_Sideremia.value='';document.form1.Wizard1$Txt_Laboratorio_Glicemia.value='';document.form1.Wizard1$Txt_Laboratorio_Emogl_Glic.value='';document.form1.Wizard1$Txt_Laboratorio_Col_Tot.value='';document.form1.Wizard1$Txt_Laboratorio_Ldl.value='';document.form1.Wizard1$Txt_Laboratorio_Hdl.value='';document.form1.Wizard1$Txt_Laboratorio_Triglic.value='';document.form1.Wizard1$Txt_Laboratorio_T3.value='';document.form1.Wizard1$Txt_Laboratorio_T4.value='';document.form1.Wizard1$Txt_Laboratorio_TSH.value='';document.form1.Wizard1$Txt_Laboratorio_Digitalemia.value='';return false;">
                                        reset</a></div></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PRO BNP</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Bnp" runat="server" MaxLength="255"></asp:TextBox> PG/DL
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TROPONINA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Troponina" runat="server" MaxLength="255"></asp:TextBox> mg/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PCR HS</td>
                                    <td align="left"    width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Pcr" runat="server" MaxLength="255"></asp:TextBox> mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        URICEMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_Uricemia" runat="server" MaxLength="255"></asp:TextBox> mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        AZOT.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Azot" runat="server" MaxLength="255"></asp:TextBox> mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CREAT.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Creat" runat="server" MaxLength="255"></asp:TextBox> mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Albuminuria</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Microalbumin" runat="server" MaxLength="255"></asp:TextBox> mg/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        NA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Na" runat="server" MaxLength="255"></asp:TextBox> mEq/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        K</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_K" runat="server" MaxLength="255"></asp:TextBox> mEq/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Ca" runat="server" MaxLength="255"></asp:TextBox> mEq/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PH</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Ph" runat="server" MaxLength="255"></asp:TextBox> mEq/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        MG</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Mg" runat="server" MaxLength="255"></asp:TextBox> mEq/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GOT</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_Got" runat="server" MaxLength="255"></asp:TextBox> IU/L
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GPT</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_Gpt" runat="server" MaxLength="255"></asp:TextBox> IU/L
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        CPK MB</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Cpk" runat="server" MaxLength="255"></asp:TextBox> IU/L
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GR</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Gr" runat="server" MaxLength="255"></asp:TextBox> M/&#956L
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        EMOGL.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Emogl" runat="server" MaxLength="255"></asp:TextBox> gr/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GB</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Gb" runat="server" MaxLength="255"></asp:TextBox> K/&#956l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        SIDEREMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Sideremia" runat="server" MaxLength="255"></asp:TextBox> mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        GLICEMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Glicemia" runat="server" MaxLength="255"></asp:TextBox> mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Piastrine</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_Piastrine" runat="server" MaxLength="255"></asp:TextBox> 10<sup>9</sup>/l
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Ferritina</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Ferritina" runat="server" MaxLength="255"></asp:TextBox> &#956g/ml
                                    </td>
                                </tr>
                             
                                <tr>
                                    <td align="right" valign="middle">
                                        EMOGL. GLIC.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Emogl_Glic" runat="server" MaxLength="255"></asp:TextBox> %
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        COL. TOT.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_Col_Tot" runat="server" MaxLength="255"></asp:TextBox> mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        LDL</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ValidationExpression="\d*" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$Txt_Laboratorio_Ldl.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire un valore numerico per il campo LDL</div></span>" ControlToValidate="Txt_Laboratorio_Ldl"></asp:RegularExpressionValidator>
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_Ldl" runat="server" MaxLength="255"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        HDL</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Hdl" runat="server" MaxLength="255"></asp:TextBox> mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TRIGLIC.</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_Triglic" runat="server" MaxLength="255"></asp:TextBox> mg/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                       F T3</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_T3" runat="server" MaxLength="255"></asp:TextBox> pg/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                       F T4</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_T4" runat="server" MaxLength="255"></asp:TextBox> &#956g/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        TSH</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_TSH" runat="server" MaxLength="255"></asp:TextBox>&#956v/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        DIGITALEMIA</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_Digitalemia" runat="server" MaxLength="255"></asp:TextBox> ng/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        PTH</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="txtATH" runat="server" MaxLength="255"></asp:TextBox> pg/ml
                                    </td>
                                </tr>
                                 <tr>
                                    <td align="right" valign="middle">
                                        ADH</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_laboratorio_adh" runat="server" MaxLength="255"></asp:TextBox> pg/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Albumina</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_laboratorio_albumina" runat="server" MaxLength="255"></asp:TextBox> g/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Aldosterone</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_laboratorio_aldosterone" runat="server" MaxLength="255"></asp:TextBox>ng/dl
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Vit.D</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox  onblur="data_change(this)"  ID="Txt_Laboratorio_VitD" runat="server" MaxLength="255"></asp:TextBox> pg/ml
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Epinefrina</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox   onblur="data_change(this)" ID="Txt_Laboratorio_Epinefrina" runat="server" MaxLength="255"></asp:TextBox> nmol/d
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                        <asp:WizardStep ID="WizardStep3" runat="server" Title="QdV" 
                            StepType="Finish">
                            <div style="width:100%; text-align:center;"> <a href="javascript:printDiv('divMinnesota')">STAMPA QUESTIONARIO</a> </div>
                           
                            <div  class="titolo_step">QUESTIONARIO MINNESOTA</div> <div id="divMinnesota">
                              <table cellspacing="4" cellpadding="4">
                                <tr>
                                    <td colspan="6" class="titolo_sezione">QUESTIONARIO MINNESOTA</td>
                                </tr>
                                <tr><td colspan="6"><b>Ha avuto gonfiore agli arti inferiori (gambe, caviglie)?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota1" Text="0 - No" ID="cmbMinnesota1_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota1" Text="1 - Minimante" ID="cmbMinnesota1_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota1" Text="2 - Molto Poco" ID="cmbMinnesota1_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota1" Text="3 - Poco" ID="cmbMinnesota1_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota1" Text="4 - Molto" ID="cmbMinnesota1_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota1" Text="5 - Moltissimo" ID="cmbMinnesota1_5" runat="server" />
                                       
                                    </td>
                                </tr>
                                <tr><td colspan="6"><b>Deve rimanere a riposo (coricato o seduto) durante il giorno?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota2" Text="0 - No" ID="cmbMinnesota2_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota2" Text="1 - Minimante" ID="cmbMinnesota2_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota2" Text="2 - Molto Poco" ID="cmbMinnesota2_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota2" Text="3 - Poco" ID="cmbMinnesota2_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota2" Text="4 - Molto" ID="cmbMinnesota2_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota2" Text="5 - Moltissimo" ID="cmbMinnesota2_5" runat="server" />
                                       
                                    </td>
                                </tr>
                                  <tr><td colspan="6"><b>Passeggia o sale le scale con difficolt�?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota3"  Text="0 - No" ID="cmbMinnesota3_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota3" Text="1 - Minimante" ID="cmbMinnesota3_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota3" Text="2 - Molto Poco" ID="cmbMinnesota3_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota3" Text="3 - Poco" ID="cmbMinnesota3_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota3" Text="4 - Molto" ID="cmbMinnesota3_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota3" Text="5 - Moltissimo" ID="cmbMinnesota3_5" runat="server" />
                                       
                                    </td>
                                </tr>
                                <tr><td colspan="6"><b>Svolge i piccoli lavori domestici con difficolt�?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota4" Text="0 - No" ID="cmbMinnesota4_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota4" Text="1 - Minimante" ID="cmbMinnesota4_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota4" Text="2 - Molto Poco" ID="cmbMinnesota4_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota4" Text="3 - Poco" ID="cmbMinnesota4_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota4" Text="4 - Molto" ID="cmbMinnesota4_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota4" Text="5 - Moltissimo" ID="cmbMinnesota4_5" runat="server" />
                                       
                                    </td>
                                </tr>
                                <tr><td colspan="6"><b>Esce di casa con difficolt�?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota5" Text="0 - No" ID="cmbMinnesota5_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota5" Text="1 - Minimante" ID="cmbMinnesota5_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota5" Text="2 - Molto Poco" ID="cmbMinnesota5_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota5" Text="3 - Poco" ID="cmbMinnesota5_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota5" Text="4 - Molto" ID="cmbMinnesota5_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota5" Text="5 - Moltissimo" ID="cmbMinnesota5_5" runat="server" />
                                       
                                    </td>
                                </tr>
                                <tr><td colspan="6"><b>Riposa male di notte?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota6" Text="0 - No" ID="cmbMinnesota6_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota6" Text="1 - Minimante" ID="cmbMinnesota6_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota6" Text="2 - Molto Poco" ID="cmbMinnesota6_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota6" Text="3 - Poco" ID="cmbMinnesota6_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota6" Text="4 - Molto" ID="cmbMinnesota6_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota6" Text="5 - Moltissimo" ID="cmbMinnesota6_5" runat="server" />
                                       
                                    </td>
                                </tr>
                                <tr><td colspan="6"><b>Svolge con difficolt� le attivit� di relazione con familiare 
                                    o amici?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota7" Text="0 - No" ID="cmbMinnesota7_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota7" Text="1 - Minimante" ID="cmbMinnesota7_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota7" Text="2 - Molto Poco" ID="cmbMinnesota7_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota7" Text="3 - Poco" ID="cmbMinnesota7_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota7" Text="4 - Molto" ID="cmbMinnesota7_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota7" Text="5 - Moltissimo" ID="cmbMinnesota7_5" runat="server" />
                                       
                                    </td>
                                </tr>
                                <tr><td colspan="6"><b>Svolge con difficolt� le attivit� lavorative?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota8" Text="0 - No" ID="cmbMinnesota8_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota8" Text="1 - Minimante" ID="cmbMinnesota8_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota8" Text="2 - Molto Poco" ID="cmbMinnesota8_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota8" Text="3 - Poco" ID="cmbMinnesota8_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota8" Text="4 - Molto" ID="cmbMinnesota8_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota8" Text="5 - Moltissimo" ID="cmbMinnesota8_5" runat="server" />
                                       
                                    </td>
                                </tr>
                                 <tr><td colspan="6"><b>Svolge con difficolt� attivit� nel tempo libero 
                                     (sport,hobby)?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota9" Text="0 - No" ID="cmbMinnesota9_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota9" Text="1 - Minimante" ID="cmbMinnesota9_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota9" Text="2 - Molto Poco" ID="cmbMinnesota9_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota9" Text="3 - Poco" ID="cmbMinnesota9_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota9" Text="4 - Molto" ID="cmbMinnesota9_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota9" Text="5 - Moltissimo" ID="cmbMinnesota9_5" runat="server" />
                                       
                                    </td>
                                </tr>
                                 <tr><td colspan="6"><b>Svolge attivit� sessuale con difficolt�?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota10" Text="0 - No" ID="cmbMinnesota10_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota10" Text="1 - Minimante" ID="cmbMinnesota10_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota10" Text="2 - Molto Poco" ID="cmbMinnesota10_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota10" Text="3 - Poco" ID="cmbMinnesota10_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota10" Text="4 - Molto" ID="cmbMinnesota10_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota10" Text="5 - Moltissimo" ID="cmbMinnesota10_5" runat="server" />
                                       
                                    </td>
                                </tr>
                                <tr><td colspan="6"><b>Ha dovuto eliminare i cibi preferiti?</b></td></tr>
                                <tr>
                                    <td align="right" valign="top">
                                        <asp:RadioButton GroupName="Minnesota11" Text="0 - No" ID="cmbMinnesota11_0" runat="server" /></td>
                                    <td align="left" height="16px" valign="top" class="style2">
                                       <asp:RadioButton GroupName="Minnesota11" Text="1 - Minimante" ID="cmbMinnesota11_1" runat="server" /></td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota11" Text="2 - Molto Poco" ID="cmbMinnesota11_2" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota11" Text="3 - Poco" ID="cmbMinnesota11_3" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota11" Text="4 - Molto" ID="cmbMinnesota11_4" runat="server" />
                                       
                                    </td>
                                    <td align="left">   
                                          <asp:RadioButton GroupName="Minnesota11" Text="5 - Moltissimo" ID="cmbMinnesota11_5" runat="server" />
                                       
                                    </td>
                                </tr>
                            </table></div>
                            <br />
                          
                        </asp:WizardStep>
                          <asp:WizardStep ID="WizardStep10" runat="server" Title="Diagnosi">
                        <table>
                         <tr>
                                    <td colspan="3" class="titolo_sezione">DIAGNOSI <asp:Label ID="lb_gv" runat="server" Text=""></asp:Label></td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Diagnosi</td>
                                    <td align="left" width="16" height="16" valign="middle">
                                    
                                    </td>
                                    <td align="left">
                                        <asp:TextBox ID="Txt_Esami_Strumentali_Diagnosi" runat="server" MaxLength="255" TextMode="MultiLine" Columns="40" Rows="5"></asp:TextBox>
                                    </td>
                                </tr>
                        </table>
                        </asp:WizardStep>
                        
                        
                    </WizardSteps>
                    
                    <StepStyle Font-Size="0.8em" ForeColor="#333333" />

<NavigationButtonStyle BorderStyle="None" Height="0px" Width="0px"></NavigationButtonStyle>

                    <SideBarStyle BackColor="#507CD1" Font-Size="0.9em" VerticalAlign="Top" />
                 
                    <SideBarButtonStyle BackColor="#507CD1" Font-Names="Verdana" ForeColor="White" />
                    <HeaderStyle BackColor="#284E98" BorderColor="#EFF3FB" BorderStyle="Solid" BorderWidth="2px"
                        Font-Bold="True" Font-Size="0.9em" ForeColor="White" HorizontalAlign="Center" />
                </asp:Wizard>
                
            </td>
          </tr>
      </table>
    </div>
        <br />
         <br />
                  <div style="width:100%; background-color:#eeeeee; height:30px; vertical-align:middle; text-decoration:none; font-size:12px; color:#333333;">
                    <div style="margin-top:8px;"><i><asp:LinkButton ForeColor="#333333" 
                            ID="LinkButton2" runat="server" style="font-weight: 700;" 
                            onclick="LinkButton1_Click">Salva Tutto</asp:LinkButton></i></div>
                    </div>
                <br />
        <br />
        <asp:Label ID="lb_mess" runat="server" Text="" ForeColor="Red"></asp:Label>
    
    </td>
    <td width="10" valign="top" background="./files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="./files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="3" style="height: 20px"><!--#include file="include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="./files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" background="./files/bg_content_bottom.gif">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>

﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Collections;

public partial class statisticheDeaths : System.Web.UI.Page
{
    protected int numPazienti = 30;
    protected int numPazientiM = 20;
    protected int numPazientiF = 10;
    protected int numPazientiD;
    protected int numPazientiV;
    protected int numPazientiDiabete = 0;
    protected int numPazientiMDiabete = 20;
    protected int numPazientiFDiabete = 10;
    protected int numPazientiIperTens = 0;
    protected int numPazientiIperTensM = 0;
    protected int numPazientiIperTensF = 0;
    private string conn_str;
    protected ListItemCollection lic;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        lic = new ListItemCollection();
        if (!IsPostBack)
        {

            GeneraStatisticheGenerali();
            GeneraStatisticheDiabete();

            //GeneraStatisticheValori("BBS", " and ECGBBS='1' ");
            //GeneraStatisticheValori("BBD", " and ECGBBD='1' ");
            //GeneraStatisticheValori("Ischemia", " and ECGIschemia='1' ");
            GeneraAltreStatistiche("BPCO", "select count(Paziente.IdPaziente) as NumCasiBPCO from FattoreRischio,Paziente,QuadroClinico where FattoreRischio.IdPaziente=Paziente.IdPaziente  and rpco='Si' and QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and ISNULL(gDead,'') != '';" +
                "select count(Paziente.IdPaziente) as NumCasiBPCO from FattoreRischio,Paziente,QuadroClinico where FattoreRischio.IdPaziente=Paziente.IdPaziente  and rpco='Si' and QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and Sesso='M' and ISNULL(gDead,'') != '';" +
                "select count(Paziente.IdPaziente) as NumCasiBPCO from FattoreRischio,Paziente,QuadroClinico where FattoreRischio.IdPaziente=Paziente.IdPaziente  and rpco='Si' and QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and Sesso='F' and ISNULL(gDead,'') != '';");
            GeneraAltreStatistiche("Valvolopatie", "Select Count(Paziente.idPaziente) from Valvolopatia,Paziente where Valvolopatia.idPaziente=Paziente.idPaziente and DataPrimoRilievo != '' and ISNULL(gDead,'') != '';" +
                "Select Count(Paziente.idPaziente) from Valvolopatia,Paziente where Valvolopatia.idPaziente=Paziente.idPaziente and DataPrimoRilievo != '' and Sesso='M' and ISNULL(gDead,'') != '';;" +
                "Select Count(Paziente.idPaziente) from Valvolopatia,Paziente where Valvolopatia.idPaziente=Paziente.idPaziente and DataPrimoRilievo != '' and Sesso='F' and ISNULL(gDead,'') != '';;");
            GeneraAltreStatistiche("CardiomiopatiaDilatativa", "Select Count(Paziente.idPaziente) from CardioMioPatiaDilatativa,Paziente where CardioMioPatiaDilatativa.idPaziente=Paziente.idPaziente and DataPrimoRilievo != '' and ISNULL(gDead,'') != '';;" +
                "Select Count(Paziente.idPaziente) from CardioMioPatiaDilatativa,Paziente where CardioMioPatiaDilatativa.idPaziente=Paziente.idPaziente and DataPrimoRilievo != '' and Sesso='M' and ISNULL(gDead,'') != '';;" +
                "Select Count(Paziente.idPaziente) from CardioMioPatiaDilatativa,Paziente where CardioMioPatiaDilatativa.idPaziente=Paziente.idPaziente and DataPrimoRilievo != '' and Sesso='F' and ISNULL(gDead,'') != '';;");

            GeneraAltreStatistiche("IpertensivaValvolare", "Select Count(Paziente.idPaziente) from CardioMioPatiaDilatativa,Paziente,Valvolopatia " +
                    "where CardioMioPatiaDilatativa.idPaziente=Paziente.idPaziente and CardioMioPatiaDilatativa.DataPrimoRilievo != '' " +
                    "and valvolopatia.IdPaziente = Paziente.idPaziente and valvolopatia.DataPrimoRilievo != '' and ISNULL(gDead,'') != '';;" +
                    "Select Count(Paziente.idPaziente) from CardioMioPatiaDilatativa,Paziente,Valvolopatia " +
                    "where CardioMioPatiaDilatativa.idPaziente=Paziente.idPaziente and CardioMioPatiaDilatativa.DataPrimoRilievo != '' " +
                    "and valvolopatia.IdPaziente = Paziente.idPaziente and valvolopatia.DataPrimoRilievo != '' and Sesso='M' and ISNULL(gDead,'') != '';;" +
                    "Select Count(Paziente.idPaziente) from CardioMioPatiaDilatativa,Paziente,Valvolopatia " +
                    "where CardioMioPatiaDilatativa.idPaziente=Paziente.idPaziente and CardioMioPatiaDilatativa.DataPrimoRilievo != '' " +
                    "and valvolopatia.IdPaziente = Paziente.idPaziente and valvolopatia.DataPrimoRilievo != '' and Sesso='F' and ISNULL(gDead,'') != '';;");

            GeneraAltreStatistiche("IschemiaValvolare", "Select count(Paziente.idPaziente) from Paziente,QuadroClinico,Valvolopatia where " +
                "QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and " +
                " (Paziente.idPaziente IN (Select idPaziente from CardiopIschemNOIMA where DataPrimoRilievo <>'' )  " +
                "or Paziente.idPaziente IN (Select idPaziente from IMA where IMA.Data <>'') ) " +
                "and valvolopatia.idPaziente=Paziente.idPaziente and Valvolopatia.DataPrimoRilievo!='' and ISNULL(gDead,'') != '';; " +

                "Select count(Paziente.idPaziente) from Paziente,QuadroClinico,Valvolopatia where  " +
                "QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and " +
                " (Paziente.idPaziente IN (Select idPaziente from CardiopIschemNOIMA where DataPrimoRilievo <>'' )  " +
                "or Paziente.idPaziente IN (Select idPaziente from IMA where IMA.Data <>'') ) " +
                "and valvolopatia.idPaziente=Paziente.idPaziente and Valvolopatia.DataPrimoRilievo!='' and Sesso='M' and ISNULL(gDead,'') != '';; " +

                "Select count(Paziente.idPaziente) from Paziente,QuadroClinico,Valvolopatia where  " +
                "QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and " +
                " (Paziente.idPaziente IN (Select idPaziente from CardiopIschemNOIMA where DataPrimoRilievo <>'' )  " +
                "or Paziente.idPaziente IN (Select idPaziente from IMA where IMA.Data <>'') ) " +
                "and valvolopatia.idPaziente=Paziente.idPaziente and Valvolopatia.DataPrimoRilievo!='' and Sesso='F' and ISNULL(gDead,'') != '';; "

                                );

            GeneraAltreStatistiche("Anemia", "select count(Paziente.IdPaziente) as NumCasiAnemia from QuadroClinico,Paziente,FattoreRischio where FattoreRischio.idPaziente=Paziente.idPaziente and ANEMIAHB='1' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and Sesso='F' and ISNULL(gDead,'') != '';;" +
            "select count(Paziente.IdPaziente) as NumCasiAnemia from QuadroClinico,Paziente,FattoreRischio where FattoreRischio.idPaziente=Paziente.idPaziente and ANEMIAHB='1' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and Sesso='M'  and Sesso='F' and ISNULL(gDead,'') != '';;" +
            "select count(Paziente.IdPaziente) as NumCasiAnemia from QuadroClinico,Paziente,FattoreRischio where FattoreRischio.idPaziente=Paziente.idPaziente and ANEMIAHB='1' and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and Sesso='F'  and Sesso='F' and ISNULL(gDead,'') != '';;");

            GeneraAltreStatistiche("IPERDISLIPIDEMIA", "select count(Paziente.IdPaziente) as NumCasiAnemia from QuadroClinico,Paziente,FattoreRischio where FattoreRischio.idPaziente=Paziente.idPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and (LDLDopoMag130='True' or LDLDopo100tra130='True' or HDLMin35='Si' or  TriglMag170='1') and Sesso='F' and ISNULL(gDead,'') != '';; " +
            "select count(Paziente.IdPaziente) as NumCasiAnemia from QuadroClinico,Paziente,FattoreRischio where FattoreRischio.idPaziente=Paziente.idPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and (LDLDopoMag130='True' or LDLDopo100tra130='True' or HDLMin35='Si' or  TriglMag170='1') and Sesso='M'  and Sesso='F' and ISNULL(gDead,'') != ''; " +
            "select count(Paziente.IdPaziente) as NumCasiAnemia from QuadroClinico,Paziente,FattoreRischio where FattoreRischio.idPaziente=Paziente.idPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente  and gtipo='RECLUTAMENTO' and (LDLDopoMag130='True' or LDLDopo100tra130='True' or HDLMin35='Si' or  TriglMag170='1') and Sesso='F'  and Sesso='F' and ISNULL(gDead,'') != ''; ");

            GeneraAltreStatistiche("Ischemia", "Select count(Paziente.idPaziente) from Paziente,QuadroClinico where QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and (Paziente.idPaziente IN (Select idPaziente from CardiopIschemNOIMA where DataPrimoRilievo <>'' ) or Paziente.idPaziente IN (Select idPaziente from IMA where IMA.Data <>'') ) and Sesso='F' and ISNULL(gDead,'') != '';" +
                "Select count(Paziente.idPaziente) from Paziente,QuadroClinico where QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and (Paziente.idPaziente IN (Select idPaziente from CardiopIschemNOIMA where DataPrimoRilievo <>'' ) or Paziente.idPaziente IN (Select idPaziente from IMA where IMA.Data <>'') ) and Sesso='M'  and Sesso='F' and ISNULL(gDead,'') != '';" +
                "Select count(Paziente.idPaziente) from Paziente,QuadroClinico where QuadroClinico.idPaziente=Paziente.idPaziente and gTipo='RECLUTAMENTO' and (Paziente.idPaziente IN (Select idPaziente from CardiopIschemNOIMA where DataPrimoRilievo <>'' ) or Paziente.idPaziente IN (Select idPaziente from IMA where IMA.Data <>'') ) and Sesso='F'  and Sesso='F' and ISNULL(gDead,'') != '';");
            //GeneraStatisticheValori("BAV", " and BAV <>'' ");
            GeneraStatisticheValori("Cardiomiopatia", " and Cardiomiopatia='True'  and Sesso='F' and ISNULL(gDead,'') != '' ");
            GeneraStatisticheValori("Ipertrofia", "  and (ECGIpertVentrSx = '1' or Eco2DIpertrSet <> '' or Eco2DIpertrConc <> '' )  and Sesso='F' and ISNULL(gDead,'') != '' ");
            GeneraStatisticheValori("Ipocinesia", "  and Eco2DIpocinesia <>''  and Sesso='F' and ISNULL(gDead,'') != '' ");
            GeneraStatisticheValori("FE", " and (CONVERT(int,Eco2DFE) < 35)  and Sesso='F' and ISNULL(gDead,'') != '' ");
            GeneraStatisticheValori("FEIntervallo", " and (CONVERT(int,Eco2DFE) >= 35 and CONVERT(int,Eco2DFE) <= 50)  and Sesso='F' and ISNULL(gDead,'') != '' ");
            GeneraStatisticheValori("PressionePolmonare", " and CONVERT(int,EcoDopplerPressPolm) > 35  and Sesso='F' and ISNULL(gDead,'') != '' ");
            //GeneraStatisticheValori("DisfunzioneDiastolica", " and EcoDopplerDisfDiast <> 'Normale' and EcoDopplerDisfDiast <> ''");
            GeneraStatisticheValori("DisfunzioneDiastolica", " and EcoDopplerDisfDiast = 'Alterato rilasciamento' and (CONVERT(int,Eco2DFE) >= 50) and EcoDopplerDisfDiast <> ''  and Sesso='F' and ISNULL(gDead,'') != ''");
            GeneraAltreStatistiche("IpertensioneArteriosa", "select count(Paziente.IdPaziente) as NumCasiIpert from FattoreRischio,Paziente,QuadroClinico where TipoIperArt!='' and FattoreRischio.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO'  and Sesso='F' and ISNULL(gDead,'') != '';" +
                 " select count(Paziente.IdPaziente) as NumCasiIpertM from FattoreRischio,Paziente,QuadroClinico where TipoIperArt!='' and Sesso='M' and FattoreRischio.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO'  and Sesso='F' and ISNULL(gDead,'') != '';" +
                 " select count(Paziente.IdPaziente) as NumCasiIpertF from FattoreRischio,Paziente,QuadroClinico where TipoIperArt!='' and Sesso='F' and FattoreRischio.IdPaziente=Paziente.IdPaziente and QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO'  and Sesso='F' and ISNULL(gDead,'') != '';");
            GeneraAltreStatistiche("InsufficienzaRenale", "select count(Paziente.IdPaziente) as NumCasiInsuffRenale from QuadroClinico,Paziente where  QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and cast(REPLACE(ISNULL(IngrCreat,'0'),',','.') as float) > 1.4  and Sesso='F' and ISNULL(gDead,'') != '';" +
                " select count(Paziente.IdPaziente) as NumCasiInsuffRenaleM from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and gTipo='RECLUTAMENTO' and cast(REPLACE(ISNULL(IngrCreat,'0'),',','.') as float) > 1.4 and Sesso='M'  and Sesso='F' and ISNULL(gDead,'') != '';" +
                " select count(Paziente.IdPaziente) as NumCasiInsuffRenaleF from QuadroClinico,Paziente where QuadroClinico.IdPaziente=Paziente.IdPaziente and  gTipo='RECLUTAMENTO' and cast(REPLACE(ISNULL(IngrCreat,'0'),',','.') as float) > 1.4 and Sesso='F'  and Sesso='F' and ISNULL(gDead,'') != '';");
            GeneraStatisticheValori("NYHA", " and (ClasseDispneaSforzo = 'III classe' or ClasseDispneaSforzo = 'IV classe')  and Sesso='F' and ISNULL(gDead,'') != ''");
            generaMorti();
        }

    }
    protected void generaMorti()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {

            connection.Open();
            SqlCommand command = new SqlCommand("Select Count(idPaziente) from Paziente where ISNULL(gDead,'') != ''; Select Count(idPaziente) from Paziente where ISNULL(gDead,'') = ''", connection);
            command.CommandType = CommandType.Text;
            SqlDataAdapter reader = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            reader.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                numPazientiD = int.Parse(ds.Tables[0].Rows[0][0].ToString());
                //Response.Write(numPazientiD);
            }
            if (ds.Tables[1].Rows.Count > 0)
            {
                numPazientiV = int.Parse(ds.Tables[1].Rows[0][0].ToString());
                //Response.Write(numPazientiV);
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            connection.Close();
        }
    }
    protected void GeneraAltreStatistiche(string name, string query)
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {

            connection.Open();
            SqlCommand command = new SqlCommand(query, connection);
            command.CommandType = CommandType.Text;
            SqlDataAdapter reader = new SqlDataAdapter(command);
            DataSet ds = new DataSet();
            reader.Fill(ds);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ListItem li = new ListItem(name, ds.Tables[0].Rows[0][0] + "-" + ds.Tables[1].Rows[0][0] + "-" + ds.Tables[2].Rows[0][0]);
                lic.Add(li);
            }

        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
        }
        finally
        {
            connection.Close();
        }
    }
    protected void GeneraStatisticheDiabete()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            int num;
            int num2;
            int num3;
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_diabete_deaths", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            if (reader["NumCasiDiabete"].ToString() != "")
            {
                num = Convert.ToInt32(reader["NumCasiDiabete"]);

            }
            else
            {
                num = 0;
            }
            numPazientiDiabete = num;
            if (this.numPazienti != 0)
            {
                double num4 = (num * 100) / this.numPazienti;

            }

            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteM"].ToString() != "")
            {
                num2 = Convert.ToInt32(reader["NumCasiDiabeteM"]);
            }
            else
            {
                num2 = 0;
            }
            numPazientiMDiabete = num2;
            if (num != 0)
            {
                double num5 = (num2 * 100) / num;

            }

            reader.NextResult();
            reader.Read();
            if (reader["NumCasiDiabeteF"].ToString() != "")
            {
                num3 = Convert.ToInt32(reader["NumCasiDiabeteF"]);
            }
            else
            {
                num3 = 0;
            }
            numPazientiFDiabete = num3;
            if (num != 0)
            {
                double num6 = (num3 * 100) / num;

            }
            ListItem li = new ListItem("Diabete", num + "-" + num2 + "-" + num3);
            lic.Add(li);
            reader.NextResult();
            reader.Read();

            reader.NextResult();
            reader.Read();

            reader.NextResult();
            reader.Read();

            reader.NextResult();
            reader.Read();

            reader.NextResult();
            reader.Read();

            reader.NextResult();
            reader.Read();

            reader.NextResult();
            reader.Read();

            reader.NextResult();
            reader.Read();

            reader.NextResult();
            reader.Read();

        }
        catch (Exception exception)
        {
            base.Response.Write(exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }

    protected void GeneraStatisticheValori(string campo, string condizioni)
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command2 = new SqlCommand("statistiche_aritmie", connection);
            command2.CommandType = CommandType.StoredProcedure;
            command2.Parameters.AddWithValue("@Condizioni", condizioni);
            //Response.Write("CONDIZIONI:" + str +  "<br/>");
            SqlDataReader reader = command2.ExecuteReader();
            reader.Read();
            int num = Convert.ToInt32(reader["NumTot"]);

            reader.NextResult();
            reader.Read();
            int num2 = Convert.ToInt32(reader["NumM"]);
            if (num != 0)
            {
                double num46 = (num2 * 100) / num;

            }

            reader.NextResult();
            reader.Read();
            int num3 = Convert.ToInt32(reader["NumF"]);
            reader.Close();
            if (campo == "DisfunzioneDiastolica") { }
            //Response.Write(num + "-" + num2 + "-" + num3);
            ListItem li = new ListItem(campo, num + "-" + num2 + "-" + num3);
            lic.Add(li);
        }
        catch (Exception exception)
        {
            base.Response.Write("ERRORE " + exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }
    protected void GeneraStatisticheGenerali()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("statistiche_generali", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            reader.Read();
            int num = Convert.ToInt32(reader["NumTotale"]);
            numPazienti = num;
            //this.lb_pazienti.Text = "Pazienti: <b>" + num.ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            int num2 = Convert.ToInt32(reader["NumMaschi"]);
            numPazientiM = num2;
            if (num != 0)
            {
                double num19 = (num2 * 100) / num;
                //string text = this.lb_pazienti.Text;
                //this.lb_pazienti.Text = text + "Maschi: <b>" + num2.ToString() + "</b> (" + num19.ToString() + "%)<br />";
            }
            else
            {
                //this.lb_pazienti.Text = this.lb_pazienti.Text + "Maschi: <b>" + num2.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            int num3 = Convert.ToInt32(reader["NumFemmine"]);
            numPazientiF = num3;
            if (num != 0)
            {
                double num20 = (num3 * 100) / num;
                // string str2 = this.lb_pazienti.Text;
                //this.lb_pazienti.Text = str2 + "Femmine: <b>" + num3.ToString() + "</b> (" + num20.ToString() + "%)<br />";
            }
            else
            {
                //this.lb_pazienti.Text = this.lb_pazienti.Text + "Femmine: <b>" + num3.ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (num != 0)
            {
                //    this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 media pazienti: <b>" + ((double)(Convert.ToInt32(reader["SumEta"]) / num)).ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (num2 != 0)
            {
                //this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 media M: <b>" + ((double)(Convert.ToInt32(reader["SumEtaM"]) / num2)).ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            if (num3 != 0)
            {
                //this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 media F: <b>" + ((double)(Convert.ToInt32(reader["SumEtaF"]) / num3)).ToString() + "</b><br />";
            }
            reader.NextResult();
            reader.Read();
            //this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 minima: <b>" + reader["EtaMin"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            //this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 massima: <b>" + reader["EtaMax"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            //this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 minima M: <b>" + reader["EtaMinM"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            //this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 massima M: <b>" + reader["EtaMaxM"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            //this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 minima F: <b>" + reader["EtaMinF"].ToString() + "</b><br />";
            reader.NextResult();
            reader.Read();
            //this.lb_pazienti.Text = this.lb_pazienti.Text + "Et\x00e0 massima F: <b>" + reader["EtaMaxF"].ToString() + "</b><br />";
            //this.lb_pazienti.Text = this.lb_pazienti.Text + "<br />";
            if (num != 0)
            {
                object obj2;
                reader.NextResult();
                reader.Read();
                int num4 = Convert.ToInt32(reader["Fumo"]);
                double num24 = (Convert.ToDouble(num4) * Convert.ToDouble(100)) / Convert.ToDouble(num);
                // this.lb_fumatori.Text = string.Concat(new object[] { "Fumatori: <b>", num4.ToString(), "</b><br />(", Math.Round(num24, 2), "% dei pazienti)<br />" });
                reader.NextResult();
                reader.Read();
                int num5 = Convert.ToInt32(reader["FumoM"]);
                if (num4 != 0)
                {
                    double num25 = (Convert.ToDouble(num5) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    // obj2 = this.lb_fumatori.Text;
                    // this.lb_fumatori.Text = string.Concat(new object[] { obj2, "Maschi: <b>", num5.ToString(), "</b> (", Math.Round(num25, 2), "%)<br />" });
                }
                else
                {
                    // this.lb_fumatori.Text = this.lb_fumatori.Text + "Maschi: <b>" + num5.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num6 = Convert.ToInt32(reader["FumoF"]);
                if (num4 != 0)
                {
                    double num26 = (Convert.ToDouble(num6) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    //object obj3 = this.lb_fumatori.Text;
                    //this.lb_fumatori.Text = string.Concat(new object[] { obj3, "Femmine: <b>", num6.ToString(), "</b> (", Math.Round(num26, 2), "%)<br />" });
                }
                else
                {
                    // this.lb_fumatori.Text = this.lb_fumatori.Text + "Femmine: <b>" + num6.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num7 = Convert.ToInt32(reader["Fumo+"]);
                //this.lb_fumatori1.Text = "Fumatori +: <b>" + num7.ToString() + "</b><br />";
                if (num4 != 0)
                {
                    double num27 = (Convert.ToDouble(num7) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    //object obj4 = this.lb_fumatori1.Text;
                    //this.lb_fumatori1.Text = string.Concat(new object[] { obj4, "(", Math.Round(num27, 2), "% dei fumatori)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num8 = Convert.ToInt32(reader["Fumo+M"]);
                if (num7 != 0)
                {
                    double num28 = (Convert.ToDouble(num8) * Convert.ToDouble(100)) / Convert.ToDouble(num7);
                    //object obj5 = this.lb_fumatori1.Text;
                    // this.lb_fumatori1.Text = string.Concat(new object[] { obj5, "Maschi: <b>", num8.ToString(), "</b> (", Math.Round(num28, 2), "%)<br />" });
                }
                else
                {
                    //this.lb_fumatori1.Text = this.lb_fumatori1.Text + "Maschi: <b>" + num8.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num9 = Convert.ToInt32(reader["Fumo+F"]);
                if (num7 != 0)
                {
                    double num29 = (Convert.ToDouble(num9) * Convert.ToDouble(100)) / Convert.ToDouble(num7);
                    // object obj6 = this.lb_fumatori1.Text;
                    // this.lb_fumatori1.Text = string.Concat(new object[] { obj6, "Femmine: <b>", num9.ToString(), "</b> (", Math.Round(num29, 2), "%)<br />" });
                }
                else
                {
                    //this.lb_fumatori1.Text = this.lb_fumatori1.Text + "Femmine: <b>" + num9.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num10 = Convert.ToInt32(reader["Fumo++"]);
                //this.lb_fumatori2.Text = "Fumatori ++: <b>" + num10.ToString() + "</b><br />";
                if (num4 != 0)
                {
                    double num30 = (Convert.ToDouble(num10) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    // object obj7 = this.lb_fumatori2.Text;
                    // this.lb_fumatori2.Text = string.Concat(new object[] { obj7, "(", Math.Round(num30, 2), "% dei fumatori)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num11 = Convert.ToInt32(reader["Fumo++M"]);
                if (num10 != 0)
                {
                    double num31 = (Convert.ToDouble(num11) * Convert.ToDouble(100)) / Convert.ToDouble(num10);
                    // object obj8 = this.lb_fumatori2.Text;
                    // this.lb_fumatori2.Text = string.Concat(new object[] { obj8, "Maschi: <b>", num11.ToString(), "</b> (", Math.Round(num31, 2), "%)<br />" });
                }
                else
                {
                    // this.lb_fumatori2.Text = this.lb_fumatori2.Text + "Maschi: <b>" + num11.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num12 = Convert.ToInt32(reader["Fumo++F"]);
                if (num10 != 0)
                {
                    double num32 = (Convert.ToDouble(num12) * Convert.ToDouble(100)) / Convert.ToDouble(num10);
                    // obj2 = this.lb_fumatori2.Text;
                    // this.lb_fumatori2.Text = string.Concat(new object[] { obj2, "Femmine: <b>", num12.ToString(), "</b> (", Math.Round(num32, 2), "%)<br />" });
                }
                else
                {
                    // this.lb_fumatori2.Text = this.lb_fumatori2.Text + "Femmine: <b>" + num12.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num13 = Convert.ToInt32(reader["Fumo+++"]);
                // this.lb_fumatori3.Text = "Fumatori +++: <b>" + num13.ToString() + "</b><br />";
                if (num4 != 0)
                {
                    double num33 = (Convert.ToDouble(num13) * Convert.ToDouble(100)) / Convert.ToDouble(num4);
                    //  obj2 = this.lb_fumatori3.Text;
                    // this.lb_fumatori3.Text = string.Concat(new object[] { obj2, "(", Math.Round(num33, 2), "% dei fumatori)<br />" });
                }
                reader.NextResult();
                reader.Read();
                int num14 = Convert.ToInt32(reader["Fumo+++M"]);
                if (num13 != 0)
                {
                    double num34 = (Convert.ToDouble(num14) * Convert.ToDouble(100)) / Convert.ToDouble(num13);
                    // obj2 = this.lb_fumatori3.Text;
                    // this.lb_fumatori3.Text = string.Concat(new object[] { obj2, "Maschi: <b>", num14.ToString(), "</b> (", Math.Round(num34, 2), "%)<br />" });
                }
                else
                {
                    //  this.lb_fumatori3.Text = this.lb_fumatori3.Text + "Maschi: <b>" + num14.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num15 = Convert.ToInt32(reader["Fumo+++F"]);
                if (num13 != 0)
                {
                    double num35 = (Convert.ToDouble(num15) * Convert.ToDouble(100)) / Convert.ToDouble(num13);
                    //obj2 = this.lb_fumatori3.Text;
                    // this.lb_fumatori3.Text = string.Concat(new object[] { obj2, "Femmine: <b>", num15.ToString(), "</b> (", Math.Round(num35, 2), "%)<br />" });
                }
                else
                {
                    // this.lb_fumatori3.Text = this.lb_fumatori3.Text + "Femmine: <b>" + num15.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num16 = Convert.ToInt32(reader["NumTotaleOss"]);
                //   this.lb_osservazioni.Text = "Osservazioni: <b>" + num16.ToString() + "</b><br />";
                double num36 = Convert.ToDouble(num16) / Convert.ToDouble(num);
                //  obj2 = this.lb_osservazioni.Text;
                //  this.lb_osservazioni.Text = string.Concat(new object[] { obj2, "N. medio oss/paziente: <b>", Math.Round(num36, 2), "</b><br />" });
                reader.NextResult();
                reader.Read();
                int num17 = Convert.ToInt32(reader["NumOssM"]);
                if (num16 != 0)
                {
                    double num37 = (Convert.ToDouble(num17) * Convert.ToDouble(100)) / Convert.ToDouble(num16);
                    //     obj2 = this.lb_osservazioni.Text;
                    //      this.lb_osservazioni.Text = string.Concat(new object[] { obj2, "Maschi: <b>", num17.ToString(), "</b> (", Math.Round(num37, 2), "%)<br />" });
                }
                else
                {
                    //   this.lb_osservazioni.Text = this.lb_osservazioni.Text + "Maschi: <b>" + num17.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();
                int num18 = Convert.ToInt32(reader["NumOssF"]);
                if (num16 != 0)
                {
                    double num38 = (Convert.ToDouble(num18) * Convert.ToDouble(100)) / Convert.ToDouble(num16);
                    //  obj2 = this.lb_osservazioni.Text;
                    //  this.lb_osservazioni.Text = string.Concat(new object[] { obj2, "Femmine: <b>", num18.ToString(), "</b> (", Math.Round(num38, 2), "%)<br />" });
                }
                else
                {
                    //  this.lb_osservazioni.Text = this.lb_osservazioni.Text + "Femmine: <b>" + num18.ToString() + "</b><br />";
                }
                reader.NextResult();
                reader.Read();

                reader.NextResult();
                reader.Read();

                // lb_exfumatori.Text += "Ex-Fumatori:<b>" + reader["ExFumo"] + "</b></br> ( " + 100 * int.Parse(reader["ExFumo"].ToString()) / num + "% dei pazienti totali)<br/>";
                reader.NextResult();
                reader.Read();
                // lb_exfumatori.Text += "Maschi:<b>" + reader["ExFumoM"] + "</b><br/>";
                reader.NextResult();
                reader.Read();
                // lb_exfumatori.Text += "Femmine:<b>" + reader["ExFumoF"] + "</b>";
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            base.Response.Write("ERRORE " + exception.Message);
        }
        finally
        {
            connection.Close();
        }
    }
}

﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="quadriclinici.aspx.cs" Inherits="quadriclinici" %>

<!--#include file="include/controllo.inc"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="stile.css" type=text/css rel="stylesheet" />
<script language="JavaScript" src="./js/ts_picker.js"></script>
<title>Gestione Quadri Clinici</title>

<style type="text/css">
<!--
body {
	background-image: url(./files/bg_blueGrad.gif);
	background-color: #324671;
}
.style1 {color: #FFFFFF}
.Stile1 {color: #336DD0}
-->
</style>
</head>

<body>
   <form id="form1" runat="server">
<table width="812" height="100%" border="0" align="center" cellpadding="0" cellspacing="0" vspace="top">
  
  <tr>
    <td width="10" valign="top" background="./files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center"><div align="left">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
              <!--#include file="include/pazienti.inc"-->

              <div class="modernbricksmenuline">
                <ul>
                    <li style="margin-left: 1px"><a href="nuovopaziente.aspx?IdPaziente=-1">Nuovo<br />Paziente</a></li>
                    <li><a href="cercapaziente.aspx">Cerca<br />Paziente</a></li>
                    <li><a href="nuovopaziente.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>">Scheda<br />Paziente</a></li>
                    <li><a onclick="return confirm('Sei sicuro di voler eliminare il paziente corrente?');" href="cercapaziente.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>">Elimina<br />Paziente</a></li>
                    <li><a href="quadriclinici.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>">Quadri<br />Clinici</a></li>
                    <li><a href="dativariabili.aspx?idpaziente=<%=Request.Params["idpaziente"].ToString() %>&idquadro=-1">Nuovo Quadro<br />Clinico</a></li>
                </ul>
              </div>
          </td>
        </tr>
      </table>
    </div>
        <br />
        <br />
        <span style="font-size:18px;color:#324671;font-weight:bold">Elenco quadri clinici<br />
            <asp:Label ID="lb_nome" runat="server" Text=""></asp:Label></span>
        <br />
        <br />
        <table border="0" cellpadding="2" cellspacing="2">
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lb_mess" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></td>
            </tr>
            <tr>
                <td align="right">Nome:
                </td>
                <td align="left">
                    <asp:Label ID="lb_cognome" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">Codice fiscale:
                </td>
                <td align="left">
                    <asp:Label ID="lb_codfis" runat="server" Text=""></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right" valign="top">Data primo accesso:
                </td>
                <td align="left">
                    <asp:Label ID="lb_data" runat="server" Text=""></asp:Label>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <asp:GridView ID="gv_pazienti" runat="server" CellPadding="4" ForeColor="#333333" GridLines="None" AutoGenerateColumns="False">
            <FooterStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <RowStyle BackColor="#EFF3FB" />
            <EditRowStyle BackColor="#2461BF" />
            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
            <HeaderStyle BackColor="#006699" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="White" />
            <Columns>
                <asp:BoundField DataField="IdQuadroClinico" Visible="False" />
                <asp:TemplateField HeaderText="Data">
                    <ItemTemplate>
                        <a href='<%# "dativariabili.aspx?idpaziente="+DataBinder.Eval(Container.DataItem, "IdPaziente")+"&idquadro="+DataBinder.Eval(Container.DataItem, "IdQuadroClinico")%>'><%# Utility.DecodificaData(DataBinder.Eval(Container.DataItem, "DataQuadroClinico").ToString())%></a>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <ItemTemplate>
                        <a onclick='return confirm("Sei sicuro di voler eliminare il quadro clinico?");' href='<%# "quadriclinici.aspx?idpaziente="+DataBinder.Eval(Container.DataItem, "IdPaziente")+"&del="+DataBinder.Eval(Container.DataItem, "IdQuadroClinico")%>'>elimina</a>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>

    </td>
    <td width="10" valign="top" background="./files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="./files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="2" style="height: 20px"><!--#include file="include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
          <td></td>
        </tr>
      </tbody>
    </table></td>
    <td background="./files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" background="./files/bg_content_bottom.gif">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>
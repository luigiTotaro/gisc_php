<%@ Page Language="C#" AutoEventWireup="true" CodeFile="trendsTotali.aspx.cs" Inherits="trendsTotali" %>
<%@ Register Src="datipaziente.ascx" TagName="datipaziente" TagPrefix="uc1" %>
<!--#include file="include/controllo.inc"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="stile.css" type=text/css rel="stylesheet" />
<script language="JavaScript" src="./js/ts_picker.js"></script>
<script language="JavaScript" src="./js/controlli.js"></script>
<title>Gestione Paziente</title>

<style type="text/css">
<!--
body {
	background-image: url(./files/bg_blueGrad.gif);
	background-color: #324671;
}
.Stile1 {color: #336DD0}
-->
</style>

<link rel="stylesheet" type="text/css" href="./ext-4.0.2a/resources/css/ext-all.css" />
    <link rel="stylesheet" type="text/css" href="./ext-4.0.2a/examples/shared/example.css" />
    <script type="text/javascript" src="./ext-4.0.2a/bootstrap.js"></script>
    <%--<script type="text/javascript" src="./ext-4.0.2a/examples/example-data.js"></script>--%>
</head>

<body>
   <form id="form1" runat="server">
    <script>
  
    
    Ext.require('Ext.chart.*');
Ext.require('Ext.layout.container.Fit');
var dataTotale;
window.generateData = function(n, floor) {
    var data = [],
            p = (Math.random() * 11) + 1,
            i;

    floor = (!floor && floor !== 0) ? 20 : floor;

    for (i = 0; i < (n || 12); i++) {
        data.push({
            name: Ext.Date.monthNames[i % 12],
            data1: Math.floor(Math.max((Math.random() * 100), floor)),
            data2: Math.floor(Math.max((Math.random() * 100), floor)),
            data3: Math.floor(Math.max((Math.random() * 100), floor)),
            data4: Math.floor(Math.max((Math.random() * 100), floor)),
            data5: Math.floor(Math.max((Math.random() * 100), floor)),
            data6: Math.floor(Math.max((Math.random() * 100), floor)),
            data7: Math.floor(Math.max((Math.random() * 100), floor)),
            data8: Math.floor(Math.max((Math.random() * 100), floor)),
            data9: Math.floor(Math.max((Math.random() * 100), floor))
        });
    }
    return data;
};
function convertiNYHA(val) {

    if (val == "")
        return "";
    if (val == 'I classe')
        return 1;
    if (val == 'II classe')
        return 2;
    if (val == 'III classe')
        return 3;
    if (val == 'IV classe')
        return 4;
    return ""; 
}

function svisceraData(val) {
    
    if (val == "")
        return "Reclutamento";
    var anno = val.substring(0, 4);
    var mese = val.substring(4, 6);
    var giorno = val.substring(6, 8);
    return giorno + "/" + mese;  //+ "/" + anno;
}

function svisceraDataCompleta(val) {

    if (val == "")
        return "Reclutamento";
    var anno = val.substring(0, 4);
    var mese = val.substring(4, 6);
    var giorno = val.substring(6, 8);
    return giorno + "/" + mese  + "/" + anno;
}
Ext.onReady(function() {
    //store1.loadData(generateDataGISCSesso());

    window.generateDataGISCValori = function() {
        var data = [],
            p = 0,
            i;

        var dataFE = [],
            p = 0,
            i;

        var dataPP = [],
            p = 0,
            i;

        var dataIC = [],
            p = 0,
            i;
        var dataBP = [],
                    p = 0,
                    i;
        var dataNY = [],
                    p = 0,
                    i;


        var varLast = 0;
        var varLastFE = 0;
        var varLastPP = 0;
        var varLastIC = 0;
        var varLastBP = 0;
        var varLastNY = 0;
        Ext.Ajax.request({
            url: 'trendsData.aspx',
            params: {
                action: 'totali',
                DA:'<%=Request.Params["DA"] %>'
            },
            success: function(response) {

                var text = eval(response.responseText);
                dataTotale = text;

            }
        });





        //}
        return data;
    }

   
    Ext.define('DataModel', {
        extend: 'Ext.data.Model',
        fields: [
            { name: 'name', type: 'string' },
            { name: 'data1', type: 'int' },
            { name: 'data2', type: 'int' },
             { name: 'data3', type: 'inf', useNull: false
             }



        ]
    });

    window.store2 = Ext.create('Ext.data.Store', {
        id: 'store2',
        pageSize: 50,
        model: 'DataModel',
        autoLoad: true,
        remoteSort: true,
        // allow the grid to interact with the paging scroller by buffering
        buffered: true,
        proxy: {
            // load using script tags for cross domain, if the data in on the same domain as
            // this page, an HttpProxy would be better
            type: 'ajax',
            // url: 'http://www.sencha.com/forum/remote_topics/index.php',
            url: 'trendsData.aspx',
            extraParams: {
                action: 'totali',
                DA: '<%=Request.Params["DA"] %>'
            },
            reader: {
                root: 'Data'

            }
            // sends single sort as multi parameter

        },
        listeners:
        {
            load: function(d) {
                //console.log(d.data.items);
            }
        }
    });


    window.storeFC = Ext.create('Ext.data.Store', {
    id: 'storeFC',
        pageSize: 50,
        model: 'DataModel',
        autoLoad: true,
        remoteSort: true,
        // allow the grid to interact with the paging scroller by buffering
        buffered: true,
        proxy: {
            // load using script tags for cross domain, if the data in on the same domain as
            // this page, an HttpProxy would be better
            type: 'ajax',
            // url: 'http://www.sencha.com/forum/remote_topics/index.php',
            url: 'trendsData.aspx',
            extraParams: {
            action: 'totaliFC',
            DA: '<%=Request.Params["DA"] %>'

            },
            reader: {
                root: 'Data'

            }
            // sends single sort as multi parameter

        },
        listeners:
        {
            load: function(d) {
                //console.log(d.data.items);
            }
        }
    });


    window.storeFE = Ext.create('Ext.data.Store', {
    id: 'storeFE',
        pageSize: 50,
        model: 'DataModel',
        autoLoad: true,
        remoteSort: true,
        // allow the grid to interact with the paging scroller by buffering
        buffered: true,
        proxy: {
            // load using script tags for cross domain, if the data in on the same domain as
            // this page, an HttpProxy would be better
            type: 'ajax',
            // url: 'http://www.sencha.com/forum/remote_topics/index.php',
            url: 'trendsData.aspx',
            extraParams: {
            action: 'totaliFE',
            DA: '<%=Request.Params["DA"] %>'

            },
            reader: {
                root: 'Data'

            }
            // sends single sort as multi parameter

        },
        listeners:
        {
            load: function(d) {
                //console.log(d.data.items);
            }
        }
    });


    window.storePP = Ext.create('Ext.data.Store', {
    id: 'storePP',
        pageSize: 50,
        model: 'DataModel',
        autoLoad: true,
        remoteSort: true,
        // allow the grid to interact with the paging scroller by buffering
        buffered: true,
        proxy: {
            // load using script tags for cross domain, if the data in on the same domain as
            // this page, an HttpProxy would be better
            type: 'ajax',
            // url: 'http://www.sencha.com/forum/remote_topics/index.php',
            url: 'trendsData.aspx',
            extraParams: {
            action: 'totaliPP',
            DA: '<%=Request.Params["DA"] %>'

            },
            reader: {
                root: 'Data'

            }
            // sends single sort as multi parameter

        },
        listeners:
        {
            load: function(d) {
                //console.log(d.data.items);
            }
        }
    });


    window.storeIC = Ext.create('Ext.data.Store', {
    id: 'storeIC',
        pageSize: 50,
        model: 'DataModel',
        autoLoad: true,
        remoteSort: true,
        // allow the grid to interact with the paging scroller by buffering
        buffered: true,
        proxy: {
            // load using script tags for cross domain, if the data in on the same domain as
            // this page, an HttpProxy would be better
            type: 'ajax',
            // url: 'http://www.sencha.com/forum/remote_topics/index.php',
            url: 'trendsData.aspx',
            extraParams: {
            action: 'totaliIC',
            DA: '<%=Request.Params["DA"] %>'

            },
            reader: {
                root: 'Data'

            }
            // sends single sort as multi parameter

        },
        listeners:
        {
            load: function(d) {
                //console.log(d.data.items);
            }
        }
    });


    window.storeBP = Ext.create('Ext.data.Store', {
    id: 'storeBP',
        pageSize: 50,
        model: 'DataModel',
        autoLoad: true,
        remoteSort: true,
        // allow the grid to interact with the paging scroller by buffering
        buffered: true,
        proxy: {
            // load using script tags for cross domain, if the data in on the same domain as
            // this page, an HttpProxy would be better
            type: 'ajax',
            // url: 'http://www.sencha.com/forum/remote_topics/index.php',
            url: 'trendsData.aspx',
            extraParams: {
            action: 'totaliBP',
            DA: '<%=Request.Params["DA"] %>'

            },
            reader: {
                root: 'Data'

            }
            // sends single sort as multi parameter

        },
        listeners:
        {
            load: function(d) {
                //console.log(d.data.items);
            }
        }
    });


    panel2 = Ext.create('widget.panel', {
        width: 1050,
        height: 200,
        title: '',
        border: false,
        style: 'font-size:9px',
        renderTo: 'divDashboardValoriNY',
        layout: 'fit',
        items: {
            id: 'chartCmp',
            xtype: 'chart',
            style: 'background:#fff',
            animate: true,
            store: store2,
            axes:
            [
             {
                 type: 'Numeric',
                 position: 'left',
                 fields: 'data1',
                 minimum: 1,
                 maximum: 4,
                 minorTickSteps: 1,
                 majorTickSteps: 1,
                 decimals: 3,

                 grid: true,
                 title: 'Classe NYHA'
             }

            ],
            insetPadding: 50,
            series: [{
    type: 'line',
    axis: 'left',
    showMarkers:false,
    listeners: {
        'convert': function() {
            console.log('afterrender');
        }
    },
   //xField: 'data2',
    yField: 'data3',
    smooth: 5,
     markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    radius: 8,
                    size: 8
                },
               style: {
    stroke: '#FF0000',
    'stroke-width': 6,
    fill: '#FF0000',
    opacity: 0.6
},

    tips: {
        trackMouse: true,
        width: 250,
        height: 120,
        renderer: function(storeItem, item) {
            var idx = parseFloat(storeItem.get('data3'));
            this.setTitle("Media NYHA: <b>" + idx + "</b>");
        }
    }
},{
                type: 'scatter',

                xField: 'data2',
                yField: 'data1',
                color: '#99bbff',
                
                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    'opacity': '0.3',
                    radius: 2,
                    size: 2
                }
            }

]
        }
    });






    panel4 = Ext.create('widget.panel', {
        width: 1050,
        height: 200,
        title: '',
        border: false,
        style: 'font-size:9px',
        renderTo: 'divDashboardValori',
        layout: 'fit',
        items: {
            id: 'chartCmp',
            xtype: 'chart',
            style: 'background:#fff',
            animate: true,
            store: storeFC,
            axes:
            [
             {
                 type: 'Numeric',
                 position: 'left',
                 fields: 'data1',
                 minimum: 35,
                 maximum: 120,
                 minorTickSteps: 1,
                 majorTickSteps: 1,
                 decimals: 0,

                 grid: true,
                 title: 'Freq. Cardiaca/min'
             }

            ],
            insetPadding: 50,
            series: [{
                type: 'line',
                axis: 'left',
                showMarkers: false,
                listeners: {
                    'convert': function() {
                        console.log('afterrender');
                    }
                },
                //xField: 'data2',
                yField: 'data3',
                smooth: 5,
                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    radius: 8,
                    size: 8
                },
                style: {
                    stroke: '#FF0000',
                    'stroke-width': 6,
                    fill: '#FF0000',
                    opacity: 0.6
                },

                tips: {
                    trackMouse: true,
                    width: 250,
                    height: 120,
                    renderer: function(storeItem, item) {
                        var idx = parseFloat(storeItem.get('data3'));
                        this.setTitle("Media Freq. Cardiaca/min: <b>" + idx + "</b>");
                    }
                }
            }, {
                type: 'scatter',

                xField: 'data2',
                yField: 'data1',
                color: '#99bbff',

                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    'opacity': '0.3',
                    radius: 2,
                    size: 2
                }
            }

]
        }
    });

    panel5 = Ext.create('widget.panel', {
        width: 1050,
        height: 200,
        title: '',
        border: false,
        style: 'font-size:9px',
        renderTo: 'divDashboardValoriFE',
        layout: 'fit',
        items: {
            id: 'chartCmp',
            xtype: 'chart',
            style: 'background:#fff',
            animate: true,
            store: storeFE,
            axes:
            [
             {
                 type: 'Numeric',
                 position: 'left',
                 fields: 'data1',
                 minimum: 20,
                 maximum: 70,
                 minorTickSteps: 1,
                 majorTickSteps: 1,
                 decimals: 3,

                 grid: true,
                 title: 'F.E. %'
             }

            ],
            insetPadding: 50,
            series: [{
                type: 'line',
                axis: 'left',
                showMarkers: false,
                listeners: {
                    'convert': function() {
                        console.log('afterrender');
                    }
                },
                //xField: 'data2',
                yField: 'data3',
                smooth: 5,
                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    radius: 8,
                    size: 8
                },
                style: {
                    stroke: '#FF0000',
                    'stroke-width': 6,
                    fill: '#FF0000',
                    opacity: 0.6
                },

                tips: {
                    trackMouse: true,
                    width: 250,
                    height: 120,
                    renderer: function(storeItem, item) {
                        var idx = parseFloat(storeItem.get('data3'));
                        this.setTitle("Media F.E. %: <b>" + idx + "</b>");
                    }
                }
            }, {
                type: 'scatter',

                xField: 'data2',
                yField: 'data1',
                color: '#99bbff',

                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    'opacity': '0.3',
                    radius: 2,
                    size: 2
                }
            }

]
        }
    });



    panel6 = Ext.create('widget.panel', {
        width: 1050,
        height: 200,
        title: '',
        border: false,
        style: 'font-size:9px',
        renderTo: 'divDashboardValoriPP',
        layout: 'fit',
        items: {
            id: 'chartCmp',
            xtype: 'chart',
            style: 'background:#fff',
            animate: true,
            store: storePP,
            axes:
            [
             {
                 type: 'Numeric',
                 position: 'left',
                 fields: 'data1',
                 minimum: 20,
                 maximum: 80,
                 minorTickSteps: 1,
                 majorTickSteps: 1,
                 decimals: 3,

                 grid: true,
                 title: 'Pres.Polm. mmhg'
             }

            ],
            insetPadding: 50,
            series: [{
                type: 'line',
                axis: 'left',
                showMarkers: false,
                listeners: {
                    'convert': function() {
                        console.log('afterrender');
                    }
                },
                //xField: 'data2',
                yField: 'data3',
                smooth: 5,
                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    radius: 8,
                    size: 8
                },
                style: {
                    stroke: '#FF0000',
                    'stroke-width': 6,
                    fill: '#FF0000',
                    opacity: 0.6
                },

                tips: {
                    trackMouse: true,
                    width: 250,
                    height: 120,
                    renderer: function(storeItem, item) {
                        var idx = parseFloat(storeItem.get('data3'));
                        this.setTitle("Media Pres.Polm. mmhg : <b>" + idx + "</b>");
                    }
                }
            }, {
                type: 'scatter',

                xField: 'data2',
                yField: 'data1',
                color: '#99bbff',

                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    'opacity': '0.3',
                    radius: 2,
                    size: 2
                }
            }

]
        }
    });



    panel7 = Ext.create('widget.panel', {
        width: 1050,
        height: 200,
        title: '',
        border: false,
        style: 'font-size:9px',
        renderTo: 'divDashboardValoriIC',
        layout: 'fit',
        items: {
            id: 'chartCmp',
            xtype: 'chart',
            style: 'background:#fff',
            animate: true,
            store: storeIC,
            axes:
            [
             {
                 type: 'Numeric',
                 position: 'left',
                 fields: 'data1',
                 minimum: 0,
                 maximum: 4,
                 minorTickSteps: 1,
                 majorTickSteps: 1,
                 decimals: 3,

                 grid: true,
                 title: 'Creatininemia mg/dl'
             }

            ],
            insetPadding: 50,
            series: [{
                type: 'line',
                axis: 'left',
                showMarkers: false,
                listeners: {
                    'convert': function() {
                        console.log('afterrender');
                    }
                },
                //xField: 'data2',
                yField: 'data3',
                smooth: 5,
                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    radius: 8,
                    size: 8
                },
                style: {
                    stroke: '#FF0000',
                    'stroke-width': 6,
                    fill: '#FF0000',
                    opacity: 0.6
                },

                tips: {
                    trackMouse: true,
                    width: 250,
                    height: 120,
                    renderer: function(storeItem, item) {
                        var idx = parseFloat(storeItem.get('data3'));
                        this.setTitle("Media Creatininemia mg/dl : <b>" + idx + "</b>");
                    }
                }
            }, {
                type: 'scatter',

                xField: 'data2',
                yField: 'data1',
                color: '#99bbff',

                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    'opacity': '0.3',
                    radius: 2,
                    size: 2
                }
            }

]
        }
    });



    panel8 = Ext.create('widget.panel', {
        width: 1050,
        height: 200,
        title: '',
        border: false,
        style: 'font-size:9px',
        renderTo: 'divDashboardValoriBP',
        layout: 'fit',
        items: {
            id: 'chartCmp',
            xtype: 'chart',
            style: 'background:#fff',
            animate: true,
            store: storeBP,
            axes:
            [
             {
                 type: 'Numeric',
                 position: 'left',
                 fields: 'data1',
                 minimum: 0,
                 maximum: 14000,
                 minorTickSteps: 1,
                 majorTickSteps: 1,
                 decimals: 0,

                 grid: true,
                 title: 'BNP pg/ml'
             }

            ],
            insetPadding: 50,
            series: [{
                type: 'line',
                axis: 'left',
                showMarkers: false,
                listeners: {
                    'convert': function() {
                        console.log('afterrender');
                    }
                },
                //xField: 'data2',
                yField: 'data3',
                smooth: 5,
                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    radius: 8,
                    size: 8
                },
                style: {
                    stroke: '#FF0000',
                    'stroke-width': 6,
                    fill: '#FF0000',
                    opacity: 0.6
                },

                tips: {
                    trackMouse: true,
                    width: 250,
                    height: 120,
                    renderer: function(storeItem, item) {
                        var idx = parseFloat(storeItem.get('data3'));
                        this.setTitle("Media BNP pg/ml : <b>" + idx + "</b>");
                    }
                }
            }, {
                type: 'scatter',

                xField: 'data2',
                yField: 'data1',
                color: '#99bbff',

                markerConfig: {
                    type: 'circle',
                    'fill': '#CCCCCC',
                    'opacity': '0.3',
                    radius: 2,
                    size: 2
                }
            }

]
        }
    });
       
       

    });

    function mostraCallout(idx,obj) {
        var strCallout = '<div><b style="font-size:14px;color:#0000FF">' + svisceraDataCompleta(dataTotale[idx].DataQuadroClinico) + '</b><br/><br/> Frequenza Cardiaca:<b>' + pulisciDato(dataTotale[idx].gFrequenzaCard, '/min') + '</b>';
                    strCallout += '<br/>F.E.: ' + pulisciDato(dataTotale[idx].Eco2DFE, ' %');
                    strCallout += '<br/>Pressione Polmonare: ' + pulisciDato(dataTotale[idx].EcoDopplerPressPolm, ' mmhg');
                    strCallout += '<br/>Creatininemia: ' + pulisciDato(dataTotale[idx].IngrCreat, ' mg/dl');
                    strCallout += '<br/>BNP: ' + pulisciDato(dataTotale[idx].IngrBNP, ' pg/ml');
                    strCallout += '<br/>Classe NYHA: ' + pulisciDato(dataTotale[idx].ClasseDispneaSforzo, ' ');
                    strCallout += "</div>";
                    obj.setTitle(strCallout);
    }
    
    function pulisciDato(val,unita) {
        if ((val == null) || (val == "") || (val == undefined)) { 
         return "NON RILEVATO";
     }
     return val + unita;
    }
    
    </script>
<table width="1100" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td width="10" valign="top" background="./files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center">
    
    <div align="left" style="margin-bottom:10px">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img style="width:100%" src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
                <!--#include file="include/statistiche.inc"-->
    <asp:Label ID="lb_gest_utenti" Visible="false" runat="server" Text=""></asp:Label>
             
          </td>
          </tr>
          <tr>
            <td align="center">
                <br />
                <br />
                <span style="font-size:18px;color:#324671;font-weight:bold"><asp:Label ID="lb_titolo" runat="server" Text=""></asp:Label></span>
               
                <uc1:datipaziente ID="Datipaziente1" Visible="false" runat="server" />
            </td>
          </tr>
          <tr>
            <td align="center">
                <br />
                <b>Trends totali<asp:Label runat="server" ID="lblPaziente"></asp:Label></b>
                <br />
                 <div style="width:100%; height:210px;" id="divDashboardValori"></div>
                  <div style="width:100%; height:210px;" id="divDashboardValoriFE"></div>
                    <div style="width:100%; height:210px;" id="divDashboardValoriPP"></div>
                     <div style="width:100%; height:210px;" id="divDashboardValoriIC"></div>
                      <div style="width:100%; height:210px;" id="divDashboardValoriBP"></div>
                       <div style="width:100%; height:210px;" id="divDashboardValoriNY"></div>
                <br /><br />
                <asp:Label ID="lb_steps" runat="server" Text=""></asp:Label>
            </td>
          </tr>
          <tr>
            <td align="center">
                

            </td>
          </tr>
      </table>
    </div>
        

        <br />
        <br />
        <asp:Label ID="lb_mess" runat="server" Text="" ForeColor="Red"></asp:Label>
    
    </td>
    <td width="10" valign="top" background="./files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="./files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="2" style="height: 20px"><!--#include file="include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="./files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
 <%-- <tr>
    <td colspan="3" background="./files/bg_content_bottom.gif">&nbsp;</td>
  </tr>--%>
</table>
</form>
</body>
</html>
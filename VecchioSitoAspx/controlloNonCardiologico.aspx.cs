﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class controlloNonCardiologico : System.Web.UI.Page
{
    private string conn_str;


   
    private int IdPaziente;
   
    private int IdUltimoPaziente;


    private bool ApportaModifiche()
    {
        string str;
        bool flag = false;
       /* if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_paziente";
        }
        else
        {
            str = "modifica_paziente";
        }*/
        str = "modifica_ricovero_non_cardiologico";
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlTransaction transaction = connection.BeginTransaction();
            try
            {
                try
                {
                    SqlCommand command = new SqlCommand(str, connection, transaction);
                    command.CommandType = CommandType.StoredProcedure;
                    if (!this.IdPaziente.Equals(-1))
                    {
                        command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
                    }
                    command.Parameters.AddWithValue("@dataRicovero", Utility.CodificaData(this.txtDataDead.Text.Trim()));
                    command.Parameters.AddWithValue("@note", this.txtNote.Text.Trim());
                    command.Parameters.AddWithValue("@idRicovero",Request.Params["idQuadro"].ToString());
                    command.Parameters.AddWithValue("@idUtente", this.Session["IdUtente"].ToString());
                    
                    SqlDataReader reader = command.ExecuteReader();
                    /*if (reader.HasRows)
                    {
                        while (reader.Read())
                        {
                            this.IdUltimoPaziente = Convert.ToInt32(reader["idultimopaziente"]);
                        }
                    }
                    else
                    {
                        this.IdUltimoPaziente = 0;
                    }*/
                    reader.Close();
                  
                    transaction.Commit();
                    flag = true;
                }
                catch (Exception exception)
                {
                    transaction.Rollback();
                    this.lb_mess.Text = exception.Message;
                }
                return flag;
            }
            finally
            {
                connection.Close();
            }
        }
        catch (Exception exception2)
        {
            this.lb_mess.Text = exception2.Message;
        }
        return flag;
    }
    /*
    private bool ApportaModificheAltreEziologie(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_altre_eziologie";
        }
        else
        {
            str = "modifica_altre_eziologie";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", this.Txt_Altre_Eziologie_Descrizione.Text.Trim());
            command.Parameters.AddWithValue("@Denominazione", this.Txt_Altre_Eziologie_Denominazione.Text.Trim());
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_Altre_Eziologie.Text.Trim()));
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Altre_Eziologie_Document_Ecg.Text.Trim());
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Altre_Eziologie_Document_Eco.Text.Trim());
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModificheCardiMioIpertrofica(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_CardiMioIpertrofica";
        }
        else
        {
            str = "modifica_CardiMioIpertrofica";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_Cardiomiopatia_Ipertrofica.Text.Trim()));
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Ecg.Text.Trim());
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Eco.Text.Trim());
            command.Parameters.AddWithValue("@Ostruttiva", this.Ddl_Eziologia_Cardiomiopatia_Ipertrofica_Ostruttiva.Text.Trim());
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModificheCardioMiopatiaDilatativa(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_CardioMiopatiaDilatativa";
        }
        else
        {
            str = "modifica_CardioMiopatiaDilatativa";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_Cardiomiopatia_Dilatativa.Text.Trim()));
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Ecg.Text.Trim());
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Eco.Text.Trim());
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModificheCardiopIschemNoIMA(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_CardiopIschemNoIMA";
        }
        else
        {
            str = "modifica_CardiopIschemNoIMA";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_Cardiop_Ischem.Text.Trim()));
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Cardiop_Document_ECG.Text.Trim());
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Cardiop_Document_ECO.Text.Trim());
            command.Parameters.AddWithValue("@Coronografia", this.Rbl_Eziologia_Cardiop_Document_Coronarogr.Text.Trim());
            command.Parameters.AddWithValue("@Scintigrafia", this.Rbl_Eziologia_Cardiop_Document_Scintigrafia.Text.Trim());
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModificheIMA(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_IMA";
        }
        else
        {
            str = "modifica_IMA";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_IMA.Text.Trim()));
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_IMA_Documentazione_Ecg.Text.Trim());
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_IMA_Documentazione_Eco.Text.Trim());
            command.Parameters.AddWithValue("@Coronografia", this.Rbl_Eziologia_IMA_Documentazione_Coronogr.Text.Trim());
            command.Parameters.AddWithValue("@Scintigrafia", this.Rbl_Eziologia_IMA_Documentazione_Scintigrafia.Text.Trim());
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModificheOspedalizzazioni(SqlTransaction trans, SqlConnection conn, TextBox cld1, TextBox cld2, TextBox text1, DropDownList drop1, TextBox text2, DropDownList drop2, int idospedalizzazione)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_ospedalizzazione";
        }
        else
        {
            str = "modifica_ospedalizzazione";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
                command.Parameters.AddWithValue("@IdOspedalizzazioneScomp", idospedalizzazione);
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@DataAccesso", Utility.CodificaData(cld1.Text.Trim()));
            command.Parameters.AddWithValue("@DataUscita", Utility.CodificaData(cld2.Text.Trim()));
            command.Parameters.AddWithValue("@Diagnosi", text1.Text.Trim());
            command.Parameters.AddWithValue("@Terapia", text2.Text.Trim());
            command.Parameters.AddWithValue("@IdClasseNYHADim", Convert.ToInt32(drop2.SelectedValue));
            command.Parameters.AddWithValue("@IdClasseNYHA", Convert.ToInt32(drop1.SelectedValue));
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModifichePrecedentiScomp(SqlTransaction trans, SqlConnection conn, TextBox cld1, TextBox text1, TextBox text2, int idprecedentescompenso)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_precedente_scompenso";
        }
        else
        {
            str = "modifica_precedente_scompenso";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
                command.Parameters.AddWithValue("@IdPrecedentiScompenso", idprecedentescompenso);
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Diagnosi", text1.Text);
            command.Parameters.AddWithValue("@DataAccesso", Utility.CodificaData(cld1.Text.Trim()));
            command.Parameters.AddWithValue("@Terapia", text2.Text);
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModificheRischi(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_rischio";
        }
        else
        {
            str = "modifica_rischio";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@FumoValore", this.Ddl_Fattori_Rischio_Fumo_Grado.Text.Trim());
            command.Parameters.AddWithValue("@FumoData", Utility.CodificaData(this.Cld_Fattori_Rischio_Comorbidita_Fumo.Text.Trim()));
            command.Parameters.AddWithValue("@Diabete1Data", Utility.CodificaData(this.Cld_Fattori_Rischio_Comorbidita_Diabete1.Text.Trim()));
            command.Parameters.AddWithValue("@Diabete1Valore", this.Txt_diabete1_grado.Text.Trim());
            command.Parameters.AddWithValue("@Diabete2Data", Utility.CodificaData(this.Cld_Fattori_Rischio_Comorbidita_Diabete2.Text.Trim()));
            command.Parameters.AddWithValue("@Diabete2Valore", this.Txt_diabete2_grado.Text.Trim());
            command.Parameters.AddWithValue("@TerapieDiabete", this.Txt_Terapie_Praticate_Diabete.Text.Trim());
            command.Parameters.AddWithValue("@IperArtData", Utility.CodificaData(this.Cld_Fattori_Rischio_Comorbidita_Ipert_Arter.Text.Trim()));
            command.Parameters.AddWithValue("@TipoIperArt", this.Ddl_Ipert_Art_Tipologia.Text.Trim());
            command.Parameters.AddWithValue("@TerapiaIperArt", this.Txt_Terapie_Praticate_Ipert_Art.Text.Trim());
            command.Parameters.AddWithValue("@NormPA", this.Rbl_Ipert_Art_Normalizzazione.Text.Trim());
            command.Parameters.AddWithValue("@LDLMag130", this.Rbl_Iperdislipidemia_LDL_Superiore_130.Text.Trim());
            command.Parameters.AddWithValue("@HDLMin35", this.Rbl_Iperdislipidemia_HDL_Inferiore_35.Text.Trim());
            command.Parameters.AddWithValue("@TriglMag170", this.Rbl_Iperdislipidemia_Trigl_Superiore_170.Text.Trim());
            command.Parameters.AddWithValue("@Statine", this.Rbl_Iperdislipidemia_Statine.Text.Trim());
            command.Parameters.AddWithValue("@TipoFarmaco", this.Txt_Iperdislipidemia_Tipo_Farmaco.Text.Trim());
            if (this.Rbl_Iperdislipidemia_LDL_Dosi.SelectedValue == "3")
            {
                command.Parameters.AddWithValue("@LDLDopoMag130", "True");
                command.Parameters.AddWithValue("@LDLDopo100tra130", "False");
                command.Parameters.AddWithValue("@LDLDopoMin100", "False");
            }
            if (this.Rbl_Iperdislipidemia_LDL_Dosi.SelectedValue == "1")
            {
                command.Parameters.AddWithValue("@LDLDopoMag130", "False");
                command.Parameters.AddWithValue("@LDLDopo100tra130", "True");
                command.Parameters.AddWithValue("@LDLDopoMin100", "False");
            }
            if (this.Rbl_Iperdislipidemia_LDL_Dosi.SelectedValue == "2")
            {
                command.Parameters.AddWithValue("@LDLDopoMag130", "False");
                command.Parameters.AddWithValue("@LDLDopo100tra130", "False");
                command.Parameters.AddWithValue("@LDLDopoMin100", "True");
            }
            if (this.Rbl_Iperdislipidemia_LDL_Dosi.SelectedValue == "")
            {
                command.Parameters.AddWithValue("@LDLDopoMag130", "False");
                command.Parameters.AddWithValue("@LDLDopo100tra130", "False");
                command.Parameters.AddWithValue("@LDLDopoMin100", "False");
            }
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool ApportaModificheValvolopatia(SqlTransaction trans, SqlConnection conn)
    {
        string str;
        bool flag = false;
        if (this.IdPaziente.Equals(-1))
        {
            str = "inserisci_Valvolopatia";
        }
        else
        {
            str = "modifica_Valvolopatia";
        }
        try
        {
            int idUltimoPaziente;
            SqlCommand command = new SqlCommand(str, conn, trans);
            command.CommandType = CommandType.StoredProcedure;
            if (this.IdPaziente.Equals(-1))
            {
                idUltimoPaziente = this.IdUltimoPaziente;
            }
            else
            {
                idUltimoPaziente = this.IdPaziente;
            }
            command.Parameters.AddWithValue("@IdPaziente", idUltimoPaziente);
            command.Parameters.AddWithValue("@Descrizione", "");
            command.Parameters.AddWithValue("@Data", Utility.CodificaData(this.Cld_Eziologia_Valvolopatie.Text.Trim()));
            command.Parameters.AddWithValue("@ECG", this.Rbl_Eziologia_Valvolopatie_Document_Ecg.Text);
            command.Parameters.AddWithValue("@ECO", this.Rbl_Eziologia_Valvolopatie_Document_Eco.Text);
            command.Parameters.AddWithValue("@TipoValvolopatia", this.Ddl_Eziologia_Valvolopatie_Tipo.SelectedValue);
            command.ExecuteNonQuery();
            flag = true;
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        return flag;
    }

    private bool CercaDuplicati()
    {
        string str;
        bool flag = false;
        if (!this.IdPaziente.Equals(-1))
        {
            str = "cerca_paziente_id_dati";
        }
        else
        {
            str = "cerca_paziente_dati";
        }
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand(str, connection);
            command.CommandType = CommandType.StoredProcedure;
            if (!this.IdPaziente.Equals(-1))
            {
                command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            }
            command.Parameters.AddWithValue("@Nome", this.txtNote.Text.Trim());
            command.Parameters.AddWithValue("@Cognome", this.Txt_Cognome.Text.Trim());
            command.Parameters.AddWithValue("@CodiceFiscale", this.Txt_Codice_Fiscale.Text.Trim());
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = true;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool controllaformati()
    {
        bool flag = false;
        if (!Utility.IsNotNegInteger(this.Txt_Altezza.Text))
        {
            this.lb_mess.Text = this.lb_mess.Text + "<br>L'altezza deve essere espressa in cm senza virgole o punti";
            flag = true;
        }
        if (!Utility.IsNotNegInteger(this.Txt_Eta.Text))
        {
            this.lb_mess.Text = this.lb_mess.Text + "<br>L'eta deve essere espressa in formato numerico";
            flag = true;
        }
        if (!Utility.IsNotNegInteger(this.Txt_Peso1.Text))
        {
            this.lb_mess.Text = this.lb_mess.Text + "<br>Il peso deve essere espresso in kg prima della virgola";
            flag = true;
        }
        if (!Utility.IsNotNegInteger(this.Txt_Circonferenza_Addominale.Text))
        {
            this.lb_mess.Text = this.lb_mess.Text + "<br>La circonferenza addominale deve essere espresso in cm senza virgole o punti";
            flag = true;
        }
        return flag;
    }

    protected void CreaMenuStep(string current_step_id)
    {
        this.lb_steps.Text = "<div class=\"menu_step\"><ul>";
        foreach (Control control in this.Wizard1.WizardSteps)
        {
            if (control.GetType().ToString().Equals("System.Web.UI.WebControls.WizardStep"))
            {
                WizardStep step = (WizardStep)control;
                if (step.ID.Equals(current_step_id))
                {
                    this.lb_steps.Text = this.lb_steps.Text + "<li class=\"current\"><span>" + step.Title + "</span></li>";
                }
                else
                {
                    this.lb_steps.Text = this.lb_steps.Text + "<li><span>" + step.Title + "</span></li>";
                }
            }
        }
        this.lb_steps.Text = this.lb_steps.Text + "</ul></div>";
    }

    private void ElencaMedici()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("elenca_medici", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                ListItem item = new ListItem(reader["Medico"].ToString(), reader["idMedicoCurante"].ToString());
                this.Medico.Items.Add(item);
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
    }

    private void ElencaTipoValvolopatie()
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("elenca_valvolopatietipo", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                ListItem item = new ListItem(reader["TipoValvolopatia"].ToString(), reader["IdTipoValvolopatia"].ToString());
                this.Ddl_Eziologia_Valvolopatie_Tipo.Items.Add(item);
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
    }

    private bool Estrai_Altre_Eziologie()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Eziologie", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Eziologia_Altre_Eziologie.Text = Utility.DecodificaData(reader["DataPrimoRilievo"].ToString());
                this.Rbl_Eziologia_Altre_Eziologie_Document_Ecg.SelectedValue = reader["DocumentECG"].ToString();
                this.Rbl_Eziologia_Altre_Eziologie_Document_Eco.SelectedValue = reader["DocumentECO"].ToString();
                this.Txt_Altre_Eziologie_Denominazione.Text = reader["Denominazione"].ToString();
                this.Txt_Altre_Eziologie_Descrizione.Text = reader["Descrizione"].ToString();
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Cardiomiopatia_Dilatativa()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Cardiomiopatia_Dilatativa", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Eziologia_Cardiomiopatia_Dilatativa.Text = Utility.DecodificaData(reader["DataPrimoRilievo"].ToString());
                this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Ecg.SelectedValue = reader["ECG"].ToString();
                this.Rbl_Eziologia_Cardiomiopatia_Dilatativa_Document_Eco.SelectedValue = reader["ECO"].ToString();
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Cardiomiopatia_Ipertrofica()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Cardiomiopatia_Ipertrofica", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Eziologia_Cardiomiopatia_Ipertrofica.Text = Utility.DecodificaData(reader["DataPrimoRilievo"].ToString());
                this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Ecg.SelectedValue = reader["DocumentazioneECG"].ToString();
                this.Rbl_Eziologia_Cardiomiopatia_Ipertrofica_Document_Eco.SelectedValue = reader["DocumentazioneECO"].ToString();
                this.Ddl_Eziologia_Cardiomiopatia_Ipertrofica_Ostruttiva.SelectedValue = reader["Ostruttiva"].ToString();
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_CARDIOP_ISCHEM_SENZA_DOCUMENT_IMA()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_CARDIOP_ISCHEM_SENZA_DOCUMENT_IMA", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Eziologia_Cardiop_Ischem.Text = Utility.DecodificaData(reader["DataPrimoRilievo"].ToString());
                this.Rbl_Eziologia_Cardiop_Document_ECG.SelectedValue = reader["ECG"].ToString();
                this.Rbl_Eziologia_Cardiop_Document_ECO.SelectedValue = reader["ECO"].ToString();
                this.Rbl_Eziologia_Cardiop_Document_Scintigrafia.SelectedValue = reader["Scintigrafia"].ToString();
                this.Rbl_Eziologia_Cardiop_Document_Coronarogr.SelectedValue = reader["Coronografia"].ToString();
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Dati_Precedenti_Ospedalizzazioni_Per_Scompenso()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Dati_Precedenti_Ospedalizzazioni_Per_Scompenso", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            {
                                this.Cld1_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataAccesso"].ToString());
                                this.Cld_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataUscita"].ToString());
                                this.Txt_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Diagnosi"].ToString();
                                this.Txt_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Terapia"].ToString();
                                int num2 = Convert.ToInt32(reader["IdClasseNYHA"].ToString());
                                int num3 = Convert.ToInt32(reader["IdClasseNYHADim"].ToString());
                                this.Ddl_Classe_NYHA_Al_Ricovero.SelectedValue = num2.ToString();
                                this.Ddl_Classe_NYHA_Alla_Dimissione.SelectedValue = num3.ToString();
                                break;
                            }
                        case 1:
                            {
                                this.Cld2_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataAccesso"].ToString());
                                this.Cld2_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataUscita"].ToString());
                                this.Txt2_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Diagnosi"].ToString();
                                this.Txt2_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Terapia"].ToString();
                                int num4 = Convert.ToInt32(reader["IdClasseNYHA"].ToString());
                                int num5 = Convert.ToInt32(reader["IdClasseNYHADim"].ToString());
                                this.Ddl2_Classe_NYHA_Al_Ricovero.SelectedValue = num4.ToString();
                                this.Ddl2_Classe_NYHA_Alla_Dimissione.SelectedValue = num5.ToString();
                                break;
                            }
                        case 2:
                            {
                                this.Cld3_DataInizioRicovero_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataAccesso"].ToString());
                                this.Cld3_DataDimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = Utility.DecodificaData(reader["DataUscita"].ToString());
                                this.Txt3_Diagnosi_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Diagnosi"].ToString();
                                this.Txt3_Terapia_Dimissione_Precedenti_Ospedalizzazioni_Per_Scompenso.Text = reader["Terapia"].ToString();
                                int num6 = Convert.ToInt32(reader["IdClasseNYHA"].ToString());
                                int num7 = Convert.ToInt32(reader["IdClasseNYHADim"].ToString());
                                this.Ddl3_Classe_NYHA_Al_Ricovero.SelectedValue = num6.ToString();
                                this.Ddl3_Classe_NYHA_Alla_Dimissione.SelectedValue = num7.ToString();
                                break;
                            }
                    }
                }
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_IMA()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_IMA", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Eziologia_IMA.Text = Utility.DecodificaData(reader["Data"].ToString());
                this.Rbl_Eziologia_IMA_Documentazione_Ecg.SelectedValue = reader["ECG"].ToString();
                this.Rbl_Eziologia_IMA_Documentazione_Eco.SelectedValue = reader["ECO"].ToString();
                this.Rbl_Eziologia_IMA_Documentazione_Scintigrafia.SelectedValue = reader["Scintigrafia"].ToString();
                this.Rbl_Eziologia_IMA_Documentazione_Coronogr.SelectedValue = reader["Coronografia"].ToString();
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool estrai_NYHA(DropDownList dropdown)
    {
        SqlConnection connection = new SqlConnection(this.conn_str);
        bool flag = false;
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_NYHA", connection);
            command.CommandType = CommandType.StoredProcedure;
            SqlDataReader reader = command.ExecuteReader();
            for (int i = 0; reader.Read(); i++)
            {
                ListItem item = new ListItem(reader["Denominazione"].ToString(), reader["idClasseNYHA"].ToString());
                dropdown.Items.Add(item);
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Precedenti_Accessi_Per_Scompenso()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Dati_Precedenti_Accessi_Per_Scompenso", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                for (int i = 0; reader.Read(); i++)
                {
                    switch (i)
                    {
                        case 0:
                            this.Cld_Precedenti_Accessi_Scompenso.Text = Utility.DecodificaData(reader["DataRicovero"].ToString());
                            this.Txt_Diagnosi_Precedenti_Accessi_Scompenso.Text = reader["Diagnosi"].ToString();
                            this.Txt_Terapia_Precedenti_Accessi_Scompenso.Text = reader["TerapiaDimissione"].ToString();
                            break;

                        case 1:
                            this.Cld2_Precedenti_Accessi_Scompenso.Text = Utility.DecodificaData(reader["DataRicovero"].ToString());
                            this.Txt2_Diagnosi_Precedenti_Accessi_Scompenso.Text = reader["Diagnosi"].ToString();
                            this.Txt2_Terapia_Precedenti_Accessi_Scompenso.Text = reader["TerapiaDimissione"].ToString();
                            break;

                        case 2:
                            this.Cld3_Precedenti_Accessi_Scompenso.Text = Utility.DecodificaData(reader["DataRicovero"].ToString());
                            this.Txt3_Diagnosi_Precedenti_Accessi_Scompenso.Text = reader["Diagnosi"].ToString();
                            this.Txt3_Terapia_Precedenti_Accessi_Scompenso.Text = reader["TerapiaDimissione"].ToString();
                            break;
                    }
                }
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Rischi()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Rischi", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                reader.Read();
                this.Cld_Fattori_Rischio_Comorbidita_Fumo.Text = Utility.DecodificaData(reader["FumoData"].ToString());
                this.Ddl_Fattori_Rischio_Fumo_Grado.SelectedValue = reader["FumoValore"].ToString();
                this.Cld_Fattori_Rischio_Comorbidita_Diabete1.Text = Utility.DecodificaData(reader["Diabete1Data"].ToString());
                this.Cld_Fattori_Rischio_Comorbidita_Diabete2.Text = Utility.DecodificaData(reader["Diabete2Data"].ToString());
                this.Txt_diabete1_grado.Text = reader["Diabete1Valore"].ToString();
                this.Txt_diabete2_grado.Text = reader["Diabete2Valore"].ToString();
                this.Txt_Terapie_Praticate_Diabete.Text = reader["TerapieDiabete"].ToString();
                this.Cld_Fattori_Rischio_Comorbidita_Ipert_Arter.Text = Utility.DecodificaData(reader["IperArtData"].ToString());
                this.Ddl_Ipert_Art_Tipologia.SelectedValue = reader["TipoIperArt"].ToString();
                this.Txt_Terapie_Praticate_Ipert_Art.Text = reader["TerapiaIperArt"].ToString();
                this.Rbl_Ipert_Art_Normalizzazione.SelectedValue = reader["NormPA"].ToString();
                this.Rbl_Iperdislipidemia_LDL_Superiore_130.SelectedValue = reader["LDLMag130"].ToString();
                this.Rbl_Iperdislipidemia_HDL_Inferiore_35.SelectedValue = reader["HDLMin35"].ToString();
                this.Rbl_Iperdislipidemia_Trigl_Superiore_170.SelectedValue = reader["TriglMag170"].ToString();
                this.Rbl_Iperdislipidemia_Statine.SelectedValue = reader["Statine"].ToString();
                this.Txt_Iperdislipidemia_Tipo_Farmaco.Text = reader["TipoFarmaco"].ToString();
                string str = "0";
                if (reader["LDLDopo100tra130"].ToString() == "True")
                {
                    str = "1";
                }
                if (reader["LDLDopoMin100"].ToString() == "True")
                {
                    str = "2";
                }
                if (reader["LDLDopoMag130"].ToString() == "True")
                {
                    str = "3";
                }
                this.Rbl_Iperdislipidemia_LDL_Dosi.SelectedValue = str;
                reader.Close();
                return true;
            }
            return false;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool Estrai_Valvolopatie()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Valvolopatie", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                return false;
            }
            reader.Read();
            this.Cld_Eziologia_Valvolopatie.Text = Utility.DecodificaData(reader["DataPrimoRilievo"].ToString());
            this.Rbl_Eziologia_Valvolopatie_Document_Ecg.SelectedValue = reader["DocumentECG"].ToString();
            this.Rbl_Eziologia_Valvolopatie_Document_Eco.SelectedValue = reader["DocumentECO"].ToString();
            int num = Convert.ToInt32(reader["TipoValvolopatia"]);
            reader.Close();
            SqlCommand command2 = new SqlCommand("Estrai_Valvolopatie_Tipo", connection);
            command2.CommandType = CommandType.StoredProcedure;
            command2.Parameters.AddWithValue("@IdTipo", num);
            SqlDataReader reader2 = command2.ExecuteReader();
            if (reader2.HasRows)
            {
                reader2.Read();
                this.Ddl_Eziologia_Valvolopatie_Tipo.SelectedValue = reader2["IdTipoValvolopatia"].ToString();
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader2.Close();
            return flag;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }
    */
    private bool EstraiDati()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("estrai_dati_controllo_non_cardiologico", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@idcontrollo", Request.Params["idQuadro"].ToString());
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    this.txtDataDead.Text = Utility.DecodificaData(reader["Data"].ToString());
                    txtNote.Text = reader["note"].ToString();
                    /*this.txtDataDead.Text = Utility.DecodificaData(reader["DataRegistrazione"].ToString());
                    this.txtNote.Text = reader["Nome"].ToString();
                    this.Txt_Cognome.Text = reader["Cognome"].ToString();
                    this.Txt_Telefono.Text = reader["Telefoni"].ToString();
                    this.Txt_Indirizzo.Text = reader["Indirizzo"].ToString();
                    this.Txt_Codice_Fiscale.Text = reader["CodiceFiscale"].ToString();
                    this.Txt_Eta.Text = reader["Eta"].ToString();
                    this.Txt_Altezza.Text = reader["Altezza"].ToString();
                    this.Txt_Peso1.Text = reader["Peso"].ToString();
                    this.Txt_Circonferenza_Addominale.Text = reader["CircAddominale"].ToString();
                    this.Sesso.SelectedValue = reader["Sesso"].ToString();
                    this.Medico.SelectedValue = reader["IdMedicoCurante"].ToString();
                    this.lb_titolo.Text = "Paziente: " + reader["Cognome"].ToString() + " " + reader["Nome"].ToString();*/
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }
    /*
    protected void Medico_SelectedIndexChanged(object sender, EventArgs e)
    {
        string connectionString = "";
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            connectionString = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        SqlConnection connection = new SqlConnection(connectionString);
        connection.Open();
        try
        {
            int num = Convert.ToInt32(this.Medico.SelectedItem.Value.ToString());
            SqlCommand command = new SqlCommand("Dati_Medico", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@id_medico", num);
            SqlDataReader reader = command.ExecuteReader();
            if (!reader.HasRows)
            {
                this.lb_mess.Text = "";
            }
            else
            {
                reader.Read();
                this.LbInfoMedico.Text = "<b>Indirizzo:</b> " + reader["Indirizzo"].ToString() + "<br><b>Telefono: </b>" + reader["telefoni"].ToString();
                reader.Close();
            }
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
    }
    */
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Utente"] == null) { Response.Redirect("login.aspx"); }
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        if (Request.Params["del"] != null)
        {
            //eliminiamo il controllo
            SqlConnection connection = new SqlConnection(this.conn_str);
            try
            {
                connection.Open();
                SqlCommand command = new SqlCommand("Delete from RicoveroNonCardiologico where idRicovero=" + Request.Params["idRef"] , connection);
                command.CommandType = CommandType.Text;
               
                SqlDataReader reader = command.ExecuteReader();
               
                reader.Close();
                Response.Redirect("HomePageTipo.aspx?idPaziente=" + Request.Params["idPaziente"]);
            }
            catch (Exception exception)
            {
                Response.Write(exception.Message);
            }
            finally
            {
                connection.Close();
            }
            
            return;
        }
        //this.LbInfoMedico.Text = "";
        this.lb_mess.Text = "";
       
        if ((base.Request.Params["idpaziente"] != null) && Utility.IsSignInteger(base.Request.Params["idpaziente"].ToString()))
        {
            this.IdPaziente = Convert.ToInt32(base.Request.Params["idpaziente"]);
        }
        else
        {
            base.Response.Redirect("homepage.aspx");
        }
        this.lb_mess.Text = "";
        if (!base.IsPostBack)
        {
            if (this.IdPaziente.Equals(-1))
            {
                this.lb_titolo.Text = "Reclutamento nuovo paziente";
                this.lb_menu.Text = "";
            }
            else
            {
                string urlControllo = "";
                switch (Session["tipo"].ToString())
                {
                    case "admin":
                        urlControllo = "dativariabilispecialist.aspx";
                       
                        break;
                    case "Medico di medicina generale":
                        urlControllo = "dativariabilimmg.aspx";
                        
                        break;
                    case "Ospedaliero":
                        urlControllo = "dativariabilispecialist.aspx";
                       
                        break;
                    case "Cardiologo ambulatoriale":
                        urlControllo = "dativariabilispecialist.aspx";
                    
                        break;
                }
                 this.lb_menu.Text = string.Concat(new object[] { "<li><a href=\"nuovopaziente.aspx?idpaziente=", this.IdPaziente, "\">Scheda<br />Paziente</a></li><li><a href=\"quadriclinici.aspx?idpaziente=", this.IdPaziente, "\">Controllo<br />Effettuati</a></li><li><a href=\"" + urlControllo + "?idpaziente=", this.IdPaziente, "&idquadro=-1\">Nuovo <br />Controllo</a></li>"});
                if (Session["Tipo"].ToString() == "admin")
                    this.lb_menu.Text = string.Concat(new object[] { "<li><a href=\"nuovopaziente.aspx?idpaziente=", this.IdPaziente, "\">Scheda<br />Paziente</a></li><li><a href=\"quadriclinici.aspx?idpaziente=", this.IdPaziente, "\">Controllo<br />Effettuati</a></li><li><a href=\"" + urlControllo + "?idpaziente=", this.IdPaziente, "&idquadro=-1\">Nuovo <br />Controllo</a></li><li><a onclick=\"return confirm('Sei sicuro di voler eliminare il ricovero corrente?');\" href=\"controlloNonCardiologico.aspx?idpaziente=" + Request.Params["idPaziente"] + "&idRef=" + Request.Params["idQuadro"] + "&del=true\">Elimina Questo <br />Ricovero</a></li>" });
            }
           /* this.ElencaMedici();
            this.ElencaTipoValvolopatie();
            this.estrai_NYHA(this.Ddl_Classe_NYHA_Al_Ricovero);
            this.estrai_NYHA(this.Ddl_Classe_NYHA_Alla_Dimissione);
            this.estrai_NYHA(this.Ddl2_Classe_NYHA_Al_Ricovero);
            this.estrai_NYHA(this.Ddl2_Classe_NYHA_Alla_Dimissione);
            this.estrai_NYHA(this.Ddl3_Classe_NYHA_Al_Ricovero);
            this.estrai_NYHA(this.Ddl3_Classe_NYHA_Alla_Dimissione);
            * */
            if (Request.Params["idQuadro"].ToString() != "-1")
            {
                if (this.EstraiDati())
                {
                   /* this.Estrai_Precedenti_Accessi_Per_Scompenso();
                    this.Estrai_Dati_Precedenti_Ospedalizzazioni_Per_Scompenso();
                    this.Estrai_IMA();
                    this.Estrai_CARDIOP_ISCHEM_SENZA_DOCUMENT_IMA();
                    this.Estrai_Cardiomiopatia_Ipertrofica();
                    this.Estrai_Cardiomiopatia_Dilatativa();
                    this.Estrai_Valvolopatie();
                    this.Estrai_Altre_Eziologie();
                    this.Estrai_Rischi();*/
                }
                else
                {
                    base.Response.Redirect("homepage.aspx");
                }
            }
            //this.CreaMenuStep(this.Wizard1.ActiveStep.ID);
        }
    }

    /*private bool trova_id_ospedalizzazioni(int idpaziente)
    {
        bool flag = false;
        int num = 1;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Dati_Precedenti_Ospedalizzazioni_Per_Scompenso", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", idpaziente);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                switch (num)
                {
                    case 1:
                        this.IdOspedalizzazione1 = Convert.ToInt32(reader["IdOspedalizzazioneScomp"].ToString());
                        break;

                    case 2:
                        this.IdOspedalizzazione2 = Convert.ToInt32(reader["IdOspedalizzazioneScomp"].ToString());
                        break;

                    case 3:
                        this.IdOspedalizzazione3 = Convert.ToInt32(reader["IdOspedalizzazioneScomp"].ToString());
                        break;
                }
                num++;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
            flag = false;
        }
        finally
        {
            connection.Close();
            flag = true;
        }
        return flag;
    }

    private bool trova_id_scompensi(int idpaziente)
    {
        bool flag = false;
        int num = 1;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("Estrai_Dati_Precedenti_Accessi_Per_Scompenso", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", idpaziente);
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                switch (num)
                {
                    case 1:
                        this.IdPrecedenteScomp1 = Convert.ToInt32(reader["IdPrecedentiScompenso"].ToString());
                        break;

                    case 2:
                        this.IdPrecedenteScomp2 = Convert.ToInt32(reader["IdPrecedentiScompenso"].ToString());
                        break;

                    case 3:
                        this.IdPrecedenteScomp3 = Convert.ToInt32(reader["IdPrecedentiScompenso"].ToString());
                        break;
                }
                num++;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            this.lb_mess.Text = exception.Message;
            flag = false;
        }
        finally
        {
            connection.Close();
            flag = true;
        }
        return flag;
    }

    protected void Wizard1_ActiveStepChanged(object sender, EventArgs e)
    {
        this.CreaMenuStep(this.Wizard1.ActiveStep.ID);
    }*/

    protected void Wizard1_FinishButtonClick(object sender, WizardNavigationEventArgs e)
    {
        /*if ((((!this.txtNote.Text.Contains("<") && !this.Txt_Cognome.Text.Contains("<")) && (!this.Txt_Indirizzo.Text.Contains("<") && !this.Txt_Telefono.Text.Contains("<"))) && ((!this.Txt_Codice_Fiscale.Text.Contains("<") && !this.Txt_Eta.Text.Contains("<")) && (!this.Txt_Altezza.Text.Contains("<") && !this.Txt_Peso1.Text.Contains("<")))) && (!this.Txt_Eta.Text.Contains("<") && !this.Txt_Circonferenza_Addominale.Text.Contains("<")))
        {
            if (((this.txtNote.Text.Trim() == "") || (this.Txt_Cognome.Text.Trim() == "")) || (this.Txt_Codice_Fiscale.Text.Trim() == ""))
            {
                this.lb_mess.Text = "Occorre inserire un valore per i dati obbligatori.";
            }
            else if (this.CercaDuplicati())
            {
                this.lb_mess.Text = "Paziente gi\x00e0 esistente. Verificare i dati inseriti.";
            }
            else if (this.ApportaModifiche())
            {*/
        if (this.ApportaModifiche())
        {
            base.Response.Redirect("HomePageTipo.aspx?idpaziente=" + this.IdPaziente);
        }
        else
        {
 
        }
         /*   }
        }
        else
        {
            this.lb_mess.Text = "Per motivi di sicurezza occorre eliminare i caratteri speciali dalle informazioni inserite.";
        }*/
    }

    protected void Wizard1_NextButtonClick(object sender, WizardNavigationEventArgs e)
    {
    }

   
}

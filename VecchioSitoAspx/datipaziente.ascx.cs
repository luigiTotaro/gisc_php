﻿using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.Profile;
using System.Web.UI;
using System.Web.UI.WebControls;
public partial class datipaziente : System.Web.UI.UserControl
{
    private string IdPaziente;


    private bool EstraiDati(string classe_current)
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["conn_str"].ToString());
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("cerca_paziente_medico_id", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    this.lb_nome_paziente.Text = "<span class=\"ToolTextDati\" onMouseOver=\"javascript:this.className='ToolTextDatiHover';\" onMouseOut=\"javascript:this.className='ToolTextDati'\"><b>" + reader["Nome"].ToString() + " " + reader["Cognome"].ToString() + "</b><div><table cellpadding=\"4\" cellspacing=\"4\" width=\"400px\"><tr><td colspan=\"2\" align=\"center\"><b>" + reader["Nome"].ToString() + " " + reader["Cognome"].ToString() + "</b><hr class=\"hr\" /></td></tr><tr><td align=\"right\">Indirizzo</td><td align=\"left\"><b>" + reader["Indirizzo"].ToString() + "</b></td></tr><tr><td align=\"right\">N\x00b0 Telefono</td><td align=\"left\"><b>" + reader["Telefoni"].ToString() + "</b></td></tr><tr><td align=\"right\">Codice Fiscale</td><td align=\"left\"><b>" + reader["CodiceFiscale"].ToString() + "</b></td></tr></table></div></span>";
                    this.lb_dati.Text = " (Sesso: <b>" + reader["Sesso"].ToString() + "</b> - Et&agrave;: <b>" + reader["Eta"].ToString() + "</b> - Medico curante: <b>" + reader["Medico"].ToString() + "</b>)";
                    this.lb_quadri.Text = "<div class=\"quadri_clinici\"><ul><li class=\"" + classe_current + "\"><a href=\"nuovopaziente.aspx?idpaziente=" + reader["IdPaziente"].ToString() + "\">Reclutamento<br />" + Utility.DecodificaData(reader["DataRegistrazione"].ToString()) + "<br />" + reader["Nominativo"].ToString() + "</a></li>";
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_dati.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    private bool EstraiQuadriClinici()
    {
        bool flag = false;
        string urldatipaziente = "";
        string tipo = "";
        string color = "";
        SqlConnection connection = new SqlConnection(ConfigurationManager.AppSettings["conn_str"].ToString());
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("elenca_quadri_clinici_utenti", connection);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@IdPaziente", this.IdPaziente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    
                    switch (reader["tipo"].ToString())
                    {
                        case "admin":
                            urldatipaziente = "dativariabilispecialist.aspx";
                            tipo = " ADMIN";
                            color = "#CCCCCC";
                            break;
                        case "Medico di medicina generale":
                            urldatipaziente = "dativariabilimmg.aspx";
                            tipo = " MMG";
                            color = "#6699CC";
                            break;
                        case "Ospedaliero":
                            urldatipaziente = "dativariabilispecialist.aspx";
                            tipo = " OSPEDALIERO";
                            color = "#9999FF";
                            break;
                        case "Cardiologo ambulatoriale":
                            urldatipaziente = "dativariabilispecialist.aspx";
                            tipo = " SPECIALIST";
                            color = "#9999FF";
                            break; 
                    }

                    if ((base.Request.Params["idquadro"] != null) && base.Request.Params["idquadro"].ToString().Equals(reader["IdQuadroClinico"].ToString()))
                    {
                        object text = this.lb_quadri.Text;
                        this.lb_quadri.Text = string.Concat(new object[] { text, "<li><a style=\"color:#000000;background-color:#99FFFF;\" href=\"" + urldatipaziente + "?idpaziente=", reader["IdPaziente"].ToString(), "&idquadro=", reader["IdQuadroClinico"], "\">Controllo" + tipo + "<br />", Utility.DecodificaData(reader["DataQuadroClinico"].ToString()), "<br />", reader["Nominativo"].ToString(), "</a></li>" });
                    }
                    else 
                    {
                        object obj3 = this.lb_quadri.Text;
                        this.lb_quadri.Text = string.Concat(new object[] { obj3, "<li style=\"background-color: " + color + ";     border-color: " + color + ";\"><a style=\"color:#ffffff;background-color: " + color + ";     border-color: " + color + ";\" href=\"" + urldatipaziente + "?idpaziente=", reader["IdPaziente"].ToString(), "&idquadro=", reader["IdQuadroClinico"], "\">Controllo" + tipo + "<br />", Utility.DecodificaData(reader["DataQuadroClinico"].ToString()), "<br />", reader["Nominativo"].ToString(), "</a></li>" });
                    }
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            this.lb_quadri.Text = this.lb_quadri.Text + "</ul></div>";
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_quadri.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["Utente"] == null) { Response.Redirect("login.aspx"); }
        if ((base.Request.Params["idpaziente"] != null) && !base.Request.Params["idpaziente"].ToString().Equals("-1"))
        {
            this.IdPaziente = base.Request.Params["idpaziente"].ToString();
            this.pn_paziente.Visible = true;
            if ((base.Request.Params["idquadro"] != null) && !base.Request.Params["idquadro"].ToString().Equals("-1"))
            {
                this.EstraiDati("");
            }
            else
            {
                this.EstraiDati("current");
            }
            this.EstraiQuadriClinici();
        }
        else
        {
            this.pn_paziente.Visible = false;
        }
    }

   
}

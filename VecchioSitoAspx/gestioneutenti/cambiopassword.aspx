<%@ page language="C#" autoeventwireup="true" inherits="gestioneutenti_cambiopassword, App_Web_cambiopassword.aspx.9e6583cf" %>
<!--#include file="../include/controllo_utenti.inc"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="../stile.css" type=text/css rel="stylesheet" />
<title>Gestione Utenti</title>

<style type="text/css">
<!--
body {
	background-image: url(../files/bg_blueGrad.gif);
	background-color: #324671;
}
.style1 {color: #FFFFFF}
.Stile1 {color: #336DD0}
-->
</style>
</head>

<body>
   <form id="form1" runat="server">
<table width="812" height="100%" border="0" align="center" cellpadding="0" cellspacing="0" vspace="top">
  
  <tr>
    <td width="10" valign="top" background="../files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="../files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center"><div align="left">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
                <!--#include file="../include/utenti.inc"-->
              <div class="modernbricksmenuline">
                <ul>
                    <li style="margin-left: 1px"><a href="utente.aspx?IdUtente=-1">Nuovo<br />Utente</a></li>
                    <li><a href="utenti.aspx">Cerca<br />Utente</a></li>
                    <asp:Label ID="lb_btn_elimina" runat="server" Text=""></asp:Label>
                </ul>
              </div>
          </td>
          </tr>

      </table>
    </div>
        <br />
        <br />
        <span style="font-size:18px;color:#324671;font-weight:bold"><asp:Label ID="lb_titolo" runat="server" Text=""></asp:Label></span>
        <br />
        <br />
        <table border="0" cellpadding="2" cellspacing="2">
            <tr>
                <td colspan="2" align="center">
                    <asp:Label ID="lb_mess" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label><br />
                    <asp:ValidationSummary id="ValidationSummary1" runat="server"></asp:ValidationSummary>
                </td>
            </tr>
            <tr>
                <td align="right">Nome:
                </td>
                <td align="left">
                    <asp:Label ID="lb_cognome" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">Tipologia:
                </td>
                <td align="left">
                    <asp:Label ID="lb_tipo" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
                <td align="right">Username:
                </td>
                <td align="left">
                    <asp:Label ID="lb_username" runat="server"></asp:Label>
                </td>
            </tr>
            <tr>
				<td align="right">
                    Password*:</td>
				<td align="left">
					<asp:TextBox id="tb_pwd_new1" runat="server" MaxLength="15" TextMode="Password"></asp:TextBox>
					<asp:RequiredFieldValidator id="rfv_1" runat="server" ControlToValidate="tb_pwd_new1" ErrorMessage="Occorre inserire la password."
						Display="None"></asp:RequiredFieldValidator></td>
			</tr>
			<tr>
				<td align="right">
                    Conferma Nuova Password*:</td>
				<td align="left">
					<asp:TextBox id="tb_pwd_new2" runat="server" MaxLength="15" TextMode="Password"></asp:TextBox>
					<asp:RequiredFieldValidator id="rfv_2" runat="server" ControlToValidate="tb_pwd_new2" ErrorMessage="Occorre confermare la password."
						Display="None"></asp:RequiredFieldValidator><asp:comparevalidator id="CompareValidator1" runat="server" ControlToValidate="tb_pwd_new2" ErrorMessage="Occorre confermare correttamente la password."
						Display="None" ControlToCompare="tb_pwd_new1"></asp:comparevalidator></td>
			</tr>
			<tr>
			    <td colspan="2" align="left">* Campi obbligatori</td>
			</tr>
            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btn_invio" runat="server" Text="Applica" OnClick="btn_invio_Click" /></td>
            </tr>
        </table>

    </td>
    <td width="10" valign="top" background="../files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="../files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="../files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="2" style="height: 20px"><!--#include file="../include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="../files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" background="../files/bg_content_bottom.gif">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>
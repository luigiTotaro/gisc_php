﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class logout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Session["IdUtente"] = null;
        this.Session["Utente"] = null;
        this.Session["IdSessione"] = null;
        this.Session["Tipo"] = null;
        base.Response.Redirect("../login.aspx");
    }

    
}

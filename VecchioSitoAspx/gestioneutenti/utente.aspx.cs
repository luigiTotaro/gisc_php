﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

public partial class gestioneutenti_utente : System.Web.UI.Page
{
    // Fields
   
    private string conn_str;

    private string IdUtente;
 
    private string str_mess;


    // Methods
    private bool ApportaModifiche()
    {
        string str;
        bool flag = false;
        if (this.IdUtente.Equals("-1"))
        {
            str = "inserisci_utente";
        }
        else
        {
            str = "modifica_utente";
        }
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand(str, connection);
            command.CommandType = CommandType.StoredProcedure;
            
            if (!this.IdUtente.Equals("-1"))
            {
                command.Parameters.AddWithValue("@IdUtente", this.IdUtente);
            }
            command.Parameters.AddWithValue("@Nominativo", this.tb_cognome.Text.Trim());
            command.Parameters.AddWithValue("@Username", this.tb_username.Text.Trim());
            string str2 = "";
            if (this.IdUtente.Equals("-1"))
            {
                str2 = Utility.Encode(this.tb_username.Text.Trim());
            }
            command.Parameters.AddWithValue("@Password", str2);
            command.Parameters.AddWithValue("@Tipo", this.ddl_tipo.SelectedValue);
            command.Parameters.AddWithValue("@email", this.tb_email.Text);
            if (!this.IdUtente.Equals("-1"))
            {
                command.ExecuteNonQuery();
                this.str_mess = "&mod=1";
                return true;
            }
            SqlDataReader reader = command.ExecuteReader();
            while (reader.Read())
            {
                this.IdUtente = reader["IdUtente"].ToString();
            }
            reader.Close();
            this.str_mess = "&mod=2";
            return true;
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    protected void btn_invio_Click(object sender, EventArgs e)
    {
        if (this.ApportaModifiche())
        {
            base.Response.Redirect("utente.aspx?idutente=" + this.IdUtente + this.str_mess);
        }
    }

    private bool EstraiDati()
    {
        bool flag = false;
        SqlConnection connection = new SqlConnection(this.conn_str);
        try
        {
            connection.Open();
            SqlCommand command = new SqlCommand("cerca_utente_id", connection);
            command.CommandType = CommandType.StoredProcedure;
            
            command.Parameters.AddWithValue("@IdUtente", this.IdUtente);
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    this.lb_titolo.Text = reader["Nominativo"].ToString();
                    this.tb_cognome.Text = reader["Nominativo"].ToString();
                    this.tb_username.Text = reader["Username"].ToString();
                    this.ddl_tipo.SelectedValue = reader["Tipo"].ToString();
                    this.tb_email.Text = reader["email"].ToString();
                }
                flag = true;
            }
            else
            {
                flag = false;
            }
            reader.Close();
        }
        catch (Exception exception)
        {
            flag = false;
            this.lb_mess.Text = exception.Message;
        }
        finally
        {
            connection.Close();
        }
        return flag;
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (base.Request.Params["idutente"] != null)
        {
            this.IdUtente = base.Request.Params["idutente"].ToString();
        }
        else
        {
            base.Response.Redirect("utenti.aspx");
        }
        if (base.Request.Params["mod"] != null)
        {
            if (base.Request.Params["mod"].ToString().Equals("1"))
            {
                this.lb_mess.Text = "La modifica \x00e8 stata effettuata correttamente.";
            }
            if (base.Request.Params["mod"].ToString().Equals("2"))
            {
                this.lb_mess.Text = "Il nuovo utente \x00e8 stato inserito correttamente.";
            }
            if (base.Request.Params["mod"].ToString().Equals("3"))
            {
                this.lb_mess.Text = "La modifica della password \x00e8 stata effettuata correttamente.";
            }
        }
        else
        {
            this.lb_mess.Text = "";
        }
        if (ConfigurationManager.AppSettings["conn_str"] != null)
        {
            this.conn_str = ConfigurationManager.AppSettings["conn_str"].ToString();
        }
        if (!base.IsPostBack)
        {
            if (this.IdUtente.Equals("-1"))
            {
                this.lb_titolo.Text = "Inserimento nuovo utente";
                this.lb_btn_elimina.Text = "";
            }
            else
            {
                this.lb_btn_elimina.Text = "<li><a href=\"utente.aspx?idutente=" + this.IdUtente + "\">Dati<br />Utente</a></li><li><a href=\"cambiopassword.aspx?idutente=" + this.IdUtente + "\">Cambio<br />Password</a></li><li><a onclick=\"return confirm('Sei sicuro di voler eliminare questo utente?');\" href=\"utenti.aspx?del=" + this.IdUtente + "\">Elimina<br />Utente</a></li>";
                this.EstraiDati();
            }
        }
    }

    // Properties
   /* protected HttpApplication ApplicationInstance
    {
        get
        {
            return this.Context.ApplicationInstance;
        }
    }

    protected DefaultProfile Profile
    {
        get
        {
            return (DefaultProfile)this.Context.Profile;
        }
    }*/

}

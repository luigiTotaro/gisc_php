
<%@ Page Language="C#" AutoEventWireup="true" CodeFile="controlloNonCardiologico.aspx.cs" Inherits="controlloNonCardiologico" %>

<%@ Register Src="datipaziente.ascx" TagName="datipaziente" TagPrefix="uc1" %>
<!--#include file="include/controllo.inc"-->
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="stile.css" type=text/css rel="stylesheet" />
<script language="JavaScript" src="./js/ts_picker.js"></script>
<script language="JavaScript" src="./js/controlli.js"></script>
<title>Gestione Paziente</title>

<style type="text/css">
<!--
body {
	background-image: url(./files/bg_blueGrad.gif);
	background-color: #324671;
}
.Stile1 {color: #336DD0}
-->
</style>
</head>

<body>
   <form id="form1" runat="server">
<table width="812" height="100%" border="0" align="center" cellpadding="0" cellspacing="0">
  
  <tr>
    <td width="10" valign="top" background="./files/bg_dropshadow_left_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_left.gif" width="10" height="695" /></td>
    <td width="792" valign="top" bgcolor="#FFFFFF" align="center">
    
    <div align="left" style="margin-bottom:10px">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
        <tr>
          <td><img src="/Cardio/files/testata.jpg" /></td>
          </tr>
        <tr>
          <td>
              <!--#include file="include/menusup.inc"-->

             <div class="modernbricksmenuline">
                <ul>
                    <li style="margin-left: 1px"></li>
                  
                    <asp:Label ID="lb_menu" runat="server" Text=""></asp:Label>
                </ul>
              </div>
          </td>
          </tr>
          <tr>
            <td align="center">
                <br />
                <br />
                <span style="font-size:18px;color:#324671;font-weight:bold"><asp:Label ID="lb_titolo" runat="server" Text=""></asp:Label></span>
                <br />
                <br />
                <uc1:datipaziente ID="Datipaziente1" Visible="false" runat="server" />
            </td>
          </tr>
          <tr>
            <td align="center">
                <br />
                <br />
                I campi contrassegnati con * sono obbligatori
                <br /><br />
                <asp:Label ID="lb_steps" runat="server" Text=""></asp:Label>
            </td>
          </tr>
          <tr>
            <td align="center">
                <asp:Wizard ID="Wizard1" runat="server" ActiveStepIndex="0"
                    FinishPreviousButtonText="Indietro" 
                    OnFinishButtonClick="Wizard1_FinishButtonClick" 
                    StepPreviousButtonText="Indietro" BackColor="#EFF3FB" BorderColor="#B5C7DE" 
                    BorderWidth="1px" DisplaySideBar="False" Font-Names="Verdana" Font-Size="12px" 
                   
                   FinishCompleteButtonText="Salva">
                    <SideBarStyle VerticalAlign="Top" BackColor="#507CD1" Font-Size="12px" />
                    <WizardSteps>
                        <asp:WizardStep ID="WizardStep1" runat="server" Title="DEAD">
                            <div class="titolo_step">RICOVERO NON CARDIOLOGICO</div>
                            <table cellpadding="4" cellspacing="4">
                                <tr>
                                    <td colspan="3" class="titolo_sezione">RICOVERO NON CARDIOLOGICO</td>
                                </tr>
                                <tr>
                                    <td align="right" valign="top">
                                        Data <br />(gg/mm/aaaa)</td>
                                    <td align="left" width="16px" height="16px" valign="top">
                                        <asp:CustomValidator ID="CustomValidator1" runat="server" ClientValidationFunction="ClientValidate"
                                            ControlToValidate="txtDataDead" ErrorMessage="<span class=&quot;ToolText&quot; onMouseOver=&quot;javascript:this.className='ToolTextHover';Wizard1$txtDataDead.focus();&quot; onMouseOut=&quot;javascript:this.className='ToolText'&quot;><img src='files/info.gif' /><div>Occorre inserire una data valida nel formato gg/mm/aaaa</div></span>"></asp:CustomValidator>
                                    </td>
                                    <td align="left">   
                                        <asp:TextBox ID="txtDataDead" runat="server" Columns="10" MaxLength="10"></asp:TextBox>
                                        <a href="javascript:show_calendar('document.form1.Wizard1$txtDataDead', document.form1.Wizard1$txtDataDead.value, 'calendar1');">
                                            <img alt="Seleziona la data" border="0" height="16" src="./files/cal.gif" width="16" /></a>
                                        <div id="calendar1" style="display: none">
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="right" valign="middle">
                                        Note</td>
                                    <td align="left" width="16px" height="16px" valign="middle">
                                        &nbsp;</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtNote" runat="server" MaxLength="255" Columns="30" 
                                            TextMode="MultiLine"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </asp:WizardStep>
                    </WizardSteps>
                    <StepStyle Font-Size="0.8em" ForeColor="#333333" />
                    <NavigationButtonStyle BackColor="White" BorderColor="#507CD1" BorderStyle="Solid"
                        BorderWidth="1px" Font-Names="Verdana" Font-Size="0.8em" ForeColor="#284E98" />
                    <SideBarButtonStyle BackColor="#507CD1" Font-Names="Verdana" ForeColor="White" />
                    <HeaderStyle BackColor="#284E98" BorderColor="#EFF3FB" BorderStyle="Solid" BorderWidth="2px"
                        Font-Bold="True" Font-Size="0.9em" ForeColor="White" HorizontalAlign="Center" />
                </asp:Wizard>

            </td>
          </tr>
      </table>
    </div>
        

        <br />
        <br />
        <asp:Label ID="lb_mess" runat="server" Text="" ForeColor="Red"></asp:Label>
    
    </td>
    <td width="10" valign="top" background="./files/bg_dropshadow_right_dark.gif" style="background-repeat:repeat-y"><img src="./files/bg_dropshadow_right.gif" width="10" height="695" /></td>
  </tr>
  <tr>
    <td background="./files/bg_dropshadow_left_dark.gif">&nbsp;</td>
    <td bgcolor="#FFFFFF"><table cellspacing="0" cellpadding="0" width="100%" bgcolor="#006699">
      <tbody>
        <tr>
          <td align="left" width="20"></td>
          <td height="20"></td>
          <td></td>
        </tr>
        <tr>
          <td align="left" width="20" bgcolor="#383B8A" style="height: 20px"></td>
          <td align="left" bgcolor="#383B8A" colspan="2" style="height: 20px"><!--#include file="include/menu_footer.inc" --></td>
        </tr>
        <tr>
          <td align="left" width="20"></td>
          <td align="left"></td>
        </tr>
      </tbody>
    </table></td>
    <td background="./files/bg_dropshadow_right_dark.gif">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3" background="./files/bg_content_bottom.gif">&nbsp;</td>
  </tr>
</table>
</form>
</body>
</html>

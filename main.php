<?php
ob_start(); 
session_name( 'PHPSESSID' );
session_start();

    //se non loggato ritorno a login
    if (!isset($_SESSION['IdUtente'])){
        header("location: index.php");//redirect
    }else{
        $idUtente = $_SESSION['IdUtente'];
        $nominativo = $_SESSION['Nominativo'];
        $tipologiaUtente = $_SESSION['Tipologia'];
    }

    //echo $tipologiaUtente;

    include('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
	$db = Xcrud_db::get_instance();

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Some page title</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
</head>
 
<body style="background-color:#cccccc;">

    <div id="contenuto" style="width:1024px;background-color:#FFFFFF;display:none;">
 
        <div style="width:100%">
            <img src="img/testataGisc.jpg" style="width:100%;">
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="logoff();">Esci</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="nuovoreclutamento();">Nuovo Reclutamento</button>
            <?     
                if ($tipologiaUtente=="Admin")
                {
                    echo '<button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goStatistiche();">Statistiche</button>';
                    //echo '<button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goUtenti();">Utenti</button>';
                    echo '<div class="dropdown" style="float:right;">
                            <button class="btn btn-primary xcrud-action dropdown-toggle" style="padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                Utenti
                                <span class="caret"></span>
                            </button>
                            <ul class="color-btn-primary dropdown-menu " aria-labelledby="dropdownMenu1">
                                <li><a href="utenti.php">Lista Utenti</a></li>
                                <li><a href="utenti.php?id=-1">Nuovo Utente</a></li>
                            </ul>
                        </div>';

                }
            ?>            
            <div style="clear:both;"></div>


        </div>

        <div style="width:100%;padding:5%;">
            <?php
                //alert
                
                /*
                if ($tipologiaUtente!="Admin")
                {
                    if ($tipologiaUtente=="Cardiologo Ambulatoriale")
                    {
                        $sel = "select * from paziente";
                    }
                    else if ($tipologiaUtente=="Medico di Medicina Generale")
                    {
                        $sel = "Select * from Paziente where idMedicoCurante=".$idUtente;
                    }
                    else if ($tipologiaUtente=="Specialista Pneumologo")
                    {
                        $sel = "Select * from Paziente where idPneumologo=".$idUtente;
                    }
                    else if ($tipologiaUtente=="Specialista Nefrologo")
                    {
                        $sel = "Select * from Paziente where idNefrologo=".$idUtente;
                    }
                    else if ($tipologiaUtente=="Nutrizionista")
                    {
                        $sel = "Select * from Paziente where idNutrizionista=".$idUtente;
                    }
                    $db->query($sel);
                    $result = $db->result();
                    $prossimiControlli="";
                    
                    foreach ($result as $paziente) {
                        //per ogni paziente vedo i suoi alert prossimi
                        $nomeCognome = $paziente["Cognome"].' '.$paziente["Nome"];
                        if ($tipologiaUtente == "Medico di medicina generale")
                        {
                            $alertSql = "Select DATE_FORMAT(DataProssimoQuadroClinico, '%d-%m-%Y') as DataProssimoQuadroClinicoFormat, DataQuadroClinico,idQuadroClinico from QuadroClinico where idPaziente=".$paziente['IdPaziente']." and gTipo IS NULL and gMinnesota1 IS NOT NULL or (gTipo ='RECLUTAMENTO' and idPaziente=".$paziente['IdPaziente'].") order by gTipo,QuadroClinico.DataQuadroClinico";
                            $tipoControllo = " di base ";
                        }
                        else
                        {
                            $alertSql = "Select DATE_FORMAT(DataProssimoQuadroClinico, '%d-%m-%Y') as DataProssimoQuadroClinicoFormat ,DataQuadroClinico, idQuadroClinico from QuadroClinico where idPaziente=".$paziente['IdPaziente']." and gMinnesota1 IS  NULL and gTipo is NULL or (gTipo ='RECLUTAMENTO' and idPaziente=".$paziente['IdPaziente'].") order by gTipo,QuadroClinico.DataQuadroClinico";
                            $tipoControllo = " specialistico ";
                        }
                        
                        $db->query($alertSql);
                        $resultAlert = $db->result();
                        
                       //echo '<pre>'; print_r($resultAlert); echo '</pre>';

                        if (count($resultAlert) > 0)
                        {
                            if (($resultAlert[0]["DataProssimoQuadroClinicoFormat"] != "") && ($resultAlert[0]["DataProssimoQuadroClinicoFormat"] != "00-00-0000"))
                            {
                                $prossimiControlli .= "<a href='storia.php?id=".$paziente['IdPaziente']."'>". $nomeCognome . "</a>: prossimo controllo " . $tipoControllo . " previsto il :" . $resultAlert[0]["DataProssimoQuadroClinicoFormat"] . "<br/>";
                            }
                        }       

                    }
                }

                */

            	$sel = "select * from paziente";
                $db->query($sel);
                $result = $db->result();
            	$pazTot = count($result);

            	$sel = "select * from paziente where idUtente=".$idUtente;
                $db->query($sel);
                $result = $db->result();
            	$pazUtente = count($result);

            ?>    
            <h2 class="titolo">Benvenuto <? echo $nominativo ?> </h2>
            <h4>Pazienti Reclutati: <? echo $pazTot ?> - Pazienti da te reclutati: <? echo $pazUtente ?></h4>

            <?php
            if ($tipologiaUtente!="Admin")
            {
            ?>    
                <!--
                <div class="panel-group" id="accordionProssimiControlli" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordionPiePazienti" href="#collapseProssimiControlli" aria-expanded="true" aria-controls="collapseOne">
                          Prossimi controlli previsti
                        </a>
                      </h4>
                    </div>
                    <div id="collapseProssimiControlli" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                            <? echo $prossimiControlli; ?> 
                      </div>
                    </div>
                  </div>
                </div>
                -->
            <?php
            }

            	//devo prendere i nominativi dei medici, per creare la lista di selezione nella ricerca avanzata del paziente
            	$sel = "SELECT Nominativo from utente, tipo_utente where utente.IdTipoUtente=tipo_utente.IdTipoUtente AND tipo_utente.Nome='Medico di Medicina Generale' order by Nominativo asc";
                $db->query($sel);
                $result = $db->result();
                $elencoMedici="";
                foreach ($result as $medico) {
                    $elencoMedici=$elencoMedici.$medico["Nominativo"].",";
                }

                $xcrud->table('paziente');
                $xcrud->join('IdMedicoCurante','utente','IdUtente');
                $xcrud->change_type('utente.Nominativo','select','',$elencoMedici);
                $xcrud->column_name('CodiceFiscale','Codice Fiscale');
                $xcrud->column_name('utente.Nominativo','Nome Medico');
                $xcrud->column_name('DataRegistrazione','Data Reclutamento');
                $xcrud->search_columns('Cognome, CodiceFiscale, DataRegistrazione, utente.Nominativo','');
            	$xcrud->columns('Cognome,Nome,DataRegistrazione,CodiceFiscale,utente.Nominativo');
                //$xcrud->column_pattern('Cognome', '*****');

                //filtri
                if ($tipologiaUtente=="Cardiologo Ambulatoriale")
                {
                   
                }
                else if ($tipologiaUtente=="Medico di Medicina Generale")
                {
                    $xcrud->where('idMedicoCurante =', $idUtente);
                }
                else if ($tipologiaUtente=="Specialista Pneumologo")
                {
                    $xcrud->where('idPneumologo =', $idUtente);
                }
                else if ($tipologiaUtente=="Specialista Nefrologo")
                {
                    $xcrud->where('idNefrologo =', $idUtente);
                }
                else if ($tipologiaUtente=="Nutrizionista")
                {
                    $xcrud->where('idNutrizionista =', $idUtente);
                }
                else if ($tipologiaUtente=="Diabetologo")
                {
                    $xcrud->where('idDiabetologo =', $idUtente);
                }

                $xcrud->button('storia.php?id={idPaziente}','Seleziona','glyphicon glyphicon-th-list');
                $xcrud->button('schedaPaziente.php?id={idPaziente}&type=RECL','Modifica','glyphicon glyphicon-pencil');
                $xcrud->table_name('Lista Pazienti');
                
                $xcrud->unset_add( true ); 
                $xcrud->unset_edit( true );
                $xcrud->unset_view( true );
                $xcrud->unset_remove( true );
                $xcrud->unset_csv( true );
                $xcrud->unset_print( true );
                //$xcrud->benchmark ( true );

            	echo $xcrud->render();

            ?>
        
        </div>
    </div>

    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <script>

    function nuovoreclutamento()
    {
        window.location.href = 'schedaPaziente.php?id=-1';
    }

    function logoff()
    {
        window.location.href = 'logout.php';
    }
    
    function goStatistiche()
    {
        window.location.href = 'statistiche.php';
    }

    function goUtenti()
    {
        window.location.href = 'utenti.php';
    }

    $( window ).resize(function() {
      var larghezza=$(window).width();
      if (larghezza>=1024) $('#contenuto').css("width","1024px");
      else $('#contenuto').css("width","100%");
      var larghFinestra=$('#contenuto').width();
      //console.log(larghFinestra);
      //console.log(larghezza);
      $('#contenuto').css("margin-left",(larghezza-larghFinestra)/2+"px");

    });

    jQuery(document).ready(function() { 

        $(window).trigger('resize');
        $('#contenuto').show();

    });
    
    $( ".xcrud-row" ).click(function(event) {
      var cella = $(event.currentTarget.childNodes[6]);
      var span=$(cella[0].childNodes[0]);
      var button=$(span[0].childNodes[0])
      //console.log(button[0].href);
      window.location.href = button[0].href;
    });

    
    </script>


</body>
</html>
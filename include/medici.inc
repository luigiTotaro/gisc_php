<div class="modernbricksmenu">
  <ul>
    <li style="margin-left: 1px">
      <% if (Request.Params["idPaziente"] != null) {%>
        <a href="HomePageTipo.aspx?idPaziente="<%=Request.Params["idPaziente"]%>">Pannello</a>
      <% } else { %>
        <a href="../HomePageTipo.aspx">Pannello</a>
      </a>
      <% } %>
    </li>
    
    <li style="margin-left: 1px"><a href="../cercapaziente.aspx">Pazienti</a></li>

    <% if (Session["tipo"].ToString() == "admin") { %>
    <li class="current">
      <a href="cercamedico.aspx">Medici</a>
    </li>
    
    <% } %>

    <li>
      <a href="../statistiche.aspx">Statistiche</a>
    </li>

    <li>
      <a href="../gestioneutenti/utenti.aspx">Utenti</a>
    </li>

    <!-- federico pici: commentato e sostituito con i due link qui di sopra -->
    <!--<asp:Label ID="lb_gest_utenti" runat="server" Text=""></asp:Label>-->

    <li><a href="../logout.aspx">Esci</a></li>	
  </ul>
</div>
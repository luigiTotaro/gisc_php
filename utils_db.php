<?php
class utils_db{
    

    public function getElencoMedici($db)
    {
        $sel = "SELECT IdUtente, Nominativo from utente, tipo_utente where utente.IdTipoUtente=tipo_utente.IdTipoUtente AND tipo_utente.Nome='Medico di Medicina Generale' order by Nominativo asc";
        $db->query($sel);
        $result = $db->result();
        $elencoMedici=array();
        foreach ($result as $medico) {
            $elencoMedici[$medico["IdUtente"]]=$medico["Nominativo"];
        }
        return $elencoMedici;
    }

    public function getElencoPneumologi($db)
    {
        $sel = "SELECT IdUtente, Nominativo from utente, tipo_utente where utente.IdTipoUtente=tipo_utente.IdTipoUtente AND tipo_utente.Nome='Specialista Pneumologo' order by Nominativo asc";
        $db->query($sel);
        $result = $db->result();
        $elencoMedici=array();
        foreach ($result as $medico) {
            $elencoMedici[$medico["IdUtente"]]=$medico["Nominativo"];
        }
        return $elencoMedici;
    }

    public function getElencoNefrologi($db)
    {
        $sel = "SELECT IdUtente, Nominativo from utente, tipo_utente where utente.IdTipoUtente=tipo_utente.IdTipoUtente AND tipo_utente.Nome='Specialista Nefrologo' order by Nominativo asc";
        $db->query($sel);
        $result = $db->result();
        $elencoMedici=array();
        foreach ($result as $medico) {
            $elencoMedici[$medico["IdUtente"]]=$medico["Nominativo"];
        }
        return $elencoMedici;
    }

    public function getElencoNutrizionisti($db)
    {
        $sel = "SELECT IdUtente, Nominativo from utente, tipo_utente where utente.IdTipoUtente=tipo_utente.IdTipoUtente AND tipo_utente.Nome='Nutrizionista' order by Nominativo asc";
        $db->query($sel);
        $result = $db->result();
        $elencoMedici=array();
        foreach ($result as $medico) {
            $elencoMedici[$medico["IdUtente"]]=$medico["Nominativo"];
        }
        return $elencoMedici;
    }

    public function getElencoDiabetologi($db)
    {
        $sel = "SELECT IdUtente, Nominativo from utente, tipo_utente where utente.IdTipoUtente=tipo_utente.IdTipoUtente AND tipo_utente.Nome='Diabetologo' order by Nominativo asc";
        $db->query($sel);
        $result = $db->result();
        $elencoMedici=array();
        foreach ($result as $medico) {
            $elencoMedici[$medico["IdUtente"]]=$medico["Nominativo"];
        }
        return $elencoMedici;
    }


    public function getTipologiaUtenti($db)
    {
        $sel = "SELECT IdTipoUtente, Nome from tipo_utente order by Nome asc";
        $db->query($sel);
        $result = $db->result();

        $elencoTipologia=array();
        foreach ($result as $tipologia) {
            $elencoTipologia[$tipologia["IdTipoUtente"]]=$tipologia["Nome"];
        }       
        return $elencoTipologia;
    }

    public function getOpzioniFumo()
    {
        $opzioniFumo=array();
        $opzioniFumo["si"]="Si";
        $opzioniFumo["no"]="No";
        $opzioniFumo["ex"]="Ex Fumatore";
        return $opzioniFumo;
    }

    public function getOpzioniIpertensioneArt()
    {
        $opzioniIpertensioneArt=array();
        $opzioniIpertensioneArt[1]="Nessuno";
        $opzioniIpertensioneArt[2]="Grado 1 (140/90 < PA < 160/100)";
        $opzioniIpertensioneArt[3]="Grado 2 (160/100 < PA < 180/110)";
        $opzioniIpertensioneArt[4]="Grado 3 (PA > 180/110)";
        return $opzioniIpertensioneArt;
    }

    public function getOpzioniAnemia()
    {
        $opzioniAnemia=array();
        $opzioniAnemia[1]="Nessuno";
        $opzioniAnemia[2]="Lieve (12 < Hb < 10 g/dL)";
        $opzioniAnemia[3]="Moderata (10 < Hb < 8 g/dL)";
        $opzioniAnemia[4]="Grave (Hb < 8 g/dL)";
        return $opzioniAnemia;
    }

    public function getOpzioniCauseScompenso()
    {
        $opzioniScompenso=array();
        $opzioniScompenso[1]="Coronaropatia";
        $opzioniScompenso[2]="Ipertensione";
        $opzioniScompenso[3]="Cardiomiopatia";
        $opzioniScompenso[4]="Difetti valvolari e difetti cardiaci congeniti";
        $opzioniScompenso[5]="Difetti del ritmo cardiaco";
        $opzioniScompenso[6]="Alcool e/o sostanze alternati";
        $opzioniScompenso[7]="Scompenso cardiaco ad alta gittata (High-output failure) da anemia, tireotossicosi, fistola arterovenosa, malattia di Paget";
        $opzioniScompenso[8]="Patologie a carico del pericardio";
        $opzioniScompenso[9]="Scompenso cardiaco destro primario";
        $opzioniScompenso[10]="Altra eziologia";
        return $opzioniScompenso;
    }

    public function getOpzioniCardiomiopatia()
    {
        $opzioniCardiomiopatia=array();
        $opzioniCardiomiopatia[1]="Cardiomiopatia dilatativa (primitiva)";
        $opzioniCardiomiopatia[2]="Cardiomiopatia ipertofica/ostruttiva";
        $opzioniCardiomiopatia[3]="Cardiomiopatia restrittiva (es. in caso di amiloidosi, sarcoidosi, emocromatosi o fibrosi endomiocardica)";
        $opzioniCardiomiopatia[4]="Cardiomiopatia Ipertensiva";
        return $opzioniCardiomiopatia;
    }

    public function getOpzioniCoronaropatia()
    {
        $opzioniCoronaropatia=array();
        $opzioniCoronaropatia[1]="Infarto miocardico";
        $opzioniCoronaropatia[2]="Ischemia";
       return $opzioniCoronaropatia;
    }

    public function getOpzioniDifettiValvolariCardiaci()
    {
        $opzioniDifettiValvolariCardiaci=array();
        $opzioniDifettiValvolariCardiaci[0]="";
        $opzioniDifettiValvolariCardiaci[1]="Aortica";
        $opzioniDifettiValvolariCardiaci[2]="Mitralica";
        $opzioniDifettiValvolariCardiaci[3]="Tricuspidale";
        $opzioniDifettiValvolariCardiaci[4]="Polmonare";
        $opzioniDifettiValvolariCardiaci[5]="Mitro-Aortica";
        $opzioniDifettiValvolariCardiaci[6]="Protesi Aortica";
        $opzioniDifettiValvolariCardiaci[7]="Protesi Mitralica";
       return $opzioniDifettiValvolariCardiaci;
    }


    public function getOpzioniDifettiRitmoCardiaco()
    {
        $opzioniDifettiRitmoCardiaco=array();
        $opzioniDifettiRitmoCardiaco[1]="Tachicardia";
        $opzioniDifettiRitmoCardiaco[2]="Bradicardia (blocco atrioventricolare completo, sick-sinus syndrome)";
        $opzioniDifettiRitmoCardiaco[3]="Difetti della conduzione atriale (es. fibrillazione atriale";
       return $opzioniDifettiRitmoCardiaco;
    }

    public function getOpzioniAlcoolSostenteAlteranti()
    {
        $opzioniAlcoolSostenteAlteranti=array();
        $opzioniAlcoolSostenteAlteranti[1]="Alcool";
        $opzioniAlcoolSostenteAlteranti[2]="Sostanze alteranti la funzionalità cardiaca (es. farmaci beta-bloccanti o calcio-antagonisti)";
       return $opzioniAlcoolSostenteAlteranti;
    }

    public function getOpzioniPatologiePericardio()
    {
        $opzioniPatologiePericardio=array();
        $opzioniPatologiePericardio[1]="Pericardite costrittiva";
        $opzioniPatologiePericardio[2]="Versamento pericardico";
       return $opzioniPatologiePericardio;
    }  

    public function getOpzioniScompensoCardiacoPrimario()
    {
        $getOpzioniScompensoCardiacoPrimario=array();
        $getOpzioniScompensoCardiacoPrimario[1]="Ipertensione polmonare (es. embolia polmonare, cuore polmonare)";
        $getOpzioniScompensoCardiacoPrimario[2]="Insufficienza tricuspidalica";
       return $getOpzioniScompensoCardiacoPrimario;
    }   

    public function getOpzioniCardiologoDisturbiRespiratori()
    {
        $getOpzioniCardiologoDisturbiRespiratori=array();
        $getOpzioniCardiologoDisturbiRespiratori[1]="Dispnea parossistica notturna";
        $getOpzioniCardiologoDisturbiRespiratori[2]="Ortopnea";
        $getOpzioniCardiologoDisturbiRespiratori[3]="Apnea notturna";
        $getOpzioniCardiologoDisturbiRespiratori[4]="Edema polmonare acuto";
        $getOpzioniCardiologoDisturbiRespiratori[5]="Dispnea";
       return $getOpzioniCardiologoDisturbiRespiratori;
    }   

    public function getClasseNYHA()
    {
        $getClasseNYHA=array();
        $getClasseNYHA["I classe"]="Classe I";
        $getClasseNYHA["II classe"]="Classe II";
        $getClasseNYHA["III classe"]="Classe III";
        $getClasseNYHA["IV classe"]="Classe IV";
        return $getClasseNYHA;
    } 

    public function getContrazioneDiuresi()
    {
        $ret=array();
        $ret["Assente"]="Assente";
        $ret["Lieve"]="Lieve";
        $ret["Moderata"]="Moderata";
        $ret["Marcata"]="Marcata";
        return $ret;
    }
 
    public function getOpzioniToniCardiaci()
    {
        $ret=array();
        $ret["Validi"]="Validi";
        $ret["Ridotti"]="Ridotti";
        return $ret;
    }   

    public function getCongestionePolmonare()
    {
        $ret=array();
        $ret["Assente"]="Assente";
        $ret["Lieve"]="Lieve";
        $ret["Moderata"]="Moderata";
        $ret["Grave"]="Grave";
        return $ret;
    }
     
    public function getOpzioniCapacitaMovimento()
    {
        $ret=array();
        $ret[1]="Non ho difficoltà nel camminare";
        $ret[2]="Ho lievi difficoltà nel camminare";
        $ret[3]="Ho moderate difficoltà nel camminare";
        $ret[4]="Non sono in grado di camminare";
        return $ret;
    } 

    public function getOpzioniCuraPersona()
    {
        $ret=array();
        $ret[1]="Non ho difficoltà nel lavarmi o vestirmi";
        $ret[2]="Ho lievi difficoltà nel lavarmi o vestirmi";
        $ret[3]="Ho moderate difficoltà nel lavarmi o vestirmi";
        $ret[4]="Ho gravi difficoltà nel lavarmi o vestirmi";
        $ret[5]="Non sono in grado di lavarmi o vestirmi";
        return $ret;
    } 

    public function getOpzioniAttivitaAbituali()
    {
        $ret=array();
        $ret[1]="Non ho difficoltà nello svolgimento delle attività abituali";
        $ret[2]="Ho lievi difficoltà nello svolgimento delle attività abituali";
        $ret[3]="Ho moderate difficoltà nello svolgimento delle attività abituali";
        $ret[4]="Ho gravi difficoltà nello svolgimento delle attività abituali";
        $ret[5]="Non sono in grado di svolgere le mie attività abituali";
        return $ret;
    } 

    public function getOpzioniDoloreFastidio()
    {
        $ret=array();
        $ret[1]="Non provo alcun dolore o fastidio";
        $ret[2]="Provo lieve dolore o fastidio";
        $ret[3]="Provo moderato dolore o fastidio";
        $ret[4]="Provo grave dolore o fastidio";
        $ret[5]="Provo estremo dolore o fastidio";
        return $ret;
    } 

    public function getOpzioniAnsiaDepressione()
    {
        $ret=array();
        $ret[1]="Non sono ansioso/a o depresso/a";
        $ret[2]="Sono lievemente ansioso/a o depresso/a";
        $ret[3]="Sono moderatamente ansioso/a o depresso/a";
        $ret[4]="Sono gravemente ansioso/a o depresso/a";
        $ret[5]="Sono estremamente ansioso/a o depresso/a";
        return $ret;
    } 

    public function getOpzioniTempoAddormentamento()
    {
        $ret=array();
        $ret[1]="≤ 15 minuti";
        $ret[2]="16-30 minuti";
        $ret[3]="31-60 minuti";
        $ret[4]="> 60 minuti";
        return $ret;
    } 

    public function getOpzioniTempoSonno()
    {
        $ret=array();
        $ret[1]="> 7 ore";
        $ret[2]="6-7 ore";
        $ret[3]="5-6 ore";
        $ret[4]="< 5 ore";
        return $ret;
    } 

    public function getOpzioniProblemiGenericiSonno()
    {
        $ret=array();
        $ret[1]="Non durante l'ultimo mese";
        $ret[2]="Meno di una volta a settimana";
        $ret[3]="Una o due volte a settimana";
        $ret[4]="Tre o più volte a settimana";
        return $ret;
    } 


    public function getOpzioniQualitaSonno()
    {
        $ret=array();
        $ret[1]="Molto buona";
        $ret[2]="Abbastanza buona";
        $ret[3]="Abbastanza cattiva";
        $ret[4]="Molto cattiva";
        return $ret;
    } 

    public function getOpzioniEnergieSufficienti()
    {
        $ret=array();
        $ret[1]="Per niente";
        $ret[2]="Poco";
        $ret[3]="Abbastanza";
        $ret[4]="Molto";
        return $ret;
    }

    public function componiTabellaTerapia($data)
    {
        $ret="";
        if ($data["Amiodarone"]==1) $ret.="Amiodarone - Posologia: ".$data["AmiodaronePosologia"]."<br>";
        if ($data["Digitale"]==1) $ret.="Digitale - Posologia: ".$data["DigitalePosologia"]."<br>";
        if ($data["Dronedarone"]==1) $ret.="Dronedarone - Posologia: ".$data["DronedaronePosologia"]."<br>";
        if ($data["Flecainide"]==1) $ret.="Flecainide - Posologia: ".$data["FlecainidePosologia"]."<br>";
        if ($data["Ivabradina"]==1) $ret.="Ivabradina - Posologia: ".$data["IvabradinaPosologia"]."<br>";
        if ($data["Propafenone"]==1) $ret.="Propafenone - Posologia: ".$data["Propafenone"]."<br>";
        if ($data["AcidoActeilsalicilico"]==1) $ret.="Acido Acteilsalicilico - Posologia: ".$data["AcidoActeilsalicilicoPosologia"]."<br>";
        if ($data["Clopidogrel"]==1) $ret.="Clopidogrel - Posologia: ".$data["ClopidogrelPosologia"]."<br>";
        if ($data["Ticlopidina"]==1) $ret.="Ticlopidina - Posologia: ".$data["TiclopidinaPosologia"]."<br>";
        if ($data["Candesartan"]==1) $ret.="Candesartan - Posologia: ".$data["CandesartanPosologia"]."<br>";
        if ($data["Irbesartan"]==1) $ret.="Irbesartan - Posologia: ".$data["IrbesartanPosologia"]."<br>";
        if ($data["Lisinopril"]==1) $ret.="Lisinopril - Posologia: ".$data["LisinoprilPosologia"]."<br>";
        if ($data["Losartan"]==1) $ret.="Losartan - Posologia: ".$data["LosartanPosologia"]."<br>";
        if ($data["Nitrato"]==1) $ret.="Nitrato - Posologia: ".$data["NitratoPosologia"]."<br>";
        if ($data["Nitroglicerina"]==1) $ret.="Nitroglicerina - Posologia: ".$data["NitroglicerinaPosologia"]."<br>";
        if ($data["Torasemide"]==1) $ret.="Torasemide - Posologia: ".$data["TorasemidePosologia"]."<br>";
        if ($data["Valsartan"]==1) $ret.="Valsartan - Posologia: ".$data["ValsartanPosologia"]."<br>";
        if ($data["Atorvastatina"]==1) $ret.="Atorvastatina - Posologia: ".$data["AtorvastatinaPosologia"]."<br>";
        if ($data["Ezetimibe"]==1) $ret.="Ezetimibe - Posologia: ".$data["EzetimibePosologia"]."<br>";
        if ($data["Omegapolienoici"]==1) $ret.="Omega polienoici - Posologia: ".$data["OmegapolienoiciPosologia"]."<br>";
        if ($data["Pravastatina"]==1) $ret.="Pravastatina - Posologia: ".$data["PravastatinaPosologia"]."<br>";
        if ($data["Rosuvastatina"]==1) $ret.="Rosuvastatina - Posologia: ".$data["RosuvastatinaPosologia"]."<br>";
        if ($data["Sinvastatina"]==1) $ret.="Sinvastatina - Posologia: ".$data["SinvastatinaPosologia"]."<br>";
        if ($data["Enalapril"]==1) $ret.="Enalapril - Posologia: ".$data["EnalaprilPosologia"]."<br>";
        if ($data["Ramipril"]==1) $ret.="Ramipril - Posologia: ".$data["RamiprilPosologia"]."<br>";
        if ($data["Zofenopril"]==1) $ret.="Zofenopril - Posologia: ".$data["ZofenoprilPosologia"]."<br>";
        if ($data["Furosemide"]==1) $ret.="Furosemide - Posologia: ".$data["FurosemidePosologia"]."<br>";
        if ($data["FurosemideSpironolattone"]==1) $ret.="Furosemide-Spironolattone - Posologia: ".$data["FurosemideSpironolattonePosologia"]."<br>";
        if ($data["CanrenoatoPotassio"]==1) $ret.="Canrenoato di Potassio - Posologia: ".$data["CanrenoatoPotassioPosologia"]."<br>";
        if ($data["Spironolattone"]==1) $ret.="Spironolattone - Posologia: ".$data["SpironolattonePosologia"]."<br>";
        if ($data["Bisoprololo"]==1) $ret.="Bisoprololo - Posologia: ".$data["BisoprololoPosologia"]."<br>";
        if ($data["Carvedilolo"]==1) $ret.="Carvedilolo - Posologia: ".$data["CarvediloloPosologia"]."<br>";
        if ($data["Metoprololo"]==1) $ret.="Metoprololo - Posologia: ".$data["MetoprololoPosologia"]."<br>";
        if ($data["Nebivololo"]==1) $ret.="Nebivololo - Posologia: ".$data["NebivololoPosologia"]."<br>";
        if ($data["Antidepressivi"]==1) $ret.="Antidepressivi"."<br>";
        if ($data["Anticoagulanti"]==1) $ret.="Anticoagulanti"."<br>";
        if ($data["Eritropietina"]==1) $ret.="Eritropoietina"."<br>";
        if ($data["Isulina"]==1) $ret.="Insulina"."<br>";
        if ($data["Ipoglicemizzanti"]==1) $ret.="Ipoglicemizzanti"."<br>";
        if ($data["Altrifarmaci"]==1) $ret.="Altrifarmaci - Principi Attivi:".$data["AltrifarmaciPrincipiAttivi"]."<br>";
        return $ret;

     }

    public function getOpzioniInvertite()
    {
        $opzioni=array();
        $opzioni[0]="Si";
        $opzioni[1]="No";
        return $opzioni;
    }

    public function getOpzioniCapacitaDeambulazione()
    {
        $opzioni=array();
        $opzioni[0]="Paziente immobile/allettato";
        $opzioni[1]="Paziente costretto su sedia";
        $opzioni[2]="Paziente cammina con l'aiuto di ausili o con l'aiuto di un'altra persona";
        $opzioni[3]="Paziente deambula in modo autonomo";
        return $opzioni;
    }

    public function getClassificazioneMalattiaRenaleCronica()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Stadio 1, funzione normale o aumentata (VFG > 90)";
        $opzioni[2]="Stadio 2, lieve compromissione funzionale (VFG 89-60)";
        $opzioni[3]="Stadio 3A, compromissione funzionale moderata (VFG 59-45)";
        $opzioni[4]="Stadio 3B, compromissione funzionale moderata (VFG 44-30)";
        $opzioni[5]="Stadio 4, compromissione funzionale grave (VFG 29-15)";
        $opzioni[6]="Stadio 5, insufficienza renale terminale (VFG < 15 o dialisi)";
        return $opzioni;
    }

    public function getClassificazioneAlbuminuriaCreatinuria()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Stadio A1 (≤30 mg/g creatininuria)";
        $opzioni[2]="Stadio A2 (31-300 mg/g creatininuria)";
        $opzioni[3]="Stadio A3 (≥300 mg/g creatininuria)";
        return $opzioni;
    }

    public function getNutrizioneCaloAlimentare()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Grave calo dell'apporto alimentare";
        $opzioni[2]="Moderato calo dell'apporto alimentare";
        $opzioni[3]="Nessun calo dell'apporto alimentare";
        return $opzioni;
    }

    public function getNutrizionePerditaPeso()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Calo ponderale superiore a 3 kg";
        $opzioni[2]="Non sa";
        $opzioni[3]="Calo ponderale compreso tra 1 e 3 kg";
        $opzioni[4]="Assenza di calo ponderale";
        return $opzioni;
    }  

    public function getNutrizioneMobilita()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Costretto a letto o su una poltrona";
        $opzioni[2]="In grado di alzarsi dal letto/dalla poltrona, ma non di uscire";
        $opzioni[3]="In grado di uscire";
        return $opzioni;
    } 
 
    public function getNutrizioneProbNeuropsicologici()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Demenza o depressione grave";
        $opzioni[2]="Depressione moderata";
        $opzioni[3]="Nessun problema psicologico";
        return $opzioni;
    } 

    public function getNutrizioneBMI()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="BMI < 19";
        $opzioni[2]="19 ≤ BMI < 21";
        $opzioni[3]="21 ≤ BMI < 23";
        $opzioni[4]="BMI ≥ 23";
        return $opzioni;
    }                       
                    
    public function getNutrizioneAssNumeroPasti()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="1 pasto";
        $opzioni[2]="2 pasti";
        $opzioni[3]="3 pasti";
        return $opzioni;
    } 
                      
    public function getNutrizioneNumRispAfferm()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Nessuna o solo una risposta affermativa";
        $opzioni[2]="Due risposte affermative";
        $opzioni[3]="Tre risposte affermative";
        return $opzioni;
    }   

    public function getNutrizioneLiquidi()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Meno di 3 bicchieri";
        $opzioni[2]="Da 3 a 5 bicchieri";
        $opzioni[3]="Più di 5 bicchieri";
        return $opzioni;
    }  

    public function getNutrizioneModalitaAlim()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Incapace di cibarsi senza assistenza";
        $opzioni[2]="Si alimenta da solo/a, ma con qualche difficoltà";
        $opzioni[3]="Si alimenta da solo/a senza difficoltà";
        return $opzioni;
    }  

    public function getNutrizioneAutovalut()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Il paziente si considera malnutrito";
        $opzioni[2]="Il paziente non sa valutare il proprio stato nutrizionale";
        $opzioni[3]="Il paziente non ritiene di avere problemi nutrizionali";
        return $opzioni;
    } 
 
    public function getNutrizioneStatoSalute()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="Peggiore";
        $opzioni[2]="Non sa";
        $opzioni[3]="Analogo";
        $opzioni[4]="Migliore";
        return $opzioni;
    }   

    public function getNutrizioneCMB()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="CMB inferiore a 21";
        $opzioni[2]="CMB compresa tra 21 e 22";
        $opzioni[3]="CMB pari o superiore a 22";
        return $opzioni;
    }    

    public function getNutrizioneCP()
    {
        $opzioni=array();
        $opzioni[0]="";
        $opzioni[1]="CP inferiore a 31";
        $opzioni[2]="CP pari o superiore a 31";
        return $opzioni;
    }   


}
?>
<?php
ob_start(); 
session_name( 'PHPSESSID' );
session_start();
include_once "./query.php";
error_reporting(E_ERROR | E_PARSE);

    //se non loggato ritorno a login
    if (!isset($_SESSION['IdUtente'])){
        header("location: index.php");//redirect
    }else{
        $idUtente = $_SESSION['IdUtente'];
        $nominativo = $_SESSION['Nominativo'];
        $tipologiaUtente = $_SESSION['Tipologia'];
    }

    if (isset($_POST['radioGroup']))
    {
        if ($_POST['radioGroup']=="bpco") $bpco=true;
        else if ($_POST['radioGroup']=="irc") $irc=true;
        else if ($_POST['radioGroup']=="cardIpert") $cardIpert=true;
        else if ($_POST['radioGroup']=="cardIsch") $cardIsch=true;
        else if ($_POST['radioGroup']=="anemia") $anemia=true;
        else if ($_POST['radioGroup']=="diab") $diab=true;
        else if ($_POST['radioGroup']=="congPolm") $congPolm=true;
        else if ($_POST['radioGroup']=="congPer") $congPer=true;
        else if ($_POST['radioGroup']=="congPolmPer") $congPolmPer=true;
    }
    else
    {
        $cardIpert=true;
    }

    if (($bpco==false) && ($irc==false) && ($cardIpert==false) && ($cardIsch==false) && ($anemia==false) && ($diab==false) && ($congPolm==false) && ($congPer==false) && ($congPolmPer==false)) $cardIpert=true;


    //echo "*****";
    //print_r($cve);
    //echo "*****";
    //print_r($ExtraSopraVentrDataRisc);
    //echo "*****";

    
    include_once('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
	$db = Xcrud_db::get_instance();

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Altre Statistiche</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
</head>
 
<body style="background-color:#cccccc;">

    <div id="tooltip" style="position:absolute;display:none;border:2px solid #596715;padding:10px;background-color:#7c901e;opacity: 0.90;z-index: 9999;color:white;"></div> 

    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;display:none;">

        <div style="width:100%;">
            <img src="img/testataGisc.jpg" style="width:100%;">
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="logoff();">Esci</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goMain();">Pannello Principale</button>            
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goStatistiche();">Statistiche</button>
            <div style="clear:both;"></div>
        </div>


        <div style="width:100%;padding:5%;">
            <?php


                $query = $query_pagina_statistiche_pazienti_totali;
                $db->query($query);
                $resultTemp = $db->result();
                //echo '<pre>'; print_r($resultTemp); echo '</pre>';
                $resultNew['NumTotaleGenerale']=$resultTemp[0]["num"];

                $datiCompleti=true;



                //compongo la lista delle condizioni
                if ($congPolm)
                {
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_congPolm);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'Tot');
                    $datiCompleti=false;

                }
                else if ($congPer)
                {
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_congPerif);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'Tot');
                    $datiCompleti=false;

                }
                else if ($congPolmPer)
                {
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_congPolmPerif);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'Tot');
                    $datiCompleti=false;

                }
                else
                {
                    $condizioni="";
                    if ($cardIpert) $condizioni.= "and FR.IpertensioneArt=1 ";
                    if ($cardIsch) $condizioni.= "and FR.Eziologia_coronaropatia<>'' ";
                    if ($bpco) $condizioni.= "and FR.BPCO=1 ";
                    if ($anemia) $condizioni.= "and FR.ANEMIAHB='1' ";
                    if ($irc) $condizioni.= "and FR.InsufRenaleCronica=1 ";
                    if ($diab) $condizioni.= "and (FR.Diabete1Data!= '' or FR.Diabete2Data!='') ";

                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_generale);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'Tot');

                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_pm);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumPM');

                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_icd);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumICD');

                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_rtc);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumRTC');
      
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_rtcicd);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumRTCICD');
      
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_fapaross);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumFAPar');
      
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_faperm);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumFAPerm');
      
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_irc);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumIRC');
      
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_bpco);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumBPCO');
      
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_anemia);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumAnemia');
      
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_nyha);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumNYHA');
      
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_fe);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumFE');
      
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_intervFe);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumFEinterv');
      
                    $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_diabete);
                    $db->query($query);
                    $resultTemp = $db->result();
                    estraiDati($resultTemp,$resultNew,'NumDiab');

                    $valvolopatie=false;
                    if (($cardIpert) or ($cardIsch))
                    {
                        $valvolopatie=true;
                        $query = str_replace ( "@Condizioni" , $condizioni , $query_pagina_statistiche_altre_valvolopatie);
                        $db->query($query);
                        $resultTemp = $db->result();
                        estraiDati($resultTemp,$resultNew,'NumValv');

                    }


                }


  


                function estraiDati($resultQuery,&$resultNew,$indice)
                {
                    foreach ($resultQuery as $item)
                    {
                        if ($item["sesso"]=="M")
                        {
                            $resultNew[$indice.'M']=$item["num"];
                        }
                        else
                        {
                            $resultNew[$indice.'F']=$item["num"];
                        }
                    }
                    if (!isset($resultNew[$indice.'M'])) $resultNew[$indice.'M']=0;
                    if (!isset($resultNew[$indice.'F'])) $resultNew[$indice.'F']=0;
                    $resultNew[$indice]=$resultNew[$indice.'M']+$resultNew[$indice.'F'];
                }
                
                //echo '<pre>'; print_r($resultNew); echo '</pre>';
              


            ?>    

            <div class="col-md-12">

                <div class="panel-group" id="accordionCondizioni" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default" style="margin-bottom:10px;">
                    <div class="panel-heading" role="tab" id="headingCondizioni">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordionCondizioni" href="#collapseCondizioni" aria-expanded="true" aria-controls="collapseCondizioni">
                          Altre Statistiche
                        </a>
                      </h4>
                    </div>
                    <div id="collapseCondizioni" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingCondizioni">
                      <div class="panel-body">
                            <form name='dataStatistiche' action='statistiche_altre.php' method='POST' >
                                <div class="row">

                                    <div class="col-md-5" style="text-align: right;">CARDIOMIOPATIA IPERTENSIVA</div><div class="col-md-1"><input name="radioGroup" style="-webkit-box-shadow: unset;box-shadow: unset;height: 16px;margin-top: 2px;" class="xcrud-input form-control" type="radio" value="cardIpert" <? if ($cardIpert) echo 'checked="checked"' ?>></div>
                                    <div class="col-md-5" style="text-align: right;">CORONAROPATIA</div><div class="col-md-1"><input name="radioGroup" style="-webkit-box-shadow: unset;box-shadow: unset;height: 16px;margin-top: 2px;" class="xcrud-input form-control" type="radio" value="cardIsch" <? if ($cardIsch) echo 'checked="checked"' ?>></div>

                                    <div class="col-md-5" style="text-align: right;">SCOMPENSO e BPCO</div><div class="col-md-1"><input name="radioGroup" style="-webkit-box-shadow: unset;box-shadow: unset;height: 16px;margin-top: 2px;" class="xcrud-input form-control" type="radio" value="bpco" <? if ($bpco) echo 'checked="checked"' ?>></div>
                                    <div class="col-md-5" style="text-align: right;">SCOMPENSO e ANEMIA</div><div class="col-md-1"><input name="radioGroup" style="-webkit-box-shadow: unset;box-shadow: unset;height: 16px;margin-top: 2px;" class="xcrud-input form-control" type="radio" value="anemia" <? if ($anemia) echo 'checked="checked"' ?>></div>

                                    <div class="col-md-5" style="text-align: right;">SCOMPENSO e IRC</div><div class="col-md-1"><input name="radioGroup" style="-webkit-box-shadow: unset;box-shadow: unset;height: 16px;margin-top: 2px;" class="xcrud-input form-control" type="radio" value="irc"<? if ($irc) echo 'checked="checked"' ?>></div>
                                    <div class="col-md-5" style="text-align: right;">SCOMPENSO e DIABETE</div><div class="col-md-1"><input name="radioGroup" style="-webkit-box-shadow: unset;box-shadow: unset;height: 16px;margin-top: 2px;" class="xcrud-input form-control" type="radio" value="diab"<? if ($diab) echo 'checked="checked"' ?>></div>

                                    <div class="col-md-5" style="text-align: right;">CONGESTIONE POLMONARE</div><div class="col-md-1"><input name="radioGroup" style="-webkit-box-shadow: unset;box-shadow: unset;height: 16px;margin-top: 2px;" class="xcrud-input form-control" type="radio" value="congPolm"<? if ($congPolm) echo 'checked="checked"' ?>></div>
                                    <div class="col-md-5" style="text-align: right;">CONGESTIONE PERIFERICA</div><div class="col-md-1"><input name="radioGroup" style="-webkit-box-shadow: unset;box-shadow: unset;height: 16px;margin-top: 2px;" class="xcrud-input form-control" type="radio" value="congPer"<? if ($congPer) echo 'checked="checked"' ?>></div>

                                    <div class="col-md-5" style="text-align: right;">CONGESTIONE POLMONARE e PERIFERICA</div><div class="col-md-1"><input name="radioGroup" style="-webkit-box-shadow: unset;box-shadow: unset;height: 16px;margin-top: 2px;" class="xcrud-input form-control" type="radio" value="congPolmPer"<? if ($congPolmPer) echo 'checked="checked"' ?>></div>




                                </div> 
                            </form>
                          <button class="btn btn-primary xcrud-action" style="padding-left:30px;padding-right:30px;margin-top:15px;margin-right:5px;float: right;" onClick="cerca();">Cerca</button>   
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel-group" id="accordionRisultati" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingRisultati">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordionRisultati" href="#collapseRisultati" aria-expanded="true" aria-controls="collapseRisultati">
                                Risultati
                                </a>
                            </h4>
                        </div>
                        <div id="collapseRisultati" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingRisultati">
                            <div class="panel-body">
                                <div class="col-md-3"></div>
                                <div class="col-md-3" style="text-align:center;">Num.Pazienti</div>
                                <div class="col-md-3" style="text-align:center;">Maschi</div>
                                <div class="col-md-3" style="text-align:center;">Femmine</div>
                                <div style="clear:both;margin-top:10px;"></div>
                                <div class="col-md-3">Totali</div>
                                <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['Tot'] ?></b><br>(<? echo number_format((float)($resultNew['Tot']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)</div>
                                <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['TotM'] ?></b></div>
                                <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['TotF'] ?></b></div>
                                <? if ($datiCompleti==true)
                                    {
                                ?>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">PM</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumPM'] ?></b><br>(<? echo number_format((float)($resultNew['NumPM']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumPMM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumPMF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">ICD</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumICD'] ?></b><br>(<? echo number_format((float)($resultNew['NumICD']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumICDM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumICDF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">RTC</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumRTC'] ?></b><br>(<? echo number_format((float)($resultNew['NumRTC']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumRTCM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumRTCF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">RTC+ICD</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumRTCICD'] ?></b><br>(<? echo number_format((float)($resultNew['NumRTCICD']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumRTCICDM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumRTCICDF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">FA Paross.</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFAPar'] ?></b><br>(<? echo number_format((float)($resultNew['NumFAParD']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFAParM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFAParF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">FA Permanente</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFAPerm'] ?></b><br>(<? echo number_format((float)($resultNew['NumFAPerm']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFAPermM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFAPermF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">IRC</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumIRC'] ?></b><br>(<? echo number_format((float)($resultNew['NumIRC']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumIRCM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumIRCF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">BPCO</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumBPCO'] ?></b><br>(<? echo number_format((float)($resultNew['NumBPCO']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumBPCOM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumBPCOF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">ANEMIA</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumAnemia'] ?></b><br>(<? echo number_format((float)($resultNew['NumAnemia']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumAnemiaM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumAnemiaF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">Classe NYHA (da III a IV)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumNYHA'] ?></b><br>(<? echo number_format((float)($resultNew['NumNYHA']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumNYHAM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumNYHAF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">F.E. < 35%</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFE'] ?></b><br>(<? echo number_format((float)($resultNew['NumFE']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFEM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFEF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">35% <= F.E. < 50%</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFEinterv'] ?></b><br>(<? echo number_format((float)($resultNew['NumFEinterv']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFEintervM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumFEintervF'] ?></b></div>
                                    <div style="clear:both;margin-top:10px;"></div>
                                    <div class="col-md-3">Diabete</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumDiab'] ?></b><br>(<? echo number_format((float)($resultNew['NumDiab']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumDiabM'] ?></b></div>
                                    <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumDiabF'] ?></b></div>
                                    <? if ($valvolopatie==true)
                                        {
                                    ?>
                                        <div style="clear:both;margin-top:10px;"></div>
                                        <hr>
                                        <div class="col-md-3">Valvolopatie</div>
                                        <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumValv'] ?></b><br>(<? echo number_format((float)($resultNew['NumValv']/$resultNew['Tot']*100), 2, '.', '') ?>%)</div>
                                        <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumValvM'] ?></b></div>
                                        <div class="col-md-3" style="text-align:center;"><b><? echo $resultNew['NumValvF'] ?></b></div>
                                    <? 
                                        }
                                    ?>                                



                                <? 
                                    }
                                ?>                                
                            </div>
                        </div>
                    </div>
                </div>


            <?php
    
            ?>  
            <div style="clear:both;"></div>




        </div>
    </div>


    <script src="xcrud/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/flot/jquery.flot.min.js"></script>
    <script src="js/flot/jquery.flot.resize.min.js"></script>
    <script src="js/flot/jquery.flot.pie.min.js"></script>
    <script src="js/flot/jquery.flot.symbol.js"></script>
    <script src="js/flot/jquery.flot.stack.min.js"></script>
    <script src="js/flot/jquery.flot.axislabels.js"></script>
    <script src="js/flot/jquery.flot.time.js"></script>
    <script src="js/flot/jquery.flot.crosshair.min.js"></script>
    <script src="js/flot/jquery.flot.categories.min.js" type="text/javascript"></script>



    <script>

    function nuovoreclutamento()
    {
        window.location.href = 'schedaPaziente.php?id=-1';
    }

    function logoff()
    {
        window.location.href = 'logout.php';
    }

    function cerca()
    {
        //ricarico la pagina mandando in post i dati che servono
        document.dataStatistiche.submit();
    }
    
    function goStatistiche()
    {
        window.location.href = 'statistiche.php';
    }  







    $( window ).resize(function() {
      var larghezza=$(window).width();
      if (larghezza>=1024) $('#contenuto').css("width","1024px");
      else $('#contenuto').css("width","100%");
      var larghFinestra=$('#contenuto').width();
      //console.log(larghFinestra);
      //console.log(larghezza);
      $('#contenuto').css("margin-left",(larghezza-larghFinestra)/2+"px");

    });

    function goMain()
    {
        window.location.href = 'main.php';
    }    

    jQuery(document).ready(function() { 

        $(window).trigger('resize');
        $('#contenuto').show();
        //disegnaStatistiche();

    });



    //$('.collapse').collapse()
    </script>


</body>
</html>
<?php
ob_start(); 
session_start();

    include('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
	$db = Xcrud_db::get_instance();

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Some page title</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
</head>
 
<body style="background-color:#cccccc;">

    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;">
 
        <div style="width:100%;padding:5%;">
            <?php
                //prima parte, compilazione della tabella terapia_farmacologica in base alla tabella previstointerapia

                //ATTENZIONE... non si possono fare tutti insieme... sono circa 12000.. farli a blocchi da 3000
                //Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 3000 OFFSET 0
                //Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 3000 OFFSET 3000
                //Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 1000 OFFSET 6000
                //Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 1000 OFFSET 7000
                //Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 1000 OFFSET 8000
                //Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 1000 OFFSET 9000
                //Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 1000 OFFSET 10000
                //Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 500 OFFSET 11000
                //Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 500 OFFSET 11500
                //Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 1000 OFFSET 12000

/*
                //estraggo la terapia, per quadro clinico
                $sel = "Select IdQuadroClinico from quadroclinico order by IdQuadroClinico LIMIT 3000 OFFSET 12000"; 
                $db->query($sel);
                $result = $db->result();
                foreach ($result as $item) {
                    echo ($item["IdQuadroClinico"]."<br>");
                    //per ogni quadro clinico, prendo i farmaci previsti
                    $selTerapia = "Select farmaco.nomeFarmaco from previstointerapia,farmaco where farmaco.IdFarmaco=previstointerapia.IdFarmaco AND previstointerapia.valore='SI' AND previstointerapia.IdQuadroClinico=".$item["IdQuadroClinico"];
                    //$selTerapia = "Select farmaco.nomeFarmaco, previstointerapia.* from previstointerapia,farmaco where farmaco.IdFarmaco=previstointerapia.IdFarmaco AND previstointerapia.IdQuadroClinico=".$item["IdQuadroClinico"];
                    $db->query($selTerapia);
                    $resultTerapia = $db->result();
                    echo '<pre>'; print_r($resultTerapia); echo '</pre>';
                    //ho i farmaci
                    //li metto in un array per poterli inserire (o modificare) dopo
                    $arrayFarmaci= array();
                    foreach ($resultTerapia as $itemTerapia) {
                        $numeroPrima=count($arrayFarmaci);
                        if (stripos($itemTerapia["nomeFarmaco"], "Amiodarone") !== false) array_push($arrayFarmaci, "Amiodarone");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Digitale") !== false) array_push($arrayFarmaci, "Digitale");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Dronedarone") !== false) array_push($arrayFarmaci, "Dronedarone");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Flecainide") !== false) array_push($arrayFarmaci, "Flecainide");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Ivabradina") !== false) array_push($arrayFarmaci, "Ivabradina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Propafenone") !== false) array_push($arrayFarmaci, "Propafenone");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Ac. Acetilsalicilico") !== false) array_push($arrayFarmaci, "AcidoActeilsalicilico");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Clapidogrel") !== false) array_push($arrayFarmaci, "Clopidogrel");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Ticlopidina") !== false) array_push($arrayFarmaci, "Ticlopidina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Amlodipina") !== false) array_push($arrayFarmaci, "Amlodipina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Amlopidina") !== false) array_push($arrayFarmaci, "Amlodipina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Candesartan") !== false) array_push($arrayFarmaci, "Candesartan");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Irbesartan") !== false) array_push($arrayFarmaci, "Irbesartan");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Lisinopril") !== false) array_push($arrayFarmaci, "Lisinopril");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Losartan") !== false) array_push($arrayFarmaci, "Losartan");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Nitrato") !== false) array_push($arrayFarmaci, "Nitrato");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Nitroglicerina") !== false) array_push($arrayFarmaci, "Nitroglicerina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Torasemide") !== false) array_push($arrayFarmaci, "Torasemide");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Valsartan") !== false) array_push($arrayFarmaci, "Valsartan");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Atorvastatina") !== false) array_push($arrayFarmaci, "Atorvastatina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Ezetimibe") !== false) array_push($arrayFarmaci, "Ezetimibe");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Omega polienoici") !== false) array_push($arrayFarmaci, "Omegapolienoici");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Pravastatina") !== false) array_push($arrayFarmaci, "Pravastatina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Rosusvastatina") !== false) array_push($arrayFarmaci, "Rosuvastatina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Sinvastatina") !== false) array_push($arrayFarmaci, "Sinvastatina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Enalapril") !== false) array_push($arrayFarmaci, "Enalapril");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Ramipril") !== false) array_push($arrayFarmaci, "Ramipril");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Zofenopril") !== false) array_push($arrayFarmaci, "Zofenopril");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Furosemide - Spironolattone") !== false) array_push($arrayFarmaci, "FurosemideSpironolattone");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Furosemide") !== false) array_push($arrayFarmaci, "Furosemide");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Kanreonato") !== false) array_push($arrayFarmaci, "CanrenoatoPotassio");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Spironolattone") !== false) array_push($arrayFarmaci, "Spironolattone");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Bisoprololo") !== false) array_push($arrayFarmaci, "Bisoprololo");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Carvedilolo") !== false) array_push($arrayFarmaci, "Carvedilolo");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Metoprololo") !== false) array_push($arrayFarmaci, "Metoprololo");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Nebivololo") !== false) array_push($arrayFarmaci, "Nebivololo");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Anticoagulanti") !== false) array_push($arrayFarmaci, "Anticoagulanti");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Antidepressivi") !== false) array_push($arrayFarmaci, "Antidepressivi");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Nebivololo") !== false) array_push($arrayFarmaci, "Nebivololo");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Eritropoietina") !== false) array_push($arrayFarmaci, "Eritropietina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Insulina") !== false) array_push($arrayFarmaci, "Isulina");
                        else if (stripos($itemTerapia["nomeFarmaco"], "Ipoglicemizzanti") !== false) array_push($arrayFarmaci, "Ipoglicemizzanti");
                        

                        $numeroDopo=count($arrayFarmaci);
                        if ($numeroPrima==$numeroDopo)
                        {
                            if (stripos($itemTerapia["nomeFarmaco"], "Ranazolina") !== false)
                            {}
                            else if (stripos($itemTerapia["nomeFarmaco"], "Dabigatran") !== false)
                            {}
                            else echo ("non trovato farmaco:".$itemTerapia["nomeFarmaco"]);
                        }
                        $arrayFarmaci = array_unique($arrayFarmaci);



                    }
                    //echo '<pre>'; print_r($arrayFarmaci); echo '</pre>';
                    //tolgo eventuali duplicati
                    //$arrayFarmaci = array_unique($arrayFarmaci);
                     //echo '<pre>'; print_r($arrayFarmaci); echo '</pre>';
                   

                    //esiste questa riga in terapia_farmacologica?
                    $selCheck = "Select IdTerapiaFarmacologica from terapia_farmacologica where IdQuadroClinico=".$item["IdQuadroClinico"];
                    //$selTerapia = "Select farmaco.nomeFarmaco, previstointerapia.* from previstointerapia,farmaco where farmaco.IdFarmaco=previstointerapia.IdFarmaco AND previstointerapia.IdQuadroClinico=".$item["IdQuadroClinico"];
                    $db->query($selCheck);
                    $resultCheck = $db->result();
                    if (count($resultCheck) > 0) //esiste già quella riga
                    {
                        //echo("Esiste<br>");
                        //se la lista farmaci è vuota, non faccio niente
                        if (count($arrayFarmaci)>0)
                        {
                            $listaCampiValori="";
                            foreach ($arrayFarmaci as $farmaco) {
                                $listaCampiValori.=$farmaco."=1,";
                            }
                            $listaCampiValori = substr($listaCampiValori, 0, -1); //tolgo l'ultima virgola
                            //for ($i = 0; $i < count($arrayFarmaci); $i++)
                            //{
                            //    $listaCampiValori.=$arrayFarmaci[$i]."=1";
                            //    if ($i<count($arrayFarmaci)-1)  $listaCampiValori.=",";
                            //}
                            $selUpdate = "UPDATE terapia_farmacologica set ".$listaCampiValori." where IdQuadroClinico=".$item["IdQuadroClinico"];
                            echo ($selUpdate."<br>");
                            $db->query($selUpdate);
                        }
                        else echo ("Nessun farmaco da updatare<br>");                   
                    }
                    else //la devo creare
                    {
                        //echo("NON Esiste<br>");
                        if (count($arrayFarmaci)>0)
                        {
                            $listaCampi = join(',', $arrayFarmaci);
                            $listaCampi .= ",IdQuadroClinico";
                            $arrayValori=array();
                            for ($i = 0; $i < count($arrayFarmaci); $i++)
                            {
                                array_push($arrayValori, 1);
                            }
                            $listaValori = join(',', $arrayValori);
                            $listaValori .= ",".$item["IdQuadroClinico"];
                            $selInsert = "INSERT into terapia_farmacologica (".$listaCampi.") VALUES (".$listaValori.")";
                            echo ($selInsert."<br>");
                        }
                        else
                        {
                            $selInsert = "INSERT into terapia_farmacologica (IdQuadroClinico) VALUES (".$item["IdQuadroClinico"].")";
                            echo ($selInsert."<br>");
                        }
                        $db->query($selInsert);                    
                    }



                }

*/ 

                //seconda parte, posologia del furosemide
                
                // si devono fare una query alla volta, perchè ci mette troppo tempo e fallisce.... percio fare un blocco, modificare salvare e far fare il successivo....
                //se qualcuna falisce, ridurre il limite....
                //se si mette l'indice su iDquadroclinico sulla tabella terapia_farmacologica va una scheggia e non ci sta bisogno di dividerla....


               
                $selTerapia = "Select valore, IdQuadroClinico from previstointerapia where IdFarmaco=28 AND valore !=''";
                $db->query($selTerapia);
                $resultTerapia = $db->result();
                foreach ($resultTerapia as $itemTerapia) {
                    echo ("Metto il valore ".$itemTerapia["valore"]." per il quadro clinico ".$itemTerapia["IdQuadroClinico"]."<br>");
                    $selUpdate = "UPDATE terapia_farmacologica set FurosemidePosologia='".$itemTerapia['valore']."' where IdQuadroClinico=".$itemTerapia['IdQuadroClinico'];
                    echo ($selUpdate."<br>");
                    $db->query($selUpdate);
                }
 

                //ultimo punto, dove ci sta un valore di furosemide, metto il booleano a 1
                $selUpdate = "UPDATE terapia_farmacologica set Furosemide=1 where FurosemidePosologia IS NOT NULL";
                $db->query($selUpdate);
               
            ?>    
        
        </div>
    </div>

    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <script>


    jQuery(document).ready(function() { 


    });
    </script>


</body>
</html>
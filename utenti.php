<?php
ob_start(); 
session_name( 'PHPSESSID' );
session_start();

    //se non loggato ritorno a login
    if (!isset($_SESSION['IdUtente'])){
        header("location: index.php");//redirect
    }else{
        $idUtente = $_SESSION['IdUtente'];
        $nominativo = $_SESSION['Nominativo'];
        $tipologiaUtente = $_SESSION['Tipologia'];
    }

    include('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
    $xcrud_pass = Xcrud::get_instance();
	$db = Xcrud_db::get_instance();

    //vuole vedere la lista degli utenti o andare in edit?
    $userId=null;
    if (isset($_GET['id']))
    {
        //è arrivato qui con un id impostato.. lo considero
        $userId = $_GET['id'];
    }


?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Some page title</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
</head>
 
<body style="background-color:#cccccc;">

    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;display:none;">

    <div style="width:100%;">
        <img src="img/testataGisc.jpg" style="width:100%;">
        <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="logoff();">Esci</button>
        <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="nuovoreclutamento();">Nuovo Reclutamento</button>
        <?     
            if ($tipologiaUtente=="Admin")
            {
                echo '<button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goStatistiche();">Statistiche</button>';
                echo '<div class="dropdown" style="float:right;">
                        <button class="btn btn-primary xcrud-action dropdown-toggle" style="padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                            Utenti
                            <span class="caret"></span>
                        </button>
                        <ul class="color-btn-primary dropdown-menu " aria-labelledby="dropdownMenu1">
                            <li><a href="utenti.php">Lista Utenti</a></li>
                            <li><a href="utenti.php?id=-1">Nuovo Utente</a></li>
                        </ul>
                    </div>';
            }
        ?>            
        <div style="clear:both;"></div>
    </div>

    <div style="width:100%;padding:5%;">



<?php

    $sel = "SELECT IdTipoUtente, Nome from tipo_utente order by Nome asc";
    $db->query($sel);
    $result = $db->result();

    $elencoTipologia=array();
    foreach ($result as $tipologia) {
        $elencoTipologia[$tipologia["IdTipoUtente"]]=$tipologia["Nome"];
    }

    $xcrud->table('utente');
    $xcrud->change_type('IdTipoUtente','select','',$elencoTipologia);

    if ($userId==null) //mostra tutti
    {
        $xcrud->column_name('Nominativo','Nome');
        $xcrud->column_name('IdTipoUtente','Tipologia');
        $xcrud->column_name('E-Mail','Data Reclutamento');
        $xcrud->search_columns('Nominativo, Username, IdTipoUtente, email','');
        $xcrud->columns('Nominativo,Username,IdTipoUtente,email');
        $xcrud->button('utenti.php?id={idUtente}','Modifica','glyphicon glyphicon-th-list');
        $xcrud->table_name('Lista Utenti');
        
        $xcrud->unset_add( true ); 
        $xcrud->unset_edit( true );
        $xcrud->unset_view( true );
        $xcrud->unset_csv( true );
        $xcrud->unset_print( true );

        echo $xcrud->render();
    }
    else if ($userId==-1) //crea
    {
        $xcrud->label('Nominativo','Nome');
        $xcrud->label('IdTipoUtente','Tipologia');
        $xcrud->label('E-Mail','Data Reclutamento');
        $xcrud->fields('Nominativo,Username,IdTipoUtente,email,Password');
        $xcrud->hide_button( 'save_new, save_return, return' );
        $xcrud->after_insert('after_insert_utente');
        $xcrud->change_type('Password', 'password', 'md5',30);
        echo $xcrud->render('create');
    }
    else //edit
    {
        $xcrud->label('Nominativo','Nome');
        $xcrud->label('IdTipoUtente','Tipologia');
        $xcrud->label('E-Mail','Data Reclutamento');
        $xcrud->fields('Nominativo,Username,IdTipoUtente,email');
        $xcrud->hide_button( 'save_new, save_return, return' );
        $xcrud->after_update('after_update_utente');
        echo $xcrud->render('edit', $userId);

        //in edit metto anche la possibilità di modificare la password
        $xcrud_pass->table('utente');
        //$xcrud_pass->fields('Password_check1');
        $xcrud_pass->fields('Password');
        $xcrud_pass->fields('Password_check');
        $xcrud_pass->label('Password','Nuova password');
        $xcrud_pass->label('Password_check','Conferma Nuova password');
        $xcrud_pass->hide_button('save_new, save_return, return');
        $xcrud_pass->before_update('before_update_utente_pass');
        $xcrud_pass->after_update('after_update_utente_pass');
        $xcrud_pass->table_name('Password');
        $xcrud_pass->change_type('Password', 'password', 'md5',30);
        $xcrud_pass->change_type('Password_check', 'password', 'md5',30);
        echo $xcrud_pass->render('edit', $userId);

    }




    //echo $xcrud_utente->result_list;
    

    //echo $xcrud_utente->render();
    //echo $xcrud_utente;
    
	//$sel = "SELECT * FROM utente";
    //$db->query($sel);
    //$result = $db->result();
    
    
    
    

	//$query='SELECT * FROM utente WHERE Username = "'.$user.'" AND Password = "'.$password.'"';
	//$query="SELECT * FROM utente";
	//echo $db->query($query); // executes query, returns count of affected rows
	//echo $result;
	//$result = $db->result(); // loads results as list of arrays
	//echo count($result);
	//$db->row(); // loads one result row as associative array


?>
    
    </div>
    </div>

    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <script>

    function nuovoreclutamento()
    {
        window.location.href = 'schedaPaziente.php?id=-1';
    }

    function logoff()
    {
        window.location.href = 'logout.php';
    }
    
    function goStatistiche()
    {
        window.location.href = 'statistiche.php';
    }

    $( window ).resize(function() {
      var larghezza=$(window).width();
      if (larghezza>=1024) $('#contenuto').css("width","1024px");
      else $('#contenuto').css("width","100%");
      var larghFinestra=$('#contenuto').width();
      //console.log(larghFinestra);
      //console.log(larghezza);
      $('#contenuto').css("margin-left",(larghezza-larghFinestra)/2+"px");

    });

    jQuery(document).ready(function() { 

        $(window).trigger('resize');
        $('#contenuto').show();

    });
    

    </script>


</body>
</html>
<?php
ob_start(); 
session_start();

    include('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
    $db = Xcrud_db::get_instance();

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Some page title</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
</head>
 
<body style="background-color:#cccccc;">

    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;">
 
        <div style="width:100%;padding:5%;">
            <?php
                

                //mappatura insufficenza renale cronica
                //il valore è inserito nella tabella quadroclinico, nella riga che identifica il reclutamento, ed è a 3 livelli (non solo 0/1)
                //mappo tutti a 0, poi se è 1 o 2 sul vecchio campo, metto 1 sul nuovo
                //bisognerebbe prendere, per ogni paziente, il valore del campo nel quadro clinico e copiarlo in fattori di rischio, nella riga del paziente
                echo ("MAPPATURA INSUFFICIENZA RENALE CRONICA<br>");
                $selInsert = "UPDATE fattorerischio_eziologia set InsufRenaleCronica=0";
                echo ($selInsert."<br>");
                $db->query($selInsert);

                //estraggo tutti i quadri clinici
                $sel = "SELECT IdPaziente, InsuffRenale FROM quadroclinico WHERE gTipo='RECLUTAMENTO'";
                $db->query($sel);
                $result = $db->result();

                foreach ($result as $item)
                {
                    echo ($item["IdPaziente"]." - ".$item["InsuffRenale"]."<br>");
                    if (($item["InsuffRenale"]==1) || ($item["InsuffRenale"]==2))
                    {
                        $selInsert = "UPDATE fattorerischio_eziologia set InsufRenaleCronica=1 WHERE IdPaziente=".$item["IdPaziente"];
                        echo ($selInsert."<br>");
                        $db->query($selInsert);
                    }
                }

                //EZIOLOGIA
                echo ("MAPPATURA EZIOLOGIA - CARDIOMIOPATIA/CORONAROPATIA<br>");
                $sel = "SELECT IdPaziente FROM paziente";
                $db->query($sel);
                $result = $db->result();

                foreach ($result as $item)
                {
                    echo ($item["IdPaziente"]."<br>");
                    
                    //cardiomiopatia
                    $val="";
                    //vedo le varie tipologie di cardiomiopatia
                    $sel = "SELECT DataPrimoRilievo FROM cardiomiopatiadilatativa WHERE IdPaziente=".$item["IdPaziente"];
                    $db->query($sel);
                    $result1 = $db->result();
                    if (count($result1)>0)
                    {
                        if ($result1[0]["DataPrimoRilievo"]!="") $val.="1,";
                    }
                    $sel = "SELECT DataPrimoRilievo FROM cardimioipertrofica WHERE IdPaziente=".$item["IdPaziente"];
                    $db->query($sel);
                    $result1 = $db->result();
                    if (count($result1)>0)
                    {
                        if ($result1[0]["DataPrimoRilievo"]!="") $val.="2,";
                    }
                    if ($val!="") $val=substr($val, 0, -1);
                    $selInsert = "UPDATE fattorerischio_eziologia set Eziologia_cardiomiopatia='".$val."' WHERE IdPaziente=".$item["IdPaziente"];
                    echo ($selInsert."<br>");
                    $db->query($selInsert);

                    //coronaropatia
                    $val="";
                    //vedo le varie tipologie di coronaropatia
                    $sel = "SELECT Data FROM ima WHERE IdPaziente=".$item["IdPaziente"];
                    $db->query($sel);
                    $result1 = $db->result();
                    if (count($result1)>0)
                    {
                        if ($result1[0]["Data"]!="") $val.="1,";
                    }
                    $sel = "SELECT DataPrimoRilievo FROM cardiopischemnoima WHERE IdPaziente=".$item["IdPaziente"];
                    $db->query($sel);
                    $result1 = $db->result();
                    if (count($result1)>0)
                    {
                        if ($result1[0]["DataPrimoRilievo"]!="") $val.="2,";
                    }
                    if ($val!="") $val=substr($val, 0, -1);
                    $selInsert = "UPDATE fattorerischio_eziologia set Eziologia_coronaropatia='".$val."' WHERE IdPaziente=".$item["IdPaziente"];
                    echo ($selInsert."<br>");
                    $db->query($selInsert);

                    //valvolopatie
                    $val="";
                    //vedo le varie tipologie di valvolopatie
                    $sel = "SELECT TipoValvolopatia FROM valvolopatia WHERE IdPaziente=".$item["IdPaziente"];
                    $db->query($sel);
                    $result1 = $db->result();
                    $tipo=0;
                    if (count($result1)>0)
                    {
                        $tipo=intval($result1[0]["TipoValvolopatia"]);
                        $tipo--;
                    }
                    $selInsert = "UPDATE fattorerischio_eziologia set Eziologia_difettiValvolariCardiaci='".$tipo."' WHERE IdPaziente=".$item["IdPaziente"];
                    echo ($selInsert."<br>");
                    $db->query($selInsert);


                    

                }


                echo ("MAPPATURA DISTURBI A CARICO DELL?APPARATO RESPIRATORIO<br>");

                //estraggo tutti i quadri clinici, ma lo faccio per paziente, che altrimenti sono troppi da gestire tutti insieme
                $sel = "SELECT IdPaziente FROM paziente order by IdPaziente";
                $db->query($sel);
                $result = $db->result();

                foreach ($result as $item)
                {
                    echo ($item["IdPaziente"]."<br>");
                    $sel = "SELECT * FROM quadroclinico WHERE IdPaziente=".$item["IdPaziente"];
                    $db->query($sel);
                    $resultQuadro = $db->result();

                    
                    foreach ($resultQuadro as $itemQuadro)
                    {
                        echo ($itemQuadro["IdQuadroClinico"]."<br>");
                        //Dispnea parossistica notturna
                        
                        $val="";
                        if ((intval($itemQuadro["NEpisodiDispnea"])>0) || ($itemQuadro["Data_Anamnesi_Dispnea_Nott"] != NULL)) $val.="1,";
                        //ortopnea
                        if (intval($itemQuadro["NCuscini"])>0) $val.="2,";
                        //apnea notturna
                        if (intval($itemQuadro["SleepApnea"])>0) $val.="3,";
                        //edema polmonare acuto
                        if (intval($itemQuadro["EdPolAcuto"])>0) $val.="4,";
                        if ($val!="") $val=substr($val, 0, -1);
                        $selInsert = "UPDATE quadroclinico set cardiologoDisturbiRespiratorio='".$val."' WHERE IdQuadroClinico=".$itemQuadro["IdQuadroClinico"];
                        echo ($selInsert."<br>");
                        $db->query($selInsert);
                        
                    }
                    
                }


                echo ("MAPPATURA TERAPIA DIABETE<br>");
                //nel vecchio sw era un campo descrittivo, ora sono 3 booleani
                $sel = "SELECT TerapieDiabete, IdFattoreRischio FROM fattorerischio_eziologia";
                $db->query($sel);
                $resultTerapia = $db->result();

                
                foreach ($resultTerapia as $itemTerapia)
                {

                    $insulina=false;
                    //insulina
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("insulin") ) !== false)
                    {
                        $insulina=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("1NSULINA") ) !== false)
                    {
                        $insulina=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("lantus") ) !== false)
                    {
                        $insulina=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("humalog") ) !== false)
                    {
                        $insulina=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("Ha mute") ) !== false)
                    {
                        $insulina=true;
                    }

                    //ipoglicemizzanti
                    $ipoglicemizzanti=false;
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("metformina") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("solosa") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("metforal") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("glimepiride") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("ipoglicemizzant") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("1POGLICEMMIZZANTI") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("Ipoglicammizzanti") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("IPOGLICEMOZZANTI") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("IPOGLICEMIZZAMNTI") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("IPGLICEMIZZANTI") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("Ipoglicemia") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("novonorm") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("glibomet") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("repaniglizide") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("glucophage") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("repiglizide") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("repiglizide") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("vitagliptin") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("metfonorm") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }
                    if (strpos(strtoupper($itemTerapia["TerapieDiabete"]), strtoupper("SUGUAN") ) !== false)
                    {
                        $ipoglicemizzanti=true;
                    }

                    $valore="";
                    if (($ipoglicemizzanti==false) && ($insulina==false)) $valore="Nessuna";
                    if (($ipoglicemizzanti==true) && ($insulina==false)) $valore="Ipoglicemizzanti";
                    if (($ipoglicemizzanti==false) && ($insulina==true)) $valore="Insulina";
                    if (($ipoglicemizzanti==true) && ($insulina==true)) $valore="Insulina, Ipoglicemizzanti";

                    $selInsert = "UPDATE fattorerischio_eziologia set DiabeteTerapia='".$valore."' WHERE IdFattoreRischio=".$itemTerapia["IdFattoreRischio"];
                    //if ($valore=="Nessuna")
                    //{
                        echo ($itemTerapia["TerapieDiabete"]."<br>");
                        echo ($selInsert."<br>");
                    //}
                    $db->query($selInsert);

                   
                }


            ?>    
        
        </div>
    </div>

    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <script>


    jQuery(document).ready(function() { 


    });
    </script>


</body>
</html>
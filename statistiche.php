<?php
ob_start(); 
session_name( 'PHPSESSID' );
session_start();
include_once "./query.php";

    //se non loggato ritorno a login
    if (!isset($_SESSION['IdUtente'])){
        header("location: index.php");//redirect
    }else{
        $idUtente = $_SESSION['IdUtente'];
        $nominativo = $_SESSION['Nominativo'];
        $tipologiaUtente = $_SESSION['Tipologia'];
    }

    include('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
	$db = Xcrud_db::get_instance();

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Statistiche</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
</head>
 
<body style="background-color:#cccccc;">

    <div id="tooltip" style="position:absolute;display:none;border:2px solid #ddd;padding:10px;background-color:#f5f5f5;opacity: 0.90;z-index: 9999;color:#000000;"></div> 

    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;display:none;">

        <div style="width:100%;">
            <img src="img/testataGisc.jpg" style="width:100%;">
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="logoff();">Esci</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goMain();">Pannello Principale</button>            
            <div style="clear:both;"></div>
        </div>


        <div style="width:100%;padding:5%;">
            <?php

                //$now = DateTime::createFromFormat('U.u', microtime(true));
                //echo "start:".$now->format("s.u")."<br>";

                $db->query($query_statistiche_pazienti); //500milli
                $result = $db->result();
                //echo '<pre>'; print_r($result); echo '</pre>';
                //$now = DateTime::createFromFormat('U.u', microtime(true));
                //echo "1:".$now->format("s.u")."<br>";
                
                $vivi=intval($result[0]['NumTotale'])-intval($result[0]['dead']);

                //le età sono da controllare anche con i pazienti che hanno avvalorato il campo data_nascita
                $sel = "select DATE_FORMAT(data_nascita, '%d-%m-%Y') as DataNascita, Sesso from paziente where data_nascita IS NOT NULL";       
                $db->query($sel);
                $resultAge = $db->result();
                
                //$now = DateTime::createFromFormat('U.u', microtime(true));
                //echo "2:".$now->format("s.u")."<br>";

                //somme delle età (con data di nascita)
                $SumEtaNew=0;
                $SumEtaMNew=0;
                $SumEtaFNew=0;
                $EtaMax=0;
                $EtaMaxM=0;
                $EtaMaxF=0;
                foreach ($resultAge as $item) {
                    
                    $anni = calcolaAnni($item["DataNascita"]);
                    $SumEtaNew=$SumEtaNew+$anni;
                    if ($item["Sesso"]=="M") $SumEtaMNew=$SumEtaMNew+$anni;
                    else $SumEtaFNew=$SumEtaFNew+$anni;
                    if ($anni>$EtaMax) $EtaMax=$anni;
                    
                    if (($item["Sesso"]=="M") and ($anni>$EtaMaxM)) $EtaMaxM=$anni;
                    if (($item["Sesso"]=="F") and ($anni>$EtaMaxF)) $EtaMaxF=$anni;

                }
                $MediaEta = intval(($SumEtaNew+$result[0]['SumEta'])/$result[0]['NumTotale']);
                $MediaEtaM = intval(($SumEtaMNew+$result[0]['SumEtaM'])/$result[0]['NumMaschi']);
                $MediaEtaF = intval(($SumEtaFNew+$result[0]['SumEtaF'])/$result[0]['NumFemmine']);
                if ($EtaMax<intval($result[0]['EtaMax'])) $EtaMax=intval($result[0]['EtaMax']);
                if ($EtaMaxM<intval($result[0]['EtaMaxM'])) $EtaMaxM=intval($result[0]['EtaMaxM']);
                if ($EtaMaxF<intval($result[0]['EtaMaxF'])) $EtaMaxF=intval($result[0]['EtaMaxF']);

                //$now = DateTime::createFromFormat('U.u', microtime(true));
                //echo "3:".$now->format("s.u")."<br>";

                $db->query($query_statistiche_Ricoveri); //300milli
                $resultRicoveri = $db->result();
                //echo $query_statistiche_Ricoveri;
                //$now = DateTime::createFromFormat('U.u', microtime(true));
                //echo "4:".$now->format("s.u")."<br>";

                function calcolaAnni($dataNascita)
                {
                    $birthDate = explode("-", $dataNascita);
                    $anni = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[0], $birthDate[2]))) > date("md")
                    ? ((date("Y") - $birthDate[2]) - 1)
                    : (date("Y") - $birthDate[2]));
                    return intval($anni);
             
                }


                //statistiche generali

                $query = $query_pagina_statistiche_pazienti_totali;
                $db->query($query);
                $resultTemp = $db->result();
                $resultStatNew['NumTotaleGenerale']=$resultTemp[0]["num"];


                //$condizioni.= "and P.gDead != '' ";                
                $condizioni.= "";                

                //Fattori di rischio e comorbidità
                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_FattoriRischio_ipertensione);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatNew,'NumCasiIpert');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_FattoriRischio_insuffRenale);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatNew,'NumCasiInsuffRenale');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_FattoriRischio_diabete);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatNew,'NumCasiDiabete');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_FattoriRischio_dislipidemia);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatNew,'NumCasiIperdislipidemia');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_FattoriRischio_anemia);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatNew,'NumCasiAnemia');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_FattoriRischio_bpco);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatNew,'NumCasiBPCO');



                //Eziologia
                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Eziologia_ischemia);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatEziologiaNew,'NumCasiIschemia');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Eziologia_dilatativa);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatEziologiaNew,'NumCasiDilatativa');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Eziologia_valvolopatie);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatEziologiaNew,'NumCasiValvolopatie');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Eziologia_ipertensiva);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatEziologiaNew,'NumCasiIpertensiva');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Eziologia_iperValv);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatEziologiaNew,'NumCasiIpertensivaValvolare');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Eziologia_iperValv);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatEziologiaNew,'NumCasiIschemiaValvolare');



                //echo '<pre>'; print_r($resultStatEziologia); echo '</pre>';
                //$now = DateTime::createFromFormat('U.u', microtime(true));
                //echo "6:".$now->format("s.u")."<br>";

                //clinica
                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Clinica_nyha);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatClinicaNew,'NumCasiNyha');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Clinica_fe);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatClinicaNew,'NumCasiFE');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Clinica_intervFe);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatClinicaNew,'NumCasiIntervalloFE');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Clinica_iperPolm);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatClinicaNew,'NumCasiIpertPolmonare');

                $query = str_replace ( "@Condizioni" , $condizioni , $query_statistiche_Clinica_disfDiast);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultStatClinicaNew,'NumCasiDisfDiastolica');

                //Fumo
                $db->query($query_statistiche_Fumo); //
                $resultStatFumo = $db->result();
                //echo '<pre>'; print_r($resultStatFumo); echo '</pre>';
                //$now = DateTime::createFromFormat('U.u', microtime(true));
                //echo "6:".$now->format("s.u")."<br>";

                //ipertensione
                $db->query($query_statistiche_Ipertensione); //
                $resultStatIpertensione = $db->result();
                //echo '<pre>'; print_r($resultStatIpertensione); echo '</pre>';

                //diabete
                $db->query($query_statistiche_Diabete); //
                $resultStatDiabete = $db->result();
                //echo '<pre>'; print_r($resultStatDiabete); echo '</pre>';

                //ischemia
                $db->query($query_statistiche_ischemica); //
                $resultStatIschemia = $db->result();
                //echo '<pre>'; print_r($resultStatIschemia); echo '</pre>';

                //ima
                $db->query($query_statistiche_ima); //
                $resultStatIma = $db->result();
                //echo '<pre>'; print_r($resultStatIma); echo '</pre>';

                //bpco
                $db->query($query_statistiche_bpco); //
                $resultStatBPCO = $db->result();
                //echo '<pre>'; print_r($resultStatBPCO); echo '</pre>';

                //Insufficenza Renale Cronica
                $db->query($query_statistiche_InsufRenCron); //
                $resultStatInsufRenCron = $db->result();
                //echo '<pre>'; print_r($resultStatInsufRenCron); echo '</pre>';

                //Anemia
                $db->query($query_statistiche_Anemia); //
                $resultStatAnemia = $db->result();
                //echo '<pre>'; print_r($resultStatAnemia); echo '</pre>';

                //Colesterolo
                $db->query($query_statistiche_Colesterolo); //
                $resultStatColesterolo = $db->result();
                //echo '<pre>'; print_r($resultStatColesterolo); echo '</pre>';

                //rigurgito mitralico severo
                $db->query($query_statistiche_RigurgitoMitralicoSevero); //
                $resultStatRigurgito = $db->result();
                //echo '<pre>'; print_r($resultStatRigurgito); echo '</pre>';
                
                //Stenosi Aortica
                $db->query($query_statistiche_StenosiAortica); //
                $resultStatStenosi = $db->result();
                //echo '<pre>'; print_r($resultStatStenosi); echo '</pre>';

                function estraiDati($resultQuery,&$resultNew,$indice)
                {
                    foreach ($resultQuery as $item)
                    {
                        if ($item["sesso"]=="M")
                        {
                            $resultNew[$indice.'M']=$item["num"];
                        }
                        else
                        {
                            $resultNew[$indice.'F']=$item["num"];
                        }
                    }
                    $resultNew[$indice]=$resultNew[$indice.'M']+$resultNew[$indice.'F'];
                }


            ?>    

            <div class="col-md-7">

                <h4><a href="statistiche_aritmie.php">Statistiche Aritmie</a></h4>
                <h4><a href="statistiche_bbs.php">Statistiche BBS</a></h4>
                <h4><a href="statistiche_bbd.php">Statistiche BBD</a></h4>
                <h4><a href="statistiche_ischemia.php">Statistiche Ischemia</a></h4>
                <h4><a href="statistiche_bav.php">Statistiche BAV</a></h4>
                <h4><a href="statistiche_ipertrofia.php">Statistiche Ipertrofia</a></h4>
                <h4><a href="statistiche_diametrosist.php">Statistiche Diametro Sistolico</a></h4>
                <h4><a href="statistiche_diametrodiast.php">Statistiche Diametro Diastolico</a></h4>
                <h4><a href="statistiche_fe.php">Statistiche FE</a></h4>
                <h4><a href="statistiche_fc.php">Statistiche FC</a></h4>
                <h4><a href="statistiche_presspolm.php">Statistiche Pressione Polmonare</a></h4>
                <h4><a href="statistiche_disfdiast.php">Statistiche Disfunzione Diastolica</a></h4>
                <h4><a href="statistiche_dopplerart.php">Statistiche Doppler Arterioso Arti Inferiori con Stenosi Significative</a></h4>
                <h4><a href="statistiche_nyha.php">Statistiche NYHA</a></h4>
                <h4><a href="statistiche_deads.php">Statistiche Deads</a></h4>
                <h4><a href="statistiche_altre.php">Altre Statistiche</a></h4>
                <h4><a href="statistiche_trend.php">Trend Totali</a></h4>





                <div class="panel-group" style="margin-top:10px;" id="accordionPiePazienti" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordionPiePazienti" href="#collapsePiePazienti" aria-expanded="true" aria-controls="collapseOne">
                          Dashboard Pazienti
                        </a>
                      </h4>
                    </div>
                    <div id="collapsePiePazienti" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                            <div id="pie_chart_pazienti" class="chart" style="height:300px;width: 100%;"></div>       
                      </div>
                    </div>
                  </div>
                </div>

            </div>
            
            <div class="col-md-5">

                <div class="panel-group" id="accordionPazienti" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingPazienti">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordionPazienti" href="#collapsePazienti" aria-expanded="true" aria-controls="collapsePazienti">
                          Pazienti
                        </a>
                      </h4>
                    </div>
                    <div id="collapsePazienti" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingPazienti">
                      <div class="panel-body">
                        Pazienti: <b><? echo $result[0]['NumTotale'] ?></b><br>
                        Maschi: <b><? echo $result[0]['NumMaschi'] ?></b> (<? echo number_format((float)($result[0]['NumMaschi']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $result[0]['NumFemmine'] ?></b> (<? echo number_format((float)($result[0]['NumFemmine']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Età media pazienti: <b><? echo $MediaEta ?></b><br>
                        Età media M: <b><? echo $MediaEtaM ?></b><br>
                        Età media F: <b><? echo $MediaEtaF ?></b><br>
                        Età massima: <b><? echo $EtaMax ?></b><br>
                        Età massima M: <b><? echo $EtaMaxM ?></b><br>
                        Età massima F: <b><? echo $EtaMaxF ?></b><br>
                        <br>
                        Decessi: <b><? echo $result[0]['dead'] ?></b><br>
                        Decessi M: <b><? echo $result[0]['deadM'] ?></b><br>
                        Decessi F: <b><? echo $result[0]['deadF'] ?></b><br>          
                      </div>
                    </div>
                  </div>
                  
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOsservazioni">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionOsservazioni" href="#collapseOsservazioni" aria-expanded="false" aria-controls="collapseOsservazioni">
                          Osservazioni
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOsservazioni" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOsservazioni">
                      <div class="panel-body">
                        Osservazioni: <b><? echo $result[0]['NumTotaleOss'] ?></b><br>
                        N. medio oss/paziente: <b><? echo number_format((float)($result[0]['NumTotaleOss']/$result[0]['NumTotale']), 2, '.', '') ?></b><br>
                        Maschi: <b><? echo $result[0]['NumOssM'] ?></b> (<? echo number_format((float)($result[0]['NumOssM']/$result[0]['NumTotaleOss']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $result[0]['NumOssF'] ?></b> (<? echo number_format((float)($result[0]['NumOssF']/$result[0]['NumTotaleOss']*100), 2, '.', '') ?>%)<br>
                        Controlli MMG: <b><? echo $resultRicoveri[0]['NumControlliMMG'] ?></b><br>
                        Controlli Specialist: <b><? echo $resultRicoveri[0]['NumControlliSpec'] ?></b><br>
                        Ricoveri Cardiologici: <b><? echo $resultRicoveri[0]['NumRicoveri'] ?></b><br>
                        Ricoveri Cardiologici Programmati: <b><? echo $resultRicoveri[0]['NumRicoveriProgrammati'] ?></b><br>
                        Ricoveri NON Cardiologici: <b><? echo $resultRicoveri[0]['NumRicoveriNonCardio'] ?></b><br>
                        <br>
                        Prec.Acc.P.S. per Scomp.: <b><? echo $result[0]['dead'] ?></b><br>
                        Pazienti con Prec.Acc. P.S. per Scomp.: <b><? echo $result[0]['deadM'] ?></b><br>
                        <br>
                        Prec.Osp. per Scomp.: <b><? echo $result[0]['deadF'] ?></b><br>  
                        Pazienti con Prec.Osp. per Scomp.: <b><? echo $result[0]['deadF'] ?></b><br>  
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingFumo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionFumo" href="#collapseFumo" aria-expanded="false" aria-controls="collapseFumo">
                          Fumo
                        </a>
                      </h4>
                    </div>
                    <div id="collapseFumo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFumo">
                      <div class="panel-body">
                        Fumatori: <b><? echo $resultStatFumo[0]['numFumatori'] ?></b> (<? echo number_format((float)($resultStatFumo[0]['numFumatori']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatFumo[0]['numFumatoriM'] ?></b> (<? echo number_format((float)($resultStatFumo[0]['numFumatoriM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatFumo[0]['numFumatoriF'] ?></b> (<? echo number_format((float)($resultStatFumo[0]['numFumatoriF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>
                        <br>
                        Ex Fumatori: <b><? echo $resultStatFumo[0]['exFumatori'] ?></b> (<? echo number_format((float)($resultStatFumo[0]['exFumatori']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatFumo[0]['exFumatoriM'] ?></b> (<? echo number_format((float)($resultStatFumo[0]['exFumatoriM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatFumo[0]['exFumatoriF'] ?></b> (<? echo number_format((float)($resultStatFumo[0]['exFumatoriF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>
                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingIpertensione">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionIpertensione" href="#collapseIpertensione" aria-expanded="false" aria-controls="collapseIpertensione">
                          Ipertensione
                        </a>
                      </h4>
                    </div>
                    <div id="collapseIpertensione" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingIpertensione">
                      <div class="panel-body">
                        Pazienti affetti: <b><? echo $resultStatIpertensione[0]['numIpertesi'] ?></b> (<? echo number_format((float)($resultStatIpertensione[0]['numIpertesi']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatIpertensione[0]['numIpertesiM'] ?></b> (<? echo number_format((float)($resultStatIpertensione[0]['numIpertesiM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatIpertensione[0]['numIpertesiF'] ?></b> (<? echo number_format((float)($resultStatIpertensione[0]['numIpertesiF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>
                        <br>
                        Età media pazienti: <b><? echo intval($resultStatIpertensione[0]['SumEta']/$resultStatIpertensione[0]['numIpertesi']) ?></b><br>
                        Età media M: <b><? echo intval($resultStatIpertensione[0]['SumEtaM']/$resultStatIpertensione[0]['numIpertesiM']) ?></b><br>
                        Età media F: <b><? echo intval($resultStatIpertensione[0]['SumEtaF']/$resultStatIpertensione[0]['numIpertesiF']) ?></b><br>
                        <br>
                        Età Massima pazienti: <b><? echo $resultStatIpertensione[0]['MaxEta'] ?></b><br>
                        Età Massima M: <b><? echo $resultStatIpertensione[0]['MaxEtaM'] ?></b><br>
                        Età Massima F: <b><? echo $resultStatIpertensione[0]['MaxEtaF'] ?></b><br>
                        <br>
                        Età Minima pazienti: <b><? echo $resultStatIpertensione[0]['MinEta'] ?></b><br>
                        Età Minima M: <b><? echo $resultStatIpertensione[0]['MinEtaM'] ?></b><br>
                        Età Minima F: <b><? echo $resultStatIpertensione[0]['MinEtaF'] ?></b><br>

                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingDiabete">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionDiabete" href="#collapseDiabete" aria-expanded="false" aria-controls="collapseDiabete">
                          Diabete
                        </a>
                      </h4>
                    </div>
                    <div id="collapseDiabete" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingDiabete">
                      <div class="panel-body">
                        Pazienti affetti: <b><? echo $resultStatDiabete[0]['numDiabetici'] ?></b> (<? echo number_format((float)($resultStatDiabete[0]['numDiabetici']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatDiabete[0]['numDiabeticiM'] ?></b> (<? echo number_format((float)($resultStatDiabete[0]['numDiabeticiM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatDiabete[0]['numDiabeticiF'] ?></b> (<? echo number_format((float)($resultStatDiabete[0]['numDiabeticiF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>
                        <br>
                        Età media pazienti: <b><? echo intval($resultStatDiabete[0]['SumEta']/$resultStatDiabete[0]['numDiabetici']) ?></b><br>
                        Età media M: <b><? echo intval($resultStatDiabete[0]['SumEtaM']/$resultStatDiabete[0]['numDiabeticiM']) ?></b><br>
                        Età media F: <b><? echo intval($resultStatDiabete[0]['SumEtaF']/$resultStatDiabete[0]['numDiabeticiF']) ?></b><br>
                        <br>
                        Età Massima pazienti: <b><? echo $resultStatDiabete[0]['MaxEta'] ?></b><br>
                        Età Massima M: <b><? echo $resultStatDiabete[0]['MaxEtaM'] ?></b><br>
                        Età Massima F: <b><? echo $resultStatDiabete[0]['MaxEtaF'] ?></b><br>
                        <br>
                        Età Minima pazienti: <b><? echo $resultStatDiabete[0]['MinEta'] ?></b><br>
                        Età Minima M: <b><? echo $resultStatDiabete[0]['MinEtaM'] ?></b><br>
                        Età Minima F: <b><? echo $resultStatDiabete[0]['MinEtaF'] ?></b><br>

                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingIma">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionIma" href="#collapseIma" aria-expanded="false" aria-controls="collapseIma">
                          IMA
                        </a>
                      </h4>
                    </div>
                    <div id="collapseIma" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingIma">
                      <div class="panel-body">
                        Pazienti affetti: <b><? echo $resultStatIma[0]['numCasi'] ?></b> (<? echo number_format((float)($resultStatIma[0]['numCasi']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatIma[0]['numCasiM'] ?></b> (<? echo number_format((float)($resultStatIma[0]['numCasiM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatIma[0]['numCasiF'] ?></b> (<? echo number_format((float)($resultStatIma[0]['numCasiF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>
                        <br>
                        Età media pazienti: <b><? echo intval($resultStatIma[0]['SumEta']/$resultStatIma[0]['numCasi']) ?></b><br>
                        Età media M: <b><? echo intval($resultStatIma[0]['SumEtaM']/$resultStatIma[0]['numCasiM']) ?></b><br>
                        Età media F: <b><? echo intval($resultStatIma[0]['SumEtaF']/$resultStatIma[0]['numCasiF']) ?></b><br>
                        <br>
                        Età Massima pazienti: <b><? echo $resultStatIma[0]['MaxEta'] ?></b><br>
                        Età Massima M: <b><? echo $resultStatIma[0]['MaxEtaM'] ?></b><br>
                        Età Massima F: <b><? echo $resultStatIma[0]['MaxEtaF'] ?></b><br>
                        <br>
                        Età Minima pazienti: <b><? echo $resultStatIma[0]['MinEta'] ?></b><br>
                        Età Minima M: <b><? echo $resultStatIma[0]['MinEtaM'] ?></b><br>
                        Età Minima F: <b><? echo $resultStatIma[0]['MinEtaF'] ?></b><br>

                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingIschemia">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionIschemia" href="#collapseIschemia" aria-expanded="false" aria-controls="collapseIschemia">
                          Coronaropatia Ischemica
                        </a>
                      </h4>
                    </div>
                    <div id="collapseIschemia" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingIschemia">
                      <div class="panel-body">
                        Pazienti affetti: <b><? echo $resultStatIschemia[0]['numCasi'] ?></b> (<? echo number_format((float)($resultStatIschemia[0]['numCasi']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatIschemia[0]['numCasiM'] ?></b> (<? echo number_format((float)($resultStatIschemia[0]['numCasiM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatIschemia[0]['numCasiF'] ?></b> (<? echo number_format((float)($resultStatIschemia[0]['numCasiF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>
                        <br>
                        Età media pazienti: <b><? echo intval($resultStatIschemia[0]['SumEta']/$resultStatIschemia[0]['numCasi']) ?></b><br>
                        Età media M: <b><? echo intval($resultStatIschemia[0]['SumEtaM']/$resultStatIschemia[0]['numCasiM']) ?></b><br>
                        Età media F: <b><? echo intval($resultStatIschemia[0]['SumEtaF']/$resultStatIschemia[0]['numCasiF']) ?></b><br>
                        <br>
                        Età Massima pazienti: <b><? echo $resultStatIschemia[0]['MaxEta'] ?></b><br>
                        Età Massima M: <b><? echo $resultStatIschemia[0]['MaxEtaM'] ?></b><br>
                        Età Massima F: <b><? echo $resultStatIschemia[0]['MaxEtaF'] ?></b><br>
                        <br>
                        Età Minima pazienti: <b><? echo $resultStatIschemia[0]['MinEta'] ?></b><br>
                        Età Minima M: <b><? echo $resultStatIschemia[0]['MinEtaM'] ?></b><br>
                        Età Minima F: <b><? echo $resultStatIschemia[0]['MinEtaF'] ?></b><br>

                      </div>
                    </div>
                  </div>


                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingBPCO">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionBPCO" href="#collapseBPCO" aria-expanded="false" aria-controls="collapseBPCO">
                          BPCO
                        </a>
                      </h4>
                    </div>
                    <div id="collapseBPCO" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingBPCO">
                      <div class="panel-body">
                        Pazienti affetti: <b><? echo $resultStatBPCO[0]['numCasi'] ?></b> (<? echo number_format((float)($resultStatBPCO[0]['numCasi']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatBPCO[0]['numCasiM'] ?></b> (<? echo number_format((float)($resultStatBPCO[0]['numCasiM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatBPCO[0]['numCasiF'] ?></b> (<? echo number_format((float)($resultStatBPCO[0]['numCasiF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>
                        <br>
                        Età media pazienti: <b><? echo intval($resultStatBPCO[0]['SumEta']/$resultStatBPCO[0]['numCasi']) ?></b><br>
                        Età media M: <b><? echo intval($resultStatBPCO[0]['SumEtaM']/$resultStatBPCO[0]['numCasiM']) ?></b><br>
                        Età media F: <b><? echo intval($resultStatBPCO[0]['SumEtaF']/$resultStatBPCO[0]['numCasiF']) ?></b><br>
                        <br>
                        Età Massima pazienti: <b><? echo $resultStatBPCO[0]['MaxEta'] ?></b><br>
                        Età Massima M: <b><? echo $resultStatBPCO[0]['MaxEtaM'] ?></b><br>
                        Età Massima F: <b><? echo $resultStatBPCO[0]['MaxEtaF'] ?></b><br>
                        <br>
                        Età Minima pazienti: <b><? echo $resultStatBPCO[0]['MinEta'] ?></b><br>
                        Età Minima M: <b><? echo $resultStatBPCO[0]['MinEtaM'] ?></b><br>
                        Età Minima F: <b><? echo $resultStatBPCO[0]['MinEtaF'] ?></b><br>

                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingInsufRenCron">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionInsufRenCron" href="#collapseInsufRenCron" aria-expanded="false" aria-controls="collapseInsufRenCron">
                          Insufficenza Renale Cronica
                        </a>
                      </h4>
                    </div>
                    <div id="collapseInsufRenCron" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingInsufRenCron">
                      <div class="panel-body">
                        Pazienti affetti: <b><? echo $resultStatInsufRenCron[0]['numCasi'] ?></b> (<? echo number_format((float)($resultStatInsufRenCron[0]['numCasi']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatInsufRenCron[0]['numCasiM'] ?></b> (<? echo number_format((float)($resultStatInsufRenCron[0]['numCasiM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatInsufRenCron[0]['numCasiF'] ?></b> (<? echo number_format((float)($resultStatInsufRenCron[0]['numCasiF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>
                        <br>
                        Età media pazienti: <b><? echo intval($resultStatInsufRenCron[0]['SumEta']/$resultStatInsufRenCron[0]['numCasi']) ?></b><br>
                        Età media M: <b><? echo intval($resultStatInsufRenCron[0]['SumEtaM']/$resultStatInsufRenCron[0]['numCasiM']) ?></b><br>
                        Età media F: <b><? echo intval($resultStatInsufRenCron[0]['SumEtaF']/$resultStatInsufRenCron[0]['numCasiF']) ?></b><br>
                        <br>
                        Età Massima pazienti: <b><? echo $resultStatInsufRenCron[0]['MaxEta'] ?></b><br>
                        Età Massima M: <b><? echo $resultStatInsufRenCron[0]['MaxEtaM'] ?></b><br>
                        Età Massima F: <b><? echo $resultStatInsufRenCron[0]['MaxEtaF'] ?></b><br>
                        <br>
                        Età Minima pazienti: <b><? echo $resultStatInsufRenCron[0]['MinEta'] ?></b><br>
                        Età Minima M: <b><? echo $resultStatInsufRenCron[0]['MinEtaM'] ?></b><br>
                        Età Minima F: <b><? echo $resultStatInsufRenCron[0]['MinEtaF'] ?></b><br>

                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingAnemia">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionAnemia" href="#collapseAnemia" aria-expanded="false" aria-controls="collapseAnemia">
                          Anemia
                        </a>
                      </h4>
                    </div>
                    <div id="collapseAnemia" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingAnemia">
                      <div class="panel-body">
                        Pazienti affetti: <b><? echo $resultStatAnemia[0]['numCasi'] ?></b> (<? echo number_format((float)($resultStatAnemia[0]['numCasi']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatAnemia[0]['numCasiM'] ?></b> (<? echo number_format((float)($resultStatAnemia[0]['numCasiM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatAnemia[0]['numCasiF'] ?></b> (<? echo number_format((float)($resultStatAnemia[0]['numCasiF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>
                        <br>
                        Età media pazienti: <b><? echo intval($resultStatAnemia[0]['SumEta']/$resultStatAnemia[0]['numCasi']) ?></b><br>
                        Età media M: <b><? echo intval($resultStatAnemia[0]['SumEtaM']/$resultStatAnemia[0]['numCasiM']) ?></b><br>
                        Età media F: <b><? echo intval($resultStatAnemia[0]['SumEtaF']/$resultStatAnemia[0]['numCasiF']) ?></b><br>
                        <br>
                        Età Massima pazienti: <b><? echo $resultStatAnemia[0]['MaxEta'] ?></b><br>
                        Età Massima M: <b><? echo $resultStatAnemia[0]['MaxEtaM'] ?></b><br>
                        Età Massima F: <b><? echo $resultStatAnemia[0]['MaxEtaF'] ?></b><br>
                        <br>
                        Età Minima pazienti: <b><? echo $resultStatAnemia[0]['MinEta'] ?></b><br>
                        Età Minima M: <b><? echo $resultStatAnemia[0]['MinEtaM'] ?></b><br>
                        Età Minima F: <b><? echo $resultStatAnemia[0]['MinEtaF'] ?></b><br>

                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingColesterolo">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionColesterolo" href="#collapseColesterolo" aria-expanded="false" aria-controls="collapseColesterolo">
                          Colesterolo (LDL>100)
                        </a>
                      </h4>
                    </div>
                    <div id="collapseColesterolo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingColesterolo">
                      <div class="panel-body">
                        Pazienti affetti: <b><? echo $resultStatColesterolo[0]['numCasi'] ?></b> (<? echo number_format((float)($resultStatColesterolo[0]['numCasi']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatColesterolo[0]['numCasiM'] ?></b> (<? echo number_format((float)($resultStatColesterolo[0]['numCasiM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatColesterolo[0]['numCasiF'] ?></b> (<? echo number_format((float)($resultStatColesterolo[0]['numCasiF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>
                        <br>
                        Età media pazienti: <b><? echo intval($resultStatColesterolo[0]['SumEta']/$resultStatColesterolo[0]['numCasi']) ?></b><br>
                        Età media M: <b><? echo intval($resultStatColesterolo[0]['SumEtaM']/$resultStatColesterolo[0]['numCasiM']) ?></b><br>
                        Età media F: <b><? echo intval($resultStatColesterolo[0]['SumEtaF']/$resultStatColesterolo[0]['numCasiF']) ?></b><br>
                        <br>
                        Età Massima pazienti: <b><? echo $resultStatColesterolo[0]['MaxEta'] ?></b><br>
                        Età Massima M: <b><? echo $resultStatColesterolo[0]['MaxEtaM'] ?></b><br>
                        Età Massima F: <b><? echo $resultStatColesterolo[0]['MaxEtaF'] ?></b><br>
                        <br>
                        Età Minima pazienti: <b><? echo $resultStatColesterolo[0]['MinEta'] ?></b><br>
                        Età Minima M: <b><? echo $resultStatColesterolo[0]['MinEtaM'] ?></b><br>
                        Età Minima F: <b><? echo $resultStatColesterolo[0]['MinEtaF'] ?></b><br>

                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingRigurgito">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionRigurgito" href="#collapseRigurgito" aria-expanded="false" aria-controls="collapseRigurgito">
                          Rigurgito Mitralico Severo (+++)
                        </a>
                      </h4>
                    </div>
                    <div id="collapseRigurgito" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingRigurgito">
                      <div class="panel-body">
                        Pazienti affetti: <b><? echo $resultStatRigurgito[0]['numCasi'] ?></b> (<? echo number_format((float)($resultStatRigurgito[0]['numCasi']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatRigurgito[0]['numCasiM'] ?></b> (<? echo number_format((float)($resultStatRigurgito[0]['numCasiM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatRigurgito[0]['numCasiF'] ?></b> (<? echo number_format((float)($resultStatRigurgito[0]['numCasiF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>

                      </div>
                    </div>
                  </div>

                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingStenosi">
                      <h4 class="panel-title">
                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordionStenosi" href="#collapseStenosi" aria-expanded="false" aria-controls="collapseStenosi">
                          Stenosi Aortica
                        </a>
                      </h4>
                    </div>
                    <div id="collapseStenosi" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingStenosi">
                      <div class="panel-body">
                        Pazienti affetti: <b><? echo $resultStatStenosi[0]['numCasi'] ?></b> (<? echo number_format((float)($resultStatStenosi[0]['numCasi']/$result[0]['NumTotale']*100), 2, '.', '') ?>%)<br>
                        Maschi: <b><? echo $resultStatStenosi[0]['numCasiM'] ?></b> (<? echo number_format((float)($resultStatStenosi[0]['numCasiM']/$result[0]['NumMaschi']*100), 2, '.', '') ?>%)<br>
                        Femmine: <b><? echo $resultStatStenosi[0]['numCasiF'] ?></b> (<? echo number_format((float)($resultStatStenosi[0]['numCasiF']/$result[0]['NumFemmine']*100), 2, '.', '') ?>%)<br>

                      </div>
                    </div>
                  </div>


                </div>

            </div>




            <div style="clear:both;"></div>
            <div class="col-md-12" style="margin-top:20px;">

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Statistiche Fattori di Rischio e Comorbidità
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                            <div id="chart_fattori_rischio" class="chart" style="height:400px;width: 100%;"></div>       
                      </div>
                    </div>
                  </div>
                </div>

            </div>

            <div class="col-md-12" style="margin-top:20px;">

                <div class="panel-group" id="accordionEziologia" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingEziologia">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordionEziologia" href="#collapseEziologia" aria-expanded="true" aria-controls="collapseOne">
                          Statistiche Eziologia
                        </a>
                      </h4>
                    </div>
                    <div id="collapseEziologia" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingEziologia">
                      <div class="panel-body">
                            <div id="chart_eziologia" class="chart" style="height:400px;width: 100%;"></div>       
                      </div>
                    </div>
                  </div>
                </div>

            </div>

            <div class="col-md-12" style="margin-top:20px;">

                <div class="panel-group" id="accordionClinica" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingClinica">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordionClinica" href="#collapseClinica" aria-expanded="true" aria-controls="collapseOne">
                          Statistiche Clinica
                        </a>
                      </h4>
                    </div>
                    <div id="collapseClinica" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingClinica">
                      <div class="panel-body">
                            <div id="chart_clinica" class="chart" style="height:400px;width: 100%;"></div>       
                      </div>
                    </div>
                  </div>
                </div>

            </div>

        </div>
    </div>

    <script src="xcrud/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/flot/jquery.flot.min.js"></script>
    <script src="js/flot/jquery.flot.resize.min.js"></script>
    <script src="js/flot/jquery.flot.pie.min.js"></script>
    <script src="js/flot/jquery.flot.symbol.js"></script>
    <script src="js/flot/jquery.flot.stack.min.js"></script>
    <script src="js/flot/jquery.flot.axislabels.js"></script>
    <script src="js/flot/jquery.flot.time.js"></script>
    <script src="js/flot/jquery.flot.crosshair.min.js"></script>
    <script src="js/flot/jquery.flot.categories.min.js" type="text/javascript"></script>



    <script>

    function nuovoreclutamento()
    {
        window.location.href = 'schedaPaziente.php?id=-1';
    }

    function logoff()
    {
        window.location.href = 'logout.php';
    }

    
    function disegnaStatistiche()
    {
        // pie pazienti
        var data = new Array();
        data.push(new Object({label:"Pazienti in Studio",data:<? echo $vivi ?>,color:"#145592"}));
        data.push(new Object({label:"Deceduti",data:<? echo $result[0]['dead'] ?>,color:"#7c901e"}));

        $.plot($('#pie_chart_pazienti'), data, {
        series: {
            pie: {
                show: true,
                radius: 1.0,
                tilt: 1.0,
                label: {
                    show: true,
                    radius: 0.5,
                    formatter: function(label, series) {
                        return '<div style="font-size:8pt;text-align:center;padding:2px;color:white;">' + label + '<br/>' + Math.round(series.percent) + '%</div>';
                    },
                    background: {
                        opacity: 0.5
                    }
                }                        
            }
        },
            legend: {
                show: true
            }
        });


        //grafico fattori di rischio
        var ext_data = [
                ["Ipertensione",<? echo $resultStatNew['NumCasiIpertM']?>,<? echo $resultStatNew['NumCasiIpertF']?>],
                ["BPCO",<? echo $resultStatNew['NumCasiBPCOM']?>,<? echo $resultStatNew['NumCasiBPCOF']?>],
                ["Anemia",<? echo $resultStatNew['NumCasiAnemiaM']?>,<? echo $resultStatNew['NumCasiAnemiaF']?>],
                ["IperDislipidemia",<? echo $resultStatNew['NumCasiIperdislipidemiaM']?>,<? echo $resultStatNew['NumCasiIperdislipidemiaF']?>],
                ["Diabete",<? echo $resultStatNew['NumCasiDiabeteM']?>,<? echo $resultStatNew['NumCasiDiabeteF']?>],
                ["Insufficienza Renale Cronica",<? echo $resultStatNew['NumCasiInsuffRenaleM']?>,<? echo $resultStatNew['NumCasiInsuffRenaleF']?>]
             ];


        var data = [{
            data: [
                [<? echo $resultStatNew['NumCasiIpert']?>, 1],
                [<? echo $resultStatNew['NumCasiBPCO']?>, 2],
                [<? echo $resultStatNew['NumCasiAnemia']?>, 3],
                [<? echo $resultStatNew['NumCasiIperdislipidemia']?>, 4],
                [<? echo $resultStatNew['NumCasiDiabete']?>, 5],
                [<? echo $resultStatNew['NumCasiInsuffRenale']?>, 6]
            ], myData:ext_data, color: "#94ae0a"
        } ];

        var options = {
            series: {
                bars: {
                    show: 1,
                    barWidth: 0.6,
                    fill: 0.8,
                    align: 'center',
                    horizontal: true
                },
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            legend: {
                show: false
            },
            yaxis: {
                ticks: [
                    [1, 'Ipertensione'],
                    [2, 'BPCO'],
                    [3, 'Anemia'],
                    [4, 'IperDislipidemia'],
                    [5, 'Diabete'],
                    [6, 'Insufficienza Renale Cronica']
                ]
            },
            xaxis: {
                axisLabel: "Numero di Pazienti",
                axisLabelPadding: 10,
                max: <? echo $resultStatNew['NumTotaleGenerale']?>
            }
        };

        $.plot($("#chart_fattori_rischio"), data, options);

        $("#chart_fattori_rischio").bind("plothover", function (event, pos, item) {
     
            if (item) {
                //console.log(item.series.myData[item.dataIndex]);
                var x = parseInt(item.datapoint[0]);
                var pazTotaliPerc=parseFloat(x/<? echo $result[0]['NumTotale'] ?>*100).toFixed(1);

                $("#tooltip").html("Numero Pazienti con " + item.series.myData[item.dataIndex][0] + ": " + x + "<br>("+pazTotaliPerc+"% sul totale)<br>Maschi: " + item.series.myData[item.dataIndex][1] + "<br>Femmine: " + item.series.myData[item.dataIndex][2])
                    .css({top: item.pageY, left: item.pageX})
                    .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
        });



        //grafico eziologia
        var ext_data = [
                ["Ischemia",<? echo $resultStatEziologiaNew['NumCasiIschemiaM']?>,<? echo $resultStatEziologiaNew['NumCasiIschemiaF']?>],
                ["Dilatativa",<? echo $resultStatEziologiaNew['NumCasiDilatativaM']?>,<? echo $resultStatEziologiaNew['NumCasiDilatativaF']?>],
                ["Valvolopatie",<? echo $resultStatEziologiaNew['NumCasiValvolopatieM']?>,<? echo $resultStatEziologiaNew['NumCasiValvolopatieF']?>],
                ["Ipertensiva",<? echo $resultStatEziologiaNew['NumCasiIpertensivaM']?>,<? echo $resultStatEziologiaNew['NumCasiIpertensivaF']?>],
                ["Ipertensiva + Valvolare",<? echo $resultStatEziologiaNew['NumCasiIpertensivaValvolareM']?>,<? echo $resultStatEziologiaNew['NumCasiIpertensivaValvolareF']?>],
                ["Ischemia + Valvolare",<? echo $resultStatEziologiaNew['NumCasiIschemiaValvolareM']?>,<? echo $resultStatEziologiaNew['NumCasiIschemiaValvolareF']?>]
             ];


        var data = [{
            data: [
                [<? echo $resultStatEziologiaNew['NumCasiIschemia']?>, 1],
                [<? echo $resultStatEziologiaNew['NumCasiDilatativa']?>, 2],
                [<? echo $resultStatEziologiaNew['NumCasiValvolopatie']?>, 3],
                [<? echo $resultStatEziologiaNew['NumCasiIpertensiva']?>, 4],
                [<? echo $resultStatEziologiaNew['NumCasiIpertensivaValvolare']?>, 5],
                [<? echo $resultStatEziologiaNew['NumCasiIschemiaValvolare']?>, 6]
            ], myData:ext_data, color: "#145592"
        } ];

        var options = {
            series: {
                bars: {
                    show: 1,
                    barWidth: 0.6,
                    fill: 0.8,
                    align: 'center',
                    horizontal: true
                },
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            legend: {
                show: false
            },
            yaxis: {
                ticks: [
                    [1, 'Ischemia'],
                    [2, 'Dilatativa'],
                    [3, 'Valvolopatie'],
                    [4, 'Ipertensiva'],
                    [5, 'Ipertensiva + Valvolare'],
                    [6, 'Ischemia + Valvolare']
                ]
            },
            xaxis: {
                axisLabel: "Numero di Pazienti",
                axisLabelPadding: 10,
                max: <? echo $resultStatNew['NumTotaleGenerale']?>
            }
        };



        $.plot($("#chart_eziologia"), data, options);

        $("#chart_eziologia").bind("plothover", function (event, pos, item) {
     
            if (item) {
                //console.log(item.series.myData[item.dataIndex]);
                var x = parseInt(item.datapoint[0]);
                var pazTotaliPerc=parseFloat(x/<? echo $result[0]['NumTotale'] ?>*100).toFixed(1);

                $("#tooltip").html("Numero Pazienti con " + item.series.myData[item.dataIndex][0] + ": " + x + "<br>("+pazTotaliPerc+"% sul totale)<br>Maschi: " + item.series.myData[item.dataIndex][1] + "<br>Femmine: " + item.series.myData[item.dataIndex][2])
                    .css({top: item.pageY, left: item.pageX})
                    .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
        });


        //grafico clinica
        var ext_data = [
                ["Classe NYHA (dalla III alla IV)",<? echo $resultStatClinicaNew['NumCasiNyhaM']?>,<? echo $resultStatClinicaNew['NumCasiNyhaF']?>],
                ["FE < 35%",<? echo $resultStatClinicaNew['NumCasiFEM']?>,<? echo $resultStatClinicaNew['NumCasiFEF']?>],
                ["35% <= FE < 50%",<? echo $resultStatClinicaNew['NumCasiIntervalloFEM']?>,<? echo $resultStatClinicaNew['NumCasiIntervalloFEF']?>],
                ["Ipertensione Polmonare",<? echo $resultStatClinicaNew['NumCasiIpertPolmonareM']?>,<? echo $resultStatClinicaNew['NumCasiIpertPolmonareF']?>],
                ["Disfunzione Diastolica",<? echo $resultStatClinicaNew['NumCasiDisfDiastolicaM']?>,<? echo $resultStatClinicaNew['NumCasiDisfDiastolicaF']?>]
             ];


        var data = [{
            data: [
                [<? echo $resultStatClinicaNew['NumCasiNyha']?>, 1],
                [<? echo $resultStatClinicaNew['NumCasiFE']?>, 2],
                [<? echo $resultStatClinicaNew['NumCasiIntervalloFE']?>, 3],
                [<? echo $resultStatClinicaNew['NumCasiIpertPolmonare']?>, 4],
                [<? echo $resultStatClinicaNew['NumCasiDisfDiastolica']?>, 5]
            ], myData:ext_data, color: "#ff9421"
        } ];

        var options = {
            series: {
                bars: {
                    show: 1,
                    barWidth: 0.6,
                    fill: 0.8,
                    align: 'center',
                    horizontal: true
                },
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            legend: {
                show: false
            },
            yaxis: {
                ticks: [
                    [1, 'Classe NYHA (dalla III alla IV)'],
                    [2, 'FE < 35%'],
                    [3, '35% <= FE < 50%'],
                    [4, 'Ipertensione Polmonare'],
                    [5, 'Disfunzione Diastolica']
                ]
            },
            xaxis: {
                axisLabel: "Numero di Pazienti",
                axisLabelPadding: 10,
                max: <? echo $resultStatNew['NumTotaleGenerale']?>
            }
        };



        $.plot($("#chart_clinica"), data, options);

        $("#chart_clinica").bind("plothover", function (event, pos, item) {
     
            if (item) {
                //console.log(item.series.myData[item.dataIndex]);
                var x = parseInt(item.datapoint[0]);
                var pazTotaliPerc=parseFloat(x/<? echo $result[0]['NumTotale'] ?>*100).toFixed(1);

                $("#tooltip").html("Numero Pazienti con " + item.series.myData[item.dataIndex][0] + ": " + x + "<br>("+pazTotaliPerc+"% sul totale)<br>Maschi: " + item.series.myData[item.dataIndex][1] + "<br>Femmine: " + item.series.myData[item.dataIndex][2])
                    .css({top: item.pageY, left: item.pageX})
                    .fadeIn(200);
            } else {
                $("#tooltip").hide();
            }
        });


    }


    $( window ).resize(function() {
      var larghezza=$(window).width();
      if (larghezza>=1024) $('#contenuto').css("width","1024px");
      else $('#contenuto').css("width","100%");
      var larghFinestra=$('#contenuto').width();
      //console.log(larghFinestra);
      //console.log(larghezza);
      $('#contenuto').css("margin-left",(larghezza-larghFinestra)/2+"px");

    });

    function goMain()
    {
        window.location.href = 'main.php';
    }    

    jQuery(document).ready(function() { 

        $(window).trigger('resize');
        $('#contenuto').show();
        disegnaStatistiche();

    });

    //$('.collapse').collapse()
    </script>


</body>
</html>
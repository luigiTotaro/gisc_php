<?php
    ob_start(); 
    session_name( 'PHPSESSID' );
    session_start();
    include_once "./utils_db.php";
    
    //se non loggato ritorno a login
    if (!isset($_SESSION['IdUtente'])){
        header("location: index.php");//redirect
    }else{
        $idUtente = $_SESSION['IdUtente'];
        $nominativo = $_SESSION['Nominativo'];
        $tipologiaUtente = $_SESSION['Tipologia'];
    }

    $IdQuadroClinico=NULL;

    if (isset($_GET['type'])) //dovrebbe esserci sempre, in caso di arrivo con idPaziente... in caso di arrivo con idQuadro non serve saperlo.....
    {
        $tipoScheda = $_GET['type'];  
    }

    //echo $tipoScheda;
    
    if (isset($_GET['id']))
    {
        //è arrivato qui con un id impostato.. lo considero
        $userId = $_GET['id'];  
    }
    else if (isset($_GET['idQuadro']))
    {
        //è arrivato qui con un id di un quadro clinico... non è la scheda del reclutamento
        $IdQuadroClinico = $_GET['idQuadro'];  

        /*
        echo ("****");
        echo ($_SESSION['IdPaziente']);
        echo ("****");
        //nessun id, vediamo se lo ho in sessione (inserimento/modifica)
        if (isset($_SESSION['IdPaziente']))
        {
            $userId = $_SESSION['IdPaziente'];
            unset($_SESSION['IdPaziente']);
        }
        else
        {
            //non posso continuare
            //header("location: main.php");
        }
        */
    }

    //echo ("****");
    //echo ($userId);
    //echo ("****");
    //echo ($IdQuadroClinico);
    //echo ("****");
    //echo $tipologiaUtente;

    //$userId=-1;

    include('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
    $xcrudQuadroClinico = Xcrud::get_instance();
    $xcrudTerapia = Xcrud::get_instance();
    $xcrudEmatochimici = Xcrud::get_instance();
    $xcrudUrine = Xcrud::get_instance();
    $xcrudCardiologo = Xcrud::get_instance();
    $xcrudPneumologo = Xcrud::get_instance();
    $xcrudMedicoDiBase = Xcrud::get_instance();
    $xcrudNefrologo = Xcrud::get_instance();
    $xcrudNutrizionista = Xcrud::get_instance();
    $xcrudDiabetologo = Xcrud::get_instance();
    $xcrudQualitaVita = Xcrud::get_instance();
    $xcrudQualitaSonno = Xcrud::get_instance();
    $xcrudPrecedenti = Xcrud::get_instance();
    $xcruddate = Xcrud::get_instance();
    $xcrudAritmie = Xcrud::get_instance();
    $xcrudEsamiStrumentali = Xcrud::get_instance();
    $xcrudDiagnosi = Xcrud::get_instance();


    $db = Xcrud_db::get_instance();

    $utils_db = new utils_db();


?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Some page title</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
    <link href="css/glyphicons.css" rel="stylesheet" type="text/css" />
    <script defer src="js/fontawesome-all.js"></script>

</head>
 
<body style="background-color:#cccccc;">

    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;display:none;">
    
        <div style="width:100%;">
            <img src="img/testataGisc.jpg" style="width:100%;">
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="logoff();">Esci</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goMain();">Pannello Principale</button>
            <?php
            if (!isset($userId))
            {?>
                <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goStoria();">Storia Paziente</button>
            <?}
            ?>
            <div style="clear:both;"></div>
        </div>
        <div style="width:100%;padding:5%;">

            <?php
                $stileNuovoReclutamento="";
                $cursorNuovoReclutamento="";
                $messaggioPerUtente="";

                //booleani che mi docono cosa mostrare
                $schedaReclutamento=false;
                $schedaCardiologo=false; //coinciderebbe, come stile, alla scheda reclutamento
                $schedaMMG=false;
                $schedaNefrologo=false;
                $schedaPneumologo=false;
                $schedaNutrizionista=false;
                $schedaDiabetologo=false;

                if (isset($userId)) // è arrivato con uno userid, dunque è una scheda reclutamento
                {

                    $mostraDataControllo=false;

                    if ($userId==-1)
                    {
                        $schedaReclutamento=true;

                        //deve essere abilitato solo l'anagrafica.. dopo che inserirà l'utente la pagina verrà ricaricata con tutte le schede disponibili
                        $stileNuovoReclutamento="disabled";
                        $cursorNuovoReclutamento="cursor: not-allowed;";
                        ?>
                        <h3 class="titolo">Reclutamento Nuovo Paziente</h3>
                        <?
                    }
                    else
                    {
                        $schedaReclutamento=false;
                        $schedaCardiologo=false;
                        $schedaMMG=false;
                        $schedaNefrologo=false;
                        $schedaPneumologo=false;
                        $schedaNutrizionista=false; 
                        $schedaDiabetologo=false;

                        //è arrivato con un id impostato.... può essere un nuovo reclutamento (con anagrafica già compilata), un nuovo controllo, un nuovo ricovero, ecc
                        // o la scheda del reclutamento

                        //DATI PAZIENTE 
                        $sel = "SELECT *, DATE_FORMAT(DataRegistrazione, '%d-%m-%Y') as DataRegistrazioneFormat, DATE_FORMAT(gdead, '%d-%m-%Y') as DataDecesso, DATE_FORMAT(data_nascita, '%d-%m-%Y') as DataNascita FROM paziente, utente WHERE paziente.IdUtente=utente.IdUtente and IdPaziente =".$userId;
                        $db->query($sel);
                        $result = $db->result();
                        $resultPaziente = $result[0];            
                        $nomeCognome = $resultPaziente["Cognome"].' '.$resultPaziente["Nome"];   
                        
                        //se anno di nascita à avvalorato, calcolo l'età da quello, altrimenti prendo il campo età del db
                        if ($resultPaziente["data_nascita"]==NULL)
                        {
                            $anni=$resultPaziente["Eta"];
                        }
                        else
                        {
                            //echo $resultPaziente["data_nascita"];
                            $birthDate = explode("-", $resultPaziente["DataNascita"]);
                            $anni = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[0], $birthDate[2]))) > date("md")
                            ? ((date("Y") - $birthDate[2]) - 1)
                            : (date("Y") - $birthDate[2]));
                        }

                        if ($tipoScheda=="NREC") //nuovo reclutamento
                        {
                            //ha censito l'utente, ora creo la riga in quadroclinico e faccio inserire i dati
                            //per evitare problemi di inserimenti multipli di reclutamento casomai riclicca ricarica con il parametro NREC, dopo l'inseriemtno lo ridirigo alla scheda


                            $insertQuery = "INSERT INTO QuadroClinico (IdPaziente,IdUtente,gTipo) VALUES (".$userId.",".$idUtente.",'RECLUTAMENTO')";
                            $db->query($insertQuery);
                            $IdQuadroClinico = $db->insert_id();  
                            $schedaReclutamento=true; 
                                                 
                        }
                        else if ($tipoScheda=="NC") //nuovo controllo
                        {
                            $mostraDataControllo=true;
                            $messaggioPerUtente="Inserire la data del Controllo";
                        }
                        else if ($tipoScheda=="NRIC") //nuovo ricovero
                        {
                            $mostraDataControllo=true;
                            $messaggioPerUtente="Inserire la data del Ricovero";
                        }
                        else if ($tipoScheda=="RECL") //vado alla scheda del reclutamento
                        {
                            //devo trovare l'id della chiave del quadro relativo alla tabella
                            $sel = "SELECT IdQuadroClinico from QuadroClinico where gTipo ='RECLUTAMENTO' AND IdPaziente=".$userId;
                            $db->query($sel);
                            $result = $db->result();
                            if (count($result)==0)
                            {
                                //ERRORE... il RECLUTAMENTO CI DEVE ESSERE
                                
                            }
                            else
                            {
                                $IdQuadroClinico = $result[0]["IdQuadroClinico"];   
                            }
                            $schedaReclutamento=true;
                            //echo 'QuadroClinico: '.$IdQuadroClinico;
                            //echo '<pre>'; print_r($result); echo '</pre>';
                        }

                        ?>
                        <h3 class="titolo"><? echo $nomeCognome ?> - Anni: <? echo $anni ?></h3>
                        <h4 >Reclutato il <? echo $resultPaziente["DataRegistrazioneFormat"] ?></h4>
                        <?
                    }
                }
                else //è arrivato con un id quadro
                {
                    //echo("test");
                    //devo vedere quel quadro da chi è stato compilato
                    $schedaReclutamento=false;
                    $schedaCardiologo=false;
                    $schedaMMG=false;
                    $schedaNefrologo=false;
                    $schedaPneumologo=false;
                    $schedaNutrizionista=false; 
                    $schedaDiabetologo=false;

                    //trovo l'id paziente e altri dati
                    $mostraDataControllo=true;

                    $sel = "SELECT DATE_FORMAT(qc.DataQuadroClinico, '%d-%m-%Y') as DataQuadroClinicoFormat, qc.IdPaziente, qc.IdUtente, tu.nome as tipologia, qc.gTipo from QuadroClinico qc, utente u, tipo_utente tu where qc.IdUtente=u.IdUtente AND u.IdTipoUtente=tu.IdTipoUtente AND IdQuadroClinico=".$IdQuadroClinico;
                    $db->query($sel);
                    $result = $db->result();
                    $userId = $result[0]["IdPaziente"];   
                    $utenteIdQuadro = $result[0]["IdUtente"];   
                    $tipologiaUtenteQuadro = $result[0]["tipologia"]; 
                    $tipologiaControlloQuadro = $result[0]["gTipo"]; 
                    $dataQuadroClinico = $result[0]["DataQuadroClinicoFormat"]; 
                    //echo $userId."<br>";  
                    //echo $utenteIdQuadro."<br>";  
                    //echo $tipologiaUtenteQuadro."<br>";  
                    //echo $tipologiaControlloQuadro;  

                    $tipoControllo;
                    if ($tipologiaControlloQuadro=="RECLUTAMENTO")
                    {
                        $schedaReclutamento=true;
                        $tipoControllo = "Reclutamento";
                    }
                    else if ($tipologiaUtenteQuadro=="Medico di Medicina Generale") 
                    {
                        $schedaMMG=true;
                        $tipoControllo = "Controllo MMG";
                    }
                    else if ($tipologiaUtenteQuadro=="Cardiologo Ambulatoriale") 
                    {
                        $schedaCardiologo=true;
                        $tipoControllo = "Controllo Specialistico Cardiologico";
                    }
                    else if ($tipologiaUtenteQuadro=="Admin") 
                    {
                        $schedaCardiologo=true;
                        $tipoControllo = "controllo Amministrativo";
                    }
                    else if ($tipologiaUtenteQuadro=="Ospedaliero") 
                    {
                        $schedaCardiologo=true; //non ce ne stanno nel db, comunque
                        $tipoControllo = "Controllo Ospedaliero";
                    }
                    else if ($tipologiaUtenteQuadro=="Specialista Pneumologo") 
                    {
                        $schedaPneumologo=true;
                        $tipoControllo = "Controllo Specialistico Pneumologico";
                    }
                    else if ($tipologiaUtenteQuadro=="Specialista Nefrologo") 
                    {
                        $schedaNefrologo=true;
                        $tipoControllo = "Controllo Specialistico Nefrologico";
                    }
                    else if ($tipologiaUtenteQuadro=="Nutrizionista") 
                    {
                        $schedaNutrizionista=true;
                        $tipoControllo = "Controllo Nutrizionista";
                    }
                    else if ($tipologiaUtenteQuadro=="Diabetologo") 
                    {
                        $schedaDiabetologo=true;
                        $tipoControllo = "Controllo Diabetologico";
                    }


                    //in base a questo prendo i vari dati del paziente
                    //DATI PAZIENTE 
                    $sel = "SELECT *, DATE_FORMAT(DataRegistrazione, '%d-%m-%Y') as DataRegistrazioneFormat, DATE_FORMAT(gdead, '%d-%m-%Y') as DataDecesso, DATE_FORMAT(data_nascita, '%d-%m-%Y') as DataNascita FROM paziente, utente WHERE paziente.IdUtente=utente.IdUtente and IdPaziente =".$userId;
                    $db->query($sel);
                    $result = $db->result();
                    $resultPaziente = $result[0];            
                    $nomeCognome = $resultPaziente["Cognome"].' '.$resultPaziente["Nome"];   
                    
                    //se anno di nascita à avvalorato, calcolo l'età da quello, altrimenti prendo il campo età del db
                    if ($resultPaziente["data_nascita"]==NULL)
                    {
                        $anni=$resultPaziente["Eta"];
                    }
                    else
                    {
                        //echo $resultPaziente["data_nascita"];
                        $birthDate = explode("-", $resultPaziente["DataNascita"]);
                        $anni = (date("md", date("U", mktime(0, 0, 0, $birthDate[1], $birthDate[0], $birthDate[2]))) > date("md")
                        ? ((date("Y") - $birthDate[2]) - 1)
                        : (date("Y") - $birthDate[2]));
                    }

                    ?>
                    <h3 class="titolo"><? echo $nomeCognome ?> - Anni: <? echo $anni ?></h3>
                    <h4 >Reclutato il <? echo $resultPaziente["DataRegistrazioneFormat"] ?></h4>
                    <h4 ><? echo $tipoControllo ?> del <? echo $dataQuadroClinico ?></h4>
                    <?
                }

                //ho un idPaziente e un IdQuadro, vedo se è un reclutamento o no
                $sel = "SELECT IdQuadroClinico from QuadroClinico where gTipo ='RECLUTAMENTO' AND IdPaziente=".$userId;
                $db->query($sel);
                $result = $db->result();
                $IdQuadroClinicoReclutamento = $result[0]["IdQuadroClinico"];   

                //a seconda del tipo di quadro, abilito o meno i pulsanti e le relative schede
                if ($schedaReclutamento)
                {
                    $showFirst="anagrafica";
                    ?>
                    <div class="row" style="margin-top:40px;">
                        <div class="col-sm-1 colonne-strette"></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonAnagrafica"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" onClick="scegliScheda('ANAGRAFICA');"><i style="" class="fas fa-user"></i><br>Anagrafica</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonPrecedenti" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('PRECEDENTI');"><i style="" class="glyphicons glyphicons-hospital-h"></i><br>Prec. Scompen.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonAnamnesi" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('ANAMNESI');"><i style="" class="glyphicons glyphicons-hospital"></i><br>Anam.Risc.Eziol.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonCardiologo" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('CARDIOLOGO');"><i style="" class="glyphicons glyphicons-folder-heart"></i><br>Quadro Clinico</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonAritmie" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('ARITMIE');"><i style="" class="glyphicons glyphicons-heartbeat"></i><br>Aritmie</button></div>
                        <div class="col-sm-1 colonne-strette"></div>
                    </div>
                    <div class="row" style="margin-top:15px;">
                        <div class="col-sm-1 colonne-strette"></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonEsamiStrumentali" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('ESAMISTRUMENTALI');"><i style="" class="fas fa-stethoscope"></i><br>Esami Strument.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonEmatochimici" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('EMATOCHIMICI');"><i style="" class="glyphicons glyphicons-lab-alt"></i><br>Es. Ematoch.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonFarmacologica" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('FARMACOLOGICA');"><i style="" class="fas fa-pills"></i><br>Terapia Farmac.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonDiagnosi" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('DIAGNOSI');"><i style="" class="far fa-edit"></i><br>Diagnosi</button></div>
                    </div>
                    <?php 
                }
                else if ($schedaCardiologo)
                {
                    $showFirst="aritmie";
                    ?>
                    <div class="row" style="margin-top:40px;">
                        <div class="col-sm-2 colonne-strette" id="divButtonAritmie" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('ARITMIE');"><i style="" class="glyphicons glyphicons-heartbeat"></i><br>Aritmie</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonCardiologo" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('CARDIOLOGO');"><i style="" class="glyphicons glyphicons-folder-heart"></i><br>Quadro Clinico</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonEsamiStrumentali" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('ESAMISTRUMENTALI');"><i style="" class="fas fa-stethoscope"></i><br>Esami Strument.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonEmatochimici" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('EMATOCHIMICI');"><i style="" class="glyphicons glyphicons-lab-alt"></i><br>Es. Ematoch.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonFarmacologica" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('FARMACOLOGICA');"><i style="" class="fas fa-pills"></i><br>Terapia Farmac.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonDiagnosi" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('DIAGNOSI');"><i style="" class="far fa-edit"></i><br>Diagnosi</button></div>
                    </div>
                    <?php 
                }
                else if ($schedaMMG)
                {
                    $showFirst="medicoBase";
                    ?>
                    <div class="row" style="margin-top:40px;">
                        <div class="col-sm-2 colonne-strette" id="divButtonMedicoBase" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('MEDICODIBASE');"><i style="" class="fas fa-plus-square"></i><br>Medico di Base</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonFarmacologica" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('FARMACOLOGICA');"><i style="" class="fas fa-pills"></i><br>Terapia Farmac.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonEmatochimici" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('EMATOCHIMICI');"><i style="" class="glyphicons glyphicons-lab-alt"></i><br>Es. Ematoch.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonQualitaVita" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('QUALITAVITA');"><i style="" class="far fa-thumbs-up"></i><br>Qualità Vita</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonQualitaSonno" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('QUALITASONNO');"><i style="" class="fas fa-bed"></i><br>Qualità Sonno</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonDiagnosi" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('DIAGNOSI');"><i style="" class="far fa-edit"></i><br>Diagnosi</button></div>
                    </div>
                    <?php 
                }
                else if ($schedaPneumologo)
                {
                    $showFirst="pneumologo";
                    ?>
                    <div class="row" style="margin-top:40px;">
                        <div class="col-sm-2 colonne-strette"></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonPneumologo" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('PNEUMOLOGO');"><i style="" class="glyphicons glyphicons-folder-heart"></i><br>Quadro Clinico</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonFarmacologica" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('FARMACOLOGICA');"><i style="" class="fas fa-pills"></i><br>Terapia Farmac.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonEmatochimici" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('EMATOCHIMICI');"><i style="" class="glyphicons glyphicons-lab-alt"></i><br>Es. Ematoch.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonDiagnosi" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('DIAGNOSI');"><i style="" class="far fa-edit"></i><br>Diagnosi</button></div>
                        <div class="col-sm-2 colonne-strette"></div>
                    </div>
                    <?php 
                }
                else if ($schedaNefrologo)
                {
                    $showFirst="nefrologo";
                    ?>
                    <div class="row" style="margin-top:40px;">
                        <div class="col-sm-2 colonne-strette"></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonNefrologo" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('NEFROLOGO');"><i style="" class="glyphicons glyphicons-folder-heart"></i><br>Quadro Clinico</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonFarmacologica" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('FARMACOLOGICA');"><i style="" class="fas fa-pills"></i><br>Terapia Farmac.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonEmatochimici" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('EMATOCHIMICI');"><i style="" class="glyphicons glyphicons-lab-alt"></i><br>Es. Ematoch.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonDiagnosi" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('DIAGNOSI');"><i style="" class="far fa-edit"></i><br>Diagnosi</button></div>
                        <div class="col-sm-2 colonne-strette"></div>
                    </div>
                    <?php 
                }
                else if ($schedaNutrizionista)
                {
                    $showFirst="nutrizionista";
                    ?>
                    <div class="row" style="margin-top:40px;">
                        <div class="col-sm-3 colonne-strette"></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonNutrizionista" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('NUTRIZIONISTA');"><i style="" class="fas fa-weight"></i><br>Nutrizionista</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonEmatochimici" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('EMATOCHIMICI');"><i style="" class="glyphicons glyphicons-lab-alt"></i><br>Es. Ematoch.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonDiagnosi" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('DIAGNOSI');"><i style="" class="far fa-edit"></i><br>Diagnosi</button></div>
                        <div class="col-sm-3 colonne-strette"></div>
                    </div>
                    <?php 
                }
                else if ($schedaDiabetologo)
                {
                    $showFirst="diabetologo";
                    ?>
                    <div class="row" style="margin-top:40px;">
                        <div class="col-sm-1 colonne-strette"></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonDiabetologo" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('DIABETOLOGO');"><i style="" class="glyphicons glyphicons-folder-heart"></i><br>Quadro Clinico</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonEsamiStrumentali" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('ESAMISTRUMENTALI');"><i style="" class="fas fa-stethoscope"></i><br>Esami Strument.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonEmatochimici" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('EMATOCHIMICI');"><i style="" class="glyphicons glyphicons-lab-alt"></i><br>Es. Ematoch.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonFarmacologica" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('FARMACOLOGICA');"><i style="" class="fas fa-pills"></i><br>Terapia Farmac.</button></div>
                        <div class="col-sm-2 colonne-strette" id="divButtonDiagnosi" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('DIAGNOSI');"><i style="" class="far fa-edit"></i><br>Diagnosi</button></div>
                        <div class="col-sm-1 colonne-strette"></div>
                    </div>
                    <?php 
                }

            ?>


<!--
            <div class="row" style="margin-top:40px;">
                <div class="col-sm-2 colonne-strette" id="divButtonAnagrafica"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" onClick="scegliScheda('ANAGRAFICA');"><i style="" class="fas fa-user"></i><br>Anagrafica</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonAnamnesi" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('ANAMNESI');"><i style="" class="fas fa-stethoscope"></i><br>Anam.Risc.Eziol.</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonPrecedenti" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('PRECEDENTI');"><i style="" class="fas fa-stethoscope"></i><br>Prec. per scompenso</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonFarmacologica" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('FARMACOLOGICA');"><i style="" class="fas fa-pills"></i><br>Terapia Farmac.</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonEmatochimici" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('EMATOCHIMICI');"><i style="" class="glyphicons glyphicons-lab-alt"></i><br>Es. Ematoch.</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonCardiologo" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('CARDIOLOGO');"><i style="" class="fas fa-heartbeat"></i><br>Quadro Clinico</button></div>
            </div>
            <div class="row" style="margin-top:15px;">
                <div class="col-sm-2 colonne-strette" id="divButtonAritmie" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('ARITMIE');"><i style="" class="fas fa-user-md"></i><br>Aritmie</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonEsamiStrumentali" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('ESAMISTRUMENTALI');"><i style="" class="fas fa-user-md"></i><br>Esami Strumentali</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonPneumologo" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('PNEUMOLOGO');"><i style="" class="fas fa-user-md"></i><br>Sp. Pneumol.</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonNefrologo" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('NEFROLOGO');"><i style="" class="fas fa-filter"></i><br>Sp. Nefrologo</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonMedicoBase" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('MEDICODIBASE');"><i style="" class="fas fa-plus-square"></i><br>Medico di Base</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonNutrizionista" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('NUTRIZIONISTA');"><i style="" class="fas fa-weight"></i><br>Nutrizionista</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonQualitaVita" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('QUALITAVITA');"><i style="" class="far fa-thumbs-up"></i><br>Qualità Vita</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonQualitaSonno" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('QUALITASONNO');"><i style="" class="fas fa-bed"></i><br>Qualità Sonno</button></div>
                <div class="col-sm-2 colonne-strette" id="divButtonDiagnosi" style="<? echo $cursorNuovoReclutamento ?>"><button class="btn btn-grey xcrud-action font-button-schede" style="width:100%;" <? echo $stileNuovoReclutamento ?> onClick="scegliScheda('DIAGNOSI');"><i style="" class="fas fa-bed"></i><br>Diagnosi</button></div>
            </div>
-->
            <h3 style="text-align:center;<? if ($messaggioPerUtente=='') echo 'display:none;' ?>"><? echo $messaggioPerUtente; ?></h3>

            

                <div class="panel-group" id="accordionDateControlli" role="tablist" aria-multiselectable="true" style="border:0px solid red; width:80%;padding: 2%;margin-left:10%;margin-bottom: -40px;<? if ($userId==-1) echo 'display:none;'?>">
                    <div class="panel panel-default">
                        <div class="panel-heading" role="tab" id="headingDateControlli" style="background-color: #428bca;color:#ffffff;">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordionDateControlli" href="#collapseDateControlli" aria-expanded="true" aria-controls="collapseDateControlli">
                                    Date Controlli 
                                </a>
                            </h4>
                        </div>
                        <div id="collapseDateControlli" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingDateControlli">
                            <div class="panel-body" style="margin-bottom: -55px;">
                                <?php // DATE CONTROLLI
                                    

                                    $xcruddate->table('QuadroClinico');
                                    $xcruddate->label('DataQuadroClinico','Data del controllo');
                                    $xcruddate->label('DataProssimoQuadroClinico','Data del prossimo controllo');
                                    if ($mostraDataControllo)
                                    {
                                        if ($IdQuadroClinico==NULL) $xcruddate->fields('DataQuadroClinico');
                                        else $xcruddate->fields('DataQuadroClinico,DataProssimoQuadroClinico');
                                    
                                    }
                                    else $xcruddate->fields('DataProssimoQuadroClinico');

                                    $xcruddate->hide_button( 'save_new, save_return, return' );
                                    $xcruddate->table_name('Date Controlli');

                                    $xcruddate->after_update('after_update_general_noTitle');
                                    $xcruddate->display_table_name(false);

                                    if ($IdQuadroClinico==NULL) // nuovo inserimento
                                    {
                                        $xcruddate->set_var("IdUtente", $idUtente ); 
                                        $xcruddate->set_var("IdPaziente", $userId );
                                        $xcruddate->set_var("type", $tipoScheda );
                                        $xcruddate->before_insert('before_insert_quadroClinico');
                                        $xcruddate->after_insert('after_insert_quadroClinico');
                                        $xcruddate->pass_default('DataQuadroClinico',date('Y-m-d'));
                                        echo $xcruddate->render('create');
                                    }
                                    else
                                    {
                                        $xcruddate->after_update('after_update_general_noTitle');
                                        echo $xcruddate->render('edit', $IdQuadroClinico);
                                    }
                                    

                                ?>
                            </div>
                        </div>
                    </div>
                </div>



            <div id="datiPaziente" style="border:0px solid red; width:80%;padding:2%;margin-left:10%;<? if ($showFirst!="anagrafica") echo 'display:none;'?>">
                
            <?php // ANAGRAFICA

                //if ($IdQuadroClinico!=NULL) 
                //{
                    //devo prendere i nominativi dei medici, per creare la lista di selezione nella ricerca avanzata del paziente
                    $elencoMedici = $utils_db->getElencoMedici($db);
                    $elencoPneumologi = $utils_db->getElencoPneumologi($db);
                    $elencoNefrologi = $utils_db->getElencoNefrologi($db);
                    $elencoNutrizionisti = $utils_db->getElencoNutrizionisti($db);
                    $elencoDiabetologi = $utils_db->getElencoDiabetologi($db);
                    
                    //array( ’values’ => array( ‘1’ => ‘Card Only’, ‘2’ => ‘Validated’, ‘3’ => ‘Blacklisted’, ‘4’ => ‘Left’, ‘5’ => ‘Inactive’ ) 

                    $xcrud->table('paziente');
                    $xcrud->change_type('IdMedicoCurante','select','',$elencoMedici);
                    $xcrud->change_type('idPneumologo','select','',$elencoPneumologi);
                    $xcrud->change_type('idNefrologo','select','',$elencoNefrologi);
                    $xcrud->change_type('idNutrizionista','select','',$elencoNutrizionisti);
                    $xcrud->change_type('idDiabetologo','select','',$elencoDiabetologi);
                    //$xcrud->change_type('utente.Nominativo','select','',$elencoMedici);
                    $xcrud->change_type('Sesso','select','M','M,F');
                    
                    $xcrud->label('CodiceFiscale','Codice Fiscale');
                    $xcrud->label('IdMedicoCurante','Medico Curante');
                    $xcrud->label('idPneumologo','Specialista Pneumologo');
                    $xcrud->label('idNefrologo','Specialista Nefrologo');
                    $xcrud->label('idNutrizionista','Nutrizionista');
                    $xcrud->label('idDiabetologo','Diabetologo');
                    $xcrud->label('DataRegistrazione','Data Reclutamento');
                    $xcrud->label('Telefoni','N° Telefonico');
                    //$xcrud->label('Eta','Età');
                    //$xcrud->label('Altezza','Altezza (cm)');
                    //$xcrud->label('Peso','Peso (kg)');
                    //$xcrud->label('CircAddominale','Circ. Addominale (cm)');
                    $xcrud->fields('DataRegistrazione,Nome,Cognome, Indirizzo, Telefoni, CodiceFiscale, Sesso, Altezza, data_nascita, IdMedicoCurante, idPneumologo, idNefrologo, idNutrizionista, idDiabetologo');
                    $xcrud->hide_button( 'save_new, save_return, return' );
                    $xcrud->buttons_position('right');
                    $xcrud->table_name('Anagrafica');
                    $xcrud->layout(5);

                    if ($userId==-1)
                    {
                        $xcrud->pass_default('DataRegistrazione',date('Y-m-d'));
                        $xcrud->after_insert('after_insert_paziente');
                        $xcrud->before_insert('before_insert_paziente');
                        $xcrud->set_var("IdUtente", $_SESSION['IdUtente'] ); 
                        echo $xcrud->render('create');
                    }
                    else
                    {
                        //$xcrud->join('IdMedicoCurante','utente','IdUtente');
                        //$xcrud->before_update('before_update_paziente');
                        $xcrud->after_update('after_update_paziente');
                        echo $xcrud->render('edit', $userId);
                    }
                //}

            ?>
            </div>

            <div id="anamnesi" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;display:none;">
                

            <?php

                if ($IdQuadroClinico!=NULL)
                {
                    $elencoTipologia = $utils_db->getTipologiaUtenti($db);
                    $opzioniFumo = $utils_db->getOpzioniFumo();
                    //$opzioniIpertensioneArt = $utils_db->getOpzioniIpertensioneArt();
                    $opzioniAnemia = $utils_db->getOpzioniAnemia();
                    //$opzioniCauseScompenso = $utils_db->getOpzioniCauseScompenso();
                    $opzioniCardiomiopatia = $utils_db->getOpzioniCardiomiopatia();
                    $opzioniCoronaropatia = $utils_db->getOpzioniCoronaropatia();
                    $opzioniDifettiValvolariCardiaci = $utils_db->getOpzioniDifettiValvolariCardiaci();
                    $opzioniDifettiRitmoCardiaco = $utils_db->getOpzioniDifettiRitmoCardiaco();
                    $opzioniAlcoolSostenteAlteranti = $utils_db->getOpzioniAlcoolSostenteAlteranti();
                    $opzioniPatologiePericardio = $utils_db->getOpzioniPatologiePericardio();
                    $opzioniScompensoCardiacoPrimario = $utils_db->getOpzioniScompensoCardiacoPrimario();

                    //devo trovare l'id della chiave del quadro relativo alla tabella
                    $sel = "SELECT IdFattoreRischio from fattorerischio_eziologia where IdPaziente=".$userId;
                    $db->query($sel);
                    $result = $db->result();
                    if (count($result)==0)
                    {
                        //non ha un scheda di fattore di rischio ed eziologia, possibile nuovo inserimento... lo inserisco io di default
                        $insertQuery = "INSERT INTO fattorerischio_eziologia (IdPaziente) VALUES (".$userId.")";
                        $db->query($insertQuery);
                        $IdFattoreRischio = $db->insert_id();  
                    }
                    else
                    {
                        $IdFattoreRischio = $result[0]["IdFattoreRischio"];   
                    }
                    //echo 'FattoreRischio: '.$IdFattoreRischio;
                    //echo '<pre>'; print_r($result); echo '</pre>';

                    $xcrudQuadroClinico->table('fattorerischio_eziologia');

                    $xcrudQuadroClinico->label('AnanmesiData','Data Compilazione/Controllo della Scheda');
                    $xcrudQuadroClinico->label('AnamnesiCompilazioneIdTipoUtente','Scheda aggiornata/controllata da');
                    $xcrudQuadroClinico->label('DiabeteTerapia','Attuale terapia per il diabete');
                    $xcrudQuadroClinico->label('IpertensioneArt','Ipertensione arteriosa');
                    $xcrudQuadroClinico->label('ANEMIAHB','Anemia');
                    $xcrudQuadroClinico->label('AnemiaGrado','Grado di anemia');
                    $xcrudQuadroClinico->label('InsufRenaleCronica','Insufficienza renale cronica');
                    $xcrudQuadroClinico->label('AltrePatologie','Altre patologie concomitanti');
                    $xcrudQuadroClinico->change_type('AnamnesiCompilazioneIdTipoUtente','select','',$elencoTipologia);
                    $xcrudQuadroClinico->change_type('Fumo','select','',$opzioniFumo);
                    $xcrudQuadroClinico->change_type('AnemiaGrado','select','',$opzioniAnemia);
                    $xcrudQuadroClinico->change_type('DiabeteTerapia','checkboxes','','Insulina, Ipoglicemizzanti, Nessuna');
                    $xcrudQuadroClinico->fields('AnanmesiData,AnamnesiCompilazioneIdTipoUtente, Fumo, Diabete1, Diabete2, DiabeteTerapia, IpertensioneArt, Dislipidemia, BPCO, ANEMIAHB, AnemiaGrado,InsufRenaleCronica, AltrePatologie', false, 'Anamnesi, Fattori di Rischio');

                    $xcrudQuadroClinico->label('Eziologia_cardiomiopatia','Cardiomiopatia');
                    $xcrudQuadroClinico->label('Eziologia_coronaropatia','Coronaropatia');
                    $xcrudQuadroClinico->label('Eziologia_difettiValvolariCardiaci','Difetti valvolari e difetti cardiaci congeniti');
                    $xcrudQuadroClinico->label('Eziologia_difettiRitmoCardiaco','Difetti del ritmo cardiaco');
                    $xcrudQuadroClinico->label('Eziologia_alcoolSostanzeAlteranti','Alcool e/o sostanze alteranti');
                    $xcrudQuadroClinico->label('Eziologia_patologiePericardio','Patologie a carico del pericardio');
                    $xcrudQuadroClinico->label('Eziologia_scompensoCardiacoPrimario','Scompenso cardiaco destro primario');
                    $xcrudQuadroClinico->label('Eziologia_altraEziologia','Altra eziologia');
                    $xcrudQuadroClinico->change_type('Eziologia_cardiomiopatia','checkboxes','',$opzioniCardiomiopatia);
                    $xcrudQuadroClinico->change_type('Eziologia_coronaropatia','checkboxes','',$opzioniCoronaropatia);
                    $xcrudQuadroClinico->change_type('Eziologia_difettiValvolariCardiaci','select','',$opzioniDifettiValvolariCardiaci);
                    $xcrudQuadroClinico->change_type('Eziologia_difettiRitmoCardiaco','checkboxes','',$opzioniDifettiRitmoCardiaco);
                    $xcrudQuadroClinico->change_type('Eziologia_alcoolSostanzeAlteranti','checkboxes','',$opzioniAlcoolSostenteAlteranti);
                    $xcrudQuadroClinico->change_type('Eziologia_patologiePericardio','checkboxes','',$opzioniPatologiePericardio);
                    $xcrudQuadroClinico->change_type('Eziologia_scompensoCardiacoPrimario','checkboxes','',$opzioniScompensoCardiacoPrimario);
                    $xcrudQuadroClinico->fields('Eziologia_cardiomiopatia,Eziologia_coronaropatia,Eziologia_difettiValvolariCardiaci,Eziologia_difettiRitmoCardiaco,Eziologia_alcoolSostanzeAlteranti,Eziologia_patologiePericardio,Eziologia_scompensoCardiacoPrimario,Eziologia_altraEziologia', false, 'Eziologia dello Scompenso');
                    $xcrudQuadroClinico->hide_button( 'save_new, save_return, return' );
                    $xcrudQuadroClinico->table_name('Anamnesi, Fattori rischio, eziologia scompenso');
                    $xcrudQuadroClinico->layout(6);

                    $xcrudQuadroClinico->after_update('after_update_anamnesi_rischio');
                    echo $xcrudQuadroClinico->render('edit', $IdFattoreRischio);
                }
            ?>
            </div>

            <div id="precedenti" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;display:none;">
                

            <?php

                if ($IdQuadroClinico!=NULL)
                {
                    //devo trovare l'id della chiave della tabella
                    $sel = "SELECT idScompensoPrecedenti from scompenso_precedenti where IdPaziente=".$userId;
                    $db->query($sel);
                    $result = $db->result();
                    if (count($result)==0)
                    {
                        //non ha un scheda di precedenti, possibile nuovo inserimento... lo inserisco io di default
                        $insertQuery = "INSERT INTO scompenso_precedenti (IdPaziente) VALUES (".$userId.")";
                        $db->query($insertQuery);
                        $IdScompensoPrecedenti = $db->insert_id();
                    }
                    else
                    {
                        $IdScompensoPrecedenti = $result[0]["idScompensoPrecedenti"];   
                    }
                    //echo 'TerapiaFarmacologica: '.$IdTerapiaFarmacologica;
                    //echo '<pre>'; print_r($result); echo '</pre>';

                    $xcrudPrecedenti->table('scompenso_precedenti');

                    $xcrudPrecedenti->label('accesso1Data','Data 1° Accesso P.S.');
                    $xcrudPrecedenti->label('accesso1Diagnosi','Diagnosi 1° Accesso P.S.');
                    $xcrudPrecedenti->label('accesso2Data','Data 2° Accesso P.S.');
                    $xcrudPrecedenti->label('accesso2Diagnosi','Diagnosi 2° Accesso P.S.');
                    $xcrudPrecedenti->label('accesso3Data','Data 3° Accesso P.S.');
                    $xcrudPrecedenti->label('accesso3Diagnosi','Diagnosi 3° Accesso P.S.');

                    $xcrudPrecedenti->label('ricovero1Data','Data Accesso 1° Ricovero');
                    $xcrudPrecedenti->label('ricovero1Dimissione','Data Dimissione 1° Ricovero');
                    $xcrudPrecedenti->label('ricovero1Diagnosi','Diagnosi 1° Ricovero');
                    $xcrudPrecedenti->label('ricovero2Data','Data Accesso 2° Ricovero');
                    $xcrudPrecedenti->label('ricovero2Dimissione','Data Dimissione 2° Ricovero');
                    $xcrudPrecedenti->label('ricovero2Diagnosi','Diagnosi 2° Ricovero');
                    $xcrudPrecedenti->label('ricovero3Data','Data Accesso 3° Ricovero');
                    $xcrudPrecedenti->label('ricovero3Dimissione','Data Dimissione 3° Ricovero');
                    $xcrudPrecedenti->label('ricovero3Diagnosi','Diagnosi 3° Ricovero');
                    $xcrudPrecedenti->fields('idPaziente',true);

                    $xcrudPrecedenti->hide_button( 'save_new, save_return, return' );
                    $xcrudPrecedenti->table_name('Precedenti per Scompenso');
                    $xcrudPrecedenti->layout(5);

                    $xcrudPrecedenti->after_update('after_update_general');
                    echo $xcrudPrecedenti->render('edit', $IdScompensoPrecedenti);
                }

            ?>
            </div>

            <div id="terapiaFarmacologica" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;display:none;">
                

            <?php

                if ($IdQuadroClinico!=NULL)
                {
                    //devo trovare l'id della chiave del quadro relativo alla tabella
                    $sel = "SELECT IdTerapiaFarmacologica from terapia_farmacologica where IdQuadroClinico=".$IdQuadroClinico;
                    $db->query($sel);
                    $result = $db->result();
                    if (count($result)==0)
                    {
                        //non ha un scheda di terapia farmacologica, possibile nuovo inserimento... lo inserisco io di default
                        $insertQuery = "INSERT INTO terapia_farmacologica (IdQuadroClinico) VALUES (".$IdQuadroClinico.")";
                        $db->query($insertQuery);
                        $IdTerapiaFarmacologica = $db->insert_id();
                    }
                    else
                    {
                        $IdTerapiaFarmacologica = $result[0]["IdTerapiaFarmacologica"];   
                    }
                    //echo 'TerapiaFarmacologica: '.$IdTerapiaFarmacologica;
                    //echo '<pre>'; print_r($result); echo '</pre>';

                    //terapia al reclutamento, ma sono se non sto mostrando il reclutamento.....
                    if ($IdQuadroClinicoReclutamento!=$IdQuadroClinico)
                    {
                        $sel = "SELECT *, DATE_FORMAT(terapiaData, '%d-%m-%Y') as DataFormat  from terapia_farmacologica where IdQuadroClinico=".$IdQuadroClinicoReclutamento;
                        $db->query($sel);
                        $result = $db->result();
                        $tabellaTerapia = $utils_db->componiTabellaTerapia($result[0]);
                        //echo '<pre>'; print_r($result); echo '</pre>';
                        //echo $tabellaTerapia;

                        ?>
                        <div class="panel-group" id="accordionTerapiaReclutamento" role="tablist" aria-multiselectable="true">
                          <div class="panel panel-default">
                            <div class="panel-heading" role="tab" id="headingOne" style="background-color: #428bca;color:#ffffff;">
                              <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordionTerapiaReclutamento" href="#collapseTerapiaReclutamento" aria-expanded="true" aria-controls="collapseOne">
                                  Terapia al Reclutamento - <? echo $result[0]["DataFormat"]; ?> 
                                </a>
                              </h4>
                            </div>
                            <div id="collapseTerapiaReclutamento" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                              <div class="panel-body">
                                    <? echo $tabellaTerapia; ?> 
                              </div>
                            </div>
                          </div>
                        </div>
                        <?php

                        //ultima terapia del cardiologo, id quadro clinico
                        $sel = "Select idQuadroClinico from QuadroClinico,Utente where idPaziente=".$userId." and Utente.idUtente = QuadroClinico.idUtente and Utente.IdTipoUtente=4 order by idQuadroClinico DESC LIMIT 1";
                        $db->query($sel);
                        $result = $db->result();
                        if (count($result)>0)
                        {
                            $sel = "SELECT *, DATE_FORMAT(terapiaData, '%d-%m-%Y') as DataFormat  from terapia_farmacologica where IdQuadroClinico=".$result[0]["idQuadroClinico"];
                            $db->query($sel);
                            $result = $db->result();
                            $tabellaTerapia = $utils_db->componiTabellaTerapia($result[0]);

                            ?>
                            <div class="panel-group" style="margin-top:20px;" id="accordionTerapiaUltima" role="tablist" aria-multiselectable="true">
                              <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="heading2" style="background-color: #428bca;color:#ffffff;">
                                  <h4 class="panel-title">
                                    <a role="button" data-toggle="collapse" data-parent="#accordionTerapiaUltima" href="#collapseTerapiaUltima" aria-expanded="true" aria-controls="collapseOne">
                                      Ultima terapia del cardiologo - <? echo $result[0]["DataFormat"]; ?> 
                                    </a>
                                  </h4>
                                </div>
                                <div id="collapseTerapiaUltima" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading2">
                                  <div class="panel-body">
                                        <? echo $tabellaTerapia; ?> 
                                  </div>
                                </div>
                              </div>
                            </div>
                            <?php
                        }
                    }

                    $xcrudTerapia->table('terapia_farmacologica');

                    $xcrudTerapia->label('terapiaData','Data Compilazione/Controllo della Scheda');
                    $xcrudTerapia->label('terapiaCompilazioneIdTipoUtente','Scheda aggiornata/controllata da');
                    $xcrudTerapia->label('AmiodaronePosologia','Amiodarone: posologia giornaliera (mg)');
                    $xcrudTerapia->label('DigitalePosologia','Digitale: posologia giornaliera (mg)');
                    $xcrudTerapia->label('DronedaronePosologia','Dronedarone: posologia giornaliera (mg)');
                    $xcrudTerapia->label('FlecainidePosologia','Flecainide: posologia giornaliera (mg)');
                    $xcrudTerapia->label('IvabradinaPosologia','Ivabradina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('PropafenonePosologia','Propafenone: posologia giornaliera (mg)');
                    $xcrudTerapia->change_type('terapiaCompilazioneIdTipoUtente','select','',$elencoTipologia);
                    $xcrudTerapia->fields('terapiaData,terapiaCompilazioneIdTipoUtente,Amiodarone,Digitale,Dronedarone,Flecainide,Ivabradina,Propafenone,AmiodaronePosologia,DigitalePosologia,DronedaronePosologia,FlecainidePosologia,IvabradinaPosologia,PropafenonePosologia', false, 'Generale, Antiaritmici');

                    $xcrudTerapia->label('AcidoActeilsalicilico','Acido Acteilsalicilico');
                    $xcrudTerapia->label('AcidoActeilsalicilicoPosologia','Acido Acteilsalicilico: posologia giornaliera (mg)');
                    $xcrudTerapia->label('ClopidogrelPosologia','Clopidogrel: posologia giornaliera (mg)');
                    //$xcrudTerapia->label('TiclopidinaPosologia','Ticlopidina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('PrasugrelPosologia','Prasugrel: posologia giornaliera (mg)');
                    //$xcrudTerapia->fields('AcidoActeilsalicilico,Clopidogrel,Ticlopidina,Prasugrel,AcidoActeilsalicilicoPosologia,ClopidogrelPosologia,TiclopidinaPosologia,PrasugrelPosologia', false, 'Antiaggreganti');
                    $xcrudTerapia->fields('AcidoActeilsalicilico,Clopidogrel,Prasugrel,AcidoActeilsalicilicoPosologia,ClopidogrelPosologia,PrasugrelPosologia', false, 'Antiaggreganti');

                    $xcrudTerapia->label('EnalaprilLercanidipina','Enalapril-Lercanidipina');
                    $xcrudTerapia->label('PerindIndapAmlo','Perind.-Indap.-Amlo.');
                    $xcrudTerapia->label('RamiprilAmlodipina','Ramipril-Amlodipina');
                    $xcrudTerapia->label('AmlodipinaPosologia','Amlodipina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('CandesartanPosologia','Candesartan: posologia giornaliera (mg)');
                    $xcrudTerapia->label('IrbesartanPosologia','Irbesartan: posologia giornaliera (mg)');
                    $xcrudTerapia->label('LisinoprilPosologia','Lisinopril: posologia giornaliera (mg)');
                    $xcrudTerapia->label('LosartanPosologia','Losartan: posologia giornaliera (mg)');
                    $xcrudTerapia->label('NitratoPosologia','Nitrato: posologia giornaliera (mg)');
                    $xcrudTerapia->label('NitroglicerinaPosologia','Nitroglicerina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('TorasemidePosologia','Torasemide: posologia giornaliera (mg)');
                    $xcrudTerapia->label('ValsartanPosologia','Valsartan: posologia giornaliera (mg)');
                    $xcrudTerapia->label('RanazolinaPosologia','Ranazolina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('DoxazosinaPosologia','Doxazosina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('PerindoprilPosologia','Perindopril: posologia giornaliera (mg)');
                    $xcrudTerapia->label('IndapamidePosologia','Indapamide: posologia giornaliera (mg)');
                    $xcrudTerapia->label('LercanidipinaPosologia','Lercanidipina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('EnalaprilLercanidipinaPosologia','Enalapril-Lercanidipina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('PerindIndapAmloPosologia','Perind.-Indap.-Amlo.: posologia giornaliera (mg)');
                    $xcrudTerapia->label('RamiprilAmlodipinaPosologia','Ramipril-Amlodipina: posologia giornaliera (mg)');
                    $xcrudTerapia->fields('Amlodipina,Candesartan,Irbesartan,Lisinopril,Losartan,Nitrato,Nitroglicerina,Torasemide,Valsartan,Ranazolina,Doxazosina,Perindopril,Indapamide,Lercanidipina,EnalaprilLercanidipina,PerindIndapAmlo,RamiprilAmlodipina,AmlodipinaPosologia,CandesartanPosologia,IrbesartanPosologia,LisinoprilPosologia,LosartanPosologia,NitratoPosologia,NitroglicerinaPosologia,TorasemidePosologia,ValsartanPosologia,RanazolinaPosologia,DoxazosinaPosologia,PerindoprilPosologia,IndapamidePosologia,LercanidipinaPosologia,EnalaprilLercanidipinaPosologia,PerindIndapAmloPosologia,RamiprilAmlodipinaPosologia', false, 'Antipertensivi/Vasodilatatori');

                    $xcrudTerapia->label('BisoprololoPosologia','Bisoprololo: posologia giornaliera (mg)');
                    $xcrudTerapia->label('CarvediloloPosologia','Carvedilolo: posologia giornaliera (mg)');
                    $xcrudTerapia->label('MetoprololoPosologia','Metoprololo: posologia giornaliera (mg)');
                    $xcrudTerapia->label('NebivololoPosologia','Nebivololo: posologia giornaliera (mg)');
                    $xcrudTerapia->label('FurosemideSpironolattone','Furosemide-Spironolattone');
                    $xcrudTerapia->fields('Bisoprololo,Carvedilolo,Metoprololo,Nebivololo,BisoprololoPosologia,CarvediloloPosologia,MetoprololoPosologia,NebivololoPosologia', false, 'Beta-Bloccanti');

                    $xcrudTerapia->label('AtorvastatinaPosologia','Atorvastatina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('EzetimibePosologia','Ezetimibe: posologia giornaliera (mg)');
                    $xcrudTerapia->label('OmegapolienoiciPosologia','Omega polienoici: posologia giornaliera (mg)');
                    $xcrudTerapia->label('PravastatinaPosologia','Pravastatina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('RosuvastatinaPosologia','Rosuvastatina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('SinvastatinaPosologia','Sinvastatina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('Omegapolienoici','Omega polienoici');
                    $xcrudTerapia->fields('Atorvastatina,Ezetimibe,Omegapolienoici,Pravastatina,Rosuvastatina,Sinvastatina,AtorvastatinaPosologia,EzetimibePosologia,OmegapolienoiciPosologia,PravastatinaPosologia,RosuvastatinaPosologia,SinvastatinaPosologia', false, 'Ipocolesterolemizzanti/statine');

                    $xcrudTerapia->label('EnalaprilPosologia','Enalapril: posologia giornaliera (mg)');
                    $xcrudTerapia->label('RamiprilPosologia','Ramipril: posologia giornaliera (mg)');
                    $xcrudTerapia->label('ZofenoprilPosologia','Zofenopril: posologia giornaliera (mg)');
                    $xcrudTerapia->fields('Enalapril,Ramipril,Zofenopril,EnalaprilPosologia,RamiprilPosologia,ZofenoprilPosologia', false, 'ACE-inibitori');

                    $xcrudTerapia->label('FurosemidePosologia','Furosemide: posologia giornaliera (mg)');
                    $xcrudTerapia->label('FurosemideSpironolattonePosologia','Furosemide-Spironolattone: posologia giornaliera (mg)');
                    $xcrudTerapia->label('CanrenoatoPotassioPosologia','Canrenoato di Potassio: posologia giornaliera (mg)');
                    $xcrudTerapia->label('SpironolattonePosologia','Spironolattone: posologia giornaliera (mg)');
                    $xcrudTerapia->label('FurosemideSpironolattone','Furosemide-Spironolattone');
                    $xcrudTerapia->label('CanrenoatoPotassio','Canrenoato di Potassio');
                    $xcrudTerapia->label('KanrenonePosologia','Kanrenone: posologia giornaliera (mg)');                    
                    $xcrudTerapia->fields('Furosemide,FurosemideSpironolattone,CanrenoatoPotassio,Spironolattone,Kanrenone,FurosemidePosologia,FurosemideSpironolattonePosologia,CanrenoatoPotassioPosologia,SpironolattonePosologia,KanrenonePosologia', false, 'Diuretici');

                    $xcrudTerapia->label('DabigatranPosologia','Dabigatran: posologia giornaliera (mg)');
                    $xcrudTerapia->label('RivaroxabanPosologia','Rivaroxaban: posologia giornaliera (mg)');
                    $xcrudTerapia->label('ApixabanPosologia','Apixaban: posologia giornaliera (mg)');
                    $xcrudTerapia->label('CoumadinPosologia','Coumadin: posologia giornaliera (mg)');
                    $xcrudTerapia->label('SintromPosologia','Sintrom: posologia giornaliera (mg)');
                    $xcrudTerapia->label('EdoxabanPosologia','Edoxaban: posologia giornaliera (mg)');
                    $xcrudTerapia->label('TicagreolPosologia','Ticagreol: posologia giornaliera (mg)');
                    $xcrudTerapia->fields('Dabigatran,Rivaroxaban,Apixaban,Coumadin,Sintrom,Edoxaban,Ticagreol,DabigatranPosologia,RivaroxabanPosologia,ApixabanPosologia,CoumadinPosologia,SintromPosologia,EdoxabanPosologia,TicagreolPosologia', false, 'AntiCoagulanti');

                    $xcrudTerapia->label('Eritropietina','Eritropoietina');
                    $xcrudTerapia->label('Isulina','Insulina');
                    $xcrudTerapia->label('Altrifarmaci','Altro/i farmaco/i');
                    $xcrudTerapia->label('AltrifarmaciPrincipiAttivi','Principi Attivi altro/i farmaco/i');
                    $xcrudTerapia->label('SacubitrilValsartan','Sacubitril-Valsartan');
                    $xcrudTerapia->label('Febuxostat80','Febuxostat 80');
                    $xcrudTerapia->label('SacubitrilValsartanPosologia','Sacubitril-Valsartan: posologia giornaliera (mg)');
                    $xcrudTerapia->label('ColecalciferoloPosologia','Colecalciferolo: posologia giornaliera (mg)');
                    $xcrudTerapia->label('AllopurinoloPosologia','Allopurinolo: posologia giornaliera (mg)');
                    $xcrudTerapia->label('Febuxostat80Posologia','Febuxostat80: posologia giornaliera (mg)');
                    $xcrudTerapia->fields('Anticoagulanti,Antidepressivi,Eritropietina,Isulina,Ipoglicemizzanti,Altrifarmaci,AltrifarmaciPrincipiAttivi,SacubitrilValsartan,Colecalciferolo,Allopurinolo,Febuxostat80,SacubitrilValsartanPosologia,ColecalciferoloPosologia,AllopurinoloPosologia,Febuxostat80Posologia', false, 'Terapia aggiuntiva');


                    $xcrudTerapia->label('glibenclamide','Glibenclamide');
                    $xcrudTerapia->label('glibenclamidePosologia','Glibenclamide: posologia giornaliera (mg)');
                    $xcrudTerapia->label('glibenclamideMetformina','Glibenclamide + Metformina');
                    $xcrudTerapia->label('glibenclamideMetforminaPosologia','Glibenclamide + Metformina: posologia giornaliera (mg)');
                    $xcrudTerapia->label('glicazide','Glicazide');
                    $xcrudTerapia->label('glicazidePosologia','Glicazide: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('glimepiride','Glimepiride');
                    $xcrudTerapia->label('glimepiridePosologia','Glimepiride: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('glipizide','Glipizide');
                    $xcrudTerapia->label('glipizidePosologia','Glipizide: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('gliquinone','Gliquinone');
                    $xcrudTerapia->label('gliquinonePosologia','Gliquinone: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('repaglinide','Repaglinide');
                    $xcrudTerapia->label('repaglinidePosologia','Repaglinide: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('metformina','Metformina');
                    $xcrudTerapia->label('metforminaPosologia','Metformina: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('acarbose','Acarbose');
                    $xcrudTerapia->label('acarbosePosologia','Acarbose: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('pioglitazone','Pioglitazone');
                    $xcrudTerapia->label('pioglitazonePosologia','Pioglitazone: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('exenatideBid','Exenatide Bid');
                    $xcrudTerapia->label('exenatideBidPosologia','Exenatide Bid: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('exenatideQw','Exenatide Qw');
                    $xcrudTerapia->label('exenatideQwPosologia','Exenatide Qw: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('liraglutide','Liraglutide');
                    $xcrudTerapia->label('liraglutidePosologia','Liraglutide: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('lixisenatide','Lixisenatide');
                    $xcrudTerapia->label('lixisenatidePosologia','Lixisenatide: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('dulaglutide','Dulaglutide');
                    $xcrudTerapia->label('dulaglutidePosologia','Dulaglutide: posologia giornaliera (mg)');                    
                    $xcrudTerapia->fields('glibenclamide,glibenclamideMetformina,glicazide,glimepiride,glipizide,gliquinone,repaglinide,metformina,acarbose,pioglitazone,exenatideBid,exenatideQw,liraglutide,lixisenatide,dulaglutide,glibenclamidePosologia,glibenclamideMetforminaPosologia,glicazidePosologia,glimepiridePosologia,glipizidePosologia,gliquinonePosologia,repaglinidePosologia,metforminaPosologia,acarbosePosologia,pioglitazonePosologia,exenatideBidPosologia,exenatideQwPosologia,liraglutidePosologia,lixisenatidePosologia,dulaglutidePosologia', false, 'Ipoglicemizzanti - 1');

                    $xcrudTerapia->label('sitagliptin','Sitagliptin');
                    $xcrudTerapia->label('sitagliptinPosologia','Sitagliptin: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('sitagliptinMet','Sitagliptin + Met');
                    $xcrudTerapia->label('sitagliptinMetPosologia','Sitagliptin + Met: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('saxagliptin','Saxagliptin');
                    $xcrudTerapia->label('saxagliptinPosologia','Saxagliptin: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('saxagliptinMet','Saxagliptin + Met');
                    $xcrudTerapia->label('saxagliptinMetPosologia','Saxagliptin + Met: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('linagliptin','Linagliptin');
                    $xcrudTerapia->label('linagliptinPosologia','Linagliptin: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('linagliptinMet','Linagliptin + Met');
                    $xcrudTerapia->label('linagliptinMetPosologia','Linagliptin + Met: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('vildagliptin','Vildagliptin');
                    $xcrudTerapia->label('vildagliptinPosologia','Vildagliptin: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('vildagliptinMet','Vildagliptin + Met');
                    $xcrudTerapia->label('vildagliptinMetPosologia','Vildagliptin + Met: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('alogliptin','Alogliptin');
                    $xcrudTerapia->label('alogliptinPosologia','Alogliptin: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('alogliptinPioglitazone','Alogliptin + Pioglitazone');
                    $xcrudTerapia->label('alogliptinPioglitazonePosologia','Alogliptin + Pioglitazone: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('alogliptinMet','Alogliptin + Met');
                    $xcrudTerapia->label('alogliptinMetPosologia','Alogliptin + Met: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('dapagliflozin','Dapagliflozin');
                    $xcrudTerapia->label('dapagliflozinPosologia','Dapagliflozin: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('dapagliflozinMet','Dapagliflozin + Met');
                    $xcrudTerapia->label('dapagliflozinMetPosologia','Dapagliflozin + Met: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('empagliflozin','Empagliflozin');
                    $xcrudTerapia->label('empagliflozinPosologia','Empagliflozin: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('empagliflozinMet','Empagliflozin + Met');
                    $xcrudTerapia->label('empagliflozinMetPosologia','Empagliflozin + Met: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('canagliflozin','Canagliflozin');
                    $xcrudTerapia->label('canagliflozinPosologia','Canagliflozin: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('canagliflozinMet','Canagliflozin + Met');
                    $xcrudTerapia->label('canagliflozinMetPosologia','Canagliflozin + Met: posologia giornaliera (mg)');                                    
                    $xcrudTerapia->fields('sitagliptin,sitagliptinMet,saxagliptin,saxagliptinMet,linagliptin,linagliptinMet,vildagliptin,vildagliptinMet,alogliptin,alogliptinPioglitazone,alogliptinMet,dapagliflozin,dapagliflozinMet,empagliflozin,empagliflozinMet,canagliflozin,canagliflozinMet,sitagliptinPosologia,sitagliptinMetPosologia,saxagliptinPosologia,saxagliptinMetPosologia,linagliptinPosologia,linagliptinMetPosologia,vildagliptinPosologia,vildagliptinMetPosologia,alogliptinPosologia,alogliptinPioglitazonePosologia,alogliptinMetPosologia,dapagliflozinPosologia,dapagliflozinMetPosologia,empagliflozinPosologia,empagliflozinMetPosologia,canagliflozinPosologia,canagliflozinMetPosologia', false, 'Ipoglicemizzanti - 2');

                    $xcrudTerapia->label('humalogU100','Humalog U100');
                    $xcrudTerapia->label('humalogU100Posologia','Humalog U100: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('humalogU200','Humalog U200');
                    $xcrudTerapia->label('humalogU200Posologia','Humalog U200: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('apidra','Apidra');
                    $xcrudTerapia->label('apidraPosologia','Apidra: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('novorapid','Novorapid');
                    $xcrudTerapia->label('novorapidPosologia','Novorapid: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('fiasp','Fiasp');
                    $xcrudTerapia->label('fiaspPosologia','Fiasp: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('glargineU100','Glargine U100');
                    $xcrudTerapia->label('glargineU100Posologia','Glargine U100: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('glargineU300','Glargine U300');
                    $xcrudTerapia->label('glargineU300Posologia','Glargine U300: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('glargineLixisenatide','Glargine + Lixisenatide');
                    $xcrudTerapia->label('glargineLixisenatidePosologia','Glargine + Lixisenatide: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('deglutec','Deglutec');
                    $xcrudTerapia->label('deglutecPosologia','Deglutec: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('deglutecLiraglutide','Deglutec + Liraglutide');
                    $xcrudTerapia->label('deglutecLiraglutidePosologia','Deglutec + Liraglutide: posologia giornaliera (mg)');                    
                    $xcrudTerapia->label('detemir','Detemir');
                    $xcrudTerapia->label('detemirPosologia','Detemir: posologia giornaliera (mg)');                    
                    $xcrudTerapia->fields('humalogU100,humalogU200,apidra,novorapid,fiasp,glargineU100,glargineU300,glargineLixisenatide,deglutec,deglutecLiraglutide,detemir,humalogU100Posologia,humalogU200Posologia,apidraPosologia,novorapidPosologia,fiaspPosologia,glargineU100Posologia,glargineU300Posologia,glargineLixisenatidePosologia,deglutecPosologia,deglutecLiraglutidePosologia,detemirPosologia', false, 'Insuline');
 


                    $xcrudTerapia->hide_button( 'save_new, save_return, return' );
                    $xcrudTerapia->table_name('Terapia Farmacologica');
                    $xcrudTerapia->layout(7);

 
                    if ($IdTerapiaFarmacologica==NULL) //sta inserendo una nuova scheda, per quell'ID quadro clinico (reclutamento oppure controllo)
                    {
                        $xcrudTerapia->pass_default('terapiaData',date('Y-m-d'));
                        $xcrudTerapia->after_insert('after_insert_terapia');
                        $xcrudTerapia->before_insert('before_insert_terapia');
                        $xcrudTerapia->set_var("IdQuadroClinico", $IdQuadroClinico ); 
                        $xcrudTerapia->set_var("IdPaziente", $userId ); 
                        echo $xcrudTerapia->render('create');
                    }
                    else
                    {
                        $xcrudTerapia->after_update('after_update_terapia');
                        echo $xcrudTerapia->render('edit', $IdTerapiaFarmacologica);
                    }
                }
            ?>
            </div>
            <div id="esamiEmatochimici" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;display:none;">
                
            <?php
                
                if ($IdQuadroClinico!=NULL)
                {
                    $opzioniClassificazioneAlbuminuriaCreatinuria = $utils_db->getClassificazioneAlbuminuriaCreatinuria();

                    $xcrudEmatochimici->table('QuadroClinico');

                    $xcrudEmatochimici->label('SchedaEsamiEmatochimiciData','Data di aggiornamento/controllo della scheda');
                    $xcrudEmatochimici->label('SchedaEsamiEmatochimiciCompilazioneIdTipoUtente','Scheda aggiornata/controllata da');
                    $xcrudEmatochimici->label('prelievoEmatochimicoData','Data di esecuzione dell\'esame ematochimico');
                    $xcrudEmatochimici->label('prelievoUrineData','Data di esecuzione dell\'esame delle urine');
                    
                    $xcrudEmatochimici->label('IngrBNP','NT-proBNP (pg/ml)');
                    $xcrudEmatochimici->label('IngrTroponina','Troponina (ug/L)');
                    $xcrudEmatochimici->label('IngrMioglobina','Mioglobina (ug/ml)');
                    $xcrudEmatochimici->label('lAldosterone','Aldosterone (pg/ml)');
                    $xcrudEmatochimici->label('lEpinefrina','Renina (mUl/L)');
                    $xcrudEmatochimici->label('IngrPCR','Proteina C-Reattiva (PCR) (mg/dl)');
                    $xcrudEmatochimici->label('IngrColTot','Colesterolo totale (mg/dl)');
                    $xcrudEmatochimici->label('IngrHDL','Colesterolo HDL (mg/dl)');
                    $xcrudEmatochimici->label('IngrLDL','Colesterolo LDL (mg/dl)');
                    $xcrudEmatochimici->label('IngrTrigl','Trigliceridi (mg/dl)');
                    $xcrudEmatochimici->label('IngrAzot','Azotemia (mg/dl)');
                    $xcrudEmatochimici->label('IngrCreat','Creatinina sierica (mg/dl)');
                    $xcrudEmatochimici->label('IngrCistacina','Cistacina C sierica (mg/L)');
                    $xcrudEmatochimici->label('IngrUricemia','Uricemia (mg/dl)');
                    $xcrudEmatochimici->label('IngrEmoglob','Emoglobina (g/dl)');
                    $xcrudEmatochimici->label('IngrGR','Eritrociti (g/dl)');
                    $xcrudEmatochimici->label('IngrMCH','Emoglobina Corpuscolare Media (pg)');
                    $xcrudEmatochimici->label('IngrMCV','Volume Corpuscolare Medio (fl)');
                    $xcrudEmatochimici->label('lPiastrine','Piastrine (x 109/L)');
                    $xcrudEmatochimici->label('IngrGB','Leucociti (x 109/L)');
                    $xcrudEmatochimici->label('IngrCA','Calcio (mg/dl)');
                    $xcrudEmatochimici->label('IngrMG','Magnesio (mg/dl)');
                    $xcrudEmatochimici->label('IngrK','Potassio (mmol/L)');
                    $xcrudEmatochimici->label('IngrNA','Sodio (mmol/L)');
                    $xcrudEmatochimici->label('IngrEmoglGlic','Emoglobina Glicata (mmol/mol)');
                    $xcrudEmatochimici->label('IngrGlicemia','Glucosio (mmol/mol)');
                    $xcrudEmatochimici->label('IngrClearanceCreatinina','Clearance Creatinina (ml/min)');
                    //$xcrudEmatochimici->label('IngrVelFiltrGlomCalc','Velocità di filtrazione glomerulare calcolata mediante clearance della creatinina (ml/min)');
                    //$xcrudEmatochimici->label('IngrVelFiltrGlomStimMDRD4','Velocità di filtrazione glomerulare stimata (Formula MDRD-4) (ml/min/1.73m2)');
                    //$xcrudEmatochimici->label('IngrVelFiltrGlomStimCKDEPI','Velocità di filtrazione glomerulare stimata (Formula CKD-EPI) (ml/min/1.73m2)');                    
                    //$xcrudEmatochimici->label('IngrClassifAlbuminuriaCreatinuria','Classificazione della albuminuria in base alla creatinuria');                    
                    $xcrudEmatochimici->label('IngrGOT','GOT (u/L)');
                    $xcrudEmatochimici->label('IngrGPT','GPT (u/L)');
                    $xcrudEmatochimici->label('IngrCPK','CPK MB (ng/ml)');
                    $xcrudEmatochimici->label('lferritina','Ferritina (ng/ml)');
                    $xcrudEmatochimici->label('IngrT3','FT3 (pg/ml)');
                    $xcrudEmatochimici->label('IngrT4','FT4 (ng/dl)');
                    $xcrudEmatochimici->label('IngrTSH','TSH (nu/ml)');
                    $xcrudEmatochimici->label('gATH','PTH (pg/ml)');
                    $xcrudEmatochimici->label('lVitD','Vitamina D (ng/dl)');
                    $xcrudEmatochimici->label('IngrTransfInsatura','Transferrina Satura (%)');

                    $xcrudEmatochimici->label('IngrMicroAlbumin','Proteinuria (mcg/mg)');
                    $xcrudEmatochimici->label('IngrGlicosuria','Glicosuria');

                    $xcrudEmatochimici->change_type('SchedaEsamiEmatochimiciCompilazioneIdTipoUtente','select','',$elencoTipologia);
                    //$xcrudEmatochimici->change_type('IngrClassifAlbuminuriaCreatinuria','select','',$opzioniClassificazioneAlbuminuriaCreatinuria);

                    //$xcrudEmatochimici->fields('SchedaEsamiEmatochimiciData,SchedaEsamiEmatochimiciCompilazioneIdTipoUtente,prelievoEmatochimicoData,prelievoUrineData,IngrBNP,IngrTroponina,IngrMioglobina,lAldosterone,lEpinefrina,IngrPCR,IngrColTot,IngrHDL,IngrLDL,IngrTrigl,IngrAzot,IngrCreat,IngrCistacina,IngrUricemia,IngrEmoglob,IngrGR,IngrMCH,lPiastrine,IngrGB,IngrCA,IngrMG,IngrK,IngrNA,IngrEmoglGlic,IngrGlicemia,IngrClearanceCreatinina,IngrVelFiltrGlomCalc,IngrVelFiltrGlomStimMDRD4,IngrVelFiltrGlomStimCKDEPI,IngrClassifAlbuminuriaCreatinuria,IngrGOT,IngrGPT,IngrCPK,lferritina,IngrT3,IngrT4,IngrTSH,gATH,lVitD,IngrTransfInsatura,IngrMicroAlbumin,IngrGlicosuria');
                    $xcrudEmatochimici->fields('SchedaEsamiEmatochimiciData,SchedaEsamiEmatochimiciCompilazioneIdTipoUtente,prelievoEmatochimicoData,prelievoUrineData,IngrBNP,IngrTroponina,IngrMioglobina,lAldosterone,lEpinefrina,IngrPCR,IngrColTot,IngrHDL,IngrLDL,IngrTrigl,IngrAzot,IngrCreat,IngrCistacina,IngrUricemia,IngrEmoglob,IngrGR,IngrMCH,lPiastrine,IngrGB,IngrCA,IngrMG,IngrK,IngrNA,IngrEmoglGlic,IngrGlicemia,IngrClearanceCreatinina,IngrGOT,IngrGPT,IngrCPK,lferritina,IngrT3,IngrT4,IngrTSH,gATH,lVitD,IngrTransfInsatura,IngrMicroAlbumin,IngrGlicosuria');

                    $xcrudEmatochimici->hide_button( 'save_new, save_return, return' );
                    $xcrudEmatochimici->table_name('Esami Ematochimici');

                    $xcrudEmatochimici->after_update('after_update_general');
                    echo $xcrudEmatochimici->render('edit', $IdQuadroClinico);
                }
            ?>
            </div>
            <div id="aritmie" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;<? if ($showFirst!="aritmie") echo 'display:none;'?>">
                
            <?php
                
                if ($IdQuadroClinico!=NULL)
                {
                    $opzioniInvertite = $utils_db->getOpzioniInvertite();

                    $xcrudAritmie->table('QuadroClinico');
                    
                    $xcrudAritmie->label('ExtraSopraVentrECG','ExtraSopraVentr. ECG');
                    $xcrudAritmie->label('ExtraSopraVentrECGHolter','ExtraSopraVentr. ECG Holter');
                    $xcrudAritmie->label('ExVentricolECG','ExtraVentric. ECG');
                    $xcrudAritmie->label('ExVentricolECGHolter','ExtraVentric. ECG Holter');
                    $xcrudAritmie->label('ExVentricolSempliciN','ExtraVentric. Semplici N°');
                    $xcrudAritmie->label('ExVentricolCoppieTripleN','ExtraVentric. Coppie e Triple N°');
                    $xcrudAritmie->label('ExVentricolTVNonSostenutaN','ExtraVentric. TV non Sostenuta N°');
                    $xcrudAritmie->label('TPSV','TPSV');
                    $xcrudAritmie->label('TPSVAblazione','TPSV Ablazione');
                    $xcrudAritmie->label('PreeccitazioneTac','Preeccitazione Tac');
                    $xcrudAritmie->label('PreeccitazioneTacAblazione','Preeccitazione Tac Ablazione');
                    $xcrudAritmie->label('FAParadossDataInizio','FA Paross. o Persistente - Data Inizio');
                    $xcrudAritmie->label('FAParadossNEpisodi','FA Paross. o Persistente - N° Episodi');
                    $xcrudAritmie->label('FAParadossDurataEpisodi','FA Paross. o Persistente - Durata Episodi');
                    $xcrudAritmie->label('FAParadossAblazTrans','FA Paross. o Persistente - Ablazione Transcat.');
                    $xcrudAritmie->label('FAPermanenteDataInizio','FA Permanente - Data Inizio');
                    $xcrudAritmie->label('CVE','CVE');
                    $xcrudAritmie->label('gDataCVE','Data Ultima CVE');
                    $xcrudAritmie->label('TVSostenutaSintomatica','TV Sostenuta');
                    $xcrudAritmie->label('TVSostenutaSintomaticaData','TV Sostenuta - Data prima osservazione');
                    $xcrudAritmie->label('TVSostenutaSintomaticaEpisodiN','TV Sostenuta - N° Episodi');
                    $xcrudAritmie->label('TVSostenutaSintomaticaDurata','TV Sostenuta - Durata Max episodi');
                    $xcrudAritmie->label('TVSostenutaSintomaticaFreqVentr','TV Sostenuta - Freq. Ventricolare');
                    $xcrudAritmie->label('MIResuscitata','MI Resuscitata');
                    $xcrudAritmie->label('PM','PM');
                    $xcrudAritmie->label('PMCausaleImpianto','PM - Causale Impianto');
                    $xcrudAritmie->label('PMDataImpianto','PM - Data Impianto');
                    $xcrudAritmie->label('PMTipologia','PM - Tipologia');
                    $xcrudAritmie->label('ICD','ICD');
                    $xcrudAritmie->label('ICDCausaleImpianto','ICD - Causale Impianto');
                    $xcrudAritmie->label('ICDDataImpianto','ICD - Data Impianto');
                    $xcrudAritmie->label('ICDScaricheAppropriateN','ICD - N° Scariche Appropriate');
                    $xcrudAritmie->label('ICDScaricheInappropriate','ICD - N° Scariche Inappropriate');
                    $xcrudAritmie->label('PMBiventricol','RCT - Causale Impianto');
                    $xcrudAritmie->label('PMBiventricolDataImpianto','RCT - Data Impianto');

                    $xcrudAritmie->change_type('TPSV','select','',$opzioniInvertite);
                    $xcrudAritmie->change_type('TPSVAblazione','select','',$opzioniInvertite);
                    $xcrudAritmie->change_type('PreeccitazioneTac','select','',$opzioniInvertite);
                    $xcrudAritmie->change_type('PreeccitazioneTacAblazione','select','',$opzioniInvertite);
                    $xcrudAritmie->change_type('FAParadossAblazTrans','select','',',Efficace,Non Efficace');
                    $xcrudAritmie->change_type('TVSostenutaSintomatica','select','',',Sintomatica,Asintomatica');


                    $xcrudAritmie->fields('ExtraSopraVentrECG,ExtraSopraVentrECGHolter,ExVentricolECG,ExVentricolECGHolter,ExVentricolSempliciN,ExVentricolCoppieTripleN,ExVentricolTVNonSostenutaN,TPSV,TPSVAblazione,PreeccitazioneTac,PreeccitazioneTacAblazione,FAParadossDataInizio,FAParadossNEpisodi,FAParadossDurataEpisodi,FAParadossAblazTrans,FAPermanenteDataInizio,CVE,gDataCVE,TVSostenutaSintomatica,TVSostenutaSintomaticaData,TVSostenutaSintomaticaEpisodiN,TVSostenutaSintomaticaDurata,TVSostenutaSintomaticaFreqVentr,MIResuscitata,PM,PMCausaleImpianto,PMDataImpianto,PMTipologia,ICD,ICDCausaleImpianto,ICDDataImpianto,ICDScaricheAppropriateN,ICDScaricheInappropriate,PMBiventricol,PMBiventricolDataImpianto');

                    $xcrudAritmie->hide_button( 'save_new, save_return, return' );
                    $xcrudAritmie->table_name('Aritmie');
                    $xcrudAritmie->layout(7);

                    $xcrudAritmie->after_update('after_update_general');
                    echo $xcrudAritmie->render('edit', $IdQuadroClinico);
                }
            ?>
            </div>
            <div id="esamiStrumentali" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;display:none;">
                
            <?php
                
                if ($IdQuadroClinico!=NULL)
                {
                    $opzioniInvertite = $utils_db->getOpzioniInvertite();

                    $xcrudEsamiStrumentali->table('QuadroClinico');
                    
                    $xcrudEsamiStrumentali->label('ECGRitmoSin','Ritmo sinusale');
                    $xcrudEsamiStrumentali->label('ECGFA','FA');
                    $xcrudEsamiStrumentali->label('ECGRitmoElettro','Ritmo elettroindotto');
                    $xcrudEsamiStrumentali->label('ECGFrequenza','Frequenza cardiaca/min');
                    $xcrudEsamiStrumentali->label('preeccitazioneECG','Preeccitazione');
                    $xcrudEsamiStrumentali->label('TPSVECG','TPSV');
                    $xcrudEsamiStrumentali->label('BAV','BAV');
                    $xcrudEsamiStrumentali->label('ECGExSopraventr','Ex. sopraventricol');
                    $xcrudEsamiStrumentali->label('ECGExVentricolClasseLown','Ex. Ventricol.');
                    $xcrudEsamiStrumentali->label('ECGTV','TV');
                    $xcrudEsamiStrumentali->label('ECGDannoAtrialeSx','Danno atriale sx');
                    $xcrudEsamiStrumentali->label('ECGBBS','Bbs');
                    $xcrudEsamiStrumentali->label('ECGBBD','Bbd');
                    $xcrudEsamiStrumentali->label('ECGEAS','Eas');
                    $xcrudEsamiStrumentali->label('ECGEPS','Eps');
                    $xcrudEsamiStrumentali->label('ECGIpertVentrSx','Ipertrofia ventricolare sx');
                    $xcrudEsamiStrumentali->label('pregressoIMA','Pregresso IMA');
                    $xcrudEsamiStrumentali->label('ECGSovraVolume','Sovraccarico di volume');
                    $xcrudEsamiStrumentali->label('ECGIschemia','Ischemia');
                    $xcrudEsamiStrumentali->label('RXKillip','RX - Killip');
                    
                    $xcrudEsamiStrumentali->change_type('preeccitazioneECG','select','','Si,No');
                    $xcrudEsamiStrumentali->change_type('TPSVECG','select','','Si,No');
                    $xcrudEsamiStrumentali->change_type('BAV','select','',',I,II,III');
                    $xcrudEsamiStrumentali->change_type('ECGExVentricolClasseLown','select','',',1a,1b,2,3,4a,4b,5');
                    $xcrudEsamiStrumentali->change_type('ECGTV','select','',',Sostenuta,Non sostenuta');
                    $xcrudEsamiStrumentali->change_type('pregressoIMA','select','','Si,No');
                    $xcrudEsamiStrumentali->change_type('RXKillip','select','',',1,2,3,4');

                    $xcrudEsamiStrumentali->fields('ECGRitmoSin,ECGFA,ECGRitmoElettro,ECGFrequenza,preeccitazioneECG,TPSVECG,BAV,ECGExSopraventr,ECGExVentricolClasseLown,ECGTV,ECGDannoAtrialeSx,ECGBBS,ECGBBD,ECGEAS,ECGEPS,ECGIpertVentrSx,pregressoIMA,ECGSovraVolume,ECGIschemia,RXKillip',false,'ECG - RX TORACE');

                    $xcrudEsamiStrumentali->label('Eco2DIpertrSet','Ipertrofia settale mm.');
                    $xcrudEsamiStrumentali->label('Eco2DIpertrConc','Ipertrofia concentrica mm.');
                    $xcrudEsamiStrumentali->label('Eco2DIpocinesia','Ipocinesia - Acinesia');
                    $xcrudEsamiStrumentali->label('Eco2DVolSistVentrSx','Diametro sistolico ventricolare SX mm.');
                    $xcrudEsamiStrumentali->label('Eco2DVolDiastolico','Diametro diastolico mm.');
                    $xcrudEsamiStrumentali->label('Eco2DFE','Fe %');
                    $xcrudEsamiStrumentali->label('diametroAtrialeSX','Diametro Atriale Sx');
                    $xcrudEsamiStrumentali->label('Eco2DDisincr','Desincronizzazione');
                    $xcrudEsamiStrumentali->label('diametroAtrialeDX','Diametro Atriale Dx');
                    $xcrudEsamiStrumentali->label('diametroVentricolareDX','Diametro Ventricolare Dx');
                    $xcrudEsamiStrumentali->label('FunzVentricoloDX','Funzione Ventricolo Dx');
                    $xcrudEsamiStrumentali->label('prossimoControlloEco2D','Prossimo Controllo');

                    $xcrudEsamiStrumentali->change_type('Eco2DIpertrSet','select','',',Anteriore,Inferiore,Settale,Apicale,Globale');
                    $xcrudEsamiStrumentali->change_type('Eco2DDisincr','select','','Si,No');

                    $xcrudEsamiStrumentali->fields('Eco2DIpertrSet,Eco2DIpertrConc,Eco2DIpocinesia,Eco2DVolDiastolico,Eco2DVolSistVentrSx,Eco2DFE,diametroAtrialeSX,diametroAtrialeDX,Eco2DDisincr,diametroVentricolareDX,FunzVentricoloDX,prossimoControlloEco2D', false, 'ECO 2D');

                    $xcrudEsamiStrumentali->label('EcoDopplerDisfDiast','Disfunzione diastolica');
                    $xcrudEsamiStrumentali->label('EcoDopplerRigurMitralico','Rigurgito mitralico');
                    $xcrudEsamiStrumentali->label('EcoDopplerRigurAortico','Rigurgito aortico');
                    $xcrudEsamiStrumentali->label('EcoDopplerRigurTricuspi','Rigurgito tricuspidale');
                    $xcrudEsamiStrumentali->label('EcoDopplerPressPolm','Pressione polmonare');
                    $xcrudEsamiStrumentali->label('gStenosiAortica','Stenosi aortica gradiente mmHg');
                    $xcrudEsamiStrumentali->label('gStenosiMitralica','Stenosi mitralica area cm2');

                    $xcrudEsamiStrumentali->change_type('EcoDopplerDisfDiast','select','',',Normale,Alterato rilasciamento,Pseudonormalizzazione,Restrittivo reversibile,Restrittivo irreversibile');
                    $xcrudEsamiStrumentali->change_type('EcoDopplerRigurMitralico','select','',',+,++,+++');
                    $xcrudEsamiStrumentali->change_type('EcoDopplerRigurAortico','select','',',+,++,+++');
                    $xcrudEsamiStrumentali->change_type('EcoDopplerRigurTricuspi','select','',',+,++,+++');

                    $xcrudEsamiStrumentali->fields('EcoDopplerDisfDiast,EcoDopplerRigurMitralico,EcoDopplerRigurAortico,EcoDopplerRigurTricuspi,EcoDopplerPressPolm,gStenosiAortica,gStenosiMitralica', false, 'ECO DOPPLER');

                    $xcrudEsamiStrumentali->label('DopplerArtArtiInfSvenosi','DOPPLER ARTERIOSO ARTI INFERIORI - Stenosi significative');
                    $xcrudEsamiStrumentali->label('DopplerVasi','DOPPLER VASI SOPRAORTICI - Stenosi significative');
                    $xcrudEsamiStrumentali->label('ECGHRitmoSin','ECG Holter - Ritmo sinusale');
                    $xcrudEsamiStrumentali->label('ECGHFrequenza','ECG Holter - Frequenza cardiaca/min');
                    $xcrudEsamiStrumentali->label('ECGHFrequenzaMAX','ECG Holter - Frequenza cardiaca/max');
                    $xcrudEsamiStrumentali->label('BAVHolter','ECG Holter - BAV');
                    $xcrudEsamiStrumentali->label('ECGHExSopraventr','ECG Holter - Ex. sopraventricol');
                    $xcrudEsamiStrumentali->label('ECGHExVentricolClasseLow','ECG Holter - Ex. Ventricol.');
                    $xcrudEsamiStrumentali->label('ECGHTV','ECG Holter - TV');
                    $xcrudEsamiStrumentali->label('ECGHFA','ECG Holter - FA');
                    $xcrudEsamiStrumentali->label('ECGHIschemia','ECG Holter - Ischemia Transitoria');

                    $xcrudEsamiStrumentali->change_type('BAVHolter','select','',',I,II,III');
                    $xcrudEsamiStrumentali->change_type('ECGHExVentricolClasseLow','select','',',I,II,III');
                    $xcrudEsamiStrumentali->change_type('ECGHTV','select','',',Sostenuta,Non sostenuta'); 

                    $xcrudEsamiStrumentali->fields('DopplerArtArtiInfSvenosi,DopplerVasi,ECGHRitmoSin,ECGHFrequenza,ECGHFrequenzaMAX,BAVHolter,ECGHExSopraventr,ECGHExVentricolClasseLow,ECGHTV,ECGHFA,ECGHIschemia', false, 'DOPPLER - ECG HOLTER');

                    $xcrudEsamiStrumentali->label('gGMWT','Ec. m.');
                    $xcrudEsamiStrumentali->label('gPO2','pO 2');
                    $xcrudEsamiStrumentali->label('gPCD2','pCO 2');

                    $xcrudEsamiStrumentali->fields('gGMWT,gPO2,gPCD2', false, '6MWT - EMOGASANALISI');

                    $xcrudEsamiStrumentali->label('retinopatia','Retinopatia');
                    $xcrudEsamiStrumentali->label('ecoAddome','Eco-Addome');

                    $xcrudEsamiStrumentali->fields('retinopatia,ecoAddome', false, 'DIABETOLOGICI');

/*
                    $xcrudAritmie->change_type('TPSV','select','',$opzioniInvertite);
                    $xcrudAritmie->change_type('TPSVAblazione','select','',$opzioniInvertite);
                    $xcrudAritmie->change_type('PreeccitazioneTac','select','',$opzioniInvertite);
                    $xcrudAritmie->change_type('PreeccitazioneTacAblazione','select','',$opzioniInvertite);
                    $xcrudAritmie->change_type('FAParadossAblazTrans','select','',',Efficace,Non Efficace');
                    $xcrudAritmie->change_type('TVSostenutaSintomatica','select','',',Sintomatica,Asintomatica');
*/

                    $xcrudEsamiStrumentali->hide_button( 'save_new, save_return, return' );
                    $xcrudEsamiStrumentali->table_name('Esami Strumentali');
                    $xcrudEsamiStrumentali->layout(7);

                    $xcrudEsamiStrumentali->after_update('after_update_general');
                    echo $xcrudEsamiStrumentali->render('edit', $IdQuadroClinico);
                }
            ?>
            </div>
            <div id="cardiologo" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;display:none;">
                
            <?php

                if ($IdQuadroClinico!=NULL)
                {
                    $opzioniDisturbiRespiratori = $utils_db->getOpzioniCardiologoDisturbiRespiratori();
                    $classeNYHA = $utils_db->getClasseNYHA();
                    $opzioniContrazioneDiuresi = $utils_db->getContrazioneDiuresi();
                    $opzioniToniCardiaci = $utils_db->getOpzioniToniCardiaci();
                    $opzioniCongestionePolmonare = $utils_db->getCongestionePolmonare();
                    
                    $xcrudCardiologo->table('QuadroClinico');

                    $xcrudCardiologo->label('cardiologoData','Data della visita cardiologica');
                    $xcrudCardiologo->label('cardiologoDisturbiRespiratorio','Disturbi a carico dell\'apparato respiratorio');
                    $xcrudCardiologo->label('ClasseDispneaSforzo','Classe NYHA');
                    $xcrudCardiologo->label('AsteniaNew','Astenia');
                    $xcrudCardiologo->label('doloreAnginoso','Dolore Anginoso');
                    $xcrudCardiologo->change_type('cardiologoDisturbiRespiratorio','checkboxes','',$opzioniDisturbiRespiratori);
                    $xcrudCardiologo->change_type('ClasseDispneaSforzo','select','',$classeNYHA);
                    $xcrudCardiologo->fields('cardiologoData,cardiologoDisturbiRespiratorio,AsteniaNew,doloreAnginoso,ClasseDispneaSforzo', false, "Generale - Sintomatologia");



                    $xcrudCardiologo->label('gPesoCorporeo','Peso corporeo (kg)');
                    $xcrudCardiologo->label('CircAddominNew','Circonferenza addominale (cm)');
                    $xcrudCardiologo->label('gPASistolica','Pressione arteriosa sistolica');
                    $xcrudCardiologo->label('gPADiastolica','Pressione arteriosa diastolica');
                    $xcrudCardiologo->label('ContrazDiuresi','Contrazione della diuresi');
                    $xcrudCardiologo->label('gFrequenzaCard','Frequenza Cardiaca');
                    $xcrudCardiologo->label('ToniValidiRidotti','Toni cardiaci');
                    $xcrudCardiologo->label('Soffi','Presenza di soffi cardiaci o vascolari');
                    $xcrudCardiologo->label('DescrizioneSoffi','Descrizione soffi cardiaci/vascolari');
                    $xcrudCardiologo->change_type('ContrazDiuresi','select','',$opzioniContrazioneDiuresi);
                    $xcrudCardiologo->change_type('ToniValidiRidotti','select','',$opzioniToniCardiaci);
                    $xcrudCardiologo->fields('gPesoCorporeo,CircAddominNew,gPASistolica,gPADiastolica,ContrazDiuresi,gFrequenzaCard,ToniValidiRidotti,Soffi,DescrizioneSoffi', false, "Esame Obiettivo");


                    $xcrudCardiologo->label('polsiArteriosiPeriferici','Presenti');
                    $xcrudCardiologo->label('CongestionePolmonare','Congestione polmonare');
                    $xcrudCardiologo->change_type('CongestionePolmonare','select','',$opzioniCongestionePolmonare);
                    $xcrudCardiologo->fields('polsiArteriosiPeriferici,Cianosi,CongestionePolmonare', false, "Polsi arteriosi periferici");

                    $xcrudCardiologo->label('CongestionePerifGiugularimag3','Turgore delle Giugulari');
                    $xcrudCardiologo->label('CongestionePerifEpatomegalia','Epatomegalia');
                    $xcrudCardiologo->label('CongestionePerifEdemiPeriferici','Edemi Periferici');
                    $xcrudCardiologo->label('CongestionePerifAscite','Ascite');
                    $xcrudCardiologo->fields('CongestionePerifGiugularimag3,CongestionePerifEpatomegalia,CongestionePerifEdemiPeriferici,CongestionePerifAscite', false, "Congestione Periferica");
                    
                    if (($tipologiaUtente=="Cardiologo Ambulatoriale") || ($tipologiaUtente=="Admin"))
                    {
                        $xcrudCardiologo->hide_button( 'save_new, save_return, return' );
                    }
                    else
                    {
                        $xcrudCardiologo->disabled('cardiologoData,cardiologoDisturbiRespiratorio,NEpisodiDispnea,NCuscini,ClasseNYHA');
                        $xcrudCardiologo->disabled('cardiologoAltezza,gPesoCorporeo,CircAddominNew,gPASistolica,gPADiastolica,ContrazDiuresi,gFrequenzaCard,ToniValidiRidotti,DescrizioneToniCardiaci,Soffi,DescrizioneSoffi');
                        $xcrudCardiologo->disabled('Cianosi,CongestionePolmonare');
                        $xcrudCardiologo->hide_button( 'save_new, save_return, return, save_edit' );
                    }

                    $xcrudCardiologo->table_name('Quadro Clinico');

                    $xcrudCardiologo->after_update('after_update_general');
                    echo $xcrudCardiologo->render('edit', $IdQuadroClinico);
                }
            ?>
            </div>            
            <div id="pneumologo" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;<? if ($showFirst!="pneumologo") echo 'display:none;'?>">
                
            <?php

                if ($IdQuadroClinico!=NULL)
                {
                    $opzioniDisturbiRespiratori = $utils_db->getOpzioniCardiologoDisturbiRespiratori();
                    $opzioniCapacitaDeambulazione = $utils_db->getOpzioniCapacitaDeambulazione();
                    $opzioniCongestionePolmonare = $utils_db->getCongestionePolmonare();

                    $xcrudPneumologo->table('QuadroClinico');

                    $xcrudPneumologo->label('pneumologoData','Data della visita pneumologica');
                    $xcrudPneumologo->label('pneumologoDoloreVisitaVAS','Presenza di Dolore al momento della visita (VAS 0-100)');
                    $xcrudPneumologo->label('cardiologoDisturbiRespiratorio','Disturbi a carico dell\'apparato respiratorio'); //lo riutilizzo, tanto in caso sono 2 righe differenti
                    $xcrudPneumologo->label('pneumologoDispneaVisita','Grado di dispnea rilevato al momento della visita (Scala di Borg (0:Nessuna - 10:Insopportabile))');
                    $xcrudPneumologo->change_type('cardiologoDisturbiRespiratorio','checkboxes','',$opzioniDisturbiRespiratori);
                    $xcrudPneumologo->fields('pneumologoData,pneumologoDoloreVisitaVAS,cardiologoDisturbiRespiratorio,pneumologoDispneaVisita', false, "Generale - Sintomatologia");

                    $xcrudPneumologo->label('gPASistolica','Pressione arteriosa sistolica');
                    $xcrudPneumologo->label('gPADiastolica','Pressione arteriosa diastolica');
                    $xcrudPneumologo->label('gFrequenzaCard','Frequenza Cardiaca');
                    $xcrudPneumologo->label('gAttiRespiratori','Frequenza Respiratoria');
                    $xcrudPneumologo->label('pneumologoCapacitaDeambulazione','Capacità di deambulazione');
                    $xcrudPneumologo->label('pneumologoOssigenoterapia','Ossigenoterapia');
                    $xcrudPneumologo->label('gCongestionePolmonare','Congestione Polmonare');
                    $xcrudPneumologo->change_type('gCongestionePolmonare','select','',$opzioniCongestionePolmonare);
                    $xcrudPneumologo->change_type('pneumologoCapacitaDeambulazione','select','',$opzioniCapacitaDeambulazione);
                    $xcrudPneumologo->fields('gPASistolica,gPADiastolica,gFrequenzaCard,gAttiRespiratori,pneumologoCapacitaDeambulazione,pneumologoOssigenoterapia,gCongestionePolmonare', false, "Esame Obiettivo");

                    $xcrudPneumologo->label('testCamminoFCmax','Frequenza Cardiaca Massima durante il test (battiti/minuto)');
                    $xcrudPneumologo->label('testCamminoSaturazioneRiposo','Saturazione di Ossigeno a riposo (%)');
                    $xcrudPneumologo->label('testCamminoSaturazioneMedia','Saturazione di Ossigeno Media durante il test (%)'); 
                    $xcrudPneumologo->label('testCamminoSaturazioneMinima','Saturazione di Ossigeno Minima durante il test (%)');
                    $xcrudPneumologo->fields('testCamminoFCmax,testCamminoSaturazioneRiposo,testCamminoSaturazioneMedia,testCamminoSaturazioneMinima', false, "Test del Cammino di 6 minuti");

                    $xcrudPneumologo->label('spirometriaVolumeMax','Volume Espiratorio Massimo in 1 Secondo (VEMS) (ml)');
                    $xcrudPneumologo->label('spirometriaCVF','Capacità Vitale Forzata (CVF) (ml)');
                    $xcrudPneumologo->label('spirometriaIndiceTiffenau','Indice di Tiffenau (VEMS/CVF) (%)'); 
                    $xcrudPneumologo->label('spirometriaPiccoEspiratorio','Picco di Flusso Espiratorio (ml)');
                    $xcrudPneumologo->label('spirometriaPiccoInspiratorio','Picco di Flusso Inspiratorio (ml)');
                    $xcrudPneumologo->fields('spirometriaVolumeMax,spirometriaCVF,spirometriaIndiceTiffenau,spirometriaPiccoEspiratorio,spirometriaPiccoInspiratorio', false, "Spirometria");


                    if (($tipologiaUtente=="Specialista Pneumologo") || ($tipologiaUtente=="Admin"))
                    {
                        $xcrudPneumologo->hide_button( 'save_new, save_return, return' );
                    }
                    else
                    {
                        $xcrudPneumologo->disabled('pneumologoData');
                        $xcrudPneumologo->hide_button( 'save_new, save_return, return, save_edit' );
                    }

                    $xcrudPneumologo->table_name('Quadro Clinico');
                    $xcrudPneumologo->layout(7);

                    if ($IdQuadroClinico==NULL) //sta inserendo una nuova scheda del quadro clinico
                    {
                        echo $xcrudPneumologo->render('create');
                    }
                    else
                    {
                        $xcrudPneumologo->after_update('after_update_general');
                        echo $xcrudPneumologo->render('edit', $IdQuadroClinico);
                    }
                }
            ?>
            </div> 
            <div id="nefrologo" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;<? if ($showFirst!="nefrologo") echo 'display:none;'?>">
                
            <?php

                if ($IdQuadroClinico!=NULL)
                {
                    $opzioniClassificazioneMalattiaRenaleCronica = $utils_db->getClassificazioneMalattiaRenaleCronica();

                    $xcrudNefrologo->table('QuadroClinico');

                    $xcrudNefrologo->label('nefrologoData','Data della visita nefrologica');
                    $xcrudNefrologo->label('nefrologoDiagnosiMalattiaRenaleCronica','Diagnosi di malattia renale cronica');
                    $xcrudNefrologo->label('nefrologoClassificazioneMalattiaRenaleCronica','Classificazione della malattia renale cronica');
                    $xcrudNefrologo->change_type('nefrologoClassificazioneMalattiaRenaleCronica','select','',$opzioniClassificazioneMalattiaRenaleCronica);
                    $xcrudNefrologo->fields('nefrologoData,nefrologoDiagnosiMalattiaRenaleCronica,nefrologoClassificazioneMalattiaRenaleCronica');

                    if (($tipologiaUtente=="Specialista Nefrologo") || ($tipologiaUtente=="Admin"))
                    {
                        $xcrudNefrologo->hide_button( 'save_new, save_return, return' );
                    }
                    else
                    {
                        $xcrudNefrologo->disabled('nefrologoData');
                        $xcrudNefrologo->hide_button( 'save_new, save_return, return, save_edit' );
                    }

                    $xcrudNefrologo->table_name('Quadro Clinico');

                    if ($IdQuadroClinico==NULL) //sta inserendo una nuova scheda del quadro clinico
                    {
                        echo $xcrudNefrologo->render('create');
                    }
                    else
                    {
                        $xcrudNefrologo->after_update('after_update_general');
                        echo $xcrudNefrologo->render('edit', $IdQuadroClinico);
                    }
                }
            ?>
            </div>  
            <div id="medicoDiBase" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;<? if ($showFirst!="medicoBase") echo 'display:none;'?>">
                
            <?php

                if ($IdQuadroClinico!=NULL)
                {
                    $opzioniCongestionePolmonare = $utils_db->getCongestionePolmonare();

                    $xcrudMedicoDiBase->table('QuadroClinico');

                    $xcrudMedicoDiBase->label('medicoBaseData','Data della visita di controllo');
                    $xcrudMedicoDiBase->label('gPesoCorporeo','Peso corporeo (kg)');
                    $xcrudMedicoDiBase->label('CircAddominNew','Circonferenza addominale (cm)');
                    $xcrudMedicoDiBase->label('gSist','Pressione Arteriosa Sistolica');
                    $xcrudMedicoDiBase->label('gDiast','Pressione Arteriosa Diastolica');
                    $xcrudMedicoDiBase->label('gFrequenzaCard','Frequenza Cardiaca');
                    $xcrudMedicoDiBase->label('gAttiRespiratori','Frequenza Respiratoria');
                    $xcrudMedicoDiBase->label('gCongestionePolmonare','Congestione Polmonare');

                    $xcrudMedicoDiBase->change_type('gCongestionePolmonare','select','',$opzioniCongestionePolmonare);

                    $xcrudMedicoDiBase->fields('medicoBaseData,gPesoCorporeo,CircAddominNew,gSist,gDiast,gFrequenzaCard,gAttiRespiratori,gCongestionePolmonare', false, "Generale - Sintomatologia");

                    $xcrudMedicoDiBase->label('CongestionePerifGiugularimag3','Turgore delle Giugulari');
                    $xcrudMedicoDiBase->label('CongestionePerifEpatomegalia','Epatomegalia');
                    $xcrudMedicoDiBase->label('CongestionePerifEdemiPeriferici','Edemi Perifierici');
                    $xcrudMedicoDiBase->label('CongestionePerifAscite','Ascite');
                    $xcrudMedicoDiBase->fields('CongestionePerifGiugularimag3,CongestionePerifEpatomegalia,CongestionePerifEdemiPeriferici,CongestionePerifAscite', false, "Congestione Periferica");

                    $xcrudMedicoDiBase->label('gDiagnosi','Diagnosi');
                    $xcrudMedicoDiBase->fields('gDiagnosi', false, "Malattie Intercorrenti");

                    $xcrudMedicoDiBase->hide_button( 'save_new, save_return, return' );
                    $xcrudMedicoDiBase->table_name('Dettagli Controllo');

                    if ($IdQuadroClinico==NULL) //sta inserendo una nuova scheda del quadro clinico
                    {
                        echo $xcrudMedicoDiBase->render('create');
                    }
                    else
                    {
                        $xcrudMedicoDiBase->after_update('after_update_general');
                        echo $xcrudMedicoDiBase->render('edit', $IdQuadroClinico);
                    }
                }
            ?>
            </div>   
            <div id="nutrizionista" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;<? if ($showFirst!="nutrizionista") echo 'display:none;'?>">
                
            <?php

                if ($IdQuadroClinico!=NULL)
                {


                    $opzioniNutrizioneCaloAlimentare = $utils_db->getNutrizioneCaloAlimentare();
                    $opzioniNutrizionePerditaPeso = $utils_db->getNutrizionePerditaPeso();
                    $opzioniNutrizioneMobilita = $utils_db->getNutrizioneMobilita();
                    $opzioniNutrizioneProbNeuropsicologici = $utils_db->getNutrizioneProbNeuropsicologici();
                    $opzioniNutrizioneBMI = $utils_db->getNutrizioneBMI();
                    $opzioniNutrizioneAssNumeroPasti = $utils_db->getNutrizioneAssNumeroPasti();
                    $opzioniNutrizioneNumRispAfferm = $utils_db->getNutrizioneNumRispAfferm();
                    $opzioniNutrizioneLiquidi = $utils_db->getNutrizioneLiquidi();
                    $opzioniNutrizioneModalitaAlim = $utils_db->getNutrizioneModalitaAlim();
                    $opzioniNutrizioneAutovalut = $utils_db->getNutrizioneAutovalut();
                    $opzioniNutrizioneStatoSalute = $utils_db->getNutrizioneStatoSalute();
                    $opzioniNutrizioneCMB = $utils_db->getNutrizioneCMB();
                    $opzioniNutrizioneCP = $utils_db->getNutrizioneCP();


                    $xcrudNutrizionista->table('QuadroClinico');

                    $xcrudNutrizionista->label('nutrizionistaData','Data della visita di controllo nutrizionale');
                    $xcrudNutrizionista->label('gPesoCorporeo','Peso corporeo (kg)');
                    $xcrudNutrizionista->label('nutrizionistaBMI','BMI');
                    $xcrudNutrizionista->label('nutriCircBraccio','Circonferenza braccio (cm)');
                    $xcrudNutrizionista->label('nutriCircPolso','Circonferenza polso (cm)');
                    $xcrudNutrizionista->label('CircAddominNew','Circonferenza vita (cm)');
                    $xcrudNutrizionista->label('nutriCircFianchi','Circonferenza fianchi (cm)');
                    $xcrudNutrizionista->label('nutriCircRadiceCoscia','Circonferenza della radice della coscia (cm)');
                    $xcrudNutrizionista->label('nutriCircMedianaCoscia','Circonferenza mediana della coscia (cm)');
                    $xcrudNutrizionista->label('nutriCircSovraPatellare','Circonferenza sovra-patellare (cm)');
                    $xcrudNutrizionista->label('nutriCircCaviglia','Circonferenza della caviglia (cm)');
                    $xcrudNutrizionista->label('nutriRappVitaFianchi','Rapporto vita/fianchi');

                    $xcrudNutrizionista->fields('nutrizionistaData,gPesoCorporeo,nutrizionistaBMI,nutriCircBraccio,nutriCircPolso,CircAddominNew,nutriCircFianchi,nutriCircRadiceCoscia,nutriCircMedianaCoscia,nutriCircSovraPatellare,nutriCircCaviglia,nutriRappVitaFianchi', false, "Generale - Esame Obiettivo");

                    $xcrudNutrizionista->label('nutriBioTBW','Acqua corporea totale (TBW) (L)');
                    $xcrudNutrizionista->label('nutriBioICW','Acqua intracellulare (ICW) (L)');
                    $xcrudNutrizionista->label('nutriBioECW','Acqua extracellulare (ECW) (L)');
                    $xcrudNutrizionista->label('nutriBioFFM','Massa magra (FFM) (Kg)');
                    $xcrudNutrizionista->label('nutriBioFM','Massa grassa (FM) (Kg)');
                    $xcrudNutrizionista->label('nutriBioRappFFMFM','Rapporto Massa magra/massa grassa (FFM/FM)');
                    $xcrudNutrizionista->label('nutriBioBMC','Massa cellulare (BMC) (Kg)');
                    $xcrudNutrizionista->label('nutriBioECM','Massa extracellulare (ECM) (Kg)');
                    $xcrudNutrizionista->label('nutriBioPotassioScamb','Potassio scambiabile (mEq)');
                    $xcrudNutrizionista->label('nutriBioRappSodPot','Rapporto sodio/potassio');
                    $xcrudNutrizionista->label('nutriBioPesoDesid','Peso desiderabile (Kg)');
                    $xcrudNutrizionista->label('nutriBioFAT','Massa grassa desiderabile (FAT) (%)');
                    $xcrudNutrizionista->label('nutriBioMassaAcquist','Massa da acquistare (Kg)');
                    $xcrudNutrizionista->label('nutriBioBMR','Metabolismo basale (BMR) (kcal)');

                    $xcrudNutrizionista->fields('nutriBioTBW,nutriBioICW,nutriBioECW,nutriBioFFM,nutriBioFM,nutriBioRappFFMFM,nutriBioBMC,nutriBioECM,nutriBioPotassioScamb,nutriBioRappSodPot,nutriBioPesoDesid,nutriBioFAT,nutriBioMassaAcquist,nutriBioBMR', false, "Bioimpedenziometria");

                    $xcrudNutrizionista->label('nutriAssScreenCaloAppAlim','L\'apporto alimentare è diminuito negli ultimi tre mesi a causa di perdita di appetito, problemi digestivi o difficoltà di masticazione o deglutizione?');
                    $xcrudNutrizionista->label('nutriAssScreenPerditaPeso','Vi è stata una perdita di peso involontaria negli ultimi 3 mesi?');
                    $xcrudNutrizionista->label('nutriAssScreenMobilita','Mobilità');
                    $xcrudNutrizionista->label('nutriAssScreenStress','Il paziente ha sofferto di stress psicologici o malattie acute negli ultimi tre mesi?');
                    $xcrudNutrizionista->label('nutriAssScreenProbNeuro','Problemi neuropsicologici');
                    $xcrudNutrizionista->label('nutriAssScreenBMI','BMI');
                    $xcrudNutrizionista->label('nutriAssScreenPunteggio','Punteggio MNA screening - (12-14 stato nutrizionale normale - 8-11 a rischio di malnutrizione - 0-7 malnutrito)');
                    $xcrudNutrizionista->change_type('nutriAssScreenCaloAppAlim','select','',$opzioniNutrizioneCaloAlimentare);
                    $xcrudNutrizionista->change_type('nutriAssScreenPerditaPeso','select','',$opzioniNutrizionePerditaPeso);
                    $xcrudNutrizionista->change_type('nutriAssScreenMobilita','select','',$opzioniNutrizioneMobilita);
                    $xcrudNutrizionista->change_type('nutriAssScreenProbNeuro','select','',$opzioniNutrizioneProbNeuropsicologici);
                    $xcrudNutrizionista->change_type('nutriAssScreenBMI','select','',$opzioniNutrizioneBMI);

                    $xcrudNutrizionista->fields('nutriAssScreenCaloAppAlim,nutriAssScreenPerditaPeso,nutriAssScreenMobilita,nutriAssScreenStress,nutriAssScreenProbNeuro,nutriAssScreenBMI,nutriAssScreenPunteggio', false, "Assessment - Screening");

                    $xcrudNutrizionista->label('nutriAssValutAutonomo','Il paziente vive autonomamente (non in una casa di cura)?');
                    $xcrudNutrizionista->label('nutriAssValutAssumeFarmaci','Assume più di 3 farmaci al giorno soggetti a prescrizione medica?');
                    $xcrudNutrizionista->label('nutriAssValutPiaghe','Piaghe da decubito o ulcere cutanee?');
                    $xcrudNutrizionista->label('nutriAssValutPasti','Quanti pasti completi consuma il paziente al giorno?');
                    $xcrudNutrizionista->change_type('nutriAssValutPasti','select','',$opzioniNutrizioneAssNumeroPasti);

                    $xcrudNutrizionista->fields('nutriAssValutAutonomo,nutriAssValutAssumeFarmaci,nutriAssValutPiaghe,nutriAssValutPasti', false, "Assessment - Valutazione");

                    $xcrudNutrizionista->label('nutriPazienteLatticini','Il paziente consuma almeno una porzione di latticini al giorno');
                    $xcrudNutrizionista->label('nutriPazienteLegumi','Il paziente consuma due o più porzioni di legumi o uova alla settimana');
                    $xcrudNutrizionista->label('nutriPazienteCarnePesce','Il paziente consuma carne, pesce o pollame ogni giorno');
                    $xcrudNutrizionista->label('nutriPazienteNumRispAfferm','Numero di risposte affermative alla precedente domanda (Il paziente consuma ...)');
                    $xcrudNutrizionista->label('nutriPazienteFrutta','Il paziente consuma due o più porzioni di frutta o verdura al giorno?');
                    $xcrudNutrizionista->label('nutriPazienteLiquidi','Quanti liquidi (acqua, succo, caffè, tè, latte) assume al giorno?');
                    $xcrudNutrizionista->label('nutriPazienteModalitaAlim','Modalità di alimentazione?');
                    $xcrudNutrizionista->label('nutriPazienteAutovalut','Autovalutazione dello stato nutrizionale');
                    $xcrudNutrizionista->label('nutriPazienteStatoSalute','Come giudica il paziente il proprio stato di salute rispetto ad altre persone della sua età:');
                    $xcrudNutrizionista->label('nutriPazienteCMB','Circonferenza a metà braccio (CMB) in cm');
                    $xcrudNutrizionista->label('nutriPazienteCP','Circonferenza del polpaccio (CP) in cm');
                    $xcrudNutrizionista->label('nutriPazienteValut','Valutazione globale');
                    $xcrudNutrizionista->change_type('nutriPazienteNumRispAfferm','select','',$opzioniNutrizioneNumRispAfferm);
                    $xcrudNutrizionista->change_type('nutriPazienteLiquidi','select','',$opzioniNutrizioneLiquidi);
                    $xcrudNutrizionista->change_type('nutriPazienteModalitaAlim','select','',$opzioniNutrizioneModalitaAlim);
                    $xcrudNutrizionista->change_type('nutriPazienteAutovalut','select','',$opzioniNutrizioneAutovalut);
                    $xcrudNutrizionista->change_type('nutriPazienteStatoSalute','select','',$opzioniNutrizioneStatoSalute);
                    $xcrudNutrizionista->change_type('nutriPazienteCMB','select','',$opzioniNutrizioneCMB);
                    $xcrudNutrizionista->change_type('nutriPazienteCP','select','',$opzioniNutrizioneCP);

                    $xcrudNutrizionista->fields('nutriPazienteLatticini,nutriPazienteLegumi,nutriPazienteCarnePesce,nutriPazienteNumRispAfferm,nutriPazienteFrutta,nutriPazienteLiquidi,nutriPazienteModalitaAlim,nutriPazienteAutovalut,nutriPazienteStatoSalute,nutriPazienteCMB,nutriPazienteCP,nutriPazienteValut', false, "Alimentazione");

                    $xcrudNutrizionista->label('nutriAssPuntFinale','Valutazione dello stato nutrizionale - (24-30 stato nutrizionale normale - 17-23.5 rischio di malnutrizione - < 17 cattivo stato nutrizionale)');
                    $xcrudNutrizionista->fields('nutriAssPuntFinale', false, "Assessment - Punteggio Finale");


                    $xcrudNutrizionista->hide_button( 'save_new, save_return, return' );
                    $xcrudNutrizionista->table_name('Nutrizionista');

                    if ($IdQuadroClinico==NULL) //sta inserendo una nuova scheda del quadro clinico
                    {
                        echo $xcrudNutrizionista->render('create');
                    }
                    else
                    {
                        $xcrudNutrizionista->after_update('after_update_general');
                        echo $xcrudNutrizionista->render('edit', $IdQuadroClinico);
                    }
                    
                }
            ?>
            </div>  
            <div id="diabetologo" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;<? if ($showFirst!="diabetologo") echo 'display:none;'?>">
                
            <?php

                if ($IdQuadroClinico!=NULL)
                {

                    
                    $xcrudDiabetologo->table('QuadroClinico');

                    $xcrudDiabetologo->label('diabetologoData','Data della visita di controllo diabetologica');
                    $xcrudDiabetologo->label('gPesoCorporeo','Peso corporeo (kg)');
                    $xcrudDiabetologo->label('CircAddominNew','Circonferenza vita (cm)');
                    $xcrudDiabetologo->label('gPASistolica','Pressione arteriosa sistolica');
                    $xcrudDiabetologo->label('gPADiastolica','Pressione arteriosa diastolica');
                    $xcrudDiabetologo->label('gFrequenzaCard','Frequenza Cardiaca');
                    
                    $xcrudDiabetologo->fields('diabetologoData,gPesoCorporeo,CircAddominNew,gPASistolica,gPADiastolica,gFrequenzaCard', false, "Generale - Esame Obiettivo");


                    $xcrudDiabetologo->hide_button( 'save_new, save_return, return' );
                    $xcrudDiabetologo->table_name('Quadro Clinico');

                    if ($IdQuadroClinico==NULL) //sta inserendo una nuova scheda del quadro clinico
                    {
                        echo $xcrudDiabetologo->render('create');
                    }
                    else
                    {
                        $xcrudDiabetologo->after_update('after_update_general');
                        echo $xcrudDiabetologo->render('edit', $IdQuadroClinico);
                    }
                    
                }
            ?>
            </div>  
            <div id="qualitaVita" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;display:none;">
            
            <?php

                if ($IdQuadroClinico!=NULL)
                {
                    //devo trovare l'id della chiave del quadro relativo alla tabella
                    $sel = "SELECT IdQualitaVita from qualita_vita where IdPaziente=".$userId;
                    $db->query($sel);
                    $result = $db->result();
                    if (count($result)==0)
                    {
                        //non ha un scheda di qulità vita, possibile nuovo inserimento... lo inserisco io di default
                        $insertQuery = "INSERT INTO qualita_vita (IdPaziente) VALUES (".$userId.")";
                        $db->query($insertQuery);
                        $IdQualitaVita = $db->insert_id();
                    }
                    else
                    {
                        $IdQualitaVita = $result[0]["IdQualitaVita"];   
                    }
                    //echo 'QualitaVita: '.$IdQualitaVita;
                    //echo '<pre>'; print_r($result); echo '</pre>';

                    $opzioniCapacitaMovimento = $utils_db->getOpzioniCapacitaMovimento();
                    $opzioniCuraPersona = $utils_db->getOpzioniCuraPersona();
                    $opzioniAttivitaAbituali = $utils_db->getOpzioniAttivitaAbituali();
                    $opzioniDoloreFastidio = $utils_db->getOpzioniDoloreFastidio();
                    $opzioniAnsiaDepressione = $utils_db->getOpzioniAnsiaDepressione();


                    $xcrudQualitaVita->table('qualita_vita');

                    $xcrudQualitaVita->label('SchedaData','Data di compilazione del questionario');
                    $xcrudQualitaVita->label('CapacitaMovimento','Capacità di movimento');
                    $xcrudQualitaVita->label('CuraPersona','Cura della persona');
                    $xcrudQualitaVita->label('AttivitaAbituali','Attività abituali (per es. lavoro, studio, lavori domestici,attività familiari o di svago)');
                    $xcrudQualitaVita->label('DoloreFastidio','Dolore o fastidio');
                    $xcrudQualitaVita->label('AnsiaDepressione','Ansia o depressione');
                    $xcrudQualitaVita->label('SaluteOggi','Indichi, su una scala 0-100, com\'è la sua salute OGGI');
                    $xcrudQualitaVita->change_type('CapacitaMovimento','select','',$opzioniCapacitaMovimento);
                    $xcrudQualitaVita->change_type('CuraPersona','select','',$opzioniCuraPersona);
                    $xcrudQualitaVita->change_type('AttivitaAbituali','select','',$opzioniAttivitaAbituali);
                    $xcrudQualitaVita->change_type('DoloreFastidio','select','',$opzioniDoloreFastidio);
                    $xcrudQualitaVita->change_type('AnsiaDepressione','select','',$opzioniAnsiaDepressione);
                    $xcrudQualitaVita->fields('SchedaData,CapacitaMovimento,CuraPersona,AttivitaAbituali,DoloreFastidio,AnsiaDepressione,SaluteOggi');


                    $xcrudQualitaVita->hide_button( 'save_new, save_return, return' );
                    $xcrudQualitaVita->table_name('Qualità della Vita');
                    $xcrudQualitaVita->layout(6);

                    $xcrudQualitaVita->after_update('after_update_general');
                    echo $xcrudQualitaVita->render('edit', $IdQualitaVita);
                }
            ?>
            </div>
            <div id="qualitaSonno" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;display:none;">
            
            <?php

                if ($IdQuadroClinico!=NULL)
                {
                    //devo trovare l'id della chiave del quadro relativo alla tabella
                    $sel = "SELECT IdQualitaSonno from qualita_sonno where IdPaziente=".$userId;
                    $db->query($sel);
                    $result = $db->result();
                    if (count($result)==0)
                    {
                        //non ha un scheda di qualità sonno, possibile nuovo inserimento... lo inserisco io di default
                        $insertQuery = "INSERT INTO qualita_sonno (IdPaziente) VALUES (".$userId.")";
                        $db->query($insertQuery);
                        $IdQualitaSonno = $db->insert_id();
                    }
                    else
                    {
                        $IdQualitaSonno = $result[0]["IdQualitaSonno"];   
                    }
                    //echo 'QualitaSonno: '.$IdQualitaSonno;
                    //echo '<pre>'; print_r($result); echo '</pre>';

                    $opzioniTempoAddormentamento = $utils_db->getOpzioniTempoAddormentamento();
                    $opzioniTempoSonno = $utils_db->getOpzioniTempoSonno();
                    $opzioniProblemiGenericiSonno = $utils_db->getOpzioniProblemiGenericiSonno();
                    $opzioniQualitaSonno = $utils_db->getOpzioniQualitaSonno();
                    $opzioniEnergieSufficienti = $utils_db->getOpzioniEnergieSufficienti();

                    $xcrudQualitaSonno->table('qualita_sonno');

                    $xcrudQualitaSonno->label('SchedaData','Data di compilazione del questionario');
                    $xcrudQualitaSonno->label('OraLetto','Di solito, a che ora è andata/o a letto la sera?');
                    $xcrudQualitaSonno->label('TempoAddormentamento','Di solito, quanto tempo ha impiegato ad addormentarsi ogni notte ?');
                    $xcrudQualitaSonno->label('OraSveglio','Di solito, a che ora si è alzata/o al mattino?');
                    $xcrudQualitaSonno->label('OreSonno','Quante ore ha dormito effettivamente per notte? (potrebbero essere diverse dal numero di ore passate a letto)');
                    $xcrudQualitaSonno->label('ProblemaAddormentarsi30Minuti','Quanto spesso ha avuto problemi di sonno dovuti a non riuscire ad addormentarsi entro 30 minuti');
                    $xcrudQualitaSonno->label('ProblemaSvegliarsiNotte','Quanto spesso ha avuto problemi di sonno dovuti a svegliarsi nel mezzo della notte o al mattino presto senza riaddormentarsi subito');
                    $xcrudQualitaSonno->label('ProblemaAlzarsiBagno','Quanto spesso ha avuto problemi di sonno dovuti a alzarsi nel mezzo della notte per andare in bagno');
                    $xcrudQualitaSonno->label('ProblemaRespirareBene','Quanto spesso ha avuto problemi di sonno dovuti a non riuscire a respirare bene');
                    $xcrudQualitaSonno->label('ProblemaTossireRussareForte','Quanto spesso ha avuto problemi di sonno dovuti a tossire o russare forte');
                    $xcrudQualitaSonno->label('ProblemaSentireFreddo','Quanto spesso ha avuto problemi di sonno dovuti a sentire troppo freddo');
                    $xcrudQualitaSonno->label('ProblemaSentireCaldo','Quanto spesso ha avuto problemi di sonno dovuti a sentire troppo caldo');
                    $xcrudQualitaSonno->label('ProblemaBruttiSogni','Quanto spesso ha avuto problemi di sonno dovuti a fare brutti sogni');
                    $xcrudQualitaSonno->label('ProblemaAvereDolori','Quanto spesso ha avuto problemi di sonno dovuti ad avere dolori');
                    $xcrudQualitaSonno->label('ProblemaAltriDisturbi','Qualche altro problema che può aver disturbato il sonno');
                    $xcrudQualitaSonno->label('ProblemaAltriDisturbiDettaglio','Se sì specificare');
                    $xcrudQualitaSonno->label('NumeroVolteProblemaDormire','E quanto spesso ha avuto problemi a dormire per questo motivo?');
                    $xcrudQualitaSonno->label('QualitaSonnoComplessiva','Come valuta complessivamente la qualità del suo sonno?');
                    $xcrudQualitaSonno->label('FarmaciPerDormire','Quanto spesso ha preso farmaci (prescritti dal medico o meno) per aiutarsi a dormire?');
                    $xcrudQualitaSonno->label('DifficoltaSveglioGuida','Quanto spesso ha avuto difficoltà a rimanere sveglia/o alla guida o nel corso di attività sociali?');
                    $xcrudQualitaSonno->label('EnergieNormaliAttivita','Ha avuto problemi ad avere energie sufficienti per concludere le sue normali attività?');
                    $xcrudQualitaSonno->change_type('TempoAddormentamento','select','',$opzioniTempoAddormentamento);
                    $xcrudQualitaSonno->change_type('OreSonno','select','',$opzioniTempoSonno);
                    $xcrudQualitaSonno->change_type('ProblemaAddormentarsi30Minuti','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('ProblemaSvegliarsiNotte','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('ProblemaAlzarsiBagno','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('ProblemaRespirareBene','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('ProblemaTossireRussareForte','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('ProblemaSentireFreddo','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('ProblemaSentireCaldo','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('ProblemaBruttiSogni','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('ProblemaAvereDolori','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('NumeroVolteProblemaDormire','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('QualitaSonnoComplessiva','select','',$opzioniQualitaSonno);
                    $xcrudQualitaSonno->change_type('FarmaciPerDormire','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('DifficoltaSveglioGuida','select','',$opzioniProblemiGenericiSonno);
                    $xcrudQualitaSonno->change_type('EnergieNormaliAttivita','select','',$opzioniEnergieSufficienti);
                    $xcrudQualitaSonno->fields('IdPaziente', true);
                    //$xcrudQualitaSonno->fields('OraLetto,TempoAddormentamento,OraSveglio,ProblemaAddormentarsi30Minuti,ProblemaSvegliarsiNotte');


                    $xcrudQualitaSonno->hide_button( 'save_new, save_return, return' );
                    $xcrudQualitaSonno->table_name('Qualità del Sonno');
                    $xcrudQualitaSonno->layout(8);
                    $xcrudQualitaSonno->column_class('EnergieNormaliAttivita', 'align-center');
                    
                    $xcrudQualitaSonno->after_update('after_update_general');
                    echo $xcrudQualitaSonno->render('edit', $IdQualitaSonno);
                }
            ?>
            </div>
            <div id="diagnosi" style="border:0px solid blue; width:80%;padding: 2%;margin-left:10%;display:none;">
                
            <?php

                if ($IdQuadroClinico!=NULL)
                {
                    $xcrudDiagnosi->table('QuadroClinico');

                    $xcrudDiagnosi->fields('Diagnosi');

                    $xcrudDiagnosi->hide_button( 'save_new, save_return, return' );

                    $xcrudDiagnosi->table_name('Diagnosi');

                    $xcrudDiagnosi->after_update('after_update_general');
                    echo $xcrudDiagnosi->render('edit', $IdQuadroClinico);
                }
            ?>
            </div> 

        </div>
    </div>

<script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

<script>

var coloreSelezionato="#1f598c";
var coloreNonSelezionato="#333333";


function scegliScheda(scheda)
{
    $('#datiPaziente').hide();
    $('#anamnesi').hide();
    $('#terapiaFarmacologica').hide();
    $('#esamiEmatochimici').hide();
    $('#esamiUrine').hide();
    $('#cardiologo').hide();
    $('#pneumologo').hide();
    $('#medicoDiBase').hide();
    $('#nefrologo').hide();
    $('#nutrizionista').hide();
    $('#diabetologo').hide();
    $('#qualitaVita').hide();
    $('#qualitaSonno').hide();
    $('#precedenti').hide();
    $('#aritmie').hide();
    $('#esamiStrumentali').hide();
    $('#diagnosi').hide();

    $('#divButtonAnagrafica button').css("color",coloreNonSelezionato);
    $('#divButtonAnamnesi button').css("color",coloreNonSelezionato);
    $('#divButtonFarmacologica button').css("color",coloreNonSelezionato);
    $('#divButtonEmatochimici button').css("color",coloreNonSelezionato);
    $('#divButtonUrine button').css("color",coloreNonSelezionato);
    $('#divButtonCardiologo button').css("color",coloreNonSelezionato);
    $('#divButtonPneumologo button').css("color",coloreNonSelezionato);
    $('#divButtonNefrologo button').css("color",coloreNonSelezionato);
    $('#divButtonMedicoBase button').css("color",coloreNonSelezionato);
    $('#divButtonNutrizionista button').css("color",coloreNonSelezionato);
    $('#divButtonDiabetologo button').css("color",coloreNonSelezionato);
    $('#divButtonQualitaVita button').css("color",coloreNonSelezionato);
    $('#divButtonQualitaSonno button').css("color",coloreNonSelezionato);
    $('#divButtonPrecedenti button').css("color",coloreNonSelezionato);
    $('#divButtonAritmie button').css("color",coloreNonSelezionato);
    $('#divButtonEsamiStrumentali button').css("color",coloreNonSelezionato);
    $('#divButtonDiagnosi button').css("color",coloreNonSelezionato);


    if (scheda=="ANAGRAFICA")
    {
        $('#datiPaziente').show();
        $('#divButtonAnagrafica button').css("color",coloreSelezionato);
    }
    else if (scheda=="ANAMNESI")
    {
        $('#anamnesi').show();
        $('#divButtonAnamnesi button').css("color",coloreSelezionato);
    }
    else if (scheda=="FARMACOLOGICA")
    {
        $('#terapiaFarmacologica').show();
        $('#divButtonFarmacologica button').css("color",coloreSelezionato);
    }
    else if (scheda=="EMATOCHIMICI")
    {
        $('#esamiEmatochimici').show();
        $('#divButtonEmatochimici button').css("color",coloreSelezionato);
    }
    else if (scheda=="URINE")
    {
        $('#esamiUrine').show();
        $('#divButtonUrine button').css("color",coloreSelezionato);
    }
    else if (scheda=="CARDIOLOGO")
    {
        $('#cardiologo').show();
        $('#divButtonCardiologo button').css("color",coloreSelezionato);
    }
    else if (scheda=="PNEUMOLOGO")
    {
        $('#pneumologo').show();
        $('#divButtonPneumologo button').css("color",coloreSelezionato);
    }
    else if (scheda=="NEFROLOGO")
    {
        $('#nefrologo').show();
        $('#divButtonNefrologo button').css("color",coloreSelezionato);
    }
    else if (scheda=="MEDICODIBASE")
    {
        $('#medicoDiBase').show();
        $('#divButtonMedicoBase button').css("color",coloreSelezionato);
    }    
    else if (scheda=="NUTRIZIONISTA")
    {
        $('#nutrizionista').show();
        $('#divButtonNutrizionista button').css("color",coloreSelezionato);
    }
    else if (scheda=="DIABETOLOGO")
    {
        $('#diabetologo').show();
        $('#divButtonDiabetologo button').css("color",coloreSelezionato);
    }
    else if (scheda=="QUALITAVITA")
    {
        $('#qualitaVita').show();
        $('#divButtonQualitaVita button').css("color",coloreSelezionato);
     }
    else if (scheda=="QUALITASONNO")
    {
        $('#qualitaSonno').show();
        $('#divButtonQualitaSonno button').css("color",coloreSelezionato);
    }
    else if (scheda=="PRECEDENTI")
    {
        $('#precedenti').show();
        $('#divButtonPrecedenti button').css("color",coloreSelezionato);
    }
    else if (scheda=="ARITMIE")
    {
        $('#aritmie').show();
        $('#divButtonAritmie button').css("color",coloreSelezionato);
    }
    else if (scheda=="ESAMISTRUMENTALI")
    {
        $('#esamiStrumentali').show();
        $('#divButtonEsamiStrumentali button').css("color",coloreSelezionato);
    }
    else if (scheda=="DIAGNOSI")
    {
        $('#diagnosi').show();
        $('#divButtonDiagnosi button').css("color",coloreSelezionato);
    }


    

    
}


function goMain()
{
    window.location.href = 'main.php';
}

function logoff()
{
    window.location.href = 'logout.php';
}

function goStoria()
{
    window.location.href = 'storia.php?id=<? echo $userId ?>';
}

$( window ).resize(function() {
    var larghezza=$(window).width();
    if (larghezza>=1024) $('#contenuto').css("width","1024px");
    else $('#contenuto').css("width","100%");
    var larghFinestra=$('#contenuto').width();
    //console.log(larghFinestra);
    //console.log(larghezza);
    $('#contenuto').css("margin-left",(larghezza-larghFinestra)/2+"px");

    console.log($('#datiPaziente').css("display"));
    console.log($('#medicoDiBase').css("display"));
    console.log($('#aritmie').css("display"));
    //quale scheda è selezionata di default (e quindi quale bottone devo colorare)?
    if ($('#datiPaziente').css("display")=="block") $('#divButtonAnagrafica button').css("color",coloreSelezionato);
    else if ($('#medicoDiBase').css("display")=="block") $('#divButtonMedicoBase button').css("color",coloreSelezionato);
    else if ($('#aritmie').css("display")=="block") $('#divButtonAritmie button').css("color",coloreSelezionato);
    else if ($('#pneumologo').css("display")=="block") $('#divButtonPneumologo button').css("color",coloreSelezionato);
    else if ($('#nefrologo').css("display")=="block") $('#divButtonNefrologo button').css("color",coloreSelezionato);

});

function handlerClick()
{
    console.log( "Handler for .click() called." );
    jQuery(document).off( "xcrudafterrequest" )
    jQuery(document).on("xcrudafterrequest",function(event,container){
        if(Xcrud.current_task == 'save')
        {
            console.log(Xcrud);
        }
    });
    $( "a[data-task='save']" ).click();
    
    setTimeout(function(){
        console.log("reastart handler");
        jQuery(document).on("xcrudafterrequest",function(event,container){
            if(Xcrud.current_task == 'save')
            {
                handlerClick();
            }
        });
    }, 5000);
    

}


jQuery(document).ready(function() { 

    //alert("qui");
    $(window).trigger('resize');
    $('#contenuto').show();
    console.log("start");

/*
    $( "a[data-task='save']" ).click(function() {
      console.log( "qui" );
      //handlerClick();
    });
*/

    jQuery(document).on("xcrudafterrequest",function(event,container){
        if(Xcrud.current_task == 'save')
        {
            //handlerClick();
        }
    });
/*
    $( "a[data-task='save']" ).click(function(target) {
      //stoppo l'handler
      console.log( "Handler for .click() called." );
      $( "a[data-task='save']" ).off( "click" )
      $( "a[data-task='save']" ).click();

      //console.log( target );
    });
*/
    //$("h2:contains('Date Controlli')").hide();

});



//dopo che ha salvato dovrebbe andare alla pagina del nuovo paziente

/*jQuery(document).on("xcrudafterrequest",function(event,container, data){
    console.log(Xcrud);
    if(Xcrud.current_task == 'save')
    {
        //window.location = 'schedaPaziente.php';
    }
});
*/

</script>

</body>
</html>
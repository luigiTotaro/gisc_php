<?php
ob_start(); 
session_name( 'PHPSESSID' );
session_start();
include_once "./query.php";

    //se non loggato ritorno a login
    if (!isset($_SESSION['IdUtente'])){
        header("location: index.php");//redirect
    }else{
        $idUtente = $_SESSION['IdUtente'];
        $nominativo = $_SESSION['Nominativo'];
        $tipologiaUtente = $_SESSION['Tipologia'];
    }

    $userId=-1;
    if (isset($_GET['id']))
    {
        $userId = $_GET['id'];  
    }


    include('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
	$db = Xcrud_db::get_instance();

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Trend Paziente</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
</head>
 
<body style="background-color:#cccccc;">

    <div id="tooltip" style="position:absolute;display:none;border:1px solid #0b2f51;padding:10px;background-color:#145592;opacity: 0.90;z-index: 9999;color:white;"></div> 

    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;display:none;">

        <div style="width:100%;">
            <img src="img/testataGisc.jpg" style="width:100%;">
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="logoff();">Esci</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="nuovoreclutamento();">Nuovo Reclutamento</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goStoria();">Storia Paziente</button>            <div style="clear:both;"></div>
        </div>

        <div style="width:100%;padding:5%;">
            <?php



                $db->query(str_replace("%%idpaziente%%", $userId, $query_trend));
                $result = $db->result();
                //echo '<pre>'; print_r($result); echo '</pre>';

                //costruisco i ticks, che serviranno per l'asse x
                $jsonTicks="[";
                $indice=1;
                //quanti "tick" devo metter? dipende da quanti sono i dati
                if (count($result)<25) $modulo=1;
                else if (count($result)<50) $modulo=2;
                else  $modulo=3;
                foreach ($result as $item) {
                    if ($indice==1) $jsonTicks.="[".$indice.",'Reclutamento'],";
                    else if ($indice==2) $jsonTicks.="[".$indice.",''],";
                    else
                    {
                        if (($indice % $modulo)==0) $jsonTicks.="[".$indice.",'".$item['Data']."'],";
                        else $jsonTicks.="[".$indice.",''],";
                    }
                    $indice++;
                }
                $jsonTicks.="]";
                //echo $jsonTicks;


                $jsonValoriFC="[";
                $indice=1;
                foreach ($result as $item) {
                    if ($item['gFrequenzaCard']!=NULL) $jsonValoriFC.="[".$indice.",".$item['gFrequenzaCard']."],";
                    //else $frequenza=0;
                    //$jsonValoriFC.="['".$item['DataQuadroClinico']."',".$frequenza."],";
                    //$jsonValoriFC.="[".$indice.",".$frequenza."],";
                    $indice++;
                }
                $jsonValoriFC.="]";
                //echo $jsonValoriFC;

                $jsonValoriFE="[";
                $indice=1;
                foreach ($result as $item) {
                    if ($item['Eco2DFE']!=NULL) $jsonValoriFE.="[".$indice.",".$item['Eco2DFE']."],";
                    $indice++;
                }
                $jsonValoriFE.="]";
                //echo $jsonValoriFE;

                $jsonValoriPP="[";
                $indice=1;
                foreach ($result as $item) {
                    if ($item['EcoDopplerPressPolm']!=NULL) $jsonValoriPP.="[".$indice.",".$item['EcoDopplerPressPolm']."],";
                    $indice++;
                }
                $jsonValoriPP.="]";
                //echo $jsonValoriPP;

                $jsonValoriNyha="[";
                $indice=1;
                foreach ($result as $item) {
                    if ($item['ClasseDispneaSforzo']!=NULL)
                        {
                            if ($item['ClasseDispneaSforzo']=="I classe") $jsonValoriNyha.="[".$indice.",1],";
                            if ($item['ClasseDispneaSforzo']=="II classe") $jsonValoriNyha.="[".$indice.",2],";
                            if ($item['ClasseDispneaSforzo']=="III classe") $jsonValoriNyha.="[".$indice.",3],";
                            if ($item['ClasseDispneaSforzo']=="IV classe") $jsonValoriNyha.="[".$indice.",4],";
                            
                        }
                    $indice++;
                }
                $jsonValoriNyha.="]";
                //echo $jsonValoriNyha;

                $jsonValoriCreat="[";
                $indice=1;
                foreach ($result as $item) {
                    if ($item['IngrCreat']!=NULL) $jsonValoriCreat.="[".$indice.",".$item['IngrCreat']."],";
                    $indice++;
                }
                $jsonValoriCreat.="]";
                //echo $jsonValoriCreat;

                $jsonValoriBNP="[";
                $indice=1;
                foreach ($result as $item) {
                    if ($item['IngrBNP']!=NULL) $jsonValoriBNP.="[".$indice.",".$item['IngrBNP']."],";
                    $indice++;
                }
                $jsonValoriBNP.="]";
                //echo $jsonValoriBNP;


                $datiEstesi="[";
                $indice=1;
                foreach ($result as $item) {
                    if ($item['gFrequenzaCard']!=NULL) $freqC=$item['gFrequenzaCard']; else $freqC="Non Rilevato";
                    if ($item['Eco2DFE']!=NULL) $Eco2D=$item['Eco2DFE']."%"; else $Eco2D="Non Rilevato";
                    if ($item['EcoDopplerPressPolm']!=NULL) $PresPolm=$item['EcoDopplerPressPolm']." mmhg"; else $PresPolm="Non Rilevato";
                    if ($item['ClasseDispneaSforzo']!=NULL) $ClasseNyha=$item['ClasseDispneaSforzo']; else $ClasseNyha="Non Rilevato";
                    if ($item['IngrCreat']!=NULL) $Creatin=$item['IngrCreat']." mg/dl"; else $Creatin="Non Rilevato";
                    if ($item['IngrBNP']!=NULL) $Bnp=$item['IngrBNP']." pg/ml"; else $Bnp="Non Rilevato";

                    if ($indice==1) $datiEstesi.="['Reclutamento','".$freqC."','".$Eco2D."','".$PresPolm."','".$ClasseNyha."','".$Creatin."','".$Bnp."'],";
                    else $datiEstesi.="['".$item['DataEstesa']."','".$freqC."','".$Eco2D."','".$PresPolm."','".$ClasseNyha."','".$Creatin."','".$Bnp."'],";
                    $indice++;
                }
                $datiEstesi.="]";
                //echo $datiEstesi;

            ?>    

            <div class="col-md-12" style="margin-top:20px;">
                <div id="chart_frequenzaC" class="chart" style="height:200px;width: 100%;"></div>       
                <div id="chart_FE" class="chart" style="height:200px;width: 100%;margin-top:20px;"></div>       
                <div id="chart_PP" class="chart" style="height:200px;width: 100%;margin-top:20px;"></div>       
                <div id="chart_Nyha" class="chart" style="height:200px;width: 100%;margin-top:20px;"></div>       
                <div id="chart_Creat" class="chart" style="height:200px;width: 100%;margin-top:20px;"></div>       
                <div id="chart_BNP" class="chart" style="height:200px;width: 100%;margin-top:20px;margin-bottom:200px;"></div>       
            </div>
        </div>
    </div>

    <script src="xcrud/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/flot/jquery.flot.min.js"></script>
    <script src="js/flot/jquery.flot.resize.min.js"></script>
    <script src="js/flot/jquery.flot.pie.min.js"></script>
    <script src="js/flot/jquery.flot.symbol.js"></script>
    <script src="js/flot/jquery.flot.stack.min.js"></script>
    <script src="js/flot/jquery.flot.axislabels.js"></script>
    <script src="js/flot/jquery.flot.time.js"></script>
    <script src="js/flot/jquery.flot.crosshair.min.js"></script>
    <script src="js/flot/jquery.flot.categories.min.js" type="text/javascript"></script>

    <script>

    function nuovoreclutamento()
    {
        window.location.href = 'schedaPaziente.php?id=-1';
    }

    function logoff()
    {
        window.location.href = 'logout.php';
    }

    function disegnaTrend()
    {



        var datiEstesi=<? echo $datiEstesi ?>;

        //var mydata=new Array();
        var jsonFC=<? echo $jsonValoriFC ?>;
        //mydata.push(json);
        //data.push(datiEstesi);
        //console.log (data);
        var jsonFE=<? echo $jsonValoriFE ?>;
        var jsonPP=<? echo $jsonValoriPP ?>;
        var jsonNyha=<? echo $jsonValoriNyha ?>;
        var jsonCreat=<? echo $jsonValoriCreat ?>;
        var jsonBnp=<? echo $jsonValoriBNP ?>;


        var dataFC = [{ data: jsonFC, dataExt:datiEstesi, color: "#145592"} ];
        var dataFE = [{ data: jsonFE, dataExt:datiEstesi, color: "#145592"} ];
        var dataPP = [{ data: jsonPP, dataExt:datiEstesi, color: "#145592"} ];
        var dataNyha = [{ data: jsonNyha, dataExt:datiEstesi, color: "#145592"} ];
        var dataCreat = [{ data: jsonCreat, dataExt:datiEstesi, color: "#145592"} ];
        var dataBnp = [{ data: jsonBnp, dataExt:datiEstesi, color: "#145592"} ];

        
        var myticks=new Array();
        var json=<? echo $jsonTicks ?>;
        myticks.push(json);
        console.log (myticks);

        //var datiEstesi=new Array();
        //datiEstesi.push(json);


        var options = {
            series: {
                lines: {
                    show: true,
                    lineWidth: 2,
                    fill: true,
                    fillColor: {
                        colors: [{
                            opacity: 0.05
                        }, {
                            opacity: 0.01
                        }]
                    }
                },
                points: {
                    show: true,
                    radius: 3,
                    lineWidth: 1
                },
                shadowSize: 2
            },
            //colors: ["#d12610"],
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            yaxis: {
                //max: 120,
                //min: 35,
                //axisLabel: "Freq. cardiaca/min",
                axisLabelPadding: 10
            },
            xaxis: {
                ticks: json
            }

        };

        options.yaxis.max=120;
        options.yaxis.min=35;
        options.yaxis.axisLabel="Freq. cardiaca/min";
        var plotFC = $.plot($("#chart_frequenzaC"), dataFC, options);

        options.yaxis.max=70;
        options.yaxis.min=20;
        options.yaxis.axisLabel="F.E. %";
        var plotFE = $.plot($("#chart_FE"), dataFE, options);

        options.yaxis.max=80;
        options.yaxis.min=20;
        options.yaxis.axisLabel="Press. Polm. mmhg";
        var plotFE = $.plot($("#chart_PP"), dataPP, options);
        
        options.yaxis.max=4;
        options.yaxis.min=1;
        options.yaxis.axisLabel="Classe NYHA";
        var plot_Nyha = $.plot($("#chart_Nyha"), dataNyha, options);

        options.yaxis.max=8;
        options.yaxis.min=0;
        options.yaxis.axisLabel="Creatinina sierica mg/dl";
        var plot_Creat = $.plot($("#chart_Creat"), dataCreat, options);

        options.yaxis.max=15000;
        options.yaxis.min=0;
        options.yaxis.axisLabel="Pro-BNP pg/ml";
        var plot_Bnp = $.plot($("#chart_BNP"), dataBnp, options);
 
        $("#chart_frequenzaC").bind("plothover", function (event, pos, item) { plotHover(event, pos, item); });
        $("#chart_FE").bind("plothover", function (event, pos, item) { plotHover(event, pos, item); });
        $("#chart_PP").bind("plothover", function (event, pos, item) { plotHover(event, pos, item); });
        $("#chart_Nyha").bind("plothover", function (event, pos, item) { plotHover(event, pos, item); });
        $("#chart_Creat").bind("plothover", function (event, pos, item) { plotHover(event, pos, item); });
        $("#chart_BNP").bind("plothover", function (event, pos, item) { plotHover(event, pos, item); });

    }
    
    function plotHover(event, pos, item)
    {
         if (item) {
            console.log(item);

            var indice=item.datapoint[0];
            indice--;
            console.log(indice);
            $("#tooltip").html("<b>" + item.series.dataExt[indice][0] + "</b><br><br>Frequenza Cardiaca: " + item.series.dataExt[indice][1] + "<br>F.E.: "+item.series.dataExt[indice][2] + "<br>Pressione Polmonare: "+item.series.dataExt[indice][3] + "<br>Classe NYHA: "+item.series.dataExt[indice][4] + "<br>Creatinina sierica: "+item.series.dataExt[indice][5] + "<br>Pro-BNP: "+item.series.dataExt[indice][6])
                .css({top: item.pageY, left: item.pageX})
                .fadeIn(200);
        } else {
            $("#tooltip").hide();
        }       
    }


    $( window ).resize(function() {
      var larghezza=$(window).width();
      if (larghezza>=1024) $('#contenuto').css("width","1024px");
      else $('#contenuto').css("width","100%");
      var larghFinestra=$('#contenuto').width();
      //console.log(larghFinestra);
      //console.log(larghezza);
      $('#contenuto').css("margin-left",(larghezza-larghFinestra)/2+"px");

    });

    jQuery(document).ready(function() { 

        $(window).trigger('resize');
        $('#contenuto').show();
        disegnaTrend();

    });

    function goStoria()
    {
        window.location.href = 'storia.php?id=<? echo $userId ?>';
    }

    //$('.collapse').collapse()
    </script>


</body>
</html>
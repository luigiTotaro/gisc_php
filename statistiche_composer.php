<?php
ob_start(); 
session_name( 'PHPSESSID' );
session_start();
include_once "./query.php";
error_reporting(E_ERROR | E_PARSE);

    //se non loggato ritorno a login
    if (!isset($_SESSION['IdUtente'])){
        header("location: index.php");//redirect
    }else{
        $idUtente = $_SESSION['IdUtente'];
        $nominativo = $_SESSION['Nominativo'];
        $tipologiaUtente = $_SESSION['Tipologia'];
    }

    $composer=null;
    if (isset($_POST['composer'])) $composer=$_POST['composer'];
    else $composer=null;
    $composerQuery="";
    if (isset($_POST['composerQuery'])) $composerQuery=$_POST['composerQuery'];
    else $composerQuery="";
    

    include_once('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
	$db = Xcrud_db::get_instance();

    echo "*";
    echo $composer."<br>";
    echo "*"."<br>";

    $composerQuery = urldecode($composerQuery);
    echo $composerQuery."<br>";
    echo "*";


?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Statistiche Composer</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
</head>
 
<body style="background-color:#cccccc;">

    <div id="tooltip" style="position:absolute;display:none;border:2px solid #596715;padding:10px;background-color:#7c901e;opacity: 0.90;z-index: 9999;color:white;"></div> 

    <input type="hidden" id="composerFromPhp" value="<?php echo $composer; ?>" />
    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;display:none;">

        <div style="width:100%;">
            <img src="img/testataGisc.jpg" style="width:100%;">
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="logoff();">Esci</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goMain();">Pannello Principale</button>            
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goStatistiche();">Statistiche</button>
            <div style="clear:both;"></div>
        </div>


        <div style="width:100%;padding:5%;">
            <?php

                


                //compongo la lista delle condizioni
                
                if ($fc) $condizioni.= "and CAST(ECGFrequenza AS UNSIGNED)>= 70 ";
                else $condizioni.= "and CAST(ECGFrequenza AS UNSIGNED)< 70 ";

                $query = $query_pagina_statistiche_Aritmie_totale;
                $db->query($query);
                $resultTemp = $db->result();
                $resultNew['NumTotaleGenerale']=$resultTemp[0]["num"];

                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_generale);
                $db->query($query);
                $resultTemp = $db->result();

                foreach ($resultTemp as $item)
                {         
                    if ($item["sesso"]=="M")
                    {
                        $resultNew['NumM']=$item["num"];
                        $resultNew['SumEtaM']=$item["sum"];
                        $resultNew['EtaMinM']=$item["min"];
                        $resultNew['EtaMaxM']=$item["max"];
                    }
                    else
                    {
                        $resultNew['NumF']=$item["num"];
                        $resultNew['SumEtaF']=$item["sum"];
                        $resultNew['EtaMinF']=$item["min"];
                        $resultNew['EtaMaxF']=$item["max"];
                    }
                }
                $resultNew['NumTot']=$resultNew['NumM']+$resultNew['NumF'];
                $resultNew['SumEta']=$resultNew['SumEtaM']+$resultNew['SumEtaF'];
                
                if ($resultNew['EtaMinM']<$resultNew['EtaMinF']) $resultNew['EtaMin']=$resultNew['EtaMinM'];
                    else $resultNew['EtaMin']=$resultNew['EtaMinF'];
                if ($resultNew['EtaMaxM']>$resultNew['EtaMaxF']) $resultNew['EtaMax']=$resultNew['EtaMaxM'];
                    else $resultNew['EtaMax']=$resultNew['EtaMaxF'];

                
                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_fumo);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultNew,'Fumo');

                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_colesterolo);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultNew,'NumCasiColesterolo');

                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_anemia);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultNew,'NumCasiAnemia');

                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_ipert);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultNew,'NumCasiIpert');

                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_diabete);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultNew,'NumCasiDiabete');

                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_bpco);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultNew,'NumCasiBPCO');

                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_insufRenal);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultNew,'NumCasiInsuffRenale');

                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_dead);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultNew,'NumDeath');

                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_ischemica);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultNew,'NumCasiIschemica');

                $query = str_replace ( "@Condizioni" , $composerQuery , $query_pagina_statistiche_Aritmie_ima);
                $db->query($query);
                $resultTemp = $db->result();
                estraiDati($resultTemp,$resultNew,'NumCasiIMA');

                function estraiDati($resultQuery,&$resultNew,$indice)
                {
                    foreach ($resultQuery as $item)
                    {
                        if ($item["sesso"]=="M")
                        {
                            $resultNew[$indice.'M']=$item["num"];
                        }
                        else
                        {
                            $resultNew[$indice.'F']=$item["num"];
                        }
                    }
                    $resultNew[$indice]=$resultNew[$indice.'M']+$resultNew[$indice.'F'];
                }


                //prendo la lista dei pazienti che corrispondono a quelle condizioni
                $sel="select P.IdPaziente, P.CodiceFiscale, P.Cognome, P.Nome, DATE_FORMAT(P.DataRegistrazione, '%d-%m-%Y') as DataRegistrazioneFormat, Q.IdQuadroClinico, Q.DataQuadroClinico FROM QuadroClinico Q INNER JOIN paziente P on Q.IdPaziente = P.IdPaziente INNER JOIN FattoreRischio_eziologia FR on Q.IdPaziente = FR.IdPaziente WHERE gtipo='RECLUTAMENTO' ".$composerQuery;
                //$sel="select IdQuadroClinico, DataQuadroClinico from QuadroClinico where  gTipo='RECLUTAMENTO' ";
                //echo $sel;


            ?>    

            <div class="col-md-12">

                <div class="panel-group" id="accordionCondizioni" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default" style="margin-bottom:10px;">
                    <div class="panel-heading" role="tab" id="headingCondizioni">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordionCondizioni" href="#collapseCondizioni" aria-expanded="true" aria-controls="collapseCondizioni">
                          Composer
                        </a>
                      </h4>
                    </div>
                    <div id="collapseCondizioni" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingCondizioni">
                      <div class="panel-body">
                            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="addComposer();">Aggungi</button>
                            
                            <form name='dataStatistiche' action='statistiche_composer.php' method='POST' >
                                <input name="composer" id="composer" style="" class="" ></input>
                                <input name="composerQuery" id="composerQuery" style="" class="" ></input>

                                <div class="row" id="composerContainer">

                                    <!--
                                    <div class="col-md-3" style="text-align: right;">
                                        <select id="categoria"></select>
                                    </div>

                                    <div class="col-md-3" style="text-align: right;">
                                        <select id="item"></select>
                                    </div>

                                    <div class="col-md-1" style="text-align: right;">
                                        <select id="operando"></select>
                                    </div>

                                    <div class="col-md-3" style="text-align: right;">
                                        <select id="value"></select>
                                        <input id="valueInput" style="display:none"></input>
                                    </div>
                                    -->

                                </div> 
                            </form>
                          <button class="btn btn-primary xcrud-action" style="padding-left:30px;padding-right:30px;margin-top:15px;margin-right:5px;float: right;" onClick="cerca();">Cerca</button>   
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel-group" id="accordionRisultati" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingRisultati">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordionRisultati" href="#collapseRisultati" aria-expanded="true" aria-controls="collapseRisultati">
                          Risultati
                        </a>
                      </h4>
                    </div>
                    <div id="collapseRisultati" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingRisultati">
                      <div class="panel-body">
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    Pazienti: <? echo $resultNew['NumTot'] ?><br> 
                                    (<? echo number_format((float)($resultNew['NumTot']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)<br>
                                    M: <? echo $resultNew['NumM'] ?> (<? echo number_format((float)($resultNew['NumM']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    F: <? echo $resultNew['NumF'] ?> (<? echo number_format((float)($resultNew['NumF']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    <?
                                        if ($resultNew['NumTot']>0)
                                        {
                                    ?>
                                    Deaths: <? echo $resultNew['NumDeath'] ?> (<? echo number_format((float)($resultNew['NumDeath']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    M: <? echo $resultNew['NumDeathM'] ?><br>
                                    F: <? echo $resultNew['NumDeathF'] ?>
                                    <?
                                        }
                                    ?>
                                </div>
                            </div>
                            <?
                                if ($resultNew['NumTot']>0)
                                {
                            ?>
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    Età media: <? echo number_format((float)($resultNew['SumEta']/$resultNew['NumTot']), 0, '.', '') ?><br> 
                                    Età media M: <? echo number_format((float)($resultNew['SumEtaM']/$resultNew['NumM']), 0, '.', '') ?><br> 
                                    Età media F: <? echo number_format((float)($resultNew['SumEtaF']/$resultNew['NumF']), 0, '.', '') ?>
                                </div>
                            </div>
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    Età min: <? echo $resultNew['EtaMin'] ?><br> 
                                    Età min M: <? echo $resultNew['EtaMinM'] ?><br> 
                                    Età min F: <? echo $resultNew['EtaMinF'] ?>
                                </div>
                            </div>
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    Età max: <? echo $resultNew['EtaMax'] ?><br> 
                                    Età max M: <? echo $resultNew['EtaMaxM'] ?><br> 
                                    Età max F: <? echo $resultNew['EtaMaxF'] ?>
                                </div>
                            </div>
                            <?
                                }
                            ?>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    <p style="width:100%;background-color:#428bca;color:#ffffff;text-align: center;padding: 2px;">Fumo</p>
                                    Fumatori: <? echo $resultNew['Fumo'] ?><br> 
                                    (<? echo number_format((float)($resultNew['Fumo']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)<br>
                                    M: <? echo $resultNew['FumoM'] ?> (<? echo number_format((float)($resultNew['FumoM']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    F: <? echo $resultNew['FumoF'] ?> (<? echo number_format((float)($resultNew['FumoF']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                </div>
                            </div>
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    <p style="width:100%;background-color:#428bca;color:#ffffff;text-align: center;padding: 2px;">Coronarop. Ischemica</p>
                                    Pazienti: <? echo $resultNew['NumCasiIschemica'] ?><br> 
                                    (<? echo number_format((float)($resultNew['NumCasiIschemica']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)<br>
                                    M: <? echo $resultNew['NumCasiIschemicaM'] ?> (<? echo number_format((float)($resultNew['NumCasiIschemicaM']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    F: <? echo $resultNew['NumCasiIschemicaF'] ?> (<? echo number_format((float)($resultNew['NumCasiIschemicaF']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                </div>
                            </div>
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    <p style="width:100%;background-color:#428bca;color:#ffffff;text-align: center;padding: 2px;">Colesterolo (LDL>100)</p>
                                    Pazienti: <? echo $resultNew['NumCasiColesterolo'] ?><br> 
                                    (<? echo number_format((float)($resultNew['NumCasiColesterolo']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)<br>
                                    M: <? echo $resultNew['NumCasiColesteroloM'] ?> (<? echo number_format((float)($resultNew['NumCasiColesteroloM']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    F: <? echo $resultNew['NumCasiColesteroloF'] ?> (<? echo number_format((float)($resultNew['NumCasiColesteroloF']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                </div>
                            </div>
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    <p style="width:100%;background-color:#428bca;color:#ffffff;text-align: center;padding: 2px;">Anemia</p>
                                    Pazienti: <? echo $resultNew['NumCasiAnemia'] ?><br> 
                                    (<? echo number_format((float)($resultNew['NumCasiAnemia']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)<br>
                                    M: <? echo $resultNew['NumCasiAnemiaM'] ?> (<? echo number_format((float)($resultNew['NumCasiAnemiaM']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    F: <? echo $resultNew['NumCasiAnemiaF'] ?> (<? echo number_format((float)($resultNew['NumCasiAnemiaF']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    <p style="width:100%;background-color:#428bca;color:#ffffff;text-align: center;padding: 2px;">Ipert. Arteriosa</p>
                                    Pazienti: <? echo $resultNew['NumCasiIpert'] ?><br> 
                                    (<? echo number_format((float)($resultNew['NumCasiIpert']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)<br>
                                    M: <? echo $resultNew['NumCasiIpertM'] ?> (<? echo number_format((float)($resultNew['NumCasiIpertM']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    F: <? echo $resultNew['NumCasiIpertF'] ?> (<? echo number_format((float)($resultNew['NumCasiIpertF']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                </div>
                            </div>
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    <p style="width:100%;background-color:#428bca;color:#ffffff;text-align: center;padding: 2px;">Diabete</p>
                                    Pazienti: <? echo $resultNew['NumCasiDiabete'] ?><br> 
                                    (<? echo number_format((float)($resultNew['NumCasiDiabete']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)<br>
                                    M: <? echo $resultNew['NumCasiDiabeteM'] ?> (<? echo number_format((float)($resultNew['NumCasiDiabeteM']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    F: <? echo $resultNew['NumCasiDiabeteF'] ?> (<? echo number_format((float)($resultNew['NumCasiDiabeteF']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                </div>
                            </div>
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    <p style="width:100%;background-color:#428bca;color:#ffffff;text-align: center;padding: 2px;">IMA</p>
                                     Pazienti: <? echo $resultNew['NumCasiIMA'] ?><br> 
                                    (<? echo number_format((float)($resultNew['NumCasiIMA']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)<br>
                                    M: <? echo $resultNew['NumCasiIMAM'] ?> (<? echo number_format((float)($resultNew['NumCasiIMAM']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    F: <? echo $resultNew['NumCasiIMAF'] ?> (<? echo number_format((float)($resultNew['NumCasiIMAF']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                </div>
                            </div>
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    <p style="width:100%;background-color:#428bca;color:#ffffff;text-align: center;padding: 2px;">BPCO</p>
                                    Pazienti: <? echo $resultNew['NumCasiBPCO'] ?><br> 
                                    (<? echo number_format((float)($resultNew['NumCasiBPCO']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)<br>
                                    M: <? echo $resultNew['NumCasiBPCOM'] ?> (<? echo number_format((float)($resultNew['NumCasiBPCOM']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    F: <? echo $resultNew['NumCasiBPCOF'] ?> (<? echo number_format((float)($resultNew['NumCasiBPCOF']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-bottom: 10px;">
                            <div class="col-md-3" >
                                <div style="border:1px solid #cccccc;padding: 10px;">
                                    <p style="width:100%;background-color:#428bca;color:#ffffff;text-align: center;padding: 2px;">Insuff. Renale</p>
                                    Pazienti: <? echo $resultNew['NumCasiInsuffRenale'] ?><br> 
                                    (<? echo number_format((float)($resultNew['NumCasiInsuffRenale']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>% sul tot.)<br>
                                    M: <? echo $resultNew['NumCasiInsuffRenaleM'] ?> (<? echo number_format((float)($resultNew['NumCasiInsuffRenaleM']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                    F: <? echo $resultNew['NumCasiInsuffRenaleF'] ?> (<? echo number_format((float)($resultNew['NumCasiInsuffRenaleF']/$resultNew['NumTotaleGenerale']*100), 2, '.', '') ?>%)<br>
                                </div>
                            </div>


                      </div>
                    </div>
                  </div>
                </div>
                <?
                if ($resultNew['NumTot']==0)
                {
                    echo "<div style='margin-top:10px;color:red;'>Nessun paziente corrispondente ai criteri indicati</div>";
                }
                    $xcrud->query($sel);
                    $xcrud->button('storia.php?id={idPaziente}','Seleziona','glyphicon glyphicon-th-list');
                    $xcrud->button('schedaPaziente.php?id={idPaziente}&type=RECL','Modifica','glyphicon glyphicon-pencil');
                    $xcrud->column_name('DataRegistrazioneFormat','Data Reclutamento');
                    $xcrud->columns("Cognome, Nome, DataRegistrazioneFormat ");
                    $xcrud->unset_csv( true );
                    $xcrud->unset_print( true );
                    $xcrud->table_name('Lista Pazienti');
                    echo $xcrud->render();
                    
                

                ?>

            </div>


            <?php
    
            ?>  
            <div style="clear:both;"></div>




        </div>
    </div>


    <!-- <script src="xcrud/plugins/jquery.min.js" type="text/javascript"></script> -->
    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/flot/jquery.flot.min.js"></script>
    <script src="js/flot/jquery.flot.resize.min.js"></script>
    <script src="js/flot/jquery.flot.pie.min.js"></script>
    <script src="js/flot/jquery.flot.symbol.js"></script>
    <script src="js/flot/jquery.flot.stack.min.js"></script>
    <script src="js/flot/jquery.flot.axislabels.js"></script>
    <script src="js/flot/jquery.flot.time.js"></script>
    <script src="js/flot/jquery.flot.crosshair.min.js"></script>
    <script src="js/flot/jquery.flot.categories.min.js" type="text/javascript"></script>



    <script>

    var composerNumber=0;
    var categorie=new Array();
    var items=new Array();

    var item = new Object();
    item.Id = 1;
    item.Nome = 'Fumo';
    item.Confronto = '=';
    item.Valori = 'Si,No,Ex Fumatore';
    item.ValoriQuery = "'Si','No','Ex'";
    item.Campo = 'AND FR.Fumo';
    items.push(item);

    item = new Object();
    item.Id = 2;
    item.Nome = 'BPCO';
    item.Confronto = '=';
    item.Valori = 'Si,No';
    item.ValoriQuery = '1,0';
    item.Campo = 'AND FR.BPCO';
    items.push(item);

    var categoria = new Object();
    categoria.Id = 1;
    categoria.Nome = 'Anamnesi, Rischio, Eziologia';
    categoria.Items = items;
    categorie.push(categoria);
    

    items=new Array();
    item = new Object();
    item.Id = 1;
    item.Nome = 'Classe NYHA';
    item.Confronto = '=';
    item.Valori = 'Classe I,Classe II,Classe III,Classe IV';
    item.ValoriQuery = "'I classe','II classe','III classe','IV Classe'";
    item.Campo = 'AND Q.ClasseDispneaSforzo';
    items.push(item);

    item = new Object();
    item.Id = 2;
    item.Nome = 'Pressione arteriosa sistolica';
    item.Confronto = '<,<=,=,>,>=';
    item.Valori = '';
    item.ValoriQuery = '';
    item.Campo = 'AND Q.gPASistolica';
    items.push(item);

    categoria = new Object();
    categoria.Id = 2;
    categoria.Nome = 'Quadro Clinico';
    categoria.Items = items;
    categorie.push(categoria);








    function addComposer()
    {
        var html='<div class="col-md-3" style="text-align: right;"><select id="categoria_'+composerNumber+'"></select></div>';
        html+='<div class="col-md-3" style="text-align: right;"><select id="item_'+composerNumber+'"></select></div>';
        html+='<div class="col-md-1" style="text-align: right;"><select id="operando_'+composerNumber+'"></select></div>';
        html+='<div class="col-md-3" style="text-align: right;"><select id="value_'+composerNumber+'"></select><input id="valueInput_'+composerNumber+'" style="display:none"></input></div>';
        $('#composerContainer').append( html );


        for (var key in categorie)
        {
            
            $('#categoria_'+composerNumber).append($('<option>', {
                value: categorie[key]['Id'],
                text: categorie[key]['Nome']
            }));
        }
        

        $( '#categoria_'+composerNumber ).change(function() {
            
            categoriaChange(composerNumber);
        });


        $( '#item_'+composerNumber ).change(function() {
            itemChange(composerNumber);
        });


        //actualComposer=composerNumber;
        categoriaChange(composerNumber);

        composerNumber++;

    }




    function nuovoreclutamento()
    {
        window.location.href = 'schedaPaziente.php?id=-1';
    }

    function logoff()
    {
        window.location.href = 'logout.php';
    }

    function cerca()
    {
        //metto nel form i dati che mi servono
        var queryData=new Object();
        queryData.idCat=parseInt($( "#categoria option:selected" ).val());
        queryData.idItem=parseInt($( "#item option:selected" ).val());
        queryData.operando=$( "#operando option:selected" ).val();
        if ($('#valueInput').is(":visible"))
        {
            queryData.value=$('#valueInput').val();
        }
        else
        {
            queryData.value=$( "#value option:selected" ).val();
        }

        var queryDataJson= JSON.stringify(queryData);
        $('#composer').val(encodeURI(queryDataJson));
        
        var queryPhp=getItemCampo(queryData.idCat,queryData.idItem)+queryData.operando+queryData.value;
        $('#composerQuery').val(encodeURI(queryPhp));




        //ricarico la pagina mandando in post i dati che servono
        document.dataStatistiche.submit();
    }
    
    function goStatistiche()
    {
        window.location.href = 'statistiche.php';
    }  



    function getItemCampo(idCat,idItem)
    {
        for (var key in categorie)
        {
            if (categorie[key]['Id']==idCat)
            {
                for (var key2 in categorie[key]['Items'])
                {
                    
                    if (categorie[key]['Items'][key2]['Id']==idItem)
                    {
                        return categorie[key]['Items'][key2]['Campo'];
                    }
                }                
            }
        }

    }



    $( window ).resize(function() {
      var larghezza=$(window).width();
      if (larghezza>=1024) $('#contenuto').css("width","1024px");
      else $('#contenuto').css("width","100%");
      var larghFinestra=$('#contenuto').width();
      //console.log(larghFinestra);
      //console.log(larghezza);
      $('#contenuto').css("margin-left",(larghezza-larghFinestra)/2+"px");

    });

    function goMain()
    {
        window.location.href = 'main.php';
    }    

    jQuery(document).ready(function() { 

        $(window).trigger('resize');
        $('#contenuto').show();
        //disegnaStatistiche();

        console.log(categorie);


        //se esiste una scelta, la re-inserisco
        var composerPhp=$('#composerFromPhp').val();   
        if (composerPhp!="")
        {
            composerPhp=decodeURI(composerPhp);  
            composerJson= JSON.parse(composerPhp);
            console.log(composerJson);
            $( "#categoria" ).val(composerJson.idCat);
            categoriaChange();
            $( "#item" ).val(composerJson.idItem);
            itemChange();
            $( "#operando" ).val(composerJson.operando);
            if ($('#valueInput').is(":visible"))
            {
                $('#valueInput').val(composerJson.value);
            }
            else
            {
                $("#value").val(composerJson.value);
            }            


        }


    });


    $( "[id^=categoria]" ).change(function() {
        
        categoriaChange();
    });


    $( "[id^=item]" ).change(function() {
        itemChange();
    });


    function categoriaChange(actualComposer)
    {
        $('#item_'+actualComposer).find('option').remove().end();
        //quale categoria ha selezionato?
        var categoria=parseInt($( "#categoria_"+actualComposer+" option:selected" ).val());

        for (var key in categorie)
        {
            if (categorie[key]['Id']==categoria)
            {
                for (var key2 in categorie[key]['Items'])
                {
                    $('#item_'+actualComposer).append($('<option>', {
                        value: categorie[key]['Items'][key2]['Id'],
                        text: categorie[key]['Items'][key2]['Nome']
                    }));

                }                
            }
        }
        itemChange(actualComposer);
    }

    function itemChange(actualComposer)
    {
        $('#operando_'+actualComposer).find('option').remove().end();
        $('#value_'+actualComposer).find('option').remove().end();
        $('#valueInput_'+actualComposer).val('');
        //quale item ha selezionato?
        var categoria=parseInt($( "#categoria_"+actualComposer+" option:selected" ).val());
        var item=parseInt($( "#item_"+actualComposer+" option:selected" ).val());

        for (var key in categorie)
        {
            if (categorie[key]['Id']==categoria)
            {
                for (var key2 in categorie[key]['Items'])
                {
                    
                    if (categorie[key]['Items'][key2]['Id']==item)
                    {
                        var operandi = categorie[key]['Items'][key2]['Confronto'].split(",");
                        for (var i=0;i<operandi.length;i++)
                        {
                            $('#operando_'+actualComposer).append($('<option>', {
                                value: operandi[i],
                                text: operandi[i]
                            }));
                        }

                        //ha dei valori da inserire da scegliere?
                        if (categorie[key]['Items'][key2]['Valori']=='')
                        {
                            $('#value_'+actualComposer).hide();
                            $('#valueInput_'+actualComposer).show();
                        }
                        else
                        {
                            var valori = categorie[key]['Items'][key2]['Valori'].split(",");
                            var valoriQuery = categorie[key]['Items'][key2]['ValoriQuery'].split(",");
                            for (var i=0;i<valori.length;i++)
                            {
                                $('#value_'+actualComposer).append($('<option>', {
                                    value: valoriQuery[i],
                                    text: valori[i]
                                }));
                            }
                            $('#value_'+actualComposer).show();
                            $('#valueInput_'+actualComposer).hide();
                        }
                    }
                }                
            }
        }
    }


    $( ".xcrud-row" ).click(function(event) {
      var cella = $(event.currentTarget.childNodes[4]);
      var span=$(cella[0].childNodes[0]);
      var button=$(span[0].childNodes[0])
      //console.log(button[0].href);
      window.location.href = button[0].href;
    });

    //$('.collapse').collapse()
    </script>


</body>
</html>
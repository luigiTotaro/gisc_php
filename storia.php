<?php
ob_start(); 
session_name( 'PHPSESSID' );
session_start();
include_once "./query.php";

    //se non loggato ritorno a login
    if (!isset($_SESSION['IdUtente'])){
        header("location: index.php");//redirect
    }else{
        $idUtente = $_SESSION['IdUtente'];
        $nominativo = $_SESSION['Nominativo'];
        $tipologiaUtente = $_SESSION['Tipologia'];
    }


	$userId = $_GET['id'];

    include('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
    //$xcrudTemp = Xcrud::get_instance();
	$db = Xcrud_db::get_instance();

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Scheda Paziente</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
    <script defer src="js/fontawesome-all.min.js"></script>
</head>
 
<body style="background-color:#cccccc;">

    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;display:none;">

        <div style="width:100%;">
            <img src="img/testataGisc.jpg" style="width:100%;">
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="logoff();">Esci</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="nuovoreclutamento();">Nuovo Reclutamento</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goMain();">Pannello Principale</button>
            <div style="clear:both;"></div>
        </div>
        <div style="width:100%;padding:5%;">
            <?php

                //DATI PAZIENTE	
                $sel = "SELECT *, DATE_FORMAT(DataRegistrazione, '%d-%m-%Y') as DataRegistrazioneFormat, DATE_FORMAT(gdead, '%d-%m-%Y') as DataDecesso FROM paziente, utente WHERE paziente.IdUtente=utente.IdUtente and IdPaziente =".$userId;
                $db->query($sel);
                $result = $db->result();
            	$resultPaziente = $result[0];

                //DATI RECLUTAMENTO
                $sel = "Select Diagnosi, IdQuadroClinico from QuadroClinico where idPaziente=".$userId." and gTipo='RECLUTAMENTO'";
                $db->query($sel);
                $result = $db->result();
                if (count($result)==0)
                {
                    $resultDiagnosi="";
                }
                else
                {
                    $resultDiagnosi = $result[0]["Diagnosi"];    
                    $idQuadroReclutamento = $result[0]["IdQuadroClinico"];    
                }

                //DATI CLINICI
                $sel = "SELECT *, DATE_FORMAT(DataQuadroClinico, '%d-%m-%Y') as DataQuadroClinicoFormat FROM QuadroClinico qc, Utente u WHERE qc.IdPaziente=".$userId." and qc.IdUtente=u.IdUtente and (qc.gTipo = 'RICOVERO' or qc.gTipo IS NULL) order by qc.DataQuadroClinico Desc";
                $db->query($sel);
                $resultClinico = $db->result();

                /*
                $sel = "";
                $sel .= '(SELECT DATE_FORMAT(gdead, "%d-%m-%Y") as Data, "Decesso" as Evento, "" as "Eseguito da", "0" as Id FROM paziente, utente WHERE paziente.IdUtente=utente.IdUtente and IdPaziente ='.$userId.')';
                $sel .= ' UNION ';
                $sel .= '(SELECT DATE_FORMAT(DataQuadroClinico, "%d-%m-%Y") as Data, IF(gTipo="RICOVERO", "Ricovero", CASE ';
                $sel .= ' WHEN tipo_utente.Nome="Cardiologo Ambulatoriale" THEN "Controllo Specialistico Cardiologico"';
                $sel .= ' WHEN tipo_utente.Nome="Medico di Medicina Generale" THEN "Controllo MMG"';
                $sel .= ' WHEN tipo_utente.Nome="Admin" THEN "Controllo Amministratore"';
                $sel .= ' WHEN tipo_utente.Nome="Ospedaliero" THEN "Controllo Ospedaliero"';
                $sel .= ' ELSE tipo_utente.Nome';
                $sel .= ' END) "Evento", Nominativo as "Eseguito da", IdQuadroClinico Id FROM QuadroClinico, Utente, tipo_utente WHERE QuadroClinico.IdPaziente='.$userId.' and QuadroClinico.IdUtente=Utente.IdUtente and utente.IdTipoUtente=tipo_utente.IdTipoUtente and (QuadroClinico.gTipo = "RICOVERO" or QuadroClinico.gTipo IS NULL) order by QuadroClinico.DataQuadroClinico Desc)';
                echo $sel;
               */

                $sel = "";
                $sel .= 'SELECT * FROM (';
                $sel .= '(SELECT DATE_FORMAT(DataQuadroClinico, "%d-%m-%Y") as Data, DataQuadroClinico as dateOrder, IF(gTipo="RICOVERO", "Ricovero", CASE ';
                $sel .= '   WHEN tipo_utente.Nome="Cardiologo Ambulatoriale" THEN "Controllo Specialistico Cardiologico"';
                $sel .= '   WHEN tipo_utente.Nome="Medico di Medicina Generale" THEN "Controllo MMG"';
                $sel .= '   WHEN tipo_utente.Nome="Admin" THEN "Controllo Amministratore"';
                $sel .= '   WHEN tipo_utente.Nome="Ospedaliero" THEN "Controllo Ospedaliero"';
                $sel .= '   WHEN tipo_utente.Nome="Specialista Pneumologo" THEN "Controllo Specialistico Pneumologico"';
                $sel .= '   WHEN tipo_utente.Nome="Specialista Nefrologo" THEN "Controllo Specialistico Nefrologico"';
                $sel .= '   WHEN tipo_utente.Nome="Nutrizionista" THEN "Controllo Nutrizionista"';
                $sel .= '   ELSE tipo_utente.Nome';
                $sel .= '   END) "Evento", Nominativo as "Eseguito da", IdQuadroClinico Id FROM QuadroClinico, Utente, tipo_utente WHERE QuadroClinico.IdPaziente='.$userId.' and QuadroClinico.IdUtente=Utente.IdUtente and utente.IdTipoUtente=tipo_utente.IdTipoUtente and (QuadroClinico.gTipo = "RICOVERO" or QuadroClinico.gTipo IS NULL))';
                $sel .= 'UNION';
                $sel .= '(SELECT DATE_FORMAT(Data, "%d-%m-%Y") as Data, Data as dateOrder, "Ricovero non cardiologico" as Evento, "" as "Eseguito da", IdRicovero Id FROM ricoverononcardiologico WHERE IdPaziente='.$userId.')) as tabella order by tabella.dateOrder DESC';

/*
                //Query per tabella
                $sel = 'SELECT DATE_FORMAT(DataQuadroClinico, "%d-%m-%Y") as Data, IF(gTipo="RICOVERO", "Ricovero", CASE 
                            WHEN tipo_utente.Nome="Cardiologo Ambulatoriale" THEN "Controllo Specialistico Cardiologico"
                            WHEN tipo_utente.Nome="Medico di Medicina Generale" THEN "Controllo MMG"
                            WHEN tipo_utente.Nome="Admin" THEN "Controllo Amministratore"
                            WHEN tipo_utente.Nome="Ospedaliero" THEN "Controllo Ospedaliero"
                            WHEN tipo_utente.Nome="Specialista Pneumologo" THEN "Controllo Specialistico Pneumologico"
                            WHEN tipo_utente.Nome="Specialista Nefrologo" THEN "Controllo Specialistico Nefrologico"
                            WHEN tipo_utente.Nome="Nutrizionista" THEN "Controllo Nutrizionista"
                            
                            ELSE tipo_utente.Nome
                            END) "Evento", Nominativo as "Eseguito da", IdQuadroClinico Id FROM QuadroClinico, Utente, tipo_utente WHERE QuadroClinico.IdPaziente='.$userId.' and QuadroClinico.IdUtente=Utente.IdUtente and utente.IdTipoUtente=tipo_utente.IdTipoUtente and (QuadroClinico.gTipo = "RICOVERO" or QuadroClinico.gTipo IS NULL) order by QuadroClinico.DataQuadroClinico Desc';
*/
                //echo $sel;
                //faccio la query per vedere quanti risultati produce, per non visualizzare la tabella, in caso
                
                $db->query($sel);
                $resultTabella = $db->result();
                if (count($resultTabella)==0)
                {
                    $divTabellaStyle="display:none;";
                    $divNoResultStyle="";
                }
                else
                {
                    $divTabellaStyle="";
                    $divNoResultStyle="display:none;";
                }


                $tabellaStoria = "" ;

                $nomeCognome = $resultPaziente["Cognome"].' '.$resultPaziente["Nome"];   
            ?>    
            <h3 class="titolo">Paziente: <? echo $nomeCognome ?></h3>
            <h4 >Reclutato il <? echo $resultPaziente["DataRegistrazioneFormat"] ?> con diagnosi:<br><? echo $resultDiagnosi ?></h4>
            <?php

                //morte
                $dettaglioDead="";
                $pulsanteDead="";
                $cursorDead="";
                if ($resultPaziente["gdead"]!="")
                {
                    echo '<h4 style="color:#FF0000;margin-top:20px;">Attenzione, il paziente risulta \'dead\' pertanto non è più possibile registrare controlli o ricoveri</h4>';
                    $dettaglioDead= '<div class="row" style="margin-top:10px;"><h4 class="col-md-8">Morte: '.$resultPaziente["DataDecesso"].'</h4><div class="col-md-4"><span><button class="btn btn-danger xcrud-action" style="width:100%;" onClick="dead();">Vai alla scheda Decesso<i style="margin-left:10px;" class="far fa-calendar-times"></i></button></span></div></div>';
                    //$tabellaStoria.= "<div style='width:340px; text-align:center;background-color:#ff0000; color:#ffffff;margin-left:10px; height:35px;border:1px #000000 solid;'><b style='margin-top:10px;'><a style='color:#ffffff; text-decoration:none;' href='dead.aspx?idPaziente=".$userId."'>DEAD (".$resultPaziente["DataDecesso"].")</a></b></div>";
                    $pulsanteDead="disabled";
                    $cursorDead="cursor: not-allowed;";
                }
                
                //Reclutamento...
                $dettaglioReclutamento= '<div class="row" style="margin-top:20px;"><h4 class="col-md-8">Reclutamento: '.$resultPaziente["DataRegistrazioneFormat"].' - Eseguito da <b>'.$resultPaziente["Nominativo"].'</b></h4><div class="col-md-4"><span><button class="btn btn-grey xcrud-action " style="width:100%;" onClick="reclutamento();">Vai alla scheda Reclutamento<i style="margin-left:10px;" class="far fa-calendar-alt"></i></button></span></div></div>';
                
                
                //echo $dettaglioReclutamento;
                //echo $dettaglioDead;


                //button bar
            ?>    
            <hr>
            <div class="row" style="margin-top:20px;">
                <div class="col-md-4"><span><button class="btn btn-grey xcrud-action font-x-large" style="width:100%;" onClick="nuovoControllo();">Nuovo Controllo<i style="margin-left:10px;" class="fas fa-stethoscope"></i></button></span></div>
                <div class="col-md-4"><span><button class="btn btn-grey xcrud-action font-x-large" style="width:100%;" onClick="nuovoRicovero();">Nuovo Ricovero<i style="margin-left:10px;" class="fas fa-heartbeat"></i></button></span></div>
                <div class="col-md-4" style="<? echo $cursorDead ?>"><span><button class="btn btn-danger xcrud-action font-x-large" style="width:100%;" <? echo $pulsanteDead ?> onClick="dead();">Decesso<i style="margin-left:10px;" class="far fa-calendar-times"></i></button></span></div>
            </div>
            <div class="row" style="margin-top:10px;">
                <div class="col-md-8"><span><button class="btn btn-grey xcrud-action font-x-large" style="width:100%;" onClick="nuovoRicoveroNonCardio();">Nuovo Ricovero non cardiologico<i style="margin-left:10px;" class="fas fa-medkit"></i></button></span></div>
                <div class="col-md-4"><span><button class="btn btn-success xcrud-action font-x-large" style="width:100%;" onClick="trend();">Trend<i style="margin-left:10px;" class="fas fa-chart-line"></i></button></span></div>
            </div>
            <hr>
            <?php    
                echo $dettaglioReclutamento;
                echo $dettaglioDead;
            ?>
            <div style="<? echo $divTabellaStyle ?>">
            <?php    
                
                $xcrud->query($sel);
                
                $xcrud->button('sceltaStoriaItem.php?id={Id}&tipo={Evento}','Modifica','glyphicon glyphicon-pencil');
                //$xcrud->button('schedaPaziente.php?idQuadro={Id}','Modifica','glyphicon glyphicon-pencil');
                $xcrud->columns("Data, Evento, Eseguito da");
                $xcrud->unset_csv( true );
                $xcrud->unset_print( true );
                $xcrud->table_name('Storia Paziente');
                $xcrud->highlight_row('Evento', '=', 'Controllo Specialistico Cardiologico', '#9999FF');
                $xcrud->highlight_row('Evento', '=', 'Controllo MMG', '#6699CC');
                $xcrud->highlight_row('Evento', '=', 'Ricovero', '#669900');
                $xcrud->highlight_row('Evento', '=', 'Controllo Specialistico Pneumologico', '#a98ccc');
                $xcrud->highlight_row('Evento', '=', 'Controllo Specialistico Nefrologico', '#ccaf96');
                $xcrud->highlight_row('Evento', '=', 'Controllo Nutrizionista', '#dcdc79');
                $xcrud->highlight_row('Evento', '=', 'Ricovero non cardiologico', '#ffffff');
                

                echo $xcrud->render();

            ?>
            </div>
            <div  style="margin-top:20px;<? echo $divNoResultStyle ?>">
                Non ci sono ancora altri eventi per questo paziente
            </div>
        </div>
    </div>

    <!-- <script src="xcrud/plugins/jquery.min.js" type="text/javascript"></script> -->
    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>

    <script>

    function nuovoreclutamento()
    {
        window.location.href = 'schedaPaziente.php?id=-1';
    }

    function dead()
    {
        window.location.href = 'dead.php?id=<? echo $userId ?>';
    }

    function reclutamento()
    {
        window.location.href = 'schedaPaziente.php?idQuadro=<? echo $idQuadroReclutamento ?>';
    }

    function logoff()
    {
        window.location.href = 'logout.php';
    }

    function trend()
    {
        window.location.href = 'trend.php?id=<? echo $userId ?>';
    }

    function goMain()
    {
        window.location.href = 'main.php';
    }

    function nuovoControllo()
    {
        window.location.href = 'schedaPaziente.php?id=<? echo $userId ?>&type=NC';
    }

    function nuovoRicovero()
    {
        window.location.href = 'schedaPaziente.php?id=<? echo $userId ?>&type=NRIC';
    }    

    function nuovoRicoveroNonCardio()
    {
        window.location.href = 'ricnoncardio.php?id=<? echo $userId ?>';
    }    



    $( window ).resize(function() {
      var larghezza=$(window).width();
      if (larghezza>=1024) $('#contenuto').css("width","1024px");
      else $('#contenuto').css("width","100%");
      var larghFinestra=$('#contenuto').width();
      //console.log(larghFinestra);
      //console.log(larghezza);
      $('#contenuto').css("margin-left",(larghezza-larghFinestra)/2+"px");

    });

    jQuery(document).ready(function() { 

        $(window).trigger('resize');
        $('#contenuto').show();

    });

    jQuery(document).on("xcrudafterrequest",function(event,container){
        $( ".xcrud-row" ).click(function(event) {
          var cella = $(event.currentTarget.childNodes[4]);
          var span=$(cella[0].childNodes[0]);
          var button=$(span[0].childNodes[0])
          //console.log(button[0].href);
          window.location.href = button[0].href;
        });
        
        $( ".xcrud-row" ).mouseover(function() {
          $(".xcrud-row").css("cursor","pointer");
        });     
    });

    $( ".xcrud-row" ).click(function(event) {
      var cella = $(event.currentTarget.childNodes[4]);
      var span=$(cella[0].childNodes[0]);
      var button=$(span[0].childNodes[0])
      //console.log(button[0].href);
      window.location.href = button[0].href;
    });

    $( ".xcrud-row" ).mouseover(function() {
      $(event.currentTarget).css("cursor","pointer");
    });    

    </script>

</body>
</html>
<?php
ob_start(); 
session_name( 'PHPSESSID' );
session_start();
include_once "./query.php";

    //se non loggato ritorno a login
    if (!isset($_SESSION['IdUtente'])){
        header("location: index.php");//redirect
    }else{
        $idUtente = $_SESSION['IdUtente'];
        $nominativo = $_SESSION['Nominativo'];
        $tipologiaUtente = $_SESSION['Tipologia'];
    }

    include('xcrud/xcrud.php');
    $xcrud = Xcrud::get_instance();
	$db = Xcrud_db::get_instance();

?>
<!DOCTYPE HTML>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title>Statistiche</title>
    <link href="xcrud/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/mediasoft.css" rel="stylesheet" type="text/css" />
</head>
 
<body style="background-color:#cccccc;">

    <div id="tooltip" style="position:absolute;display:none;border:2px solid #ddd;padding:10px;background-color:#f5f5f5;opacity: 0.90;z-index: 9999;color:#000000;"></div> 

    <div id="contenuto" style="width:1024px;margin-left:20%;background-color:#FFFFFF;display:none;">

        <div style="width:100%;">
            <img src="img/testataGisc.jpg" style="width:100%;">
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="logoff();">Esci</button>
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goMain();">Pannello Principale</button>            
            <button class="btn btn-primary xcrud-action" style="float:right;padding-left:30px;padding-right:30px;margin-top:5px;margin-right:5px;" onClick="goStatistiche();">Statistiche</button>
            <div style="clear:both;"></div>
        </div>


        <div style="width:100%;padding:5%;">
            <?php

                //Frequenza cardiaca
                $db->query("SELECT ECGFrequenza FROM quadroclinico where ECGFrequenza IS NOT NULL and ECGFrequenza!='' ORDER BY dataQuadroClinico ASC"); 
                
                $resultFC = $db->result();
                //echo '<pre>'; print_r($resultStat); echo '</pre>';
    
                //mi calcolo la linea di tendenza
                $indice=0;
                $totaleParziale=0;
                $resultFCTrend=null;
                $intervallo= intval(count($resultFC)/50);
                foreach ($resultFC as $item)
                {         
                    
                    //echo ($indice % 50);
                    if ((($indice % $intervallo) == 0) && ($indice!=0))
                    {
                        //echo $totaleParziale."\n";
                        $resultFCTrend[$indice]=$totaleParziale/$intervallo;
                        $totaleParziale=0;
                    }
                    else
                    {
                        //echo $item['ECGFrequenza']."\n";
                        $totaleParziale+=intval($item['ECGFrequenza']);
                    }
                    $indice++;
                }
                if ($totaleParziale>0)
                {
                    //$resultFCTrend[$indice]=$totaleParziale/($indice % $intervallo);
                }
                //echo '<pre>'; print_r($resultFCTrend); echo '</pre>';

                //FE
                $db->query("SELECT CAST(Eco2DFE AS UNSIGNED) FE FROM quadroclinico where Eco2DFE IS NOT NULL and Eco2DFE!='' ORDER BY dataQuadroClinico ASC"); 
                
                $resultFE = $db->result();
                //echo '<pre>'; print_r($resultStat); echo '</pre>';
    
                //mi calcolo la linea di tendenza
                $indice=0;
                $totaleParziale=0;
                $resultFETrend=null;
                $intervallo= intval(count($resultFE)/50);
                foreach ($resultFE as $item)
                {         
                    
                    //echo ($indice % 50);
                    if ((($indice % $intervallo) == 0) && ($indice!=0))
                    {
                        //echo $totaleParziale."\n";
                        $resultFETrend[$indice]=$totaleParziale/$intervallo;
                        $totaleParziale=0;
                    }
                    else
                    {
                        //echo $item['ECGFrequenza']."\n";
                        $totaleParziale+=intval($item['FE']);
                    }

                    $indice++;
                }
                if ($totaleParziale>0)
                {
                    //$resultFETrend[$indice]=$totaleParziale/($indice % $intervallo);
                }
                //echo '<pre>'; print_r($resultFETrend); echo '</pre>';

                //Pressione polmonare
                $db->query("SELECT CAST(EcoDopplerPressPolm AS UNSIGNED) PP FROM quadroclinico where EcoDopplerPressPolm IS NOT NULL and EcoDopplerPressPolm!='' ORDER BY dataQuadroClinico ASC"); 
                
                $resultPP = $db->result();
                //echo '<pre>'; print_r($resultStat); echo '</pre>';
    
                //mi calcolo la linea di tendenza
                $indice=0;
                $totaleParziale=0;
                $resultPPTrend=null;
                $intervallo= intval(count($resultPP)/50);
                foreach ($resultPP as $item)
                {         
                    
                    //echo ($indice % 50);
                    if ((($indice % $intervallo) == 0) && ($indice!=0))
                    {
                        //echo $totaleParziale."\n";
                        $resultPPTrend[$indice]=$totaleParziale/$intervallo;
                        $totaleParziale=0;
                    }
                    else
                    {
                        //echo $item['ECGFrequenza']."\n";
                        $totaleParziale+=intval($item['PP']);
                    }
                    $indice++;
                }
                if ($totaleParziale>0)
                {
                    //$resultPPTrend[$indice]=$totaleParziale/($indice % $intervallo);
                }
                //echo '<pre>'; print_r($resultPPTrend); echo '</pre>';

                //creatininemia
                $db->query("SELECT CAST(IngrCreat AS DECIMAL(10,1)) CR FROM quadroclinico where IngrCreat IS NOT NULL and IngrCreat!='' and CAST(IngrCreat AS DECIMAL(10,1))<8 ORDER BY dataQuadroClinico ASC"); 
                
                $resultCR = $db->result();
                //echo '<pre>'; print_r($resultStat); echo '</pre>';
    
                //mi calcolo la linea di tendenza
                $indice=0;
                $totaleParziale=0;
                $resultCRTrend=null;
                $intervallo= intval(count($resultCR)/50);
                foreach ($resultCR as $item)
                {         
                    
                    //echo ($indice % 50);
                    if ((($indice % $intervallo) == 0) && ($indice!=0))
                    {
                        //echo $totaleParziale."\n";
                        $resultCRTrend[$indice]=$totaleParziale/$intervallo;
                        $totaleParziale=0;
                    }
                    else
                    {
                        //echo $item['ECGFrequenza']."\n";
                        $totaleParziale+=intval($item['CR']);
                    }
                    $indice++;
                }
                if ($totaleParziale>0)
                {
                    //$resultCRTrend[$indice]=$totaleParziale/($indice % $intervallo);
                }
                //echo '<pre>'; print_r($resultCRTrend); echo '</pre>';

                //BNP
                $db->query("SELECT CAST(IngrBNP AS DECIMAL(10,1)) BNP FROM quadroclinico where IngrBNP IS NOT NULL and IngrBNP!='' and CAST(IngrBNP AS DECIMAL(10,1))<14000 ORDER BY dataQuadroClinico ASC"); 
                
                $resultBNP = $db->result();
                //echo '<pre>'; print_r($resultStat); echo '</pre>';
    
                //mi calcolo la linea di tendenza
                $indice=0;
                $totaleParziale=0;
                $resultBNPTrend=null;
                $intervallo= intval(count($resultBNP)/50);
                foreach ($resultBNP as $item)
                {         
                    
                    //echo ($indice % 50);
                    if ((($indice % $intervallo) == 0) && ($indice!=0))
                    {
                        //echo $totaleParziale."\n";
                        $resultBNPTrend[$indice]=$totaleParziale/$intervallo;
                        $totaleParziale=0;
                    }
                    else
                    {
                        //echo $item['ECGFrequenza']."\n";
                        $totaleParziale+=intval($item['BNP']);
                    }
                    $indice++;
                }
                if ($totaleParziale>0)
                {
                    //$resultBNPTrend[$indice]=$totaleParziale/($indice % $intervallo);
                }
                //echo '<pre>'; print_r($resultBNPTrend); echo '</pre>';

                //nyha
                $db->query("SELECT CASE ClasseDispneaSforzo WHEN 'I classe' THEN 1 WHEN 'II classe' THEN 2 WHEN 'III classe' THEN 3 WHEN 'IV classe' THEN 4 ELSE 0 END as nyha FROM quadroclinico where ClasseDispneaSforzo IS NOT NULL order by dataQuadroClinico ASC"); 
                
                $resultNYHA = $db->result();
                //echo '<pre>'; print_r($resultNYHA); echo '</pre>';

                //mi calcolo la linea di tendenza
                $indice=0;
                $totaleParziale=0;
                $resultNYHATrend=null;
                $intervallo= intval(count($resultNYHA)/50);
                //echo $intervallo;

                foreach ($resultNYHA as $item)
                {         
                    
                    //echo ($indice % 50);
                    if ((($indice % $intervallo) == 0) && ($indice!=0))
                    {
                        //echo $totaleParziale."\n";
                        $resultNYHATrend[$indice]=$totaleParziale/$intervallo;
                        $totaleParziale=0;
                    }
                    else
                    {
                        //echo $item['ECGFrequenza']."\n";
                        $totaleParziale+=intval($item['nyha']);
                    }
                    $indice++;
                }
                if ($totaleParziale>0)
                {
//                    $resultNYHATrend[$indice]=$totaleParziale/($indice % $intervallo);
                }
                //echo '<pre>'; print_r($resultNYHATrend); echo '</pre>';





            ?>    




            <div class="col-md-12" style="margin-top:20px;">

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                          Statistiche Frequenza Cardiaca
                        </a>
                      </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                      <div class="panel-body">
                            <div id="chart_frequenza_cardiaca" class="chart" style="height:400px;width: 100%;"></div>       
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true" style="margin-top:20px;">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading2">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapse2" aria-expanded="true" aria-controls="collapse2">
                          Statistiche Fattore di Eiezione
                        </a>
                      </h4>
                    </div>
                    <div id="collapse2" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading2">
                      <div class="panel-body">
                            <div id="chart_fattore_eiezione" class="chart" style="height:400px;width: 100%;"></div>       
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true" style="margin-top:20px;">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading3">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapse3" aria-expanded="true" aria-controls="collapse3">
                          Statistiche Pressione Polmonare
                        </a>
                      </h4>
                    </div>
                    <div id="collapse3" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading3">
                      <div class="panel-body">
                            <div id="chart_pressione_polmonare" class="chart" style="height:400px;width: 100%;"></div>       
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true" style="margin-top:20px;">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading4">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapse4" aria-expanded="true" aria-controls="collapse4">
                          Statistiche Creatininemia
                        </a>
                      </h4>
                    </div>
                    <div id="collapse4" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading4">
                      <div class="panel-body">
                            <div id="chart_creatininemia" class="chart" style="height:400px;width: 100%;"></div>       
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel-group" id="accordion5" role="tablist" aria-multiselectable="true" style="margin-top:20px;">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading5">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion5" href="#collapse5" aria-expanded="true" aria-controls="collapse5">
                          Statistiche Pro-BNP
                        </a>
                      </h4>
                    </div>
                    <div id="collapse5" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading5">
                      <div class="panel-body">
                            <div id="chart_bnp" class="chart" style="height:400px;width: 100%;"></div>       
                      </div>
                    </div>
                  </div>
                </div>

                <div class="panel-group" id="accordion6" role="tablist" aria-multiselectable="true" style="margin-top:20px;">
                  <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="heading6">
                      <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion6" href="#collapse6" aria-expanded="true" aria-controls="collapse6">
                          Statistiche Classe NYHA
                        </a>
                      </h4>
                    </div>
                    <div id="collapse6" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="heading6">
                      <div class="panel-body">
                            <div id="chart_nyha" class="chart" style="height:400px;width: 100%;"></div>       
                      </div>
                    </div>
                  </div>
                </div>
            </div>



        </div>
    </div>

    <script src="xcrud/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="xcrud/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="js/flot/jquery.flot.min.js"></script>
    <script src="js/flot/jquery.flot.resize.min.js"></script>
    <script src="js/flot/jquery.flot.pie.min.js"></script>
    <script src="js/flot/jquery.flot.symbol.js"></script>
    <script src="js/flot/jquery.flot.stack.min.js"></script>
    <script src="js/flot/jquery.flot.axislabels.js"></script>
    <script src="js/flot/jquery.flot.time.js"></script>
    <script src="js/flot/jquery.flot.crosshair.min.js"></script>
    <script src="js/flot/jquery.flot.categories.min.js" type="text/javascript"></script>



    <script>

    function nuovoreclutamento()
    {
        window.location.href = 'schedaPaziente.php?id=-1';
    }

    function logoff()
    {
        window.location.href = 'logout.php';
    }

    function goStatistiche()
    {
        window.location.href = 'statistiche.php';
    }  
    
    function disegnaStatistiche()
    {



        //grafico fc
        var data1 = [
                <?
                    $indice=0;
                    foreach ($resultFC as $value)
                    {
                        echo "[".$indice.",".$value['ECGFrequenza']."],";
                        $indice++;
                    }

                ?>
                [<? echo $indice?>,0]
            ];

        var data2 = [
                <?
                    foreach ($resultFCTrend as $key => $item)
                    {
                        echo "[".$key.",".$item."],";
                    }

                ?>
            ];

        var options = {
            series: {
                points: { show: true,
                radius: 1 },
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            legend: {
                show: false
            },
            colors: [ "#aaaaaa", "#ff0000"]
        };

        $.plot($("#chart_frequenza_cardiaca"), [{data:data1, points: { show: true, radius: 1 }},{data:data2, lines: { show: true }}], options );

        
        //grafico fe
        var data1 = [
                <?
                    $indice=0;
                    foreach ($resultFE as $value)
                    {
                        echo "[".$indice.",".$value['FE']."],";
                        $indice++;
                    }

                ?>
                [<? echo $indice?>,0]
            ];

        var data2 = [
                <?
                    foreach ($resultFETrend as $key => $item)
                    {
                        echo "[".$key.",".$item."],";
                    }

                ?>
            ];

        var options = {
            series: {
                points: { show: true,
                radius: 1 },
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            legend: {
                show: false
            },
            colors: [ "#aaaaaa", "#ff0000"]
        };

        $.plot($("#chart_fattore_eiezione"), [{data:data1, points: { show: true, radius: 1 }},{data:data2, lines: { show: true }}], options );
    
        //grafico pressione_polmonare
        var data1 = [
                <?
                    $indice=0;
                    foreach ($resultPP as $value)
                    {
                        echo "[".$indice.",".$value['PP']."],";
                        $indice++;
                    }

                ?>
                [<? echo $indice?>,0]
            ];

        var data2 = [
                <?
                    foreach ($resultPPTrend as $key => $item)
                    {
                        echo "[".$key.",".$item."],";
                    }

                ?>
            ];

        var options = {
            series: {
                points: { show: true,
                radius: 1 },
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            legend: {
                show: false
            },
            colors: [ "#aaaaaa", "#ff0000"]
        };

        $.plot($("#chart_pressione_polmonare"), [{data:data1, points: { show: true, radius: 1 }},{data:data2, lines: { show: true }}], options );

        //grafico creatininemia
        var data1 = [
                <?
                    $indice=0;
                    foreach ($resultCR as $value)
                    {
                        echo "[".$indice.",".$value['CR']."],";
                        $indice++;
                    }

                ?>
                [<? echo $indice?>,0]
            ];

        var data2 = [
                <?
                    foreach ($resultCRTrend as $key => $item)
                    {
                        echo "[".$key.",".$item."],";
                    }

                ?>
            ];

        var options = {
            series: {
                points: { show: true,
                radius: 1 },
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            legend: {
                show: false
            },
            colors: [ "#aaaaaa", "#ff0000"]
        };

        $.plot($("#chart_creatininemia"), [{data:data1, points: { show: true, radius: 1 }},{data:data2, lines: { show: true }}], options );

        //grafico bnp
        var data1 = [
                <?
                    $indice=0;
                    foreach ($resultBNP as $value)
                    {
                        echo "[".$indice.",".$value['BNP']."],";
                        $indice++;
                    }

                ?>
                [<? echo $indice?>,0]
            ];

        var data2 = [
                <?
                    foreach ($resultBNPTrend as $key => $item)
                    {
                        echo "[".$key.",".$item."],";
                    }

                ?>
            ];

        var options = {
            series: {
                points: { show: true,
                radius: 1 },
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            legend: {
                show: false
            },
            colors: [ "#aaaaaa", "#ff0000"]
        };

        $.plot($("#chart_bnp"), [{data:data1, points: { show: true, radius: 1 }},{data:data2, lines: { show: true }}], options );

        
        //grafico nyha
        var data1 = [
                <?
                    $indice=0;
                    foreach ($resultNYHA as $value)
                    {
                        echo "[".$indice.",".$value['nyha']."],";
                        $indice++;
                    }

                ?>
                [<? echo $indice?>,0]
            ];

        var data2 = [
                <?
                    foreach ($resultNYHATrend as $key => $item)
                    {
                        echo "[".$key.",".$item."],";
                    }

                ?>
            ];

        var options = {
            series: {
                points: { show: true,
                radius: 1 },
            },
            grid: {
                hoverable: true,
                borderWidth: 0
            },
            legend: {
                show: false
            },
            colors: [ "#aaaaaa", "#ff0000"]
        };

        $.plot($("#chart_nyha"), [{data:data1, points: { show: true, radius: 1 }},{data:data2, lines: { show: true }}], options );




    }


    $( window ).resize(function() {
      var larghezza=$(window).width();
      if (larghezza>=1024) $('#contenuto').css("width","1024px");
      else $('#contenuto').css("width","100%");
      var larghFinestra=$('#contenuto').width();
      //console.log(larghFinestra);
      //console.log(larghezza);
      $('#contenuto').css("margin-left",(larghezza-larghFinestra)/2+"px");

    });

    function goMain()
    {
        window.location.href = 'main.php';
    }    

    jQuery(document).ready(function() { 

        $(window).trigger('resize');
        $('#contenuto').show();
        disegnaStatistiche();

    });

    //$('.collapse').collapse()
    </script>


</body>
</html>